/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License ("CDDL")(the "License"). You
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language
 * governing permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL Header Notice
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the
 * fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.datacleanser.util;

import com.sun.mdm.index.dataobject.DataObject;
import com.sun.mdm.index.util.Logger;
import java.text.ParseException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * This class injects the system fields whenever the source is staging database.
 * Only the Global Identifier is computed and injected for good file sources.
 * User code and system code are taken as inputs.
 *
 * For Data Integrator source extraction use-case:
 *
 *      SystemCode and UserCode are taken as input and GID,LID and updateDate are computed
 *      UpdateDate could come from the source system. SystemCode and UserCode could be
 *      populated during source extraction. This needs some revisit.
 *
 * For Good file sources:
 *      Only GID is computed and injected locally. Rest of the fields are retained as is from the
 *      source file
 *
 *
 * @author srengara@dev.java.net
 */
public class SystemFieldGenerator {

    private static final String USER_CODE = "LOADER";
    private static final Logger logger = Logger.getLogger(SystemFieldGenerator.class.getName());
    private static long seqCounter;
    private static SimpleDateFormat dateFormat;

    static {
        try {

            SequenceGenerator.createSequence("LID_SEQ", 0);
        } catch (Exception ex) {
            logger.fatal(ex.getLocalizedMessage());
        }
    }

    public static void setSequenceCounter(long count) {
        seqCounter = count;
        try {
            SequenceGenerator.createSequence("GID_SEQ", seqCounter);
        } catch (Exception ex) {
            logger.fatal(ex.getLocalizedMessage());
        }
    }

    public static void setDateFormat(String format) {
        try {
            dateFormat = new SimpleDateFormat(format);
        } catch (Exception ex) {
            dateFormat = null;
        }
    }

    /**
     * injects systemCode, Local Id, userCode and update Date fields into the Data Object
     * @param systemCode uniquely identifies the source system
     * @param localId uniquely identifies the record within the source system context
     * @param updateDate specifies the last updated Date for the record
     * @param userCode identifies the user
     * @param DataObject object into which the fields are to be injected
     * @return void
     **/
    public static void appendSystemFields(String systemCode, String localId, Date updateDate, String userCode, DataObject dataObject) {
        int index = 0;
        String strData = dataObject.getFieldValue(0);
        String tmpUpdateDate = dataObject.getFieldValue(3);
        if (strData == null || strData.trim().length() == 0) {
            dataObject.setFieldValue(index, String.valueOf(SequenceGenerator.getNextValue("GID_SEQ")));
        }
        //dataObject.setFieldValue(index++, systemCode);
        /*if (localId == null || "".equalsIgnoreCase(localId)) {
        localId = String.valueOf(SequenceGenerator.getNextValue("LID_SEQ"));
        }*/
        //dataObject.setFieldValue(index++, localId);
        index = index + 3;
        String formattedDateString = null;
        Date date = null;
        if (tmpUpdateDate == null || tmpUpdateDate.trim().length() <= 0) {
            date = new Date(System.currentTimeMillis());

        } else {
            SimpleDateFormat dt = new SimpleDateFormat("dd-MMM-yy hh:mm:ss");
            if (dt != null) {
                try {
                    date = dt.parse(tmpUpdateDate);
                } catch (ParseException ex) {
                    date = new Date(System.currentTimeMillis());
                }
            } else {
                date = new Date(System.currentTimeMillis());
            }

        }
        if (dateFormat != null) {
            formattedDateString = dateFormat.format(date);

        } else {
            formattedDateString = DateFormat.getInstance().format(date);
        }

        // Date date = (updateDate == null) ? new Date(System.currentTimeMillis()) : updateDate;
        // formattedDateString = DateFormat.getInstance().format(date);
        dataObject.setFieldValue(index, formattedDateString);
    /*if (userCode == null || "".equalsIgnoreCase(userCode)) {
    userCode = USER_CODE;
    }*/
    //dataObject.setFieldValue(index, userCode);
    }
}
