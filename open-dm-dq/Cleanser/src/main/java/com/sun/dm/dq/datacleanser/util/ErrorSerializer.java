/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.datacleanser.util;


import java.util.ArrayList;
import java.util.ListIterator;
import com.sun.dm.dq.rules.output.RuleErrorObject;

/**
 *
 * @author admin
 */
public class ErrorSerializer {
    
    /**
     * utility method to generate error string from the results of the rule execution
     * in the form
     * {Rule Step Name:Failure reason::......}
     * It serializes the string according to the hierarchy of rules 
     * @param errors list of rulelist which is a list of rules
     * @return String serialized error string which is appended to the reject file
     **/
    public static String serializeRuleErrors(ArrayList<ArrayList<RuleErrorObject>> errors) {
        StringBuffer bf = new StringBuffer("{");
        if (errors == null) {
            return null;
        }
        ListIterator rlListItr = errors.listIterator();

        ListIterator rlListItr1 = null;
        ArrayList<RuleErrorObject> ro = new ArrayList<RuleErrorObject>();
        while (rlListItr.hasNext()) {
            ro = (ArrayList<RuleErrorObject>) rlListItr.next();
            if (ro == null) {
                return null;
            }
            rlListItr1 = ro.listIterator();

            while (rlListItr1.hasNext()) {
                RuleErrorObject var = (RuleErrorObject) rlListItr1.next();
                bf.append(var.getEPath());
                bf.append(var.getRuleStepName());
                bf.append(":").append(var.getFailReason());
                bf.append("::");
            }
        }
        bf.append("}");
        return bf.toString();
    }
            
}
