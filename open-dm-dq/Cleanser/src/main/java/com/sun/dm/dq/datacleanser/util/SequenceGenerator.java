/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.datacleanser.util;

import com.sun.dm.dq.util.Localizer;
import java.util.HashMap;
import java.util.Map;
/**
 * This utility is used to generate the Unique Global identifier used by the bulk loader
 * Id starts with the initValue given and always increments by 1. Could be enhanced to 
 * take increment as well. This generator is locked at class level to make sequence 
 * operations thread-safe. Main consumer of this class is Data Cleanser utility. In 
 * cases of multiple runs of Data Cleanser, it should recreate the sequence with 
 * appropriate initial value to start from where it left during the last run. 
 * @author admin
 */
public class SequenceGenerator {
    
    
    /**
     * Internationalization utility to generate resource bundles from Java Code. 
     */
    private final static Localizer sLoc = Localizer.get();
    
    /**
     * Holds the sequences and their current states
     **/
    private static  Map<String,Long> sequenceMap = new HashMap<String,Long>();
    
    /**
     * This method creates a sequence given sequence name and initialValue.
     * @param name a String representing sequence name
     * @param initValue a int value representing start value of sequence 
     * @return void
     */
    public static synchronized void createSequence(String name, long initValue) throws Exception {
        if( !sequenceMap.containsKey(name)) {
            sequenceMap.put(name, new Long(initValue));
        } else {
            throw new Exception(sLoc.x("SEQ001: Sequence with given name {0} Already Exist", name).toString());
        }
    }
    
    /**
     * This method generates next value of the sequence given the sequence name
     * @param sequenceName a String identifiying the sequence
     * @return int next value of the sequence
     **/
    public static long getNextValue(String sequenceName) {
        long nextValue = -1;
        synchronized(SequenceGenerator.class) {
            long currentValue = sequenceMap.get(sequenceName);
            long currentVal = currentValue;
            nextValue = ++currentVal;
            sequenceMap.put(sequenceName, new Long(nextValue));
        }
        return nextValue;
    }
    
    
    public static void main(String[] args) throws Exception {
        int initValue = 0;
        createSequence("GID_SEQ",initValue);
        createSequence("GID_SEQ",initValue);
        
        for(int i=0;i<100;i++) {
            System.out.println(getNextValue("GID_SEQ"));
        }
    }
    

}
