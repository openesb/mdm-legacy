<?xml version="1.0"?>
<?xml-stylesheet href="simple-document.xsl" type="text/xsl"?>
<document>
    <title>
        Standardization Plugin Usage Example
    </title>
    <author>
        <a href="mailto:ricardo.rocha@sun.com">Ricardo Rocha</a>
    </author>
    <body>
        <h1>Introduction</h1>
        <p>
                This document summarizes the procedures required to deploy and
                configure a Mural standardization application inside a modern
                eView project as well as to execute it in a generated EDM web
                application.
        </p>

        <p>
                The steps presented are:
        </p>

        <ul>
                <li>
                        <b>Importing a standardization plugin</b> inside
                        an eView project, both for data types and variants. 
                </li>
                <li>
                        <b>Configuring the eView project</b> to specify
                        how to support deployed standardization types.
                </li>
                <li>
                        <b>Exercising field standardization</b> from the
                        generated EDM web application.
                </li>
        </ul>

        <p>
                Central to Mural's standardization are the notions of
        </p>

        <ul>
                <li>
                        <em>Input Record</em>. A structured string that is
                        parsed and canonicalized in order to identify and
                        separate its constituent parts.
                </li>
                <li>
                        <em>Data Type</em>. A category describing a family
                        of input records. Examples include <i>Address</i>,
                        <i>PhoneNumber</i> or <i>PersonName</i>.
                </li>
                <li>
                        <em>Variant</em>. A subclassification within data
                        type that further specializes it. A typical case of
                        variant is that of locale-specific domains such as
                        <i>UKAddress</i> or <i>USPhoneNumber</i>.
                        Some data types do not lend themselves to
                        specialization by locale. In this case it is
                        customary for Mural data types to have a single
                        variant called <i>Generic</i>.
                </li>
                <li>
                        <em>Standardization</em>. The process of parsing
                        a free-form string to identify and extract its
                        constituent parts. The result of standarization
                        is a collection of standardized records each
                        specifying the names and values of
                        <em>output fields</em> resulting from the
                        standardization process.
                </li>
                <li>
                        <em>Standardized Record</em>. A data structure
                        containing one or more groups of string tokens
                        each identified by an associated couple of
                        <em>input</em> and <em>output</em> field names.
                        <em>Input fields</em> correspond to the token
                        types identified in the source record string
                        while <em>output fields</em> correspond to the
                        assigned types in the resulting standardized record.
                </li>
                <li>
                        <em>Normalization</em>. The process of substituting
                        a given string (as qualified by a field name) by
                        a canonical representation. This includes, for
                        example, transforming a first name of value
                        <i>Bobby</i> to the base form
                        <i>Robert</i> or the long address form
                        <i>Boulevard</i> by the more commonly used
                        abbreviation <i>Blvd</i>.
                </li>
        </ul>

        <h1>Preparing the eView project</h1>
        <p>
                This section describes how to prepare an eView project in
                order to deploy a standardization data type and/or variant.
        </p>
        <p>
                Whenever a new eView project is created it's automatically
                populated with the four built-in standardization data types
                supplied with mural:
        </p>
        <figure src="img/project-02.png"/>
        <p>
                These data types are:
        </p>
        <ul>
                <li>
                        <b>Address</b>. Postal addresses with built-in
                        support for the following variants:
                        <ul>
                                <li><em>AU</em>. Australia</li>
                                <li><em>FR</em>. France<sup>(1)</sup></li>
                                <li><em>UK</em>. United Kingdom</li>
                                <li><em>US</em>. United States of America</li>
                        </ul>
                </li>
                <li>
                        <b>BusinessName</b>. Company names with a single
                        <code>Generic</code> variant.
                </li>
                <li>
                        <b>PersonName</b>. Person name standardization and
                        normalization with built-in support for the following
                        variants:
                        <ul>
                                <li><em>AU</em>. Australia</li>
                                <li><em>FR</em>. France</li>
                                <li><em>UK</em>. United Kingdom</li>
                                <li><em>US</em>. United States of America</li>
                        </ul>
                </li>
                <li>
                        <b>PhoneNumber</b><sup>(2)</sup>. Phone numbers with
                        a single <code>Generic</code> variant.
                </li>
        </ul>
        <p class="note">
                (1) French support for addresses has been placed in a
                separate zip deployment file.
        </p>
        <p class="note">
                (2) Currently, phone number standardization only supports 
                US-style phone numbers but it can be easily extended to
                accomodate other variants.
        </p>
        <p>
                In this section we'll illustrate how to import both variant
                and data type deployment files. In order to do this, it's
                necessary to first remove the corresponding variant and
                data type originally imported by the eView project
                generator. After removal, the variant and data type used
                in this example are then re-imported using the plugin
                mechanism.
        </p>

        <h2>Removing an Existing Variant</h2>
        <p>
                The <code>FR</code> variant of the <code>Address</code>
                data type is included among the Mural built-ins:
        </p>
        <figure src="img/project-03.png"/>
        <p>
                For our tutorial purposes we'll remove the French variant
                of the Address data type in order to illustrate how to
                deploy a variant on top of an existing data type.
        </p>
        <figure src="img/project-04.png"/>
        <p>
                After variant removal the left-hand side of the eView
                project should look like:
        </p>
        <figure src="img/project-05.png"/>
        <p>
                We'll also remove the <code>PhoneNumber</code> data type
                along with its single variant <code>Generic</code>:
        </p>
        <figure src="img/project-06.png"/>
        <p>
                After these deletions the left-hand side of the eView
                project should look like:
        </p>
        <figure src="img/project-07.png"/>

        <h2>Importing a Variant into an Existing Data Type</h2>
        <p>
                Next, we import the standardization plugin corresponding
                to the <code>FR</code> variant of the <code>Address</code>
                data type:
        </p>
        <figure src="img/project-08.png"/>
        <p>
                We're presented with a file selection dialog like:
        </p>
        <figure src="img/project-09.png"/>
        <p>
                In its current configuration, eView stores its data type
                and variant deployment files under the
                <code>$NETBEANS/soa1/modules/ext/eview/standardizer/deployment</code>
                directory.
        </p>
        <p>
                The newly imported <code>FR</code> variant should now display
                along with the other <code>Address</code> variants:
        </p>
        <figure src="img/project-10.png"/>
        <p>
                We can now expand the variant's tree node to reveal its
                <code>resource</code> subdirectory where all the variant
                configuration files reside. This files can be edited to
                customize the behavior of the address standardizer:
        </p>
        <figure src="img/project-11.png"/>

        <h2>Importing an Entire Data Type</h2>
        <p>
                The mechanism used to import a data type (and any built-in
                variants it may support) we use the same mechanism used to
                import the <code>FR</code> variant above: we right-click on
                the <code>Configuration</code> subdirectory and request the
                import of a standardization plugin:

        </p>
        <figure src="img/project-08.png"/>
        <p>
                We now import file <code>PhoneNumber.zip</code> from
                eView's standardization deployment directory.
        </p>
        <p>
                After importing the <code>PhoneNumber</code> data type
                we can expand its associated standardization project
                directory so that it looks like:
        </p>
        <figure src="img/project-12.png"/>
        <p>
                The core configuration file for standardization is
                <code>standardizer.xml</code>. This file contains the
                state model, input and output field definitions and
                normalization rules for any type of standardization.
                By populating this file with the proper declarative
                definitions it is possible to configure virtually
                any type of standardization without the need to
                resort to Java code. Of course, it's possible to
                write Java classes implementing framework-defined
                interfaces in order to produce more customized
                instantiations of the framework.
        </p>
        <p class="note">
                NOTE:
                The standardization data types <code>Address</code> and
                <code>BusinessName</code> are built on top of a rule-based
                legacy framework that differs from the new, finite state
                machine-based framework.
                Configuration of these data types requeries editing of
                the old text files supported by eView Classic.
        </p>

        <h2>Configuring the Database</h2>
        <p>
                The next step is to configure the database as to add to it
                new fields required by for phone number standardization.
        </p>
        <figure src="img/project-13.png"/>
        <p>
                A dialog is diplayed in the right-hand side of the eView
                project that presents the project's database structure:
        </p>
        <figure src="img/project-14.png"/>
        <p>
                After expanding the <code>Person</code> master entity
                we'll see:
        </p>
        <figure src="img/project-15.png"/>
        <p>
                We're now ready to add the required fields to the
                <code>Phone</code> database table. These fields are drawn from
                the set of output fields specified by the
                <code>PhoneNumber</code> data type deployment descriptor:
        </p>
        <figure src="img/serviceType.png"/>
        <p>
                To add a new field we right-click on the <code>Phone</code>
                table:
        </p>
        <figure src="img/project-16.png"/>
        <p>
                We then add a <code>PhoneNo</code> field which will contain
                the free-form phone number to be later standardized in the
                generated EDM web application.
        </p>
        <figure src="img/project-17.png"/>
        <p>
                Other fields are added until we have the complete output
                fields which are populated by the phone number normalization:
        </p>
        <figure src="img/project-18.png"/>

        <p class="note">
                NOTE: Due to a bug in the current build of eView, after
                altering the database schema it's necessary to save and close
                the configuration tab (called <code>Person</code>) and then
                re-open it by right-clicking on the <code>Configuration</code>
                project directory again.
        </p>

        <h2>Configuring the Standardization</h2>
        <p>
                We now add and configure phone number standardization by
                selecting the <code>Standardization</code> tab. When the
                <code>Add</code> standardization button is clicked a
                dialog appears that allows us to enter the
                <code>PhoneNumber</code> configuration:
        </p>
        <figure src="img/project-19.png"/>
        <p>
                After selecting <code>PhoneNumber</code> as our
                standardization data type we select the source database
                field to be standardized:
        </p>
        <figure src="img/project-20.png"/>
        <p>
                We're now ready to map the newly created database field with
                the output fields defined by the <code>PhoneNumber</code>
                data type:
        </p>
        <figure src="img/project-21.png"/>
        <p>
                After mapping all of the relevant fields our configuration
                looks like:
        </p>
        <figure src="img/project-22.png"/>
        <p>
                After we have configured the <code>PhoneNumber</code>
                standardization the project's corresponding tab should
                look like:
        </p>
        <figure src="img/project-23.png"/>

        <h2>Generating and Deploying</h2>
        <p>
                After having configured the data types and their variants
                as well as the database and its mappings we must generate the
                master index files and the application deployment
                (<code>ear</code>) file.
        </p>
        <p class="note">
                Database scripts
        </p>
        <p>
                The next (and last) design-time step is to deploy the
                <code>ear</code> file onto the application server.
        </p>

        <h1>Executing the EDM Application</h1>
        <p>
                Once the EDM application has been properly deployed we're
                ready to test phone number standardization:
        </p>
        <p>
                For this we create a <code>SystemRecord</code> like in:
        </p>
        <figure src="img/edm-01.png"/>
        <p>
                Next, we enter <code>Person</code> data like in:
        </p>
        <figure src="img/edm-02.png"/>
        <p>
                We're now ready to add a phone whose <code>PhoneNo</code>
                field will be standardized.
        </p>
        <figure src="img/edm-03.png"/>
        <p>
                After adding and commiting the above phone number
                standardization will be applied to yield the following
                results:
        </p>
        <figure src="img/edm-04.png"/>
        <p>
                A quick glimpse reveals how these additional fields have been
                populated in the database:
        </p>
        <figure src="img/db-01.png"/>
    </body>
</document>
