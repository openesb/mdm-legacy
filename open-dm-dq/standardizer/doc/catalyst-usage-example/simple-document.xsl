<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output indent="yes" method="html" encoding="ISO-8859-1"/>

	<xsl:template match="document">
		<html>
			<head>
				<title>
					<xsl:value-of select="string(title)"/>
				</title>
				<link rel="stylesheet" href="document.css" type="text/css" media="all"/>
			</head>
			<xsl:apply-templates select="body"/>
		</html>
	</xsl:template>

	<xsl:template match="body">
		<body>
			<xsl:apply-templates select="@*"/>
			<h1 class="topTitle">
				<xsl:apply-templates select="../title/node()"/>
			</h1>
            <div style="text-align: right">
                <xsl:text>By: </xsl:text>
                <xsl:apply-templates select="../author"/>
            </div>
			<xsl:apply-templates/>
		</body>
	</xsl:template>

	<xsl:template match="figure">
		<div style="text-align: center">
            <img border="1" src="{@src}"/>
		</div>
	</xsl:template>

	<xsl:template match="@*|node()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
