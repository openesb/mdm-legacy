<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output indent="yes" method="html" encoding="ISO-8859-1"/>

	<xsl:template match="document">
		<html>
			<head>
				<title>
					<xsl:value-of select="string(title)"/>
				</title>
				<link rel="stylesheet" href="document.css" type="text/css" media="all"/>
			</head>
			<xsl:apply-templates select="body"/>
		</html>
	</xsl:template>

	<xsl:template match="body">
		<body>
			<xsl:apply-templates select="@*"/>
			<h1 class="topTitle">
				<xsl:apply-templates select="../title/node()"/>
			</h1>

            <xsl:if test="../author">
                <table border="0" align="right">
                    <tr>
                        <td valign="top">By:</td>
                        <td>
                            <xsl:for-each select="../author">
                                <xsl:choose>
                                    <xsl:when test="@email">
                                        <a href="mailto: {@email}">
                                            <xsl:value-of select="string(.)"/>
                                        </a>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="string(.)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <br/>
                            </xsl:for-each>
                        </td>
                    </tr>
                </table>
            </xsl:if>

			<xsl:apply-templates/>
		</body>
	</xsl:template>

	<xsl:template match="snippet">
        <table border="1" align="center">
            <tr>
                <td>
                    <blockquote>
                        <code class="block">
                            <pre><xsl:apply-templates/></pre>
                        </code>
                    </blockquote>
                </td>
            </tr>
        </table>
	</xsl:template>

	<xsl:template match="figure">
		<div style="text-align: center">
            <xsl:variable name="border">
                <xsl:choose>
                    <xsl:when test="@border">
                        <xsl:value-of select="@border"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>1</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <img border="{$border}" src="{@src}"/>
		</div>
	</xsl:template>

	<xsl:template match="todo">
        <fieldset>
            <legend>&#160;TODO&#160;</legend>
            <div style="font-size: 80%">
                <xsl:apply-templates/>
            </div>
        </fieldset>
	</xsl:template>

	<xsl:template match="link">
        <a href="{@href}"><xsl:apply-templates/></a>
	</xsl:template>

	<xsl:template match="@*|node()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
