/address\.set[A-Z]/ {
    tokens = gensub(/.*address\.set([A-Za-z]+)\((.*)\);/, "\\1|\\2", "g")
    split(tokens, arr, "|")
    propertyName = arr[1]
    propertyValue = arr[2]

    propertySymbol = ""
    for (i = 1; i <= length(propertyName); i++) {
        letter = substr(propertyName, i, 1) 
        if (i > 1 && letter >= "A" && letter <= "Z") {
            propertySymbol = propertySymbol "_"
        }
        if (letter >= "a" && letter <= "z") {
            letter = toupper(letter)
        }
        propertySymbol = propertySymbol letter
    }
    gsub(/address.set[A-Za-z]+\(.*\);/, "address.set(Address." propertySymbol ", " propertyValue ");")
}
{ print }
