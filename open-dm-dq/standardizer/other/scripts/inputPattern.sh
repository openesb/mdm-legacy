#!/bin/bash
LOCALES="AU FR UK US"
DATA_DIR=../../data/config/sbme/standardizer
CONFIG_DIR=../../data/config/mdm/standardizer/address

for locale in $LOCALES
do
    awk -f inputPattern.awk $DATA_DIR/addressPatterns$locale.dat > $CONFIG_DIR/patterns$locale.xml
done
