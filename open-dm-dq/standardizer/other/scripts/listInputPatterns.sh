#!/bin/bash
DATA_DIR=../../data/config/sbme/standardizer

sort -u knownInputPatternSymbols.txt -o knownInputPatternSymbols.txt

awk '{
        cnt = split(substr($0, 1, 35), fields)
        for (i = 1; i <= cnt; i++)
            print $i

        getline; getline
}' $DATA_DIR/addressPatterns??.dat |\
sort -u |\
join -1 1 -2 1 -a 1 -o 1.1 2.2 - knownInputPatternSymbols.txt |\
awk '{
    currentSymbol = $1
    currentName = $2

    if (currentName == "") {
        suffix = substr(currentSymbol, 2, 1)
        if (suffix == "+")
            suffix = "_PLUS"
        if (suffix == "*")
            suffix = "_STAR"

        currentName = "UNKNOWN_" substr(currentSymbol, 1, 1) suffix
    }

    symbol[NR] = currentSymbol
    name[NR] = currentName
}
END {
    for (i = 1; i <= NR; i++) {
        terminator = ","
        if (i == NR)
            terminator = ";"
        print "    "  name[i] "(\"" symbol[i] "\")" terminator
    }
}' |\
sort
