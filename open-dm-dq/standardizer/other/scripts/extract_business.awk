BEGIN {
    FS = "\t"
    code["AUSTRALIA"] = "au";
    code["FRANCE"] = "fr";
    code["UNITED KINGDOM"] = "uk";
    code["UNITED STATES"] = "us";
}
code[$26] != "" {
    print $17 | "sort -u -o business-input-" code[$26] ".txt"
}
