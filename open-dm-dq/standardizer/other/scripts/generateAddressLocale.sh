#!/bin/bash
INPUT_DIR=../data/config/sbme/standardizer
OUTPUT_DIR=../data/config/dmd/META-INF/dmd/address

for locale in AU FR UK US
do
    constantsFile=$INPUT_DIR/addressConstants${locale}.cfg
    if [ ! -e $constantsFile ]
    then
        constantsFile=$INPUT_DIR/addressConstants.cfg
    fi

    internalConstantsFile=$INPUT_DIR/addressInternalConstants${locale}.cfg
    if [ ! -r $internalConstantsFile ]
    then
        internalConstantsFile=$INPUT_DIR/addressInternalConstants.cfg
    fi

    outputFile=$OUTPUT_DIR/addressLocale${locale}.xml

    cat > $outputFile <<!
<?xml version="1.0"?>
<locale id="$locale" class="com.sun.mdm.standardizer.test.datatype.address.locale.${locale}AddressLocale">
  <properties>
!

    awk '/^[a-z][A-Z]/ {$0 = toupper(substr($0, 1, 1)) substr($0, 2)} {print}' $constantsFile |\
    sed -f generateAddressLocale.sed >> $outputFile

    awk '/^[a-z][A-Z]/ {$0 = toupper(substr($0, 1, 1)) substr($0, 2)} {print}' $internalConstantsFile |\
    sed -f generateAddressLocale.sed >> $outputFile

    cat >> $outputFile <<!
  </properties>
</locale>
!
done
