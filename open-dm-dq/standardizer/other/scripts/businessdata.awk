BEGIN {
    FS = "\t"
    OFS = ":"
}
{
    for (i = 1; i <= NF; i++)
        print i, $i
    printf("\n")
}
