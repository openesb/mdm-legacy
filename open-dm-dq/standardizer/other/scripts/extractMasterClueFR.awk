BEGIN {
    OFS = "\t"
}

{
    id = extract(1, 4)
    type = extract(71, 2)
    fullName = extract(5, 33)
    standardAbbreviation = extract(39, 13)
    shortAbbreviation = extract(53, 5)
    uspsAbbreviation = extract(59, 4)

    print id, \
          type, \
          fullName, \
          standardAbbreviation, \
          shortAbbreviation, \
          uspsAbbreviation
}

function extract(offset, len) {
    result = substr($0, offset, len)
    gsub(/^[	 ]*/, "", result)
    gsub(/[	 ]*$/, "", result)
    gsub(/\&/, "&amp;", result)
    return result
}
