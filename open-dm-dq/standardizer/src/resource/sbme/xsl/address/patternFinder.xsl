<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmAddressPatternFinder">
		<bean id="patternFinder" class="com.sun.mdm.sbme.pattern.DefaultPatternFinder">
			<property name="outputPatternBuilder">
				<bean class="com.sun.mdm.sbme.datatype.address.pattern.AddressOutputPatternBuilder">
					<property name="patternRegistry">
						<ref local="patternRegistry"/>
					</property>
				</bean>
			</property>
			<property name="outputPatternScorer">
				<bean class="com.sun.mdm.sbme.datatype.address.pattern.AddressOutputPatternScorer">
					<property name="unmatchedPatternFactor">
						<value>
							<xsl:value-of select="string(unmatchedPatternFactor)"/>
						</value>
					</property>
					<property name="patternWeight">
						<value>
							<xsl:value-of select="string(patternWeight)"/>
						</value>
					</property>
				</bean>
			</property>
			<xsl:if test="postprocessors">
				<property name="postprocessors">
					<list>
						<xsl:apply-templates select="postprocessors/*"/>
					</list>
				</property>
			</xsl:if>
		</bean>
	</xsl:template>
	
	<xsl:template match="javaPatternFinder">
		<bean id="patternFinder">
			<xsl:apply-templates select="@*|node()"/>
		</bean>
	</xsl:template>

</xsl:stylesheet>
