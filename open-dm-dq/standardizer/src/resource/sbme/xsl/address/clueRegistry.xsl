<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="clues">
		<bean id="clueRegistry" class="com.sun.mdm.sbme.clue.ClueRegistry">
			<constructor-arg>
				<map>
					<xsl:apply-templates/>
				</map>
			</constructor-arg>
		</bean>
	</xsl:template>
	
	<xsl:template match="clues/clue">
		<entry>
			<key>
				<value><xsl:value-of select="normalize-space(string(name))"/></value>
			</key>
			<bean class="com.sun.mdm.sbme.clue.Clue">
				<property name="name">
					<value>
						<xsl:value-of select="normalize-space(string(name))"/>
					</value>
				</property>				
				<property name="translation">
					<value>
						<xsl:value-of select="normalize-space(string(translation))"/>
					</value>
				</property>				
				<property name="translationExpanded">
					<value>
						<xsl:value-of select="normalize-space(string(translationExpanded))"/>
					</value>
				</property>				
				<property name="words">
					<list>
						<xsl:for-each select="words/word">
							<bean class="com.sun.mdm.sbme.normalizer.ClueWord">
								<constructor-arg>
									<value>
										<xsl:value-of select="normalize-space(string(id))"/>
									</value>
								</constructor-arg>
								<constructor-arg>
									<value type="com.sun.mdm.sbme.datatype.address.pattern.InputTokenType"><xsl:value-of select="normalize-space(string(type))"/></value>
								</constructor-arg>
							</bean>
						</xsl:for-each>
					</list>
				</property>
			</bean>
		</entry>
	</xsl:template>

</xsl:stylesheet>
