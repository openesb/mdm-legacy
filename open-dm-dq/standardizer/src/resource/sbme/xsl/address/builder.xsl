<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmAddressBuilder">
		<bean id="builder" class="com.sun.mdm.sbme.builder.DefaultStandardizedRecordBuilder">
		<property name="standardizedRecordFactory">
			<bean class="com.sun.mdm.sbme.datatype.address.builder.AddressFactory"/>
		</property>
			<property name="preprocessors">
				<list>
					<xsl:for-each select="preprocessors/*">
						<bean class="{@class}">
							<xsl:apply-templates select="property"/>
						</bean>
					</xsl:for-each>
				</list>
			</property>
			<property name="outputTokenTypeHandlers">
				<list>
					<xsl:apply-templates select="propertyBuilders/*" />
				</list>
			</property>
			<property name="postprocessors">
				<list>
					<xsl:for-each select="postprocessors/*">
						<bean class="{@class}">
							<property name="locale">
								<ref local="locale"/>
							</property>
							<property name="clueRegistry">
								<ref local="clueRegistry"/>
							</property>
							<property name="masterClueRegistry">
								<ref local="masterClueRegistry"/>
							</property>
							<xsl:apply-templates select="property"/>
						</bean>
					</xsl:for-each>
				</list>
			</property>
		</bean>
	</xsl:template>

	<xsl:template match="propertyBuilders/propertyBuilder">
		<bean class="com.sun.mdm.sbme.builder.OutputTokenTypeHandler">
			<constructor-arg>
				<bean class="{@class}">
					<property name="locale">
						<ref local="locale"/>
					</property>
					<property name="clueRegistry">
						<ref local="clueRegistry"/>
					</property>
					<property name="masterClueRegistry">
						<ref local="masterClueRegistry"/>
					</property>
					<xsl:apply-templates select="property"/>
				</bean>
			</constructor-arg>
			<constructor-arg>
				<list>
					<xsl:for-each select="outputTokenType">
						<value type="com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType">
							<xsl:value-of select="normalize-space(string(.))"/>
						</value>
					</xsl:for-each>
				</list>
			</constructor-arg>
		</bean>
	</xsl:template>
</xsl:stylesheet>
