<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" indent="yes"/>

   <xsl:template match="keys/country/name">
   		<value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>
   <xsl:template match="keys/country/code">
       <value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>
   <xsl:template match="keys/country/nationality">
       <value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>
      
</xsl:stylesheet>
