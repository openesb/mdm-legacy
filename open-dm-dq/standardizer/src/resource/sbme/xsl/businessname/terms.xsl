<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:util="http://www.springframework.org/schema/util">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="terms">
		<util:list id="generalTerms">
			<xsl:apply-templates/>
		</util:list>
	</xsl:template>
	
	<xsl:template match="terms/general">
		<value><xsl:value-of select="normalize-space(string(termName))"/></value>
	</xsl:template>


</xsl:stylesheet>
