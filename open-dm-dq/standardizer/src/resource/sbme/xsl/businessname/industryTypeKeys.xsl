<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" indent="yes"/>
   
   <xsl:template match="keys/industryTypeRegistry/industryType">
		<entry>
			<key>
				<value><xsl:value-of select="normalize-space(string(industry))"/></value>
			</key>
			<bean class="com.sun.mdm.sbme.datatype.businessname.BusinessNameIndustryType">
				<constructor-arg>
					<value><xsl:value-of select="normalize-space(string(industry))"/></value>
				</constructor-arg>
				<constructor-arg>
					<value><xsl:value-of select="normalize-space(string(standardForm))"/></value>
				</constructor-arg>
				<constructor-arg>
					<list>
						<xsl:apply-templates select="sectorCode/code"/>
					</list>
				</constructor-arg>
			</bean>
		</entry>
	</xsl:template>
	
	<xsl:template match="keys/industryTypeRegistry/industryType/sectorCode/code">
		<value><xsl:value-of select="normalize-space(string(.))"/></value>
	</xsl:template>
   	
</xsl:stylesheet>
   
