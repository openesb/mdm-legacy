package com.sun.mdm.standardizer.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.mdm.standardizer.StandardizedRecord;

public class StandardizedRecordImpl implements StandardizedRecord {
	private Map<String, List<Field>> fieldMap;
	private Map<String, Object> parameters;
	
	public StandardizedRecordImpl() {
	    this(new ArrayList<Field>());
	}

	public StandardizedRecordImpl(List<Field> fields) {
		if (fields == null) {
			throw new NullPointerException("Null output fieldMap specified");
		}

		this.fieldMap = new LinkedHashMap<String, List<Field>>(fields.size());
		for (Field field : fields) {
			if (field.getOutputSymbol() != null) {
				List<Field> fieldValues = this.fieldMap.get(field.getOutputSymbol().getName());
				if (fieldValues == null) {
					fieldValues = new ArrayList<Field>();
					this.fieldMap.put(field.getOutputSymbol().getName(), fieldValues);
				}
				fieldValues.add(field);
			}
		}
	}
	
    public StandardizedRecordImpl(List<Field> fields, Map<String, Object> parameters) {
        this(fields);
        this.parameters = parameters;
    }
    
    public Set<String> getFieldNames() {
	    return Collections.unmodifiableSet(fieldMap.keySet());
	}
	
	private Map<String, List<StandardizedField>> standardizedFieldMap;
	public List<StandardizedField> getFields(String fieldName) {
        List<Field> recordFields = fieldMap.get(fieldName);
	    if (recordFields == null) {
	        return Collections.emptyList();
	    }
	    if (standardizedFieldMap == null) {
	        standardizedFieldMap = new LinkedHashMap<String, List<StandardizedField>>(recordFields.size());
	    }
        List<StandardizedField> standardizedFields = standardizedFieldMap.get(fieldName);
        if (standardizedFields == null) {
            standardizedFields = new ArrayList<StandardizedField>(recordFields.size());
            for (Field recordField: recordFields) {
                StandardizedField standardizedField =
                    new StandardizedField(recordField.getTokens(),
                                          recordField.getInputSymbol().getName(),
                                          recordField.getOutputSymbol().getName());
                standardizedFields.add(standardizedField);
            }
            standardizedFieldMap.put(fieldName, standardizedFields);
        }
	    return standardizedFields;
	}

	private Map<String, String> fieldValueMap;
	public String getFieldValue(String fieldName) {
        List<Field> recordFields = fieldMap.get(fieldName);
        if (recordFields == null || recordFields.size() == 0) {
            return "";
        }
        if (fieldValueMap == null) {
            fieldValueMap = new LinkedHashMap<String, String>(fieldMap.size());
        }
        String fieldValue = fieldValueMap.get(fieldName);
        if (fieldValue == null) {
            List<List<String>> allTokens = new ArrayList<List<String>>(recordFields.size());
            for (Field recordField: recordFields) {
                allTokens.add(recordField.getTokens());
            }
            fieldValue = recordFields.get(0).getOutputSymbol().concatenateOccurrences(allTokens);
            fieldValueMap.put(fieldName, fieldValue);
        }
        return fieldValue;
	}
	
	private Map<String, List<String>> fieldListMap;
    public List<String> getFieldValues(String fieldName) {
        List<Field> recordFields = fieldMap.get(fieldName);
        if (recordFields == null) {
            return Collections.emptyList();
        }
        if (recordFields.size() == 0) {
            return Collections.emptyList();
        }
        if (fieldListMap == null) {
            fieldListMap = new LinkedHashMap<String, List<String>>(fieldMap.size());
        }
        List<String> allTokens = fieldListMap.get(fieldName);
        if (allTokens == null) {
            allTokens = new ArrayList<String>(recordFields.size());
            for (Field recordField: recordFields) {
                allTokens.add(recordFields.get(0).getOutputSymbol().concatenateTokens(recordField.getTokens()));
            }
            fieldListMap.put(fieldName, allTokens);
        }
        return allTokens;
    }

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String fieldName : fieldMap.keySet()) {
			sb.append(fieldName);
			sb.append(": ");

			sb.append(getFieldValue(fieldName));
			sb.append(". ");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 2);
		}
		return sb.toString();
	}

	public String inputFieldPattern() {
	    StringBuilder sb = new StringBuilder();
	    for (String fieldName : fieldMap.keySet()) {
	        List<Field> fields = fieldMap.get(fieldName);
	        for (Field field: fields) {
	            sb.append(field.getInputSymbol());
	            sb.append(' ');
	        }
	    }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 2);
        }
	    return sb.toString();
	}

	public Map<String, Object> getParameters() {
		if (parameters == null) {
		    parameters = new LinkedHashMap<String, Object>();
		}
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
}
