/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.impl.learner;

import static com.sun.mdm.standardizer.impl.InputSymbol.EOF;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ListIterator;
import java.util.Set;

import net.java.hulp.i18n.Logger;

import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.impl.InputSymbol;
import com.sun.mdm.standardizer.impl.State;
import com.sun.mdm.standardizer.impl.StateTransition;

/**
 * This class learns the statistical distribution of the probability that a given
 * input symbols is followed by another one. Collecting these statistics helps in
 * resolving ambiguities when a record matches more than one entire sequence.
 * <br/>
 * It is assumed that training data is collected by applying a first-cut
 * standardization on some training input file. Initially, the configuration of
 * this standardization need not be exact though a good guesstimate simplifies the
 * iterative learning process. The effect of using less-than-ideal
 * probability distributions in the standardizer configuration is that some
 * parsings may be incorrect (i.e. they will result in the wrong sequence of
 * input symbols). It may be necessary to inspect and fix such errors manually prior
 * to running the training process.
 * <br/> 
 * The result of running the training process is an augmented state model where the
 * transition probabilities have been adjusted to reflect their actual statistical
 * distribution.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class ProbabilityLearner {
    /**
     * The logger instance
     */
    /**
     * The globalization localizer instancer
     */
    private static final Logger logger = Logger.getLogger(ProbabilityLearner.class.getName());
	private static final Localizer localizer = Localizer.getInstance();
    
	public void learn(State initialState, Iterator<TrainingSequence> trainingSequenceIterator) {
        Set<State> states = collectStates(initialState);
        
        clearProbabilities(states);

        int recordNumber = 0;
        while (trainingSequenceIterator.hasNext()) {
            recordNumber++;
            TrainingSequence trainingSequence = trainingSequenceIterator.next();

            if (trainingSequence.getCount() > 0) {
                try {
                    learn(initialState, trainingSequence);
                } catch (Exception e) {
                    logger.warn(localizer.x("STD-112: Record #{0}: {1}", recordNumber, e));
                }
            }
        }

        setProbabilities(states);
    }

    private void learn(State state, TrainingSequence trainingSequence) {
        ListIterator<InputSymbol> iterator = trainingSequence.getSequence().listIterator();
        
        while (iterator.hasNext()) {
            InputSymbol inputSymbol = iterator.next();
            
            StateTransition stateTransition = state.accept(inputSymbol);
            if (stateTransition == null) {
                throw new IllegalArgumentException("Unexpected input symbol '" + inputSymbol + "' for state '" + state + "'. Seen at position " + (iterator.previousIndex() + 1) + " of " + trainingSequence.getSequence().size() );
            }
            
            stateTransition.setProbability(trainingSequence.getCount() + stateTransition.getProbability());

            state = stateTransition.getNextState();
        }

        StateTransition stateTransition = state.accept(EOF);
        if (stateTransition != null) {
            stateTransition.setProbability(trainingSequence.getCount() + stateTransition.getProbability());
        }
    }

    private static void setProbabilities(Set<State> states) {
        for (State state: states) {
            double total = 0d;
            for (InputSymbol inputSymbol: state.expectedInputSymbols()) {
                StateTransition stateTransition = state.accept(inputSymbol);
                total += stateTransition.getProbability();
            }
            
            if (total > 0d) {
                for (InputSymbol inputSymbol: state.expectedInputSymbols()) {
                    StateTransition stateTransition = state.accept(inputSymbol);
                    stateTransition.setProbability(stateTransition.getProbability() / total);
                }
            }
        }
    }

    private static void clearProbabilities(Set<State> states) {
        for (State state: states) {
            for (InputSymbol inputSymbol: state.expectedInputSymbols()) {
                StateTransition stateTransition = state.accept(inputSymbol);
                stateTransition.setProbability(0d);
            }
        }
    }

    private static Set<State> collectStates(State initialState) {
        Set<State> states = new LinkedHashSet<State>();
        collectStates(initialState, states);
        return states;
    }
    private static void collectStates(State state, Set<State> visitedStates) {
        if (state != null && !visitedStates.contains(state)) {
            visitedStates.add(state);
            for (InputSymbol inputSymbol: state.expectedInputSymbols()) {
                StateTransition stateTransition = state.accept(inputSymbol);
                collectStates(stateTransition.getNextState(), visitedStates);
            }
        }
    }
}
