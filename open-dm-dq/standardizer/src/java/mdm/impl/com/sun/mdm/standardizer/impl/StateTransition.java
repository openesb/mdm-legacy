package com.sun.mdm.standardizer.impl;

public class StateTransition {
    private State nextState;
    private double probability;
    private OutputSymbol outputSymbol;

    public StateTransition() {
    }

    public StateTransition(State nextState, double probability, OutputSymbol outputSymbol) {
        this.nextState = nextState;
        this.probability = probability;
        this.outputSymbol = outputSymbol;
    }
    
    public String toString() {
        return nextState.getName() + " -> " + outputSymbol + ": " + probability;
    }
    
    public State getNextState() {
        return nextState;
    }

    public void setNextState(State nextState) {
        this.nextState = nextState;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public OutputSymbol getOutputSymbol() {
        return outputSymbol;
    }

    public void setOutputSymbol(OutputSymbol outputSymbol) {
        this.outputSymbol = outputSymbol;
    }
}