/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sun.inti.components.string.match.incremental.IncrementalMatcher;
import com.sun.inti.components.string.transform.StringTransformer;

/**
 * This class represents the input token types recognized during the string parsing phase
 * of standardization.
 * 
 * <code>InputSymbol</code>s can be <em>observed</em> within a given standardization
 * <a href="State.html">state</a>. Observations result in an <a href="OutputSymbol">output field</a>
 * being selected and a state transition being triggered.
 * 
 * The special <code>EOF</code> symbol (actually, a <code>null</code>) is used as an input
 * type instance resulting from observing the end of the standardized string.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class InputSymbol extends Symbol {
    /**
     * The <code>null<code> instance corresponding to the <code>EOF</code> observation.
     */
    public static final InputSymbol EOF = null;

    /**
     * The list of token matchers used to determine whether successive string tokens
     * match or not. 
     */
    private List<TokenMatcher> tokenMatchers;

    /**
     * The empty constructor.
     */
    public InputSymbol() {
    }

    /**
     * Match a list of tokens starting at the given offset.
     * <em>Matching</em> is the process of ascertaining whether a given set of
     * consecutive strings conform to some defined pattern or rule.
     * 
     * @param tokens a list containing the tokens to be matched
     * @param offset the offset at which to start matching
     * @return A <code>Match</code> object encapsulating the result of the match
     *         or <code>null</code> if there was no match
     */
    public Match match(List<String> tokens, int offset) {
        for (TokenMatcher tokenMatcher : tokenMatchers) {
            Match match = tokenMatcher.buildMatch(tokens.subList(offset, tokens.size()).iterator());
            if (match != null) {
                return match;
            }
        }

        return null;
    }

    /**
     * This class embodies the result of matching a string iterator against an
     * incremental rule.
     *
     * @author Ricardo Rocha (ricardo.rocha@sun.com)
     */
    public static class Match {
        /**
         * The number of tokens in the original input string iterator that were
         * successfully matched. Note that this number is not necessarily the same
         * as the size of the <code>tokens</code> field.
         */
        private int length;
        /**
         * The list of tokens matched. Note that the length of this list is not
         * necessarily the same as the number of input tokens that mathed. This
         * is so because post-processing of matched tokens may result in shortening
         * or enlargement of the associated matched token list. 
         */
        private List<String> tokens;
        /**
         * The factor used to adjust priority.
         */
        double factor;
        /**
         * The constructor.
         * 
         * @param length Number of matched tokens in the input string 
         * @param tokens List of tokens matched
         * @param context An arbitrary record containing application-level metadata
         */
        private Match(int length, List<String> tokens, double factor) {
            this.length = length;
            this.tokens = tokens;
            this.factor = factor;
        }

        /**
         * Return the string representation of this match.
         */
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            sb.append(length);
            sb.append(") ");
            for (String token : tokens) {
                sb.append(token);
                sb.append(' ');
            }
            sb.setLength(sb.length() - 1);
            return sb.toString();
        }

        /**
         * Return the length of this match
         * @return the length of this match
         */
        public int getLength() {
            return length;
        }

        /**
         * Return the tokens associated with this match
         * @return the tokens associated with this match
         */
        public List<String> getTokens() {
            return tokens;
        }
        
        /**
         * Return the factor used to adjust priority. 
         */
        public double getFactor() {
            return factor;
        }
    }

    /**
     * This class is responsible for the actual matching of one or more consecutive
     * strings against some pattern or rule. Such patterns or rules are embodied in
     * the <em>incremental matchers</em> contained in this matcher.
     *
     * @author Ricardo Rocha (ricardo.rocha@sun.com)
     */
    public static class TokenMatcher {
        /**
         * The <code>IncrementalMatcher</code> instance actually responsible for matching
         * sequences of consecutive strings.
         */
        private IncrementalMatcher matcher;
        /**
         * The <code>factor</code> used to adjust probability for this matcher. Must be
         * between 0 and 1
         */
        double factor = 1d;
        /**
         * The list of pre-processors (<code>StringTransformer</code>s) applied to the
         * incoming sequence of consecutive strings.
         */
        private List<StringTransformer> preProcessors;
        
        /**
         * The list of post-processors (<code>StringTransformer</code>s) applied to the
         * result of matching.
         */
        private List<StringTransformer> postProcessors;

        /**
         * Build a match given a sequence of consecutive strings represented in a
         * <code>Iterator&lt;String&gt;</code>.
         * 
         * @param iterator The string iterator providing the consecutive tokens to be
         *                 matched
         * @return The <code>Match</code> built for the given iterator or <code>null</code>
         *         if there was no match
         */
        private Match buildMatch(Iterator<String> iterator) {
            return new MatchBuilder(iterator).buildMatch();
        }

        /**
         * Inner class used to wrap a <code>Iterator&lt;String&gt;</code> so that
         * preprocessors and postprocessors can be applied to each successive token.
         *
         * @author Ricardo Rocha (ricardo.rocha@sun.com)
         */
        private class MatchBuilder implements Iterator<String> {
            /**
             * The wrapped string iterator instance
             */
            private Iterator<String> source;

            /**
             * The delegate string iterator instance. This iterator can be
             * dynamically extended as the result of pre-processing.
             */
            private Iterator<String> delegateIterator;
            
            /**
             * The list of matched tokens collected so far.
             */
            private List<List<String>> collectedTokens = new ArrayList<List<String>>();

            /**
             * The constructor.
             * 
             * @param source The input string iterator
             */
            public MatchBuilder(Iterator<String> source) {
                this.source = source;
                advance();
            }

            public boolean hasNext() {
                if (delegateIterator.hasNext()) {
                    return true;
                }

                return advance();
            }

            /**
             * Return the next string
             * @return the next string
             * @see java.util.Iterator#next()
             */
            public String next() {
                return delegateIterator.next();
            }

            /**
             * Remove the current iterator element. This operation is not implemented.
             * 
             * @throws UnsupportedOperationException
             * @see java.util.Iterator#remove()
             */
            public void remove() {
                throw new UnsupportedOperationException("Can't remove");
            }

            /**
             * Advance to the next string token as given by the wrapped iterator
             * and the matcher's string preprocessors.
             * 
             * @return <code>true</code> if there are more tokens to be returned,
             *         <code>false</code> otherwise
             */
            private boolean advance() {
                while (source.hasNext()) {
                    String token = source.next();

                    if (preProcessors != null) {
                        for (StringTransformer preProcessor : preProcessors) {
                            token = preProcessor.transform(token);
                        }
                    }

                    List<String> newTokens = new ArrayList<String>();
                    if (token != null && token.trim().length() > 0) {
                        for (String word : token.trim().split("\\s+")) {
                            newTokens.add(word);
                        }
                    }
                    collectedTokens.add(newTokens);
                    delegateIterator = newTokens.iterator();
                    if (delegateIterator.hasNext()) {
                        return true;
                    }
                }

                return false;
            }

            /**
             * Build a match using the <code>matcher</code> instance and applying the
             * (optional) post-processors.
             * @return A <code>Match</code> composed of the recognized tokens or <code>null</code>
             *         if no such tokens were found
             */
            private Match buildMatch() {
                int matcherCount = matcher.match(this);

                if (matcherCount == 0) {
                    return null;
                }

                int tokenCount = 0;
                List<String> matchedTokens = new ArrayList<String>(matcherCount);
                for (List<String> tokenList : collectedTokens) {
                    tokenCount++;
                    List<String> subList;
                    if (matcherCount > tokenList.size()) {
                        subList = tokenList;
                        matcherCount -= tokenList.size();
                    } else {
                        subList = tokenList.subList(0, matcherCount);
                        matcherCount = 0;
                    }

                    matchedTokens.addAll(subList);

                    if (matcherCount == 0) {
                        break;
                    }
                }

                if (postProcessors != null) {
                    for (int i = 0; i < matchedTokens.size(); i++) {
                        String matchedToken = matchedTokens.get(i);
                        for (StringTransformer postProcessor : postProcessors) {
                            matchedToken = postProcessor.transform(matchedToken);
                        }
                        matchedTokens.set(i, matchedToken);
                    }
                }

                Iterator<String> iterator = matchedTokens.iterator();
                while (iterator.hasNext()) {
                    String token = iterator.next();
                    if (token == null || token.trim().length() == 0) {
                        iterator.remove();
                    }
                }

                if (matchedTokens.size() == 0) {
                    return null;
                }

                return new Match(tokenCount, matchedTokens, factor);
            }
        }

        /**
         * The empty constructor
         */
        public TokenMatcher() {
        }

        /**
         * Return the <code>Matcher</code> instance
         * @return the <code>Matcher</code> instance
         */
        public IncrementalMatcher getMatcher() {
            return matcher;
        }

        /**
         * Set the <code>Matcher</code> instance
         * @param matcher the <code>Matcher</code> instance
         */
        public void setMatcher(IncrementalMatcher matcher) {
            this.matcher = matcher;
        }

        /**
         * Return the list of pre-processors for this match builder
         * @return the list of pre-processors for this match builder
         */
        public List<StringTransformer> getPreProcessors() {
            return preProcessors;
        }

        /**
         * Set the list of pre-processors for this match builder
         * @param preProcessors the list of pre-processors for this match builder
         */
        public void setPreProcessors(List<StringTransformer> preProcessors) {
            this.preProcessors = preProcessors;
        }

        /**
         * Return the list of post-processors for this match builder
         * @return the list of post-processors for this match builder
         */
        public List<StringTransformer> getPostProcessors() {
            return postProcessors;
        }

        /**
         * Set the list of post-processors for this match builder
         * @param postProcessors the list of post-processors for this match builder
         */
        public void setPostProcessors(List<StringTransformer> postProcessors) {
            this.postProcessors = postProcessors;
        }

        /**
         * Return the factor used to adjust priority.
         * @return
         */
        public double getFactor() {
            return factor;
        }

        /**
         * Set the factor used to adjust priority.
         * @param factor
         */
        public void setFactor(double factor) {
            this.factor = factor;
        }
    }

    /**
     * Return the list of token matchers defined for this input type.
     * @return the list of token matchers defined for this input type.
     */
    public List<TokenMatcher> getTokenMatchers() {
        return tokenMatchers;
    }

    /**
     * Set the list of token matchers defined for this input type.
     * @param tokenMatchers the list of token matchers defined for this input type.
     */
    public void setTokenMatchers(List<TokenMatcher> tokenMatchers) {
        this.tokenMatchers = tokenMatchers;
    }
}
