<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl">
                
	<xsl:output method="xml"
				indent="yes"
				doctype-public="-//SPRING//DTD BEAN 2.0//EN"
				doctype-system="http://www.springframework.org/dtd/spring-beans-2.0.dtd"/>

	<xsl:param name="typeName"/>
	<xsl:param name="workingDirectory"/>
	
	<xsl:template match="standardizer">
        <xsl:variable name="extraInformationName">
            <xsl:choose>
                <xsl:when test="@extraInformation">
                    <xsl:value-of select="@extraInformation"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>extraInformation</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="conjunctionName">
            <xsl:choose>
                <xsl:when test="@conjunction">
                    <xsl:value-of select="@conjunction"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>conjunction</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="initialStateName">
            <xsl:choose>
                <xsl:when test="stateModel/@name">
                    <xsl:value-of select="stateModel/@name"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>initialState</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

		<beans>
			<bean name="standardizer" class="com.sun.mdm.standardizer.impl.StandardizerImpl">
				<property name="tokenizer">
					<ref local="tokenizer"/>
				</property>
				
				<property name="recordCleanser">
					<ref local="cleanser"/>
				</property>
				
				<property name="initialState" ref="initialState"/>

                <property name="conjunctionInputSymbol">
                    <ref local="inputSymbol_{$conjunctionName}"/>
                </property>

                <property name="extraInformationInputSymbol">
                    <ref local="inputSymbol_{$extraInformationName}"/>
                </property>

				<property name="extraInformationOutputSymbol">
					<ref local="outputSymbol_{$extraInformationName}"/>
				</property>
			</bean>
			
			<bean name="normalizer" class="com.sun.mdm.standardizer.impl.NormalizerImpl">
				<property name="normalizerMap">
					<map>
						<xsl:for-each select="normalizer/for">
							<entry>
								<key>
									<value>
										<xsl:value-of select="@field"/>
									</value>
								</key>
								<ref local="inputSymbol_{@use}"/>
							</entry>
						</xsl:for-each>
					</map>
				</property>
				<xsl:if test="normalizer/preProcessing">
					<property name="preProcessors">
						<list>
							<xsl:apply-templates select="normalizer/preProcessing/*"/>
						</list>
					</property>
				</xsl:if>
                <property name="tokenizer">
                    <ref local="tokenizer"/>
                </property>
			</bean>
			
			<bean name="tokenizer">
				<xsl:choose>
				    <!--  TODO This form is yet untested -->
					<xsl:when test="tokenizer">
                        <xsl:apply-templates select="@*"/>
                        <xsl:apply-templates select="*"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class">
							<xsl:text>com.sun.inti.components.string.tokenize.RegexTokenizer</xsl:text>
						</xsl:attribute>
						<constructor-arg value="\s+"/>
					</xsl:otherwise>
				</xsl:choose>
			</bean>

			<bean name="initialState,state_{$initialStateName}" class="com.sun.mdm.standardizer.impl.State">
			    <property name="name">
			         <value>
			             <xsl:value-of select="$initialStateName"/>
			         </value>
			    </property>
				<property name="transitions">
					<map>
						<xsl:apply-templates select="stateModel/when"/>
					</map>
				</property>
			</bean>

			<xsl:apply-templates select="stateModel/state"/>

			<xsl:apply-templates select="inputSymbols">
                <xsl:with-param name="extraInformationName" select="$extraInformationName"/>
			</xsl:apply-templates>

			<xsl:apply-templates select="outputSymbols">
				<xsl:with-param name="extraInformationName" select="$extraInformationName"/>
			</xsl:apply-templates>

			<bean name="cleanser" class="com.sun.inti.components.string.transform.CompoundStringTransformer">
				<constructor-arg>
					<list>
						<xsl:apply-templates select="cleanser/*"/>
					</list>
				</constructor-arg>
			</bean>
			
			<bean name="inputSymbols" class="org.springframework.beans.factory.config.MapFactoryBean">
			     <property name="sourceMap">
			         <map>
                         <entry key="{$extraInformationName}">
                             <ref local="inputSymbol_{$extraInformationName}"/>
                         </entry>
			             <xsl:for-each select="inputSymbols/inputSymbol">
			                 <entry key="{@name}">
			                     <ref local="inputSymbol_{@name}"/>
			                 </entry>
			             </xsl:for-each>
			         </map>
			     </property>
			</bean>

			<bean id="customEditorConfigurer" class="org.springframework.beans.factory.config.CustomEditorConfigurer">
			  <property name="customEditors">
			    <map>
			      <entry key="java.util.Date">
			        <bean class="org.springframework.beans.propertyeditors.CustomDateEditor">
			        	<constructor-arg>
			        		<bean class="java.text.SimpleDateFormat">
			        			<constructor-arg>
			        				<value>MM/dd/yyyy</value>
			        			</constructor-arg>
			        		</bean>
			        	</constructor-arg>
			        	<constructor-arg>
			        		<value>false</value>
			        	</constructor-arg>
			        </bean>
			      </entry>
			    </map>
			  </property>
			</bean>

		</beans>
	</xsl:template>

	<xsl:template match="outputSymbols">
		<xsl:param name="extraInformationName"/>
		<xsl:if test="not(outputSymbol[@name = $extraInformationName])">
			<bean name="outputSymbol_{$extraInformationName}" class="com.sun.mdm.standardizer.impl.OutputSymbol">
				<property name="name" value="{$extraInformationName}"/>
			</bean>
		</xsl:if>
		<xsl:apply-templates select="outputSymbol"/>
	</xsl:template>

	<xsl:template match="state">
		<bean name="state_{@name}" class="com.sun.mdm.standardizer.impl.State">
			<property name="name" value="{@name}"/>
			<property name="transitions">
				<map>
					<xsl:apply-templates select="eof"/>
					<xsl:apply-templates select="when"/>
				</map>
			</property>
		</bean>
	</xsl:template>

	<xsl:template match="eof">
		<entry>
			<key>
				<null/>
			</key>
			<bean class="com.sun.mdm.standardizer.impl.StateTransition">
				<property name="probability" value="{@probability}"/>
			</bean>
		</entry>
	</xsl:template>

	<xsl:template match="when">
		<entry>
			<key>
				<ref local="inputSymbol_{@inputSymbol}"/>
			</key>
			<bean class="com.sun.mdm.standardizer.impl.StateTransition">
                <property name="nextState" ref="state_{@nextState}"/>
				<property name="probability" value="{@probability}"/>
                <xsl:if test="@outputSymbol">
                    <property name="outputSymbol" ref="outputSymbol_{@outputSymbol}"/>
                </xsl:if>
			</bean>
		</entry>
    </xsl:template>

    <xsl:template match="inputSymbols">
        <xsl:param name="extraInformationName"/>
        <xsl:if test="not(inputSymbol[@name = $extraInformationName])">
            <bean name="inputSymbol_{$extraInformationName}" class="com.sun.mdm.standardizer.impl.InputSymbol">
                <property name="name" value="{$extraInformationName}"/>
            </bean>
        </xsl:if>
        <xsl:apply-templates select="inputSymbol"/>
    </xsl:template>

    <xsl:template match="inputSymbol">
		<bean name="inputSymbol_{@name}" class="com.sun.mdm.standardizer.impl.InputSymbol">
			<property name="name" value="{@name}"/>
			<property name="tokenMatchers">
				<list>
					<xsl:apply-templates select="matchers/matcher"/>
				</list>
			</property>
		</bean>
	</xsl:template>

	<xsl:template match="matcher">
		<bean class="com.sun.mdm.standardizer.impl.InputSymbol$TokenMatcher">
			<property name="matcher">
				<xsl:apply-templates select="*[name() != 'preProcessing' and name () != 'property' and name() != 'postProcessing']"/>
			</property>
			<xsl:if test="@factor">
				<property name="factor" value="{@factor}"/>
			</xsl:if>
			<xsl:if test="preProcessing">
				<property name="preProcessors">
					<list>
						<xsl:apply-templates select="preProcessing/*"/>
					</list>
				</property>
			</xsl:if>
			<xsl:if test="postProcessing">
				<property name="postProcessors">
					<list>
						<xsl:apply-templates select="postProcessing/*"/>
					</list>
				</property>
			</xsl:if>
		</bean>
	</xsl:template>

	<xsl:template match="matcher/lexicon">
		<bean class="com.sun.inti.components.string.match.incremental.LexiconIncrementalMatcher">
			<constructor-arg>
				<xsl:choose>
					<xsl:when test="@resource">
						<bean class="com.sun.inti.components.beans.ResourceLoaderFactoryBean">
							<property name="resourceName" value="{@resource}"/>
						</bean>
					</xsl:when>
					<xsl:when test="@file">
						<bean class="java.io.File">
							<constructor-arg>
								<bean class="java.io.File">
									<constructor-arg>
										<value type="java.lang.String">
											<xsl:value-of select="$workingDirectory"/>
										</value>
									</constructor-arg>
								</bean>
							</constructor-arg>
							<constructor-arg>
								<value type="java.lang.String">
									<xsl:value-of select="@file"/>
								</value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@url">
						<bean class="java.net.URL">
							<constructor-arg>
								<value type="java.lang.String">
									<xsl:value-of select="@url"/>
								</value>
							</constructor-arg>
						</bean>
					</xsl:when>
				</xsl:choose>
			</constructor-arg>
			
            <constructor-arg>
                <xsl:choose>
                    <xsl:when test="@cleanse = 'yes'">
                        <ref local="cleanser"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <null/>
                    </xsl:otherwise>
                </xsl:choose>
            </constructor-arg>
			
			<constructor-arg>
				<value>
					<xsl:choose>
						<xsl:when test="@separator">
							<xsl:value-of select="@separator"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>\s+</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</value>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="matcher/matchAnyPattern">
		<bean class="com.sun.inti.components.string.match.incremental.OrRegexIncrementalMatcher">
			<constructor-arg>
				<list>
					<xsl:for-each select="pattern">
						<bean class="com.sun.inti.components.string.match.incremental.AbstractRegexIncrementalMatcher$RegexMatcher">
							<constructor-arg>
								<value>
									<xsl:value-of select="@regex"/>
								</value>
							</constructor-arg>
							<xsl:if test="exceptFor">
								<constructor-arg>
									<list>
										<xsl:for-each select="exceptFor">
											<value type="java.util.regex.Pattern">
												<xsl:value-of select="@regex"/>
											</value>
										</xsl:for-each>
									</list>
								</constructor-arg>
							</xsl:if>
						</bean>
					</xsl:for-each>
				</list>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="matcher/pattern">
		<bean class="com.sun.inti.components.string.match.incremental.OrRegexIncrementalMatcher">
			<constructor-arg>
				<list>
					<bean class="com.sun.inti.components.string.match.incremental.AbstractRegexIncrementalMatcher$RegexMatcher">
						<constructor-arg>
							<value>
								<xsl:value-of select="@regex"/>
							</value>
						</constructor-arg>
						<xsl:if test="exceptFor">
							<constructor-arg>
								<list>
									<xsl:for-each select="exceptFor">
										<value type="java.util.regex.Pattern">
											<xsl:value-of select="@regex"/>
										</value>
									</xsl:for-each>
								</list>
							</constructor-arg>
						</xsl:if>
					</bean>
				</list>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="matcher/matchAllPatterns">
		<bean class="com.sun.inti.components.string.match.incremental.AndRegexIncrementalMatcher">
			<constructor-arg>
				<list>
					<xsl:for-each select="pattern">
						<bean class="com.sun.inti.components.string.match.incremental.AbstractRegexIncrementalMatcher$RegexMatcher">
							<constructor-arg>
								<value>
									<xsl:value-of select="@regex"/>
								</value>
							</constructor-arg>
							<xsl:if test="exceptFor">
								<constructor-arg>
									<list>
										<xsl:for-each select="exceptFor">
											<value type="java.util.regex.Pattern">
												<xsl:value-of select="@regex"/>
											</value>
										</xsl:for-each>
									</list>
								</constructor-arg>
							</xsl:if>
						</bean>
					</xsl:for-each>
				</list>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="matcher/fixedStrings">
		<bean class="com.sun.inti.components.string.match.incremental.FixedStringIncrementalMatcher">
			<constructor-arg>
				<list>
					<xsl:for-each select="fixedString">
						<value>
							<xsl:value-of select="string(.)"/>
						</value>
					</xsl:for-each>
				</list>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="matcher/fixedString">
		<bean class="com.sun.inti.components.string.match.incremental.FixedStringIncrementalMatcher">
			<constructor-arg>
				<list>
					<value>
						<xsl:value-of select="string(.)"/>
					</value>
				</list>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="dictionary">
		<bean class="com.sun.inti.components.string.transform.MapBasedStringTransformer">
			<constructor-arg>
				<xsl:choose>
					<xsl:when test="@resource">
						<bean class="com.sun.inti.components.beans.ResourceLoaderFactoryBean">
							<property name="resourceName" value="{@resource}"/>
						</bean>
					</xsl:when>
					<xsl:when test="@file">
						<bean class="java.io.File">
							<constructor-arg>
								<bean class="java.io.File">
									<constructor-arg>
										<value type="java.lang.String">
											<xsl:value-of select="$workingDirectory"/>
										</value>
									</constructor-arg>
								</bean>
							</constructor-arg>
							<constructor-arg>
								<value type="java.lang.String">
									<xsl:value-of select="@file"/>
								</value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@url">
						<bean class="java.net.URL">
							<constructor-arg>
								<value type="java.lang.String">
									<xsl:value-of select="@url"/>
								</value>
							</constructor-arg>
						</bean>
					</xsl:when>
				</xsl:choose>
			</constructor-arg>
            
            <constructor-arg>
                <xsl:choose>
                    <xsl:when test="@cleanse = 'yes'">
                        <ref local="cleanser"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <null/>
                    </xsl:otherwise>
                </xsl:choose>
            </constructor-arg>
            
			<constructor-arg>
				<value>
					<xsl:choose>
						<xsl:when test="@separator">
							<xsl:value-of select="@separator"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>\t</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</value>
			</constructor-arg>
		</bean>
	</xsl:template>

	<xsl:template match="outputSymbol">
		<xsl:param name="extraInformation"/>
		<bean name="outputSymbol_{@name}" class="com.sun.mdm.standardizer.impl.OutputSymbol">
			<xsl:variable name="name">
				<xsl:choose>
					<xsl:when test="@name = $extraInformation">
						<xsl:value-of select="concat(concat('outputSymbol_', @name), ',extraInformation')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat('outputSymbol_', @name)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="name">
				<xsl:value-of select="$name"/>
			</xsl:attribute>
			<property name="name" value="{@name}"/>
			<xsl:apply-templates/>
		</bean>
	</xsl:template>

	<xsl:template match="tokenConcatenator">
		<property name="tokenConcatenator">
			<bean>
                <xsl:for-each select="@*">
                    <xsl:attribute name="{name(.)}">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:for-each>
                <xsl:apply-templates/>
			</bean>
		</property>
	</xsl:template>

	<xsl:template match="occurrenceConcatenator">
		<property name="occurrenceConcatenator">
			<bean>
                <xsl:for-each select="@*">
                    <xsl:attribute name="{name(.)}">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                </xsl:for-each>
                <xsl:apply-templates/>
			</bean>
		</property>
	</xsl:template>

	<xsl:template match="normalizeSpace">
		<bean class="com.sun.inti.components.string.transform.TrimStringTransformer"/>
	</xsl:template>

	<xsl:template match="uppercase">
		<bean class="com.sun.inti.components.string.transform.UpperCaseStringTransformer"/>
	</xsl:template>

	<xsl:template match="replace">
    	<bean class="com.sun.inti.components.string.transform.SimpleRegexStringTransformer">
    		<constructor-arg value="{@regex}"/>
    		<constructor-arg value="{@replacement}"/>
    		<constructor-arg value="false"/>
    	</bean>
	</xsl:template>

	<xsl:template match="replaceAll">
    	<bean class="com.sun.inti.components.string.transform.SimpleRegexStringTransformer">
    		<constructor-arg value="{@regex}"/>
    		<constructor-arg value="{@replacement}"/>
    		<constructor-arg value="true"/>
    	</bean>
	</xsl:template>

	<xsl:template match="transliterate">
    	<bean class="com.sun.inti.components.string.transform.TransliterationStringTransformer">
    		<constructor-arg value="{@from}"/>
    		<constructor-arg value="{@to}"/>
    	</bean>
	</xsl:template>

	<xsl:template match="@*|node()"  priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
