package com.sun.mdm.standardizer.impl;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class State {
	private String name;
	private Map<InputSymbol, StateTransition> transitions;

	public State() {}
	
	public State(String name, Map<InputSymbol, StateTransition> transitions) {
		this.name = name;
		this.transitions = transitions;
	}

	public Set<InputSymbol> expectedInputSymbols() {
        return transitions.keySet();
	}
	
	public StateTransition accept(InputSymbol inputSymbol) {
		return transitions.get(inputSymbol);
	}
	
	public String toString() {
	    return name;
	}
	
	public String toXMLString() {
	    StringBuffer sb = new StringBuffer();
	    
	    sb.append("<stateModel name='" + name + "'>\n");
	    
        for (InputSymbol inputSymbol: expectedInputSymbols()) {
            StateTransition stateTransition = accept(inputSymbol);
            sb.append(stateTransitionXMLString(inputSymbol, stateTransition, 1));
        }
        
        Set<State> states = new LinkedHashSet<State>();
        for (InputSymbol inputSymbol: expectedInputSymbols()) {
            StateTransition stateTransition = accept(inputSymbol);
            sb.append(stateXMLString(stateTransition.getNextState(), states));
        }
        
	    sb.append("</stateModel>\n");
	    return sb.toString();
	}
	private String stateTransitionXMLString(InputSymbol inputSymbol, StateTransition stateTransition, int level) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < level; i++) {
            sb.append("  ");
        }
        if (inputSymbol == null) {
            sb.append("<eof probability='" + stateTransition.getProbability() + "'/>");
        } else {
            sb.append("<when inputSymbol='" + inputSymbol.getName() + "'");
            if (stateTransition.getOutputSymbol() != null) {
                sb.append(" outputSymbol='" + stateTransition.getOutputSymbol() + "'");
            }
            sb.append(" probability='" + stateTransition.getProbability() + "'/>");
        }
        sb.append('\n');
        return sb.toString();
	}
    private String stateXMLString(State currentState, Set<State> states) {
        StringBuffer sb = new StringBuffer();
        if (currentState != null && !states.contains(currentState)) {
            states.add(currentState);
            sb.append("\n  <state name='" + currentState.getName() + "'>\n");
            for (InputSymbol inputSymbol: currentState.expectedInputSymbols()) {
                StateTransition stateTransition = currentState.accept(inputSymbol);
                sb.append(stateTransitionXMLString(inputSymbol, stateTransition, 2));
            }
            sb.append("  </state>\n");
            for (InputSymbol inputSymbol: currentState.expectedInputSymbols()) {
                StateTransition stateTransition = currentState.accept(inputSymbol);
                sb.append(stateXMLString(stateTransition.getNextState(), states));
            }
        }
        return sb.toString();
    }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
    public Map<InputSymbol, StateTransition> getTransitions() { return transitions; }
	public void setTransitions(Map<InputSymbol, StateTransition> transitions) { this.transitions = transitions; }
}
