/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.impl;

import java.util.List;

import com.sun.inti.components.string.concatenate.SeparatorBasedStringConcatenator;

/**
 * This class embodies a standardization output field. An <i>output field</i> is composed
 * of:
 * 
 * <ul>
 *  <li>
 *      A <code>List</code> of string tokens making up the field. This tokens may not be
 *      necessarily contiguous in the original standardized string.
 *  </li>
 *  <li>
 *      An <code>InputSymbol</code> corresponding to the input field under which the tokens
 *      were originally matched in the original standardized string.
 *  </li>
 *  <li>
 *      An <code>OutputSymbol</code> corresponding to the output field assigned to the tokens
 *      as they appeared in the original standardized string.
 *  </li>
 *  <li>
 *      A <code>Record</code> context containing arbitrary application-provided metadata that can be
 *      associated with each <code>InputField</code>.
 *  </li>
 *  <li>
 *      A <code>probability</code> that reflects the probability with which the input field
 *      appeared in the original standardization string.
 *  </li>
 * </li>
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class Field {
    /**
     *      A <code>List</code> of string tokens making up the field. This tokens may not be
     *      necessarily contiguous in the original standardized string.
     */
    private List<String> tokens;
    
    /**
     *      An <code>InputSymbol</code> corresponding to the input field under which the tokens
     *      were originally matched in the original standardized string.
     */
    private InputSymbol inputSymbol;
    
    /**
     *      An <code>OutputSymbol</code> corresponding to the output field assigned to the tokens
     *      as they appeared in the original standardized string.
     */
    private OutputSymbol outputSymbol;
    
    /**
     *      A <code>probability</code> that reflects the probability with which the input field
     *      appeared in the original standardization string.
     */
    private double probability;

    /**
     * The empty constructor.
     */
    public Field() {
    }

    /**
     * The <code>EOF</code> field constructor. This constructor is used when <code>EOF</code> is
     * observed. Since <code>EOF</code> has no associated input or output fields only its
     * probability is used.
     *  
     * @param priority The priority originally associated with the <code>EOF</code> observation
     */
    public Field(double priority) {
        this.probability = priority;
    }

    /**
     * The complete <code>Field</code> constructor.
     * 
     * @param tokens The list of tokens making up the field.
     * @param inputSymbol The input type identified for the field
     * @param outputSymbol The output type assigned to the field
     * @param probability The probability associated with the observation that originated this field
     * @param context The application-defined metadata associated with this [input] field
     */
    public Field(List<String> tokens, InputSymbol inputSymbol, OutputSymbol outputSymbol, double probability) {
        this.tokens = tokens;
        this.inputSymbol = inputSymbol;
        this.outputSymbol = outputSymbol;
        this.probability = probability;
    }

    /**
     * Return the concatenation of tokens of this field as dictated by its output field type.
     * 
     * @return The concatenated token string or <code>null</code> if no tokens exist (as is the
     * case when this field corresponds to an <code>EOF</code> observation)
     */
    public String getTokenString() {
        if (tokens == null) {
            return null;
        }
        if (outputSymbol != null) {
            return outputSymbol.concatenateTokens(tokens);
        }
        return new SeparatorBasedStringConcatenator(" ").concatenate(tokens);
    }

    /**
     * Determine if this field is equal to the given object.
     * Note that the field's input type does not participate in equality
     * comparison. 
     * 
     * @return <code>true</code> is the given object is also a Field and has
     *         the same tokens, output type and probability
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Field)) {
            return false;
        }
        Field that = (Field) o;
        if (this.probability != that.probability) {
            return false;
        }
        if (this.tokens == null && this.tokens != null) {
            return false;
        }
        if (this.tokens != null && this.tokens == null) {
            return false;
        }
        if (this.tokens != null && !this.tokens.equals(that.tokens)) {
            return false;
        }
        if (this.outputSymbol == null && that.outputSymbol != null) {
            return false;
        }
        if (this.outputSymbol != null && that.outputSymbol == null) {
            return false;
        }
        if (this.outputSymbol != null && !this.outputSymbol.equals(that.outputSymbol)) {
            return false;
        }
        return true;
    }

    /**
     * Return the string representation of this field.
     * Note that the field's input type is not included in this representation.
     * 
     * @return The string "<code>EOF</code>" followed by the field's probability
     *         if this field corresponds to an <code>EOF</code> observation or
     *         the concatenation of the output type name and concatenation
     *         followed by the probability.
     */
    public String toString() {
        if (tokens == null) {
            return "EOF (" + probability + ")";
        } else {
            StringBuilder sb = new StringBuilder();
            if (outputSymbol != null) {
                sb.append(outputSymbol.getName());
                sb.append(": '");
                sb.append(outputSymbol.concatenateTokens(tokens));
                sb.append("' (");
                sb.append(probability);
                sb.append(")");
            } else {
                sb.append(tokens);
            }

            return sb.toString();
        }
    }

    /**
     * Return the list of tokens making up this field
     * @return The list of tokens making up this field
     */
    public List<String> getTokens() {
        return tokens;
    }

    /**
     * Set the list of tokens making up this field
     * @param tokens the list of tokens making up this field
     */
    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    /**
     * Return the input type associated with this field
     * @return the input type associated with this field
     */
    public InputSymbol getInputSymbol() {
        return inputSymbol;
    }

    /**
     * Set the input type associated with this field
     * @param inputSymbol the input type associated with this field
     */
    public void setInputSymbol(InputSymbol inputSymbol) {
        this.inputSymbol = inputSymbol;
    }

    /**
     * Return the output type associated with this field
     * @return the output type associated with this field
     */
    public OutputSymbol getOutputSymbol() {
        return outputSymbol;
    }

    /**
     * Set the output type associated with this field
     * @param outputSymbol the output type associated with this field
     */
    public void setOutputSymbol(OutputSymbol outputSymbol) {
        this.outputSymbol = outputSymbol;
    }

    /**
     * Return the occurrence probability associated with this field
     * @return the occurrence probability associated with this field
     */
    public double getProbability() {
        return probability;
    }

    /**
     * Set the occurrence probability associated with this field
     * @param probability the occurrence probability associated with this field
     */
    public void setProbability(double probability) {
        this.probability = probability;
    }
}
