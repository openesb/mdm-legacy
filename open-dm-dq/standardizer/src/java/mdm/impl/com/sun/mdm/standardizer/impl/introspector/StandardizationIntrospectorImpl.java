package com.sun.mdm.standardizer.impl.introspector;

import java.io.File;
import java.util.Iterator;
import java.util.zip.ZipFile;

import net.java.hulp.i18n.Logger;

import com.sun.inti.components.util.ClassUtils;
import com.sun.inti.container.ContainerDescriptor;
import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ContainerIntrospector;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;
import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.introspector.DataTypeDescriptor;
import com.sun.mdm.standardizer.introspector.Descriptor;
import com.sun.mdm.standardizer.introspector.IntrospectionException;
import com.sun.mdm.standardizer.introspector.StandardizationIntrospector;
import com.sun.mdm.standardizer.introspector.VariantDescriptor;

public class StandardizationIntrospectorImpl implements StandardizationIntrospector {
    private static final Logger logger = Logger.getLogger(StandardizationIntrospectorImpl.class.getName());
	private static final Localizer localizer = Localizer.getInstance();
    
    private ContainerIntrospector containerIntrospector;
    
    public StandardizationIntrospectorImpl() throws IntrospectionException {
        try {
            Iterator<ContainerIntrospector> iterator = ClassUtils.loadServiceClass(ContainerIntrospector.class);
            if (!iterator.hasNext()) {
                throw new IntrospectionException("No suitable container introspector implementation");
            }
            containerIntrospector = iterator.next();
        } catch (IntrospectionException ie) { 
            throw ie;
        } catch (Exception e) {
            logger.warn(localizer.x("STD-102: Error initializing standardization introspector: {0}", e));
            throw new IntrospectionException("Error initializing standardization introspector: " + e, e);
        }
    }
    
    public void setRepositoryDirectory(File repositoryDirectory) throws IntrospectionException {
        try {
            containerIntrospector.setRepository(repositoryDirectory);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-103: Container error: {0}", e));
            throw new IntrospectionException("Container error: " + e, e);
        }
    }

    public Descriptor deploy(ZipFile deploymentFile)  throws IntrospectionException {
        try {
            ContainerDescriptor descriptor = containerIntrospector.importFile(deploymentFile);
            if (descriptor instanceof ServiceTypeDescriptor) {
                return new DataTypeDescriptorImpl((ServiceTypeDescriptor) descriptor);
            }
            if (descriptor instanceof ServiceInstanceDescriptor) {
                return new VariantDescriptorImpl((ServiceInstanceDescriptor) descriptor);
            }
            logger.severe(localizer.x("STD-104: Shouldn't happen: returned descriptor is neither a service type nor a service instance!"));
            throw new IntrospectionException("Shouldn't happen: returned descriptor is neither a service type nor a service instance!");
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-105: Error deploying data type file '" + deploymentFile.getName() + "': " + e));
            throw new IntrospectionException("Error deploying data type file '" + deploymentFile.getName() + "': " + e, e);
        }
    }
    
    public DataTypeDescriptor deployDataType(ZipFile deploymentFile) throws IntrospectionException {
        try {
            ServiceTypeDescriptor descriptor = containerIntrospector.importServiceType(deploymentFile);
            return new DataTypeDescriptorImpl(descriptor);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-106: Error deploying data type file '{0}': {e}", deploymentFile.getName(), e));
            throw new IntrospectionException("Error deploying data type file '" + deploymentFile.getName() + "': " + e, e);
        }
    }

    public VariantDescriptor deployVariant(String dataTypeName, ZipFile deploymentFile) {
        try {
            ServiceInstanceDescriptor descriptor = containerIntrospector.importServiceInstance(dataTypeName, deploymentFile);
            return new VariantDescriptorImpl(descriptor);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-107: Error deploying variant file '{0}: {1}", deploymentFile.getName(), e));
            throw new IntrospectionException("Error deploying variant file '" + deploymentFile.getName() + "': " + e, e);
        }
    }

    public DataTypeDescriptor[] getDataTypes() {
        ServiceTypeDescriptor[] serviceTypeDescriptors = containerIntrospector.getServiceTypes();
        DataTypeDescriptor[] dataTypeDescriptors = new DataTypeDescriptor[serviceTypeDescriptors.length];
        for (int i = 0; i < serviceTypeDescriptors.length; i++) {
            dataTypeDescriptors[i] = new DataTypeDescriptorImpl(serviceTypeDescriptors[i]);
        }
        return dataTypeDescriptors;
    }
    
    public DataTypeDescriptor getDataType(String dataTypeName) throws IntrospectionException {
        try {
            return new DataTypeDescriptorImpl(containerIntrospector.getServiceType(dataTypeName));
        } catch (ContainerException ce) {
            throw new IntrospectionException("No such data type: " + dataTypeName);
        }
    }
    
    public void undeploy(String dataTypeName, String variantName) throws IntrospectionException {
        try {
            containerIntrospector.removeInstance(dataTypeName, variantName);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-108: Error deploying variant '{0}' of data type '{1}': {2}", variantName, dataTypeName, e));
            throw new IntrospectionException("Error deploying variant '" + variantName + "' of data type '" + dataTypeName + "': " + e, e);
        }
    }

    public void undeploy(String dataTypeName) throws IntrospectionException {
        try {
            containerIntrospector.removeType(dataTypeName);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-109: Error deploying data type '{0}': {1}", dataTypeName, e));
            throw new IntrospectionException("Error deploying data type '" + dataTypeName + "': " + e, e);
        }
    }
    
    public void takeSnapshot(File zipDestination) throws ContainerException {
        try {
            containerIntrospector.takeSnapshot(zipDestination);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-110: Error taking snapshot: {0}", e));
            throw new IntrospectionException("Error taking snapshot : " + e, e);
        }
    }
}
