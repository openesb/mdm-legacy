/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sun.inti.components.string.tokenize.Tokenizer;
import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.standardizer.Normalizer;
import com.sun.mdm.standardizer.impl.InputSymbol.Match;

/**
 * This class implements the <code>Normalizer</code> interface by means of a map of input
 * types keyed by field name.
 * 
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class NormalizerImpl implements Normalizer {
    /**
     * The string preprocessors to be applied to field values prior to their matching
     * by the corresponding input symbol.
     */
    private StringTransformer[] preProcessors;
    
    /**
     * The string tokenizer to used to split field values prior to matching.
     */
    private Tokenizer tokenizer;
    
    /**
     * The map of input types keyed by field name.
     */
    private Map<String, InputSymbol> normalizerMap;

    /**
     * Normalize the input <code>fieldValue</code> as dictated by its
     * <code>fieldName</code>. In absence of a matching input symbol this
     * method returns the <code>fieldValue</code> unchanged.
     */
    public String normalize(String fieldName, String fieldValue) {
        if (fieldValue == null) {
            return null;
        }
        if (normalizerMap != null) {
            if (preProcessors != null) {
                for (StringTransformer preProcessor : preProcessors) {
                    fieldValue = preProcessor.transform(fieldValue);
                }
            }
            InputSymbol inputSymbol = normalizerMap.get(fieldName);
            if (inputSymbol != null) {
                Match match;
                int offset = 0;
                List<String> normalizedTokens = new ArrayList<String>();
                List<String> fieldValues = tokenizer.tokenize(fieldValue);
                while (offset < fieldValues.size() && (match = inputSymbol.match(fieldValues, offset)) != null) {
                    offset += match.getLength();
                    normalizedTokens.addAll(match.getTokens());
                }
                StringBuilder sb = new StringBuilder();
                for (String matchedToken: normalizedTokens) {
                    sb.append(matchedToken);
                    sb.append(' ');
                }
                if (sb.length() > 0) {
                    sb.setLength(sb.length() - 1);
                    return sb.toString();
                }
                return fieldValue;
            }
        }
        return fieldValue;
    }

    /**
     * Return the map of input types keyed by field name
     * @return the map of input types keyed by field name
     */
    public Map<String, InputSymbol> getNormalizerMap() {
        return normalizerMap;
    }
    /**
     * Set the map of input types keyed by field name
     * @param normalizerMap the map of input types keyed by field name
     */

    public void setNormalizerMap(Map<String, InputSymbol> normalizerMap) {
        this.normalizerMap = normalizerMap;
    }

    /**
     * Return the array of string pre-processors
     * @return the array of string pre-processors
     */
    public StringTransformer[] getPreProcessors() {
        return preProcessors;
    }

    /**
     * Set the array of string pre-processors
     * @param preProcessors the array of string pre-processors
     */
    public void setPreProcessors(StringTransformer[] preProcessors) {
        this.preProcessors = preProcessors;
    }

    public Tokenizer getTokenizer() {
        return tokenizer;
    }

    public void setTokenizer(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }
}
