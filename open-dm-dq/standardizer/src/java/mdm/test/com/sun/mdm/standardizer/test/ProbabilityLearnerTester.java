package com.sun.mdm.standardizer.test;

import static java.util.logging.Level.FINE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.sun.inti.components.component.BeanComponentManagerFactory;
import com.sun.inti.components.component.ComponentManager;
import com.sun.inti.components.io.OutputStreamSource;
import com.sun.inti.components.url.FileURLSource;
import com.sun.mdm.standardizer.impl.InputSymbol;
import com.sun.mdm.standardizer.impl.State;
import com.sun.mdm.standardizer.impl.learner.ProbabilityLearner;
import com.sun.mdm.standardizer.impl.learner.TrainingSequence;
import com.sun.mdm.standardizer.impl.learner.reader.DelimitedTrainingSequenceLineReader;
import com.sun.mdm.standardizer.impl.learner.reader.LineReaderTrainingSequenceIterator;
import com.sun.mdm.standardizer.impl.learner.reader.LineReaderTrainingSequenceIterator.TrainingSequenceLineReader;

public class ProbabilityLearnerTester {
    private static final Logger logger = Logger.getLogger(StandardizerTester.class.getName());
    
    private static class SimpleTrainingSequenceLineReader extends DelimitedTrainingSequenceLineReader {
        private SimpleTrainingSequenceLineReader(Map<String, InputSymbol> inputSymbols, Pattern separator) {
            super(inputSymbols, separator);
        }

        protected TrainingSequence fromLine(String[] fields) {
            if (fields.length < 1) {
                throw new IllegalArgumentException("Insufficient number of fields: " + fields.length);
            }
            
            int count = 1;
            if (fields.length > 1) {
                count = Integer.parseInt(fields[1]);
            }
            
            List<InputSymbol> inputSymbolList = new ArrayList<InputSymbol>();
            for (String inputSymbolName: fields[0].split("\\s+")) {
                InputSymbol inputSymbol = super.getInputSymbols().get(inputSymbolName);
                inputSymbolList.add(inputSymbol);
            }

            return new TrainingSequence(inputSymbolList, count);
        }
    }

    public static void main(final String[] args) throws Exception {
        logger.setLevel(FINE);
        
        final String basename = "person-name";
        final File directory = new File("data/test/personname");
        final ClassLoader classLoader = StandardizerTester.class.getClassLoader();
        BeanComponentManagerFactory factory = new BeanComponentManagerFactory();
        factory.setUrlSource(new FileURLSource(new File(directory, "standardizer.xml")));
        factory.setStylesheetURL(classLoader.getResource("com/sun/mdm/standardizer/impl/standardizer.xsl"));
        factory.setOutputStreamSource(new OutputStreamSource() {
            public OutputStream getOutputStream() throws IOException {
                return new FileOutputStream(new File(directory, basename + ".xml"));
            }
        });
        ComponentManager componentManager = factory.newInstance(classLoader);
        State initialState = (State) componentManager.getComponent("initialState");
        @SuppressWarnings("unchecked")
        Map<String, InputSymbol> inputSymbols = (Map<String, InputSymbol>) componentManager.getComponent("inputSymbols");
        InputStream is = new FileInputStream(new File(directory, basename + "-training.txt"));
        TrainingSequenceLineReader reader = new SimpleTrainingSequenceLineReader(inputSymbols, Pattern.compile("\\|"));
        Iterator<TrainingSequence> iterator = new LineReaderTrainingSequenceIterator(is, reader);
        ProbabilityLearner probabilityLearner = new ProbabilityLearner();
        probabilityLearner.learn(initialState, iterator);

        File outputFile = new File(directory, basename + "-states.xml");
        PrintWriter out = new PrintWriter(new FileWriter(outputFile), true);
        String initialStateString = initialState.toXMLString();
        out.println(initialStateString);
        // System.out.println(initialStateString);
    }
}
