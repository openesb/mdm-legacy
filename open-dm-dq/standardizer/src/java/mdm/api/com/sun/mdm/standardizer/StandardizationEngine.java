/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer;

import java.util.List;

/**
 * <p>The <code>StandardizationEngine</code> interface exposes services related to string
 * standardization and normalization as well as life-cycle events used to activate and
 * deactivate it.</p>
 * 
 * <p><i>Standardization</i> refers to the process of parsing a textual representation of
 * a structured object so as to identify, extract and normalize its components.</p>
 * 
 * <p><i>Normalization</i> refers to the process of transforming a string, as specified by
 * its field name, so that it conforms to some canonical form.</p>
 * 
 * <p>Both standardization and normalization are specialized by data type and variant</p>
 * 
 * <p>A <i>data type</i> specifies the kind of content of a string such as <code>Address</code>,
 * <code>PersonName</code> or <code>BusinessName</code>.</p>
 * 
 * <p>A <i>variant</i> is a specialization of a data type (e.g. a locale-based instance like
 * <code>US</code> or <code>UK</code>). It is customary for data types to which the notion of
 * variant does not apply to have a single variant named <code>Generic</code>.</p>
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface StandardizationEngine {
    /**
     * Initialize this engine to allocate resources needed by standardization and
     * normalization services.
     *  
     * @throws StandardizationException If a resource allocation error occurs during engine
     *                                  initialization
     */
	public void initialize() throws StandardizationException;
	
	/**
	 * Standardize a string value in accordance to its <code>dataType</code> and <code>variant</code>.
	 * 
	 * @param dataTypeName The name of the data type defining the structure of the input string's content
	 * @param variantName The name of the variant within the data type
	 * @param record The string to be standardized
	 * 
	 * @return A list of <a href="StandardizedRecord.html">standardized records</a> extracted from the
	 *         input string. Note that a list, rather than a single record, is returned. This accounts
	 *         for the case when the string contains multiple occurrences of its data type separated
	 *         by conjunctions such as <i>and</i> or <i>aka</i>.
	 * 
	 * @throws StandardizationException
	 */
	public List<StandardizedRecord> standardize(String dataTypeName, String variantName, String record) throws StandardizationException;
	
	/**
	 * Normalize a string value in accordance to the transformation rules associated with its <code>fieldName</code>.
	 * It is possible for no transformation rule to apply to a particular string in which case it is returned unchanged.
	 * The <code>null</code> value may be acceptable as <code>fieldValue</code> as it may have a domain-specific
	 * representation such as <i>N/A</i> or the empty string.
	 * 
     * @param dataTypeName The name of the data type defining the structure of the input string's content
     * @param variantName The name of the variant within the data type
	 * @param fieldName The name of the field qualifying the string to be normalized
	 * @param fieldValue The string to be normalized
	 * @return A normalized representation of the input string or the unchagned input string if no transformations
	 *         applied
	 * @throws StandardizationException If any of the given data type, variant name or field name are unknown
	 */
	public String normalize(String dataTypeName, String variantName, String fieldName, String fieldValue) throws StandardizationException;
	
	/**
	 * Shutdown the engine instance by deallocating resources used by the standardization and normalization
	 * services.
	 * 
	 * @throws StandardizationException If an error occurs during resource deallocation
	 */
	public void shutdown() throws StandardizationException;
}
