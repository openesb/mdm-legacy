/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer;

/**
 * Base exception for all standardization and normalization errors.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class StandardizationException extends RuntimeException {
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Simple message constructor
	 * 
	 * @param message The error message associated with the exception
	 */
	public StandardizationException(String message) {
		super(message);
	}

	/**
	 * Nested exception constructor
	 * 
	 * @param message The error message specifying the context in which the cause was thrown
	 * @param cause The exception originating the error condition
	 */
	public StandardizationException(String message, Throwable cause) {
		super(message, cause);
	}
}
