/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer;

/**
 * A <code>Normalizer</code> takes a string value (as qualified by a field name) and
 * transforms it to conform to a canonical form.
 *  
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface Normalizer {
    /**
     * Transform a string to a canonical form as dictated by its field name. It is possible
     * for the given string not to match any transformation rule in which case it is
     * returned unchanged. Null values are acceptable as values as they can be represented
     * in a domain-specific manner (e.g. 'N/A').
     * 
     * @param fieldName The name of the field to which the value corresponds
     * @param fieldValue The string value to be transformed
     * @return The transformed string value or the unchanged string if it does not match
     *         any transformation rule
     * @throws NullPointerException if <code>fieldValue</code> is null
     */
    public String normalize(String fieldName, String fieldValue);
}