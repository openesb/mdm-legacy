/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.introspector;

/**
 * This interface extends <code>Descriptor</code> to add variant-container
 * services.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface DataTypeDescriptor extends Descriptor {
    /**
     * Return the array of field names exposed by this data type.
     * @return The array of field names exposed by this data type
     */
    public String[] getFieldNames();
    
    /**
     * Return the array of variants supported by this data type
     * @return the array of variants supported by this data type
     */
    public VariantDescriptor[] getVariants();
}
