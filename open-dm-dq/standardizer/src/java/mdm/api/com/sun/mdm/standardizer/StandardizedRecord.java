/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The result of standardizing an input string by extracting its component fields.
 * This interface provides access to a simple, concatenated string representation of
 * the standardized record's fields as well as to its individual components by retaining
 * the input and output field types identified during parsing.
 * 
 * An <in>input type</i> is a lexical category recognized in the input string.
 * 
 * An <i>output type</i> is a syntactic category identified during parsing.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface StandardizedRecord {
    /**
     * Return the set of <em>output</em> field names in the standaredized record.
     * This is the set of the output fields actually populated during parsing, not
     * the list of all possible field names defined by the standardizer.
     * 
     * @return The set of all output field names
     */
    public Set<String> getFieldNames();
    
    /**
     * Return the list of standardized field values associated with a given output
     * field name. Note that the given name may not necessarily appear among the
     * populated fields in the record.
     * 
     * @param fieldName
     * @return The list of <code>StandardizedField</code>s associated with a given
     *         output name or an empty list if such field was not populated for this
     *         standardized record.
     */
    public List<StandardizedField> getFields(String fieldName);

    /**
     * Return the concatenated string value of the output tokens corresponding to the
     * given field name. Contrast this method with <code>getFields(String fieldName)</code>
     * which returns the list of separated tokens. Concatenation is performed in an
     * output type-specific way that is not necessarily space-based. Note that the
     * given field may not necessarily be populated for this standardized record.
     * 
     * @param fieldName The field for which a concatenated string is to be produced
     * @return The concatenated output field representation or the empty string when
     *         the given field was not populated for this standardized record.
     */
    public String getFieldValue(String fieldName);
    
    /**
     * Return the list of concatenated string values corresponding to this given field
     * name. Note that the given field may not necessarily be populated for this standardized
     * record.
     * 
     * @param fieldName The field for which a list of concatenated strings is to be produced
     * @return The list of concatenated output fields or the empty list if the given field
     *         was not populated for this standardized record.
     */
    public List<String> getFieldValues(String fieldName);

    /**
     * Return the optional parameters associated with this standardized record. These
     * parameters store arbitrary application-specific data and may be empty. Parameters
     * are used to store metadata about the record such as its application-level data
     * type.
     * 
     * @return The, possibly empty, map of parameters associated with this standardized
     *         record.
     */
    public Map<String, Object> getParameters();

    /**
     * This class embodies a standardized field that specifies the input token type under
     * which it was recognized, the output token type to which it was assigned and the list
     * of tokens making up the field's content.
     *
     * @author Ricardo Rocha (ricardo.rocha@sun.com)
     */
    public static class StandardizedField {
        /**
         * The name of the input token type under which this field was recognized.
         */
        private String inputSymbol;
        /**
         * The name of the output token type to which this field was assigned.
         */
        private String outputSymbol;
        /**
         * The list of tokens making up the field value.
         */
        private List<String> tokens;
        
        /**
         * Construct a standardized field from its constituents.
         * 
         * @param tokens The list of tokens making up the field value
         * @param inputType The name of the input token type under which this field was recognized
         * @param outputType The name of the output token type to which this field was assigned
         */
        public StandardizedField(List<String> tokens, String inputSymbol, String outputSymbol) {
            this.tokens = tokens;
            this.inputSymbol = inputSymbol;
            this.outputSymbol = outputSymbol;
        }

        /**
         * Accessor for the standardized field's input type.
         * @return The input type name
         */
        public String getInputSymbol() { return inputSymbol; }
        /**
         * Accessor for the standardized field's output type.
         * @return The output type name
         */
        public String getOutputSymbol() { return outputSymbol; }
        /**
         * Accessor for the list of tokens making up the field's contents
         * @return
         */
        public List<String> getTokens() { return tokens; }
    }
}