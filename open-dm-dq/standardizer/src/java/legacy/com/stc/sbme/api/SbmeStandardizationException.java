/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

/**
 * The base class that handles exceptions related to the standardization engine
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeStandardizationException
    extends SbmeMatchEngineException {
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with no detail
     * message.
     * @param t a throwable
     */
    public SbmeStandardizationException(Throwable t) {
        super(t);
    }
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with the specified
     * detail message.
     *
     * @param s the detail message
     * @param t a throwable

     */
    public SbmeStandardizationException(String s, Throwable t) {
        super(s, t);
    }
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with the specified
     * detail message.
     *
     * @param s the detail message.
     */
    public SbmeStandardizationException(String s) {
        super(s);
    }
}
