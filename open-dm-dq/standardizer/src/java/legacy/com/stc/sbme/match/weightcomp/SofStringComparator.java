/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.util.EmeUtil;

/**
 * This algorithm comprises my own version of a generic string comparator, yet 
 * faster and less complicated than the Jaro or the Winkler/Jaro algorithms.
 * It handles transpositions, typos related to keypunch errors, characters with 
 * diacretical marks. You can tune the inportance of each parameters by changing 
 * the associated coefficients
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SofStringComparator {
    
    /**
     * Measure the degree of similarity between recordA and recordB using a generic
     * string comparator algorithm that accounts for accounts for insertions,
     * deletions, transpositions plus three additional enhancements (scanning errors,
     * large strings and linear dependencies of the error rate function of the
     * length)
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @param      mEng  an instance of the SbmeMatchingEngine class
     * @return     a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     */
    static double sofStrComparator(String recordA, String recordB, int index, boolean cons, MatchVariables mVar)
    throws SbmeMatchingException {
        
        StringBuffer recA = new StringBuffer(recordA.trim().toUpperCase());
        StringBuffer recB = new StringBuffer(recordB.trim().toUpperCase());
        StringBuffer sb = null;
        int lenA;
        int lenB;
        int fixLen;
        char aCh;
        char bCh;
        char aCh1;
        char bCh1;
                
        double exactM = 0;
        double typoM = 0;
        double transpM = 0;
        double diaM = 0;
                
        int minv;
        int maxv;
        int exactCoef = 6;
        int transpCoef = 4;
        int typoCoef = 3;
        int diaCoef = 5;
        int maxWeight;

        int i;
        int j;
        int set1;
        int set2;
        int numExact = 0;
        int numTypo = 0;
        int numDia = 0;
        int numTransp = 0;                               

        lenA = recA.length();
        lenB = recB.length();

        // Before going any further, test if we are using a 'nS' and if the record 
        // is not in the list of invalid SSNs (e.g. XXXXXX or 9999999)
        if (cons) {
 
            // Modify the pattern so that it can handle exactly Length times 
            // any specified constraint, Length being the length of the records A and B
            fixLen = mVar.ssnLength[index];

            // First test if the SSN needs to have a fixed length
            if (fixLen > 0) {
            
                // Make sure that both records have the same axpected length
                if ((lenA != fixLen) || (lenB != fixLen)) {
                    return 0.0;
                }
            }
 
            
            // Test also the type of the records (numeric or alphanumeric)
            // First, numeric
            if (mVar.recType[index].compareTo("nu") == 0) {

                // Make sure that both records are numerics
                for (i = 0; i < lenA; i++) {
                    if (!Character.isDigit(recA.charAt(i))) {
                        return 0.0;
                    }
                }

                for (i = 0; i < lenB; i++) {
                    if (!Character.isDigit(recB.charAt(i))) {
                        return 0.0;
                    }
                }

            // Second, alphanumeric
            } else if (mVar.recType[index].compareTo("an") == 0) {

                // Make sure that both records are numerics
                for (i = 0; i < lenA; i++) {
                    if (!Character.isLetterOrDigit(recA.charAt(i))) {
                        return 0.0;
                    }
                }

                for (i = 0; i < lenB; i++) {
                    if (!Character.isLetterOrDigit(recB.charAt(i))) {
                        return 0.0;
                    }
                }
            }
            
            if (mVar.ssnConstraints[index]) {
                sb = new StringBuffer();
                //First extend the pattern to include at least min(A, B) number
                // of identical characters in the pattern
                sb.append(mVar.ssnList[index]);
                sb.append("{").append(Math.min(lenA, lenB)).append(",}");

                Pattern pa = Pattern.compile(sb.toString());               

                /* Regular expression that catches empty strings */
                Matcher matA = pa.matcher(recA);
                Matcher matB = pa.matcher(recB);

                /* test if the record is empty or null */
                if (matA.lookingAt() || matB.lookingAt()) {
                    return 0.0;
                }
            }        
        } 


        // Determine which field is longer
        if (lenA > lenB) {
            maxv = lenA;
            minv = lenB;
            
        } else {
            maxv = lenB;
            minv = lenA;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }     
        
        // First, test the corresponding character-to-character for exact, keypunch
        // errors and diacritical marks matches
        for (i = 0; i < minv; i++) {
            aCh = recA.charAt(i);
            bCh = recB.charAt(i);
           
            // Exact character-to-character match
            if (aCh == bCh) {
                numExact++;
               
            // The characters pair is in the list of Key-punch errors
            } else if (mVar.adjwt[aCh][bCh] == 3) {
                
                numTypo++;
                
            // Only the first character has a diacretical mark   
            }  else if ((aCh > 191) && (bCh < 192)) {  
                
                if(EmeUtil.removeDiacriticalMark(aCh) == bCh) {
                    numDia++;
                }
                
            // Only the second character has a diacretical mark     
            }  else if ((bCh > 191) && (aCh < 192)) { 
                
                if(EmeUtil.removeDiacriticalMark(bCh) == aCh) {
                    numDia++;
                } 
            }                                                 
        }
 
        // Handle transposition
        for (i = 0; i < minv - 1; i++) { 
            aCh = recA.charAt(i);
            bCh = recB.charAt(i); 
            aCh1 = recA.charAt(i + 1);
            bCh1 = recB.charAt(i + 1);                          
            
            if ((aCh != bCh) && ((aCh == bCh1) & (aCh1 == bCh))) {                
                    numTransp++;     
            }                          
        }                                                

        // Measure the maximum weight. 
        maxWeight = maxv * exactCoef;
        
        // Measure the total weight
        exactM = numExact * exactCoef + numTypo * typoCoef + numDia * diaCoef 
                 + numTransp * transpCoef;
        
            
        if (exactM >= maxWeight) {
            return 1.0;
        } else {
            return exactM / maxWeight;
        }               
    }   
}
