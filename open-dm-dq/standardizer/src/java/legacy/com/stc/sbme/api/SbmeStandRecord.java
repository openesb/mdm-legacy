/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.util.List;
import java.util.Set;


/**
 * The SbmeStandRecord interface should be implemented by any standardization class.
 * This interface is designed to provide a common protocol for objects that perform
 * a record standardization
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public interface SbmeStandRecord {
    
    /**
     *
     * @param key a person name's field key
     * @return string representation field's value
     */
    String getValue(String key);
    
    /**
     *
     * @param key a person name's field key
     * @param value a person name's field value
     */
    void setValue(String key, String value);
    
    
    /**
     *
     * @param key a person name's field key
     * @return string representation field's value
     */
    List getValues(String key);

    /**
     *
     * @param key a person name's field key
     * @param value a person name's field value
     */
    void setValues(String key, List value);

    /**
     *
     * @param key value a person name's status key
     * @return string representation of the status
     */
    String getStatusValue(String key);
    
    /**
     *
     * @param key a person name status' key
     * @param value a person name's status value
     */
    void setStatusValue(String key, String value);

    
    /**
     * Gets the type of the record to be standardized (person, address...)
     * @return the type of the record
     */
    String getType();
    
    /**
     * Gets all the fields of a record
     *
     * @return a set of object's fields
     */
    Set getAllFields();
}
