/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import com.stc.sbme.match.emalgorithm.EMAlgorithm;
import com.stc.sbme.match.emalgorithm.PatternsCounter;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.match.weightcomp.MatchConfigFile;
import com.stc.sbme.match.weightcomp.MatchCounts;
import com.stc.sbme.match.weightcomp.MatchProcessor;
import com.stc.sbme.match.weightcomp.MatchVariables;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;


/**
 * Countains all the public methods used to calculate the matching weights
 * between the different compared records in the reference and candidate data.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeMatchingEngine {
    
    /*
     * Used as a flag for the one-to-one matching option.
     * The default choice is one-to-many matching.
     */
    private boolean isOneToOneMatch = false;
    
    /*
     * An instance variable of the base class that process all
     * the matching tasks
     */
    private MatchProcessor matchPro;
    
    /*
     * An instance variable of the base class that process all
     * the matching tasks
     */
    private MatchConfigFile mConfig;
    
    /*
     * An instance of the class that hold all the variables
     * in the matching engine
     */
    private MatchVariables matchVar;

    /*
     * Provides public methods to read and set the number of
     * different quantities
     */
    private PatternsCounter pattCount;

    /* * Provides public methods to read and set the number of 
     * different quantities 
     */
    private EMAlgorithm emAl;

    
    /*
     * Provides public methods to read and set the number of
     * different quantities
     */
    private MatchCounts matchC;

    /*
     * The input stream of the matching configuration file. 
     */
    private InputStream matchConfigStream;
    
    /* Get the input stream for the data file */
    private InputStream[] matchDataStream  = new InputStream[2];

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  

    /**
     * Returns an instance of the MatchVariables class
     * @param mEng the SbmeMatchingEngine object
     * @return a MatchVariables object
     */
    public MatchVariables getMatchVar(SbmeMatchingEngine mEng) {
        return mEng.matchVar;
    }
    
    /**
     * Returns an instance of the MatchCounts class
     * @param mEng the SbmeMatchingEngine object
     * @return a MatchCounts object
     */
    public MatchCounts getMatchC(SbmeMatchingEngine mEng) {
        return mEng.matchC;
    }
    
    
    /**
     * Returns an instance of the MatchConfigFile class
     * @param mEng the SbmeMatchingEngine object
     * @return a MatchConfigFile object
     */
    public MatchConfigFile getMatchConfig(SbmeMatchingEngine mEng) {
        return mEng.mConfig;
    }
    
    
    /**
     * Default constructor
     * @param filesAccess a stream to the data files
     * @throws SbmeConfigurationException a missing file exception
     * @throws SbmeMatchingException a matching exception
     * @throws IOException an I/O exception
     */
    public SbmeMatchingEngine(SbmeConfigFilesAccess filesAccess)
        throws SbmeConfigurationException, SbmeMatchingException, IOException {

        try {
            // Get the input stream for the data file
            matchDataStream[0] = filesAccess.getConfigFileAsStream("matchConstants.cfg");
            matchDataStream[1] = filesAccess.getConfigFileAsStream("matchConstantsInternal.cfg"); 
            ReadMatchConstantsValues.readMatchConstants(matchDataStream);
    
            mConfig = MatchConfigFile.getInstance();
            matchVar = MatchVariables.getInstance();
            matchC = MatchCounts.getInstance();
            matchPro = new MatchProcessor();

            pattCount = PatternsCounter.getInstance();
            emAl = EMAlgorithm.getInstance();
            
        } catch (Exception e) {
            mLogger.error("Exception", e);
        }
    }
    
    
    /**
     * Initialization method that upload all the data files.
     * @param filesAccess a stream to the data files
     * @throws SbmeConfigurationException a missing file exception
     * @throws SbmeMatchingException a matching exception
     * @throws IOException an I/O exception
     */
    public void initializeData(SbmeConfigFilesAccess filesAccess) 
        throws SbmeConfigurationException, SbmeMatchingException, IOException {
        
        try {
            // Get the input stream for the configuratin file matchConfigStream =
            matchConfigStream = filesAccess.getConfigFileAsStream("matchConfigFile.cfg");
            
        } catch (Exception e) {
            mLogger.error("Exception", e);
        }
    }
    
    
    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)
     *
     * @param domain geographic location of the compared data
     * @throws SbmeMatchingException a match exception
     * @throws SbmeMatchEngineException a generic match exception
     */
    public void upLoadConfigFile(String domain)
        throws SbmeMatchingException, SbmeMatchEngineException {
        
        // Reads the string comparator type
        // The initial probability/frequency tables
        // The down weighting parameters
        try {
            mConfig.getConfigData(matchConfigStream, this, domain);
            
        } catch (IOException e) {
            mLogger.error("Matching configuration File Not Found");
            throw new SbmeMatchingException("File Not Found", e);
        }
    }
        
    /**
     * Returns the status of the one-to-one matching flag.
     * @return a boolean
     */
    public boolean getOneToOneMatch() {
        return isOneToOneMatch;
    }
    
    
    /**
     * Updates the value (true/false) of the one-to-one matching flag.
     *
     * @param value the one to one match value
     */
    public void setOneToOneMatch(boolean value) {
        isOneToOneMatch = value;
    }
    
    
    /**
     * Clean up the memory from any data related to a given SbmematchingEngine
     * instance that is not needed anymore
     */
    public void shutdown() {
    }
    
    
    /**
     * Matches a candidate record against a reference record (defined by
     * instances of the MatchRecord class)
     *
     * @param candRec an instance of MatchRecord representing a candidate record
     * @param refRec an instance of MatchRecord representing a reference record
     * @return the matching weight of the records compared
     * @throws SbmeMatchingException the matching exception class
     */
    public double matchWeight(SbmeMatchRecord candRec, SbmeMatchRecord refRec)
    throws SbmeMatchingException {
        
        // Perform a record-to-record comparison
        return matchPro.performMatch(candRec, refRec, this);
    }
    
    
    /**
     * Same functionality as in the previous method, but the type
     * of the argument is different for performance reasons
     *
     * @param matchFieldsIDs the fields' names
     * @param candRecVals array of values of the fields in the candidate record
     * @param refRecVals the associated values of the reference record
     * @return a double
     * @throws SbmeMatchingException the matching exception class
     */
    public double matchWeight(String[] matchFieldsIDs, String[] candRecVals,
                              String[] refRecVals)
    throws SbmeMatchingException {
        
        // Perform a record-to-record comparison
        return matchPro.performMatch(matchFieldsIDs, candRecVals, refRecVals, this);
    }
    
    
    /**
     * Matches a candidate record against an array of reference records
     * and returns an array of matching weights.
     *
     * @param candRec the candidate record
     * @param refRecArray array of objects representing a reference records
     * @return an array of weights
     * @throws SbmeMatchingException the matching exception class
     */
    public double[] matchWeight(SbmeMatchRecord candRec, SbmeMatchRecord[] refRecArray)
    throws SbmeMatchingException {
        
        // Perform a record-arrayOfRecord matching process
        return matchPro.performMatch(candRec, refRecArray, this);
    }
    
    /**
     * Same functionality as in the previous method.
     *
     * @param matchFieldsIDs  the types of the records
     * @param candRecVals the value of the candidate record
     * @param refRecArrayVals 2D array of values associated with the fields of
     *                        an array of the reference records
     * @return an array of double
     * @throws SbmeMatchingException the matching exception class
     */
    public double[] matchWeight(String[] matchFieldsIDs, String[] candRecVals,
    String[][] refRecArrayVals)
    throws SbmeMatchingException {
        
        // Perform a record-arrayOfRecord matching process
        return matchPro.performMatch(matchFieldsIDs, candRecVals, refRecArrayVals, this);
    }
    
    
    /**
     * Matches an array of candidate records against an array of reference
     * records and returns a 2D array of matching weights.
     *
     * @param candRecArray array of objects representing a candidate records
     * @param refRecArray the reference record
     * @return a 2D array of doubles
     * @throws SbmeMatchingException the matching exception class
     */
    public double[][] matchWeight(SbmeMatchRecord[] candRecArray, SbmeMatchRecord[] refRecArray)
    throws SbmeMatchingException {
        
        // Perform a arrayOfRecord-arrayOfRecord matching process
        return matchPro.performMatch(candRecArray, refRecArray, this);
    }
    
    
    /**
     * Same functionality as in the previous method.
     *
     * @param matchFieldsIDs  the types of the fields
     * @param candRecArrayVals the candidate records
     * @param refRecArrayVals the reference records
     * @return a 2D array of doubles
     * @throws SbmeMatchingException the matching exception class
     * @throws ParseException if a string parsing failed
     */
    public double[][] matchWeight(String[] matchFieldsIDs, String[][] candRecArrayVals,
    String[][] refRecArrayVals)
        throws SbmeMatchingException, ParseException {
        
        
        if (matchVar.eMSwitch > 0) {
        
            // Create the frequency patterns
            pattCount.getPatternFrequencies(matchFieldsIDs, candRecArrayVals,
                                            refRecArrayVals, this);

            // Compute the fields' probabilities
            emAl.computeEMProba(matchFieldsIDs, this);
        }
        
        // Perform a arrayOfRecord-arrayOfRecord matching process
        return matchPro.performMatch(matchFieldsIDs, candRecArrayVals,
        refRecArrayVals, this);
    }
    
    
    /**
     * Searches for possible duplicates in a list of records to clean the data,
     * called cleansing or auto-matching
     *
     * @param selfRecArray array of objects representing a list of 'dirty' records
     * @return a 2Darray of doubles
     * @throws SbmeMatchingException the matching exception class
     */
    public double[][] matchWeight(SbmeMatchRecord[] selfRecArray)
    throws SbmeMatchingException {
        
        // Perform the self-matching process
        return matchPro.performMatch(selfRecArray, this);
    }
    
    
    /**
     * Same functionality as in the previous method.
     *
     * @param matchFieldsIDs  the types of the fields
     * @param selfRecArrayVals the set of records to be cleaned up
     * @return a 2D array of doubles
     * @throws SbmeMatchingException the matching exception class
     */
    public double[][] matchWeight(String[] matchFieldsIDs, String[][] selfRecArrayVals)
    throws SbmeMatchingException {
        
        // Perform the self-matching process
        return matchPro.performMatch(matchFieldsIDs, selfRecArrayVals, this);
    }
}
