/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class GetTable {
    
    // maximum number of frequency tables
    private static final int MAX_NO_TAB = ReadMatchConstantsValues.getMaxNumberTables();
    // maximum input record size
    private static final int MAX_INPUT_SIZE = ReadMatchConstantsValues.getMaxFreqTableSize();
    
    private static double[] pwgtTab;
    private static StringBuffer ppntTable;
    
    private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.match.weightcomp.GetTable");  
    
    /**
     *
     *
     * @param matchVar an instance of the MatchVariables class
     * @return a boolean tester
     * @throws SbmeMatchingException an exception
     * @throws SbmeMatchEngineException an exception
     * @throws IOException an exception
     */
    public static boolean initTable(MatchVariables matchVar)
    throws SbmeMatchingException, SbmeMatchEngineException, IOException {
        
        int i;
        
        // Use table with index 'x'
        StringBuffer filename = new StringBuffer("tablex.dat");
        
        for (i = 0; i < MAX_NO_TAB; i++) {
            
            // Replace the 'x' with the corresponding integer
            filename.insert(5, i + 1);
            
            // Verify if the table exists
            if ((matchVar.tableSize[i] = getTable(filename, matchVar.tableW[i])) == -1) {
                
                return false;
            }
            
            matchVar.pntTable[i] = ppntTable;
            matchVar.wgtTable[i] = pwgtTab;
        }
        return true;
    }
    
    /**
     *
     *
     * @param filename the name of the file
     * @param tableWidth the size of the table
     * @return the size of the table
     * @throws SbmeMatchingException an exception
     * @throws SbmeMatchEngineException an exception
     * @throws IOException an exception
     */
    public static int getTable(StringBuffer filename, int tableWidth)
    throws SbmeMatchingException, SbmeMatchEngineException, IOException {
        
        int i;
        int i1;
        int i2;
        
        int jj;
        int kk;
        int ll;
        
        int tableSize;
        int ptableSize;
        
        StringBuffer buffer = new StringBuffer();
        
        File frequencyTable = new File(filename.toString());
        
        // Test if the file is readable
        if (!frequencyTable.canRead()) {
            
            LOGGER.error("Cannot open the frequency table file " + filename);
            return -1;
        }
        
        i = 0;
        // Open a stream to read from the frequency table
        BufferedReader brFreq = EmeUtil.openBR(frequencyTable);
        
        // Count the number of lines in the frequency file
        while (!EmeUtil.feof(brFreq)) {
            
            if (EmeUtil.fgets(brFreq, buffer, MAX_INPUT_SIZE) != Boolean.FALSE) {
                i++;
            }
        }
        
        ptableSize = i;
        
        ll = buffer.length() - 1;
        brFreq.close();
        
        brFreq = EmeUtil.openBR(frequencyTable);
        ppntTable = new StringBuffer((ptableSize + 2) * tableWidth);
        pwgtTab = new double[ptableSize + 2];
        
        i = 0;
        jj = tableWidth - 1;
        kk = tableWidth + 1;
        
        if (kk > ll) {
            i1 = kk;
            i2 = ll;
            
        } else {
            i1 = ll;
            i2 = kk;
        }
        
        // Last entry is disagr wgt, first is highest agree wgt
        while (!EmeUtil.feof(brFreq)) {
            
            EmeUtil.fgets(brFreq, buffer, MAX_INPUT_SIZE);
            
            // fix the length of the string
            EmeUtil.setEndSb(ppntTable, i * tableWidth);
            
            // Copy the content of the table as a string into ppntTable
            EmeUtil.strncat(ppntTable, i * tableWidth, buffer, jj);
            
            // Store the parsed data into pwgtTab[].
            pwgtTab[i] = Double.parseDouble(buffer.substring(i1, i2));
            i++;
        }
        brFreq.close();
        
        return ptableSize;
    }
}
