/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.util.ArrayList;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Main class to process any request to match candidate to reference
 * records
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class AddressNormalizer {

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final String domain;

    private static final int SPACES = 0; 
    private static final int SLASH = 1;
    private static final int NOTHING = 2;
    
    static AddressClueWordsTable clueW;
    private final StandTokensRead standTokensRead;
    private final LegitTokenExists legitTokenExists;
    
    AddressNormalizer(String domain) {
        standTokensRead = new StandTokensRead(domain);
        legitTokenExists = new LegitTokenExists(domain);
        this.domain = domain;
    }
    
    /**
     * Define a factory method that return an instance of the class 
     * @return an instance of the class
     */
    static AddressNormalizer getInstance(String domain) {
        return new AddressNormalizer(domain);
    }   
    
    
    /**
     *
     *
     * @param fieldKey aaa
     * @param fieldValue aaa
     * @param sVar aaa
     * @param domain  aaa
     * @throws SbmeStandardizationException aaa
     * @return an array of normalized strings 
     */
    String[] performFieldsNormalization(String[] fieldKey, String[] fieldValue,
                    AddressStandVariables sVar, String domain)
        throws SbmeStandardizationException {
        
        /* The number of tokens */
        int numberKeys = fieldKey.length;

        int i;
        int index;

        /* The array of strings that hold the normalized fields */
        String[] normFields = new String[numberKeys];

        return normFields;
    }


    /**
     *
     *
     * @param standFields aaa
     * @param sVar aaa
     * @param aLoad an instance of the loading class
     * @param domain aaa
     * @param state the state code
     * @param zip the zip code
     * @throws SbmeStandardizationException aaa
     * @throws SbmeMatchEngineException aaa
     * @return an ArrayList of normalized tokens from an initial string
     */
    ArrayList performStringNormalization(ArrayList standFields, AddressStandVariables sVar,
                                        AddressTablesLoader aLoad, String domain, StringBuffer state,
                                                StringBuffer zip, int[] lineNums) 
        throws SbmeStandardizationException, SbmeMatchEngineException {

        char[] specialCharacters = standTokensRead.getSpecialCharacters();
        int MAX_INPUT_TOKENS = ReadStandConstantsValues.getInstance(domain).getMaxInputTokens();
        int MAX_WORDS = ReadStandConstantsValues.getInstance(domain).getMaxWords();
        int MAX_WORDS_INCLUE = ReadStandConstantsValues.getInstance(domain).getMaxWordsInclue();
                    
        /* Total number of fields inside arrayFields */
        int numberFields = standFields.size();

        int endWord;
        int currWord;
        int sn = 0;
        sVar.numTokensBuilt = 0;
        int i;
        int j;
        int k;
        int cWord = 0;
        int cWordId;

        boolean combo; 
        int itok;
        int[] clarr = new int[5]; 
        boolean oTfound;
        boolean tstLoaded = false;
        
        
        
        StringBuffer srchStr = new StringBuffer();
        StringBuffer newAddrString = new StringBuffer();
        StringBuffer cType = new StringBuffer(3);
        StringBuffer hold = new StringBuffer();
        
        ClueTable clue = new ClueTable();

        clueW =  aLoad.getClueWord();

        /* Instantiate the NormalizedToken objects */
        for (i = 0; i < numberFields; i++) { 
            sVar.tokensSt[i] = new TokenStruct(domain);
        }
        
       /*
        *  This is the search pattern for a hypothetical address:
        *
        *  (1) (2)  (3)   (4)
        *  123  N  MAINE  AVE
        *
        *  a.  1,2,3,4
        *  b.    2,3,4
        *  c.      3,4
        *  d.        4
        *
        *  e.    1,2,3
        *  f.      2,3
        *  g.        3
        *
        *  h.      1,2
        *  i.        2
        *
        *  j.        1
        *
        *  NOTE:  This sequence assumes that a combination of words
        *         was never found in the clues table, AND
        *         that the maximum number of words in a clue isn't
        *         greater than 4.  If it was, say, 3 then the first
        *         search pattern above would be:   
        *                      2,3,4
        *                          3,4
        *                            4
        *  VARIABLES
        *  ---------
        *   endWord:   The last word in a pattern search.
        *              In the example above, the endWord for e-g is AVE
        *  currWord:   The first word in a pattern search.
        *              In the example above, currWords are
        *              a. 1    b. 2    c. 3    d. 4
        *
        */
        endWord = numberFields;

        itok = 0;

        while (endWord > 0) {

            currWord = (endWord < MAX_WORDS_INCLUE) ? 1 : (endWord - MAX_WORDS_INCLUE) + 1;


            for (; currWord <= endWord;  currWord++) {

                combWords(srchStr, EmeUtil.readSubArray(sVar.wordArray, currWord - 1,
                       endWord), currWord, endWord, SPACES);
                       
                /* Create a copy of srchStr with uppercase characters */
                upCase(srchStr, hold);

                if ((clue = clueW.searchForClue(hold, state, zip)) != null) {


                   /* Whenever a clue is found, make sure its token type is
                    * legitimate for the current usage flag
                    */
                    if (!legitTokenExists.legitTokenExists(sVar.inUsageFlag, clue,
                    clarr)) {

                        clue = null;
                        continue;
                    }

                    if (unacceptableHRcombo(currWord - 1, clue, currWord, endWord,
                        numberFields, state, zip, sVar, domain)) {
                        
                        continue;
                    }
                    break;
                }
            }

            if (currWord > endWord) {
                currWord = endWord;
            }



            sVar.tokensSt[itok] = initTokenStruct(sVar.tokensSt[itok], domain);

            oTfound = false;


            /* Current word was not found in the clues list */
            if (clue == null) {

                if (validFraction(srchStr, domain)) {
                    
                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "FC", 1,
                                            0, currWord, endWord, sVar, lineNums);
                    k = 2;
                    
                } else if (sVar.inUsageFlag == 'N' && legitA3exists(srchStr)) {

                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "A3", 1,
                                            0, currWord, endWord, sVar, lineNums);
                    k = 2;
                    
                } else if (legitA2exists(srchStr)) {

                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "A2", 1,
                                            0, currWord, endWord, sVar, lineNums);
                    k = 2;
                    
                } else {
                    k = 1;
                }

                combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], tokenifyWord(srchStr), 
                                        k, 0, currWord, endWord, sVar, lineNums);

                if (combo) {

                     //sVar.tokensSt--;
                    itok--;
                }

                tstLoaded = true;
                endWord--;
                
            } else {
            // Current word WAS found

                if ((currWord > 1) && (validOTCombo(currWord - 1, (itok == 0) ? null
                        : sVar.tokensSt[itok - 1], tstLoaded, sVar))) {

                    oTfound = true;
                   /* 
                    * Combine the OT (previous word + current word)
                    * before placing it into the inputImage
                    */
                    combWords(srchStr, EmeUtil.readSubArray(sVar.wordArray, currWord - 2, MAX_WORDS),
                              currWord - 1, currWord, NOTHING);


                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "AU", 1, 0, currWord - 1,
                                            endWord, sVar, lineNums);


                    if ((sVar.inUsageFlag != 'T') || (sVar.inUsageFlag != 'S')) {

                        combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "OT",
                                                 2, 0, currWord - 1, endWord, sVar, lineNums);
                    }

                    tstLoaded = true;
                    
                    // shift over an extra word to account for the OT
                    endWord -= 2;

                } else if (((sVar.inUsageFlag != 'T') || (sVar.inUsageFlag != 'S')
                    || (sVar.inUsageFlag != ' ')) && dirNUcombo(srchStr, currWord, sVar)) {

                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "A1", 1, 0,
                                             currWord, endWord, sVar, lineNums);

                    if (combo) {
                        //sVar.tokensSt--;
                        itok--;
                    }

                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "D1", 2, 0,
                                            currWord, endWord, sVar, lineNums);

                    if (combo) {
                        //sVar.tokensSt--;
                        itok--;
                    }

                    tstLoaded = true;
                    endWord--;

                } else if (validFraction(currWord - 1, currWord, tstLoaded, (itok == 0) ? null 
                    : sVar.tokensSt[itok - 1], clue.getClueType1(), sVar)) {

                   /* 
                    * Combine the fraction with the previous word in the word
                    * array before placing it into the inputImage
                    */
                    EmeUtil.strcpy(srchStr, sVar.wordArray[currWord - 2]);
                    EmeUtil.strcat(srchStr, " ");
                    EmeUtil.strcat(srchStr, clue.getTranslation());

                    combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], "NU", 1, 0,
                                            currWord - 1, endWord, sVar, lineNums);

                    if (combo) {
                        //sVar.tokensSt--;
                        itok--;
                    }

                    tstLoaded = true;
                    endWord -= 4;

                /*  shift over 4 words */

                } else {


                   /*
                    * Load the token struct with all tokens returned from the clue
                    * word abbreviation struct, EXCEPT if any clue word type is OT
                    * (we already handled those cases). Also, if the current clue has
                    * only partially acceptable tokens, the make the clue word id's
                    * for the invalid one be 9999.
                    */
                    for (cWordId = 1, i = 1; i <= 5; i++) {

                        if (i == 1) {

                            cWord = (clarr[i - 1] == 1) ? EmeUtil.parseIntMethod(
                                   clue.getClueWordId1().toString()) : 9999;

                            EmeUtil.sprintf(cType, "%.2s", clue.getClueType1());

                        } else if (i == 2) {
                        
                            cWord = (clarr[i - 1] == 1) ? EmeUtil.parseIntMethod(
                                   clue.getClueWordId2().toString()) : 9999;

                            EmeUtil.sprintf(cType, "%.2s", clue.getClueType2());

                        } else if (i == 3) {

                            cWord = (clarr[i - 1] == 1) ? EmeUtil.parseIntMethod(
                                   clue.getClueWordId3().toString()) : 9999;

                            EmeUtil.sprintf(cType, "%.2s", clue.getClueType3());

                        } else if (i == 4) {

                            cWord = (clarr[i - 1] == 1) ? EmeUtil.parseIntMethod(
                                   clue.getClueWordId4().toString()) : 9999;

                            EmeUtil.sprintf(cType, "%.2s", clue.getClueType4());

                        } else if (i == 5) {

                            cWord = (clarr[i - 1] == 1) ? EmeUtil.parseIntMethod(
                                   clue.getClueWordId5().toString()) : 9999;

                            EmeUtil.sprintf(cType, "%.2s", clue.getClueType5());
                        }


                       /*
                        * Break when there's no more clues. Continue if an OT has
                        * been found, because we've already handled them, UNLESS the
                        * variable oTfound is TRUE (meaning we've found a valid OT
                        * for this clue and it was already combined, hence, we don't
                        * need the OT token).
                        */
                        if (cWord == 0 && (EmeUtil.strncmp(cType, "  ", 2) == 0)) {
                            break;
                        }
                        
                        // Skip if the type is "OT"
                        if ((EmeUtil.strncmp(cType, "OT", 2) == 0) && oTfound) {
                            continue;
                        }

                        combo = loadTokenStruct(srchStr, sVar.tokensSt[itok], cType.toString(),
                                                cWordId, cWord, currWord, endWord, sVar, lineNums);

                        if (combo) {
                            //sVar.tokensSt--;
                            itok--;
                        }

                        tstLoaded = true;
                        cWordId++;
                    }
                    endWord = currWord - 1;
                }
            }


           /*
            * Increment the counter for the number of tokens
            * Increment the pointer to the array of token structs
            */
            sVar.numTokensBuilt++;

            /* check for a blowout */
            if (sVar.numTokensBuilt > MAX_INPUT_TOKENS) {
                
                mLogger.error("ERROR: trying to build more than"
                + MAX_INPUT_TOKENS + " tokens \n Address: " + newAddrString);

                throw new SbmeStandardizationException("ERROR: trying to build more than"
                    + MAX_INPUT_TOKENS + " tokens \n Address: " + newAddrString);
            }

            // sVar.tokensSt++;
            itok++;
        } /* end while */

        //Shift the line numbers as necessary
        for (int x=lineNums.length - 1; x >=0; x--) {
            if (lineNums[x]==-1) {
                shiftElements(lineNums, x, 1);
            }
        }
        
        // Clear the standFields ArrayList object, then update it with the new fields 
        standFields.clear();
        sn = sVar.numTokensBuilt;
        
        for (i = 0; i < sn; i++) {
            standFields.add(i, sVar.tokensSt[i].getInputImage());
        }
        

        /* Free the sVar.wordArray objects */
        for (i = 0; i < numberFields; i++) {
            sVar.wordArray[i] = null;
        }

        //sVar.tokensSt -= numTokensBuilt;
        itok -= sVar.numTokensBuilt;

        /* Reverse the token array (the tokens were loaded in reverse order) */
        for (i = sVar.numTokensBuilt / 2, j = i - ((sVar.numTokensBuilt + 1) % 2);
             i < sVar.numTokensBuilt; i++, j--) {

            switchTokStruct(sVar.tokensSt[i], sVar.tokensSt[j]);
        }

        // Check for AN's
        numberFields = checkForANs(sVar.tokensSt, sVar.numTokensBuilt, sVar);

        while (sVar.numTokensBuilt > numberFields) {
            standFields.remove(sVar.numTokensBuilt-- - 1);
        }
        
        return standFields;
    }

    /**
     * Returns true if the char sent is a special character that can NOT
     * be removed from the address.
     *
     * @param ch special character
     * @return <code>boolean</code>
     */
    boolean isSpecialChar(char ch) {

        int i = 0;

        char[] specialCharacters = standTokensRead.getSpecialCharacters();
        
        while (i < specialCharacters.length) {

            if (specialCharacters[i] == ch) {
                return true;
            }
            i++;
        }
        return false;
    }

    /**
     *  Sets all lower case characters in "str" to upper case and returns a copy of
     *  str, leaving the original unchanged.
     *
     * @param str the address string in
     * @param out the address string out
     */
    protected static void upCase(StringBuffer str, StringBuffer out) {

        int i = 0;

        /* Set the length of the string */
        int len = str.length();

        EmeUtil.strcpy(out, str);

        while (i < len) {

            if (Character.isLowerCase(out.charAt(i))) {
                out.setCharAt(i, Character.toUpperCase(out.charAt(i)));
            }
            i++;
        }
    }


    /**
     *
     * @param str the address string in
     * @param out the address string out
     */
    protected static void lowCase(StringBuffer str, StringBuffer out) {
        int i = 0;
        int len = str.length();
        EmeUtil.strcpy(out, str);

        while (i < len) {

            if (Character.isUpperCase(out.charAt(i))) {
                out.setCharAt(i, Character.toLowerCase(out.charAt(i)));
            }
            i++;
        }
    }


    /**
     *  Receives the word array, a starting point, and an ending point and 
     *  will combine the words in that range into 1 string.
     * @param str the address string in
     * @param array an array of strings
     * @param st the start index
     * @param end the end endex
     * @param combine aaa
     */
    private static void combWords(StringBuffer str, StringBuffer[] array, 
                                  int st, int end, int combine) {

        int j;
        int i;
        int len = 0;

        /* Adjust st and end because arrays start with 0 */
        st -= 1;  
        end -= 1;

        for (i = st, j = 0; i <= end; i++, j++) {
            len += array[j].length();
        }


        for (i = st, j = 0; i <= end; i++, j++) {

            if (i == st) {
                EmeUtil.strcpy(str, array[j]);

            } else {
                EmeUtil.strcat(str, array[j]);
            }

            if ((i != end) && (combine == SPACES)) {
                EmeUtil.strcat(str, " ");
            }

            if ((i != end) && (combine == SLASH)) {
                 EmeUtil.strcat(str, "/");
            }
        }
    }

    /**
     *  Receives a word and returns the token (NU, AU, or A1) if noy found in
     *  the clues table
     *
     * @param str
     * @return <code>String</code>
     */
    private static String tokenifyWord(StringBuffer str) {
        char cStr;
        int len;
        int i;

        boolean alphaex = false;
        boolean digitex = false;
        
        /* length of the string */
        len = str.length();
        if (str.length() == 0) return "";

        cStr = str.charAt(0);

        if ((len == 1) && ((Character.isLetter(cStr)) || (cStr == '-' || cStr == '�'))) {
            return ("A1");
        }

        for (i = 0; i < len; i++) {
            cStr = str.charAt(i);

            if (Character.isDigit(cStr)) {
                digitex = true;
                
            } else if ((i > 0) & (i <len - 1) && (cStr == '-'|| cStr == '�') && Character.isDigit(str.charAt(i - 1))
                               && Character.isDigit(str.charAt(i + 1))) {
                digitex = true;

            } else {
                alphaex = true;
            }
        }
        return (alphaex ? "AU" : "NU");
    }


    /**
     *  Sends a string and return TRUE if the string contains only 2 alphas (A2), 
     *  FALSE otherwise
     *
     * @param str
     * @return <code>boolean</code>
     */
    private static boolean legitA2exists(StringBuffer str) {
    
        int i;

        if (str.length() != 2) {
            return false;
        }

        for (i = 0; i < 2; i++) {

            if (!Character.isLetter(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }


    /**
     *  Sends a string and return TRUE if the string contains only 3 alphas (A3),
     *  FALSE otherwise
     *
     * @param str the input string
     * @return <code>boolean</code>
     */
    private static boolean legitA3exists(StringBuffer str) {

        int i;

        if (str.length() != 3) {
            return false;
        }

        for (i = 0; i < 3; i++) {

            if (!Character.isLetter(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }


    /**
     *  Receives a NormalizedToken object and initializes its 5 clue word id's
     *  to 0, and the 5 clue types to blank, and the input image st
     *
     * @param tst a NormalizedToken instance
     * @return the output NormalizedToken instance
     */
    private static TokenStruct initTokenStruct(TokenStruct tst, String domain) {
    
        int in = 0;
        String sIn = "  ";

        if (tst == null) {
            tst = new TokenStruct(domain);
        }

        int IMAGE_SIZE = ReadStandConstantsValues.getInstance(domain).getImageSize();   
        tst.setInputImage(EmeUtil.memset1(tst.getInputImage(), ' ', IMAGE_SIZE));

        tst.setClueWordId1(in);
        tst.setClueType1(sIn);

        tst.setClueWordId2(in);
        tst.setClueType2(sIn);

        tst.setClueWordId3(in);
        tst.setClueType3(sIn);

        tst.setClueWordId4(in);
        tst.setClueType4(sIn);

        tst.setClueWordId5(in);
        tst.setClueType5(sIn);

        tst.setBeg(in);
        tst.setEnd(in);
         
        return tst;
    }


    /**
     *  Receives sVar.wordArray (positioned at the current word), the previously determined
     *  token struct, and the clues table struct for the current word. Returns true if:
     *     1.  The current word is an ordinal type
     *     2.  The previous word in the string is a matching numeric (eg.  1 for st, 
     *         2 for nd, 3 for rd, and 0,4-9 for th).
     *     3.  If the current word is st or rd, then the next word in the string must 
     *         be a TY
     *  For example:     123 1 st ave;  if st is the current word
     *  being processed then we know it's an ordinal type.
     *
     * @param index aaa
     * @param tst aaa 
     * @param tstLoaded  aaa
     * @param sVar aaa
     * @return <code>boolean</code>
     */
    private boolean validOTCombo(int index, TokenStruct tst, boolean tstLoaded, 
                                 AddressStandVariables sVar) {

        int i;
        int len;
        boolean isFR = domain.compareTo("FR") == 0;      
        String TY = "TY";;
        String ST;
        String RD;        
        String st;
        String rd;
        
        if (isFR) {
            ST = "ER";
            RD = "EME";        
            st = "er";
            rd = "eme";              
        } else {
            ST = "ST";
            RD = "RD";        
            st = "st";
            rd = "rd";            
        }

        StringBuffer str;
        StringBuffer word = new StringBuffer();
        StringBuffer hold = new StringBuffer(3);
        
        /* Copy a lowercase version of sVar.wordArray[index] into word */
        lowCase(sVar.wordArray[index], word);

        // Make sure the current word is ST, ND, RD, or TH
        if (!ordinalType(word, domain)) {
            return false;
        }

        // Check to see if the previous word is all digits
        str = sVar.wordArray[index - 1];

        if (!allDigits(str)) {
            return false;
        }
        

       /* 
        * Examine the previous word to see what its OT compliment is.
        * If the word is "TH" or "ND", then change the OT to match
        * the number in the previous, if necessary.
        */
        matchNumberWithOT(str, hold, domain);

        if ((EmeUtil.strncmp(word, st, 2) != 0)
            && EmeUtil.strncmp(word, rd, 2) != 0) {

            lowCase(hold, word);
        }

        EmeUtil.strcpy(sVar.wordArray[index], word);

        // If the OT is "ST" or "RD" then the word following the OT
        // must be a type ("TY") token.
        if (EmeUtil.strcmp(sVar.wordArray[index], st) == 0
              || EmeUtil.strcmp(sVar.wordArray[index], ST) == 0
            || EmeUtil.strcmp(sVar.wordArray[index], rd) == 0     
              || EmeUtil.strcmp(sVar.wordArray[index], RD) == 0) {

            if (tstLoaded) {

                if (EmeUtil.strcmp(tst.getClueType1(), TY) != 0
                        && EmeUtil.strcmp(tst.getClueType2(), TY) != 0
                        && EmeUtil.strcmp(tst.getClueType3(), TY) != 0
                        && EmeUtil.strcmp(tst.getClueType4(), TY) != 0
                        && EmeUtil.strcmp(tst.getClueType5(), TY) != 0) {

                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     *  Receives the sVar.wordArray (positioned at the current word), the previously
     *  determined token struct, and the current word number, and will return TRUE if:
     *     1.  The current word is a fraction,
     *     2.  The previous word is a numeric, and
     *     3.  The following word is NOT a TY or A1.
     *     4.  The current word number is not 1, ie.
     *         "1/2 street" has sVar.NOTHING to combine 1/2 with.
     *
     *  EX:  123 1/2 main st.    will be combined to:
     *        NU - 1231/2
     *        AU - main
     *        TY - st
     *  This is done because for every NU... pattern, there is a NU FC ... pattern.  
     *  This eliminates the second set of patterns.
     *
     * @param index aaa
     * @param currWord aaa
     * @param tstLoaded aaa
     * @param tst aaa
     * @param cType aaa
     * @param sVar aaa
     * @return <code>boolean</code>
     */
    private static boolean validFraction(int index, int currWord, boolean tstLoaded, TokenStruct tst,
                                         StringBuffer cType, AddressStandVariables sVar) {

        int i;
        int len;

        StringBuffer str;
        char cStr;

        if ((currWord <= 1) || (EmeUtil.strcmp(cType, "FC") != 0)) {
            return false;
        }

        str = sVar.wordArray[index - 1];


        /* length of the string */
        len = str.length();

        cStr = str.charAt(0);

        for (i = 0; i < len; i++) {

            cStr = str.charAt(i);

            if (!Character.isDigit(cStr)) {
                 return false;
            }
        }

       /*
        * If the endWord = numwords then don't check the previously
        * loaded tst struct, because NOTHING was loaded!!!!
        */
        if (tstLoaded) {

            if ((EmeUtil.strcmp(tst.getClueType1(), "TY") == 0)
                    || (EmeUtil.strcmp(tst.getClueType1(), "A1") == 0)) {

                 return false;
            }
        }
        return true;
    }


    /**
     *  Receives the sVar.wordArray (positioned at the current word), the previously
     * @param index aaa
     * @param currWord aaa
     * @param tstLoaded aaa
     * @param tst aaa
     * @param cType aaa
     * @param sVar aaa
     * @return <code>boolean</code>
     */
    private static boolean validFraction(StringBuffer strToken, String domain) {

        int ii;
        int len = strToken.length();
        char csm;
        char csp;
        char cStr;
        
       /*
        * Accept tokens with an inner fraction '/' if:
        * it is a S/C for the US locale or C/O for the UK locale
        * it is a numeric on both sides, then for the US locale it must be
        * a 1 or 3 on the left-side and a 2, 3, 4, or 8 on the right-side.
        * For the UK locale it is any number on both sides
        */  
        for (ii = 0; ii < len; ii++) {      

            cStr = strToken.charAt(ii);
            
            if ((cStr == '/') && (ii > 0) && (ii < len - 1)) {
                    
                csm = strToken.charAt(ii - 1);
                csp = strToken.charAt(ii + 1);  
                             
                if (domain == "US") {

//                    if (csm == 'S' && csp == 'C') {
//                        return true;
//                    }
                
                    if (Character.isDigit(csp) && Character.isLetterOrDigit(csm)) { 
                        return true;       
                    }
/*                                
                    if ((Character.isDigit(csm) && (csm == '1' || csm == '3')
                        && csp == '2' || csp == '3' || csp == '4' || csp == '8')) {                                
                        return true;
                    } else {
                        strToken.setCharAt(ii, ' ');
                    }
*/                
                } else if (domain == "UK") {
                    if (csm == 'C' && csp == 'O') { 

                        return true;
                    }
                    if (Character.isDigit(csp) && Character.isLetterOrDigit(csm)) {
                        return true;       
                    }
                }
            }
        }
        return false;
    }





    /**
     *  Receives a string and return true if it's
     *  one of the 4 ordinal types (st, nd, rd, or th).
     *
     * @param str the input string
     * @return <code>boolean</code>
     */
    private static boolean ordinalType(StringBuffer str, String domain) {

        String ST;
        String RD;        
        String st;
        String rd;
        String TH;
        String th;
        String nd;
        String ND;
        
        if (domain.compareTo("FR") == 0) {
            ST = "ER";
            RD = "EME";        
            st = "er";
            rd = "eme";
            TH = "�ME"; 
            th = "�me";
            ND = "ERE";
            nd = "ere";             
        } else {
            ST = "ST";
            RD = "RD";        
            st = "st";
            rd = "rd"; 
            TH = "TH"; 
            th = "th";
            ND = "ND";
            nd = "nd";                        
        }
        return ((EmeUtil.strcmp(str, ST) == 0) || (EmeUtil.strcmp(str, ND) == 0)
                || (EmeUtil.strcmp(str, RD) == 0)
                || (EmeUtil.strcmp(str, TH) == 0)
                || (EmeUtil.strcmp(str, st) == 0)
                || (EmeUtil.strcmp(str, nd) == 0)
                || (EmeUtil.strcmp(str, rd) == 0)
                || (EmeUtil.strcmp(str, th) == 0));
    }


    /**
     * Builds a token struct with the information sent. Returns TRUE if the
     * following is true:
     *   the string sent is a comma.  Commas were given their own words but we
     *   dont want them in the output token structure.  For example,  123 main
     *   st, rt 4. In this case the comma was use to separate "st" and "rt" but
     *   it serves no purpose from here on out.
     *
     * @param srchStr aaa
     * @param tst aaa
     * @param cType aaa
     * @param cWordId aaa
     * @param cWord aaa
     * @param curr aaa
     * @param end aaa
     * @param sVar aaa
     * @return <code>boolean</code>
     */
    private static boolean loadTokenStruct(StringBuffer srchStr, TokenStruct tst, String cType,
                        int cWordId, int cWord, int curr, int end, AddressStandVariables sVar, int[] lineNums) {

        StringBuffer hold;

       /* 
        * EXCEPTION 1:  If the current string is a comma, then dont load
        * anything, and return true.
        */
        if (EmeUtil.strcmp(srchStr, ",") == 0) {
            lineNums[curr] = -1;
            //shiftElements(lineNums, curr, 1);
            sVar.numTokensBuilt--;
            return true;
        }
        
        tst.setInputImage(srchStr);
        
        if (cWordId == 1) {

            tst.setClueWordId1(cWord);
            tst.setClueType1(cType);

        } else if (cWordId == 2) {

            tst.setClueWordId2(cWord);
            tst.setClueType2(cType);

        } else if (cWordId == 3) {

            tst.setClueWordId3(cWord);
            tst.setClueType3(cType);

        } else if (cWordId == 4) {

            tst.setClueWordId4(cWord);
            tst.setClueType4(cType);

        } else if (cWordId == 5) {

            tst.setClueWordId5(cWord);
            tst.setClueType5(cType);
        }

        tst.setBeg(curr);
        tst.setEnd(end);
        for (int i=curr; i < end; i++) {
            lineNums[curr] = -1;
        }
        return false;
    }

    private static void shiftElements(int[] lineNums, int start, int shift) {
        for (int i=start; i < lineNums.length - shift; i++) {
            lineNums[i] = lineNums[i + shift];
        }
        //Don't worry about items at end of array whose value is not changed.  
    }
    
    /**
     *  charExists is sent a string and returns TRUE if any
     *  character in str is an alpha, FALSE otherwise.
     *
     * @param str the input string
     * @return <code>boolean</code>
     *
     */
    private static boolean charExists(StringBuffer str) {

        int i;
        int len;

        for (i = 0, len = AddressClueWordsTable.trimln(str); i < len; i++) {

            if (Character.isLetter(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }


    /**
     *  Receives a string and returns the substring directly after
     *  the first blank in str, or NULL if str contains no blank characters.
     *
     * @param str the input string
     * @return <code>String</code>
     */
    private static String embeddedBlanks(StringBuffer str) {

        int indBlank = str.toString().indexOf(' ');

        if (indBlank == -1) {
            return null;

        } else {
            return str.substring(indBlank + 1);
        }
    }


    /**
     * Receives a number (char) and match that number with its valid Ordinal Type and
     * return it, UNLESS it's not a match and the OT is ST or RD (example:
     *  "65st" wont be corrected). EX:  62 is matched properly with "ND".
     *
     * @param num aaa
     * @param ord aaa
     */
    static void matchNumberWithOT(StringBuffer num, StringBuffer ord, String domain) {

        char lastDigit;
        char sec2LastDigit;
        boolean dFR = domain.compareTo("FR") == 0;
        String th;
        String nd;        
        String st;
        String rd;
        
        if (dFR) {
            return;
//            th = "eme";
//            nd = "�me";        
//            st = "er";
//            rd = "ere";              
        } else {
            th = "th";
            nd = "nd";        
            st = "st";
            rd = "rd";            
        }

        lastDigit = num.charAt(num.length() - 1);

        sec2LastDigit = (AddressClueWordsTable.trimln(num) > 1)
                          ? num.charAt(num.length() - 2) : ' ';

       /*
        *  Valid combos:
        *                  ST    ND    RD    TH
        *                  --    --    --    --
        *                   1     2     3    4-20
        *                  21    22    23   24-30
        *                  31    32    33   34-40
        *                   .     .     .     .
        *                   .     .     .     .
        *                 101   102   103  104-120
        *                 121   122   123  124-130
        *                   .     .     .     .
        *                   .     .     .     .
        */
        if (lastDigit == '1' && sec2LastDigit != '1') {

            EmeUtil.strcpy(ord, st);

        } else if (lastDigit == '2' && sec2LastDigit != '1') {

            EmeUtil.strcpy(ord, nd);

        } else if (lastDigit == '3' && sec2LastDigit != '1') {

            EmeUtil.strcpy(ord, rd);

        } else {
      
            EmeUtil.strcpy(ord, th);              
        }       
    }


    /**
     * allDigits is sent a string, str, and returns TRUE if every
     * character in str is a digit, FALSE otherwise.
     *
     * @param str the input string
     * @return <code>boolean</code>
     */
    static boolean allDigits(StringBuffer str) {

        int i;
        int len;

        /* Length of the string */
        len = str.length();

        for (i = 0; i < len; i++) {
            
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     *
     * @param clue aaa
     * @return <code>boolean</code>
     */
    private static boolean roadOrHighway(ClueTable clue) {

        int cWord;

        cWord = Integer.parseInt(clue.getClueWordId1().toString().trim());

        if ((EmeUtil.strncmp(clue.getClueType1(), "HR", 2) == 0)
            && ((cWord == 67)              /* County highway */
                || (cWord == 69)           /* County road */
                || (cWord == 99)           /*  Farm road */
                || (cWord == 257)          /* State highway */
                || (cWord == 258)          /* State road */
                || (cWord == 272)          /*  Township highway */
                || (cWord == 273)          /*  Township road */
                || (cWord >= 320))) {      /*  Township road */

            return true;
        }
        return false;
    }


    /**
     * If the current word is N, S, W, or E  AND  if it was previously split up
     * from a numeric, then make it only a 0A1... EX:  123 main st e6
     * Here, we don't want "e" to be the suffix direction EAST.
     *
     * @param str the input string
     * @param curr aaa
     * @param sVar aaa
     * @return <code>boolean</code>
     */
    private boolean dirNUcombo(StringBuffer str, int curr, AddressStandVariables sVar) {
        String w;
        String W;
        
        if (domain.compareTo("FR") == 0) {
            w = "o";
            W = "O";                   
        } else {
            w = "w";
            W = "w";              
        }      
        return ((EmeUtil.strcmp(str, "N") == 0 || EmeUtil.strcmp(str, "n") == 0
                || EmeUtil.strcmp(str, "S") == 0
                || EmeUtil.strcmp(str, "s") == 0
                || EmeUtil.strcmp(str, "E") == 0
                || EmeUtil.strcmp(str, w) == 0
                || EmeUtil.strcmp(str, W) == 0
                || EmeUtil.strcmp(str, "e") == 0)
            && (sVar.wordSplits[curr - 1] != 0));
    }


    /**
     *  If the clue is ST xx, (ie. ST RT, or ST RD,etc), then examine the previous
     *  word.  If the previous word is a type (eg. rd, ave, etc) or an SA (full state
     *  name or a state abbreviation) or EX (old), then return FALSE
     *  (keep this clue).  If it's not a type and if any character exists in the
     *  previous word, then return true. Also, if the clue is xxxx RD or xxxx ROAD then
     *  examine the word AFTER the clue.  If it's not a NU, A1, or A2 then
     *  return true.  This will solve the following cases:
     *       123 california st hwy 1 (keep st hwy)
     *       123 old st hwy 1 (keep st hwy)
     *       123 main st st rt 4 box 2340 (keep st rt)
     *       123 main st rt 4 (dont keep st rt)
     *       123 county rd (dont keep county rd)
     *       123 county rd y33 (keep county rd)
     *
     * @param index aaa
     * @param clue aaa
     * @param currWord aaa
     * @param endWord aaa
     * @param numwords aaa
     * @param state aaa
     * @param zip aaa
     * @param sVar aaa
     * @return <code>boolean</code>
     */
    private static boolean unacceptableHRcombo(int index, ClueTable clue,
            int currWord, int endWord, int numwords, StringBuffer state,
            StringBuffer zip, AddressStandVariables sVar, String domain) {

        String pos;
        StringBuffer prevword;
        StringBuffer nextword;

        StringBuffer hold = new StringBuffer();
        ClueTable prevclue;
        
        String TY = "TY";
        String SA = "SA";
        String ST;        
        if (domain.compareTo("FR") == 0) {
            ST = "R ";              
        } else {
            ST = "ST ";              
        }              

        if ((currWord > 1) && (pos = embeddedBlanks(clue.getName())) != null) {

            if (EmeUtil.strncmp(clue.getName(), ST, 3) == 0) {

                prevword = sVar.wordArray[index - 1];

                /* Create a copy of srchStr with uppercase characters */
                upCase(prevword, hold);

                prevclue = clueW.searchForClue(hold, state, zip);

                if ((prevclue != null) 
                    && ((EmeUtil.strcmp(prevclue.getClueType1(), TY) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType2(), TY) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType3(), TY) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType4(), TY) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType5(), TY) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType1(), "EX") == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType1(), SA) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType2(), SA) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType3(), SA) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType4(), SA) == 0)
                    || (EmeUtil.strcmp(prevclue.getClueType5(), SA) == 0))) {

                    return false;
                }


                if (charExists(prevword)) {
                    return true;
                }
            }
        }

        if (roadOrHighway(clue)) {

            if (endWord == numwords) {
                return true;
            }

            nextword = sVar.wordArray[index + (endWord - currWord) + 1];

            if (!allDigits(nextword) 
                    && (AddressClueWordsTable.trimln(nextword) > 1)
                    && !legitA2exists(nextword)) {

                return true;
            }
        }
        return false;
    }



    /**
     *
     *
     * @param tst aaa
     * @param numtoks aaa
     * @param sVar aaa
     */
    private static int checkForANs(TokenStruct[] tst, int numtoks, AddressStandVariables sVar) {

        int i;
        int j;
        boolean anfound = false;
        String NU = "NU";
        String AN = "AN";
        String A1 = "A1";
        String tspace = "  ";
        int zer = 0;
        

        // Check for BX, HR, RR, BP, PT, or WD
        for (i = 0; i < numtoks; i++) {

            if (tokenInStruct(tst[i], "BX") 
                    || tokenInStruct(tst[i], "HR")
                    || tokenInStruct(tst[i], "WD")
                    || tokenInStruct(tst[i], "RR")
                    || tokenInStruct(tst[i], "PT")
                    || tokenInStruct(tst[i], "BP")) {

               /* Start at the word after the current word and make
                * the subsequent words AN's if the following cases are true:
                *    1.  All subsequent words that are potential ANs must be
                *        adjacent.
                *    2.  The first word must be an NU or A1.
                *    3.  All other words must be an NU, A1, or AU and
                *        if we are processesing an HR, then they CANT be D1
                *        (this prevents  123 hwy 1e  from grouping 1e together)
                */
                for (j = (i + 1); j < numtoks; j++) {

                    if (sVar.wordSplits[tst[j].getEnd() - 1] == 0) {
                        break;
                    }

                    if (j == (i + 1)) {

                        // First word after
                        if (!tokenInStruct(tst[j], NU)
                                && !tokenInStruct(tst[j], A1)) {

                            break;
                        }

                        // Next check solves:  123 hwy i80
                        if (tokenInStruct(tst[j], "HR")) {
                            break;
                        }
                    } else {

                        if ((tokenInStruct(tst[j], NU)
                                || tokenInStruct(tst[j], A1)
                                || tokenInStruct(tst[j], "AU"))
                                && (!tokenInStruct(tst[j], "D1")
                                || !tokenInStruct(tst[j], "HR"))) {

                            // Make the 1st word an AN
                            tst[i + 1].setClueType1(AN);
                            tst[i + 1].setClueWordId1(zer);

                            tst[i + 1].setClueType2(tspace);
                            tst[i + 1].setClueWordId2(zer);

                            tst[i + 1].setClueType3(tspace);
                            tst[i + 1].setClueWordId3(zer);

                            tst[i + 1].setClueType4(tspace);
                            tst[i + 1].setClueWordId4(zer);

                            tst[i + 1].setClueType5(tspace);
                            tst[i + 1].setClueWordId5(zer);

                            // Make every subsequent word an AN
                            tst[j].setClueType1(AN);
                            tst[j].setClueWordId1(zer);

                            tst[j].setClueType2(tspace);
                            tst[j].setClueWordId2(zer);

                            tst[j].setClueType3(tspace);
                            tst[j].setClueWordId3(zer);

                            tst[j].setClueType4(tspace);
                            tst[j].setClueWordId4(zer);

                            tst[j].setClueType5(tspace);
                            tst[j].setClueWordId5(zer);

                            anfound = true;
                        } else {
                            break;
                        }
                    }
                }
                i = j - 1;
            }
        }

       /*
        * If any ANs were found then you must now collapse the token
        * structure together.
        */
        if (anfound) {

            for (i = 0; i < (numtoks - 1); i++) {

                if (tokenInStruct(tst[i], AN)
                        && tokenInStruct(tst[i + 1], AN)) {

                    appendTokStruct(tst[i], tst[i + 1]);
                    nullTokenStruct(tst[i + 1]);

                    for (j = i + 1; j < (numtoks - 1); j++) {
                        copyTokStruct(tst[j], tst[j + 1]);
                        nullTokenStruct(tst[j + 1]);
                    }
                    
                    --numtoks;
                    --i;
                }
            }
        }
        
        return numtoks;
    }


    /**
     * Add the properties of a NormalizedToken object into another NormalizedToken
     * object
     *
     * @param tst1 aaa
     * @param tst2 aaa
     */
    private static void appendTokStruct(TokenStruct tst1, TokenStruct tst2) {
    
        tst1.getInputImage().append(tst2.getInputImage());

        tst1.setClueType1(tst2.getClueType1());
        tst1.setClueType2(tst2.getClueType2());
        tst1.setClueType3(tst2.getClueType3());
        tst1.setClueType4(tst2.getClueType4());
        tst1.setClueType5(tst2.getClueType5());

        tst1.setClueWordId1(tst2.getClueWordId1());
        tst1.setClueWordId2(tst2.getClueWordId2());
        tst1.setClueWordId3(tst2.getClueWordId3());
        tst1.setClueWordId4(tst2.getClueWordId4());
        tst1.setClueWordId5(tst2.getClueWordId5());

        tst1.setBeg(tst2.getBeg());
        tst1.setEnd(tst2.getEnd());
    }


    /**
     * Copy the properties of a NormalizedToken object into another NormalizedToken 
     * object
     * @param tst1 aaa
     * @param tst2 aaa
     */ 
    static void copyTokStruct(TokenStruct tst1, TokenStruct tst2) {

        tst1.setInputImage(tst2.getInputImage());

        tst1.setClueType1(tst2.getClueType1());
        tst1.setClueType2(tst2.getClueType2());
        tst1.setClueType3(tst2.getClueType3());
        tst1.setClueType4(tst2.getClueType4());
        tst1.setClueType5(tst2.getClueType5());
    
        tst1.setClueWordId1(tst2.getClueWordId1());
        tst1.setClueWordId2(tst2.getClueWordId2());
        tst1.setClueWordId3(tst2.getClueWordId3());
        tst1.setClueWordId4(tst2.getClueWordId4());
        tst1.setClueWordId5(tst2.getClueWordId5());
    
        tst1.setBeg(tst2.getBeg()); 
        tst1.setEnd(tst2.getEnd());
    }

    /**
     *
     *
     * @param tst1 aaa
     * @param tst2 aaa
     */
    private static void switchTokStruct(TokenStruct tst1, TokenStruct tst2) {


        String himage = new String(tst1.getInputImage());
        String ht1 = new String(tst1.getClueType1());
        String ht2 = new String(tst1.getClueType2());
        String ht3 = new String(tst1.getClueType3());
        String ht4 = new String(tst1.getClueType4());
        String ht5 = new String(tst1.getClueType5());

        int hi1 = tst1.getClueWordId1();
        int hi2 = tst1.getClueWordId2();
        int hi3 = tst1.getClueWordId3();
        int hi4 = tst1.getClueWordId4();
        int hi5 = tst1.getClueWordId5();
        int hbeg = tst1.getBeg();
        int hend = tst1.getEnd();

        tst1.setInputImage(tst2.getInputImage());
        tst1.setClueType1(tst2.getClueType1());
        tst1.setClueType2(tst2.getClueType2());
        tst1.setClueType3(tst2.getClueType3());
        tst1.setClueType4(tst2.getClueType4());
        tst1.setClueType5(tst2.getClueType5());
        
        
        tst1.setClueWordId1(tst2.getClueWordId1());
        tst1.setClueWordId2(tst2.getClueWordId2());
        tst1.setClueWordId3(tst2.getClueWordId3());
        tst1.setClueWordId4(tst2.getClueWordId4());
        tst1.setClueWordId5(tst2.getClueWordId5());
        tst1.setBeg(tst2.getBeg());
        tst1.setEnd(tst2.getEnd());

        tst2.setInputImage(himage);
        tst2.setClueType1(ht1);
        tst2.setClueType2(ht2);
        tst2.setClueType3(ht3);
        tst2.setClueType4(ht4);
        tst2.setClueType5(ht5);

        tst2.setClueWordId1(hi1);
        tst2.setClueWordId2(hi2);
        tst2.setClueWordId3(hi3);
        tst2.setClueWordId4(hi4);
        tst2.setClueWordId5(hi5);
        tst2.setBeg(hbeg);
        tst2.setEnd(hend);
             
    }



    /**
     *
     *
     * @param tst the input string
     * @param token aaa
     * @return <code>boolean</code>
     */
    private static boolean tokenInStruct(TokenStruct tst, String token) {

        return (EmeUtil.strncmp(tst.getClueType1(), token, 2) == 0
                || (EmeUtil.strncmp(tst.getClueType2(), token, 2) == 0)
                || (EmeUtil.strncmp(tst.getClueType3(), token, 2) == 0)
                || (EmeUtil.strncmp(tst.getClueType4(), token, 2) == 0)
                || (EmeUtil.strncmp(tst.getClueType5(), token, 2) == 0));
    }

    /**
     *
     *
     * @param tst aaa
     */
    private static void nullTokenStruct(TokenStruct tst) {

        String empty = "";
        int zero = 0;
        
        tst.setInputImage(empty);
        
        tst.setClueWordId1(zero);
        tst.setClueType1(empty);
        
        tst.setClueWordId2(zero);
        tst.setClueType2(empty);
        
        tst.setClueWordId3(zero);
        tst.setClueType3(empty);
        
        tst.setClueWordId4(zero);
        tst.setClueType4(empty);
        
        tst.setClueWordId5(zero);
        tst.setClueType5(empty);

        tst.setBeg(zero);
        tst.setEnd(zero);
    }


    /**
     *
     *
     * @param str the input string
     * @param len the size of the string
     * @return <code>boolean</code>
     */
    static boolean firstCharDashFound(StringBuffer str, int len) {

        int i;

        for (i = 0; i < len; i++) {

            if (str.charAt(i) == ' ') {
                continue;
            }

            return (str.charAt(i) == '-' || str.charAt(i) == '�');
        }
        return false;
    }
    /**
     *
     *
     * @param str the input string
     * @return <code>boolean</code>
     */
    static boolean dashFound(StringBuffer str) {

        int i;
        int len = str.length();

        for (i = 0; i < len; i++) {
            if(str.charAt(i) == '-' || str.charAt(i) == '�') {
                return true;
            }
        }
        return false;
    }    
}
