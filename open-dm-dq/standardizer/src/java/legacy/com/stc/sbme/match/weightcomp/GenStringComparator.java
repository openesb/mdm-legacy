/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.EmeUtil;

/**
 * The algorithm comprises the Jaro string comparator plus One additional enhancements
 * which makes use of the information from the prior characteristics.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class GenStringComparator {

    /*   */
    private static final String NULL50 = ReadMatchConstantsValues.NULL50;
    /*   */
    private static int[] indC = {0, 0, 0, 0, 0, 0};


    /**
     * Measure the degree of similarity between recordA and recordB using a generic
     * string comparator algorithm that accounts for accounts for insertions,
     * deletions, transpositions plus three additional enhancements (scanning errors,
     * large strings and linear dependencies of the error rate function of the
     * length)
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @param      mEng  an instance of the SbmeMatchingEngine class
     * @return     a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     */
    static double genStrComparator(String recordA, String recordB, int index, boolean cons, MatchVariables mVar)
    throws SbmeMatchingException {
        
        StringBuffer recAHold = new StringBuffer();
        StringBuffer recBHold = new StringBuffer();
        
        StringBuffer recAFlag = new StringBuffer();
        StringBuffer recBFlag = new StringBuffer();


        StringBuffer recA = new StringBuffer(recordA.trim().toUpperCase());
        StringBuffer recB = new StringBuffer(recordB.trim().toUpperCase());
        StringBuffer sb = null;        
        int lenA;
        int lenB;
        int fixLen;
        char tempHold;

        double weight;
        double numSim;
        int minv;
        int searchRange;
        int lowLim;
        int hiLim;
        int numTrans;
        int numCom;
        int yl1;
        int yiSt;
        int numSimi = 0;
        int i;
        int j;
        int k;

        lenA = recA.length();
        lenB = recB.length();

        // Before going any further, test if we are using a 'nS' and if the record 
        // is not in the list of invalid SSNs (e.g. XXXXXX or 9999999)
        if (cons) {

            // Modify the pattern so that it can handle exactly Length times 
            // any specified constraint, Length being the length of the records A and B
            fixLen = mVar.ssnLength[index];

            // First test if the SSN needs to have a fixed length
            if (fixLen > 0) {
            
                // Make sure that both records have the same axpected length
                if ((lenA != fixLen) || (lenB != fixLen)) {
                    return 0.0;
                }
            }
    
            // Test also which type are the records (numeric or alphanumeric)
            // First, numeric
            if (mVar.recType[index].compareTo("nu") == 0) {

                // Make sure that both records are numerics
                for (i = 0; i < lenA; i++) {
                    if (!Character.isDigit(recA.charAt(i))) {
                        return 0.0;
                    }
                }

                for (i = 0; i < lenB; i++) {
                    if (!Character.isDigit(recB.charAt(i))) {
                        return 0.0f;
                    }
                }

            // First, alphanumeric
            } else if (mVar.recType[index].compareTo("an") == 0) {

                // Make sure that both records are numerics
                for (i = 0; i < lenA; i++) {
                    if (!Character.isLetterOrDigit(recA.charAt(i))) {
                        return 0.0;
                    }
                }

                for (i = 0; i < lenB; i++) {
                    if (!Character.isLetterOrDigit(recB.charAt(i))) {
                        return 0.0;
                    }
                }
            }

            if (mVar.ssnConstraints[index]) {
                sb = new StringBuffer();
                //First extend the pattern to include at least min(A, B) number
                // of identical characters in the pattern
//                mVar.ssnList[index].append("{").append(Math.min(lenA, lenB)).append(",}");
                sb.append(mVar.ssnList[index]);
                sb.append("{").append(Math.min(lenA, lenB)).append(",}");
                Pattern pa = Pattern.compile(sb.toString());                                 
                Matcher matA;
                Matcher matB;


                /* Regular expression that catches empty strings */
                matA = pa.matcher(recA);
                matB = pa.matcher(recB);

                /* test if the record is empty or null */
                if (matA.lookingAt() || matB.lookingAt()) {
                    return 0.0;
                }
            }
        }
        
               
        lenA = recA.length();
        lenB = recB.length();
       
        recAHold.setLength(0);
        recBHold.setLength(0);
        
        // Copy the trimed fields into these 2 variables
        EmeUtil.strcat(recAHold, recA);
        EmeUtil.strcat(recBHold, recB);
        
        // Determine which field is longer
        if (lenA > lenB) {
            searchRange = lenA;
            minv = lenB;
            
        } else {
            searchRange = lenB;
            minv = lenA;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }
        
        recAFlag.setLength(0);
        recBFlag.setLength(0);
        
        // Blank out the flags
        EmeUtil.strncat(recAFlag, NULL50, searchRange);
        EmeUtil.strncat(recBFlag, NULL50, searchRange);
    
        // Is lower-side of the middle of the largest field
//        searchRange = (searchRange / 2) - 1;
        searchRange = (searchRange >> 1) - 1;        
        
        // Looking only within the search range, count and flag the matched pairs.
        numCom = 0;
        // Index of last char in field from B-file
        yl1 = lenB - 1;
        
        // Loop over chars of the field from A-file
        for (i = 0; i < lenA; i++) {
            
            lowLim = (i >= searchRange) ? i - searchRange : 0;           
            hiLim = ((i + searchRange) <= yl1) ? (i + searchRange) : yl1;
            
            tempHold = recAHold.charAt(i);
            
            for (j = lowLim; j <= hiLim; j++) {
                
                if ((recBFlag.charAt(j) != '1')
                && (recBHold.charAt(j) == tempHold)) {

                    recBFlag.setCharAt(j, '1');
                    recAFlag.setCharAt(i, '1');
                    numCom++;
                    break;
                }
            }
        }
        
        // If there are no characters in common, return zero agreement.
        // The definition of 'common' is that the agreeing characters must be
        // within � the length of the shorter string min (s1, s2).
        if (numCom == 0) {
            return 0.0;
        }
        
        // Count the number of transpositions. The definition of 'transposition' is
        // that the character from one string is out of order with the corresponding
        // common character from the other string (in the sens defined above).
        k = 0;
        numTrans = 0;
        
        // Loop over the A-file field chars to search for transpositions
        for (i = 0; i < lenA; i++) {
            
            // If the actual character has some corresponding commons, then
            // loop over the chars of the B-file field.
            if (recAFlag.charAt(i) == '1') {
                
                for (j = k; j < lenB; j++) {
                    
                    // If there is a common between indices (i,j), then break the
                    // inner loop and go for next i and start search from j+1.
                    if (recBFlag.charAt(j) == '1') {
                        
                        k = j + 1;
                        break;
                    }
                }
                
                // If char from A-field has no corresponding common from B-field,
                // increment the number of transposition
                if (recAHold.charAt(i) != recBHold.charAt(j)) {
                    
                    numTrans++;
                }
            }
        }
       
        // 1/2 the transpositions
//        numTrans = numTrans / 2;        
        numTrans = numTrans >> 1;
        
        // First inhancement by McLaughlin (1993). Adjust for similarities in
        // nonmatched characters.
        // Similar characters might occur because of scanning errors ('1' versus 'l')
        // or keypunch ('V' versus 'C'). The #common (in Jaro) gets // increased by
        // 0.3 for each similar character
        numSimi = 0;
        
        // Authorize the use of similar characters weightings
        if (indC[1] == 0) {
            
            // Test if the min(str1, str2) is larger than the number of common
            if (minv > numCom) {
                
                for (i = 0; i < lenA; i++) {
                    
                    // Check if the character does not have a commom and is inRange.
                    // See def, at the end.
                    if ((recAFlag.charAt(i) == ' ') && inRange(recAHold.charAt(i))) {
                        
                        tempHold = recAHold.charAt(i);
                        
                        // Loop aver the corresponding B-field
                        for (j = 0; j < lenB; j++) {
                            
                            // Check also if the character does not have
                            // a commom and is inRange.
                            if ((recBFlag.charAt(j) == ' ') && inRange(recBHold.charAt(j))) {

                                // Test if the pair of chars is in the list of
                                // similar characters defined in array adjwt
                                if (mVar.adjwt[tempHold][recBHold.charAt(j)] > 0) {
                                    
                                    // Increment the number of similar pairs
                                    // Need review
                                    numSimi += mVar.adjwt[tempHold][recBHold.charAt(j)];
                                    
                                    // Flag the B-field. How about the A-field?
                                    recBFlag.setCharAt(j, '2');
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
              
        // Update the number of commons by replacing them with:
        // #common + 0.3 * #similar
        numSim = (numSimi) / 10.0 + numCom;
                
        // Main weight computation.
        // Since the initial formula was: 1/3(#common/length(str1) + #common/
        // length(str2) + 1/2*(#transposition / #common).
        // The new formula (adding similar chars) is:
        // 1/3((#common + 0.3*simi)/length(str1) + (#common + 0.3*simi)/length(str2)
        // + 1/2*(#transposition / #common).
        weight = numSim / (lenA) + numSim / (lenB) 
                 + ((double) (numCom - numTrans)) / ((double) numCom);
          
        
        // Multiply by 1/3
        return weight / 3.0;
        
    }   
    
    // Need to be reviewed
    private static boolean inRange(char c) {
        return ((c > 0)  && (c < 91)
        || (c > 191)  && (c < 256));
    }
    
}
