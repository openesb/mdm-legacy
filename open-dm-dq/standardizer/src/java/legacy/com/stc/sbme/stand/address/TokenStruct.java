/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * 
 * 
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class TokenStruct {

    /* input image */
    private StringBuffer inputImage;
    private int clueWordId1;
    private StringBuffer clueType1;
    private int clueWordId2;
    private StringBuffer clueType2;
    private int clueWordId3;
    private StringBuffer clueType3;
    private int clueWordId4;
    private StringBuffer clueType4;
    private int clueWordId5;
    private StringBuffer clueType5;
    
    /* beginning word position */
    private int beg;
    /* ending word position */
    private int end;

    /**
     * Default constructor
     */
    TokenStruct(String domain) {
        int IMAGE_SIZE = ReadStandConstantsValues.getInstance(domain).getImageSize();
        inputImage = new StringBuffer(IMAGE_SIZE);
        clueType1 = new StringBuffer();
        clueType2 = new StringBuffer();
        clueType3 = new StringBuffer();
        clueType4 = new StringBuffer();
        clueType5 = new StringBuffer();
        
    }

    /**
     *  
     * 
     * @param st a NormalizedToken object
     */
    void createCopy(TokenStruct st) {

        st.setInputImage(inputImage);
        st.setClueWordId1(clueWordId1);
        st.setClueType1(clueType1);        
        st.setClueWordId2(clueWordId2);
        st.setClueType2(clueType2);
        st.setClueWordId3(clueWordId3);
        st.setClueType3(clueType3);
        st.setClueWordId4(clueWordId4);
        st.setClueType4(clueType4);
        st.setClueWordId5(clueWordId5);
        st.setClueType5(clueType5);
        st.setBeg(beg);
        st.setEnd(end);             
    }

    /**
     * @return aaa
     */
    StringBuffer getInputImage() {
        return inputImage;
    }

    /**
     * @return aaa
     */
    int getClueWordId1() {
        return clueWordId1;
    }

    /**
     * @return aaa
     */
    String getClueType1() {
        return clueType1.toString();
    }

    /**
     * @return aaa
     */
    int getClueWordId2() {
        return clueWordId2;
    }
    
    /**
     * @return aaa
     */
    String getClueType2() {
        return clueType2.toString();
    }

    /**
     * @return aaa
     */
    int getClueWordId3() {
        return clueWordId3;
    }

    /**
     * @return aaa
     */
    String getClueType3() {
        return clueType3.toString();
    }

    /**
     * @return aaa
     */
    int getClueWordId4() {
        return clueWordId4;
    }

    /**
     * @return aaa
     */
    String getClueType4() {
        return clueType4.toString();
    }

    /**
     * @return aaa
     */
    int getClueWordId5() {
        return clueWordId5;
    }

    /**
     * @return aaa
     */
    String getClueType5() {
        return clueType5.toString();
    }

    /**
     * @return aaa
     */
    int getBeg() {
        return beg;
    }

    /**
     * @return aaa
     */
    int getEnd() {
        return end;
    }

    /**
     * @param inputImage aaa
     */
    void setInputImage(StringBuffer inputImage) {

        this.inputImage.setLength(0);
        this.inputImage.append(inputImage);
    }

    /**
     * @param inputImage aaa
     */
    void setInputImage(String inputImage) {

        this.inputImage.setLength(0);
        this.inputImage.append(inputImage);
    }
    

    /**
     * @param clueWordId1 aaa
     */
    void setClueWordId1(int clueWordId1) {

        this.clueWordId1 = clueWordId1;
    }

    /**
     * @param clueType1 aaa
     */
    void setClueType1(String clueType1) {

        this.clueType1.setLength(0);
        this.clueType1.append(clueType1);
    }

    /**
     * @param clueType1 aaa
     */
    void setClueType1(StringBuffer clueType1) {

        this.clueType1.setLength(0);
        this.clueType1.append(clueType1);
    }

    /**
     * @param clueWordId2 aaa
     */
    void setClueWordId2(int clueWordId2) {
        this.clueWordId2 = clueWordId2;
    }

    /**
     * @param clueType2 aaa
     */
    void setClueType2(String clueType2) {

        this.clueType2.setLength(0);
        this.clueType2.append(clueType2);
    }

    /**
     * @param clueType2 aaa
     */
    void setClueType2(StringBuffer clueType2) {

        this.clueType2.setLength(0); 
        this.clueType2.append(clueType2);
    }

    /**
     * @param clueWordId3 aaa
     */
    void setClueWordId3(int clueWordId3) {
        this.clueWordId3 = clueWordId3;

    }

    /**
     * @param clueType3 aaa
     */
    void setClueType3(String clueType3) {
        this.clueType3.setLength(0);
        this.clueType3.append(clueType3);
    }

    /**
     * @param clueType3 aaa
     */
    void setClueType3(StringBuffer clueType3) {
        this.clueType3.setLength(0);
        this.clueType3.append(clueType3);
    }

    /**
     * @param clueWordId4 aaa
     */
    void setClueWordId4(int clueWordId4) {
        this.clueWordId4 = clueWordId4;
    }

    /**
     * @param clueType4 aaa
     */
    void setClueType4(String clueType4) {
         this.clueType4.setLength(0);
        this.clueType4.append(clueType4);
    }

    /**
     * @param clueType4 aaa
     */
    void setClueType4(StringBuffer clueType4) {
         this.clueType4.setLength(0);
        this.clueType4.append(clueType4);
    }

    /**
     * @param clueWordId5 aaa
     */
    void setClueWordId5(int clueWordId5) {
        this.clueWordId5 = clueWordId5;
    }

    /**
     * @param clueType5 aaa
     */
    void setClueType5(String clueType5) {
        this.clueType5.setLength(0);
        this.clueType5.append(clueType5);
    }

    /**
     * @param clueType5 aaa
     */
    void setClueType5(StringBuffer clueType5) {
        this.clueType5.setLength(0);
        this.clueType5.append(clueType5);
    }

    /**
     * @param beg aaa
     */
    void setBeg(int beg) {
        this.beg = beg;
    }

    /**
     * @param end aaa
     */
    void setEnd(int end) {
        this.end = end;
    }
}
