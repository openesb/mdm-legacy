/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.emalgorithm;

import com.stc.sbme.api.SbmeMatchingEngine;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.match.weightcomp.MatchConfigFile;
import com.stc.sbme.match.weightcomp.MatchCounts;
import com.stc.sbme.match.weightcomp.MatchVariables;
import com.stc.sbme.util.EmeUtil;

/**
 *
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PatternsCounter {
    
    // read constants from ReadMatchConstantsValues class
    /* */
    private static final int N_FIELDS = ReadMatchConstantsValues.getNFields();
    /* */
    private static final String NULL50 = ReadMatchConstantsValues.NULL50;
    /* */
    private static final String ZERO50 = ReadMatchConstantsValues.ZERO50;
    /* */
    private int year;
    /* */
    private int numberFields;
    /* */
    private static int pat;
    /* */
    private double cmpVal;
    
    /**
     * Define a factory method that return an instance of the class
     * @return a EMAlgorithm object
     */
    public static PatternsCounter getInstance() {
        return new PatternsCounter();
    }
    
    
    /**
     *
     *
     * @param matchFieldsIDs the fields' keys
     * @param candRecArrayVals the array of candidate records
     * @param refRecArrayVals the array of reference records
     * @param mEng an instance of the SbmeMatchingEngine class
     */
    public void getPatternFrequencies(String[] matchFieldsIDs, String[][] candRecArrayVals,
    String[][] refRecArrayVals, SbmeMatchingEngine mEng) {
        
        int i = 0;
        int j = 0;
        int k = 0;
        int l = 0;
        int flag = 0;
        int pato;
        int inth;
        int n = 0;
        
        int fieldsNumber;
        int blockArecNumber;
        int blockBrecNumber;
        
        MatchVariables matchVar = mEng.getMatchVar(mEng);
        MatchCounts matchC = mEng.getMatchC(mEng);
        MatchConfigFile mConfig = mEng.getMatchConfig(mEng);
        
        /* Call the data read from the configuration file */
        numberFields = matchC.getNumFields();
        matchVar.jIndex = mConfig.getFieldIndices(matchFieldsIDs, mEng);
        /*
         * Get the number of matching records and associated fields
         */
        fieldsNumber = matchFieldsIDs.length;
        blockArecNumber = candRecArrayVals.length;
        blockBrecNumber = refRecArrayVals.length;
        
        /* Define a variable that holds the fields of candidate records */
        String[] candRecVals = new String[fieldsNumber];
        
        /* Define a variable that holds the fields of reference records */
        String[] refRecVals = new String[fieldsNumber];
        
        /* Define a variable that holds the field's names */
        String fieldName;
        
        /* Initial value */
        matchVar.ibaso[(numberFields - 1)] = 1;
        
        for (i = 0; i < (fieldsNumber - 1); i++) {
            
            matchVar.ibaso[fieldsNumber - i - 2] = matchVar.ibaso[fieldsNumber - i - 1] 
                    * matchVar.fieldPatt[matchVar.jIndex[fieldsNumber - i - 1]];
        }
        
        
        for (i = 0; i < blockArecNumber; i++) {
            
            // Array of fields of record i in the candidate block
            candRecVals = candRecArrayVals[i];
            
            // Loop over a corresponding block in the corresponding file 'sortb.dat'.
            for (k = 0; k < blockBrecNumber; k++) {
                
                pat = 0;
                pato = 0;
                
                
                // Array of fields of record k in the candidate block
                refRecVals = refRecArrayVals[k];
                
                // Loop over all the matching fields in the actual record from
                // 'sorta.dat'
                for (j = 0; j < fieldsNumber; j++) {
                    
                                    /* field's name */
                    fieldName = matchFieldsIDs[j];
                    
                    pato = pat;
                    
                    //pat is updated
                    compareWeight(candRecVals[j], refRecVals[j], matchVar,
                    matchVar.jIndex[j], k);
                    
                    // collapse as necessary
                    if (matchVar.fieldPatt[matchVar.jIndex[j]] == 2) {
                        
                        if ((pat - pato) == 2) {
                            
                            pat = pato + matchVar.ibaso[matchVar.jIndex[j]];
                            
                        } else {
                            pat = pato;   // blank considered same as disagree
                        }
                        
                    } else {
                        pat = pato + (pat - pato) * matchVar.ibaso[matchVar.jIndex[j]];
                    }
                }
                matchVar.freqPatt[pat] += 1;
            } /*for k*/
        }
    }
    
    /**
     *
     *
     * @param fieldJFromA
     * @param fieldJFromB
     * @param matchVar
     * @param j index of the jth matching field
     * @param k index of the kth record in the B block
     */
    private void compareWeight(String fieldJFromA, String fieldJFromB, MatchVariables matchVar, 
                               int j, int k) {
        
        int num1 = 0;
        int num2 = 0;
        int min = 0;
        int diff = 0;
        double hf1 = 0.0;
        double hf2 = 0.0;
        
        // Length of fields
        int fieldALength = fieldJFromA.length();
        int fieldBLength = fieldJFromB.length();
        
        if ((EmeUtil.strncmp(fieldJFromB, NULL50, fieldBLength) == 0) 
        || (EmeUtil.strncmp(fieldJFromB, ZERO50, fieldBLength) == 0) 
        || (EmeUtil.strncmp(fieldJFromA, NULL50, fieldALength) == 0) 
        || (EmeUtil.strncmp(fieldJFromA, ZERO50, fieldALength) == 0)) {
            
            pat++;
            
        } else if (EmeUtil.strncmp(fieldJFromB, fieldJFromA, matchVar.lengthF[j]) == 0) {
            
            pat += 2;
            
        } else if (matchVar.comparatorType[j].charAt(0) == 'c') {
            // end exact comparison. nonnull data comparison loop
            
            // update pattern
            if (EmeUtil.strncmp(fieldJFromA, fieldJFromB, matchVar.lengthF[j]) == 0) {
                
                pat += 2;
            }
            
        } else if ((matchVar.comparatorType[j].charAt(0) == 'u')
        && (matchVar.comparatorType[j].charAt(1) == 'o')) { // end comp_type 'c'
            
            cmpVal = compareWstr(fieldJFromA, fieldJFromB, 0.2, 0.3, 0.4, 0.5, 0);
            
            hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) 
                 * (1 - cmpVal) * 4.5);
            
            if (hf1 > 0) {
                pat += 2;
            }
            
        } else if ((matchVar.comparatorType[j].charAt(0) == 'u')
        && (matchVar.comparatorType[j].charAt(1) == 'f')) { // end comp_type 'uo'
            
            cmpVal = compareWstr(fieldJFromA, fieldJFromB, 0.2, 0.3, 0.4, 0.5, 0);
            
            if (cmpVal > 0.92) {
                pat += 2;
                
            } else if ((cmpVal > 0.75) && (cmpVal <= 0.92)) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) * (1 - cmpVal)
                     * 1.5);
                
                if (hf1 > 0) {
                    pat += 2;
                }
                
            } else if (cmpVal <= 0.75) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j])
                * (1 - cmpVal) * 3.0);
                
                if (hf1 > 0) {
                    pat += 2;
                }
            }
            
        } else if ((matchVar.comparatorType[j].charAt(0) == 'u')
        && (matchVar.comparatorType[j].charAt(1) == 'l')) { /*end comp_type 'uf'*/
            
            cmpVal = compareWstr(fieldJFromA, fieldJFromB, 0.2, 0.3, 0.4, 0.5, 0);
            
            if (cmpVal > 0.96) {
                pat += 2;
                
            } else if ((cmpVal > 0.88) && (cmpVal <= 0.96)) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) 
                     * (1 - cmpVal) * 3.0);
                
                if (hf1 > 0) {
                    pat += 2;
                }
                
            } else if (cmpVal <= 0.88) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j]  - matchVar.agrWeight[j]) 
                     * (1 - cmpVal) * 4.5);
                
                if (hf1 > 0) {
                    pat += 2;
                }
            }
        } else if ((matchVar.comparatorType[j].charAt(0) == 'u')
        && (matchVar.comparatorType[j].charAt(1) == 'n')) { // end comp_type 'ul'
            
            cmpVal = compareWstr(fieldJFromA, fieldJFromB, 0.2, 0.3, 0.4, 0.5, 1);
            
            if (cmpVal > 0.98) {
                pat += 2;
                
            } else if ((cmpVal > 0.83) && (cmpVal <= 0.98)) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) 
                     * (1 - cmpVal) * 4.5);
                
                if (hf1 > 0) {
                    pat += 2;
                }
                
            } else if (cmpVal <= 0.83) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) 
                     * (1 - cmpVal) * 7.5);
                
                if (hf1 > 0) {
                    pat += 2;
                }
            }
            
        } else if (matchVar.comparatorType[j].charAt(0) == 'u') { // end comp_type 'un'
            
            cmpVal = compareWstr(fieldJFromA, fieldJFromB, 0.2, 0.3, 0.4, 0.5, 0);
            
            if (cmpVal > 0.95) {
                pat += 2;
                
            } else if ((cmpVal > 0.90) && (cmpVal <= 0.95)) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) 
                     * (1 - cmpVal) * 1.0);
                
                if (hf1 > 0) {
                    pat += 2;
                }
                
            } else if (cmpVal <= 0.90) {
                
                hf1 = (matchVar.agrWeight[j] - (matchVar.agrWeight[j] - matchVar.disWeight[j]) 
                     * (1 - cmpVal) * 1.5);
                
                
                if (hf1 > 0) {
                    pat += 2;
                }
            }
            
        } else if ((matchVar.comparatorType[j].charAt(0) == 'p')
        || (matchVar.comparatorType[j].charAt(0) == 'y')) { /*end comp_type 'u'*/
            
            num1 = Integer.parseInt(fieldJFromA);
            num2 = Integer.parseInt(fieldJFromB);
            
            if (matchVar.comparatorType[j].charAt(0) == 'y') {
                
                num1 = year - num1;
                num2 = year - num2;
                
                if (num1 <= 0) {
                num1 = 1;
                }
                
                if (num2 <= 0) {
                num2 = 1;
                }
            }
            
            if (num1 > num2) {
                
                min = num2;
                diff = num1 - num2;
                
            } else {
                min = num1;
                diff = num2 - num1;
            }
            
            if (min < 1) {
                min = 1;
            }
            
            if ((num1 < 20) && (diff < 2)) {
                pat += 2;
                
            } else if (!(num1 < 20) && (diff < 3)) {
                pat += 2;
                
            } else {
                
                hf1 = (min);
                
                if (num1 >  num2) {
                    hf2 = ((num1 - num2));
                    
                } else {
                    hf2 = ((num2 - num1));
                }
                
                hf2 = matchVar.agrWeight[j] - ((matchVar.agrWeight[j]
                - matchVar.disWeight[j]) * hf2) / (0.15 * hf1);
                
                if (hf2 < matchVar.disWeight[j]) {
                    
                    hf2 = matchVar.disWeight[j];
                }
                
                if (hf2 > 0) {
                    pat += 2;
                }
            }
        }
    } /*compareWeight*/
    
    
    /**
     *
     *
     * @param fieldA
     * @param fieldB
     * @param scmet1
     * @param scmet2
     * @param scmet3
     * @param scmet4
     * @param indC
     */
    static double compareWstr(String fieldA, String fieldB, double scmet1,
    double scmet2, double scmet3, double scmet4, int indC) {
        
        StringBuffer fieldAHold = new StringBuffer();
        StringBuffer fieldBHold = new StringBuffer();
        
        StringBuffer fieldAFlag = new StringBuffer();
        StringBuffer fieldBFlag = new StringBuffer();
        
        StringBuffer tokenA = new StringBuffer(fieldA);
        StringBuffer tokenB = new StringBuffer(fieldB);
        
        double weight;
        int minV;
        int searchRange;
        int lowLim;
        int highLim;
        int nTrans;
        int numCom;
        int i;
        int j;
        int k;
        int fieldALength = tokenA.length();
        int fieldBLength = tokenB.length();
        
        weight = 0.0;
        fieldAHold.setLength(0);
        fieldBHold.setLength(0);
        
        k = fieldALength - 1;
        
        for (j = 0; ((tokenA.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        
        for (i = k; ((tokenA.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        fieldALength = i + 1 - j;
        EmeUtil.strncat(fieldAHold, tokenA, j, fieldALength);
        
        k = fieldBLength - 1;
        
        for (j = 0; ((tokenB.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        
        for (i = k; ((tokenB.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        fieldBLength = i + 1 - j;
        EmeUtil.strncat(fieldBHold, tokenB, j, fieldBLength);
        
        if ((fieldALength < 4) && (fieldBLength < 4)) {
            return 0.0;
        }
        
        if (fieldALength > fieldBLength) {
            
            searchRange = fieldALength;
            minV = fieldBLength;
            
        } else {
            searchRange = fieldBLength;
            minV = fieldALength;
        }
        
        fieldAFlag = EmeUtil.strcpy(fieldAFlag, "");
        fieldBFlag = EmeUtil.strcpy(fieldBFlag, "");
        
        EmeUtil.strncat(fieldAFlag, NULL50, searchRange);
        EmeUtil.strncat(fieldBFlag, NULL50, searchRange);
        
        searchRange = (searchRange / 2) - 1;
        
        for (i = 0; i < fieldALength; i++) {
            
            lowLim = i - searchRange;
            
            if (lowLim < 0) {
                
                lowLim = 0;
            }
            
            highLim = i + searchRange;
            
            if (highLim > (fieldBLength - 1)) {
                
                highLim = fieldBLength - 1;
            }
            
            for (j = lowLim; j <= highLim; j++) {
                
                if ((fieldBFlag.charAt(j) != '1')
                && (fieldBHold.charAt(j) == fieldAHold.charAt(i))) {
                    
                    fieldBFlag.setCharAt(j, '1');
                    fieldAFlag.setCharAt(i, '1');
                    
                    break;
                }
            }
        }
        
        j = 0;
        k = 0;
        nTrans = 0;
        numCom = 0;
        
        for (i = 0; i < fieldALength; i++) {
            
            if (fieldAFlag.charAt(i) == '1') {
                
                for (j = k; j < fieldBLength; j++) {
                    
                    if (fieldBFlag.charAt(j) == '1') {
                        
                        k = j + 1;  // successively increment
                        break;
                    }
                }
                
                numCom++;
                
                if (fieldAHold.charAt(i) != fieldBHold.charAt(j)) {
                    nTrans++;
                }
            }
        }
        
        nTrans = nTrans / 2;
        
        if (numCom == 0) {
            return 0.0;
            
        } else {
            
            weight = ((double) numCom) / ((double) fieldALength)
                    + ((double) numCom) / ((double) fieldBLength)
                    + ((double) (numCom - nTrans)) / ((double) numCom);
            
            weight = weight / (3.0);
        }
        
        if ((indC == 0) && (weight > 0.7)) {
            
            if (minV > 4) {
                minV = 4;
            }
            
            for (i = 0; i < minV; i++) {
                
                if (fieldAHold.charAt(i) != fieldBHold.charAt(i)) {
                    break;
                }
            }
            
            if (i == 4) {
                weight += scmet4 * (1.0 - weight);
                
            } else if (i == 3) {
                
                weight += scmet3 * (1.0 - weight);
                
            } else if (i == 2) {
                weight += scmet2 * (1.0 - weight);
                
            } else if (i == 1) {
                weight += scmet1 * (1.0 - weight);
                
            }
        }
        return weight;
    }
}
