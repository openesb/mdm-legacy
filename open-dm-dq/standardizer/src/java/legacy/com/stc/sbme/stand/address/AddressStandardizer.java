/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeAddress;
import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandRecordFactory;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * Main class that processes any request for normalizing or standardizing 
 * a generic address presented in the form of an object or a string
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.2 $
 */
public class AddressStandardizer {

    /*
     * An instance of the class that hold all the variables
     * in the standardization engine
     */
    private AddressNormalizer addrNorm;

    /*
     * An instance of the class that hold all the variables
     * in the standardization engine
     */
    private AddressZipCode fZip;
    private AddressStateCode fState;

    /*
     * An instance of the class that hold all the variables
     * in the standardization engine
     */
    private AddressParser addrParse;

    /*
     * An instance of the class that hold all the variables
     * in the standardization engine
     */
    private AddressPatternsFinder addrPatt;

    /*
     * An instance of the class that hold all the variables
     * in the standardization engine
     */
    private AddressStandBuilder addrBuild;
    
    /*
     * Set true to embed pattern trace into return value
     */
    private boolean enablePatternTrace = true;
    
    private final AddressPatternsTable addrPattTable;
    
    private final String domain;
    
    private final AddressStandVariables sVar;
    
    private final AddressTablesLoader aLoad;
    
    /**
     * the default constructor
     */
    public AddressStandardizer(AddressStandVariables vars, AddressTablesLoader tblLoader, String domain) {

        addrNorm = new AddressNormalizer(domain);
        addrParse = new AddressParser(domain);
        addrPatt = new AddressPatternsFinder(domain);
        addrBuild = new AddressStandBuilder(domain);
        
        fZip = AddressZipCode.getInstance();
        fState = AddressStateCode.getInstance();
        addrPattTable = AddressPatternsTable.getInstance(domain);
        this.domain = domain;
        aLoad = tblLoader;
        sVar = vars;
    }


    /**
     * This method reads in a person name object, search for the standard
     * synonyms of its associated fields in some domain-dependent look up
     * tables, then returns a normalized person name object, without changing
     * the number of fields
     *
     * @param standObj an instance of the SbmeStandRecord class
     * @return a SbmeStandRecord object
     * @throws SbmeStandardizationException exception
     */
    public SbmeStandRecord normalize(SbmeStandRecord standObj)
        throws SbmeStandardizationException {

       return null;
    }

    /**
     * This method reads in a array of person name objects, search for each
     * of them, the standard synonyms of its associated fields in the look up
     * tables, then returns a normalized person name array of objects,
     * without changing the number of fields
     *
     * @param addressList an array of strings
     * @throws SbmeStandardizationException exception
     */
    public void normalize(ArrayList addressList) 
        throws SbmeStandardizationException {
        
    }

    /**
     *
     * This method reads in a person name string, parse it, search for 
     * the types and standard formats of all the tokens by using  in the
     * look up tables, finally cearch for the best pattern for the record, 
     * and returns standardized person name record object.
     *
     * @param record an input string
     * @return an array of SbmeStandRecord objects
     * @throws SbmeStandardizationException exception
     * @throws SbmeMatchEngineException exception
     */
    public SbmeStandRecord[] standardize(String record) 
        throws SbmeStandardizationException, SbmeMatchEngineException {
// RRC
int myPatternCount = 0;
OutputPattern[] myPatterns = null;

        int WORDS = ReadStandConstantsValues.getInstance(domain).getMaxWords();
        int MAX_OUTPUT_PATTERNS = ReadStandConstantsValues.getInstance(domain).getMaxOutputPatterns();
        int MAX_INPUT_TOKENS = ReadStandConstantsValues.getInstance(domain).getMaxInputTokens();    
            
        int i;
        int j;
        int ik;
        int k;       
        int maxLineNums;
        int totNumPats;
        int totNumFields;        
        int fLen;
        int index;
        ArrayList[] subFieldList;                
        int[] numLines = new int[1];
        
        int actFieldsSize;
        int numFields;
        int fieldsDiff;
        int fieldsBef;        
        int minSize;        
//        InputPattern pa = InputPattern.compile("\\S");
//        Matcher mat;
        
        /* Number of addresses in the string */
        int numberOfAddr;
        /* Number of patterns */
        int numPats;
        
        /* Number of fields per address */
        int numberOfKeys;
        
        /* Number of fields associated with an address per record */
        int numberOfFieldKeys[] = new int[3];
        
        /* Holds the tokens obtained from parsing the string */
        ArrayList fieldList = new ArrayList();
        
        /* Hold the outputpatterns */
        OutputPattern[] returnPatt = new OutputPattern[MAX_OUTPUT_PATTERNS];
        OutputPattern[] tempReturnPatt = new OutputPattern[MAX_OUTPUT_PATTERNS];         
 
        /* Hold output pattern line numbers */
        int[] lineNums = new int[MAX_INPUT_TOKENS * 3/2];
        
        // Instantiate the array of AddressOutputPattern objects 
        for (i = 0; i < MAX_OUTPUT_PATTERNS; i++) {
            returnPatt[i] = new OutputPattern(domain);
            tempReturnPatt[i] = new OutputPattern(domain);
        }

        /*
         * Defines the standardized address to be returned 
         */
        StandAddress standAddr;              

        /* define an array of strings to hold the fields' values */
        StringBuffer[][] orderedKeys = new StringBuffer[1][WORDS * 2];
        
        /* define an array of strings to hold the fields' values */
        StringBuffer[][] orderedValues = new StringBuffer[1][WORDS * 2];
        
       /* Define instances of the SbmeStandRecord class */
        SbmeStandRecord[] standObj = new SbmeStandRecord[1];
        
       /* Define the value of InUsageFlag we need to custmize usf = ' '*/
        sVar.usf = ' ';
       
       /* Define the value of InUsageFlag we need to custmize usf = ' '*/
        sVar.abbrevOpt = 'u';
  
        /* Regular expression that catches empty strings */
//        mat = pa.matcher(record);
        // Remove any dots or commas
        record = record.replaceAll("[.]+", "");
        record = record.replaceAll("[,]+", " "); 
        record = record.replaceAll("[ ]+[|]", "|");
        record = record.replaceAll("[|][ ]+", "|");        
        /* Remove any pipes at the border. Replace successive ones with only one */
        if (record.indexOf("||") != -1) {                              
              record = record.replaceAll("[|]+", "|");
        }          

        /* test if the record is empty or null */
        if ((record == null) || (record.trim().length() == 0) || record.compareTo("|") == 0 
        /*|| !(mat.lookingAt())*/) {
    
           /*
            * Create an instance of the PersonNameParser through
            * the call of the StandardizationEngineWrapper instance
            */
            standObj[0] = SbmeStandRecordFactory.getInstance("Address");
            return standObj;
        }
       
       /* 
        * Call method findPostalState() to find the state code inside the string
        * and to apply on it any rules
        */
//       StringBuffer postalState = fState.findStateCode(record, domain, sVar);
        StringBuffer postalState = new StringBuffer("  ");
       /* 
        * Call method findZipCode() to find the zip code inside the string
        * and to apply on it any rules
        */
//       StringBuffer zipCode = fZip.findZipCode(record, domain);
        StringBuffer zipCode = new StringBuffer("     ");
       
        sVar.inUsageFlag = Character.toUpperCase(sVar.usf);
        // Make sure that the number of blocks is less than the maximum alowed.
        // If not, then trim it.
        StringTokenizer st = new StringTokenizer(record, " |");
        index = 0;
        minSize = Math.min(WORDS, MAX_INPUT_TOKENS);
        minSize = Math.min(minSize, MAX_OUTPUT_PATTERNS);
            
        if (st.countTokens() > minSize) {
            for (ik = 0; ik < minSize; ik++) {
                index += st.nextToken().length();
                while (record.charAt(index) == ' ' || record.charAt(index) == '|') {
                    index++;
                }
            }
            record = record.substring(0, index);
        }     
        /*
         * Start first by parsing the string into tokens. We input a string,
         * and return an array of strings 'fieldList' which hold the fields.
         */
        fieldList.addAll(Arrays.asList(addrParse.performParsing(record, sVar, lineNums, numLines, domain).toArray()));

        subFieldList = new ArrayList[numLines[0]];
        // Initialize tempWordArray
        for (k = 0; k < fieldList.size(); k++) {
            sVar.tempWordArray[k] = new StringBuffer();
        }

        if (numLines[0] > 1) {
                    
            // Calculate the number of address lines
            ik = 1;
            fLen = fieldList.size();
            k = 0;
            subFieldList[0] = new ArrayList();
            subFieldList[0].add(fieldList.get(0));
                
            for (k = 0; k < numLines[0]; k++) {
          
                while (lineNums[ik] == lineNums[ik - 1] && (ik < fLen)) {                   

                        subFieldList[k].add(fieldList.get(ik)); 
                        ik++;                                
                } 
                if (ik == fieldList.size()) {
                    numLines[0] = k + 1;
                    break;                  
                }
                        
                if (k < numLines[0] - 1) {                      
                    subFieldList[k + 1] = new ArrayList();
                    subFieldList[k + 1].add(fieldList.get(ik));
                    ik++;
                }
            }
                    
            totNumPats = 0; 
            totNumFields = 0;
            numFields = 0;  
            fieldsDiff = 0;
            
            // Initiate the tokenStruct
            for (k = 0; k < fieldList.size(); k++) { 
                sVar.tempTokensSt[k] = new TokenStruct(domain);
            }
            
            for (k = 0; k < numLines[0]; k++) {   
                 
                fieldsBef = subFieldList[k].size();
                                                           
                /*
                 * We then normalize the different fields. We send in the parsed
                 * fields and return back the normalized ones. We keep the same
                 * number of fields, but we replace them with their normalized forms
                 * if found, or as an empty string if the field is to be removed. Also
                 * we define the fieldType inside this method.
                 *
                 * D.C. The comment above is not correct.  For example, "Business Park"
                 * goes from being two tokens to just one token.
                 *
                 */                    
                subFieldList[k] = addrNorm.performStringNormalization(subFieldList[k], sVar, aLoad, domain,
                                                                   postalState, zipCode, lineNums);      

                fieldsDiff +=   fieldsBef - subFieldList[k].size();
                actFieldsSize = subFieldList[k].size();
                            
                // Search for the best patterns
                numPats = addrPatt.getBestPatterns(returnPatt, subFieldList[k], sVar, postalState, zipCode, lineNums, domain);


                updateReturnPatt(tempReturnPatt, returnPatt, numPats, totNumPats, numFields, k);
  
                numFields += actFieldsSize;                                
                totNumPats+= numPats;
                    
                updateTokenStruct(sVar, subFieldList[k], fieldList, numLines[0] - k, fieldsDiff, numFields);
            }
        


            // Use the addressOutPatterns table to reajust the output patterns
            correctOutputPatterns(tempReturnPatt, totNumPats);
       
            /* If no pattern is found
            /*
             * Finally, identify the best pattern (the order in the arangement of the
             * fields) for the record, and also the fields to be returned.
             */
            standAddr = addrBuild.buildStandAddress(sVar, tempReturnPatt, totNumPats, postalState, zipCode,
                                                       aLoad);
            // Add the different patterns
         
// RRC
myPatterns = tempReturnPatt;
myPatternCount = totNumPats;
        } else {

        
            /*
             * We then normalize the different fields. We send in the parsed
             * fields and return back the normalized ones. We keep the same
             * number of fields, but we replace them with their normalized forms
             * if found, or as an empty string if the field is to be removed. Also
             * we define the fieldType inside this method.
             */
            fieldList = addrNorm.performStringNormalization(fieldList, sVar, aLoad, domain, postalState,
                                                            zipCode, lineNums);
            
            /*
             * Finally, identify the best pattern (the order in the arangement of the
             * fields) for the record, and also the fields to be returned.
             */
            numPats = addrPatt.getBestPatterns(returnPatt, fieldList, sVar, postalState, zipCode, lineNums, domain);
            if (domain.compareTo("UK") == 0) {          
            replaceAllEIsWithNAs(returnPatt);
            }
            /*
             * Finally, identify the best pattern (the order in the arangement of the
             * fields) for the record, and also the fields to be returned.
             */
            standAddr = addrBuild.buildStandAddress(sVar, returnPatt, numPats, postalState, zipCode, aLoad);
// RRC
myPatterns = returnPatt;
myPatternCount = numPats;
        }

       /*
        * Create an instance of the PersonNameParser through
        * the call of the StandardizationEngineWrapper instance
        */
        standObj[0] = SbmeStandRecordFactory.getInstance("Address");

// RRC
OutputPattern[] outputPatterns = new OutputPattern[myPatternCount];
for (int idx = 0; idx < outputPatterns.length; idx++) {
	outputPatterns[idx] = myPatterns[idx];
}
((SbmeAddress)standObj[0]).setOutputPatterns(outputPatterns);
                
        
        /*
         * Copy the address fields into the orderedKeys and orderedValues variables
         */
        numberOfKeys = PopulateStandRecords.copyFields(standAddr, orderedKeys, orderedValues);
        
        for (j = 0; j < numberOfKeys; j++) {
        
            standObj[0].setValue(orderedKeys[0][j].toString(),
                                 orderedValues[0][j].toString());
        }

        return standObj;
    }


    /**
     *
     * This method reads in an array of person names' strings, parse them,
     * search for the types and standard formats of all the tokens within each
     * string using the look up tables, and finally, it searches for the best
     * pattern for each record. Returns an 2D array of standardized person names
     * objects. One dimension for one record with multiple people.
     *
     * @param addressList an array of input strings
     * @return a 2D array of SbmeStandRecord objects
     * @throws SbmeStandardizationException exception
     * @throws SbmeMatchEngineException exception
     */
    public SbmeStandRecord[][] standardize(ArrayList addressList)
        throws SbmeStandardizationException, SbmeMatchEngineException {
                        
        int WORDS = ReadStandConstantsValues.getInstance(domain).getMaxWords();
        int MAX_OUTPUT_PATTERNS = ReadStandConstantsValues.getInstance(domain).getMaxOutputPatterns();
        int MAX_INPUT_TOKENS = ReadStandConstantsValues.getInstance(domain).getMaxInputTokens();      
            
        int i;
        int j;
        int k;
        int ik;
        int maxLineNums;
        int totNumPats;
        int totNumFields;        
        int fLen;
        int index;
        int[] numLines = new int[1];
        ArrayList[] subFieldList;
        
//        InputPattern pa = InputPattern.compile("\\S");
//        Matcher mat;


        /* Number of record submitted for standardization */
        int numberOfRec = addressList.size();
        
        /* Number of patterns */
        int numPats;
        
        int actFieldsSize;
        int numFields;
        int fieldsDiff;
        int fieldsBef;
        int minSize;
        
        /* Temporarily hold the array of input records */
        String[] record = new String[numberOfRec];
        
        /* Number of fields per address */
        int[] numberOfKeys = new int[numberOfRec];
        
        /* Number of fields associated with an address per record */
        int numberOfFieldKeys[][] = new int[numberOfRec][1];
 
        /* Holds the tokens obtained from parsing the string */
        ArrayList[] fieldList = new ArrayList[numberOfRec];
        
   
        /* Hold the outputpatterns */
        OutputPattern[][] returnPatt = new OutputPattern[numberOfRec][MAX_OUTPUT_PATTERNS];
        OutputPattern[] tempReturnPatt = new OutputPattern[MAX_OUTPUT_PATTERNS]; 

        /* Hold output pattern line numbers */
        int[] lineNums = new int[MAX_INPUT_TOKENS * 3/2];
        
        // Instantiate the array of AddressOutputPattern objects 
        for (j = 0; j < MAX_OUTPUT_PATTERNS; j++) {

            for (i = 0; i < numberOfRec; i++) {         
                returnPatt[i][j] = new OutputPattern(domain);
            }           
            tempReturnPatt[j] = new OutputPattern(domain); 
        }
 
        for (i = 0; i < numberOfRec; i++) {
            fieldList[i] = new ArrayList(); 
        }
 
        /*
         * Defines the standardized address to be returned 
         */
        StandAddress[] standAddr = new StandAddress[numberOfRec];

        /* define an array of strings to hold the fields' types */
        StringBuffer[][][] orderedKeys = new StringBuffer[numberOfRec][1][WORDS * 2];
        
        /* define an array of strings to hold the fields' types */
        StringBuffer[][][] orderedValues = new StringBuffer[numberOfRec][1][WORDS * 2];
        
        
       /* Define instances of the SbmeStandRecord class */
        SbmeStandRecord[][] standObj = new SbmeStandRecord[numberOfRec][1];
        
       /* Define the value of InUsageFlag we need to custmize usf = ' '*/
       sVar.usf = ' ';
       
       /* Define the value of InUsageFlag we need to custmize usf = ' '*/
       sVar.abbrevOpt = 'u';
       
       /* 
        * Call method findPostalState() to find the state code inside the string
        * and to apply on it any rules
        */
//       StringBuffer postalState = fState.findStateCode(record, domain, sVar);
       StringBuffer postalState = new StringBuffer("  ");
       /* 
        * Call method findZipCode() to find the zip code inside the string
        * and to apply on it any rules
        */
//       StringBuffer zipCode = fZip.findZipCode(record, domain);
       StringBuffer zipCode = new StringBuffer("     ");
       
       sVar.inUsageFlag = Character.toUpperCase(sVar.usf);
        // Related to the max number of blocks
       minSize = Math.min(WORDS, MAX_INPUT_TOKENS);
       minSize = Math.min(minSize, MAX_OUTPUT_PATTERNS);

        for (i = 0; i < numberOfRec; i++) {
        
            /* Read the objects from the ArrayList */
            record[i] = (String) addressList.get(i);
            // Remove any dots or commas
            record[i] = record[i].replaceAll("[.]+", "");
            record[i] = record[i].replaceAll("[,]+", " "); 
            record[i] = record[i].replaceAll("[ ]+[|]", "|");
            record[i] = record[i].replaceAll("[|][ ]+", "|");
            /* Remove any pipes at the border. Replace successive ones with only one */
            if (record[i].indexOf("||") != -1) {                              
                record[i] = record[i].replaceAll("[|]+", "|");
            }            
            // Make sure that the number of blocks is less than the maximum alowed.
            // If not, then trim it.
            StringTokenizer st = new StringTokenizer(record[i], " |");
            index = 0;
            
            if (st.countTokens() > minSize) {
                for (ik = 0; ik < minSize; ik++) {
                    index += st.nextToken().length();
                    while (record[i].charAt(index) == ' ' || record[i].charAt(index) == '|') {
                        index++;
                    }
                }
                record[i] = record[i].substring(0, index);
            }

            /* Regular expression that catches empty strings */
//            mat = pa.matcher(record[i]);
            
            /* test if the record is empty or null */
            if ((record[i] == null) || (record[i].trim().length() == 0) || record[i].compareTo("|") == 0
            /*|| !(mat.lookingAt())*/) {
                
               /*
                * Create an instance of the PersonNameParser through
                * the call of the StandardizationEngineWrapper instance
                */
                standObj[i][0] = SbmeStandRecordFactory.getInstance("Address");
                
                // Jump to the next index 
                continue;
            }

            /*
             * Start first by parsing the string into tokens. We input a string,
             * and return an array of strings 'fieldList' which hold the fields.
             */
            fieldList[i].addAll(Arrays.asList(addrParse.performParsing(record[i], sVar, lineNums, numLines, domain).toArray()));

            subFieldList = new ArrayList[numLines[0]];
            // Initialize tempWordArray
            for (k = 0; k < fieldList[i].size(); k++) {
                sVar.tempWordArray[k] = new StringBuffer();
            }

            if (numLines[0] > 1) {
                        
                // Calculate the number of address lines
                ik = 1;
                fLen = fieldList[i].size();
                k = 0;
                subFieldList[0] = new ArrayList();
                subFieldList[0].add(fieldList[i].get(0));
                    
                for (k = 0; k < numLines[0]; k++) {
              
                    while (lineNums[ik] == lineNums[ik - 1] && (ik < fLen)) {                   

                            subFieldList[k].add(fieldList[i].get(ik)); 
                            ik++;                                
                    } 
                            
                    if (ik == fieldList[i].size()) {
                        numLines[0] = k + 1;
                        break;                  
                    }
                    if (k < numLines[0] - 1) {                      
                        subFieldList[k + 1] = new ArrayList();
                        subFieldList[k + 1].add(fieldList[i].get(ik));
                        ik++;
                    }
                }
                        
                totNumPats = 0; 
                totNumFields = 0;
                numFields = 0;  
                fieldsDiff = 0;
                
                // Initiate the tokenStruct
                for (k = 0; k < fieldList[i].size(); k++) { 
                    sVar.tempTokensSt[k] = new TokenStruct(domain);
                }
                
                for (k = 0; k < numLines[0]; k++) {   
                     
                    fieldsBef = subFieldList[k].size();
                                                               
                    /*
                     * We then normalize the different fields. We send in the parsed
                     * fields and return back the normalized ones. We keep the same
                     * number of fields, but we replace them with their normalized forms
                     * if found, or as an empty string if the field is to be removed. Also
                     * we define the fieldType inside this method.
                     *
                     * D.C. The comment above is not correct.  For example, "Business Park"
                     * goes from being two tokens to just one token.
                     *
                     */                    
                    subFieldList[k] = addrNorm.performStringNormalization(subFieldList[k], sVar, aLoad, domain,
                                                                       postalState, zipCode, lineNums);      

                    fieldsDiff +=   fieldsBef - subFieldList[k].size();
                    actFieldsSize = subFieldList[k].size();
                                
                    // Search for the best patterns
                    numPats = addrPatt.getBestPatterns(returnPatt[i], subFieldList[k], sVar, postalState, zipCode, lineNums, domain);


                    updateReturnPatt(tempReturnPatt, returnPatt[i], numPats, totNumPats, numFields, k);
  
                    numFields += actFieldsSize;                                
                    totNumPats+= numPats;
                    
                    updateTokenStruct(sVar, subFieldList[k], fieldList[i], 
                        numLines[0] - k, fieldsDiff, numFields);
                }


                // Use the addressOutPatterns table to reajust the output patterns
                correctOutputPatterns(tempReturnPatt, totNumPats);
           
                /* If no pattern is found
                /*
                 * Finally, identify the best pattern (the order in the arangement of the
                 * fields) for the record, and also the fields to be returned.
                 */
                standAddr[i] = addrBuild.buildStandAddress(sVar, tempReturnPatt, totNumPats, postalState, zipCode,
                                                           aLoad);
                // Add the different patterns
             
            } else {

                /*
                 * We then normalize the different fields. We send in the parsed
                 * fields and return back the normalized ones. We keep the same
                 * number of fields, but we replace them with their normalized forms
                 * if found, or as an empty string if the field is to be removed. Also
                 * we define the fieldType inside this method.
                 *
                 * D.C. The comment above is not correct.  For example, "Business Park"
                 * goes from being two tokens to just one token.
                 *
                 */
                fieldList[i] = addrNorm.performStringNormalization(fieldList[i], sVar, aLoad, domain,
                                                                   postalState, zipCode, lineNums);

                
                /*
                 * Finally, identify the best pattern (the order in the arangement of the
                 * fields) for the record, and also the fields to be returned.
                 */      
                numPats = addrPatt.getBestPatterns(returnPatt[i], fieldList[i], sVar, postalState, zipCode, lineNums, domain);
                // Use it only for "UK"
                if (domain.compareTo("UK") == 0) {          
                    replaceAllEIsWithNAs(returnPatt[i]);
                }
    
                /* If no pattern is found
                /*
                 * Finally, identify the best pattern (the order in the arangement of the
                 * fields) for the record, and also the fields to be returned.
                 */
                standAddr[i] = addrBuild.buildStandAddress(sVar, returnPatt[i], numPats, postalState, zipCode,
                                                           aLoad);                
            }
                      
           /*
            * Create an instance of the PersonNameParser through
            * the call of the StandardizationEngineWrapper instance
            */
            standObj[i][0] = SbmeStandRecordFactory.getInstance("Address");
        
            /*
             * Copy the address fields into the orderedKeys and orderedValues variables
             */
            numberOfKeys[i] = PopulateStandRecords.copyFields(standAddr[i], orderedKeys[i],
                                                           orderedValues[i]);

            for (j = 0; j < numberOfKeys[i]; j++) {
            
                standObj[i][0].setValue(orderedKeys[i][0][j].toString(),
                                    orderedValues[i][0][j].toString());
            }
        }
        return standObj;
    }
    
    public void enablePatternTrace() {
        enablePatternTrace = true;
    }

    private void updateReturnPatt(OutputPattern[] tempReturnPatt, OutputPattern[] returnPatt, 
                        int nPatt, int k, int end, int idex) {
        
        int i;
        int lastEnd; 
        
        if (idex == 0) {             
            lastEnd = 0;
        } else {
            lastEnd = end;
        }
        
        // Loop over the list of sub-patterns
        for (i = 0; i < nPatt; i++) {           
            
            // Update all the components of the AddressOutputPattern            
            tempReturnPatt[k + i].setInputPatt(returnPatt[i].getInputPatt().toString());
            tempReturnPatt[k + i].setOutputPatt(returnPatt[i].getOutputPatt().toString());
            tempReturnPatt[k + i].setBeg(returnPatt[i].getBeg() + lastEnd);
            tempReturnPatt[k + i].setEnd(returnPatt[i].getEnd() + lastEnd);
            tempReturnPatt[k + i].setPatternType(returnPatt[i].getPatternType().toString());
            tempReturnPatt[k + i].setPriority(returnPatt[i].getPriority().toString());

            //
            returnPatt[i].setInputPatt("");
            returnPatt[i].setOutputPatt("");
            returnPatt[i].setBeg(0);
            returnPatt[i].setEnd(0);
            returnPatt[i].setPatternType("");
            returnPatt[i].setPriority("");
            
        }         
    }

    
    private void updateTokenStruct(AddressStandVariables sVar, ArrayList subFields, ArrayList fieldList, 
                                          int end , int diff, int sumF) {
        
        int i;
        int lastEnd; 
        int subSize = subFields.size(); 
        int size = fieldList.size();
        
        for (i = 0; i < subSize; i++) { 

            sVar.tempWordArray[i + sumF - subSize].setLength(0);
            sVar.tempWordArray[i + sumF - subSize].append(subFields.get(i));                                         
 
            sVar.tokensSt[i].createCopy(sVar.tempTokensSt[i + sumF - subSize]);
        }

        if (end == 1) {

            for (i = 0; i < size; i++) {

                if (sVar.tokensSt[i] == null) {
                    sVar.tokensSt[i] = new TokenStruct(domain);                 
                }
                sVar.tempTokensSt[i].createCopy(sVar.tokensSt[i]);                
            }
        } else {

            for (i = 0; i < size - sumF - diff; i++) { 
                sVar.wordArray[i] = (StringBuffer) fieldList.get(i + sumF + diff);    
            }                                     
        }       
    }
    
    
    private void correctOutputPatterns(OutputPattern[] tempReturnPatt, int numPatt) {
        
   
        StringBuffer strB = new StringBuffer();
        String[] str = new String[numPatt];    
        StringTokenizer st;       
        int i;
        int bnInd = -1;
        int naInd = -1;
        int bnNum = 0;
        int naNum = 0;        
                
        // First, make sure that the output pattern contains
        // duplicate types that need to be separated        
        for (i = 0; i < numPatt; i++){
            str[i] = tempReturnPatt[i].getOutputPatt().toString();
            
            if (str[i].indexOf("BN") != -1) {
                if (bnNum > 0) {               
                    bnInd = i;
                }
                bnNum++;
            }            
            if (str[i].indexOf("NA") != -1) {
                if(naNum > 0) {             
                    naInd = i;
                }
                naNum++;
            } 
        }
        /*
        if (bnNum < 2 && naNum < 2) {
            return; 
        }
        */
        int max = Math.max(bnInd, naInd);
        if (max == -1) {
            max = numPatt - 1;
        }
        boolean genAd = false;
        boolean[] specialCase = new boolean[numPatt - max];
        for (i = 0; i < numPatt - max; i++) {
            specialCase[i] = false;
        }
        
        // Form the global pattern
        for (i = 0; i < numPatt; i++){
            
            if (i > max && (str[i].indexOf("BN") == 0 
                             || (str[i].indexOf("NA") == 0))) {
                specialCase[i - max - 1] =true;
            }
            if (i <= max) {
                strB.append(str[i]);  
                
                if (i < max) {
                    strB.append("|");                                                     
                }
            } else if (specialCase[i - max - 1]) {
                strB.append("|");                                                               
                strB.append(str[i]); 
            } else if (!genAd) {
                strB.append("|*");  
                genAd = true;        
            }
            if(i == numPatt - 1 && !genAd) {
                strB.append("|*");           
            }
        }

        st = new StringTokenizer(addrPattTable.searchForAlterPattern(strB.toString()), "|");
        i = 0;

        while (st.hasMoreElements()) {
            if (i <= max) {
                tempReturnPatt[i].setOutputPatt(st.nextToken());
            } else if (specialCase[i - max - 1]) {
                tempReturnPatt[i].setOutputPatt(st.nextToken());
            } else if (i < numPatt) {
                tempReturnPatt[i].setOutputPatt(str[i]); 
                st.nextToken();               
            } else {
                st.nextToken();
            }
            i++;           
        } 
    }   
    private static void replaceAllEIsWithNAs(OutputPattern[] outP) {

        int len = outP.length;
        int j = 0;
        int length;
        StringBuffer sb; 
        
        while (outP[j].getEnd() != 0) {
            sb = outP[j].getOutputPatt();  
            
            length = sb.length();  
            if (length < 2) {
                return;
            }

            for (int i = 0; i < length - 1; i++) {
                if ((sb.charAt(i) == 'E' && sb.charAt(i + 1) == 'I')) {
                    sb.setCharAt(i, 'N');
                    sb.setCharAt(i + 1, 'A');
                    i++;

                } else if (sb.charAt(i) == 'N' && sb.charAt(i + 1) == 'A') {
                    i+=2;
                    continue;
                } else {     
                    break;                              
                }
                i++;
            }
            j++;
        }
    }
}
