/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * This class reads all the configured maximum values used in the matching code
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class ReadStandConstantsValues {

  private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.stand.util.ReadStandConstantsValues");

    /* declare all constants used in the person name standardization process */   
    /*    */
    private int words;
    /*     */
    private int conjmax;
    /*     */
    private int jrsrmax;
    /*     */
    private int nickmax;
    /*     */
    private static int lastmax;    
    /*     */
    private int premax;
    /*     */
    private int titlmax;
    /*     */
    private int sufmax;
    /*     */
    private int skpmax;
    /*     */
    private int ptrnmax1;
    /*     */
    private int ptrnmax2 = 10;
    /*     */
    private int twomax;
    /*     */
    private int thremax;
    /*     */
    private int blnkmax;
    /*     */
    private int dashSize;
    

    /* declare all constants used in the address standardization process */
    /*     */
    private int maxWords;
    /*     */
    private int clueArraySize;
    /*     */
    private int patternArraySize;
    /* maximum length of any pattern */
    private int maxPattSize;
    /*     */
    private int imageSize;
    /*     */
    private int nameOutputFieldSize;
    /*     */
    private int numberOutputFieldSize;
    /*     */
    private int directionOutputFieldSize;
    /*     */
    private int typeOutputFieldSize;
    /*     */
    private int prefixOutputFieldSize;
    /*     */
    private int suffixOutputFieldSize;
    /*     */
    private int extensionOutputFieldSize;
    /*     */
    private int extrainfoOutputFieldSize;

    /*
     * declare all constants used in the address standardization process, but
     * are hidden data for internal use only
     */

    /* number of output token types */
    private int numberOfOutputTokenTypes;
    /*     */
    private int maxInputTokens;
    /* in/out token string size */
    private int patSize;
    /*     */
    private int maxWordsInclue;
    /* maximum number of output sub-patterns */
    private int maxOutputPatterns;
    /* Max record length for the pattern records */
    private int patternReclen;
    /* record length for the clue words records */
    private int clueReclen;
    /* record length for the clue words records */
    private int masterClueReclen;
        
    private String tokensBlank;
    private String tokensB;;
    private String tokensT;
    private String tokensS;
    private String tokensW;
    private String tokensN;
    private String tokensR;
    private String spCh;

    private String tokenTypes;
    private String patTypes;

    /* master array size for types */
    private int masterArraySizeTY;
    /* master array size for alphas */
    private int masterArraySizeAU;
    /* master array size for properties */
    private int masterArraySizeBU;
    /* master array size for directions */
    private int masterArraySizeDR;
    /* master array size for extra info */
    private int masterArraySizeEI;
    /* master array size for extensions */
    private int masterArraySizeEX;
    /* master array size for address number extensions */
    private int masterArraySizeEN;    
    /* master array size for mile posts */
    private int masterArraySizeMP;
    /* master array size for fractions */
    private int masterArraySizeFC;
    /* master array size for ordinal types */
    private int masterArraySizeOT;
    /* master array size for boxes */
    private int masterArraySizeBX;
    /* master array size for rural routes */
    private int masterArraySizeRR;
    /* master array size for within descr. */
    private int masterArraySizeWD;
    /* master array size for within ident. */
    private int masterArraySizeWI;
    /* master array size for ampersands */
    private int masterArraySizeAM;
    /* master array size for state abbrevs */
    private int masterArraySizeSA;
    /* master array size for PR urbanizations */
    private int masterArraySizeUR;
    /* master array size for DM months of the year */
    private int masterArraySizeDM;
    /* master array size for  */
    private int masterArraySizeHR;
    /* master array size for  */
    private int masterArraySizeBP;    
    /* master array size for CORNERS  */        
    private int masterArraySizeCN;    

    private double hStarWeight;
    private double tStarWeight;
    private double pStarWeight;
    private double rStarWeight;
    private double wStarWeight;
    private double bStarWeight;
    private double uStarWeight;
    private double hPlusWeight;
    private double bPlusWeight;
    private double pPlusWeight;
    private double rPlusWeight;
    private double wPlusWeight;
    private String unmatchPts;

    private int bizMaxWords;
    private int cityMax;
    private int primaryMax;
    private int countryMax;
    private int industryMax;
    private int patternMax;
    private int mergerMax;
    private int adjectiveMax;
    private int orgMax;
    private int assocMax;
    private int genTermMax;
    private int charsMax;

    /** a string of empty spaces     */
    public final String NULL50 = "                                                  ";

    /* pattern table hash size */
    private int patternHashSize;
    /* hash table size of clue table. NOTE:  must be >= clueArraySize */
    private int clueHashSize;


    private static final HashMap CONSTANTS = new HashMap();
    
    public static ReadStandConstantsValues getInstance(String domain) {
        ReadStandConstantsValues vals = (ReadStandConstantsValues) CONSTANTS.get(domain);
        return vals;
    }
    
    public static synchronized void registerInstance(String domain, ReadStandConstantsValues vals) {
        if (getInstance(domain) != null) {
//            throw new RuntimeException("Instance already registered: " + domain);
            return;
        }
        CONSTANTS.put(domain, vals);         
    }
    
    /**
     * Returns the input streams for data file
     * @param standDataStream an input stream from the repository
     * @throws SbmeStandardizationException the generic stand exception
     * @throws IOException an I/O exception
     */
    public void readStandConstants(InputStream[] standDataStream)
        throws SbmeStandardizationException, IOException {
        
        LOGGER.debug("Starting to read personConstants.cfg file");
    
        try {
            
            Properties prop = new Properties();
            prop.load(standDataStream[0]);
            
            words = Integer.parseInt(prop.getProperty("words"));
            conjmax = Integer.parseInt(prop.getProperty("conjmax"));
            jrsrmax = Integer.parseInt(prop.getProperty("jrsrmax"));
            nickmax = Integer.parseInt(prop.getProperty("nickmax"));
            lastmax = Integer.parseInt(prop.getProperty("lastmax"));            
            premax = Integer.parseInt(prop.getProperty("premax"));
            titlmax = Integer.parseInt(prop.getProperty("titlmax"));
            sufmax = Integer.parseInt(prop.getProperty("sufmax"));
            skpmax = Integer.parseInt(prop.getProperty("skpmax"));
            ptrnmax1 = Integer.parseInt(prop.getProperty("ptrnmax1"));
            twomax = Integer.parseInt(prop.getProperty("twomax"));
            thremax = Integer.parseInt(prop.getProperty("thremax"));
            blnkmax = Integer.parseInt(prop.getProperty("blnkmax"));
            dashSize = Integer.parseInt(prop.getProperty("dashSize"));

        } catch (IOException e) {
            LOGGER.fatal("Coud not read personConstants.cfg file", e);
        }

        LOGGER.debug("Starting to read addressConstants.cfg file");

        try {
            
            Properties prop = new Properties();
            prop.load(standDataStream[1]);
            
            maxWords = Integer.parseInt(prop.getProperty("maxWords"));
            clueArraySize = Integer.parseInt(prop.getProperty("clueArraySize"));
            patternArraySize =
            Integer.parseInt(prop.getProperty("patternArraySize"));
            maxPattSize = Integer.parseInt(prop.getProperty("maxPattSize"));
            imageSize = Integer.parseInt(prop.getProperty("imageSize"));
            nameOutputFieldSize = Integer.parseInt(prop.getProperty("nameOutputFieldSize"));
            numberOutputFieldSize = Integer.parseInt(prop.getProperty("numberOutputFieldSize"));
            directionOutputFieldSize = Integer.parseInt(prop.getProperty("directionOutputFieldSize"));
            typeOutputFieldSize = Integer.parseInt(prop.getProperty("typeOutputFieldSize"));
            prefixOutputFieldSize = Integer.parseInt(prop.getProperty("prefixOutputFieldSize"));
            suffixOutputFieldSize = Integer.parseInt(prop.getProperty("suffixOutputFieldSize"));
            extensionOutputFieldSize = Integer.parseInt(prop.getProperty("extensionOutputFieldSize"));
            extrainfoOutputFieldSize = Integer.parseInt(prop.getProperty("extrainfoOutputFieldSize"));
            
        } catch (IOException e) {
            LOGGER.fatal("Coud not read addressConstants.cfg file", e);
        }

        LOGGER.debug("Starting to read addressInternalConstants.cfg file");

        try {
            
            Properties prop = new Properties();
            prop.load(standDataStream[2]);

            numberOfOutputTokenTypes = Integer.parseInt(prop.getProperty("numberOfOutputTokenTypes"));
            maxInputTokens = Integer.parseInt(prop.getProperty("maxInputTokens"));
            patSize = Integer.parseInt(prop.getProperty("patSize"));
            maxWordsInclue = Integer.parseInt(prop.getProperty("maxWordsInclue"));
            maxOutputPatterns = Integer.parseInt(prop.getProperty("maxOutputPatterns"));
            patternReclen = Integer.parseInt(prop.getProperty("patternReclen"));
            clueReclen = Integer.parseInt(prop.getProperty("clueReclen"));
            masterClueReclen = Integer.parseInt(prop.getProperty("masterClueReclen"));
                        
            tokensBlank  = prop.getProperty("tokensBlank");
            tokensB = prop.getProperty("tokensB");
            tokensT = prop.getProperty("tokensT");
            tokensS = prop.getProperty("tokensS");
            tokensW = prop.getProperty("tokensW");
            tokensN = prop.getProperty("tokensN");
            tokensR = prop.getProperty("tokensR");
            spCh = prop.getProperty("spCh");

            tokenTypes = prop.getProperty("tokenTypes");
            patTypes = prop.getProperty("patTypes");

            masterArraySizeTY =
            Integer.parseInt(prop.getProperty("masterArraySizeTY"));
            masterArraySizeAU =
            Integer.parseInt(prop.getProperty("masterArraySizeAU"));
            masterArraySizeBU =
            Integer.parseInt(prop.getProperty("masterArraySizeBU"));
            masterArraySizeDR =
            Integer.parseInt(prop.getProperty("masterArraySizeDR"));
            masterArraySizeEI =
            Integer.parseInt(prop.getProperty("masterArraySizeEI"));
            masterArraySizeEX =
            Integer.parseInt(prop.getProperty("masterArraySizeEX"));
            masterArraySizeEN =
            Integer.parseInt(prop.getProperty("masterArraySizeEN", "0"));            
            masterArraySizeMP =
            Integer.parseInt(prop.getProperty("masterArraySizeMP"));
            masterArraySizeFC =
            Integer.parseInt(prop.getProperty("masterArraySizeFC"));
            masterArraySizeOT =
            Integer.parseInt(prop.getProperty("masterArraySizeOT"));
            masterArraySizeBX =
            Integer.parseInt(prop.getProperty("masterArraySizeBX"));
            masterArraySizeRR =
            Integer.parseInt(prop.getProperty("masterArraySizeRR"));
            masterArraySizeWD =
            Integer.parseInt(prop.getProperty("masterArraySizeWD"));
            masterArraySizeWI =
            Integer.parseInt(prop.getProperty("masterArraySizeWI"));
            masterArraySizeAM =
            Integer.parseInt(prop.getProperty("masterArraySizeAM"));
            masterArraySizeSA =
            Integer.parseInt(prop.getProperty("masterArraySizeSA"));
            masterArraySizeUR =
            Integer.parseInt(prop.getProperty("masterArraySizeUR"));
            masterArraySizeDM =
            Integer.parseInt(prop.getProperty("masterArraySizeDM", "0"));
            masterArraySizeHR =
            Integer.parseInt(prop.getProperty("masterArraySizeHR", "0"));
            masterArraySizeBP =
            Integer.parseInt(prop.getProperty("masterArraySizeBP", "0"));
            masterArraySizeCN =
            Integer.parseInt(prop.getProperty("masterArraySizeCN", "0"));

            hStarWeight = Double.parseDouble(prop.getProperty("hStarWeight"));
            tStarWeight = Double.parseDouble(prop.getProperty("tStarWeight"));
            pStarWeight = Double.parseDouble(prop.getProperty("pStarWeight"));
            rStarWeight = Double.parseDouble(prop.getProperty("rStarWeight"));
            wStarWeight = Double.parseDouble(prop.getProperty("wStarWeight"));
            bStarWeight = Double.parseDouble(prop.getProperty("bStarWeight"));
            uStarWeight = Double.parseDouble(prop.getProperty("uStarWeight"));
            hPlusWeight = Double.parseDouble(prop.getProperty("hPlusWeight"));
            bPlusWeight = Double.parseDouble(prop.getProperty("bPlusWeight"));
            pPlusWeight = Double.parseDouble(prop.getProperty("pPlusWeight"));
            rPlusWeight = Double.parseDouble(prop.getProperty("rPlusWeight"));
            wPlusWeight = Double.parseDouble(prop.getProperty("wPlusWeight"));
            unmatchPts = prop.getProperty("unmatchPts");
            //Handle cases where the maxOutputPatterns is set much smaller than 
            //the maxInputTokens. There is a risk of throwing an exception.
            if (maxOutputPatterns < maxInputTokens * 2/3) {
                maxOutputPatterns = maxInputTokens;
            }

        } catch (IOException e) {
            LOGGER.fatal("Coud not read addressInternalConstants.cfg file", e);
        }
        
        LOGGER.debug("Starting to read bizConstants.cfg file");

        try {
            
            Properties prop = new Properties();
            prop.load(standDataStream[3]);
        
            bizMaxWords = Integer.parseInt(prop.getProperty("bizMaxWords", "0"));
            cityMax = Integer.parseInt(prop.getProperty("cityMax"));
            primaryMax = Integer.parseInt(prop.getProperty("primaryMax"));
            countryMax = Integer.parseInt(prop.getProperty("countryMax"));
            industryMax = Integer.parseInt(prop.getProperty("industryMax"));
            patternMax = Integer.parseInt(prop.getProperty("patternMax"));
            mergerMax = Integer.parseInt(prop.getProperty("mergerMax"));
            adjectiveMax = Integer.parseInt(prop.getProperty("adjectiveMax"));
            orgMax = Integer.parseInt(prop.getProperty("orgMax"));
            assocMax = Integer.parseInt(prop.getProperty("assocMax"));
            genTermMax = Integer.parseInt(prop.getProperty("genTermMax"));
            charsMax = Integer.parseInt(prop.getProperty("charsMax"));
            
        } catch (IOException e) {
            LOGGER.fatal("Coud not read bizConstants.cfg file", e);
        }
        
    }
    
    

    /**
     * @return the field
     */
    public int getWords() {
        return words;
    }
    /**
     * @return the
     */
    public int getConjmax() {
        return conjmax;
    }
    /**
     * @return the
     */
    public int getJrsrmax() {
        return jrsrmax;
    }
    /**
     * @return the
     */
    public int getNickmax() {
        return nickmax;
    }
    /**
     * @return the
     */
    public int getLastMax() {
        return lastmax;
    }
    /**
     * @return the
     */
    public int getPremax() {
        return premax;
    }
    /**
     * @return the
     */
    public int getTitlmax() {
        return titlmax;
    }
    /**
     * @return the
     */
    public int getSufmax() {
        return sufmax;
    }
    
    /**
     * @return the
     */
    public int getSkpmax() {
        return skpmax;
    }
    
    /**
     * @return the
     */
    public int getPtrnmax1() {
        return ptrnmax1;
    }
    
    /**
     * @return the
     */
    public int getPtrnmax2() {
        return ptrnmax2;
    }
    
    /**
     * @return the
     */
    public int getTwomax() {
        return twomax;
    }

    /**
     * @return the
     */
    public int getThremax() {
        return thremax;
    }

    /**
     * @return the
     */
    public int getBlnkmax() {
        return blnkmax;
    }

    /**
     * @return the
     */
    public int getDashSize() {
        return dashSize;
    }


    /**
     * @return the
     */
    public int getMaxWords() {
        return maxWords;
    }

    /**
     * max array size of clue table
     * @return the size
     */
    public int getClueArraySize() {
        return clueArraySize;
    }

    /**
     * maximum  array size for the pattern table
     * @return the size
     */
    public int getPatternArraySize() {
        return patternArraySize;
    }

    /**
     * maximum length of any pattern
     * @return the size
     */
    public int getMaxPattSize() {
        return maxPattSize;
    }

    /**
     * @return the
     */
    public int getImageSize() {
        return imageSize;
    }

    /**
     * @return the size of the output fields name
     */
    public int getNameOutputFieldSize() {
        return nameOutputFieldSize;
    }
    
    /** 
     * @return the size of the output fields number
     */
    public int getNumberOutputFieldSize() {
        return numberOutputFieldSize;
    }
    
    /**
     * @return the size of the output fields direction
     */
    public int getDirectionOutputFieldSize() {
        return directionOutputFieldSize;
    }
    
    /** 
     * @return the size of the output fields type
     */
    public int getTypeOutputFieldSize() {
        return typeOutputFieldSize;
    }
    
    /** 
     * @return the size of the output fields prefix
     */
    public int getPrefixOutputFieldSize() {
        return prefixOutputFieldSize;
    }
    
    /**   
     * @return the size of the output suffix
     */
    public int getSuffixOutputFieldSize() {
        return suffixOutputFieldSize;
    }
    
    /** 
     * @return the size of the output fields extension
     */
    public int getExtensionOutputFieldSize() {
        return extensionOutputFieldSize;
    }
    
    /**  
     * @return the size of the output extra info
     */
    public int getExtrainfoOutputFieldSize() {
        return extrainfoOutputFieldSize;
    }



    /**
     * number of output token types
     * @return the size
     */
    public int getNumberOfOutputTokenTypes() {
        return numberOfOutputTokenTypes;
    }

    /**  
     * @return the
     */
    public int getMaxInputTokens() {
        return maxInputTokens;
    }
    
    /**
     * in/out token string size
     * @return the size
     */
    public int getPatSize() {
        return patSize;
    }

    /**    
     * @return the
     */
    public int getMaxWordsInclue() {
        return maxWordsInclue;
    }
    
    /**
     * maximum number of output sub-patterns
     * @return the size
     */
    public int getMaxOutputPatterns() {
        return maxOutputPatterns;
    }

    /** 
     * record length for the InputPattern records
     * @return the size 
     */
    public int getPatternReclen() {
        return patternReclen;
    }
 
    /** 
     * record length for the InputPattern records
     * @return the size 
     */
    public int getClueReclen() {
        return clueReclen;
    } 

    /** 
     * record length for the InputPattern records
     * @return the size 
     */
    public int getMasterClueReclen() {
        return masterClueReclen;
    } 
    
    /**
     *
     * @return the string of
     */
    public String getTokensBlank() {
        return tokensBlank;
    }
    
    /**
     *
     * @return the string of
     */
    public String getTokensB() {
        return tokensB;
    }
    
    /**
     *
     * @return the string of
     */
    public String getTokensT() {
        return tokensT;
    }
    
    /**
     *
     * @return the string of
     */
    public String getTokensS() {
        return tokensS;
    }

    /**
     *
     * @return the string of
     */
    public String getTokensW() {
        return tokensW;
    }

    /**
     *
     * @return the string of
     */
    public String getTokensN() {
        return tokensN;
    }
    
    /**
     *
     * @return the string of
     */
    public String getTokensR() {
        return tokensR;
    }

    /**
     *
     * @return the string of
     */
    public String getSpCh() {
        return spCh;
    }

    /**
     *
     * @return the string of
     */
    public String getTokenTypes() {
        return tokenTypes;
    }
 
    /**
     *
     * @return the string of
     */
    public String getPatTypes() {
        return patTypes;
    }

    /**
     * master array size for types
     * @return the size
     */
    public int getMasterArraySizeTY() {
        return masterArraySizeTY;
    }
    
    /**
     * master array size for alphas
     * @return the size
     */
    public int getMasterArraySizeAU() {
        return masterArraySizeAU;
    }
    
    /**
     * master array size for properties
     * @return the size
     */
    public int getMasterArraySizeBU() {
        return masterArraySizeBU;
    }
    
    /**
     * master array size for directions
     * @return the size
     */
    public int getMasterArraySizeDR() {
        return masterArraySizeDR;
    }
    
    /**
     * master array size for extra info
     * @return the size
     */
    public int getMasterArraySizeEI() {
        return masterArraySizeEI;
    }
    
    /**
     * master array size for extensions
     * @return the size
     */
    public int getMasterArraySizeEX() {
        return masterArraySizeEX;
    }
    /**
     * master array size for extensions
     * @return the size
     */
    public int getMasterArraySizeEN() {
        return masterArraySizeEN;
    }
    
    /**
     * master array size for mile posts
     * @return the size
     */
    public int getMasterArraySizeMP() {
        return masterArraySizeMP;
    }
    
    /**
     * master array size for fractions
     * @return the size
     */
    public int getMasterArraySizeFC() {
        return masterArraySizeFC;
    }
    
    /**
     * master array size for ordinal types
     * @return the size
     */
    public int getMasterArraySizeOT() {
        return masterArraySizeOT;
    }
    
    /**
     * master array size for boxes
     * @return the size
     */
    public int getMasterArraySizeBX() {
        return masterArraySizeBX;
    }
    
    /**
     * master array size for rural routes
     * @return the size
     */
    public int getMasterArraySizeRR() {
        return masterArraySizeRR;
    }
    
    /**
     * master array size for within descr.
     * @return the size
     */
    public int getMasterArraySizeWD() {
        return masterArraySizeWD;
    }
    
    /**
     * master array size for within ident.
     * @return the size
     */
    public int getMasterArraySizeWI() {
        return masterArraySizeWI;
    }
    
    /**
     * master array size for ampersands
     * @return the size
     */
    public int getMasterArraySizeAM() {
        return masterArraySizeAM;
    }
    
    /**
     * master array size for state abbrevs
     * @return the size
     */
    public int getMasterArraySizeSA() {
        return masterArraySizeSA;
    }
    
    /**
     * master array size for PR urbanizations
     * @return the size
     */
    public int getMasterArraySizeUR() {
        return masterArraySizeUR;
    }
    
    /**
     * master array size for PR urbanizations
     * @return the size
     */
    public int getMasterArraySizeDM() {
        return masterArraySizeDM;
    }
    /**
     * master array size for HR
     * @return the size
     */
    public int getMasterArraySizeHR() {
        return masterArraySizeHR;
    }
 
    /**
     * master array size for BP 
     * @return the size
     */
    public int getMasterArraySizeBP() {
        return masterArraySizeBP;
    } 
    /**
     * master array size for CN 
     * @return the size
     */
    public int getMasterArraySizeCN() {
        return masterArraySizeCN;
    }     
    /**
     *
     * @return a bouble of
     */
    public double getHStarWeight() {
        return hStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getTStarWeight() {
        return tStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getPStarWeight() {
        return pStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getRStarWeight() {
        return rStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getWStarWeight() {
        return wStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getBStarWeight() {
        return bStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getUStarWeight() {
        return uStarWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getHPlusWeight() {
        return hPlusWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getBPlusWeight() {
        return bPlusWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getPPlusWeight() {
        return pPlusWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getRPlusWeight() {
        return rPlusWeight;
    }

    /**
     *
     * @return the integer of
     */
    public double getWPlusWeight() {
        return wPlusWeight;
    }

    /**
     *
     * @return the string of
     */
    public String getUnmatchPts() {
        return unmatchPts;
    }


    /** 
     * pattern table hash size 
     * @return the size 
     */
    public int getPatternHashSize() {
    
        // Assign to this constants a value > PATTERN_HASHSIZE
        patternHashSize = patternArraySize;

        return patternHashSize;
    }
  
    /** 
     * hash table size of clue table. NOTE:  must be >= clueArraySize
     * @return the size 
     */
    public int getClueHashSize() {
    
        // Assign to this constants a value > PATTERN_HASHSIZE
        clueHashSize = clueArraySize;

        return clueHashSize;
    }
    
// Start of the constants associated with bizConstants

   /**
    * @return the field
    */
    public int getBizMaxWords() {
        if (bizMaxWords > 0) {
            return bizMaxWords;
        } else {
            return words;
        }
    }
    /**
     *
     * @return the string of
     */
    public int getCityMax() {
        return cityMax;
    }
  
    
    /**
     *
     * @return the int of
     */
    public int getPrimaryMax() {
        return primaryMax;
    }

 
    /**
     *
     * @return the int of
     */
    public int getCountryMax() {
        return countryMax;
    }
    
    
    /**
     *
     * @return the int of
     */
    public int getIndustryMax() {
        return industryMax;
    }
  
    
    /**
     *
     * @return the int of
     */
    public int getPatternMax() {
        return patternMax;
    }


    /**
     *
     * @return the int of
     */
    public int getMergerMax() {
        return mergerMax;
    }

    
    /**
     *
     * @return the int of
     */
    public int getAdjectiveMax() {
        return adjectiveMax;
    }
    
    /**
     *
     * @return the int of
     */
    public int getOrgMax() {
        return orgMax;
    }
    
    /**
     *
     * @return the int of
     */
    public int getAssocMax() {
        return assocMax;
    }
    
    /**
     *
     * @return the int of
     */
    public int getGenTermMax() {
        return genTermMax;
    }
    
    /**
     *
     * @return the int of
     */
    public int getCharsMax() {
        return charsMax;
    }
} 
   