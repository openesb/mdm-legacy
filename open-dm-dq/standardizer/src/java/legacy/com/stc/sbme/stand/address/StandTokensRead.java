/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;


/**
 * 
 *
 * @author Sofiane Ouaguenouni 
 * @version $Revision: 1.1.2.1 $
 */
class StandTokensRead {

    private final String TOKENS_BLANK;
    private final String TOKENS_B;
    private final String TOKENS_T;
    private final String TOKENS_S;
    private final String TOKENS_W;
    private final String TOKENS_N;
    private final String TOKENS_R;
    private final String SP_CH;

    StandTokensRead(String domain) {
        TOKENS_BLANK = ReadStandConstantsValues.getInstance(domain).getTokensBlank();
        TOKENS_B = ReadStandConstantsValues.getInstance(domain).getTokensB();
        TOKENS_T = ReadStandConstantsValues.getInstance(domain).getTokensT();
        TOKENS_S = ReadStandConstantsValues.getInstance(domain).getTokensS();
        TOKENS_W = ReadStandConstantsValues.getInstance(domain).getTokensW();
        TOKENS_N = ReadStandConstantsValues.getInstance(domain).getTokensN();
        TOKENS_R = ReadStandConstantsValues.getInstance(domain).getTokensR();
        SP_CH = ReadStandConstantsValues.getInstance(domain).getSpCh();

       /* 
        * Call method to read from the above strings, assign them to sting
        * array element, and return their number
        */
        legalTokensBlank = EmeUtil.stringArrayTokens(TOKENS_BLANK, " ");
        legalTokensB = EmeUtil.stringArrayTokens(TOKENS_B, " ");
        legalTokensT = EmeUtil.stringArrayTokens(TOKENS_T, " ");
        legalTokensS = EmeUtil.stringArrayTokens(TOKENS_S, " ");
        legalTokensW = EmeUtil.stringArrayTokens(TOKENS_W, " ");
        legalTokensN = EmeUtil.stringArrayTokens(TOKENS_N, " ");
        legalTokensR = EmeUtil.stringArrayTokens(TOKENS_R , " ");
        specialCharacters = EmeUtil.charArrayTokens(SP_CH, " ");
        
        SZ_BLANK = legalTokensBlank.length;
        SZ_B = legalTokensB.length;
        SZ_T = legalTokensT.length;
        SZ_S = legalTokensS.length;
        SZ_W = legalTokensW.length;
        SZ_N = legalTokensN.length;
        SZ_R = legalTokensR.length;
        S_C = specialCharacters.length;
    }
    
    

    /* declare all constants used in the standardization process in this class */

    private String[] legalTokensBlank;
    
    private String[] legalTokensB;

    private String[] legalTokensT;

    private String[] legalTokensS;

    private String[] legalTokensW;

    private String[] legalTokensN;

    private String[] legalTokensR;

    private char[] specialCharacters;

    /**   */
    final int SZ_BLANK;
    /**   */
    final int SZ_B;
    /**   */
    final int SZ_T;
    /**   */
    final int SZ_S;
    /**   */
    final int SZ_W;
    /**   */
    final int SZ_N;
    /**   */
    final int SZ_R;
    /**   */
    final int S_C;
 
    
    /** 
     * 
     * @return an array of strings of
     */
    String[] getLegalTokensBlank() {
        return legalTokensBlank;
    }

    /** 
     * 
     * @return an array of strings of
     */
    String[] getLegalTokensB() {
        return legalTokensB;
    }
    
    /** 
     * 
     * @return an array of strings of         
     */
    String[] getLegalTokensT() {
        return legalTokensT;
    }
    
    /** 
     * 
     * @return an array of strings of
     */
    String[] getLegalTokensS() {
        return legalTokensS;
    }
    
    /** 
     * 
     * @return an array of strings of
     */
    String[] getLegalTokensW() {
        return legalTokensW;
    }

    /** 
     * 
     * @return an array of strings of
     */
    String[] getLegalTokensN() {
        return legalTokensN;
    }
    
    /** 
     * 
     * @return an array of strings of
     */
    String[] getLegalTokensR() {
        return legalTokensR;
    }
    
    /** 
     * 
     * @return an array of strings of
     */
    char[] getSpecialCharacters() {
        return specialCharacters;
    }
}
