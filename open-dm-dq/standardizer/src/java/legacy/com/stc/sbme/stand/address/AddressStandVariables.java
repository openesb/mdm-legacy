/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * Defines all the 'global' variables used in the standardization code.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class AddressStandVariables {


    /**
     *
     */
    public ClueTable[] entries;


    /**
     * a boolean
     */
    public boolean firstDash;
    
    /**
     * a boolean
     */
    public boolean[] fracTok = new boolean[2];    
    
    /**
     * a 
     */
    public int[] wordSplits;

    /**
     * a 
     */
    StringBuffer[] wordArray;
    StringBuffer[] tempWordArray;
    /**
     * a 
     */
    boolean stateFlag = false;
    
    /**
     * a 
     */
    StringBuffer[] stateAb;

    /**
     * a 
     */
    StringBuffer[] stateFull;
    
    /**
     * a 
     */
    int nbState;

    /**
     * a 
     */
    TokenStruct[] tokensSt;
    TokenStruct[] tempTokensSt;    
    
    /**
     * a boolean
     */
    public int numWords;
    
    /**
     * aaa
     */
    public char inUsageFlag;

    /**
     * aaa
     */
    public char usf;

    /**
     * aaa
     */
    public char abbrevOpt;
    
    /**
     * aaa
     */
    int numTokensBuilt;

    /**
     * aaa
     */
    public PatternTable[] patterns;


    private AddressStandVariables(String domain) {
        int MAX_WORDS = ReadStandConstantsValues.getInstance(domain).getMaxWords();
        int MAX_INPUT_TOKENS = ReadStandConstantsValues.getInstance(domain).getMaxInputTokens();
        int WORDS = ReadStandConstantsValues.getInstance(domain).getWords();
        wordSplits = new int[MAX_WORDS * 3/2];
        wordArray = new StringBuffer[MAX_WORDS * 3/2];
        tempWordArray = new StringBuffer[MAX_WORDS * 3/2];
        tokensSt = new TokenStruct[MAX_INPUT_TOKENS];
        tempTokensSt = new TokenStruct[MAX_INPUT_TOKENS];  
    }

    /**
     * Define a factory method that return an instance of the class 
     * @return an AddressVariables instance
     */
    public static AddressStandVariables getInstance(String domain) {
        return new AddressStandVariables(domain);
    }
}
