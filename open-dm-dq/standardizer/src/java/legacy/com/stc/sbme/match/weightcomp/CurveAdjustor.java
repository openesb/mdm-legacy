/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.util.ArrayList;

import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.match.util.ReadMatchConstantsValues;

/**
 * Defines default curve-adjustment parameters used with the comparators FREQUENCY,
 * UNCERTAINTY, PRORATED
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class CurveAdjustor {
    
    // Index of the first character of a comparison type
    private static final int PRIMARY = ReadMatchConstantsValues.getPrimary();
    // Index of the second character of a comparison type
    private static final int SECONDARY = ReadMatchConstantsValues.getSecondary();
    // Index of the third character of a comparison type
    private static final int TERTIARY = ReadMatchConstantsValues.getTertiary();
    
    // data-oriented matching Comparators
    // A comparison type used with fields like 'age'.
    private static final char PRORATED = ReadMatchConstantsValues.getProrated();
    // A comparison type used with fields like 'year'.
    private static final char NUMBER = ReadMatchConstantsValues.getNumber();
    // A comparison type used simultaneously for fields like first names,
    // last names, and numeric values.
    private static final char UNCERTAINTY = ReadMatchConstantsValues.getUncertainty(); 
    // A comparison type used with frequency analysis, when dealing with
    // fields like first names, last names, and numeric values.
    private static final char FREQUENCY = ReadMatchConstantsValues.getFrequency();
    private static final char FIRST_NAME = ReadMatchConstantsValues.getFirstName();
    private static final char LAST_NAME = ReadMatchConstantsValues.getLastName();
    private static final char ADDRESS_NUMERIC = ReadMatchConstantsValues.getAddressNumeric();
    
    private static double defaultCurv[] = {1, 0.0, 4.5, 4.5};
    private static double defaultCurvFirstN[] = {0.92, 0.75, 1.5, 3.0};
    private static double defaultCurvLastN[] = {0.96, 0.88, 3.0, 4.5};
    private static double defaultCurvNumeric[] = {0.98, 0.9, 7.5, 4.5};
    
    private static double[] hf = {0.0, 0.0, 0.0, 0.0};
    
    /**
     *
     *
     * @param matchVar
     * @param al
     * @param i
     * @return boolean
     * @throws SbmeMatchingException
     */
    static boolean adjustParameters(MatchVariables matchVar, ArrayList al, int i)
    throws SbmeMatchingException {
        
        int j;
        int defSw = 0;
        boolean defaultAdjustment = false;
        
        StringBuffer method = matchVar.comparatorType[i];
        
        // Assign to hf[] the real values hold in ArrayList 'al' which represent
        // the custom curve-adjustment values defined in config. file 'parmmf.txt'.
        // The default values (if there is no data) are zeros.
        for (j = 0; j < 4; j++) {
            
            hf[j] = ((Double) al.get(j)).doubleValue();
        }
        
        // For comparison types YEAR and PRORATED, check if the custom values meet
        // requirements, otherwise, replace with appropriate values
        if ((method.charAt(PRIMARY) == NUMBER)
        || (method.charAt(PRIMARY) == PRORATED)) {
            
            // Use the custom value hf[0] only if it is  between 0 and 1
            // exclusive.
            if ((hf[0] > 0.0) && (hf[0] < 1.0)) {
                
                matchVar.adjCurv[i << 2] = hf[0];
                
            } else {
                //
                matchVar.adjCurv[i << 2] = 0.15;
                defaultAdjustment = true;
            }
            
            matchVar.adjCurv[(i << 2) + 1] = 0.0;
            matchVar.adjCurv[(i << 2) + 2] = 0.0;
            matchVar.adjCurv[(i << 2) + 3] = 0.0;
            
        } else if ((method.charAt(PRIMARY) == UNCERTAINTY)
        || (method.charAt(PRIMARY) == FREQUENCY)) {
            
            for (j = 0; j < 4; j++) {
                
                // The custom values must be different from zero
                if (hf[j] != 0.0)  {
                    matchVar.adjCurv[(i << 2) + j] = hf[j];
                    
                } else {
                    
                    defSw++;
                    // If one of the values is zero, need further consideration.
                    break;
                }
            }
            
            // If at least one of the custom adjustment parameters is not
            // specified (i.e. hf[] = 0), use default values.
            if (defSw != 0) {
                
                if ((method.charAt(SECONDARY) == FIRST_NAME)
                || (method.charAt(TERTIARY) == FIRST_NAME)) {
                    
                    for (j = 0; j < 4; j++) {
                        
                        // Default adjustment values for first name
                        matchVar.adjCurv[(i << 2) + j] = defaultCurvFirstN[j];
                    }
                    
                } else if ((method.charAt(SECONDARY) == LAST_NAME)
                || (method.charAt(TERTIARY) == LAST_NAME)) {
                    
                    for (j = 0; j < 4; j++) {
                        
                        // Default adjustment values for last name
                        matchVar.adjCurv[(i << 2) + j] = defaultCurvLastN[j];
                    }
                    
                } else if ((method.charAt(SECONDARY) == ADDRESS_NUMERIC)
                || (method.charAt(TERTIARY) == ADDRESS_NUMERIC)) {
                    
                    for (j = 0; j < 4; j++) {
                        
                        // Default adjustment values for NUMERIC
                        matchVar.adjCurv[(i << 2) + j] = defaultCurvNumeric[j];
                    }
                    
                } else {
                    
                    for (j = 0; j < 4; j++) {
                        
                        // Default generic adjustment parameter values
                        matchVar.adjCurv[(i << 2) + j] = defaultCurv[j];
                    }
                }
                defSw = 0;
            }
        }
        return defaultAdjustment;
    }
}
