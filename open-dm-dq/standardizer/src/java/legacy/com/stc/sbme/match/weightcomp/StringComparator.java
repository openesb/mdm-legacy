/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.EmeUtil;


/**
 * The algorithm comprises the Jaro string comparator plus three additional enhancements
 * due to Winkler and al. Each one makes use of the information from the prior characteristics.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class StringComparator {


    /*
     *
     */
    private static final String NULL50 = ReadMatchConstantsValues.NULL50;

    
    /*
     *
     */
    private static int[] indC = {0, 0, 0, 0, 0, 0};

    /**
     * Measure the degree of similarity between recordA and recordB using a generic
     * string comparator algorithm that accounts for insertions,
     * deletions, transpositions plus three additional enhancements (scanning errors,
     * large strings and linear dependencies of the error rate function of the
     * length)
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @param      mEng  an instance of the SbmeMatchingEngine class
     * @return     a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     */
    static double genStrComparator(String recordA, String recordB, int ind, MatchVariables matchVar)
    throws SbmeMatchingException {
        
        StringBuffer recAHold = new StringBuffer();
        StringBuffer recBHold = new StringBuffer();
        
        StringBuffer recAFlag = new StringBuffer();
        StringBuffer recBFlag = new StringBuffer();
        
        
        StringBuffer recA = new StringBuffer(recordA);
        StringBuffer recB = new StringBuffer(recordB);
        
        double weight, numSim;
        int minv;
        int searchRange;
        int lowLim;
        int recALength;
        int hiLim;
        int numTrans;
        int numCom;
        int recBLength;
        int yl1;
        int yiSt;
        int numSimi;
        int i;
        int j;
        int k;


        //Index of the last character in recA
        k = recA.length() - 1;
        
        // Identify the strings to be compared by stripping off all leading and
        // trailing spaces.
        for (j = 0; ((recA.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        for (i = k; ((recA.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        //Index of the last character in recA
        k = recB.length() - 1;
        
        // Start and end indices of field from candidate file
        recALength = i + 1 - j;
        yiSt = j;
        
        for (j = 0; ((recB.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        for (i = k; ((recB.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        recBLength = i + 1 - j;
        
        // If both fields have less than 4 characters, return a zero weight.
        // Subject to review
        if ((recALength < 4) && (recBLength < 4)) {
            return 0.0;
        }
        
        recAHold.setLength(0);
        recBHold.setLength(0);
        
        // Copy the trimed fields into these 2 variables
        EmeUtil.strncat(recAHold, recA, yiSt, recALength);
        EmeUtil.strncat(recBHold, recB, j, recBLength);
        
        // Determine which field is longer
        if (recALength > recBLength) {
            
            searchRange = recALength;
            minv = recBLength;
            
        } else {
            searchRange = recBLength;
            minv = recALength;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }
        
        recAFlag.setLength(0);
        recBFlag.setLength(0);
        
        // Blank out the flags
        EmeUtil.strncat(recAFlag, NULL50, searchRange);
        EmeUtil.strncat(recBFlag, NULL50, searchRange);

        
        // Is lower-side of the middle of the largest field     
//        searchRange = (searchRange / 2) - 1;
        searchRange = (searchRange >> 1) - 1;        

        // Conditionally convert all lower case characters to upper case.
        if (indC[1] == 0) {
            
            // Transform all lowercase characters of A-field to uppercase
            for (i = 0; i < recALength; i++) {
                
                if (Character.isLowerCase(recAHold.charAt(i))) {
                    
                    recAHold.setCharAt(i, Character.toUpperCase(
                    recAHold.charAt(i)));
                }
            }
            
            // Transform all lowercase characters of B-field to uppercase
            for (j = 0; j < recBLength; j++) {
                
                if (Character.isLowerCase(recBHold.charAt(j))) {
                    
                    recBHold.setCharAt(j, Character.toUpperCase(recBHold.charAt(j)));
                }
            }
        }
        
        // Looking only within the search range, count and flag the matched pairs.
        numCom = 0;
        // Index of last char in field from B-file
        yl1 = recBLength - 1;
        
        // Loop over chars of the field from A-file
        for (i = 0; i < recALength; i++) {
            
            lowLim = (i >= searchRange) ? i - searchRange : 0;
            
            hiLim = ((i + searchRange) <= yl1) ? (i + searchRange) : yl1;
            
            for (j = lowLim; j <= hiLim; j++) {
                
                if ((recBFlag.charAt(j) != '1')
                && (recBHold.charAt(j) == recAHold.charAt(i))) {
                    
                    recBFlag.setCharAt(j, '1');
                    recAFlag.setCharAt(i, '1');
                    numCom++;
                    break;
                }
            }
        }
        
        // If there are no characters in common, return zero agreement.
        // The definition of 'common' is that the agreeing characters must be
        // within � the length of the shorter string min (s1, s2).
        if (numCom == 0) {
            return 0.0;
        }
        
        // Count the number of transpositions. The definition of 'transposition' is
        // that the character from one string is out of order with the corresponding
        // common character from the other string (in the sens defined above).
        k = 0;
        numTrans = 0;
        
        // Loop over the A-file field chars to search for transpositions
        for (i = 0; i < recALength; i++) {
            
            // If the actual character has some corresponding commons, then
            // loop over the chars of the B-file field.
            if (recAFlag.charAt(i) == '1') {
                
                for (j = k; j < recBLength; j++) {
                    
                    // If there is a common between indices (i,j), then break the
                    // inner loop and go for next i and start search from j+1.
                    if (recBFlag.charAt(j) == '1') {
                        
                        k = j + 1;
                        break;
                    }
                }
                
                // If char from A-field has no corresponding common from B-field,
                // increment the number of transposition
                if (recAHold.charAt(i) != recBHold.charAt(j)) {
                    
                    numTrans++;
                }
            }
        }
        
        // 1/2 the transpositions
//        numTrans = numTrans / 2;
        numTrans = numTrans >> 1;
                
        if (ind == 1) { 
        
            // Main weight computation.
            // Since the initial formula was: 1/3(#common/length(str1) + #common/
            // length(str2) + 1/2*(#transposition / #common).
            weight = numCom / ((double) recALength) + numCom / ((double) recBLength)
                     + (((double) (numCom - numTrans)) / ((double) numCom));
                     
            // Multiply by 1/3
            weight = weight / 3.0;

            return weight;
        }



        // First inhancement by McLaughlin (1993). Adjust for similarities in
        // nonmatched characters.
        // Similar characters might occur because of scanning errors ('1' versus 'l')
        // or keypunch ('V' versus 'C'). The #common (in Jaro) gets // increased by
        // 0.3 for each similar character
        numSimi = 0;
        
        // Authorize the use of similar characters weightings
        if (indC[1] == 0) {
            
            // Test if the min(str1, str2) is larger than the number of common
            if (minv > numCom) {
                
                for (i = 0; i < recALength; i++) {
                    
                    // Check if the character does not have a commom and is inRange.
                    // See def, at the end.
                    if ((recAFlag.charAt(i) == ' ') && inRange(recAHold.charAt(i))) {
                        
                        // Loop aver the corresponding B-field
                        for (j = 0; j < recBLength; j++) {
                            
                            // Check also if the character does not have
                            // a commom and is inRange.
                            if ((recBFlag.charAt(j) == ' ') && inRange(recBHold.charAt(j))) {
                                
                                // Test if the pair of chars is in the list of
                                // similar characters defined in array adjwt
                                if (matchVar.adjwt[recAHold.charAt(i)][recBHold.charAt(j)] > 0) {
                                    
                                    // Increment the number of similar pairs
                                    // Need review
                                    numSimi +=
                                    matchVar.adjwt[recAHold.charAt(i)][recBHold.charAt(j)];
                                    
                                    // Flag the B-field. How about the A-field?
                                    recBFlag.setCharAt(j, '2');
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        // Update the number of commons by replacing them with:
        // #common + 0.3 * #similar
        numSim = (numSimi) / 10.0 + numCom;
        
        // Main weight computation.
        // Since the initial formula was: 1/3(#common/length(str1) + #common/
        // length(str2) + 1/2*(#transposition / #common).
        // The new formula (adding similar chars) is:
        // 1/3((#common + 0.3*simi)/length(str1) + (#common + 0.3*simi)/length(str2)
        // + 1/2*(#transposition / #common).
        // DO NOT UNDERSTAND THE LAST PART OF THE FORMULA. A PROBLEM?
        weight = numSim / (recALength) + numSim / (recBLength) 
                 + ((double) (numCom - numTrans)) / ((double) numCom);
        
        // Multiply by 1/3
        weight = weight / 3.0;
        

        // Second enhancement due to Winkler (1990), based on Pollock & Zamora
        // empirical study on very large chemical data (1984).
        // Continue to boost the weight if the strings are similar
        if (weight > 0.7) {
            
            // Adjust for having up to the first 4 characters in common
            // consider up to the first 4 characters
            j = (minv >= 4) ? 4 : minv;
            
            // Consider the first j characters that agree and are not numbers(?)
            for (i = 0; ((i < j) && (recAHold.charAt(i) == recBHold.charAt(i))
            && (notNumber(recAHold.charAt(i)))); i++) {
                continue;
            }
            
            // Increase the weight of agreement  monotonically with the number of
            // agreeing fields. Need review.
            if (i != 0) {
                weight += i * 0.1 * (1.0 - weight);
            }
            
            // The third and final enhancement adjusts the string comparator value if
            // the strings are longer than 6 characters and that more than half the
            // characters after the 4th one agree. This final improvement is based on
            // comparison of tens of thousands of pairs of first names, last names
            // and street names on truly matching records (done by Lynch & Winkler).
            
            // Test if we want to add this enhancement. Test also that the min length
            // is more than 4 and that nmuber of common is more thaa the first
            // agreeing chacters. The last test need to be reviwed
            if ((indC[0] == 0) && (minv > 4) && (numCom > i + 1)
            && ((2 * numCom) >= (minv + i))) {
                
                // The first character should not be a digit
                if (notNumber(recAHold.charAt(0))) {
                    
                    weight += (1.0 - weight) * ((numCom - i - 1.0)
                    / ((recALength + recBLength - i * 2.0 + 2.0)));
                }
            }
        }
        
        return weight;
    }
    
    // Need to be reviewed
    private static boolean inRange(char c) {
        
        return ((c > 0)  && (c < 91)
        || (c > 191)  && (c < 256));  
    }
    
    // Check for characters that are not numbers [0,9]
    private static boolean notNumber(char c) {
        
        return !Character.isDigit(c);
    }
}
