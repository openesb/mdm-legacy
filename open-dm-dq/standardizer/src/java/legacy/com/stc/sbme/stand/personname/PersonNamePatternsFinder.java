/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.personname;

import java.util.ArrayList;

import com.stc.sbme.api.SbmePersonName;
import com.stc.sbme.stand.util.TableSearch;
import com.stc.sbme.util.EmeUtil;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PersonNamePatternsFinder {
    
    private static final String UNKNOWNTYPE = SbmePersonName.UNKNOWNTYPE;
    private static final String FIRSTNAME = SbmePersonName.FIRSTNAME;
    private static final String MIDDLENAME = SbmePersonName.MIDDLENAME;
    private static final String LASTNAME = SbmePersonName.LASTNAME;
    private static final String TITLE = SbmePersonName.TITLE;
    private static final String GENDER = SbmePersonName.GENDER;
    private static final String GENSUFFIX = SbmePersonName.GENSUFFIX;
    private static final String OCCUPSUFFIX = SbmePersonName.OCCUPSUFFIX;
    private static final String LASTPREFIX = SbmePersonName.LASTPREFIX;
    private static final String BUSINESSORRELATED = SbmePersonName.BUSINESSORRELATED;
    private static final String PERSONSTATUS = SbmePersonName.PERSONSTATUS;
 
    /**
     * an index to the parson name's type
     */
    public String[] indexField = {UNKNOWNTYPE, TITLE, FIRSTNAME, MIDDLENAME, 
                                  LASTPREFIX, LASTNAME, GENSUFFIX, OCCUPSUFFIX, 
                                  BUSINESSORRELATED, PERSONSTATUS};
    
    
    /**
     * Define a factory method that return an instance of the class
     * @return an instance of the PersonNamePatternsFinder class
     */
    public static PersonNamePatternsFinder getInstance() {
        return new PersonNamePatternsFinder();
    }
    
    
    /**
     * Search for the best pattern (or patterns if more than one person is in a
     * string) for a one-string person name. Note that such pattern is domain-
     * independent because the structure of names is quasi-universal
     *
     * @param orderedKeys a list of keys
     * @param orderedValues a list of values
     * @param fieldList the fields' types
     * @param sVar an instance of the BusinessStandVariables class
     */
    public void findPatterns(StringBuffer[][] orderedKeys, StringBuffer[][] orderedValues,
    ArrayList fieldList, PersonNameStandVariables sVar) {
        
       /*
        * String that holds the association of fields' types represented by
        * numbers from 1 and up
        */
        String typesAggreg;
        
        
        /* Call the method that constructs the string of types aggregation */
        typesAggreg = groupFieldsTypes(fieldList, sVar);
        
        
       /*
        * Search for the best pattern linked to the association of types
        * and return it
        */
        getBestPatterns(orderedKeys, orderedValues, typesAggreg, fieldList, sVar);
    }
    
    /**
     * Search for the best pattern (or patterns if more than one person is in a
     * string) for a one-string person name. Note that such pattern is domain-
     * independent because the structure of names is quasi-universal
     *
     * @param orderedKeys a list of keys
     * @param orderedValues a list of values
     * @param fieldList the fields' types
     * @param sVar an instance of the BusinessStandVariables class
     */
    public void findPatterns(StringBuffer[][][] orderedKeys, StringBuffer[][][] orderedValues,
    ArrayList[] fieldList, PersonNameStandVariables sVar) {
        
        /* Number of records */
        int numberOfRecords = fieldList.length;
        
       /*
        * String that holds the association of fields' types represented by
        * numbers from 1 and up
        */
        String[] typesAggreg = new String[numberOfRecords];
        
        orderedValues = new StringBuffer[numberOfRecords][][];
        
        orderedKeys = new StringBuffer[numberOfRecords][][];
        
        int i;
        
        
            /* For each record, get the best pattern(s) */
        for (i = 0; i < numberOfRecords; i++) {
            
           /* Call the method that constructs the string of types aggregation*/
            typesAggreg[i] = groupFieldsTypes(fieldList[i], sVar);
            
           /*
            * Search for the best pattern linked to the association of types
            * and return it
            */
            getBestPatterns(orderedKeys[i], orderedValues[i], typesAggreg[i], fieldList[i], sVar);
        }
    }
    
    
    /**
     *
     *
     * @param fieldList
     * @param sVar
     * @return a String
     */
    private String groupFieldsTypes(ArrayList fieldList, PersonNameStandVariables sVar) {
        
        int i;
        
        /* Number of records */
        int numberOfFields = fieldList.size();
        
        /* Define a string that holds the grouping of the field's types */
        StringBuffer fieldsTypes = new StringBuffer();
        
        // Loop over all the fields in the present record
        for (i = 0; i < numberOfFields; i++) {
            
            /* Append the wordtype associated with the present field */
            EmeUtil.strcat(fieldsTypes, sVar.wordType[i]);
        }
        return fieldsTypes.toString();
    }
    
    
    /**
     *
     *
     * @param fieldTypes
     * @param typesAggreg
     * @param sVar
     */
    private void getBestPatterns(StringBuffer[][] orderedKeys, StringBuffer[][] orderedValues, String typesAggreg,
    ArrayList fieldList, PersonNameStandVariables sVar) {
        
        /* The indexes of the found patterns in the table */
        int[] ind = new int[3];
        
        /* Number of matched patterns */
        short numberMatch = 0;
        
        /* Number of records */
        int numberOfFields = fieldList.size();
        
        int i;
        int j;
        int ll;
        int indx;
        int index = -1;
        
        /* Inintiate the indexes to default */
        for (i = 0; i < 3; i++) {
            ind[i] = -1;
        }
        
        
        /* Search for the best pattern for the present record in the pattern table */
        if ((index = TableSearch.searchTable(typesAggreg, 1, sVar.nbPatt, sVar.ptrn1)) >= 0) {
            
           /*
            * Check if more than one pattern is found. That means that there
            * are more than one person in the string, and change the indexes
            * accordingly.
            */
            if ((index > 0) && EmeUtil.strcmp(sVar.ptrn1[index], sVar.ptrn1[index - 1]) == 0) {
                
                ind[0] = index - 1;
                ind[1] = index;
                
                /* The number of person inside the record */
                numberMatch = 2;
                
                /* Update the number of patterns found */
                sVar.nbStan += 2;
                
            } else if (EmeUtil.strcmp(sVar.ptrn1[index], sVar.ptrn1[index + 1]) == 0) {
                
                ind[0] = index;
                ind[1] = index + 1;
                
                /* The number of person inside the record */
                numberMatch = 2;
                
                /* Update the number of patterns found */
                sVar.nbStan += 2;
                
            } else {
                numberMatch = 1;
                
                /* Update the number of patterns found */
                sVar.nbStan++;
            }
        }
        
            /* Loop over the different patterns found */
        for (j = 0; j < numberMatch; j++) {
            
            /*
             * Loop over the 10 integers that define the right combination
             * of fields for each pattern
             */
            for (i = 0; i < 10; i++) {
                
                
                        /* Consider only the strictly positive integers */
                if ((ll = sVar.ptrn2[ind[j]][i]) > 0) {
                    
                   /*
                    * table index 1->n, while java index 0->n-1
                    * Decrement to be conform with the indexing convention
                    */
                    ll--;
                    
                            /* Defines the title */
                    if (i == 0) {
                        
                       /*
                        * Copy the title key and value into into their
                        * respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(TITLE);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                    } else if (i == 1) {
                        // Defines first name
                        
                       /*
                        * Copy the first name key and value into into their
                        * respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(FIRSTNAME);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                    } else if (i == 2) {
                        // Defines middle name
                        
                       /*
                        * Copy the middle name key and value into into their
                        * respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(MIDDLENAME);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                    } else if (i == 3) {
                        // Defines last name prefix
                        
                       /*
                        * Copy the last name prefix key and value into into
                        * their respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(LASTPREFIX);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                       /*
                        * Copy the last name prefix key and value into into
                        * their respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i + 1].append(LASTNAME);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i + 1].append((String) fieldList.get(i + 1));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                    } else if (((i != 4) && (sVar.ptrn2[ind[j]][3] != 0))
                    || ((i == 4) && (sVar.ptrn2[ind[j]][3] == 0))) {
                        // Defines last name without a prefix
                        
                       /*
                        * Copy the last name key and value into into their
                        * respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(LASTNAME);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                    } else if (i == 5) {
                        // Defines generational suffixes
                        
                       /*
                        * Copy the generational suffixes prefix key and
                        * value into into their respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(GENSUFFIX);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].toString());
                        
                    } else if (i == 6) {
                        // Defines occupational suffixes
                        
                       /*
                        * Copy the occupational suffixes prefix key and
                        * value into into their respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(OCCUPSUFFIX);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].
                        toString());
                        
                    } else if (i == 7) {
                        // Defines business Or Related entities
                        
                       /*
                        * Copy the business Or Related prefix key and value
                        * into into their respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(BUSINESSORRELATED);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].
                        toString());
                        
                    } else if (i == 8) {
                        // Still empty. Can be used later
                        
                    } else if (i == 9) {
                        // Defines a person status
                        
                       /*
                        * Copy the last name prefix key and value into into their
                        * respective variables
                        */
                        orderedKeys[j][i] = new StringBuffer();
                        orderedKeys[j][i].append(PERSONSTATUS);
                        
                        orderedValues[j][i] = new StringBuffer();
                        orderedValues[j][i].append((String) fieldList.get(i));
                        
                                    /* Also include the type of the field */
                        sVar.sWordType[j][i].replace(0, 1, sVar.wordTypeFound[i].
                        toString());
                        
                    }
                    
                } else {
                    
                   /*
                    * Copy the last name prefix key and value into into their
                    * respective variables
                    */
                    orderedKeys[j][i] = new StringBuffer();
                    orderedKeys[j][i].append(UNKNOWNTYPE);
                    
                    orderedValues[j][i] = new StringBuffer();
                    orderedValues[j][i].append((String) fieldList.get(i));
                    
                                /* Also include the type of the field */
                    sVar.sWordType[j][i].setCharAt(0, 'F');
                    
                }
                
                                /* Assign a value to gender */
                orderedKeys[j][10] = new StringBuffer();
                orderedKeys[j][10].append(GENDER);
                
                orderedValues[j][10] = new StringBuffer();
                orderedValues[j][10].append(sVar.gender[j]);
                
            }
        }
        
        /* If no pattern was found */
        if (numberMatch == 0) {
            
            for (i = 0; i < numberOfFields; i++) {
                
                indx = Character.getNumericValue(typesAggreg.charAt(i));
                
               /*
                * Copy the key and value of the different fields into their
                * respective variables
                */
                orderedKeys[0][i] = new StringBuffer();
                orderedKeys[0][i].append(indexField[indx]);
                
                orderedValues[0][i] = new StringBuffer();
                orderedValues[0][i].append((String) fieldList.get(i));
                
                            /* Also include the type of the field */
                sVar.sWordType[0][i].setLength(0);
                sVar.sWordType[0][i].append(sVar.wordTypeFound[i]);
            }
            

            /* Assign a value to gender */
            orderedKeys[0][10] = new StringBuffer();
            orderedKeys[0][10].append(GENDER);

            orderedValues[0][10] = new StringBuffer();
            orderedValues[0][10].append(sVar.gender[0]);
            
        }
    }
}
