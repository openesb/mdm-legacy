/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.util.ArrayList;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * This class parses the person name string into its individual components
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class AddressParser {

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
   private final int MAX_WORDS;
   private final AddressNormalizer addressNormalizer;
   private boolean isAU;
   private boolean isUS;
   private boolean isFR;
   private boolean isUK;               
   private static ArrayList baseSpecChar;              
   
   static {
      baseSpecChar = new ArrayList();
      baseSpecChar.add("-");
      baseSpecChar.add("�");
      baseSpecChar.add("#");
      baseSpecChar.add("/");
      baseSpecChar.add("&"); 
      baseSpecChar.add("|");
      baseSpecChar.add(" ");        
   }
    /**
     * Define a factory method that return an instance of the class 
     * @return a class instance
     */
    static AddressParser getInstance(String domain) {
        return new AddressParser(domain);
    }    
    
    AddressParser(String domain) {
        MAX_WORDS = ReadStandConstantsValues.getInstance(domain).getMaxWords();
        addressNormalizer = new AddressNormalizer(domain);
    }

    /**
     * Performs the parsing of the input string
     *
     * @param rec the string to be parsed
     * @param sVar the basic instance that provide access to any other
     *             used instances
     * @param lineNums the list to be populated with the line numbers of
     *                 each token
     * @throws SbmeStandardizationException an exception
     * @throws SbmeMatchEngineException an exception
     * @return an array of tokens
     */
    ArrayList performParsing(String rec, AddressStandVariables sVar, int[] lineNums, int[] numLines, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        int i;
        int[] output = new int[2];
        isAU = (domain.compareTo("AU") == 0);
        isUS = (domain.compareTo("US") == 0);
        isUK = (domain.compareTo("UK") == 0);
        isFR = (domain.compareTo("FR") == 0);        
        
        // Holds the parsed fields
        ArrayList fields = new ArrayList();

        /* Wrap the string record into a StringBuffer  */
        StringBuffer record = new StringBuffer(rec);

        /* Remove most special characters and collapse multiple spaces */
        prepIncomingAddress(record, sVar);        

        /* Separate into an array of words */
        output = separateIntoWords(record, sVar.wordArray, sVar, lineNums, domain);        
        
        
        sVar.numWords = output[0];  
        numLines[0] = output[1];      

        if (sVar.numWords > MAX_WORDS) {
            
            mLogger.error("ERROR:  address consists of " + sVar.numWords + " words\n"
                + "        which is more than the maximum number\n"
                + "        of words allowed (" + MAX_WORDS + ")\n Address: " + record);
//            throw new SbmeStandardizationException("Warning: The address record: "+record + "\n consists of " + sVar.numWords + " words"
//                + " which is more than the maximum number specified: (" +MAX_WORDS+ ")\n"
//                + " It will be trimmed at word "+MAX_WORDS);
            sVar.numWords = MAX_WORDS;     
        }
        
        // Put the array of parsed strings into the array list
        for (i = 0; i < sVar.numWords; i++) {
            fields.add(i, sVar.wordArray[i]);
        }
        
        return fields;
    }


    /**
     * Receives the address string and remove all special characters,
     * except one in the "special_characters" array, and squash any
     * consecutive blanks together.
     *
     * @param record the address string 
     * @param sVar an instance of the AddressVariables class
     * @throws SbmeStandardizationException an exception
     */
    void prepIncomingAddress(StringBuffer record, AddressStandVariables sVar)
        throws SbmeStandardizationException {

        boolean pBlank;
        int i;
        int len;
        int cnt = 0;
        int ii = 0;
        int jj = 0;
        char cStr;
        char csm;
        char csm2;        
        char csp;
        boolean removeSlash = false;
        boolean removeSlash2 = false;
        
        StringBuffer newStr = new StringBuffer();
        String sb = record.toString().trim().toUpperCase();

        /* Remove trailing blanks */
        record.setLength(0);
        record.append(sb);

        // The length of the record
        len = record.length();
        
        //Remove trailing pipe
        if (record.charAt(len - 1) == '|') {
            record.deleteCharAt(len - 1);
            len -= 1;
        }
                        
        /* 
         * Collapse multiple spaces to one space inside the record.
         * Keep more than 2 spaces at the end of the record. Otherwise, it will crash
         */
        for (i = 0; i < len; i++) {

            // Do not consider the first and last characters since the string is trimmed
            if (i > 0 && i < (len - 1)) {

                cStr = record.charAt(i);                
                csp = record.charAt(i + 1);
                
                // Keep only one pipe when there are more than one pipe that separate
                // tokens
                if (cStr == '|' && csp == '|') {
                
                    record.deleteCharAt(i);
                    i--;
                    len--;
                } else {
                
                    // Keep only one space when there are more than one space that separate
                    // tokens
                    if (Character.isWhitespace(cStr) && Character.isWhitespace(csp)) {

                        record.deleteCharAt(i);
                        i--;
                        len--;
                    }                
/*
                    // Keep only one space when there are more than one space that separate
                    // tokens
                    if (Character.isWhitespace(record.charAt(i))
                            && Character.isWhitespace(record.charAt(i + 1))) {

                        record.deleteCharAt(i);
                        i--;
                        len--;
                    }
*/                    
                }
            }
            
            // Any characters other than the alphanumeric, the basic ones used 
            // in the standardization ('-', '#', '/', '&'), the '.' and ',' that
            // are automatically removed, or the special characters defined 
            // in the config file must be removed from the string.
            cStr = record.charAt(i);
            if (!Character.isLetterOrDigit(cStr) 
                && !baseSpecChar.contains(record.substring(i, i+ 1))
                && !addressNormalizer.isSpecialChar(cStr)) {
                // Consider one special case (keep "'" when followed by a letter
                if ((cStr == '\'' || cStr == '�') && (i < len - 1) && Character.isLetter(record.charAt(i + 1))
                                && (i > 0) && Character.isLetter(record.charAt(i - 1))) {
                    continue;
                }
                record.deleteCharAt(i);
                i--;
                len--;                  
            }
        }
        
        
        // Test if the record is empty
        if (record.length() == 0) {
            return;
        }
        
        // If a token formed of a # and a numeric, separate them
        // into 2 tokens to be correctly handled inside the code.
        for (i = 0; i < len; i++) { 

            if ((record.charAt(i) == '#') && (record.substring(i).length() > 1)
                && (Character.isLetterOrDigit(record.charAt(i + 1)))) {

                record.insert(i + 1, " "); 
                len++;
            }
 
            // Handle dashes
            if (record.charAt(i) == '-' || record.charAt(i) == '�') {
                if ((i > 0) && (i < len - 2)
                && (Character.isLetterOrDigit(record.charAt(i - 1)))
                && (Character.isWhitespace(record.charAt(i + 1)))
                && (Character.isDigit(record.charAt(i + 2)))) {

                record.deleteCharAt(i + 1); 
                len--;
                }            
                if ((i > 1) && (i < len - 1)
                    && (Character.isDigit(record.charAt(i + 1)))
                    && (Character.isWhitespace(record.charAt(i - 1)))
                    && (Character.isDigit(record.charAt(i - 2)))) {
                            
                    record.deleteCharAt(i - 1); 
                    i--;
                    
                    len--;        
                } 
//              Whitespace on both sides
/*
                if ((i > 1) && (i < len - 2)
                    && (Character.isWhitespace(record.charAt(i + 1)))
                    && (Character.isDigit(record.charAt(i + 2)))
                    && (Character.isWhitespace(record.charAt(i - 1)))
                    && (Character.isDigit(record.charAt(i - 2)))) {
                            
                    record.deleteCharAt(i + 1); 
                    record.deleteCharAt(i - 1); 
                    i--;                   
                    len-=2;        
                }  
*/
                // Whitespace on both sides
                if ((i > 1) && (i < len - 2)
                    && (Character.isWhitespace(record.charAt(i + 1)))
                    && (Character.isWhitespace(record.charAt(i - 1)))) {
                    
                    if (!(i > 2 & Character.isLetter(record.charAt(0)))) {                           
                    if (Character.isDigit(record.charAt(i - 2))
                        && Character.isDigit(record.charAt(i + 2))) {
                            
                        record.deleteCharAt(i + 1); 
                        record.deleteCharAt(i - 1);                             
                    } else {
                        record.deleteCharAt(i);        
                        record.deleteCharAt(i - 1); 
                    }
                    i--;                   
                    len-=2;        
                    } else {
                        if (Character.isLetter(record.charAt(i - 2))
                            && Character.isLetter(record.charAt(i + 2))) { 
                                record.deleteCharAt(i);   
                                i--;                   
                                len-=1;                                                              
                            }                          
                    }
                }  
            }    
        }

        // Before starting character-by-character analysis, make sure that the last characters
        // are valid ones. If not, remove any to-be-removed special characters.
        i = len - 1;
        if (i >= 0) {
            cStr = record.charAt(i);
            while (!Character.isLetterOrDigit(cStr) && (cStr != '/') 
                && !addressNormalizer.isSpecialChar(cStr)) {
                    
                record.deleteCharAt(i);
                len--;              
                cStr = record.charAt(--i);                                  
            }             
        }

        
        // Recalculate the length of the record
        len = record.length();
        
        // The first character of the string
        cStr = record.charAt(0);


       /* 
        * If we are doing house number only (N) and the first char is a
        * dash, then blank it, and set the "firstdash" variable to TRUE
        */
        sVar.firstDash = ((sVar.inUsageFlag == 'N' && AddressNormalizer.firstCharDashFound(record, len))
                        ? true : false);
    
        pBlank = ((cStr == ' ') || (!Character.isLetterOrDigit(cStr)
                && (!addressNormalizer.isSpecialChar(cStr)))) ? true : false;
       
        while (ii < len) {

            cStr = record.charAt(ii);                
            
            // Handle the slash pattern for Australia locale
            if ((cStr == '/') && isAU && (ii > 0) && (ii < len - 2)) {   
                                     
                csm = record.charAt(ii - 1);
                csp = record.charAt(ii + 1);
                csm2 = record.charAt(ii + 2);
                
                if (Character.isLetterOrDigit(csm) && Character.isDigit(csp)) { 
                    removeSlash = true;   
                } else if (Character.isLetter(csm) && csp == 'P' && 
                (csm2 == 'O' || (csm2 == ' ' & ((ii < len - 3) && record.charAt(ii + 3) == 'O')))) {
                    removeSlash2 = true;                 
                // If we have c/o, then keep the slash
                } else if ((csm == 'C' && csp == 'O') || (csm == 'C' && (csp == '-' || csp == '�'))
                    || (csm == 'C' && csp == 'V') || (csm == 'C' && csp == 'P')) {                                     
                
                // If there are at least 2 characters before the slash 
                // and the closeset one is a whitespace 
                } else if (csm == ' ' && (ii > 1)) {
                    csm2 = record.charAt(ii - 2);
                    
                    // If the character before the whitespace is a digit
                    if (Character.isLetterOrDigit(csm2)) {

                        removeSlash = true;              
                        record.deleteCharAt(ii - 1);    
                        ii--;
                        jj--;
                        len--;  
                        cStr = record.charAt(ii);                    
                    }
                // If the character after the slash is a whitespace
                // and there is at least one character after this whitespace
                } else if (csp == ' ' && (ii < len - 1)) {
                    csm2 = record.charAt(ii + 2);
                    
                    // If the character after the whitespace is a digit
                    if (Character.isDigit(csm2)) {
                        removeSlash = true;
                        record.deleteCharAt(ii + 1);    
                        len--; 
                         
                    // If the character after the whitespace is a letter             
                    } else if (Character.isLetter(csm2)) {
                        record.deleteCharAt(ii);
                        len--;  
                        cStr = record.charAt(ii);                          
                    }      
                } else {
//                    removeSlash = true;                   
                }
            }
            
            
            // Handle the slash pattern for US locale
            if ((cStr == '/') && isUS && (ii > 0) && (ii < len - 2)) {   
                                     
                csm = record.charAt(ii - 1);
                csp = record.charAt(ii + 1);
                                
                // If we have c/o, then keep the slash
                if (csm == 'C' && csp == 'O') {
                     
                                   
                // If there are at least 2 characters before the slash 
                // and the closeset one is a whitespace 
                } else if (csm == ' ' && (ii > 1)) {
                    csm2 = record.charAt(ii - 2);
                    
                    // If the character before the whitespace is a digit
                    if (Character.isDigit(csm2)) {

                        removeSlash = true;              
                        record.deleteCharAt(ii - 1);    
                        ii--;
                        jj--;
                        len--;  
                        cStr = record.charAt(ii);                    
                    }
                // If the character after the slash is a whitespace
                // and there is at least one character after this whitespace
                } else if (csp == ' ' && (ii < len - 1)) {
                    csm2 = record.charAt(ii + 2);
                    
                    // If the character after the whitespace is a digit
                    if (Character.isDigit(csm2)) {
                        removeSlash = true;
                        record.deleteCharAt(ii + 1);    
                        len--; 
                         
                    // If the character after the whitespace is a letter             
                    } else if (Character.isLetter(csm2)) {
                        record.deleteCharAt(ii);
                        len--;  
                        cStr = record.charAt(ii);                          
                    }
                // If the index is at least 3 characters away from the end and
                // the surounding characters are digits and the second character on the right
                // is a whitespace and the digits are any combination of (1,3) and (2,3,4,8)
                // for US addresses use only    
                } else if ((ii < (len - 2)) && Character.isWhitespace(record.charAt(ii + 2)) 
                    && Character.isDigit(csm) && Character.isDigit(csp) 
                    && (csm == '1' || csm == '3')
                    && (csp == '2' || csp == '3' || csp == '4' || csp == '8')) {
                    
                    // Check if the previous token was a directional prefix (SW, SE,...)
                    if (ii > 5 
                        && (record.substring(ii - 4, ii - 2).compareTo("SE") == 0
                            || record.substring(ii - 4, ii - 2).compareTo("SW") == 0
                            || record.substring(ii - 4, ii - 2).compareTo("NE") == 0
                            || record.substring(ii - 4, ii - 2).compareTo("NW") == 0)
                        && record.charAt(ii - 5) == ' ') {
                        
                        // Switch the direction with the fraction
                        String temp = record.substring(ii - 4, ii - 2);
                        record.delete(ii - 4, ii - 1);
                        newStr.delete(jj - 4, jj - 1);
                        record.insert(ii, temp);
                        newStr.append(record.substring(ii - 3, ii));
                        record.insert(ii + 2, ' ');  
                        cStr = record.charAt(ii);  

                    } else if (ii > 4 
                        && (record.charAt(ii - 3) == 'S'
                            || record.charAt(ii - 3) == 'E'
                            || record.charAt(ii - 3) == 'N'
                            || record.charAt(ii - 3) == 'W')
                        && record.charAt(ii - 4) == ' ') {
                        
                        // Switch the direction with the fraction
                        char temp = record.charAt(ii - 3);
                        record.delete(ii - 3, ii - 1);
                        newStr.delete(jj - 3, jj - 1);
                        record.insert(ii, temp);
                        newStr.append(record.substring(ii - 2, ii));
                        record.insert(ii, ' ');  
                        cStr = record.charAt(ii);                                                                                         
                    }
                                                       
                // Don't remove the slash        
                } else {
//                    removeSlash = true;                   
                }
            }            
            // Handle the slash pattern for FR locale
            if ((cStr == '/' || cStr == '�') && isFR && (ii > 0) && (ii < len - 2)) {   
                                     
                csm = record.charAt(ii - 1);
                csp = record.charAt(ii + 1);
                
                // If we have c/o, then keep the slash
//                if (csm == 'C' && csp == 'O') {
                if ((Character.isDigit(csm) || csm == 'A' || csm == 'B' || csm == 'C' || csm == 'D' 
                    || csm == 'T' || csm == 'Q') && Character.isDigit(csp)) { 
                    removeSlash = true;                                   
                                  
                // If there are at least 2 characters before the slash 
                // and the closeset one is a whitespace 
                } else if (csm == ' ' && (ii > 1)) {
                    csm2 = record.charAt(ii - 2);
                    
                    // If the character before the whitespace is a digit or a specific letter
                    if (Character.isDigit(csm2) || csm2 == 'A' || csm2 == 'B'
                        || csm2 == 'C' || csm2 == 'D' || csm2 == 'T' || csm2 == 'Q') {

                        removeSlash = true;              
                        record.deleteCharAt(ii - 1);    
                        ii--;
                        len--;  
                        cStr = record.charAt(ii);                    
                    }
                    
                    if (csp == ' ' && (ii < len - 1)) {
                        csm2 = record.charAt(ii + 2);
                    
                        // If the character after the whitespace is a digit, then
                        // remove both the slash and the digit after that
                        if (Character.isDigit(csm2)) {
                            record.deleteCharAt(ii + 1);    
                            len--;
                        }
                    }                     
                // If the character after the slash is a whitespace
                // and there is at least one character after this whitespace
                } else if (csp == ' ' && (ii < len - 1)) {
                    csm2 = record.charAt(ii + 2);
                    
                    // If the character after the whitespace is a digit, then
                    // remove both the slash and the digit after that
                    if (Character.isDigit(csm2) || csm2 == 'A' || csm2 == 'B'
                        || csm2 == 'C' || csm2 == 'D' || csm2 == 'T' || csm2 == 'Q') {
                        removeSlash = true;
                        record.deleteCharAt(ii + 1);    
                        len--; 
                         
                    // If the character after the whitespace is a letter             
                    } else if (Character.isLetter(csm2)) {
                        record.deleteCharAt(ii);
                        len--;  
                        cStr = record.charAt(ii);                          
                    }
                         
                // Don't remove the slash        
//                } else {
//                    removeSlash = true;                   
                }
            }
                                    
           
            if (!Character.isLetterOrDigit(cStr) && (cStr != '/') && (!addressNormalizer.isSpecialChar(cStr))) {
                if (ii == 0) {
                    record.deleteCharAt(ii);
                    len--;  
                    continue;                    
                }
                record.setCharAt(ii, ' ');
            }
            
            if (cStr == ' ' && pBlank) {

                cStr = record.charAt(ii++);
                continue;
            }

            if (sVar.inUsageFlag == 'N' && cStr == ' ') {

                /* For HN only, squash "123 e" together */
                csm = record.charAt(ii - 1);
                csp = record.charAt(ii + 1);

                if ((Character.isDigit(csm) && Character.isLetter(csp)) 
                    || (Character.isDigit(csp) && Character.isLetter(csm))) {

                   cStr = record.charAt(ii++);
                   continue;
                }
            }
            
            // CASE WHERE WE HAVE ST/PO OR ST/P O
            if (removeSlash2) {
                newStr.insert(jj, ' ');
                jj++;
                removeSlash2 = false;                                
            
            } else if (removeSlash) {

                newStr.insert(jj, ' ');
                
                // Add the string "UNIT " at the start of the record if not there
                if (isAU && Character.isDigit(newStr.charAt(0)) ||
                    newStr.substring(0, newStr.length()).trim().length() == 0) {
                    newStr.insert(0, "UNIT ");
                    
                    jj+=6;                    
                } else if (isFR) {     
                    // Remove the numeric after the slash (since in France, 12/13 
                    // means only 12)
                    if ((ii < len - 3) && record.charAt(ii + 2) == ' ') {
                        // Remove the one numeric character
                        record.deleteCharAt(ii + 1); 
                        len--;                                    
                    } else if ((ii < len - 4) && Character.isDigit(record.charAt(ii + 2))
                        && record.charAt(ii + 3) == ' ') {
                                           
                        // Remove both numeric characters
//                        record.deleteCharAt(ii + 1); 
//                        record.deleteCharAt(ii + 1);
                        record.delete(ii + 1, ii + 3);
                        len-=2;  
                    } else if ((ii < len - 5) && Character.isDigit(record.charAt(ii + 2))
                        && Character.isDigit(record.charAt(ii + 3)) 
                        && record.charAt(ii + 4) == ' ') {   
                                        
                        // Remove the three numeric characters
//                        record.deleteCharAt(ii + 1); 
//                        record.deleteCharAt(ii + 1);
//                        record.deleteCharAt(ii + 1);
                        record.delete(ii + 1, ii + 4);
                        len-=3;  
                    } else if ((ii < len - 6) && Character.isDigit(record.charAt(ii + 2))
                        && Character.isDigit(record.charAt(ii + 3)) 
                        && record.charAt(ii + 5) == ' ') {   
                        // Remove the three numeric characters
//                        record.deleteCharAt(ii + 1); 
//                        record.deleteCharAt(ii + 1);
//                        record.deleteCharAt(ii + 1);
//                        record.deleteCharAt(ii + 1);
                        record.delete(ii + 1, ii + 5);
                        len-=4;  
                    }                                                                                                
                } else {
                    jj++;
                }
                
                removeSlash = false;

            } else {          
                newStr.insert(jj, cStr);
                pBlank = (newStr.charAt(jj) == ' ') ? true : false;
                jj++;
            }

            if (ii == len - 1) {
                break;
            }          
            cStr = record.charAt(++ii);

            cnt++;
        }
        
        
        // Replace record with the newStr
        record.setLength(0);
        record.append(newStr);
    }


    /**
     *  separateIntoWords is sent the prepped address string and
     *  an array of Strings.  The array is filled with
     *  words delineated by spaces, special characters, or changes
     *  from alpha to digit, or vica versa.  The one exception
     *  to the last statement is "1st" and "3rd".
     *  For example, if "123 1st" is the incoming address, then
     *  we are pretty sure that "1st" is the street name, hence
     *  we never want to split "1" and "st", which could
     *  subsequently identify "st" as a type.
     *
     * @param record the input string
     * @param wordArray aaa
     * @param sVar an instance of the AddressVariables class
     * @return <code>int</code> representing the number of tokens forming the
     * address string.
     * @exception <code>SbmeStandardizationException</code> an exception
     * @exception <code>SbmeMatchEngineException</code> an exception
     */
    private int[] separateIntoWords(StringBuffer record, StringBuffer[] wordArray,
                                  AddressStandVariables sVar, int[] lineNums, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException {

        char prevch = ' ';
        char cStr = ' ';
        int len;
        int i;
        int sti = 0;
        int numWrds = 0;
        int wa = 0;
        int lineNum = 1;
        boolean alphaNum;
        boolean numAlpha;
        String tempS;
        String tempS1;
        int[] output = new int[2];
        boolean dFR = domain.compareTo("FR") == 0;
        String ST;
        String RD;                
        String st;
        String rd;
        String RD1;        
        String RD2;        
        String rd1;
        
        if (dFR) {
            ST = "1ER";
            RD = "�ME"; 
            RD1 = "EME";                   
            RD2 = "E ";                               
            st = "1er";
            rd = "�me"; 
            rd1 = "eme";                         
        } else {
            ST = "1ST";
            RD = "3RD";        
            st = "1st";
            rd = "3rd";
            RD1 = "   "; 
            RD2 = "   "; 
            rd1 = "   ";                                     
        }
        
       /*
        * For HN only addresses, if the first non-blank char found was a dash
        * then make the 1st word in the array be "-".
        */
        for (i = 0; i < MAX_WORDS; i++) {
            sVar.wordSplits[i] = 0;
        }


        if (sVar.firstDash) {

            wordArray[0] = new StringBuffer(2);

            EmeUtil.sprintf(wordArray[wa++], "%.2s", "-");
            numWrds++;
        }
        
        // The length of the record after we trim it.
        len = AddressClueWordsTable.trimln(record);

        for (i = 0; i < len; i++) {
            boolean incrLineNum = false;
            cStr = record.charAt(i);

            if (cStr == '|') {
                incrLineNum = true;
                cStr = ' ';
                record.setCharAt(i, ' ');
            } 
            
            if (i == 0 || addressNormalizer.isSpecialChar(cStr)) {
                prevch = cStr;
                continue;
            }
            
            // Handle complex alphanumeric or numericAlpha where a '-', a "*" 
            // could separate the alpha from the numeric
            alphaNum = (Character.isLetter(prevch) && Character.isDigit(cStr)) ? true : false;

            numAlpha = (Character.isDigit(prevch) && Character.isLetter(cStr)) ? true : false;

            tempS = record.substring(i - 1);
            tempS1 = record.substring(i);

            if ((cStr == ' ') /*|| (addressNormalizer.isSpecialChar(cStr))*/
               /* || (addressNormalizer.isSpecialChar(prevch))*/
                || (alphaNum) 
                || (numAlpha           
                    && ((EmeUtil.strncmp(tempS, st, 3) != 0) 
                    && (EmeUtil.strncmp(dFR?tempS1:tempS, rd, 3) != 0)
                    && (dFR?EmeUtil.strncmp(tempS1, rd1, 3) != 0:true))                                         
                    && ((EmeUtil.strncmp(tempS, ST, 3) != 0)
                    && (EmeUtil.strncmp(dFR?tempS1:tempS, RD, 3) != 0)
                    && (dFR?EmeUtil.strncmp(tempS1, RD1, 3) != 0:true)
                    && (dFR?EmeUtil.strncmp(tempS1, RD2, 2) != 0:true))                               
                    && ((i == len - 1)? true : (record.charAt(i + 1) != '/')))) { 
        

                if (/*!addressNormalizer.isSpecialChar(cStr) ||*/ (i != sti)) {

                    wordArray[wa] = new StringBuffer();
                    lineNums[wa] = lineNum;
                    wordArray[wa++].append(record.substring(sti, i));
                    

                    if (alphaNum || numAlpha) {


                        if (sVar.wordSplits[numWrds] == 0) {
                            sVar.wordSplits[numWrds] = 1;
                            
                        } else {
                            sVar.wordSplits[numWrds] = 2;
                        }
                        
                        sVar.wordSplits[numWrds + 1] = 1;
                    }
                    numWrds++;
                }
             
                sti = (cStr == ' ') ? i + 1 : i;
            }
            prevch = cStr;
            if (incrLineNum) {
                lineNum++;
            }
        }

        wordArray[wa] = new StringBuffer();
        lineNums[wa] = lineNum;
        wordArray[wa++].append(record.substring(sti, i));    
        
        numWrds++;

        // return the number of tokens in the string
        output[0] = numWrds;
        output[1] = lineNum;
        
        return output;
    }   
}
