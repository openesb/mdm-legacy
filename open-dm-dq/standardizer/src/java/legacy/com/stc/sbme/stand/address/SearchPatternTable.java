/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;

/**
 * Searches the pattern table for the pattern string, "string". 
 * NOTE: if consecutive AU input tokens are contained in the pattern string, then 
 * they are squashed together before searching. 
 *  
 * @author Sofiane Ouaguenouni 
 * @version $Revision: 1.1.2.1 $
 */
class SearchPatternTable {

    private final int MAX_INPUT_TOKENS;
    private final int MAX_PATT_SIZE;
    private final AddressPatternsTable addressPatternsTable;

    SearchPatternTable(String domain) {
        MAX_INPUT_TOKENS = ReadStandConstantsValues.getInstance(domain).getMaxInputTokens();
        MAX_PATT_SIZE = ReadStandConstantsValues.getInstance(domain).getMaxPattSize();
        addressPatternsTable = AddressPatternsTable.getInstance(domain);
    }
    
    /**
     *  
     *
     * @param instring input string
     * @param ip aaa
     * @param op aaa
     * @param usf aaa
     * @param state aaa
     * @param zip aaa
     * @throws SbmeMatchEngineException exception
     * @return a PatternRegistry object
     */
    protected PatternTable searchPatternTable(StringBuffer instring, StringBuffer ip,
            StringBuffer op, char usf, StringBuffer state, StringBuffer zip)
        throws SbmeMatchEngineException {
        
        int[] aUarr = new int[MAX_INPUT_TOKENS];
        int[] nLarr = new int[MAX_INPUT_TOKENS];

        StringBuffer srch = new StringBuffer(MAX_PATT_SIZE);
        StringBuffer hout = new StringBuffer(MAX_PATT_SIZE);
        StringBuffer string = new StringBuffer(MAX_PATT_SIZE);
        StringBuffer tOp = new StringBuffer(MAX_PATT_SIZE);


        int i;
        int j;
        int totNumIntToks;
        int numintoks;
        int numouttoks;
        int cnt = 0;
        int cnt2;

        boolean multiAUs = false;
        boolean foundNL = false;
        
        PatternTable temp;
        PatternTable retpat;


        for (i = 0; i < MAX_INPUT_TOKENS; i++) {
        
            aUarr[i] = 0;
            nLarr[i] = 0;
        }
        
        totNumIntToks = (instring.length() + 1) / 3;

        // Save copy of input string before you do anything
        EmeUtil.memset(ip, ' ', MAX_PATT_SIZE);

        EmeUtil.strcpy(ip, instring);

        // Search for NL (null) tokens
        for (j = 0; j < totNumIntToks; j++) {
        
            if (EmeUtil.strncmp(instring, j * 3, "NL", 2) == 0) {
                nLarr[j] = 1;
                foundNL = true;
                
            } else {
                EmeUtil.sprintf(string, cnt * 3, "%.2s ", instring, j * 3);
                cnt++;
            }
        }

        // If all tokens are NLs, then return
        if (cnt == 0) {
            return null;
        }

        numintoks = (string.length() + 1) / 3;

        string.setLength((cnt - 1) * 3 + 2);

        EmeUtil.sprintf (srch, "%.2s ", string);
        
        cnt = 0;
    
        // Collapse consecutive AUs
        for (j = 1; numintoks > 1 & j < numintoks; j++) {

            if ((EmeUtil.strncmp(string, j * 3, "AU", 2) == 0) 
                && (EmeUtil.strncmp(string, j * 3, string, (j - 1) * 3, 2) == 0)) {
                
                aUarr[cnt] += 1;
                multiAUs = true;
                continue;
            }
            cnt++;
            EmeUtil.sprintf (srch, cnt * 3, "%.2s ", string, j * 3);
        }
        srch.setLength((cnt * 3) + 2);

   
        // pattern string FOUND
        if ((temp = addressPatternsTable.searchForPattern(srch, state, zip, usf)) != null) {
        
            EmeUtil.memset(op, ' ', MAX_PATT_SIZE);
            EmeUtil.memset(tOp, ' ', MAX_PATT_SIZE);

            if (multiAUs) {
            
                numouttoks = cnt + 1;
                cnt2 = 0;

                for (i = 0; i < numouttoks; i++) {
                
                    for (j = 0; j <= aUarr[i]; j++) {
                    
                        EmeUtil.sprintf(hout, cnt2 * 3, "%.2s ", temp.getOutputTokenString(), i * 3);
                        
                        cnt2++;
                    }
                }
 
                hout.setLength((cnt2 * 3) - 1);
                EmeUtil.strcpy(tOp, hout);
                
            } else {
                EmeUtil.strcpy(tOp, temp.getOutputTokenString());
            }
    
            // Reload NLs if any were sent
            cnt = 0;

            if (foundNL) {
            
                for (i = 0; i < totNumIntToks; i++) {
                
                    if (nLarr[i] == 1) {
                    
                        op.setLength(i * 3);
                        op.insert(i * 3, "NL ");
                        
                    } else {
                        EmeUtil.sprintf (op, i * 3, "%.2s ", tOp, cnt * 3);
                        cnt++;
                    }
                }
                op.setLength(i * 3);
                
            } else {
                EmeUtil.strcpy(op, tOp);
            }

            op.setLength(ip.length());

            return temp;
            
        } else {
        // pattern string NOT FOUND
            return null;
        }
    }
}
