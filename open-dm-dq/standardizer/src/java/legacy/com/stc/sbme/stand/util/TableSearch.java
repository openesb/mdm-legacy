/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.util;

import com.stc.sbme.util.EmeUtil;

/**
 * Preforms a binary search on a given table: It looks for the string
 * "data" of length "length" in table "c_table" between subscripts
 * "low" and "high". It returns the subscript if it is found, otherwise
 * it returns 0.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class TableSearch {
    
    /**
     * Only the first 'length' number of characters in the string are searched in
     * the provided data table.
     * @param data the input string
     * @param low the low index
     * @param high the high index
     * @param length the length of the string
     * @param table the table's name
     * @return the index of the matched string
     */
    public static int searchTable(String data, int low, int high, int length,
    StringBuffer[] table) {
        
        int mid = 0;
        
        low = low - 1;
        high = high - 1;
        
        // Search algorithm (NEED TO IDENTIFY)
        while (low <= high) {
            
            mid = (low + high) / 2;
            
            if (EmeUtil.strncmp(data, table[mid], length) == 0) {
                
                return mid;
                
            } else if (EmeUtil.strncmp(data, table[mid], length) < 0) {
                high = mid - 1;
                
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }

    /**
     * Only the first 'length' number of characters in the string are searched in
     * the provided data table.
     * @param data the input string
     * @param low the low index
     * @param high the high index
     * @param length the length of the string
     * @param table the table's name
     * @return the index of the matched string
     */
    public static int searchTable(String data, int low, int high, int length,
                                  String[] table) {
        
        int mid = 0;
        
        low = low - 1;
        high = high - 1;
        
        // Search algorithm (NEED TO IDENTIFY)
        while (low <= high) {
            
            mid = (low + high) / 2;
            
            if (EmeUtil.strncmp(data, table[mid], length) == 0) {
                
                return mid;
                
            } else if (EmeUtil.strncmp(data, table[mid], length) < 0) {
                high = mid - 1;
                
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }
    

    
    /**
     * The entire string is searched in the provided data table
     * @param data the input string
     * @param low the low index
     * @param high the high index
     * @param table the table's name
     * @return the index of the matched string
     */
    public static int searchTable(String data, int low, int high, StringBuffer[] table) {
        
        int mid = 0;
        low = low - 1;
        high = high - 1;
        
        while (low <= high) {
            
            mid = (low + high) / 2;
            
            if (EmeUtil.strcmp1(data, table[mid]) == 0) {
                return mid;
                
            } else if (EmeUtil.strcmp1(data, table[mid]) < 0) {
                high = mid - 1;
                
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }
    
    /**
     * The entire string is searched in the provided data table
     * @param data the input string
     * @param low the low index
     * @param high the high index
     * @param table the table's name
     * @return the index of the matched string
     */
    public static int searchTable(String data, int low, int high, String[] table) {
        
        int mid = 0;
        low = low - 1;
        high = high - 1;
        
        while (low <= high) {
            
            mid = (low + high) / 2;
            
            if (EmeUtil.strcmp(data, table[mid]) == 0) {
                return mid;
                
            } else if (EmeUtil.strcmp(data, table[mid]) < 0) {
                high = mid - 1;
                
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }   
}
