/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.io.InputStream;

import com.stc.sbme.match.util.ReadMatchConstantsValues;

/**
 * Defines all the 'global' variables used in the matching code.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class MatchVariables {
    
	/* */
	private static final int N_FIELDS = ReadMatchConstantsValues.getNFields();
	/* */
	private static final int MAX_NO_TAB = ReadMatchConstantsValues.getMaxNumberTables();
	/* */
	private static final int PATTERNS_N_PAT = ReadMatchConstantsValues.getNumberPatt();



    /**
     * Default constructor
     */
    private MatchVariables() {
        super();
    }
 
    /**
     * The input stream of the matching configuration file. 
     */
    InputStream matchConfigStream;
    
    // Get the input stream for the data file
    private static InputStream matchDataStream;
    
    /**
     *
     */
    static InputStream getMatchDataStream() {
        return matchDataStream;
    }


    /**
     *
     */
    public double[] cWeight;
    /**
     *
     */
    public double cmpVal;
    
    /**
     *
     */
    public double minD;
    /**
     *
     */
    public double maxD;
    
    /**
     * Agreement weight
     */
    public double agrWeight[] = new double[N_FIELDS];
    
    /**
     * Disagreement weight
     */
    public double disWeight[] = new double[N_FIELDS];

    /**
     *
     */
    public double[] Mproba = new double[N_FIELDS];
    /**
     *
     */
    public double[] Uproba = new double[N_FIELDS];
    /**
     *
     */
    public double[] fracVal = new double[N_FIELDS];


    /**
     *
     */
    public String[] nullFlag = new String[N_FIELDS];

    /**
     *
     */
    public int year;
    /**
     *
     */
    public int nTable;
    /**
     *
     */
    public int nCrosses;
    /**
     *
     */
    public int criticalTest;
    /**
     *
     */
    public int dw;
    /**
     *
     */
    public int dw1;
    /**
     *
     */
    public int dw2;
    /**
     *
     */
    public int dw3;
    /**
     *
     */
    public int m1;
    /**
     *
     */
    public int m2;
    /**
     *
     */
    public double fw1;
    /**
     *
     */
    public double fw2;
    /**
     *
     */
    public double fw3;
    /**
     *
     */
    public double fm1;
    /**
     *
     */
    public double fm2;

    /**
     *
     */
    public double proportion;
    /**
     *
     */
    public double degPrecision;
    /**
     *
     */
    public double nIteration;
    
    /**
     *
     */
    public int[] jIndex;
    
    /**
     *
     */
    public int eMSwitch = 0;
    
    
    /**
     *
     */
    public int[] ibaso = new int[N_FIELDS];
    
    /**
     *
     */
    public int[] dwi = new int[N_FIELDS];
    /**
     *
     */
    public int[] tableW = new int[MAX_NO_TAB];
    /**
     *
     */
    public int[] tableSize = new int[MAX_NO_TAB];
    /**
     *
     */
    public double[] adjCurv = new double[(N_FIELDS * 4) + 3];
    
    /**
     *
     */
    public int[] lengthF = new int[N_FIELDS];
    
    /**
     *
     */
    public int numberItems;
    /**
     *
     */
    public int[] fieldIndex = new int[N_FIELDS];
    /**
     *
     */
    public int[] fieldPatt = new int[N_FIELDS];
    /**
     *
     */
    public int[] freqPatt = new int[PATTERNS_N_PAT];
    
    /**
     *
     */
    public StringBuffer[] comparatorType = new StringBuffer[N_FIELDS];
    /**
     *
     */
    public boolean[] trans = new boolean[N_FIELDS];
    /**
     *
     */
    public boolean[] ssnConstraints = new boolean[N_FIELDS];

    /**
     *
     */
    public int[] param1 = new int[N_FIELDS];
    /**
     *
     */
    public int[] param2 = new int[N_FIELDS];
    /**
     *
     */
    public double[] param3 = new double[N_FIELDS];

    /**
     *
     */
    public double[] tolerance1 = new double[N_FIELDS];
    /**
     *
     */
    public double[] tolerance2 = new double[N_FIELDS];
    /**
     *
     */
    public double[] rangeP = new double[N_FIELDS];

    /**
     *
     */
    public StringBuffer[] ssnList = new StringBuffer[N_FIELDS];
 
    /**
     *
     */
    public int[] ssnLength = new int[N_FIELDS];

    /**
     *
     */
    public String[] recType = new String[N_FIELDS];
    /**
     *
     */
    public StringBuffer[] fieldName = new StringBuffer[N_FIELDS];
//    public HashMap fieldName = new HashMap();    
    /**
     *
     */
    public StringBuffer[] idName = new StringBuffer[N_FIELDS];
    /**
     *
     */
    public StringBuffer[] pntTable = new StringBuffer[MAX_NO_TAB];
    /**
     *
     */
    public double[][] wgtTable = new double[MAX_NO_TAB][];
    
    /**
     *
     */
    public int[][] adjwt = new int[255][255];
        
    /**
     *
     */
    public int pass = 0;
    
    /**
     * Language name used in the Locale class
     */ 
    public String language;
    
    /**
     * Language name used in the Locale class
     */ 
    public String[] lang = {"af", "am", "ar", "be", "bg", "bn", "ca", "cs", 
        "da", "de", "el", "en", "eo", "es", "et", "eu", "fa", "fi", "fo", 
        "fr", "ga", "gl", "gu", "gv", "he", "hi", "hr", "hu", "hy", "id", "is", 
        "it", "ja", "kl", "kn", "ko", "kw", "lt", "lv", "mk", "mr", "mt", "nb",
        "nl", "nn", "om", "pl", "ps", "pt", "ro", "ru", "sh", "sk", "sl", "so",
        "sq", "sr", "sv", "sw", "ta", "te", "th", "ti", "tr", "uk", "vi", "zh"
    };
    // The adjwt values are used to give partial credit for characters that
    // may be in errors due to known phonetic or character recognition errors.
    // A typical example is to match the letter "O" with the number "0"
    public static char[][] sp  = {
    {'A', 'E'}, {'A', 'I'}, {'A', 'O'}, {'A', 'U'},
    {'B', 'V'}, {'E', 'I'}, {'E', 'O'}, {'E', 'U'}, 
    {'I', 'O'}, {'I', 'U'}, {'O', 'U'}, {'I', 'Y'}, 
    {'E', 'Y'}, {'C', 'G'}, {'E', 'F'}, {'W', 'U'}, 
    {'W', 'V'}, {'X', 'K'}, {'S', 'Z'}, {'X', 'S'}, 
    {'Q', 'C'}, {'U', 'V'}, {'M', 'N'}, {'L', 'I'}, 
    {'Q', 'O'}, {'P', 'R'}, {'I', 'J'}, {'2', 'Z'}, 
    {'5', 'S'}, {'8', 'B'}, {'1', 'I'}, {'1', 'L'}, 
    {'0', 'O'}, {'0', 'Q'}, {'C', 'K'}, {'G', 'J'}, 
    {'E', ' '}, {'Y', ' '}, {'S', ' '}, 
    {'1', '2'}, {'2', '3'}, {'3', '4'}, {'4', '5'}, {'5', '6'},
    {'6', '7'}, {'7', '8'}, {'8', '9'}, {'9', '0'}
    };    

    // The adjwt values are used to give partial credit for characters that
    // may be in errors due to known phonetic or character recognition errors.
    // A typical example is to match the letter "O" with the number "0"
    public static int[][] spUnicode  = {
    {82, 87}, {82, 91}, {82, 97}, {82, 103},
    {83, 104}, {87, 91}, {87, 97}, {87, 103}, 
    {91, 97}, {91, 103}, {97, 103}, {91, 107}, 
    {87, 107}, {84, 89}, {87, 88}, {105, 103}, 
    {105, 104}, {106, 93}, {101, 108}, {106, 101}, 
    {99, 84}, {103, 104}, {95, 96}, {94, 91}, 
    {99, 97}, {98, 100}, {91, 92}, {71, 108}, 
    {74, 101}, {77, 83}, {70, 91}, {70, 94}, 
    {69, 97}, {69, 99}, {84, 93}, {89, 92}, 
    {87, 0}, {107, 0}, {101, 0}, 
    {70, 71}, {71, 72}, {72, 73}, {73, 74}, {74, 75},
    {75, 76}, {76, 77}, {77, 78}, {78, 69}
    };    

    {        
        
        int i;
        int j;
        int set1;
        int set2;
        int arrLen = sp.length;
        
        for (i = 0; i < N_FIELDS; i++) {
            comparatorType[i] = new StringBuffer(6);
            fieldName[i] = new StringBuffer();
            idName[i] = new StringBuffer();
            ssnList[i] = new StringBuffer();
            recType[i] = new String();
            ssnConstraints[i] = false;
            ssnLength[i] = 0;
            trans[i] = false;
        }
        
        for (i = 0; i < MAX_NO_TAB; i++) {
            pntTable[i] = new StringBuffer();
        }
        
        for (i = 0; i < 111; i++) {
            
            if (i < 42) {          
                set1 = 48 + i;
                
            } else {
                set1 = 144 + i; 
            }
                              
            for (j = 0; j < 111; j++) { 
                 
                if (j < 42) {          
                    set2 = 48 + j;
                
                } else {
                    set2 = 144 + j; 
                }                                                   
                adjwt[set1][set2] = 0;
            }
        }
        
        //
        for (i = 0; i < arrLen; i++) {
         
            adjwt[sp[i][0]][sp[i][1]] = 3;
            adjwt[sp[i][1]][sp[i][0]] = 3;
        }       
    }
    
    /**
     * Define a factory method that return an instance of the class
     * @return a MatchVariables object
     */
    public static MatchVariables getInstance() {
        return new MatchVariables();
    }
}
