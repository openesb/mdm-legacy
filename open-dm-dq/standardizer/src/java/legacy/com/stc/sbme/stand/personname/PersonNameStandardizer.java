/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.personname;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.stc.sbme.api.SbmePersonName;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandRecordFactory;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * Main class that processes any request for normalizing or standardizing
 * a person name object or string
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.2 $
 */
public class PersonNameStandardizer {
 
    /* An instance of the PersonNameNormalizer class that normalize the fields */
    private PersonNameNormalizer pNameNorm;
    
    /*
     * An instance of PersonNameParser class that parse the string into
     * separate components
     */
    private PersonNameParser pNameParse;
    
    /*
     * An instance of PersonNamePatternsFinder class that search for the
     * appropriate pattern
     */
    private PersonNamePatternsFinder pNamePatt;
    
    private final String domain;
    
    private final PersonNameStandVariables sVar;
    
    /**
     * Default constructor
     */
    public PersonNameStandardizer(PersonNameStandVariables vars, String domain) {
        
        pNameNorm = PersonNameNormalizer.getInstance();
        pNameParse = PersonNameParser.getInstance();
        pNamePatt = PersonNamePatternsFinder.getInstance();
        this.domain = domain;
        sVar = vars;
    }
   
    
    /**
     * This method reads in a person name object, search for the standard
     * synonyms of its associated fields in some look up
     * tables, then returns a normalized person name object, without changing
     * the number of fields
     *
     * @param standObj a SbmeStandRecord object
     * @param sVar an instance of the BusinessStandVariables class
     * @return a SbmeStandRecord normalized objects
     * @throws SbmeStandardizationException the generic stand exception
     */
    public SbmeStandRecord normalize(SbmeStandRecord standObj)
    throws SbmeStandardizationException {
        
        int i;
        String[] fieldList;
        
        /* Read the fields of the standObj into an array of strings */
        Object[] objectList =  standObj.getAllFields().toArray();
        
        /* The number of elements in the object */
        int numberOfFields = objectList.length;
        
        /* Initiate the array of fields */
        fieldList = new String[numberOfFields];
        
        for (i = 0; i < numberOfFields; i++) {
            fieldList[i] = objectList[i].toString();
        }
        
        
       /*
        * Call the method that normalizes the fields. We need to update
        * the index of the key and value strings if we remove any field
        */
        pNameNorm.performFieldsNormalization(fieldList, standObj, sVar, domain);
        
        return standObj;
    }
        
    /**
     * This method reads in a array of person name objects, search for each
     * of them, the standard synonyms of its associated fields in the look up
     * tables, then returns a normalized person name array of objects,
     * without changing the number of fields
     *
     * @param personList a list of objects to normalize
     * @param sVar an instance of the BusinessStandVariables class
     * @throws SbmeStandardizationException the generic stand exception
     */
    public void normalize(ArrayList personList)
    throws SbmeStandardizationException {
        
        int i;
        int j;
        
        /* The length of the array of standObjs */
        int numberOfObjects = personList.size();
        
        /* The number of elements in the object */
        int numberOfFields;
        
        /* Define an array of SbmeStandRecord */
        SbmeStandRecord[] standObjs = new SbmeStandRecord[numberOfObjects];
        
       /*
        * Define a 2D array of the string fields in the
        * SbmeStandRecord objects
        */
        String[][] fieldList = new String[numberOfObjects][];
        
        /* Read the fields of the standObj into an array of strings */
        Object[][] objectList = new Object[numberOfObjects][];
        
        /*
         * Read in the fields from each object of the array
         * standObjs in an array of strings
         */
        for (i = 0; i < numberOfObjects; i++) {
            
            /* Read the  ith object from the ArrayList */
            standObjs[i] = (SbmeStandRecord) personList.get(i);
            
            personList.remove(i);
            
            /* Read the fields of the standObj into an array of strings */
            objectList[i] =  standObjs[i].getAllFields().toArray();
            
            
            /* The number of elements in the object */
            numberOfFields = objectList[i].length;
            
            /* Initiate the array of fields */
            fieldList[i] = new String[numberOfFields];
            
            for (j = 0; j < numberOfFields; j++) {
                fieldList[i][j] = objectList[i][j].toString();
            }

           /*
            * Call the method that normalizes the fields. We need to update
            * the index of the key and value strings if we remove any field
            */
            pNameNorm.performFieldsNormalization(fieldList[i], standObjs[i], sVar, domain);
            
            personList.add(i, standObjs[i]);
        }
    }
    
    
    /**
     *
     * This method reads in a person name string, parse it, search for
     * the types and standard formats of all the tokens by using  in the
     * look up tables, finally cearch for the best pattern for the record,
     * and returns standardized person name record object.
     *
     * @param record the record to standardize
     * @param sVar an instance of the BusinessStandVariables class
     * @return a standardized array of SbmeStandRecord object(s)
     * @throws SbmeStandardizationException the generic stand exception
     */
    public SbmeStandRecord[] standardize(String record)
    throws SbmeStandardizationException {
        
        int i;
        int j;
        Pattern pa = Pattern.compile("\\S"); 
        Matcher mat;

        /* Regular expression that catches empty strings */
        mat = pa.matcher(record);
 
       /* Define instances of the SbmeStandRecord class */
        SbmeStandRecord[] standObj = null;

        /* test if the record is empty or null */
        if ((record == null) || (record.length() == 0) || !(mat.lookingAt())) {
        
            /* Initialize the array */
            standObj = new SbmeStandRecord[1];
            
            /*
             * Create an instance of the PersonNameParser through
             * the call of the StandardizationEngineWrapper instance
             */
            standObj[0] = SbmeStandRecordFactory.getInstance("PersonName");

            // return the null object
            return standObj;
        }
        
        
        /* Number of person in the string */
        int numberOfPerson;
        
        /* Number of fields per person */
        int numberOfKeys;
        
        /* Number of fields associated with persons per record */
        int numberOfFieldKeys[] = new int[3];
        
        /* Holds the tokens obtained from parsing the string */
        ArrayList fieldList = new ArrayList();
        
        /*
         * Defines an array of strings to hold the fields' types
         * in a given order
         */
        int WORDS = ReadStandConstantsValues.getInstance(domain).getWords();
        StringBuffer[][] orderedKeys = new StringBuffer[3][WORDS];;
        
        /* define an array of strings to hold the fields' values */
        StringBuffer[][] orderedValues = new StringBuffer[3][WORDS];;
        
        
        /*
         * Start first by parsing the string into tokens. We input a string,
         * and return an array of strings 'fieldList' which hold the fields.
         */
        fieldList.addAll(Arrays.asList(pNameParse.performParsing(record,
        sVar).toArray()));
        
        /* Temporary number of fields */
        numberOfKeys = fieldList.size();
        
        /* Initialize all the types of the fields */
        for (i = 0; i < numberOfKeys; i++) {
            
            sVar.wordType[i].setLength(1);
            sVar.wordType[i].setCharAt(0, '0');
            
            for (j = 0; j < 2; j++) {
                sVar.sWordType[j][i].setLength(1);
                sVar.sWordType[j][i].setCharAt(0, '0');
            }
        }
        
        
        /*
         * We then normalize the different fields. We send in the parsed
         * fields and return back the normalized ones. We keep the same
         * number of fields, but we replace them with their normalized forms
         * if found, or as an empty string if the field is to be removed. Also
         * we define the fieldType inside this method.
         */
        fieldList = pNameNorm.performStringNormalization(fieldList, sVar, domain);
        
        /*
         * Finally, identify the best pattern (the order in the arangement of the
         * fields) for the record, and also the fields to be returned.
         */
        pNamePatt.findPatterns(orderedKeys, orderedValues, fieldList, sVar);
        
        
        /* Calculate the number of person in the string */
        numberOfPerson = orderedKeys.length;
        
        /* Recalculate the correct number of person in the string */
        for (i = 0; i < numberOfPerson; i++) {
            
            if (orderedKeys[i][0] == null) {
                
                /* Deternime the number of person/record */
                numberOfPerson = i;
                
                /* Exit the loop */
                break;
            }
            
            
            for (j = 0; j <= numberOfKeys; j++) {
                
                if (orderedKeys[i][j] == null) {
                    
                    /* Number of fields per person per record */
                    numberOfFieldKeys[i] = j;
                }
            }
        }
        
        /* Define the number of SbmeStandRecord elements in the array */
        standObj = new SbmeStandRecord[numberOfPerson];
        
        /*
         * Instantiate the SbmeStandRecord objects and store the corresponding
         * fields' values into these objects
         */
        for (i = 0; i < numberOfPerson; i++) {
            
           /*
            * Create an instance of the PersonNameParser through
            * the call of the StandardizationEngineWrapper instance
            */
            standObj[i] = SbmeStandRecordFactory.getInstance("PersonName");

            /* Handle the gender field */
            standObj[i].setValue("Gender", orderedValues[i][10].toString());
            
            String keySignature = "";
            String valueSignature = "";
            for (j = 0; j < numberOfFieldKeys[i]; j++) {
                
                standObj[i].setValue(orderedKeys[i][j].toString(),
                                     orderedValues[i][j].toString());
                keySignature += orderedKeys[i][j].toString() + " ";
                valueSignature += orderedValues[i][j].toString() + " ";

                if ((sVar.sWordType[i][j].toString()).compareTo("T") == 0) {

                    standObj[i].setStatusValue(orderedKeys[i][j].toString(), "true");
                    
                } else {
                    standObj[i].setStatusValue(orderedKeys[i][j].toString(), "false"); 
                }
            }
            
            ((SbmePersonName) standObj[i]).setSignature(keySignature.trim() + " | " + valueSignature.trim());
        }
        return standObj;
    }
    
    /**
     *
     * This method reads in an array of person names' strings, parse them,
     * search for the types and standard formats of all the tokens within each
     * string using the look up tables, and finally, it searches for the best
     * pattern for each record. Returns an 2D array of standardized person names
     * objects. One dimension for one record with multiple people.
     *
     * @param personList the list of records to standardize
     * @param sVar an instance of the BusinessStandVariables class
     * @return a standardized 2D array of SbmeStandRecord objects
     * @throws SbmeStandardizationException the generic stand exception
     */
    public SbmeStandRecord[][] standardize(ArrayList personList)
    throws SbmeStandardizationException {
        
        int i;
        int j;
        int k;
        int numberPer;
        int numberFld;
        Pattern pa = Pattern.compile("\\S");
        Matcher mat;

        
        /* Number of record submitted for standardization */
        int numberOfRec = personList.size();

        /* Set the status of the records to true */
        boolean[] recordStatus = new boolean[numberOfRec];
        
        /* Number of person per string */
        int numberOfPerson[] = new int[numberOfRec];
        
        /* Number of fields per person */
        int[] numberOfKeys = new int[numberOfRec];
        
        /* Number of person per record */
        int numberOfFieldKeys[][] = new int[numberOfRec][3];
        
        /* Holds the tokens obtained from parsing the string */
        int WORDS = ReadStandConstantsValues.getInstance(domain).getWords();
        ArrayList[] fieldList = new ArrayList[WORDS];
        
        /* Temporarily hold the array of input records */
        String[] record = new String[numberOfRec];
        
        /* define an array of strings to hold the fields' types */
        StringBuffer[][][] orderedKeys = new StringBuffer[numberOfRec][3][WORDS];
        
        /* define an array of strings to hold the fields' types */
        StringBuffer[][][] orderedValues = new StringBuffer[numberOfRec][3][WORDS];
        
        
       /* Define instances of the SbmeStandRecord class */
        SbmeStandRecord[][] standObj = new SbmeStandRecord[numberOfRec][3];
        
        
        /* Initiate the 3 dimensional array */
        sVar.s2WordType = new StringBuffer[numberOfRec][3][WORDS];
        
        /* Initiate the fieldList variable */
        for (i = 0; i < numberOfRec; i++) {
            for (k = 0; k < 3; k++) {
                for (j = 0; j < WORDS; j++) {
                    sVar.s2WordType[i][k][j] = new StringBuffer("F");
                }
            }
        }
        
        for (i = 0; i < WORDS; i++) {
            fieldList[i] = new ArrayList();
        }
        
        
        for (i = 0; i < numberOfRec; i++) {
            
            /* Read the objects from the ArrayList */
            record[i] = (String) personList.get(i);
            
            /* Regular expression that catches empty strings */
            mat = pa.matcher(record[i]);
            
            /* We assume initially the status record to be true */
            recordStatus[i] = true;

            /* test if the record is empty or null */
            if ((record[i] == null) || (record[i].length() == 0) || !(mat.lookingAt())) {
                
                // Flag a record that is null, empty or has only white characters 
                recordStatus[i] = false;
                
                // Jump to the next index 
                continue;
            }

            
            /* Start first by parsing the strings into tokens */
            fieldList[i].addAll(Arrays.asList(pNameParse.performParsing(record[i], sVar).toArray()));
            
            /* Temporary number of fields */
            numberOfKeys[i] = fieldList[i].size();
            
            
            /* Initialize all the types of the fields */
            for (j = 0; j < numberOfKeys[i]; j++) {
                
                sVar.wordType[j].setLength(1);
                sVar.wordType[j].setCharAt(0, '0');
                
                sVar.wordTypeFound[j].setLength(1);
                sVar.wordTypeFound[j].setCharAt(0, 'F');
                
                for (k = 0; k < 3; k++) {
                    sVar.sWordType[k][j].setLength(1);
                    sVar.sWordType[k][j].setCharAt(0, 'F');
                    
                    sVar.s2WordType[i][k][j].setLength(1);
                    sVar.s2WordType[i][k][j].setCharAt(0, 'F');
                }
            }
            
           /*
            * We send in the parsed fields, and return back the normalized ones.
            * We keep the same number of fields, but we replace them with their
            * normalized form if found or as an empty string if the field is to
            * be removed. Also define the fieldType inside this method.
            */
            fieldList[i] = pNameNorm.performStringNormalization(fieldList[i], sVar, domain);
            
            /*
             * Finally, identify the best pattern (the order in the arangement of the
             * fields) for the record and also the fields to be returned.
             */
            pNamePatt.findPatterns(orderedKeys[i], orderedValues[i], fieldList[i], sVar);
            
            /* store the fields statuses inside this variable per record */
            sVar.s2WordType[i] = sVar.sWordType;
            
            numberPer = sVar.sWordType.length;
            
            for (j = 0; j < numberPer; j++) {
                
                numberFld = sVar.sWordType[j].length;
                
                for (k = 0; k < numberFld; k++) {
                    sVar.s2WordType[i][j][k].append(sVar.sWordType[j][k]);
                }
            }
        }
        
        /*
         * Set the SbmeStandRecord objects and store the corresponding
         * fields' values into these objects
         */
        for (k = 0; k < numberOfRec; k++) {

            /* 
             * If the record is intially null, empty or filled with
             * white chracters, skip this part.
             */
            if (!recordStatus[k]) {
            
               /*
                * Create an instance of the PersonNameParser through
                * the call of the StandardizationEngineWrapper instance
                */
                standObj[k][0] = SbmeStandRecordFactory.getInstance("PersonName");
                
                /* Go to the next iteration */
                continue;
            }

            
            /* Calculate the number of person */
            numberOfPerson[k] = orderedKeys[k].length;
            
            /* Recalculate the correct number of person in the string */
            for (i = 0; i < numberOfPerson[k]; i++) {
                
                if (orderedKeys[k][i][0] == null) {
                    
                    /* Deternime the number of person/record */
                    numberOfPerson[k] = i;
                    /* Exit the loop */
                    break;
                }
                
                for (j = 0; j <= numberOfKeys[k]; j++) {
                    
                    if (orderedKeys[k][i][j] == null) {
                        
                        /* Number of fields per person per record */
                        numberOfFieldKeys[k][i] = j;
                    }
                }
            }
            
            
            /* Define the number of elements in the array */
            standObj[k] = new SbmeStandRecord[numberOfPerson[k]];
            

            /*
             * Set the SbmePersonName object and store the corresponding
             * fields' values
             */
            for (i = 0; i < numberOfPerson[k]; i++) {
                
               /*
                * Create an instance of the PersonNameParser through
                * the call of the StandardizationEngineWrapper instance
                */
                standObj[k][i] = SbmeStandRecordFactory.getInstance("PersonName");
                
                /* Handle the gender field */
                standObj[k][i].setValue("Gender", orderedValues[k][i][10].toString());
 
                for (j = 0; j < numberOfFieldKeys[k][i]; j++) {
                    
                    standObj[k][i].setValue(orderedKeys[k][i][j].toString(),
                    orderedValues[k][i][j].toString());
                    
                    
                    if (sVar.s2WordType[k][i][j].charAt(0) == 'T') {
                        
                        standObj[k][i].setStatusValue(orderedKeys[k][i][j].toString(), "true");
                        
                    } else {
                        standObj[k][i].setStatusValue(orderedKeys[k][i][j].toString(), "false");
                    }
                }

            }
        }
        return standObj;
    }
}
