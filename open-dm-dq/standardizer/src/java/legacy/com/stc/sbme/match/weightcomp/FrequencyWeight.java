/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.EmeUtil;

/**
 * This routine searches for two strings, "fieldA" and "fieldB", in the frequency
 * tables and returns the associated weight
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class FrequencyWeight {
    
    // Max number of frequency tables
    private static final int MAX_NO_TAB = ReadMatchConstantsValues.getMaxNumberTables();
    
    
    /**
     *
     *
     * @param fieldA the candidate field
     * @param fieldB the reference field
     * @param length the length of the field
     * @param tableNumber the size of the table
     * @param mVar the MatchVariables instance
     * @return the frequency of occurence
     */
    public static double getFrequency(String fieldA, String fieldB, int length, int tableNumber,
    MatchVariables mVar) {
        
        int fieldAIndex;
        int fieldBIndex;
        int low = 1;
        int high = mVar.tableSize[tableNumber];
        
        // Search for the specific matched fields in the frequency table
        fieldAIndex = searchTable(fieldA, low, high, length, tableNumber, mVar);
        fieldBIndex = searchTable(fieldB, low, high, length, tableNumber, mVar);
        
        // If the first field, from sorta.dat, is found in the frequency table
        if (fieldAIndex != 0) {
            
            // If the second field, from sortb.dat, is found in the frequency table
            if (fieldBIndex != 0) {
                
                // Return the lowest weight from the fields min(w1, w2).  This
                // means the one with the most distinguishing power
                if (mVar.wgtTable[tableNumber][fieldBIndex] 
                    > mVar.wgtTable[tableNumber][fieldAIndex]) {
                    
                    return mVar.wgtTable[tableNumber][fieldAIndex];
                    
                } else {
                    return mVar.wgtTable[tableNumber][fieldBIndex];
                }
                
            } else {
                // If only the first field exists, return the weight of the first field
                
                return mVar.wgtTable[tableNumber][fieldAIndex];
            }
        }
        // Not very logic! Need to be reviewed in the case when both are not found
        return mVar.wgtTable[tableNumber][fieldBIndex];
    }
    
    
    /**
     * Searches for 'data' in the frequency table indexed: 'tableNumber'
     *
     */
    /**
     *
     *
     * @param data
     *
     * @param low
     * @param high
     * @param length
     * @param tableNumber
     * @return a integer
     */
    private static int searchTable(String data, int low, int high, int length, int tableNumber,
    MatchVariables mVar) {
        
        int mid = 0;
        int tw = mVar.tableW[tableNumber];
        
        while (low <= high) {
            
            mid = (low + high) / 2;
            
            if (EmeUtil.strncmp(data, mVar.pntTable[tableNumber], mid * tw, length) == 0) { 
                return mid;
                
            } else if (EmeUtil.strncmp(data, mVar.pntTable[tableNumber], mid * tw, length) < 0) {
                high = mid - 1;
                
            } else {
                low = mid + 1;
            }
        }
        return 0;
    }
}
