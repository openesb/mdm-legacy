/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.util.EmeUtil;

/**
 * Main class that processes any request for normalizing or standardizing 
 * a generic address presented in the form of an object or a string
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class TokenFound {

    /**
     *
     * @param token aaa 
     * @param tokenStringArray aaa 
     * @return a boolean
     */
    static boolean tokenFound(StringBuffer token, String[] tokenStringArray) {
    
        int i = 0;
        boolean match = false;
        
        while ((EmeUtil.strcmp(tokenStringArray[i], "") != 0) && (!match)) {
        
            if (EmeUtil.strcmp(token, tokenStringArray[i]) == 0) {
                match = true;
                
                // We don't need to loop over
                return match;
            }
            
            if (i == (tokenStringArray.length - 1)) {
                break;
            }
            
            i++;
        }
        return match;
    }  
}
