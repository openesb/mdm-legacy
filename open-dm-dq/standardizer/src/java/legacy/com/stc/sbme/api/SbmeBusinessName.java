/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * The SbmePersonName represents and manipulates any person name record that
 * has been broken down into its basic components
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeBusinessName
implements SbmeStandRecord {
    // Added for capturing pattern signature
    private String signature;
    public String getSignature() { return signature; }
    public void setSignature(String signature) { this.signature = signature; }
    
    
    /** Defines the primary name associated with a business entity */
    public static final String PRIMARY_NAME = "PrimaryName";
    
    /** Defines the organization type of the business entity */
    public static final String ORG_TYPE_KEY = "OrgTypeKeyword";
    
    /** Defines the association type of the business entity */
    public static final String ASSOC_TYPE_KEY = "AssocTypeKeyword";
 
    /** Defines the association type of the business entity */
    public static final String INDUSTRY_TYPE_KEY = "IndustryTypeKeyword";
    
    /** Defines the association type of the business entity */
    public static final String ALIAS_LIST = "AliasList";
    
    /** Defines the association type of the business entity */
    public static final String URL_NAME = "Url";

    /** Defines the association type of the business entity */
    public static final String INDUSTRY_SECTOR_LIST = "IndustrySectorList";

    /** Defines the association type of the business entity */
    public static final String NOT_FOUND_FIELDS = "NotFoundFields";


    /**
     * Used in the standardizationEngine class to identify the type of records
     */
    public static final String BUSINESS_NAME = "BusinessName";


    /* Holds keys-values of the different fields */
    private HashMap businessFieldName;
    
    /* Holds keys-values of the status of the different fields */
    private HashMap businessFieldStatus;
    
    /* Holds keys-values of the precision of the different records */
    private HashMap businessRecordAccuracy;
    
    /* */
    private ArrayList ar = new ArrayList();
    /**
     * Public constructor
     */
    public SbmeBusinessName() {
        businessFieldName = new HashMap();
        businessFieldStatus = new HashMap();
        businessRecordAccuracy = new HashMap();
    }
    
    /**
     * This method identify the type of this class's objects (BusinessName)
     * @return a string
     */
    public String getType() {
        return BUSINESS_NAME;
    }
    
    /**
     * This getter method provides the value associated with primary name
     * @return a company primary name
     */
    public String getPrimaryName() {
        return (String) businessFieldName.get(PRIMARY_NAME);
    }
    
    /**
     * This method sets the value associated with primary name
     * @param value the company primary name
     */
    public void setPrimaryName(String value) {
        businessFieldName.put(PRIMARY_NAME, value);
    }
    
    
    /**
     * This getter method provides the value associated with organization type key
     * @return an organization type key
     */
    public String getOrgTypeKeyword() {
        return (String) businessFieldName.get(ORG_TYPE_KEY);
    }
    
    /**
     * This method sets the value associated with organization type key
     * @param value of the organization type key
     */
    public void setOrgTypeKeyword(String value) {
        businessFieldName.put(ORG_TYPE_KEY, value);
    }
  
    /**
     * This getter method provides the value associated with association type key
     * @return an association type key
     */
    public String getAssocTypeKeyword() {
        return (String) businessFieldName.get(ASSOC_TYPE_KEY);
    }
    
    /**
     * This method sets the value associated with association type key
     * @param value of the company association type key
     */
    public void setAssocTypeKeyword(String value) {
        businessFieldName.put(ASSOC_TYPE_KEY, value);
    }
  
    /**
     * This getter method provides the value associated with the alias list
     * @return a list of alternative names
     */
    public List getAliasList() {
        return (ArrayList) businessFieldName.get(ALIAS_LIST);
    }
    
    /**
     * This method sets the value associated with the alias list
     * @param values list of the company's alternative names
     */
    public void setAliasList(List values) {
        businessFieldName.put(ALIAS_LIST, values);
    }  
   
    /**
     * This getter method provides the value associated with the industry type key
     * @return a company industry type key
     */
    public String getIndustryTypeKeyword() {
        return (String) businessFieldName.get(INDUSTRY_TYPE_KEY);
    }
    
    /**
     * This method sets the value associated with the industry type key
     * @param value of the company industry type key
     */
    public void setIndustryTypeKeyword(String value) {
        businessFieldName.put(INDUSTRY_TYPE_KEY, value);
    }
    
    /**
     * This getter method provides the value associated with the url name
     * @return a company name as url
     */
    public String getUrl() {
        return (String) businessFieldName.get(URL_NAME);
    }

    /**
     * This method sets the value associated with the url name
     * @param value the company url name
     */
    public void setUrl(String value) {
        businessFieldName.put(URL_NAME, value);
    }

    /**
     * This getter method provides the value associated with the industry sector(s)
     * @return a list of industry sector(s)
     */
    public List getIndustrySectorList() {
        return (ArrayList) businessFieldName.get(INDUSTRY_SECTOR_LIST);
    }
 
    
    /**
     * This method sets the value associated with the industry sector(s)
     * @param values list of the company's alternative names
     */
    public void setIndustrySectorList(List values) {
        businessFieldName.put(INDUSTRY_SECTOR_LIST, values);
    }

    /**
     * Getters methods that provide the values associated with each field
     * @return a company type
     */
    public String getNotFoundFields() {
        return (String) businessFieldName.get(NOT_FOUND_FIELDS);
    }
    
    /**
     * Setters methods that update the values associated with each field
     * @param value the company name
     */
    public void setNotFoundFields(String value) {
        businessFieldName.put(NOT_FOUND_FIELDS, value);
    }


    /**
     * Generic getter method for fields' names.
     * @param key the field
     * @return the associated value
     */
    public String getValue(String key) {
        if (businessFieldName.get(key) == null) {
            return "";
        }       
        if (businessFieldName.get(key) instanceof ArrayList) {
            return (String) getValues(key).get(0);
        }
        return (String) businessFieldName.get(key);
    }


    /**
     * Generic setter method for fields' names.
     * @param key the field
     * @param value the field's value
     */
    public void setValue(String key, String value) {
        businessFieldName.put(key, value);
    }
 

    /**
     * Generic getter method for fields' names.
     * @param key the field's type
     * @return a string array
     */
    public List getValues(String key) {
        if (businessFieldName.get(key) instanceof String) {
            ar.clear();
            ar.add(businessFieldName.get(key));
            return ar;
        }
        return (ArrayList) businessFieldName.get(key);  
    }
    
    /**
     * Generic setter method for fields' names.
     * @param key the generic key
     * @param values the corresponding array of values
     */
    public void setValues(String key, List values) {
        businessFieldName.put(key, values);
    }

    
    /**
     * Generic getter method for fields' status.
     *
     * @param key the status' field
     * @return a status value
     */
    public String getStatusValue(String key) {
        return (String) businessFieldStatus.get(key);
    }
    
    /**
     * Generic setter method for fields' status.
     *
     * @param key the status' field
     * @param value the status value
     */
    public void setStatusValue(String key, String value) {
        businessFieldStatus.put(key, value);
    }
    
    /**
     * This method returns all the fields in a particular PersonName record
     * as a Setinterface (use HashMap to implement it).
     *
     * @return a Set grouping all the fields
     */
    public Set getAllFields() {
        return businessFieldName.keySet();
    }
    
    /**
     * This method returns all the statuses values associated with the fields
     * in a given PersonName record as a Collection.
     *
     * @return a Collection of status values
     */
    public Collection getAllStatusValues() {
        return businessFieldStatus.values();
    }
    
    /**
     * This method returns all the names associated with the fields in a
     * given PersonName record as a Collection.
     *
     * @return a Collection of fields' values
     */
    public Collection getAllNameValues() {
        return businessFieldName.values();
    }
    
    /**
     * Get the value of the degree of precision of the standardization
     * @param object the SbmeStandRecord object
     * @return an integer representing the degree of precision of the standardization
     */
    public int getStandDegreeOfAccuracy(SbmeStandRecord object) {
        return ((Integer) businessRecordAccuracy.get(object)).intValue();
    }
    
    /**
     * Set the value of the degree of precision of the standardization
     * @param object the SbmeStandRecord object
     * @param value the degree of precision of the stand
     */
    public void setStandDegreeOfAccuracy(SbmeStandRecord object, Integer value) {
        businessRecordAccuracy.put(object, value);
    }
}
