/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;


/**
 * 
 * 
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PatternTable {

    private final int PATSIZE;

    /* input token string */
    private StringBuffer inputTokenString;
    /* output token string */
    private StringBuffer outputTokenString;
    /* pattern type */
    private StringBuffer patternType;
    /* priority */
    private StringBuffer priority;
    /* state code */
    private StringBuffer state;
    /* county code */
    private StringBuffer county;
    /* place code */
    private StringBuffer place;
    /* zip code */
    private StringBuffer zipCode;
    /* language */
    private char language;
    /* usage flag */
    private char usf;
    /* exclude flag */
    private char exclude;
    /* pointer to the next element in the linked list */
    private PatternTable next;

    /**
     * Default constructor
     */
    public PatternTable(String domain) {

       PATSIZE = ReadStandConstantsValues.getInstance(domain).getPatSize();
       inputTokenString = new StringBuffer(PATSIZE);
       outputTokenString = new StringBuffer(PATSIZE);
       patternType = new StringBuffer(3);
       priority = new StringBuffer(3);
       state = new StringBuffer(3);
       county = new StringBuffer(4);
       place = new StringBuffer(5);
       zipCode = new StringBuffer(6);
    }

    /** 
     * 
     * @return the input string
     */
    public StringBuffer getInputTokenString() {
        return inputTokenString;
    }

    /** 
     * 
     * @return the output string
     */
    public StringBuffer getOutputTokenString() {
        return outputTokenString;
    }

    /** 
     * 
     * @return the type of pattern
     */
    public StringBuffer getPatternType() {
        return patternType;
    }

    /** 
     * 
     * @return the priority
     */
    public StringBuffer getPriority() {
        return priority;
    }

    /** 
     * 
     * @return the state code
     */
    public StringBuffer getState() {
        return state;
    }

    /** 
     * 
     * @return the county code
     */
    public StringBuffer getCounty() {
        return county;
    }

    /** 
     * 
     * @return the place 
     */
    public StringBuffer getPlace() {
        return place;
    }

    /** 
     * 
     * @return the zip code
     */
    public StringBuffer getZipCode() {
        return zipCode;
    }

    /** 
     * 
     * @return the language
     */
    public char getLanguage() {
        return language;
    }

    /** 
     * 
     * @return the usf code
     */
    public char getUsf() {
        return usf;
    }

    /** 
     * 
     * @return the
     */
    public char getExclude() {
        return exclude;
    }

    /** 
     * 
     * @return the
     */
    public PatternTable getNext() {
        return next;
    }


    /** 
     * 
     * @param str the input token string
     */
    public void setInputTokenString(String  str) {
       this.inputTokenString = EmeUtil.replace(this.inputTokenString, str);
    }

    /** 
     * 
     * @param str the replaced string
     */
    public void setOutputTokenString(String str) {
        this.outputTokenString = EmeUtil.replace(this.outputTokenString, str);
    }

    /** 
     * 
     * @param str the output token string
     */
    public void setPatternType(String  str) {
        this.patternType = EmeUtil.replace(this.patternType, str);
    }

    /** 
     * 
     * @param str aaa
     */
    public void setPriority(String  str) {
        this.priority = EmeUtil.replace(this.priority, str);
    }


    /** 
     * 
     * @param str aaa
     */
    public void setState(String  str) {
        this.state = EmeUtil.replace(this.state, str);
    }


    /** 
     * 
     * @param str aaa
     */
    public void setCounty(String  str) {
        this.county = EmeUtil.replace(this.county, str);
    }

    /** 
     * 
     * @param str aaa
     */
    public void setPlace(String  str) {
        this.place = EmeUtil.replace(this.place, str);
    }

    /** 
     * 
     * @param str aaa
     */
    public void setZipCode(String  str) {
        this.zipCode = EmeUtil.replace(this.zipCode, str);
    }

    /** 
     * 
     * @param language aaa
     */
    public void setLanguage(char  language) {
        this.language = language;
    }

    /** 
     * 
     * @param usf aaa
     */
    public void setUsf(char  usf) {
        this.usf = usf;
    }

    /** 
     * 
     * @param exclude aaa
     */
    public void setExclude(char  exclude) {
        this.exclude = exclude;
    }

    /** 
     * 
     * @param next aaa
     */
    public void setNext(PatternTable  next) {
        this.next = next;
    }


    /** 
     * 
     * @param sb the copy string
     * @param str the initial string
     */
    public static void replace(StringBuffer sb, String  str) {

        sb.setLength(0);
        sb.insert(0, str);
    }
}
