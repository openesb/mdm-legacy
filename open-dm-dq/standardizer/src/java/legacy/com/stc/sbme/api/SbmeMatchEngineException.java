/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

/**
 * The base class that handles exceptions related to the match engine
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeMatchEngineException
    extends java.lang.Exception {
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with
     * no detail message.
     * @param t a throwable.
     */
    public SbmeMatchEngineException(Throwable t) {
        super(t);
    }
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with
     * a description message of the exception.
     *
     * @param  msg exception message
     * @param  t a throwable
     */
    public SbmeMatchEngineException(String msg, Throwable t) {
        super(msg, t);
    }
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with
     * a description message of the exception.
     *
     * @param  msg exception message
     */
    public SbmeMatchEngineException(String msg) {
        super(msg);
    }

}
