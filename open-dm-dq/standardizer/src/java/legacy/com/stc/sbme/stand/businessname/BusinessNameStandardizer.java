/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.stc.sbme.api.SbmeBusinessName;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandRecordFactory;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * Main class that processes any request for normalizing or standardizing
 * a generic business name presented in the form of an object or a string
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class BusinessNameStandardizer {
    
    /* Max number of fields */
    private final int WORDS;
    
    /* An instance of the class that performs the  normalization process */
    private BusinessNameNormalizer bizNorm;
    
    /* An instance of the class that performs the  parsing process */
    private BusinessNameParser bizParse;
    
    /* An instance of the class that performs the  pattern(s) identifications */
    private BusinessNamePatternsFinder bizPatt;
    
    private BusinessStandVariables sVar;
    
    /**
     * Default constructor that instantiate any instance of the class. We
     * instantiate any objects of classes that are called from this class
     * inside the constructor to create a tree-like structure dependency.
     */
    public BusinessNameStandardizer(String domain, BusinessStandVariables sVar) {
        
        bizNorm = new BusinessNameNormalizer();
        bizParse = new BusinessNameParser();
        bizPatt = new BusinessNamePatternsFinder();
        WORDS = ReadStandConstantsValues.getInstance(domain).getWords();
        this.sVar = sVar;
    }
    
    /**
     * This method reads in a business name object, search for the standard
     * synonyms of its associated fields using some domain-independent lookup
     * tables, then returns a normalized business name object, without changing
     * the number of fields. (Return an SbmeBusinessName object)
     *
     * @param standObj a SbmeStandRecord object
     * @return a SbmeStandRecord normalized objects
     * @throws SbmeStandardizationException the generic stand exception
     */
    public SbmeStandRecord normalize(SbmeStandRecord standObj)
        throws SbmeStandardizationException {
        
        /* Put here your implementation of this method */
        return null;
    }
    
    /**
     * This method reads in a array of business name objects (addressList),
     * search for the standard synonyms of its associated fields using some
     * domain-independent lookup tables, then returns a normalized business name
     * object tables, then returns a normalized business name array of objects,
     * without changing the number of fields (update the ArrayList)
     *
     * @param addressList the list of objects to normalize
     * @throws SbmeStandardizationException the generic stand exception
     */
    public void normalize(ArrayList addressList) 
        throws SbmeStandardizationException {
                /* Implementation */
    }
    
    /**
     * This method reads in a business name string, parse it, search for
     * the types and the standard formats of all the tokens using the
     * lookup tables. It finally searches for the best pattern for that record,
     * and returns an array of SbmeBusinessName objects. Here the array size
     * represents the number of different addresses inside the same string.
     * For example 404 & 408 E wilson st has two addresses. Therefore the size
     * is 2. In general the size will be 1.
     *
     * @param record the strings to standardize
     * @return an array of standardized SbmeStandRecord objects
     * @throws SbmeStandardizationException the generic stand exception
     */
    public SbmeStandRecord[] standardize(String record) 
        throws SbmeStandardizationException {
        
        /* Implementation */
        ArrayList list = new ArrayList();
        list.add(record);
        SbmeStandRecord[][] standRecs = standardize(list);
        if (standRecs == null) {
            throw new SbmeStandardizationException("Null pointer returned for record: " + record);
        }
        if (!(standRecs.length == 1)) {
            throw new SbmeStandardizationException("Incorrect standardization for record: " + record);
        }
        return standRecs[0];
    }
    
    
    /**
     * This method reads in an array of business name strings, parse them,
     * search for the types and the standard formats of all the tokens of
     * each string using lookup tables. Finally, it searches for the
     * best pattern for each record. Returns an 2D array of standardized
     * business name objects (inside the addressList). The first dimension
     * is for the different records and the second one is for possible
     * multiple addresses in one string.
     *
     * @param businessList the list of the strings to standardize
     * @throws SbmeStandardizationException the generic stand exception
     * @return a 2D array of standardized SbmeStandRecord objects
     */
    public SbmeStandRecord[][] standardize(ArrayList businessList)
        throws SbmeStandardizationException {
          
        int i;
        int j;
        int jk = 0;
//        InputPattern pa = InputPattern.compile("\\S");
//        Matcher mat;
        Object ob;
        int max;
        ArrayList arLi;

        /* Number of record submitted for standardization */
        int numberOfRec = businessList.size();
        
        /* Number of patterns */
        int numPats;  
        
        /* intialize the array of aliasListType */
        sVar.aliasListType = new HashMap[numberOfRec];

        /* Temporarily hold the array of input records */
        String[] record = new String[numberOfRec];
        
        /* Number of fields per business entity */
        int[] numberOfKeys = new int[numberOfRec];
        
        /* Number of fields associated with a business name per record */
        int numberOfFieldKeys[][] = new int[numberOfRec][1];
 
        /* Holds the tokens obtained from parsing the string */
        ArrayList[] fieldList = new ArrayList[numberOfRec];
        
        /* Holds the types of the tokens within a record */
        ArrayList[] fieldsType = new ArrayList[numberOfRec];

        /*
         * Defines the standardized address to be returned 
         */
        StandBusinessName[] standBizName = new StandBusinessName[numberOfRec];
        
        for (i = 0; i < numberOfRec; i++) {
            fieldList[i] = new ArrayList();
            
            /* intialize the array of aliasListType */
            sVar.aliasListType[i] = new HashMap();
        }
 

        /* define an array of strings to hold the fields' types */
        StringBuffer[][][] orderedKeys = new StringBuffer[numberOfRec][1][WORDS];

        /* define an array of strings to hold the fields' types */
        StringBuffer[][][] orderedValues = new StringBuffer[numberOfRec][1][WORDS];
        
        
        /* Define instances of the SbmeStandRecord class */
        SbmeStandRecord[][] standObj = new SbmeStandRecord[numberOfRec][1];
        


        for (i = 0; i < numberOfRec; i++) {
        
            /* Read the objects from the ArrayList */
            record[i] = (String) businessList.get(i);

            /* Regular expression that catches empty strings */
//            mat = pa.matcher(record[i]);
            
            /* test if the record is empty or null */
            if ((record[i] == null) || (record[i].length() == 0) /*|| !(mat.lookingAt())*/) {
                
               /*
                * Create an instance of the BusinessName through
                * the call of the SbmeStandRecordFactory instance
                */
                standObj[i][0] = SbmeStandRecordFactory.getInstance("BusinessName");
                
                // Jump to the next record
                continue;
            }

            /*
             * Start first by parsing the string into tokens. We input a string,
             * and return an array of strings 'fieldList' which hold the fields.
             */
            fieldList[i].addAll(Arrays.asList(bizParse.performParsing(record[i], sVar).toArray()));
            
            
            /*
             * We then normalize the different fields. We send in the parsed
             * fields and return back the normalized ones. We keep the same
             * number of fields, but we replace them with their normalized forms
             * if found, or as an empty string if the field is to be removed. Also
             * we define the fieldType inside this method.
             */
            fieldsType[i] = bizNorm.performStringNormalization(fieldList[i], sVar, i);


            /*
             * Finally, identify the best pattern (the order in the arangement of the
             * fields) for the record, and also the fields to be returned.
             */
            standBizName[i] = bizPatt.getBestPatterns(fieldsType[i], fieldList[i], sVar);

           /*
            * Create an instance of the BusinessName class through
            * the call of the SbmeStandRecordFactory instance
            */
            standObj[i][0] = SbmeStandRecordFactory.getInstance("BusinessName");

            /*
             * Copy the address fields into the orderedKeys and orderedValues variables
             */
            numberOfKeys[i] = PopulateBizStandRecords.copyFields(standBizName[i], orderedKeys[i],
                                                           orderedValues[i], sVar);

          
            max = numberOfKeys[i] - sVar.intV;    

            // Set first the parsed data
            for (j = 0; j < max; j++) {
                standObj[i][0].setValue(orderedKeys[i][0][j].toString(),
                                        orderedValues[i][0][j].toString());                                      
            }


            // handle the additional information related to the industry sector list           
            if (sVar.indSectorF) {      
                arLi = standBizName[i].getIndustrySectorList();

                if (arLi.size() > 1) {               
                    standObj[i][0].setValues(orderedKeys[i][0][j].toString(),
                        standBizName[i].getIndustrySectorList());
                        
                } else {  
                    
                    standObj[i][0].setValue(orderedKeys[i][0][j++].toString(),
                        arLi.get(arLi.size() - 1).toString());                
                }
            }
        
            // handle the additional information related to the aliases
            if (sVar.aliasListF && sVar.incompAliasFlag) {
                arLi = standBizName[i].getAliasList();

                if (arLi.size() > 1) {
                    standObj[i][0].setValues(orderedKeys[i][0][j].toString(), arLi);
                } else {
        
                    ob = arLi.get(0);
        
                    if (ob instanceof HashMap) {
        
                        if (((HashMap) ob).containsKey("CompanyAcronym")) {
        
                            standObj[i][0].setValue(orderedKeys[i][0][j].toString(),
                            (String) ((HashMap) arLi.get(0)).get("CompanyAcronym"));
        
                        } else if (((HashMap) ob).containsKey("CompanyListOfAcronym")) {
        
                            standObj[i][0].setValue(orderedKeys[i][0][j].toString(),
                            (String) ((ArrayList) ((HashMap) arLi.get(0)).get("CompanyListOfAcronym")).get(0));
                        }
        
                    } else {
                        standObj[i][0].setValue(orderedKeys[i][0][j].toString(),
                            (String) standBizName[i].getAliasList().get(0));
                    }
                }
            }
            ((SbmeBusinessName) standObj[i][0]).setSignature(standBizName[i].getSignature());
        }
        return standObj;
    }
}
