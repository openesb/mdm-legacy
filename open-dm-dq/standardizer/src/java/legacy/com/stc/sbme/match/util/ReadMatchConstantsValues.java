/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * This class reads all the configured maximum values used in the matching code
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class ReadMatchConstantsValues {
    
    private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.match.util.ReadMatchConstantsValues");  
    /* declare all constants used in the matching process in this class */
    /*    */
    private static int nFields;
    /* Maximum number of different patterns */
    private static int maxFreqTableSize;
    /* The maximum number of the sum of possible agreements */
    private static int maxNumberTables;
    // For the EM Algorithm
    /* Number of latent k-classes used in the algorithm */
    private static int mcls;

    /*    */
    private static char frequency;
    /*    */
    private static char exact;
    /*    */
    private static char uncertainty;
    /*    */
    private static char number;
    /*    */
    private static char prorated;
    /*    */
    private static char date;
    /*    */
//    private static String matchTypeList; 
    /*    */
    private static char bigram;
    /*    */
    private static char inversion;
    /*    */
    private static char majorInversion;
    /*    */
    private static char cross;
    /*    */
    private static char critical;
    /*    */
    private static char addressNumeric;
    /*    */
    private static char firstName;
    /*    */
    private static char lastName;


    /*    */
    private static final int PRIMARY = 0;
    /*    */
    private static final int SECONDARY = 1;
    /*    */
    private static final int TERTIARY = 2;

    /**
     * A constants holding a 50 empty-character block
     */
    public static final String NULL50 = "                                                  ";
    /**
     * A constants holding a 60 empty-character block
     */
    public static final String NULL60 = "                                       " 
                                       + "                     ";
    /** 
     * A constants holding a 50 zero-character block
     */
    public static final String ZERO50 = "00000000000000000000000000000000000000000000000000";
    /**
     * A constants holding a 60 zero-character block
     */
    public static final String ZERO60 = "000000000000000000000000000000000000000" 
                                       + "000000000000000000000";
 

    /**
     * open an input stream to read values available from matchConstantsValues.txt
     *
     * @param matchDataStream an input stream from the repository
     * @throws SbmeMatchingException a matching exception
     * @throws IOException an I/O exception
     */
    public static void readMatchConstants(InputStream[] matchDataStream)
        throws SbmeMatchingException, IOException {
        
        LOGGER.debug("Starting to read matchConstants.cfg file");
        
        try {
            
            /*  */
            Properties prop = new Properties();           
            prop.load(matchDataStream[0]);

            // assign the read values to all the declared constants
            nFields = Integer.parseInt(prop.getProperty("nFields"));

            maxFreqTableSize = Integer.parseInt(prop.getProperty("maxFreqTableSize"));
            maxNumberTables = Integer.parseInt(prop.getProperty("maxNumberTables"));
            // used in EMalgorithm class
            mcls = Integer.parseInt(prop.getProperty("mcls"));

        } catch (IOException e) {
            LOGGER.fatal("Coud not read matchConstants.cfg file", e);
        }
 
        LOGGER.debug("Starting to read matchConstantsInternal.cfg file");
        
        try {
            
            /*  */
            Properties prop = new Properties();
            prop.load(matchDataStream[1]);


            frequency = (prop.getProperty("frequency")).charAt(0);
            exact = (prop.getProperty("exact")).charAt(0);
            uncertainty = (prop.getProperty("uncertainty")).charAt(0);
            bigram = (prop.getProperty("bigram")).charAt(0);
            number = (prop.getProperty("number")).charAt(0);
            prorated = (prop.getProperty("prorated")).charAt(0);
            date = (prop.getProperty("date")).charAt(0);
//            matchTypeList = prop.getProperty("matchTypeList");
            inversion = (prop.getProperty("inversion")).charAt(0);
            majorInversion = (prop.getProperty("majorInversion")).charAt(0);
            cross = (prop.getProperty("cross")).charAt(0);
            critical = (prop.getProperty("critical")).charAt(0);
            addressNumeric = (prop.getProperty("addressNumeric")).charAt(0);
            firstName = (prop.getProperty("firstName")).charAt(0);
            lastName = (prop.getProperty("lastName")).charAt(0);

        } catch (IOException e) {
            LOGGER.fatal("Coud not read matchConstantsInternal.cfg file", e);
        }
    }
   
    
    /**
     * @return aaaa
     */
    public static int getNFields() {
        return nFields;
    }
    
    /** 
     * @return aaa   
     */
    public static int getMaxFreqTableSize() {
        return maxFreqTableSize;
    } 
 
    /** 
     * @return  aaaaa
     */
    public static int getMaxNumberTables() {
        return maxNumberTables;
    }

    /**
     * @return Number of latent k-classes used in the algorithm
     */
    public static int getMcls() {
        return mcls;
    }


    /** 
     * @return  aaaaa
     */
    public static char getFrequency() {
        return frequency;
    }
    /** 
     * @return aaaaa  
     */
    public static char getExact() {
        return exact;
    }
    
    /** 
     * @return  aaaaa
     */
    public static char getUncertainty() {
        return uncertainty;
    }
    
    /** 
     * @return  aaaa
     */
    public static char getNumber() {
        return number;
    }
    
    /** 
     * @return  aaaaa
     */
    public static char getProrated() {
        return prorated;
    }
    
    /** 
     * @return  aaaaa
     */
    public static char getDate() {
        return date;
    }
 
//    /** 
//     * @return  aaaaa
//     */
//    public static String getMatchTypeList() {
//        return matchTypeList;
//    }
    
    /** 
     * @return aaaaa
     */
    public static char getBigram() {
        return bigram;
    }
    
    /** 
     * @return  aaaaa
     */
    public static char getInversion() {
        return inversion;
    }
    
    /** 
     * @return aaaaa
     */
    public static char getMajorInversion() {
        return majorInversion;
    }
    /** 
     * @return aaaaa  
     */
    public static char getCross() {
        return cross;
    }
    
    /** 
     * @return aaaaa
     */
    public static char getCritical() {
        return critical;
    }
    
    /** 
     * @return aaaaa
     */
    public static char getAddressNumeric() {
        return addressNumeric;
    }
    
    /** 
     * @return aaaaa
     */
    public static char getFirstName() {
        return firstName;
    }
    
    /** 
     * @return  aaaaa
     */
    public static char getLastName() {
        return lastName;
    }
    
    
    // For the EM Algorithm    
    /** 
     * @return The maximum number of the sum of possible agreements 
     */
    public static int getMComb() {
        return (2 * nFields);
    }
    
 
    /** 
     * @return Number of latent k-classes used in the algorithm
     */
    public static int getNumberPatt() {
    
        // for now set the number of fields to 10
        // (int) Math.pow(2, nFields);
        return (int) Math.pow(2, 10);
    }
   
    
    /** 
     * @return the constant 'PRIMARY'
     */
    public static int getPrimary() {
        return PRIMARY;
    }
    
    /** 
     * @return the constant 'SECONDARY'
     */
    public static int getSecondary() {
        return SECONDARY;
    }
    
    /** 
     * @return the constant 'TERTIARY'
     */
    public static int getTertiary() {
        return TERTIARY;
    }
}
