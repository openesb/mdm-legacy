/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.util;

/**
 * A wrapper class for the boolean type
 * 
 * @author Sofiane Ouaguenouni 
 * @version $Revision: 1.1.2.1 $
 */
public class BooleanWrapper {

    private Boolean bol;

    /**
     * Constructor
     */
    public BooleanWrapper() {
        super();
    }

    /**
     * 
     *
     * @return a boolean object
     */
    public Boolean getBoolean () {
         return bol;
    }

    /**
     *
     * @param bol a boolean
     */
    public void setBoolean(Boolean bol) {
        this.bol = bol;
    }
}
