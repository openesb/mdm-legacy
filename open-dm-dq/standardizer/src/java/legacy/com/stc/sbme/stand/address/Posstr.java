/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

/**
 * 
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class Posstr {

    private int begTok;
    private int  order;

    /**
     * Default constructor
     */
    Posstr() {
        super();
    }

    /**
     * @return a 
     */
    int getBegTok() {
        return begTok;
    }
    
    /**
     * @return a
     */
    int getOrder() {
        return order;
    }

    /**
     * @param begTok aaa
     * @return an integer
     */    
    int setBegTok(int begTok) {
        return this.begTok = begTok;
    }

    /**
     * @param order aaa
     * @return an integer
     */
    int setOrder(int order) {
        return this.order = order;
    }
}
