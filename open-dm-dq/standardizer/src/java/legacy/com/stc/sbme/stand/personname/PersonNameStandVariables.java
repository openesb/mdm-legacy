/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.personname;

import java.io.File;
import java.io.PrintWriter;

import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * Defines all the 'global' variables used in the standardization code.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PersonNameStandVariables {
    
    /*      */
    private final int WORDS;
        /*      */
    private final int PTRNMAX1;
        /*      */
    private final int PTRNMAX2;
        /*      */
    private final int CONJMAX;
        /*      */
    private final int JRSRMAX;
        /*      */
    private final int NICKMAX;
    /*      */
    private final int LASTMAX;
        /*      */
    private final int PREMAX;
        /*      */
    private final int SKPMAX;
        /*      */
    private final int SUFMAX;
        /*      */
    private final int TITLMAX;
    /*      */
    private final int TWOMAX;
        /*      */
    private final int THREMAX;
        /*      */
    private final int BLNKMAX;
    /*      */
    private final int DASH;

    
    /**      */
    public int nbTitl;
    /**      */
    public int nbConj;
    /**      */
    public int nbFirst;
    /**      */
    public int nbLast;    
    /**      */
    public int nbJrsr;
    /**      */
    public int nbPatt;
    /**      */
    public int nbOccupSuff;
    /**      */
    public int nbPref;
    /**      */
    public int nbOther;
    /**      */
    public int nbTwo;
    /**      */
    public int nbThree;
    /**      */
    public int nbFirstDash;
    /**      */
    public int nbWords;
    /**      */
    public int nbStan;
    /**      */
    public int nbBus;
    /**      */
    public int nbChars;

    
    /**      */
    public StringBuffer[] conj;
    /**      */
    public StringBuffer[] jrsrcd;
    /**      */
    public StringBuffer[] jrsr2;
    /**      */
    public StringBuffer[] firstName;
    /**      */
    public StringBuffer[] lastName;
    /**      */
    public String[] standFirstName;
    /**      */
    public String[] standLastName;    
    /**      */
    public StringBuffer[] nameGender;
    /**      */
    public StringBuffer[] ptrn1;
    /**      */
    public int[][] ptrn2;
    /**      */
    public StringBuffer[] prefix;
    /**      */
    public String[] standPrefix;    
    /**      */
    public StringBuffer[] other;
    /**      */
    public StringBuffer[] otherIndex;
    /**      */
    public StringBuffer[] occupSuffix;
    /**      */
    public StringBuffer[] title;
    /**      */
    public String[] standTitle;
    /**      */
    public String[] titleGender;
    /**      */
    public StringBuffer[] twoSuff;
    /**      */
    public StringBuffer[] threeSuff;
    /**      */
    public StringBuffer[] dashFirstName;
    /**      */
    public StringBuffer[] nameDashGender;
    /**      */
    public String[] specChars;
    
    /**      */
    public boolean[] emailFlag;
    /**      */
    public boolean[] lastNameDashPreFlag;
    /**      */
    public boolean[] lastNameWithPrefixPreFlag;
    /**      */
    public boolean[] midInitialFlag;
    /**      */
    public boolean[] genSuffixPreFlag;
    /**      */
    public boolean[] titlePreFlag;
    /**      */
    public boolean[] lastNamePreFlag;
    /**      */
    public boolean[] firstNameFirstPreFlag;
    /**      */
    public boolean[] firstNameSecondPreFlag;
    /**      */
    public boolean[] firstNameDashPreFlag;
    /**      */
    public boolean[] firstNamePreFlag;
    
    /**      */
    public StringBuffer[] wordType;
    /**      */
    public StringBuffer[][] sWordType;
    /**      */
    public StringBuffer[] wordTypeFound;
    /**      */
    public StringBuffer[][][] s2WordType;
    /**      */
    public StringBuffer[] gender;
    /**      */
    public File fptri;
    /**      */
    public File fptrn;
    /**      */
    public PrintWriter pwFptro;
    /**      */
    public PrintWriter pwFother;
    /**      */
    public String delimiters;
    
    /**
     * Default constructor
     *
     */
    public PersonNameStandVariables(ReadStandConstantsValues vals) {
        WORDS = vals.getWords();
        PTRNMAX1 = vals.getPtrnmax1();
        PTRNMAX2 = vals.getPtrnmax2();
        CONJMAX = vals.getConjmax();
        JRSRMAX = vals.getJrsrmax();
        NICKMAX = vals.getNickmax();
        PREMAX = vals.getPremax();
        SKPMAX = vals.getSkpmax();
        SUFMAX = vals.getSufmax();
        TITLMAX = vals.getTitlmax();
        TWOMAX = vals.getTwomax();
        THREMAX = vals.getThremax();
        BLNKMAX = vals.getBlnkmax();
        DASH = vals.getDashSize();
        LASTMAX = vals.getLastMax();
        conj = new StringBuffer[CONJMAX];
        jrsrcd = new StringBuffer[JRSRMAX];
        jrsr2 = new StringBuffer[JRSRMAX];
        firstName = new StringBuffer[NICKMAX];
        standFirstName = new String[NICKMAX];
        lastName = new StringBuffer[LASTMAX];
        standLastName = new String[LASTMAX];
        nameGender = new StringBuffer[NICKMAX];
        ptrn1 = new StringBuffer[PTRNMAX1];
        ptrn2 = new int[PTRNMAX1][PTRNMAX2];
        prefix = new StringBuffer[PREMAX];
        standPrefix = new String[PREMAX];
        other = new StringBuffer[SKPMAX];
        otherIndex = new StringBuffer[SKPMAX];
        occupSuffix = new StringBuffer[SUFMAX];
        title = new StringBuffer[TITLMAX];
        standTitle = new String[TITLMAX];
        titleGender = new String[TITLMAX];
        twoSuff = new StringBuffer[TWOMAX];
        threeSuff = new StringBuffer[THREMAX];
        dashFirstName = new StringBuffer[DASH];
        nameDashGender = new StringBuffer[NICKMAX];
        specChars = new String[BLNKMAX];
        emailFlag = new boolean[WORDS];
        lastNameDashPreFlag = new boolean[WORDS];
        lastNameWithPrefixPreFlag = new boolean[WORDS];
        midInitialFlag = new boolean[WORDS];
        genSuffixPreFlag = new boolean[WORDS];
        titlePreFlag = new boolean[WORDS];
        lastNamePreFlag = new boolean[WORDS];
        firstNameFirstPreFlag = new boolean[WORDS];
        firstNameSecondPreFlag = new boolean[WORDS];
        firstNameDashPreFlag = new boolean[WORDS];
        firstNamePreFlag = new boolean[WORDS];
        wordType = new StringBuffer[WORDS];
        sWordType = new StringBuffer[3][WORDS];
        wordTypeFound = new StringBuffer[WORDS];
        gender = new StringBuffer[3];
        delimiters = " \t\n\r\f";
        for (int i = 0; i < WORDS; i++) {
            
            wordType[i] = new StringBuffer(3);
            
            wordTypeFound[i] = new StringBuffer("F");
            
            for (int j = 0; j < 3; j++) {
                sWordType[j][i] = new StringBuffer(3);
            }
            
        }
        
        for (int j = 0; j < 2; j++) {
            gender[j] = new StringBuffer(1);
        }
    }
    
    /**
     * Define a factory method that return an instance of the class
     *
     * @return a PersonNameStandVariables instance
     */
    public PersonNameStandVariables getInstance(ReadStandConstantsValues vals) {
        return new PersonNameStandVariables(vals);
    }

}
