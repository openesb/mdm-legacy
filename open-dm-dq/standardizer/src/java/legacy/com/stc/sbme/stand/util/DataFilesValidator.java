/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.TreeSet;

import com.stc.sbme.api.SbmeStandardizationException;

/**
 * @author souaguenouni
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DataFilesValidator {
    
    /**
     * Ensure, for a certain type of data files that the keys are unique and that there is
     * only one level of mapping between the different normalized names
     *
     * @param tset the list of names
     * @param ind1 the size of the first column
     * @param ind2 the size of the second column
     * @throws SbmeStandardizationException
     */    
    public static TreeSet enforceOneLevelMapping(TreeSet tset, int ind1, int ind2) {

        int i; 
        int len;
        String s;
        String prevS = "                                    ";   
        Iterator it = tset.iterator(); 
        LinkedList ll = new LinkedList(); 
        LinkedHashMap lm = new LinkedHashMap();
        TreeSet ts = new TreeSet();
                 
        // Ensure that all the key names are unique
        while (it.hasNext()) {
            s = (String) it.next();
            if (prevS.substring(0, ind1).trim().compareTo(s.substring(0, ind1).trim()) == 0) {
                if(s.charAt(ind1) == '0' && prevS.charAt(ind1) != '0') {  
                    tset.remove(prevS); 
                } else {
                    it.remove();
                }
            } else {
                // Create a list of root names
                if (s.charAt(ind1) == '0') {
                    ll.add(s.substring(0, ind1).trim());
                }
                lm.put(s.substring(0, ind1).trim(), s.substring(ind1, ind2).trim());                
            }
            prevS = s;            
        }
        
        // re-Open the iterator
        it = tset.iterator();
        String sub1;
        String sub2;
        String ss1;
        StringBuffer ss = new StringBuffer(); 
        Object ob;
        
        // Ensure one level of mapping
        while (it.hasNext()) {
            s = (String) it.next();     
            sub1 = s.substring(0, ind1).trim();                                
            sub2 = s.substring(ind1, ind2).trim();
            
            if (sub2.compareTo("0") == 0) {
                ts.add(s);                  
                continue;
            } else {
                // Search for the names in the list of root names
                if(!ll.contains(sub2)) {
                    
                    // Read the name on the left (one level lower)
                    if (lm.containsKey(sub2)) {
                        
                        if (ll.contains(lm.get(sub2))) {
                            lm.put(sub1, lm.get(sub2));

                        } else {
                            ob = lm.get(lm.get(sub2));
                            if (ll.contains(ob)) {
                                lm.put(lm.get(sub2), ob);
                                lm.put(sub1, ob);  
                                  
                            } else {
                                ob = lm.get(lm.get(lm.get(sub2)));
                                if (ll.contains(ob)) {
                                    lm.put(lm.get(lm.get(sub2)), ob);
                                    lm.put(lm.get(sub2), ob); 
                                    lm.put(sub1, lm.get(lm.get(sub2))); 
                                }
                            }
                        }                        
                    }
                    // Update the TreeSet
                    ss1 = (String)lm.get(sub1);
                    len = ss1.length();                  
                    ss.append(s);
                    ss = ss.replace(ind1, ind1 + len, ss1);
                    
                    for(i = ind1 + len; i < ind2; i++) {
                        ss.setCharAt(i, ' ');
                    }
                    ts.add(ss.toString());                   
                    ss.setLength(0);
                    
                } else {
                    ts.add(s); 
                }

            }
        }
        return ts;                                                                                                    
    }    

    /**
     * Ensure, for a certain type of data files that the keys are unique and that there is
     * only one level of mapping between the different normalized names
     *
     * @param tset the list of names
     * @param ind1 the size of the first column
     * @throws SbmeStandardizationException
     */    
    public static TreeSet enforceOneLevelMapping(TreeSet tset, int ind1) {

        int i; 
        int len;
        int origL;
        String s;
        String prevS = "                                         ";   
        Iterator it = tset.iterator(); 
        LinkedList ll = new LinkedList(); 
        LinkedHashMap lm = new LinkedHashMap();
        TreeSet ts = new TreeSet();
                 
        // Ensure that all the key names are unique
        while (it.hasNext()) {

            s = (String) it.next();
            if (prevS.substring(0, ind1).trim().compareTo(s.substring(0, ind1).trim()) == 0) {
                if(s.charAt(ind1) == '0' && prevS.charAt(ind1) != '0') {  
                    tset.remove(prevS); 
                } else {
                    it.remove();
                }
            } else {
                // Create a list of root names
                if (s.charAt(ind1) == '0') {
                    ll.add(s.substring(0, ind1).trim());
                }
                lm.put(s.substring(0, ind1).trim(), s.substring(ind1).trim());                
            }
            prevS = s;            
        }
        
        // re-Open the iterator
        it = tset.iterator();
        String sub1;
        String sub2;
        String ss1;
        StringBuffer ss = new StringBuffer(); 
        Object ob;
        
        // Ensure one level of mapping
        while (it.hasNext()) {
            s = (String) it.next();     
            sub1 = s.substring(0, ind1).trim();                                
            sub2 = s.substring(ind1).trim();
            
            if (sub2.compareTo("0") == 0) {
                ts.add(s);                               
                continue;
            } else {
                // Search for the names in the list of root names
                if(!ll.contains(sub2)) {
                    
                    // Read the name on the left (one level lower)
                    if (lm.containsKey(sub2)) {
                        
                        if (ll.contains(lm.get(sub2))) {
                            lm.put(sub1, lm.get(sub2));

                        } else {
                            ob = lm.get(lm.get(sub2));
                            if (ll.contains(ob)) {
                                lm.put(lm.get(sub2), ob);
                                lm.put(sub1, ob);  
                                  
                            } else {
                                ob = lm.get(lm.get(lm.get(sub2)));
                                if (ll.contains(ob)) {
                                    lm.put(lm.get(lm.get(sub2)), ob);
                                    lm.put(lm.get(sub2), ob); 
                                    lm.put(sub1, lm.get(lm.get(sub2))); 
                                }
                            }
                        }                        
                    }
                    // Update the TreeSet
                    ss1 = (String)lm.get(sub1);
                    len = ss1.length();
                    origL = s.length();                  
                    ss.append(s);
                    ss = ss.replace(ind1, ind1 + len, ss1);
                    
                    for(i = ind1 + len; i < origL; i++) {
                        ss.setCharAt(i, ' ');
                    }
                    ts.add(ss.toString());                   
                    ss.setLength(0);
                    
                } else {
                    ts.add(s); 
                }

            }
        }
        return ts;                                                                                                    
    }    
}
