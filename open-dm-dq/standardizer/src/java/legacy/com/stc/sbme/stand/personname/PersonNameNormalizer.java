/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.personname;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmePersonName;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.TableSearch;
import com.stc.sbme.util.EmeUtil;

/**
 * Main class to process any request to match candidate to reference
 * records
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PersonNameNormalizer {
    
    private static final String FIRSTNAME = SbmePersonName.FIRSTNAME;
    private static final String MIDDLENAME = SbmePersonName.MIDDLENAME;
    private static final String LASTNAME = SbmePersonName.LASTNAME;
    private static final String TITLE = SbmePersonName.TITLE;
    private static final String GENDER = SbmePersonName.GENDER;
    private static final String GENSUFFIX = SbmePersonName.GENSUFFIX;
    private static final String OCCUPSUFFIX = SbmePersonName.OCCUPSUFFIX;
    private static final String LASTPREFIX = SbmePersonName.LASTPREFIX;
    private static final String BUSINESSORRELATED = SbmePersonName.BUSINESSORRELATED;
    private static final String PERSONSTATUS = SbmePersonName.PERSONSTATUS;
    
    /**
     * Define a factory method that return an instance of the class
     *
     * @return a PersonNameNormalizer instance
     */
    public static PersonNameNormalizer getInstance() {
        return new PersonNameNormalizer();
    }
    
    
    /**
     *
     *
     * @param fieldKey the keys
     * @param standObj the SbmeStandRecord object to normalize
     * @param sVar the instance of the PersonNameStandVariables class
     * @param domain the geolocation
     * @throws SbmeStandardizationException the generic stand exception
     */
    public void performFieldsNormalization(String[] fieldKey, SbmeStandRecord standObj,
    PersonNameStandVariables sVar, String domain)
    throws SbmeStandardizationException {
        
        /* The number of tokens */
        int numberOfKeys = fieldKey.length;
        
        int i;
        int j;
        int id;
        int index;
        /* String used in the search for last name prefixes within lastname */
        StringBuffer sb1 = new StringBuffer();
        StringBuffer srb;
        String tempStr;
        String clStr;
        StringBuffer strB;
//        InputPattern pa = InputPattern.compile("\\S"); 
//        Matcher mat;

        /* Set the status of the records to true */
        boolean[] keyStatus = new boolean[numberOfKeys];
        
        /* Holds the values of the fields associated with the keys */
        String[] fieldValue = new String[numberOfKeys];
        
        /* The array of strings that hold the normalized fields */
        String[] normFields = new String[numberOfKeys];
        
        for (i = 0; i < numberOfKeys; i++) {
            
            fieldValue[i] = standObj.getValue(fieldKey[i]);
 
            /* We assume initially the key value is not null */ 
            keyStatus[i] = true;
            
            /* Regular expression that catches empty strings */
//            mat = pa.matcher(fieldValue[i]);

            /* test if the key is empty or null */
            if ((fieldValue[i] == null) || (fieldValue[i].length() == 0) /*|| !(mat.lookingAt())*/) {
                
                // Flag a record that is null, empty or has only white characters 
                keyStatus[i] = false;
                
                // Jump to the next index  
                continue;
            }
            
            /* transform field to uppercase and trim white spaces */
            fieldValue[i] = fieldValue[i].trim().toUpperCase();

            /* Remove special characters types (with sbme exceptions) */
            fieldValue[i] = removeSpecChars(fieldValue[i], fieldKey[i], i, sVar);
        }
        
        
        /* Set initial gender value to neutral */
        standObj.setValue("Gender", "N");
        
       /*
        * For every field, search for the corresponding standard field
        * Then, update the field
        */
        for (i = 0; i < numberOfKeys; i++) {
            
            /* Set the initial status to false */
            standObj.setStatusValue(fieldKey[i], "false");

            /* 
             * If the record is intially null, empty or filled with
             * white chracters, skip this part.
             */
            if (!keyStatus[i]) {
            
                /* Go to the next iteration */
                continue;
            }
            // Check if the field contains diacretical marks. 
            // Remove them when searching the tables
            clStr = EmeUtil.removeDiacriticalMark(fieldValue[i]);

            /* First, search if the field's key is a title */
            if (fieldKey[i].compareTo(TITLE) == 0) {

                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbTitl,
                sVar.title)) >= 0) {
                    
                    /* Check if there is a standard form for this field */
                    if (sVar.standTitle[index].toString().compareTo("0") != 0) {
                        standObj.setValue(TITLE, sVar.standTitle[index]);
                        
                    } else {
                        standObj.setValue(TITLE, fieldValue[i]);
                    }
                    
                    /* Define the gender of this person */
                    if (standObj.getValue(GENDER).compareTo("N") == 0) {
                        
                        standObj.setValue(GENDER, sVar.titleGender[index].toString());
                        
                        /* Set the status of gender to true */
                        standObj.setStatusValue(GENDER, "true");
                    }

                    /* Set the status of title to true */
                    standObj.setStatusValue(TITLE, "true");
                    
                } else {
                    srb = searchLongTitle(fieldValue[i], sVar);
                    if (srb.length() > 0) {
                        standObj.setValue(TITLE, srb.toString());
                    } else {
                        standObj.setValue(TITLE, fieldValue[i]);
                    }
                }
                
            } else if (fieldKey[i].compareTo(OCCUPSUFFIX) == 0) {
            /* Else, search the occupational suffixes table (i.e. 'Esq' or 'PHD') */
                
                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbOccupSuff,                
                sVar.occupSuffix)) >= 0) {
                    
                    standObj.setValue(OCCUPSUFFIX, fieldValue[i]);
                    
                    /* Set the status of occup suffix to true */
                    standObj.setStatusValue(OCCUPSUFFIX, "true");
                                                                        
                } else {
                    standObj.setValue(OCCUPSUFFIX, fieldValue[i]);
                }
                
            } else if (fieldKey[i].compareTo(FIRSTNAME) == 0) {
                    
//                if (!sVar.firstNameDashPreFlag[i]) {

                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbFirst,
                    sVar.firstName)) >= 0) {
                        
                        /* Check if there is a standard form for this first name */
                        if (sVar.standFirstName[index].compareTo("0") != 0) {

                            standObj.setValue(FIRSTNAME, sVar.standFirstName[index]);
                            
                        } else {

                            standObj.setValue(FIRSTNAME, fieldValue[i]);
                        }
                        
                        if (standObj.getValue(GENDER).compareTo("N") == 0) {
                            
                            /* Define the gender of this person */
                            standObj.setValue(GENDER, sVar.nameGender[index].toString());
                            
                            /* Set the status of gender to true */
                            standObj.setStatusValue(GENDER, "true");
                        }
                        
                        /* Set the status of first name to true */
                        standObj.setStatusValue(FIRSTNAME, "true");
                        

                    
               } else if (sVar.firstNameDashPreFlag[i] && (index = TableSearch.searchTable(clStr, 1, sVar.nbFirstDash,
                sVar.dashFirstName)) >= 0) {

                    standObj.setValue(FIRSTNAME, fieldValue[i]);
                    
                    if (standObj.getValue(GENDER).compareTo("N") == 0) {
                        
                        /* Define the gender of this person */
                        standObj.setValue(GENDER, sVar.nameDashGender[index].toString());
                        
                        /* Set the status of gender to true */
                        standObj.setStatusValue(GENDER, "true");
                    }
                    
                    /* Set the status of first name to true */
                    standObj.setStatusValue(FIRSTNAME, "true");
                    
                    // Reset the different flags
                    for (j = i + 1; j < numberOfKeys; j++) {
                        sVar.firstNameDashPreFlag[j] = false;
                    }                    
                } else {
                    standObj.setValue(FIRSTNAME, fieldValue[i]);
                    
                    /* Set the status of first name to true */
                    standObj.setStatusValue(FIRSTNAME, "false");
                }
                
            } else if (fieldKey[i].compareTo(MIDDLENAME) == 0) {
            /* Second, search if the field's key is middle name */

                /*
                 * If the middle name is longer than one character, then call
                 * the table to search for it
                 */
                if (fieldValue[i].length() > 1) {
                    
                    if ((index = TableSearch.searchTable(clStr, 1, sVar.nbFirst,
                    sVar.firstName)) >= 0) {
                        
                        /* Check if there is a standard form for this first name */
                        if (sVar.standFirstName[index].compareTo("0") != 0) {
                            
                            standObj.setValue(MIDDLENAME, sVar.standFirstName[index]);
                        } else {
                        
                            standObj.setValue(MIDDLENAME, fieldValue[i]);
                        }
                        
                        /* Set the status of middle name to true */
                        standObj.setStatusValue(MIDDLENAME, "true");
                        
                    } else {
                        standObj.setValue(MIDDLENAME, fieldValue[i]);
                        
                        /* Set the status of middle name to true */
                        standObj.setStatusValue(MIDDLENAME, "false");
                    }
                    
                } else {
                    standObj.setValue(MIDDLENAME, fieldValue[i]);
                    
                    /* Set the status of middle name to true */
                    standObj.setStatusValue(MIDDLENAME, "neutral");
                }
                
            } else if (fieldKey[i].compareTo(LASTPREFIX) == 0) {
                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbPref,
                    sVar.prefix)) >= 0) { 
                        /* Check if there is a standard form for this first name */
                        if (sVar.standPrefix[index].compareTo("0") != 0) {
                            standObj.setValue(LASTPREFIX, sVar.standPrefix[index]);
                        
                        } else {
                            standObj.setValue(LASTPREFIX, fieldValue[i]);
                        }                                                              
                
                /* Set the status of last prefix to true */
                standObj.setStatusValue(LASTPREFIX, "true");
                } else {                             
                    standObj.setValue(LASTPREFIX, fieldValue[i]);
                }               
                
            } else if (fieldKey[i].compareTo(LASTNAME) == 0) {
                /* Search for last name prefixes within lastname */
                sb1.setLength(0);
                for (id = sVar.nbPref - 1; id >= 0; id--) {
                    tempStr = sVar.prefix[id].toString();
                    if (fieldValue[i].startsWith(tempStr) 
                    && ((fieldValue[i].length() == tempStr.length()) 
                    || (fieldValue[i].length() > tempStr.length()
                        && fieldValue[i].charAt(tempStr.length()) == ' '))) {
                        
                        /* Check if there is a standard form for this last name */
                        if (sVar.standPrefix[id].compareTo("0") != 0) {
                            sb1.append(sVar.standPrefix[id]).append(fieldValue[i].substring(sVar.prefix[id].length()));;      
                            fieldValue[i] = sb1.toString();
                            break;
                        }             
                    }
                }

                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbLast,
                    sVar.lastName)) >= 0) {
                        
                    /* Check if there is a standard form for this first name */
                    if (sVar.standLastName[index].compareTo("0") != 0) {
                        standObj.setValue(LASTNAME, sVar.standLastName[index]);
                        
                    } else {
                        standObj.setValue(LASTNAME, fieldValue[i]);
                    }                    
                
                    /* Set the status of last name to true */
                    standObj.setStatusValue(LASTNAME, "true");
                } else {
                    standObj.setValue(LASTNAME, fieldValue[i]);                    
                }
                
                /* if the normalized last name is dashed, remove the dash */
                if (sVar.lastNameDashPreFlag[i]) {
                    tempStr = standObj.getValue(LASTNAME);
                    strB = new StringBuffer(tempStr);
                    strB.deleteCharAt(tempStr.indexOf('-'));
                    standObj.setValue(LASTNAME, strB.toString());                               
                }
                
            } else if (fieldKey[i].compareTo(GENSUFFIX) == 0) {

                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbJrsr,
                    sVar.jrsrcd)) >= 0) {
           
                    /* Check if there is a standard form for this field */
                    if (sVar.jrsr2[index].toString().compareTo("0") != 0) {
                        standObj.setValue(GENSUFFIX, sVar.jrsr2[index].toString());
                        
                    } else {
                        standObj.setValue(GENSUFFIX, fieldValue[i]);
                    }
                    
                    /* Set the status of GENSUFFIX to true */
                    standObj.setStatusValue(GENSUFFIX, "true");
                } else {
                    standObj.setValue(GENSUFFIX, fieldValue[i]);
                }
                
            } else if (fieldKey[i].compareTo(BUSINESSORRELATED) == 0) {
                standObj.setValue(BUSINESSORRELATED, fieldValue[i]);
                
                /* Set the status of business-related to true */
                standObj.setStatusValue(BUSINESSORRELATED, "true");
            }
        }
        
        // Reset the different flags
        for (i = 0; i < numberOfKeys; i++) {
            sVar.firstNameDashPreFlag[i] = false;
            sVar.lastNameDashPreFlag[i] = false;
        }
    }
    
    
    /**
     *
     *
     * @param standFields the fields to normalize
     * @param sVar the instance of the PersonNameStandVariables class
     * @param domain the geolocation
     * @return a list of fields
     * @throws SbmeStandardizationException the generic stand exception
     */
    public ArrayList performStringNormalization(ArrayList standFields, PersonNameStandVariables sVar,
    String domain)
    throws SbmeStandardizationException {
        
        /* Total number of fields inside arrayFields */
        int numberFields = standFields.size();
        
           /*
            * Normalize all the tokens by, first, searching for their types, then
            * eventually grouping some tokens together, and return the corresponding
            * standard fields. Use one of the following methods to handle the
            * normalization process when dealing with input string.
            */
        if (numberFields == 1) {
            
            /* Call the algorithm for one field */
            normalizeOneField(sVar, standFields, domain);
            
        } else if (numberFields == 2) {
            
            /* Call the algorithm for one field */
            normalizeTwoFields(sVar, standFields, domain);
            
        } else if (numberFields == 3) {
            
            /* Call the algorithm for one field */
            normalizeThreeFields(sVar, standFields, domain);
            
        } else if (numberFields == 4) {
            
            /* Call the algorithm for one field */
            normalizeFourFields(sVar, standFields, domain);
            
        } else if (numberFields == 5) {
            
            /* Call the algorithm for one field */
            normalizeFiveFields(sVar, standFields, domain);
            
        } else if (numberFields > 5) {
            
            /* Call the algorithm for one field */
            normalizeMoreThanFiveFields(sVar, standFields, domain);
            
        } else {
            /* For an empty string, just return it as it is */
        }
        return standFields;
    }
    
    
    /**
     *
     *
     * @param  sVar the instance of the PersonNameStandVariables class
     * @param arrayFields the list of fields
     * @param domain the geolocation
     */
    private void normalizeOneField(PersonNameStandVariables sVar, ArrayList arrayFields,
    String domain) {
        
        int index = -1;
        
        /* We have only one token */
        String token = (String) arrayFields.get(0);
        
        sVar.wordTypeFound[0].setCharAt(0, 'F');
        sVar.wordTypeFound[0].setLength(1);
        
        if (token.length() > 1) {
            
            /*
             * Check if there are some flags or preflags identified during
             * the parsing process
             */
            if (sVar.firstNameDashPreFlag[0]) {
                
                /* Search for the token as a first name with a dash */
                if ((index = TableSearch.searchTable(token, 1, sVar.nbFirstDash,
                sVar.dashFirstName)) >= 0) {
                    
                    /* Type for first name */
                    sVar.wordType[0].setCharAt(0, '2');
                    
                    /* Add the gender of the person */
                    sVar.gender[0].replace(0, 1, sVar.nameDashGender[index].toString());
                    
                    /* Also include the type of the field */
                    sVar.wordTypeFound[0].setCharAt(0, 'T');;
                    
                } else {
                    
                    /* Consider the token as last name with prefix */
                    sVar.wordType[0].setCharAt(0, '5');
                    
                    /* Add the gender of the person */
                    sVar.gender[0].replace(0, 1, "N");
                    
                    /* Also include the type of the field */
                    sVar.wordTypeFound[0].setCharAt(0, 'F');
                    
                }
            } else {
                
                /* First, search for the token as a first name */
                if ((index = TableSearch.searchTable(token, 1, sVar.nbFirst,
                sVar.firstName)) >= 0) {
                    
                    /* Check if there is a standard form for this first name */
                    if (sVar.standFirstName[index].compareTo("0") != 0) {
                        arrayFields.set(0, sVar.standFirstName[index]);
                    }
                    
                    /* We know that it is first name */
                    sVar.wordType[0].setCharAt(0, '2');
                    
                    /* Add the gender of the person */
                    sVar.gender[0].replace(0, 1, sVar.nameGender[index].toString());
                    
                    /* Also include the type of the field */
                    sVar.wordTypeFound[0].setCharAt(0, 'T');
                    
                } else {
                    
                    /* We assume that it is last name */
                    sVar.wordType[0].setCharAt(0, '5');
                    
                    /* Add the gender of the person */
                    sVar.gender[0].replace(0, 1, "N");
                    
                    /* Also include the type of the field */
                    sVar.wordTypeFound[0].setCharAt(0, 'F');
                    
                }
            }
            
        } else {
        /* If the token is a character, we assume it is last name initial */
            
            /* Consider the token as last name with prefix */
            sVar.wordType[0].setCharAt(0, '5');
            
            /* Add the gender of the person */
            sVar.gender[0].replace(0, 1, "N");
            
            /* Also include the type of the field */
            sVar.wordTypeFound[0].setCharAt(0, 'F');
            
            /* Consider later giving a degree of accuracy measure */
        }
    }
    
    
    /**
     *
     *
     * @param  sVar the instance of the PersonNameStandVariables class
     * @param arrayFields the list of fields
     * @param domain the geolocation
     */
    private void normalizeTwoFields(PersonNameStandVariables sVar, ArrayList
    arrayFields, String domain) {
    }
    
    
    /**
     *
     *
     * @param  sVar the instance of the PersonNameStandVariables class
     * @param arrayFields the list of fields
     * @param domain the geolocation
     */
    private void normalizeThreeFields(PersonNameStandVariables sVar, ArrayList
    arrayFields, String domain) {
    }
    
    
    /**
     *
     *
     * @param  sVar the instance of the PersonNameStandVariables class
     * @param arrayFields the list of fields
     * @param domain the geolocation
     */
    private void normalizeFourFields(PersonNameStandVariables sVar, ArrayList
    arrayFields, String domain) {
    }
    
    
    /**
     *
     *
     * @param  sVar the instance of the PersonNameStandVariables class
     * @param arrayFields the list of fields
     * @param domain the geolocation
     */
    private void normalizeFiveFields(PersonNameStandVariables sVar, ArrayList
    arrayFields, String domain) {
    }
    
    
    /**
     *
     *
     * @param  sVar the instance of the PersonNameStandVariables class
     * @param arrayFields the list of fields
     * @param domain the geolocation
     */
    private void normalizeMoreThanFiveFields(PersonNameStandVariables sVar,
    ArrayList arrayFields, String domain) {
    }
 
 
    /**
     *
     *
     * @param fieldValue the value contained in the field
     * @param fieldKey the type of the field
     * @param ind the index of the field
     * @param sVar the instance of the PersonNameStandVariables class
     * @return a String
     */
    static StringBuffer searchLongTitle(String fieldValue, PersonNameStandVariables sVar) {
        
        int numT;
        int index;
        int i;
        boolean ing = false;
        

        StringBuffer tempS = new StringBuffer();
        StringBuffer srb = new StringBuffer();
        StringTokenizer sTok = new StringTokenizer(fieldValue);
        numT = sTok.countTokens();
        
        String[] strTok = new String[numT];
        
        
        for (i = 0; i < numT; i++) {
            strTok[i] = sTok.nextToken();
        }
        
        for (i = 0; i < numT; i++) {
            
            if (ing) { 
                tempS.append(strTok[i]);
            } else {

                if ((strTok[i].compareTo("&") == 0) || (strTok[i].compareTo("AND") == 0)) {
                    srb.append(strTok[i]);
                    srb.append(" ");
                    continue;
                } else {
                    tempS.setLength(0);
                    tempS.append(strTok[i]);
                }
            }
            
        
            if ((index = TableSearch.searchTable(tempS.toString(), 1, sVar.nbTitl, sVar.title)) >= 0) {
                    
                if (sVar.standTitle[index].toString().compareTo("0") != 0) {
                    
                    srb.append(sVar.standTitle[index]);
                } else {
                    srb.append(tempS);
                }
                
                if (i < numT - 1) {
                    srb.append(" ");
                    ing = false;
                } else {
                    break;
                }
            } else {
                if (i < numT - 1) {
                    tempS.append(" ");
                    ing = true;
                } else {
                    srb.append(tempS);
                }
            }
        }
        return srb;
    }
    
    
    /**
     *
     *
     * @param fieldValue the value contained in the field
     * @param fieldKey the type of the field
     * @param ind the index of the field
     * @param sVar the instance of the PersonNameStandVariables class
     * @return a String
     */
    String removeSpecChars(String fieldValue, String fieldKey, int ind,
            PersonNameStandVariables sVar) {
        
        int i;
        char cc;
        int numberOfChars = fieldValue.length();
        
        /* A wrapper for the field */
        StringBuffer modifyField = new StringBuffer(fieldValue);
        
        for (i = 0; i < numberOfChars; i++) {
            
            cc = modifyField.charAt(i);
            
            if (Character.isWhitespace(cc)) { 
                continue;
            } else if (!Character.isLetterOrDigit(cc)) {

                if ((fieldKey.compareTo("FirstName") == 0 || (fieldKey.compareTo("MiddleName") == 0)) && cc == '-') {
                    
                    if ((i > 0) && (i < (numberOfChars - 1))) {
                        /* This is a legal character in first name */
                        sVar.firstNameDashPreFlag[ind] = true;

                    } else {
                    
                        modifyField.deleteCharAt(i);
                        
                        // Update the number of characters and the index
                        numberOfChars--;
                        i--;
                    }
                    
                } else if ((fieldKey.compareTo("LastName") == 0) && cc == '-'){
                    
                    if ((i > 0) && (i < (numberOfChars - 1))) {
                        /* This is a legal character in last name */
                        sVar.lastNameDashPreFlag[ind] = true;

                    } else {                    
                        modifyField.deleteCharAt(i);
                    
                        // Update the number of characters and the index
                        numberOfChars--;
                        i--;
                    }    
                                                                            
                } else if ((cc == '%') || (cc == '&')) {
                    continue;
                } else if (cc == '-' && (fieldKey.compareTo("Title") == 0)) {
                    continue;
                } else {

                    modifyField.deleteCharAt(i);
                    
                    // Update the number of characters and the index
                    numberOfChars--;
                    i--;
                }
//            } else if (Character.isDigit(cc) && ((fieldKey.compareTo("FirstName") == 0)
//                || (fieldKey.compareTo("LastName") == 0))) {

//                    modifyField.deleteCharAt(i);
                    
                    // Update the number of characters and the index
//                    numberOfChars--;
//                    i--;
            }
        }
        return modifyField.toString();
    }
}
