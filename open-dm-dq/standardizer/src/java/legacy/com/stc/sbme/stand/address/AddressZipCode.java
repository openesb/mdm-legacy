/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.api.SbmeStandardizationException;

/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class AddressZipCode
    implements AddressProperties {
    

    /**
     * Define a factory method that return an instance of the class 
     * @return a AddressZipCode instance
     */
    public static AddressZipCode getInstance() {
        return new AddressZipCode();
    }

    /** 
     * Performs the parsing of the input string 
     * 
     * @param str the input string
     * @param domain the geolocation
     * @throws SbmeStandardizationException an exception 
     * @return the returned string
     */ 
    public StringBuffer findZipCode(String str, String domain)
        throws SbmeStandardizationException {
        
        /* Search for integers that are zipcodes */
        return extractZip(str, domain);
    }
        
    /** 
     * 
     * 
     * @param strg the input string
     * @param domain the geolocation
     * @throws SbmeStandardizationException an exception 
     * @return the returned string
     */ 
    public StringBuffer extractZip(String strg, String domain)
        throws SbmeStandardizationException {
        
        int i;
        int j;
        int k;
        int numTok;
        
        StringBuffer[] zipCode = new StringBuffer[4];
        
        StringBuffer str = new StringBuffer(strg);
        
        int strLength = str.length();
        
        /* Number of numeric tokens */
        numTok = 0;
        
        /* search for tokens that have some digits inside then. */
        for (i = 0; i < strLength; i++) {
            
            if (Character.isWhitespace(str.charAt(i))
                && Character.isDigit(str.charAt(i + 1))) {
                
                for (j = i + 2; j < strLength; j++) {
                
                    if (Character.isWhitespace(str.charAt(j))) {
                    
                        zipCode[numTok] = new StringBuffer();
                        zipCode[numTok].append(str.substring(i, j));
                        
                        numTok++;
                        i = j + 1;
                        break;
                        
                    } else if (Character.isDigit(str.charAt(j)) || (str.charAt(j) == '-')) {
                        continue;
                    } else {
                        i = j + 1;
                        break;
                    }
                }
            }
        }
        
        /* Test if the token is a zipcode. Analyze if it is a 5-digit or more */
        for (k = numTok - 1; k >= 0; k--) {
        
            if (zipCode[numTok].length() == 5) {
                return zipCode[numTok];
                
            } else if (zipCode[numTok].charAt(6) == '-') {
            
                /* Remove any dash or digit and keep only the five digits */
                zipCode[numTok].setLength(5);
                
                /* return the 5-digit zip */
                return zipCode[numTok];
                
            } else {
                return new StringBuffer("     ");
            }
        }
        
        return new StringBuffer("     ");
    }
}
