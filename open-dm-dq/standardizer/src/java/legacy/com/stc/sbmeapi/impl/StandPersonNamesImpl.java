/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;
import java.util.HashMap;
import java.util.Set;

import com.stc.sbmeapi.StandPersonNames;

/**
 * Expose all the methods needed to manipulate person names
 *
 * @author Sofiane O
 */
public class StandPersonNamesImpl
    implements StandPersonNames {
    
    /**             */
    public static final String FIRSTNAME = "FirstName";
    /**             */    
    public static final String MIDDLENAME = "MiddleName";
    /**             */    
    public static final String LASTNAME = "LastName";
    /**             */    
    public static final String LASTNAMEPREFIX = "LastNamePrefix";    
    /**             */    
    public static final String TITLE = "Title";
    /**             */    
    public static final String GENDER = "Gender";
    /**             */    
    public static final String GENSUFFIX = "GenSuffix";
    /**             */    
    public static final String OCCUPSUFFIX = "OccupSuffix";
    /**             */    
    public static final String BUSINESSORRELATED = "BusinessOrRelated";
    /**             */    
    public static final String UNKNOWNTYPE = "Unknown";
    
        
    private HashMap personFieldName;       

    /**
     * Public constructor
     */
    public StandPersonNamesImpl() {
        personFieldName = new HashMap();
    }

    /**
     *
     * @return the first name field
     */
    public String getFirstName() {
        return (String) personFieldName.get(FIRSTNAME);
    }
    
    /**
     *
     * @return the last name field
     */
    public String getLastName() {
        return (String) personFieldName.get(LASTNAME);
    }
    
    /**
     *
     * @return the middle name field
     */
    public String getMiddleName() {
        return (String) personFieldName.get(MIDDLENAME);
    }
    
    /**
     *
     * @return the title field
     */
    public String getTitle() {
        return (String) personFieldName.get(TITLE);
    }
    
    /**
     *
     * @return the generational suffix field
     */
    public String getGenSuffix() {
        return (String) personFieldName.get(GENSUFFIX);
    }
    
    
    /**
     *
     * @return the person's gender field
     */
    public String getGender() {
        return (String) personFieldName.get(GENDER);
    }
    
    /**
     *
     * @return the occupational suffix field
     */
    public String getOccupSuffix() {
        return (String) personFieldName.get(OCCUPSUFFIX);
    }

    /**
     *
     * @return the occupational suffix field
     */
    public String getLastPrefix() {
        return (String) personFieldName.get(LASTNAMEPREFIX);
    }
    /**
     *
     * @return the business or any related field(s)
     */
    public String getBizOrRelated() {
        return (String) personFieldName.get(BUSINESSORRELATED);
    }
    
    /**
     *
     * @return any unknown type(s)
     */
    public String getUnknownType() {
        return (String) personFieldName.get(UNKNOWNTYPE);
    }
    
    
    /**
     *
     * @param value the first name field
     */
    public void setFirstName(String value) {
        personFieldName.put(FIRSTNAME, value);
    }
    
    /**
     *
     * @param value the last name field
     */
    public void setLastName(String value) {
        personFieldName.put(LASTNAME, value);
    }
    
    /**
     *
     * @param value the middle name field
     */
    public void setMiddleName(String value) {
        personFieldName.put(MIDDLENAME, value);
    }
    
    /**
     *
     * @param value the title field
     */
    public void setTitle(String value) {
        personFieldName.put(TITLE, value);
    }
    
    /**
     *
     * @param value the generational suffix field
     */
    public void setGenSuffix(String value) {
        personFieldName.put(GENSUFFIX, value);
    }
    
    /**
     *
     * @param value the person's gender field
     */
    public void setGender(String value) {
        personFieldName.put(GENDER, value);
    }
    
    /**
     *
     * @param value the occupational suffix field
     */
    public void setOccupSuffix(String value) {
        personFieldName.put(OCCUPSUFFIX, value);
    }
    
    /**
     *
     * @param value the occupational suffix field
     */
    public void setLastPrefix(String value) {
        personFieldName.put(LASTNAMEPREFIX, value);
    }
    /**
     *
     * @param value the business or related term(s)
     */
    public void setBizOrRelated(String value) {
        personFieldName.put(BUSINESSORRELATED, value);
    }
    
    /**
     *
     * @param value the unknown field(s)
     */
    public void setUnknownType(String value) {
        personFieldName.put(UNKNOWNTYPE, value);
    }

    /**
     *
     * @return personFieldName.keySet() the set of all the object's keys
     */
    public Set getAllFields() {
        return personFieldName.keySet();
    }          
}
