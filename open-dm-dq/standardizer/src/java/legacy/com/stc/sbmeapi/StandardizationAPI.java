/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi;

import com.stc.sbme.api.SbmeConfigurationException;
import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;

/**
 * The interfaces that need to be implemented to get a standardization service
 */
public interface StandardizationAPI {
    
    /**
     * Initialize the standardization engine and read all the data & config files
     * @param stream a stream from the data files
     * @throws StandardizationException if the initialization failed
     */        
    void initialize(StandConfigFilesAccess stream)
            throws SbmeStandardizationException, SbmeConfigurationException,
                SbmeMatchEngineException;
    

    /**
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return an array of standardized objects (the array handles cases
     *         where one string has more than one person)
     * @throws SbmeStandardizationException; if the initialization failed
     */        
    StandPersonNames[] standardizePersonName(String stringValue, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException;
            

    /**
     * @param value the array of strings to parse
     * @param domain geographic location of the compared data
     * @return a 2D array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person)
     * @throws SbmeStandardizationException; if the initialization failed
     */        
    StandPersonNames[][] standardizePersonName(String[] stringValues, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException;


    /**
     * @param personObj an StandPersonNames object
     * @param domain geographic location of the compared data
     * @return the same object after normalizing the data
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    StandPersonNames normalizePersonName(StandPersonNames personObj, String domain)
        throws SbmeStandardizationException;

    /**
     * @param personObjs an array of StandPersonNames objects
     * @param domain geographic location of the compared data
     * @return the input array of the same objects after normalization
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    StandPersonNames[] normalizePersonName(StandPersonNames[] personObjs, String domain)
        throws SbmeStandardizationException;
        

    /**
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return a standardized address object
     * @throws SbmeStandardizationException; if the initialization failed
     */        
    StandAddresses standardizeAddress(String stringValue, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException;
            

    /**
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return an array of standardized address objects 
     * @throws SbmeStandardizationException; if the initialization failed
     */        
    StandAddresses[] standardizeAddress(String[] stringValues, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException;


    /**
     * @param addressObj a StandPersonNames object
     * @param domain geographic location of the compared data
     * @return the same object after normalizing the data
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    StandAddresses normalizeAddress(StandAddresses addressObj, String domain)
            throws SbmeStandardizationException;

    /**
     * @param addressObjs an array of address objects
     * @param domain geographic location of the compared data
     * @return the input array of the same objects after normalization
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    StandAddresses[] normalizeAddress(StandAddresses[] addressObjs, String domain)
            throws SbmeStandardizationException;
            
            
    /**
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return a standardized business name object
     * @throws SbmeStandardizationException; if the initialization failed
     */        
    StandBusinessNames standardizeBusinessName(String stringValue)
            throws SbmeStandardizationException, SbmeMatchEngineException;


    /**
     * @param value the array of strings to parse
     * @param domain geographic location of the compared data
     * @return an array of standardized business name objects 
     * @throws SbmeStandardizationException; if the initialization failed
     */        
    StandBusinessNames[] standardizeBusinessName(String[] stringValues)
            throws SbmeStandardizationException, SbmeMatchEngineException;
 
    /**
     * @param businessObj a business name objects
     * @return the same object after normalizing the data
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    StandBusinessNames normalizeBusinessName(StandBusinessNames businessObj)
            throws SbmeStandardizationException;
            
    /**
     * @param businessObjs an array of business name objects
     * @return the input array of the same objects after normalization
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    StandBusinessNames[] normalizeBusinessName(StandBusinessNames[] businessObjs)
            throws SbmeStandardizationException;
            
   
    /**
     * Shutdown and release any resources associated with the standardization
     * engine 
     * @throws SbmeStandardizationException, if the shutdown failed
     */            
    void shutdown() 
        throws SbmeStandardizationException;
            
}
