/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;

import java.util.Set;

import com.stc.sbme.api.SbmeAddress;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbmeapi.StandAddresses;


/**
 *
 */
public class PopulateAddress {

    
    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec an array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandAddresses[] returnStandObject(SbmeStandRecord[][] sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        int numberOfRecords = sRec.length;        
        StandAddressesImpl[] sAddress = new StandAddressesImpl[numberOfRecords];
        
        int numberOfFields;
        
        for (i = 0; i < numberOfRecords; i++) {            
        
            hs = sRec[i][0].getAllFields();
            numberOfFields = hs.size();
            sAddress[i] = new StandAddressesImpl();
            
            oFields = hs.toArray();
            sFields = new String[numberOfFields];

                
            for (j = 0; j < numberOfFields; j++) {
   
                sFields[j] = (String) oFields[j];
                      
                if (sFields[j].compareTo(StandAddressesImpl.HOUSE_NUMBER) == 0) {
                    sAddress[i].setHouseNumber(sRec[i][0].getValue(sFields[j]));
                    continue;
                } else if (sFields[j].compareTo(StandAddressesImpl.SECOND_HOUSE_NUMBER) == 0) {
                    sAddress[i].setSecondHouseNumber(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.ORIG_STREET_NAME) == 0)) {                     
                    sAddress[i].setOrigStreetName(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.ORIG_SECOND_STREET_NAME) == 0)) {
                    
                    sAddress[i].setOrigSecondStreetName(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.MATCH_STREET_NAME) == 0)) {  
                        
                    sAddress[i].setMatchStreetName(sRec[i][0].getValue(sFields[j]));
                    continue;
                                                          
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_DIRECTION) == 0)) {
                        
                    sAddress[i].setStreetNamePrefDir(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_DIRECTION) == 0)) {
                        
                    sAddress[i].setStreetNameSufDir(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_TYPE) == 0)) {
                        
                    sAddress[i].setStreetNamePrefType(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_TYPE) == 0)) {
                        
                    sAddress[i].setStreetNameSufType(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.SECOND_STREET_NAME_SUF_TYPE) == 0)) {

                    sAddress[i].setSecondStreetNameSufType(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_DESCRIPT) == 0)) {
                            
                    sAddress[i].setWithinStructDescript(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_IDENTIF) == 0)) {
                            
                    sAddress[i].setWithinStructIdentif(sRec[i][0].getValue(sFields[j]));
                    continue;    
                                    
                }  else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_DESCRIPT) == 0) { 
                    sAddress[i].setRuralRouteDescript(sRec[i][0].getValue(sFields[j]));
                    continue;       
                                 
                } else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_IDENTIF) == 0) {
                    sAddress[i].setRuralRouteIdentif(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandAddressesImpl.BOX_DESCRIPT) == 0) { 
                    sAddress[i].setBoxDescript(sRec[i][0].getValue(sFields[j]));
                    continue;   
                                     
                }  else if (sFields[j].compareTo(StandAddressesImpl.BOX_IDENTIF) == 0) { 
                    sAddress[i].setBoxIdentif(sRec[i][0].getValue(sFields[j]));
                    continue;          
                              
                } else if (sFields[j].compareTo(StandAddressesImpl.STATE) == 0) {
                    sAddress[i].setStateName(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandAddressesImpl.ZIP) == 0) { 
                    sAddress[i].setZipCode(sRec[i][0].getValue(sFields[j]));
                    continue;   
                                     
                }  else if (sFields[j].compareTo(StandAddressesImpl.CITY) == 0) { 
                    sAddress[i].setCityName(sRec[i][0].getValue(sFields[j]));
                    continue;                     
                                       
                } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_DIRECTION) == 0) { 
                   sAddress[i].setPropDesPrefDirection(sRec[i][0].getValue(sFields[j]));
                   continue;         
                
                } else if (sFields[j].compareTo(StandAddressesImpl.MATCH_PROPERTY_NAME) == 0) { 
                    sAddress[i].setMatchPropertyName(sRec[i][0].getValue(sFields[j]));
                    continue;          
                  
                } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_TYPE) == 0) {
                    sAddress[i].setPropDesPrefType(sRec[i][0].getValue(sFields[j]));
                    continue;
                            
                } else if (sFields[j].compareTo(StandAddressesImpl.ORIG_PROPERTY_NAME) == 0) { 
                    sAddress[i].setOrigPropertyName(sRec[i][0].getValue(sFields[j]));
                    continue;   
                         
                }  else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_TYPE) == 0) { 
                    sAddress[i].setPropertySufType(sRec[i][0].getValue(sFields[j]));
                    continue;      
                                  
                } else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_DIRECTION) == 0) { 
                    sAddress[i].setPropertySufDirection(sRec[i][0].getValue(sFields[j]));
                    continue;   
                         
                }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_DESCRIPT) == 0) { 
                    sAddress[i].setStructureDescript(sRec[i][0].getValue(sFields[j]));
                    continue;         
                }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_IDENTIF) == 0) { 
                    sAddress[i].setStructureIdentif(sRec[i][0].getValue(sFields[j]));
                    continue;      
                }                 
                
                
            }
        } 
        return sAddress;         
    }

    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec an array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandAddresses[] returnStandObject(SbmeStandRecord[] sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        int numberOfRecords = sRec.length;        
        StandAddressesImpl[] sAddress = new StandAddressesImpl[numberOfRecords];
        
        int numberOfFields;
        
        for (i = 0; i < numberOfRecords; i++) {            
        
            hs = sRec[i].getAllFields();
            numberOfFields = hs.size();
            sAddress[i] = new StandAddressesImpl();
            
            oFields = hs.toArray();
            sFields = new String[numberOfFields];
                
            for (j = 0; j < numberOfFields; j++) {
            
                sFields[j] = (String) oFields[j];
                
                if (sFields[j].compareTo(StandAddressesImpl.HOUSE_NUMBER) == 0) {
                    sAddress[i].setHouseNumber(sRec[i].getValue(sFields[j]));
                    continue;
                } else if (sFields[j].compareTo(StandAddressesImpl.SECOND_HOUSE_NUMBER) == 0) {
                    sAddress[i].setSecondHouseNumber(sRec[i].getValue(sFields[j]));
                    continue;
                    
                } else if ((sFields[j].compareTo(StandAddressesImpl.ORIG_STREET_NAME) == 0)) {
                    
                    sAddress[i].setOrigStreetName(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.ORIG_SECOND_STREET_NAME) == 0)) {
                    
                    sAddress[i].setOrigSecondStreetName(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                }else if ((sFields[j].compareTo(StandAddressesImpl.MATCH_STREET_NAME) == 0)) {  
                        
                    sAddress[i].setMatchStreetName(sRec[i].getValue(sFields[j]));
                    continue;
                                                          
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_DIRECTION) == 0)) {
                        
                    sAddress[i].setStreetNamePrefDir(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_DIRECTION) == 0)) {
                        
                    sAddress[i].setStreetNameSufDir(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_TYPE) == 0)) {
                        
                    sAddress[i].setStreetNamePrefType(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_TYPE) == 0)) {
                        
                    sAddress[i].setStreetNameSufType(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.SECOND_STREET_NAME_SUF_TYPE) == 0)) {
                        
                    sAddress[i].setSecondStreetNameSufType(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_DESCRIPT) == 0)) {
                            
                    sAddress[i].setWithinStructDescript(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if ((sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_IDENTIF) == 0)) {
                            
                    sAddress[i].setWithinStructIdentif(sRec[i].getValue(sFields[j]));
                    continue;    
                                    
                }  else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_DESCRIPT) == 0) { 
                    sAddress[i].setRuralRouteDescript(sRec[i].getValue(sFields[j]));
                    continue;       
                                 
                } else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_IDENTIF) == 0) {
                    sAddress[i].setRuralRouteIdentif(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandAddressesImpl.BOX_DESCRIPT) == 0) { 
                    sAddress[i].setBoxDescript(sRec[i].getValue(sFields[j]));
                    continue;   
                                     
                }  else if (sFields[j].compareTo(StandAddressesImpl.BOX_IDENTIF) == 0) { 
                    sAddress[i].setBoxIdentif(sRec[i].getValue(sFields[j]));
                    continue;          
                              
                } else if (sFields[j].compareTo(StandAddressesImpl.STATE) == 0) {
                    sAddress[i].setStateName(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandAddressesImpl.ZIP) == 0) { 
                    sAddress[i].setZipCode(sRec[i].getValue(sFields[j]));
                    continue;   
                                     
                }  else if (sFields[j].compareTo(StandAddressesImpl.CITY) == 0) { 
                    sAddress[i].setCityName(sRec[i].getValue(sFields[j]));
                    continue; 
                    
                } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_DIRECTION) == 0) { 
                   sAddress[i].setPropDesPrefDirection(sRec[i].getValue(sFields[j]));
                   continue;         
            
                } else if (sFields[j].compareTo(StandAddressesImpl.MATCH_PROPERTY_NAME) == 0) { 
                    sAddress[i].setMatchPropertyName(sRec[i].getValue(sFields[j]));
                    continue;          
              
                } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_TYPE) == 0) {
                    sAddress[i].setPropDesPrefType(sRec[i].getValue(sFields[j]));
                    continue;
                        
                } else if (sFields[j].compareTo(StandAddressesImpl.ORIG_PROPERTY_NAME) == 0) { 
                    sAddress[i].setOrigPropertyName(sRec[i].getValue(sFields[j]));
                    continue;   
                     
                }  else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_TYPE) == 0) { 
                    sAddress[i].setPropertySufType(sRec[i].getValue(sFields[j]));
                    continue;      
                              
                } else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_DIRECTION) == 0) { 
                    sAddress[i].setPropertySufDirection(sRec[i].getValue(sFields[j]));
                    continue;   
                     
                }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_DESCRIPT) == 0) { 
                    sAddress[i].setStructureDescript(sRec[i].getValue(sFields[j]));
                    continue;         
                }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_IDENTIF) == 0) { 
                    sAddress[i].setStructureIdentif(sRec[i].getValue(sFields[j]));
                    continue;      
                }                 
            }
        }
        return sAddress;         
    }




    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec a SbmeStandRecord object (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandAddresses returnStandObject(SbmeStandRecord sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
       
        StandAddressesImpl sAddress = new StandAddressesImpl();
        
        int numberOfFields;
             
        
        hs = sRec.getAllFields();
        numberOfFields = hs.size();

        
        oFields = hs.toArray();
        sFields = new String[numberOfFields];
            
        for (j = 0; j < numberOfFields; j++) {
        
            sFields[j] = (String) oFields[j];
            

            if (sFields[j].compareTo(StandAddressesImpl.HOUSE_NUMBER) == 0) {
                sAddress.setHouseNumber(sRec.getValue(sFields[j]));
                continue;
            } else if (sFields[j].compareTo(StandAddressesImpl.SECOND_HOUSE_NUMBER) == 0) {
                sAddress.setSecondHouseNumber(sRec.getValue(sFields[j]));
                continue;
                    
            } else if ((sFields[j].compareTo(StandAddressesImpl.ORIG_STREET_NAME) == 0)) {
                    
                sAddress.setOrigStreetName(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.ORIG_SECOND_STREET_NAME) == 0)) {
                    
                sAddress.setOrigSecondStreetName(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.MATCH_STREET_NAME) == 0)) {  
                        
                sAddress.setMatchStreetName(sRec.getValue(sFields[j]));
                continue;
                                                          
            } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_DIRECTION) == 0)) {
                        
                sAddress.setStreetNamePrefDir(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_DIRECTION) == 0)) {
                        
                sAddress.setStreetNameSufDir(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_TYPE) == 0)) {
                        
                sAddress.setStreetNamePrefType(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_TYPE) == 0)) {
                        
                sAddress.setStreetNameSufType(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.SECOND_STREET_NAME_SUF_TYPE) == 0)) {
                        
                sAddress.setSecondStreetNameSufType(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_DESCRIPT) == 0)) {
                            
                sAddress.setWithinStructDescript(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if ((sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_IDENTIF) == 0)) {
                            
                sAddress.setWithinStructIdentif(sRec.getValue(sFields[j]));
                continue;    
                                    
            }  else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_DESCRIPT) == 0) { 
                sAddress.setRuralRouteDescript(sRec.getValue(sFields[j]));
                continue;       
                                 
            } else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_IDENTIF) == 0) {
                sAddress.setRuralRouteIdentif(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if (sFields[j].compareTo(StandAddressesImpl.BOX_DESCRIPT) == 0) { 
                sAddress.setBoxDescript(sRec.getValue(sFields[j]));
                continue;   
                                     
            }  else if (sFields[j].compareTo(StandAddressesImpl.BOX_IDENTIF) == 0) { 
                sAddress.setBoxIdentif(sRec.getValue(sFields[j]));
                continue;          
                              
            } else if (sFields[j].compareTo(StandAddressesImpl.STATE) == 0) {
                sAddress.setStateName(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if (sFields[j].compareTo(StandAddressesImpl.ZIP) == 0) { 
                sAddress.setZipCode(sRec.getValue(sFields[j]));
                continue;   
                                     
            }  else if (sFields[j].compareTo(StandAddressesImpl.CITY) == 0) { 
                sAddress.setCityName(sRec.getValue(sFields[j]));
                continue;   
                
            } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_DIRECTION) == 0) { 
               sAddress.setPropDesPrefDirection(sRec.getValue(sFields[j]));
               continue;         
            
            } else if (sFields[j].compareTo(StandAddressesImpl.MATCH_PROPERTY_NAME) == 0) { 
                sAddress.setMatchPropertyName(sRec.getValue(sFields[j]));
                continue;          
              
            } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_TYPE) == 0) {
                sAddress.setPropDesPrefType(sRec.getValue(sFields[j]));
                continue;
                        
            } else if (sFields[j].compareTo(StandAddressesImpl.ORIG_PROPERTY_NAME) == 0) { 
                sAddress.setOrigPropertyName(sRec.getValue(sFields[j]));
                continue;   
                     
            }  else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_TYPE) == 0) { 
                sAddress.setPropertySufType(sRec.getValue(sFields[j]));
                continue;      
                              
            } else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_DIRECTION) == 0) { 
                sAddress.setPropertySufDirection(sRec.getValue(sFields[j]));
                continue;   
                     
            }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_DESCRIPT) == 0) { 
                sAddress.setStructureDescript(sRec.getValue(sFields[j]));
                continue;         
            }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_IDENTIF) == 0) { 
                sAddress.setStructureIdentif(sRec.getValue(sFields[j]));
                continue;      
            }                 
            
        }
        return sAddress;         
    }
    

    /**
     * Input the data from the StandPersonNames into the SbmeStandRecord 
     * standardization object
     * @param sRec an array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static SbmeStandRecord inputStandObject(StandAddresses sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        
        SbmeStandRecord sAddress = new SbmeAddress();
        
        int numberOfFields;
             
        
        hs = sRec.getAllFields();
        numberOfFields = hs.size();
        
        oFields = hs.toArray();
        sFields = new String[numberOfFields];
            
        for (j = 0; j < numberOfFields; j++) {
        
            sFields[j] = (String) oFields[j];
            
            if (sFields[j].compareTo(StandAddressesImpl.HOUSE_NUMBER) == 0) {
                
                sAddress.setValue(sFields[j],sRec.getHouseNumber());
                continue;
            } else if (sFields[j].compareTo(StandAddressesImpl.SECOND_HOUSE_NUMBER) == 0) {
                sAddress.setValue(sFields[j],sRec.getSecondHouseNumber());
                continue; 
                    
            } else if (sFields[j].compareTo(StandAddressesImpl.ORIG_STREET_NAME) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getOrigStreetName());
                continue;
         
            } else if (sFields[j].compareTo(StandAddressesImpl.ORIG_SECOND_STREET_NAME) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getOrigSecondStreetName());
                continue;
         
            } else if (sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_DIRECTION) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getStreetNamePrefDir());
                continue;
                
            } else if (sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_DIRECTION) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getStreetNameSufDir());
                continue;
         
            } else if (sFields[j].compareTo(StandAddressesImpl.STREET_NAME_PREF_TYPE) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getStreetNamePrefType());
                continue;

            } else if (sFields[j].compareTo(StandAddressesImpl.STREET_NAME_SUF_TYPE) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getStreetNameSufType());
                continue;
         
            } else if (sFields[j].compareTo(StandAddressesImpl.SECOND_STREET_NAME_SUF_TYPE) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getSecondStreetNameSufType());
                continue;
         
            } else if (sFields[j].compareTo(StandAddressesImpl.WITHIN_STRUCT_DESCRIPT) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getWithinStructDescript());
                continue;
 
                
            } else if (sFields[j].compareTo(StandAddressesImpl.RURAL_ROUTE_DESCRIPT) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getRuralRouteDescript());
                continue;

            } else if (sFields[j].compareTo(StandAddressesImpl.BOX_DESCRIPT) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getBoxDescript());
                continue;
         
            } else if (sFields[j].compareTo(StandAddressesImpl.STATE) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getStateName());
                continue;                
                
            } else if (sFields[j].compareTo(StandAddressesImpl.CITY) == 0) {
                    
                sAddress.setValue(sFields[j],sRec.getCityName());
                continue;   
                      
            } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_DIRECTION) == 0) { 
               sAddress.setValue(sFields[j],sRec.getPropDesPrefDirection());
               continue;         
                
            } else if (sFields[j].compareTo(StandAddressesImpl.MATCH_PROPERTY_NAME) == 0) { 
                sAddress.setValue(sFields[j],sRec.getMatchPropertyName());
                continue;          
                  
            } else if (sFields[j].compareTo(StandAddressesImpl.PROP_DES_PREF_TYPE) == 0) {
                sAddress.setValue(sFields[j],sRec.getPropDesPrefType());
                continue;
                            
            } else if (sFields[j].compareTo(StandAddressesImpl.ORIG_PROPERTY_NAME) == 0) { 
                sAddress.setValue(sFields[j],sRec.getOrigPropertyName());
                continue;   
                         
            }  else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_TYPE) == 0) { 
                sAddress.setValue(sFields[j],sRec.getPropertySufType());
                continue;      
                                  
            } else if (sFields[j].compareTo(StandAddressesImpl.PROPERTY_SUF_DIRECTION) == 0) { 
                sAddress.setValue(sFields[j],sRec.getPropertySufDirection());
                continue;   
                         
            }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_DESCRIPT) == 0) { 
                sAddress.setValue(sFields[j],sRec.getStructureDescript());
                continue;         
            }  else if (sFields[j].compareTo(StandAddressesImpl.STRUCTURE_IDENTIF) == 0) { 
                sAddress.setValue(sFields[j],sRec.getWithinStructIdentif());
                continue;      
            }                                                  
        }     
        return sAddress;         
    }       
}