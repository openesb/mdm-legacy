/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;

import java.io.InputStream;

import com.stc.sbme.api.SbmeConfigFilesAccess;
import com.stc.sbme.api.SbmeConfigurationException;
import com.stc.sbmeapi.StandConfigFilesAccess;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class StandConfigFilesAccessImpl
    implements StandConfigFilesAccess {

    private final SbmeConfigFilesAccess mFilesAccess;

    //Use class loader to get resource
    public StandConfigFilesAccessImpl() {
        mFilesAccess = null;
    }
    
    public StandConfigFilesAccessImpl(SbmeConfigFilesAccess filesAccess) {
        mFilesAccess = filesAccess;
    }
    
    private InputStream getResource(String resource, String domain, String suffix)
    throws SbmeConfigurationException {
        InputStream stream = null;
        //First, try to get domain specific file
        stream = getFullyQualifiedResource(resource + domain + suffix);
        //If domain specific file not found, get non-domain file
        if (stream == null) {
            stream = getFullyQualifiedResource(resource + suffix);
        }
        if (stream == null) {
            throw new SbmeConfigurationException("Resource: " + resource + 
            " not found for domain: " + domain);
        }
        return stream;
    }
    
    private InputStream getFullyQualifiedResource(String resource)
    throws SbmeConfigurationException {
        InputStream stream = null;
        if (mFilesAccess == null) {          
            stream = this.getClass().getResourceAsStream(resource);
        } else {             
            stream = mFilesAccess.getConfigFileAsStream(resource);
        }
        if (stream == null) {
            if (mFilesAccess == null) {
                stream = this.getClass().getResourceAsStream("/" + resource);
            } else {
                stream = mFilesAccess.getConfigFileAsStream("/" + resource);
            }
        }
        
        return stream;
    }
    
    /**
     * Provides a stream from the person first name file
     *
     * @return the corresponding stream for first name
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonFirstNameFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("personFirstName",domain,".dat");
    }
    /**
     * Provides a stream from the person last name file
     *
     * @return the corresponding stream for last name
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonLastNameFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("personLastName",domain,".dat");
    }
        
    /**
     * Provides a stream from the person conjonctions file
     *
     * @return a stream from the person conjonctions file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonConjonctionFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personConjon", domain, ".dat");

    }        
        
        
    /**
     * Provides a stream from the person generational suffixes file
     *
     * @return a stream from the person generational suffixes file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonGenSuffixFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personGenSuffix", domain, ".dat");
    }
 
        
    /**
     * Provides a stream from the person name patterns file
     *
     * @return a stream from the person name patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonNamePatternsFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personNamePatt", domain, ".dat");
    }
        
        
    /**
     * Provides a stream from the person last name prefix file
     *
     * @return a stream from the person last name prefix file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonLastNamePrefixFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personLastNamePrefix", domain, ".dat");
    }
        
        
    /**
     * Provides a stream of the person occupational suffixes file
     *
     * @return a stream of the person occupational suffixes file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonOccupSuffixFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personOccupSuffix", domain, ".dat");
    }
 
        
    /**
     * Provides a stream of the person titles file
     *
     * @return  a stream of the person titles file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonTitleFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personTitle", domain, ".dat");
    }

        
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonConstantsFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personConstants", domain, ".cfg");
    }

    /**
     *
     * @return a stream of the person dashed first names file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideFirstNameDashFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personFirstNameDash", domain, ".dat");
    }
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBusinessOrRelatedFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("businessOrRelated", domain, ".dat");
    }
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonSpecCharsFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personRemoveSpecChars", domain, ".dat");
    }
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonThreeFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personThree", domain, ".dat");
    }
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream providePersonTwoFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("personTwo", domain, ".dat");
    }
 
        
    /**
     * Provides a stream of the address clues abbreviations file
     *
     * @return a stream of the address clues abbreviations file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideAddressClueAbbrevFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("addressClueAbbrev", domain, ".dat");
    }

    /**
     * Provides a stream of the address master clues file
     *
     * @return a stream of the address master clues file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideAddressMasterCluesFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("addressMasterClues", domain, ".dat");
}

    /**
     * Provides a stream of the address patterns file
     *
     * @return a stream of the address patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideAddressPatternsFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("addressPatterns", domain, ".dat");
    }

    /**
     * Provides a stream of the address patterns file
     *
     * @return a stream of the address patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideAddressOutputPatternsFile(String domain)
        throws SbmeConfigurationException {
        
            return getResource("addressOutPatterns", domain, ".dat");
    }    

    /**
     * Provides a stream of the address config file
     *
     * @return a stream of the address config file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideAddressConstantsFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("addressConstants", domain, ".cfg");
    }


    /**
     * Provides a stream of the address config file
     *
     * @return a stream of the address config file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideAddressInternalConstantsFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("addressInternalConstants", domain, ".cfg");
    }


    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBizConstantsFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizConstants", domain, ".cfg");
    }
    

    /**
     * Provides a stream of the business adjectives file
     *
     * @return a stream of the business adjectives file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBussinessAdjectivesFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizAdjectivesTypeKeys", domain, ".dat");
    }

    /**
     * Provides a stream of the business aliases file
     *
     * @return a stream of the business aliases file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBussinessAliasFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizAliasTypeKeys", domain, ".dat");
    }

    
    /**
     * Provides a stream of the business associattion type keys file
     *
     * @return a stream of the business associattion keys file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBussinessAssociatitionFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizAssociationTypeKeys", domain, ".dat");
    }
    
    
    /**
     * Provides a stream of the business general terms file
     *
     * @return a stream of the business general terms file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBussinessGenTermsFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizBusinessGeneralTerms", domain, ".dat");
    }
    
    
    /**
     * Provides a stream of the city or state keys file
     *
     * @return a stream of the city or state keys file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideCityStateTypeFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizCityOrStateTypeKeys", domain, ".dat");
    }
    
    
    /**
     * Provides a stream of the business former names file
     *
     * @return a stream of the business former names file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBussinessFormerNameFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizCompanyFormerNames", domain, ".dat");
    }
    
    /**
     * Provides a stream of the business merger names file
     *
     * @return a stream of the business merger names file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideCompanyMergerNameFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizCompanyMergerNames", domain, ".dat");
    }
    
    
    /**
     * Provides a stream of the company prymary names file
     *
     * @return a stream of the company primary names file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideCompanyPrimaryNameFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizCompanyPrimaryNames", domain, ".dat");
    }
    
    
    /**
     * Provides a stream of the business connector tokens file
     *
     * @return a stream of the business connector tokens file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBussinessConnectorTokensFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizConnectorTokens", domain, ".dat");
    }                            

    /**
     * Provides a stream of the country type keys file
     *
     * @return a stream of the country type keys file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideCountryTypeKeysFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizCountryTypeKeys", domain, ".dat");
    }                            


    /**
     * Provides a stream of the industry categories file
     *
     * @return a stream of the industry categories file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideIndustryCategoriesFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizIndustryCategoriesCode", domain, ".dat");
    }                            


    /**
     * Provides a stream of the business connector tokens file
     *
     * @return a stream of the business connector tokens file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideIndustryTypeKeysFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizIndustryTypeKeys", domain, ".dat");
    }                            


    /**
     * Provides a stream of the Organization Type Keys file
     *
     * @return a stream of the Organization type keys file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideOrganizationTypeKeysFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizOrganizationTypeKeys", domain, ".dat");
    }                            

    /**
     * Provides a stream of the business patterns file
     *
     * @return a stream of the business patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideBusinessPatternsFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizPatterns", domain, ".dat");
    }                      
    
    
    /**
     * Provides a stream of the special characters to remove file
     *
     * @return a stream of the special characters to remove file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideSpecCharsFile(String domain)
        throws SbmeConfigurationException {
        
        return getResource("bizRemoveSpecChars", domain, ".dat");
    }    
    
}
