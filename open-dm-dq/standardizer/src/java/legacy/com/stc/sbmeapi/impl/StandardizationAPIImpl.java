/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;

import java.io.IOException;

import com.stc.sbme.api.SbmeConfigurationException;
import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandardizationEngine;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbmeapi.StandAddresses;
import com.stc.sbmeapi.StandBusinessNames;
import com.stc.sbmeapi.StandConfigFilesAccess;
import com.stc.sbmeapi.StandPersonNames;
import com.stc.sbmeapi.StandardizationAPI;

/**
 *
 */
public class StandardizationAPIImpl implements StandardizationAPI {


    // Define an StandardizationEngineWrapper object
    SbmeStandardizationEngine sbme;
    
    // Define an array of SbmeStandRecord objects
    SbmeStandRecord sRec;
	SbmeStandRecord[] sRec1;
    SbmeStandRecord[][] sRec2;
    	
    // Define a string that hold the location variable
    String standLoc;
    
    /**
     * Initialize the standardization engine and and read all the data files
     * @param stream a stream from the data files
     * @param domain geographic location of the compared data
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public void initialize(StandConfigFilesAccess stream)
            throws SbmeStandardizationException, SbmeConfigurationException,
                SbmeMatchEngineException {
        

        // Reads the local-specification of the files
        if (stream == null) {
            throw new SbmeStandardizationException("Failed to retrieve the configuration files"
                + "from the configuration service.");
        }

        // Read the chosen location if different from the default
        try {
            
            // Instantiate the main class for standardization
            sbme = new SbmeStandardizationEngine(stream);
                       
        } catch (IOException e) {
			throw new SbmeStandardizationException("Could not initialize the eView" 
			    + " standardization engine");
        }
    }
    

    /**
     * @param stringValue is a 'PersonName' input string.
     * @param domain geographic location of the compared data
     * @return an array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person, address or business
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public StandPersonNames[] standardizePersonName(String stringValue, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        // Call the engine to standardize the string
        try {               
            sRec1 = sbme.standardize("PersonName", stringValue, domain);
        } catch (IOException e){
            throw new SbmeStandardizationException("");
        }
        
        // Transfer the returned data to a StandPersonNames object
        return PopulatePersonName.returnStandObject(sRec1);
    }
            

    /**
     * @param stringValue is an array of 'PersonName' input strings.
     * @param domain geographic location of the compared data
     * @return a 2D array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person, address or business
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public StandPersonNames[][] standardizePersonName(String[] stringValues, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException {

        int i;   
        int size = stringValues.length;
        StandPersonNames[][] pRec = null;
        
        String[] sType = new String[size];
        
        for (i = 0; i < size; i++) {
            sType[i] = "PersonName"; 
        }
        
        // Call the engine to standardize the string
        try {               
            sRec2 = sbme.standardize(sType, stringValues, domain);
        } catch (IOException e){
            throw new SbmeStandardizationException("");
        }
               
        // Transfer the returned data to a StandPersonNames object
        for (i = 0; i < sRec2.length; i++) {            
            pRec[i] = PopulatePersonName.returnStandObject(sRec2[i]);
        }       
        return pRec;
    }        
        

	/**
	 * @param personObj an StandPersonNames object
	 * @param domain geographic location of the compared data
	 * @return the same object after normalizing the data
	 * @throws SbmeStandardizationException; for any type of normalization failure
	 */        
	public StandPersonNames normalizePersonName(StandPersonNames personObj, String domain)
		throws SbmeStandardizationException {

        // Transfer the input data to a SbmeStandRecord object
        sRec = PopulatePersonName.inputStandObject(personObj);
        
        // Call the engine to standardize the string            
        sRec = sbme.normalize(sRec, domain);
    
        // Transfer the returned data to a StandPersonNames object
        return PopulatePersonName.returnStandObject(sRec);
      			
	}

	/**
	 * @param personObjs an array of StandPersonNames objects
	 * @param domain geographic location of the compared data
	 * @return the input array of the same objects after normalization
	 * @throws SbmeStandardizationException; for any type of normalization failure
	 */        
	public StandPersonNames[] normalizePersonName(StandPersonNames[] personObjs, String domain)
		throws SbmeStandardizationException {
            
        int i;   
        int size = personObjs.length;
        StandPersonNames[][] pRec;
        sRec1 = new SbmeStandRecord[size];
        
        for (i = 0; i < size; i++) {              
            sRec1[i] = PopulatePersonName.inputStandObject(personObjs[i]);
        }       
    
        // Call the engine to standardize the string            
        sRec1 = sbme.normalize(sRec1, domain);

        // Transfer the returned data to a StandPersonNames object
        return PopulatePersonName.returnStandObject(sRec1);            
    }
		

    /**
     * @param typeName can be either 'PersonName', 'Address' or 'BusinessName'.
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return a 2D array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person, address or business
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public StandAddresses standardizeAddress(String stringValue, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException {


        // Call the engine to standardize the string
        try {               
            sRec1 = sbme.standardize("Address", stringValue, domain);
        } catch (IOException e){
            throw new SbmeStandardizationException("");
        }

        // Transfer the returned data to a StandPersonNames object
        return (PopulateAddress.returnStandObject(sRec1))[0];   
    }
            

    /**
     * @param typeName can be either 'PersonName', 'Address' or 'BusinessName'.
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return a 2D array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person, address or business
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public StandAddresses[] standardizeAddress(String[] stringValues, String domain)
            throws SbmeStandardizationException, SbmeMatchEngineException {
        
        int i;   
        int size = stringValues.length;
        StandAddresses[] pRec;

        String[] sType = new String[size];

        for (i = 0; i < size; i++) {
            sType[i] = "Address"; 
        }

        // Call the engine to standardize the string
        try {               
            sRec2 = sbme.standardize(sType, stringValues, domain);
        } catch (IOException e){
            throw new SbmeStandardizationException("");
        }
       
        // Transfer the returned data to a StandPersonNames object      
        pRec = PopulateAddress.returnStandObject(sRec2);
        
        return pRec;           
    }

    /**
     * @param addressObj a StandPersonNames object
     * @param domain geographic location of the compared data
     * @return the same object after normalizing the data
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    public StandAddresses normalizeAddress(StandAddresses addressObj, String domain)
            throws SbmeStandardizationException {
                
        return null;         
    }

    /**
     * @param addressObjs an array of address objects
     * @param domain geographic location of the compared data
     * @return the input array of the same objects after normalization
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    public StandAddresses[] normalizeAddress(StandAddresses[] addressObjs, String domain)
            throws SbmeStandardizationException {
        return null;             
    }
    
            
    /**
     * @param typeName can be either 'PersonName', 'Address' or 'BusinessName'.
     * @param value the string to parse
     * @param domain geographic location of the compared data
     * @return a 2D array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person, address or business
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public StandBusinessNames standardizeBusinessName(String stringValue)
            throws SbmeStandardizationException, SbmeMatchEngineException {
            
        // Call the engine to standardize the string
        try {               
            sRec1 = sbme.standardize("BusinessName", stringValue, standLoc);
            
        } catch (IOException e){
            throw new SbmeStandardizationException("");
        }

        // Transfer the returned data to a StandPersonNames object
        return (PopulateBusinessName.returnStandObject(sRec1))[0];   
    }
 


    /**
     * @param typeName can be either 'PersonName', 'Address' or 'BusinessName'.
     * @param value the array of strings to parse
     * @param domain geographic location of the compared data
     * @return a 2D array of standardized objects (the extra dimension handles cases
     *         where one string has more than one person, address or business
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public StandBusinessNames[] standardizeBusinessName(String[] stringValues)
            throws SbmeStandardizationException, SbmeMatchEngineException {
        
        int i;   
        int size = stringValues.length;
        StandBusinessNames[] pRec;

        String[] sType = new String[size];

        for (i = 0; i < size; i++) {
            sType[i] = "BusinessName"; 
        }

        // Call the engine to standardize the string
        try {               
            sRec2 = sbme.standardize(sType, stringValues, standLoc);
            
        } catch (IOException e){
            throw new SbmeStandardizationException("");
        }
   
        // Transfer the returned data to a StandPersonNames object      
        pRec = PopulateBusinessName.returnStandObject(sRec2);

        return pRec;                     
    }
            
    /**
     * @param addressObj a StandPersonNames object
     * @param domain geographic location of the compared data
     * @return the same object after normalizing the data
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    public StandBusinessNames normalizeBusinessName(StandBusinessNames addressObj)
            throws SbmeStandardizationException {
                
        return null;         
    }

    /**
     * @param addressObjs an array of address objects
     * @param domain geographic location of the compared data
     * @return the input array of the same objects after normalization
     * @throws SbmeStandardizationException; for any type of normalization failure
     */        
    public StandBusinessNames[] normalizeBusinessName(StandBusinessNames[] addressObjs)
            throws SbmeStandardizationException {
        return null;             
    }
 
   
    /**
     * Shutdown and release any resources associated with the standardization engine
     * @throws SbmeStandardizationException if the shutdown failed
     */            
    public void shutdown() 
        throws SbmeStandardizationException {
        
        // Handle correctly the end of the process
        if (sbme != null) {
            try {
                sbme.shutdown();
            } catch (Exception ex) {
                throw new SbmeStandardizationException(
                    "Failed to shutdown match engine: " + ex.getMessage(), ex);            
            } finally {
                sbme = null;
            }
        }
    }                   
}

