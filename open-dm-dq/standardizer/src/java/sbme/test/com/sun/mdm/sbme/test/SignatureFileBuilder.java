/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignatureFileBuilder extends FileBuilder {
    Map<String, RecordData> signatures;

    @Override
    public void begin() throws Exception {
        this.signatures = new HashMap<String, RecordData>();
    }

    @Override
    public void processRecord(final int recordNumber, final Map<String, String> standardizedFields, final Object parsedRecord, final String signature) throws Exception {
        final RecordData newRecord = new RecordData((String) parsedRecord, signature);
        if (this.signatures.containsKey(signature)) {
            final RecordData previousRecord = this.signatures.get(signature);
            if (newRecord.isRicher(previousRecord)) {
                this.signatures.put(signature, newRecord);
            }
        } else {
            this.signatures.put(signature, newRecord);
        }
    }

    @Override
    public void end() throws Exception {
        final List<RecordData> records = new ArrayList<RecordData>(this.signatures.values());
        Collections.sort(records);

        for (final RecordData recordData : records) {
            this.out.println(recordData.record + "\t" + recordData.signature);
        }
    }

    public static void main(final String[] args) throws Exception {
        final FileBuildingFramework builder = new FileBuildingFramework(args, new SignatureFileBuilder());
        builder.buildFiles();
    }
    
    public void buildSignatureFiles(String standardizerClassName,
                                    String variantCodes,
                                    String inputFilePrefix,
                                    String outputFilePrefix,
                                    String errorFilePrefix,
                                    boolean force) throws Exception {
        final FileBuildingFramework builder = new FileBuildingFramework(standardizerClassName,
                                                                        variantCodes,
                                                                        inputFilePrefix,
                                                                        outputFilePrefix,
                                                                        errorFilePrefix,
                                                                        force,
                                                                        new SignatureFileBuilder());       
        builder.buildFiles();
    }

    private static class RecordData implements Comparable<RecordData> {
        private final String record;
        private final String signature;
        private final int tokenCount;

        private RecordData(final String record, final String signature) {
            this.record = record;
            this.signature = signature;
            this.tokenCount = record.split("[\\s|]").length;
        }

        private boolean isRicher(final RecordData other) {
            if (this.tokenCount > other.tokenCount) {
                return true;
            }
            if (this.tokenCount == other.tokenCount) {
                return this.record.length() > other.record.length();
            }
            return false;
        }

        public int compareTo(final RecordData other) {
            return this.record.compareTo(other.record);
        }
    }
}
