package com.sun.mdm.sbme.test;

import java.io.File;
import java.util.jar.JarFile;

import com.sun.mdm.sbme.DataTypeDescriptor;
import com.sun.mdm.sbme.StandardizationEngine;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.VariantDescriptor;
import com.sun.mdm.sbme.engine.DefaultStandardizationEngine;
import com.sun.mdm.sbme.util.StandardizerUtils;

public class DefaultStandardizationEngineTester {
	public static void main(String[] args) throws Exception {
		StandardizationEngine engine = new DefaultStandardizationEngine();
		File repositoryDirectory = new File(System.getProperty("java.io.tmpdir") + File.separator + System.currentTimeMillis());
		repositoryDirectory.mkdirs();
		
		try {
			engine.initialize(repositoryDirectory);
			engine.start();
			engine.deploy(new JarFile("dist/sbme-address-deployment.zip"));
			engine.deploy(new JarFile("dist/sbme-fr-address-deployment.zip"));
			engine.deploy(new JarFile("dist/sbme-businessname-deployment.zip"));
			engine.deploy(new JarFile("dist/mdm-personname-deployment.zip"));
			
			StandardizedRecord record = null;
			
			Standardizer usStandardizer = engine.getStandardizer("Address", "US");
			record = usStandardizer.standardize("1001 LINCOLN STREET B 202");
			System.out.println(record);
			record = usStandardizer.standardize("1010 OLD HIGHWAY 12 MILE RD");
			System.out.println(record);
			
			Standardizer frStandardizer = engine.getStandardizer("Address", "FR");
			record = frStandardizer.standardize("1 rue Jean Jaures, BP 27");
			System.out.println(record);
			record = frStandardizer.standardize("10 r du vieux march� aux vins");
			System.out.println(record);
			
			Standardizer genericBusinessStandardizer = engine.getStandardizer("BusinessName", "Generic");
			record = genericBusinessStandardizer.standardize("AETEA  Information Technology, Inc.");
			System.out.println(record);
			
			record = genericBusinessStandardizer.standardize("SSTAR OF RHODE ISLAND INC");
			System.out.println(record);
			
			for (String dataTypeName: engine.dataTypeNames()) {
				System.err.println("*** Data type name: "+ dataTypeName);
				DataTypeDescriptor dataTypeDescriptor = engine.getDataTypeDescriptor(dataTypeName);
				for (VariantDescriptor variantDescriptor: dataTypeDescriptor.variants()) {
					System.err.println("    **** Variant name: " + variantDescriptor.getName());
				}
			}
			
			Standardizer genericPersonStandardizer = engine.getStandardizer("PersonName", "Generic");
			record = genericPersonStandardizer.standardize("Ricardo Rocha, Esq.");
			System.out.println(record);
			
			engine.undeployDataType("Address");
			engine.undeployDataType("BusinessName");
			engine.undeployDataType("PersonName");
			
			engine.stop();
		} finally {
			StandardizerUtils.delete(repositoryDirectory);
		}
	}
}
