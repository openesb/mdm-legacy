package com.sun.mdm.sbme.test.businessname;

import java.io.PrintWriter;
import java.io.StringWriter;

import junit.framework.TestCase;
import bmsi.util.DiffPrint;

import com.sun.mdm.sbme.test.SignatureFileBuilder;
import com.sun.mdm.sbme.test.TestFileBuilder;

public class BusinessNameTest extends TestCase {

//    @Before
    public void setUp() throws Exception {
        
        SignatureFileBuilder signatureBuilder = new SignatureFileBuilder();
        signatureBuilder.buildSignatureFiles("com.sun.mdm.sbme.test.MdmBusinessNameStandardizerProxy",
                "Generic",
                "input",
                "signature-mdm",
                "signature-mdm-err",
                false);
        
        TestFileBuilder testBuilder = new TestFileBuilder();
        testBuilder.buildFiles("com.sun.mdm.sbme.test.MdmBusinessNameStandardizerProxy",
                "Generic",
                "signature-mdm",
                "test-mdm",
                "signature-mdm-err",
                false);

    }
    
//    @Test
    public void testDiffSignatureFiles() throws Exception {
        System.out.println("testDiffSignatureFiles()");
        String[] myStrings = new String[3];
        myStrings[0] = "-c";
        myStrings[1] = "test-sbme-generic.txt";
        myStrings[2] = "test-mdm-generic.txt";
                    
        StringWriter myStringWriter = new StringWriter();
        PrintWriter  myWriter       = new PrintWriter(myStringWriter);
        DiffPrint    myDiffPrint    = new DiffPrint();
        myDiffPrint.doWork(myStrings, myWriter);

        myWriter.flush();
        myWriter.close();
        String myDiff = myStringWriter.toString();

        if (myDiff.length() > 0) {
            System.out.println("Differences found: " + myDiff);
        }
        assertTrue(myDiff.length() == 0);
    }
}
