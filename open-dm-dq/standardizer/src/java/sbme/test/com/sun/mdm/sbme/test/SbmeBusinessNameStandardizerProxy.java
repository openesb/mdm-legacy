/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.stc.sbme.api.SbmeBusinessName;
import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationEngine;
import com.stc.sbmeapi.StandConfigFilesAccess;
import com.stc.sbmeapi.impl.StandConfigFilesAccessImpl;

public class SbmeBusinessNameStandardizerProxy implements StandardizerProxy {
    private final SbmeStandardizationEngine engine;

    public SbmeBusinessNameStandardizerProxy() throws TestException, Exception {
        final StandConfigFilesAccess standConfigFilesAccess = new StandConfigFilesAccessImpl();
        this.engine = new SbmeStandardizationEngine(standConfigFilesAccess);
    }

    public Object standardize(final String record, final String domain) throws Exception {
        try {
            return this.engine.standardize("BusinessName", record, domain.toUpperCase())[0];
        } catch (final SbmeMatchEngineException sbme) {
            throw new TestException(sbme.getMessage(), sbme);
        } catch (final Exception e) {
            throw e;
        }
    }

    public String getSignature(final Object source) {
    	// FIXME Removing test support for sbme
        return "";//((SbmeBusinessName) source).getSignature().trim();
    }

    public Map<String, String> getFields(final Object source) {
        final SbmeBusinessName businessName = (SbmeBusinessName) source;
        final Map<String, String> fields = new HashMap<String, String>();
        @SuppressWarnings("unchecked")
        Set<String> allFields = businessName.getAllFields();
        for (final String name : allFields) {
            fields.put(name, businessName.getValue(name));
        }
        return fields;
    }
}
