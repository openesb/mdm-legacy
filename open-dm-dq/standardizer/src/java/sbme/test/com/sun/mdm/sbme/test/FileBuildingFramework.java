/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <p>
 * Framework for building standarization test files. This class is configured
 * with instances of:
 * </p>
 * <ul>
 * <li> <code>StandardizerProxy</code> which provides a uniform interface to
 * the Sbme and Mdm versions of standardizer components. Mdm is a
 * refactored version of the various legacy Sbme standardizers. </li>
 * <li> <code>FileBuilder</code> which processes input directoryFile records as they
 * are read by the framework. </li>
 * </ul>
 * <p>
 * This class uses input, output and error directoryFile prefixes to build directoryFile names
 * whose suffixes are drawn from each variant.
 * </p>
 * 
 * @author Ricardo Rocha
 * 
 * See {@link StandardizerProxy} See {@link FileBuilder}
 */
public class FileBuildingFramework {
    private static final Logger logger = Logger.getLogger(FileBuildingFramework.class.getName());

    private final StandardizerProxy standardizer;
    private final FileBuilder builder;
    private final String[] variants;
    private final String inputFilePrefix;
    private final String outputFilePrefix;
    private final String errorFilePrefix;
    private boolean force = false;

    public FileBuildingFramework(final String[] args, final FileBuilder builder) throws Exception {
        this(args[0], args[1], args[2], args[3], args[4], false, builder);
        if (args.length > 5) {
            this.force = Boolean.parseBoolean(args[5]);
            if (this.force) {
                logger.info("Forcing test directoryFile building...");
            }
        }
    }
    
    public FileBuildingFramework(String standardizerClassName,
                                 String variantCodes,
                                 String inputFilePrefix,
                                 String outputFilePrefix,
                                 String errorFilePrefix,
                                 boolean force,
                                 final FileBuilder builder) throws Exception {
        this.builder = builder;
        this.standardizer = (StandardizerProxy) Class.forName(standardizerClassName).newInstance();
        this.variants = variantCodes.split("\\s*,\\s*");
        for (int i = 0; i < this.variants.length; i++) {
            this.variants[i] = this.variants[i].trim().toLowerCase();
        }
        this.inputFilePrefix = inputFilePrefix;
        this.outputFilePrefix = outputFilePrefix;
        this.errorFilePrefix = errorFilePrefix;
        this.force = force;
    }

    public void buildFiles() throws Exception {    
        // Fail fast
        for (final String variant : this.variants) {
            final File inputFile = getFile(this.inputFilePrefix, variant);
            if (!inputFile.canRead()) {
                throw new IllegalArgumentException("Can't read: " + inputFile);
            }
        }

        for (final String variant : this.variants) {
            final File inputFile = getFile(this.inputFilePrefix, variant);
            final File outputFile = getFile(this.outputFilePrefix, variant);

            if (this.force || inputFile.lastModified() > outputFile.lastModified()) {
                logger.info("Variant: " + variant);
                final File errorFile = getFile(this.errorFilePrefix, variant);

                final BufferedReader in = new BufferedReader(new FileReader(inputFile));
                final PrintWriter out = new PrintWriter(new FileWriter(outputFile), true);
                final PrintWriter err = new PrintWriter(new FileWriter(errorFile), true);

                this.builder.init(out, err);

                String record;
                for (int recordNumber = 1; (record = in.readLine()) != null; recordNumber++) {
                    final Object parsedRecord = this.builder.parseRecord(record);

                    if (parsedRecord != null) {
                        final String string = this.builder.getString(parsedRecord);

                        Object standardizedObject = null;
                        try {
                            standardizedObject = this.standardizer.standardize(string, variant.toUpperCase());
                        } catch (final Exception e) {
                            final String messageText = buildErrorMessage(recordNumber, record, e);
                            logger.warning(messageText);
                            err.println(messageText);
                            e.printStackTrace();
                            continue;
                        }

                        final String signature = this.standardizer.getSignature(standardizedObject);
                        final Map<String, String> standardizedFields = this.standardizer.getFields(standardizedObject);
                        this.builder.processRecord(recordNumber, standardizedFields, parsedRecord, signature);
                    }
                }

                this.builder.end();

                in.close();
                out.close();
                err.close();

                if (errorFile.length() == 0L) {
                    errorFile.delete();
                }
            }
        }
    }

    private static File getFile(final String prefix, final String variant) {
        return new File(prefix + "-" + variant + ".txt");
    }

    private static String buildErrorMessage(final int recordNumber, final String record, final Throwable t) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final PrintWriter pw = new PrintWriter(baos);
        t.printStackTrace(pw);
        pw.flush();
        pw.close();
        final String message = baos.toString().replaceAll("[\\r\\n]+", "|");
        final StackTraceElement element = t.getStackTrace()[0];
        final String fileName = element.getFileName();
        final int lineNumber = element.getLineNumber();
        return recordNumber + "\t" + record + "\t" + message + "\t" + fileName + "\t" + lineNumber;
    }
}
