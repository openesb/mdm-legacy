package com.sun.mdm.sbme.pattern;

import java.util.List;

public interface OutputPatternPostprocessor<I extends Enum<I>, O extends Enum<O>> {
	public void postprocess(List<? extends OutputPattern<I, O>> patternsFound);
}
