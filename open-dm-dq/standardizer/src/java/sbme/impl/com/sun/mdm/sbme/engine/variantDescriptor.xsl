<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:exsl="http://exslt.org/common">

	<xsl:output method="xml"
				indent="yes"
				doctype-public="-//SPRING//DTD BEAN 2.0//EN"
				doctype-system="http://www.springframework.org/dtd/spring-beans-2.0.dtd"/>

	<xsl:template match="dataTypeVariant">
		<beans>
			<bean id="dataTypeVariant" class="com.sun.mdm.sbme.engine.VariantDescriptorImpl">
				<property name="name">
					<value>
						<xsl:value-of select="@name"/>
					</value>
				</property>
				<property name="standardizerFactory">
					<xsl:choose>
						<xsl:when test="@factoryClass">
							<bean class="{@factoryClass}"/>
						</xsl:when>
						<xsl:otherwise>
							<bean class="{factory/@class}">
								<xsl:apply-templates select="factory/*"/>
							</bean>
						</xsl:otherwise>
					</xsl:choose>
				</property>
				<xsl:if test="description">
					<property name="description">
						<value>
							<xsl:value-of select="normalize-space(string(description))"/>
						</value>
					</property>
				</xsl:if>
				<xsl:if test="@dataType">
					<property name="dataTypeName">
						<value>
							<xsl:value-of select="@dataType"/>
						</value>
					</property>
				</xsl:if>
			</bean>
		</beans>
	</xsl:template>
	<xsl:template match="@*|node()"  priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
