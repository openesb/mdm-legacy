/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.normalizer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sun.inti.components.string.format.EnumerationFormatter;
import com.sun.inti.components.string.format.Formatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedLengthTokenizer;
import com.sun.inti.components.string.tokenize.Tokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.inti.components.util.Abbreviated;


/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class ClueWord<I extends Enum<I>> implements Serializable{
    private int id;
    private I type;

    public ClueWord(final int id, final I type) {
        this.id = id;
        this.type = type;
    }

    public ClueWord(final I type) {
        this.id = 0;
        this.type = type;
    }

    public static class ClueWordListFormatter implements Formatter {
    	private Tokenizer fieldTokenizer;
    	private Tokenizer partTokenizer;
    	private Formatter formatter;
    	
    	public ClueWordListFormatter(Class<? extends Enum<?>> enumType, boolean useAbbreviation) {
            FixedFieldTokenizer theTokenizer = new FixedFieldTokenizer();
            FieldDescriptor[] descriptors = new FieldDescriptor[] {
                    new FieldDescriptor(0, 4, true), // Id
                    new FieldDescriptor(4, 2, false), // Input token type
            };
            
            theTokenizer.setFieldDescriptors(descriptors);
            
            this.fieldTokenizer = theTokenizer;
            this.partTokenizer = new FixedLengthTokenizer(6);
            this.formatter = new EnumerationFormatter(enumType, useAbbreviation);
    	}
    	
		public String format(Object object) {
			@SuppressWarnings("unchecked")
			List<ClueWord<?>> clueWords = (List<ClueWord<?>>) object;
			StringBuilder builder = new StringBuilder();
			for (ClueWord<?> clueWord: clueWords) {
				final String type;
				if (clueWord.getType() instanceof Abbreviated) {
					type = ((Abbreviated) clueWord.getType()).getAbbreviation();
				} else {
					type = clueWord.getType().toString();
				}
				builder.append(String.format("%4d%s", clueWord.getId(), type));
			}
			return builder.toString();
		}

		public List<ClueWord<?>> parse(String string) {
			List<ClueWord<?>> clueWords = new ArrayList<ClueWord<?>>();
			for (String part: this.partTokenizer.tokenize(string)) {
				if (part.trim().length() != 0) {
					List<String> tokens = this.fieldTokenizer.tokenize(part);
					int id = Integer.parseInt(tokens.get(0));
					@SuppressWarnings("unchecked")
					Enum<?> type = (Enum<?>) this.formatter.parse(tokens.get(1));
					// FIXME Deal with parametrized type in ClueWord<I>
					ClueWord<?> clueWord = new ClueWord(id, type);
					clueWords.add(clueWord);
				}
			}
			return clueWords;
		}
    }
   
    public String toString() {
    	return "{" + id + ", " + type + "}";
    }

    public int getId() {
        return this.id;
    }

    public I getType() {
        return this.type;
    }
}
