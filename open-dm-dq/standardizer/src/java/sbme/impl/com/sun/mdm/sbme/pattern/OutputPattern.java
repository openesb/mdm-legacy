package com.sun.mdm.sbme.pattern;

public interface OutputPattern<I extends Enum<I>, O extends Enum<O>> extends Pattern<I, O> {
	public int getBegin();
	public int getEnd();
}
