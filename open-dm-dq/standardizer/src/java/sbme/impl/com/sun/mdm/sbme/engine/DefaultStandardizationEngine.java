package com.sun.mdm.sbme.engine;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DescriptiveResource;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.sun.inti.components.classloading.FileClassLoaderFactory;
import com.sun.inti.components.classloading.URLFileClassLoaderFactory;
import com.sun.inti.components.util.WorkingDirectoryAware;
import com.sun.mdm.sbme.DataTypeDescriptor;
import com.sun.mdm.sbme.Descriptor;
import com.sun.mdm.sbme.StandardizationEngine;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.StandardizerFactory;
import com.sun.mdm.sbme.VariantDescriptor;
import com.sun.mdm.sbme.util.StandardizerUtils;

// FIXME Refactor to unify different deployment scenarios (jar, directory)
// FIXME Redeployment of jar-deployed variants results in file removal errors
public class DefaultStandardizationEngine implements StandardizationEngine {
	private static final String LIB_DIRECTORY_NAME = "lib";
	private static final String RESOURCE_DIRECTORY_NAME = "resource";
	
	private static final String DATA_TYPE_BEAN_NAME = "dataType";
	private static final String DATA_TYPE_DESCRIPTOR_NAME = "dataTypeDescriptor";
	private static final String DATA_TYPE_DESCRIPTOR_STYLESHEET = DATA_TYPE_DESCRIPTOR_NAME + ".xsl";
	private static final Templates dataTypeDescriptorTemplates;
	private static final Templates variantDescriptorTemplates;

	private static final String VARIANT_DIRECTORY_NAME = "variants";
	private static final String VARIANT_BEAN_NAME = "dataTypeVariant";
	private static final String VARIANT_DESCRIPTOR_NAME = "variantDescriptor";
	private static final String VARIANT_DESCRIPTOR_STYLESHEET = VARIANT_DESCRIPTOR_NAME + ".xsl";

	public static final String DATA_TYPE_DESCRIPTOR_FILENAME = DATA_TYPE_DESCRIPTOR_NAME + ".xml";
	public static final String VARIANT_DESCRIPTOR_FILENAME = VARIANT_DESCRIPTOR_NAME + ".xml";
	
	public static final Logger logger = Logger.getLogger(DefaultStandardizationEngine.class.getName());
	
	private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();
	private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	
	private final static FileFilter directoryFilter = new FileFilter() {
		public boolean accept(File file) {
			return file.isDirectory() && !file.getName().equals("META-INF");
		}
	};
	
	static {
		try {
			InputStream dtdis = DefaultStandardizationEngine.class.getResourceAsStream(DATA_TYPE_DESCRIPTOR_STYLESHEET);
			Source dtdisSource = new StreamSource(dtdis);
			dataTypeDescriptorTemplates = transformerFactory.newTemplates(dtdisSource);

			InputStream vdis = DefaultStandardizationEngine.class.getResourceAsStream(VARIANT_DESCRIPTOR_STYLESHEET);
			Source vdisSource = new StreamSource(vdis);
			variantDescriptorTemplates = transformerFactory.newTemplates(vdisSource);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error loading deployer stylesheets: " + e, e);
		}
	}
	
	private static int UNINITIALIZED = 0;
	private static int STARTED = 1;
	private static int STOPPED = 2;
	
	private FileClassLoaderFactory classLoaderFactory;
	
	public DefaultStandardizationEngine() {
		try {
			this.classLoaderFactory = (FileClassLoaderFactory) StandardizerUtils.loadService(FileClassLoaderFactory.class);
		} catch (Exception e) {
			this.classLoaderFactory = new URLFileClassLoaderFactory();
		}
	}
	
	public DefaultStandardizationEngine(FileClassLoaderFactory classLoaderFactory) {
		this.classLoaderFactory = classLoaderFactory;
	}
	
	private int state = UNINITIALIZED;
	private File repositoryDirectory;
	private Map<String, DataTypeDescriptorImpl> dataTypeDescriptors = new HashMap<String, DataTypeDescriptorImpl>();

	public void initialize(File repositoryDirectory) {
		if (this.state != UNINITIALIZED) {
			throw new IllegalStateException("Engine has already been initialized");
		}
		
		repositoryDirectory.mkdirs();
		if (!repositoryDirectory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + repositoryDirectory);
		}
		if (!repositoryDirectory.canRead() || !repositoryDirectory.canWrite()) {
			throw new IllegalArgumentException("Can't read/write: " + repositoryDirectory);
		}

		this.state = STOPPED;
		this.repositoryDirectory = repositoryDirectory;
		logger.info("Initializing standardization engine. Repository: " + this.repositoryDirectory.getAbsolutePath());
	}
	
	public void start() throws Exception {
		if (this.state != STOPPED) {
			throw new IllegalStateException("Engine is not stopped");
		}
		
		logger.info("Starting standardization engine");
		for (File directory: this.repositoryDirectory.listFiles(directoryFilter)) {
			logger.info("Deploying previously loaded data type: " + directory.getAbsolutePath());
			DataTypeDescriptorImpl dataTypeDescriptor = this.deployDataTypeDir(directory);
			this.dataTypeDescriptors.put(dataTypeDescriptor.getName(), dataTypeDescriptor);
		}
		this.state = STARTED;
	}
	
	public Descriptor deploy(JarFile jarFile) throws Exception {
		if (jarFile.getJarEntry(DATA_TYPE_DESCRIPTOR_FILENAME) != null) {
			return this.deployDataType(jarFile);
		} else if (jarFile.getJarEntry(VARIANT_DESCRIPTOR_FILENAME) != null) {
			return this.deployVariant(jarFile);
		}
		throw new IllegalArgumentException("Invalid or missing descriptor in jar file: " + jarFile.getName());
	}
	
	public DataTypeDescriptor deployDataType(JarFile jarFile) throws Exception {
		if (this.state != STARTED) {
			throw new IllegalStateException("Engine is not started");
		}
		
        final DataTypeDescriptorImpl dataTypeDescriptor = deployDataTypeJar(jarFile);
        this.dataTypeDescriptors.put(dataTypeDescriptor.getName(), dataTypeDescriptor);
		return dataTypeDescriptor;
	}
	
	public VariantDescriptor deployVariant(JarFile jarFile) throws Exception {
		if (this.state != STARTED) {
			throw new IllegalStateException("Engine is not started");
		}
		
        final VariantDescriptorImpl variantDescriptor = deployVariantJar(jarFile);
        DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(variantDescriptor.getDataTypeName());
        dataTypeDescriptor.addVariant(variantDescriptor);
		return variantDescriptor;
	}
	
	public DataTypeDescriptor undeployDataType(String dataTypeName) throws Exception {
		if (this.state != STARTED) {
			throw new IllegalStateException("Engine is not started");
		}
		
		DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(dataTypeName);
		if (dataTypeDescriptor == null) {
			throw new IllegalArgumentException("No such data type: " + dataTypeName);
		}

		logger.info("Undeploying data type '" + dataTypeName + "'");
		this.dataTypeDescriptors.remove(dataTypeName);
		closeDataType(dataTypeDescriptor);
		StandardizerUtils.delete(new File(this.repositoryDirectory, dataTypeName));
		
		return dataTypeDescriptor;
	}
	
	public VariantDescriptor undeployVariant(String dataTypeName, String variantName) throws Exception {
		if (this.state != STARTED) {
			throw new IllegalStateException("Engine is not started");
		}
		
		DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(dataTypeName);
		if (dataTypeDescriptor == null) {
			throw new IllegalArgumentException("No such data type: " + dataTypeName);
		}
		
		VariantDescriptorImpl variantDescriptor = dataTypeDescriptor.getVariant(variantName);

		logger.info("Undeploying variant '" + variantName + "' of data type '" + dataTypeName + "'");
		dataTypeDescriptor.removeVariant(variantName);
		this.classLoaderFactory.close(variantDescriptor.getClassLoader());
		File dataTypeDirectory = new File(this.repositoryDirectory, dataTypeName);
		File variantDirectory = new File(new File(dataTypeDirectory, VARIANT_DIRECTORY_NAME), variantName);
		StandardizerUtils.delete(variantDirectory);
		
		return variantDescriptor;
	}
	
	public void stop() throws Exception {
		if (this.state != STARTED) {
			throw new IllegalStateException("Engine is not started");
		}
		
		logger.info("Stopping standardization engine");
		this.dataTypeDescriptors.clear();
		for (DataTypeDescriptorImpl dataTypeDescriptor: this.dataTypeDescriptors.values()) {
			closeDataType(dataTypeDescriptor);
		}
	}
	
	public Set<String> dataTypeNames() {
		return Collections.unmodifiableSet(this.dataTypeDescriptors.keySet());
	}
	
	public DataTypeDescriptorImpl getDataTypeDescriptor(String dataTypeName) {
		DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(dataTypeName);
		if (dataTypeDescriptor == null) {
			throw new IllegalArgumentException("No such data type: " + dataTypeName);
		}
		return dataTypeDescriptor;
	}
	
	public Standardizer getStandardizer(String dataTypeName, String variantName) throws Exception {
		DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(dataTypeName);
		if (dataTypeDescriptor == null) {
			throw new IllegalArgumentException("No such data type: " + dataTypeName);
		}
		VariantDescriptorImpl variantDescriptor = dataTypeDescriptor.getVariant(variantName);
		return variantDescriptor.getStandardizerFactory().newStandardizer();
	}
	
	private void closeDataType(DataTypeDescriptorImpl dataTypeDescriptor) throws Exception {
		for (String variantName: dataTypeDescriptor.variantNames()) {
			VariantDescriptor variantDescriptor = dataTypeDescriptor.getVariant(variantName);
			logger.info("Undeploying variant '" + variantDescriptor.getName() + "'");
			this.classLoaderFactory.close(((VariantDescriptorImpl) variantDescriptor).getClassLoader());
		}
		this.classLoaderFactory.close(dataTypeDescriptor.getClassLoader());
	}
	
	private VariantDescriptorImpl deployVariantJar(JarFile jarFile) throws Exception {
		ZipEntry descriptorEntry = jarFile.getEntry(VARIANT_DESCRIPTOR_FILENAME);
		if (descriptorEntry == null) {
			throw new IllegalArgumentException("Missing descriptor '" + VARIANT_DESCRIPTOR_FILENAME + "' in deployment file '" + jarFile.getName() + "'");
		}
		
		InputStream is = jarFile.getInputStream(descriptorEntry);
		Document variantDocument = getDataTypeDocument(is);
		is.close();

		String dataTypeName = variantDocument.getDocumentElement().getAttribute("dataType").trim();
		if ("".equals(dataTypeName)) {
			throw new IllegalArgumentException("Missing data type name in descriptor file");
		}
		DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(dataTypeName);
		if (dataTypeDescriptor == null) {
			throw new IllegalArgumentException("No such data type: " + dataTypeName);
		}

		String variantName = variantDocument.getDocumentElement().getAttribute("name").trim();
		if ("".equals(variantName)) {
			throw new IllegalArgumentException("Missing variant name in descriptor file");
		}
		if (VARIANT_DIRECTORY_NAME.equals(variantName)) {
			throw new IllegalArgumentException("'" + VARIANT_DIRECTORY_NAME + "' is a reserved name, cannot be used for data type variant");
		}

		logger.info("Deploying variant '" + variantName + "' of data type '" + dataTypeName + "' from file '" + jarFile.getName() + "'");
		
		File dataTypeDirectory = new File(this.repositoryDirectory, dataTypeName);
		File variantDirectory = new File(new File(dataTypeDirectory, VARIANT_DIRECTORY_NAME), variantName);
		
		StandardizerUtils.extract(jarFile, variantDirectory);
		
		File libDirectory = new File(variantDirectory, LIB_DIRECTORY_NAME);
		File resourceDirectory = new File(variantDirectory, RESOURCE_DIRECTORY_NAME);
        ClassLoader classLoader = createClassLoader(libDirectory, resourceDirectory, dataTypeDescriptor.getClassLoader());

        VariantDescriptorImpl variantDescriptor = (VariantDescriptorImpl) getBean(variantDescriptorTemplates, variantDocument, classLoader, VARIANT_BEAN_NAME, variantName);
        variantDescriptor.setClassLoader(classLoader);
        variantDescriptor.setLibDirectory(libDirectory);
        variantDescriptor.setResourceDirectory(resourceDirectory);
        StandardizerFactory standardizerFactory = variantDescriptor.getStandardizerFactory();
        if (standardizerFactory instanceof WorkingDirectoryAware) {
        	((WorkingDirectoryAware) standardizerFactory).setWorkingDirectory(resourceDirectory);
        }
        
        dataTypeDescriptor.addVariant(variantDescriptor);
        
        return variantDescriptor;
	}
	
	private VariantDescriptorImpl deployVariantDir(DataTypeDescriptorImpl dataTypeDescriptor, File variantDirectory) throws Exception {
		InputStream is = new FileInputStream(new File(variantDirectory, VARIANT_DESCRIPTOR_FILENAME));
		Document variantDocument = getDataTypeDocument(is);
		is.close();

		String variantName = variantDocument.getDocumentElement().getAttribute("name").trim();
		if ("".equals(variantName)) {
			throw new IllegalArgumentException("Missing variant name in descriptor file");
		}

		logger.info("Deploying variant '" + variantName + "' of data type '" + dataTypeDescriptor.getName() + "' from directory '" + variantDirectory + "'");
				
		File libDirectory = new File(variantDirectory, LIB_DIRECTORY_NAME);
		File resourceDirectory = new File(variantDirectory, RESOURCE_DIRECTORY_NAME);
        ClassLoader classLoader = createClassLoader(libDirectory, resourceDirectory, dataTypeDescriptor.getClassLoader());

        VariantDescriptorImpl variantDescriptor = (VariantDescriptorImpl) getBean(variantDescriptorTemplates, variantDocument, classLoader, VARIANT_BEAN_NAME, variantName);
        variantDescriptor.setClassLoader(classLoader);
        variantDescriptor.setLibDirectory(libDirectory);
        variantDescriptor.setResourceDirectory(resourceDirectory);
        StandardizerFactory standardizerFactory = variantDescriptor.getStandardizerFactory();
        if (standardizerFactory instanceof WorkingDirectoryAware) {
        	((WorkingDirectoryAware) standardizerFactory).setWorkingDirectory(resourceDirectory);
        }

        dataTypeDescriptor.addVariant(variantDescriptor);
        
        return variantDescriptor;
	}
	
	private DataTypeDescriptorImpl deployDataTypeJar(JarFile jarFile) throws Exception {
		ZipEntry descriptorEntry = jarFile.getEntry(DATA_TYPE_DESCRIPTOR_FILENAME);
		if (descriptorEntry == null) {
			throw new IllegalArgumentException("Missing descriptor '" + DATA_TYPE_DESCRIPTOR_FILENAME + "' in deployment file '" + jarFile.getName() + "'");
		}
		
		InputStream is = jarFile.getInputStream(descriptorEntry);
		Document dataTypeDocument = getDataTypeDocument(is);
		is.close();

		String dataTypeName = dataTypeDocument.getDocumentElement().getAttribute("name").trim();
		if ("".equals(dataTypeName)) {
			throw new IllegalArgumentException("Missing data type in descriptor file");
		}

		logger.info("Deploying data type '" + dataTypeName + "' from file '" + jarFile.getName() + "'");
		
		File dataTypeDirectory = new File(this.repositoryDirectory, dataTypeName);
		DataTypeDescriptorImpl dataTypeDescriptor = this.dataTypeDescriptors.get(dataTypeName);
		if (dataTypeDescriptor != null) {
			this.undeployDataType(dataTypeName);
		}
		
		StandardizerUtils.extract(jarFile, dataTypeDirectory);
		
		File libDirectory = new File(dataTypeDirectory, LIB_DIRECTORY_NAME);
		File resourceDirectory = new File(dataTypeDirectory, RESOURCE_DIRECTORY_NAME);
        ClassLoader classLoader = createClassLoader(libDirectory, resourceDirectory);

        dataTypeDescriptor = (DataTypeDescriptorImpl) getBean(dataTypeDescriptorTemplates, dataTypeDocument, classLoader, DATA_TYPE_BEAN_NAME, dataTypeName);
        dataTypeDescriptor.setClassLoader(classLoader);
        dataTypeDescriptor.setLibDirectory(libDirectory);
        dataTypeDescriptor.setResourceDirectory(resourceDirectory);

        File[] variantDirectories = new File(dataTypeDirectory, VARIANT_DIRECTORY_NAME).listFiles(directoryFilter);
        if (variantDirectories != null) {
            for (File variantDirectory: variantDirectories) {
            	try {
                	this.deployVariantDir(dataTypeDescriptor, variantDirectory);
            	} catch (Exception e) {
            		logger.warning("Can't deploy variant: '" + variantDirectory.getName() + "': " + e);
            	}
            }
        }
        
        return dataTypeDescriptor; 
	}

	private DataTypeDescriptorImpl deployDataTypeDir(File dataTypeDirectory) throws Exception {
		InputStream is = new FileInputStream(new File(dataTypeDirectory, DATA_TYPE_DESCRIPTOR_FILENAME));
		Document dataTypeDocument = getDataTypeDocument(is);
		is.close();

		String dataTypeName = dataTypeDocument.getDocumentElement().getAttribute("name").trim();
		if ("".equals(dataTypeName)) {
			throw new IllegalArgumentException("Missing data type in descriptor file");
		}

		logger.info("Deploying data type '" + dataTypeName + "' from directory '" + dataTypeDirectory.getAbsolutePath() + "'");
		
		File libDirectory = new File(dataTypeDirectory, LIB_DIRECTORY_NAME);
		File resourceDirectory = new File(dataTypeDirectory, RESOURCE_DIRECTORY_NAME);
        ClassLoader classLoader = createClassLoader(libDirectory, resourceDirectory);

        DataTypeDescriptorImpl dataTypeDescriptor = (DataTypeDescriptorImpl) getBean(dataTypeDescriptorTemplates, dataTypeDocument, classLoader, DATA_TYPE_BEAN_NAME, dataTypeName);
        dataTypeDescriptor.setClassLoader(classLoader);
        dataTypeDescriptor.setLibDirectory(libDirectory);
        dataTypeDescriptor.setResourceDirectory(resourceDirectory);

        File[] variantDirectories = new File(dataTypeDirectory, VARIANT_DIRECTORY_NAME).listFiles(directoryFilter);
        if (variantDirectories != null) {
            for (File variantDirectory: variantDirectories) {
            	try {
                	this.deployVariantDir(dataTypeDescriptor, variantDirectory);
            	} catch (Exception e) {
            		logger.warning("Can't deploy variant: '" + variantDirectory.getName() + "': " + e);
            	}
            }
        }
        
        return dataTypeDescriptor; 
	}

	private ClassLoader createClassLoader(File libDirectory, File resourceDirectory) throws Exception {
		return createClassLoader(libDirectory, resourceDirectory, DefaultStandardizationEngine.class.getClassLoader());
	}

	private ClassLoader createClassLoader(File libDirectory, File resourceDirectory, ClassLoader parentClassLoader) throws Exception {
		List<File> locations = new ArrayList<File>();
		
		if (resourceDirectory.isDirectory()) {
			locations.add(resourceDirectory);
		}
		
		if (libDirectory.isDirectory()) {
			File[] jarFiles = libDirectory.listFiles(new FilenameFilter() {
	            public boolean accept(File dir, String name) {
	                return name.endsWith(".jar") ||
	                       name.endsWith(".zip");
	            }
	        });
			for (File jarFile: jarFiles) {
				locations.add(jarFile);
			}
		}
		
		if (locations.size() == 0) {
			return parentClassLoader;
		}

        return this.classLoaderFactory.newClassLoader(locations.toArray(new File[locations.size()]), parentClassLoader);
	}
	
	private static Object getBean(Templates templates, Document inputDocument, ClassLoader classLoader, String beanName, String fileName) throws Exception {
		Source source = new DOMSource(inputDocument);
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document outputDocument = documentBuilder.newDocument();
		Result result = new DOMResult(outputDocument);
		Transformer transformer = templates.newTransformer();
		transformer.transform(source, result);
		
		GenericApplicationContext context = null;
		ClassLoader threadClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			context = new GenericApplicationContext();
			XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(context);
			reader.registerBeanDefinitions(outputDocument, new DescriptiveResource(fileName));
			return context.getBean(beanName);
		} finally {
			Thread.currentThread().setContextClassLoader(threadClassLoader);
		}
	}
	private static Document getDataTypeDocument(InputStream is) throws Exception {
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		return documentBuilder.parse(new InputSource(is));
	}
}
