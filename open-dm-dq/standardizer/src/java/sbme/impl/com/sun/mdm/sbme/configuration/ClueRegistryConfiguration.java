package com.sun.mdm.sbme.configuration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.clue.Clue;

public abstract class ClueRegistryConfiguration {
	public Map<String, Clue<?>> getClues() throws Exception {
        FixedFieldTokenizer clueRegistryTokenizer = new FixedFieldTokenizer();
        clueRegistryTokenizer.setFieldDescriptors(this.getFieldDescriptors());
        
		PropertyDescriptor[] clueRegistryPropertyDescriptors = this.getPropertyDescriptors();

        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(clueRegistryTokenizer, clueRegistryPropertyDescriptors),
        };
        
        String clueFile = "META-INF/mdm/sbme/" + this.getDataTypeName() + "/" + this.getVariantName() + "/clues.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(clueFile);

        if (is == null) {
        	throw new IllegalArgumentException("Could not load clue data file: " + clueFile);
        }
        
        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
    	Map<String, Clue<?>> clues = new LinkedHashMap<String, Clue<?>>();
        while ((record = recordSource.nextRecord()) != null) {
        	@SuppressWarnings("unchecked")
        	Clue<?> clue = (Clue<?>) PropertyDescriptor.populate(Clue.class, record);
        	clues.put(clue.getName(), clue);
        }

        return clues;
	}
	
	protected abstract String getDataTypeName();
	protected abstract String getVariantName();
	protected abstract PropertyDescriptor[] getPropertyDescriptors();
	protected abstract FieldDescriptor[] getFieldDescriptors();
}
