package com.sun.mdm.sbme;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextFactory  {
    private final String beanName;
    private final ApplicationContext applicationContext;
    
    public ApplicationContextFactory(String dataTypeName, String variantName, String resourceFormat, String beanName) {
    	this.beanName = beanName;
    	if (variantName != null) {
    		variantName = variantName.toUpperCase();
    	}
        final String resourceLocation = String.format(resourceFormat, dataTypeName, variantName);
        this.applicationContext = new ClassPathXmlApplicationContext(resourceLocation);
    }
    
	public Object getBean() {
		return  this.applicationContext.getBean(this.beanName);
	}
}
