package com.sun.mdm.sbme.pattern;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractPattern<I extends Enum<I>, O extends Enum<O>> implements Pattern<I, O>, Serializable {
	private List<I> inputTokenTypes;
	private List<O> outputTokenTypes;
	
	public AbstractPattern() {}
	
	public AbstractPattern(List<I> inputTokenTypes, List<O> outputTokenTypes)  {
		// FIXME Should we avoid creating additional lists?
		this.inputTokenTypes = new ArrayList<I>(inputTokenTypes);
		this.outputTokenTypes = new ArrayList<O>(outputTokenTypes);
	}

	public List<I> getInputTokenTypes() {
		return Collections.unmodifiableList(this.inputTokenTypes);
	}

	public List<O> getOutputTokenTypes() {
		return Collections.unmodifiableList(this.outputTokenTypes);
	}
	public void setInputTokenTypes(List<I> inputTokenTypes) {
		this.inputTokenTypes = inputTokenTypes;
	}

	public void setOutputTokenTypes(List<O> outputTokenTypes) {
		this.outputTokenTypes = outputTokenTypes;
	}

	public I getInputTokenType(int index) {
		return this.inputTokenTypes.get(index);
	}

	public void setInputTokenType(int index, I type) {
		this.inputTokenTypes.set(index, type);
	}

	public O getOutputTokenType(int index) {
		return this.outputTokenTypes.get(index);
	}

	public void setOutputTokenType(int index, O type) {
		this.outputTokenTypes.set(index, type);
	}
}
