package com.sun.mdm.sbme;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.StandardizerFactory;
import com.sun.mdm.sbme.StandardizerRegistry;

public class DefaultStandardizerRegistry implements StandardizerRegistry {
	private Logger logger = Logger.getLogger(DefaultStandardizerRegistry.class.getName());
	
	private Map<String, Map<String, StandardizerFactory>> factories = new HashMap<String, Map<String, StandardizerFactory>>();
	
	public DefaultStandardizerRegistry() throws Exception {
		Enumeration<URL> urls = getClass().getClassLoader().getResources("META-INF/mdm/sbme/standardizer.properties");
		while (urls.hasMoreElements()) {
			URL url = urls.nextElement();
			InputStream is = url.openStream();
			load(is);
			is.close();
		}
	}
	
	private void load(InputStream is) throws Exception {
		Properties standardizerProperties = new Properties();
		try {
			standardizerProperties.load(is);
		} catch (Exception e) {
			logger.severe("Error loading standardizer properties" + e);
			throw e;
		} finally {
			try { is.close(); } catch (Exception x) {}
		}
		
		String[] dataTypeNames = standardizerProperties.getProperty("dataTypes").split("\\s*,\\s*");
		for (String dataTypeName: dataTypeNames) {
			String[] variantNames = standardizerProperties.getProperty(dataTypeName + ".variants").split("\\s*,\\s*");
			for (String variantName: variantNames) {
				String className = standardizerProperties.getProperty(dataTypeName + "." + variantName + ".factory");
				@SuppressWarnings ("unchecked")
				Class<StandardizerFactory> clazz = (Class<StandardizerFactory>) getClass().getClassLoader().loadClass(className);
				
				Map<String, StandardizerFactory> variantFactories = this.factories.get(dataTypeName);
				if (variantFactories == null) {
					variantFactories = new HashMap<String, StandardizerFactory>();
					this.factories.put(dataTypeName, variantFactories);
				}
				
				variantFactories.put(variantName, clazz.newInstance());
			}
		}
	}
	
	public Standardizer lookup(String type, String variant) {
		Map<String, StandardizerFactory> variantFactories = this.factories.get(type);
		if (variantFactories == null) {
			throw new IllegalArgumentException("No such data type: " + type); 
		}
		
		StandardizerFactory factory = variantFactories.get(variant);
		if (factory == null) {
			throw new IllegalArgumentException("No such variant: " + variant);
		}
		
		return factory.newStandardizer();
	}

}
