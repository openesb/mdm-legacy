/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.normalizer;

import java.util.ArrayList;
import java.util.List;

import com.sun.mdm.sbme.MdmStandardizationException;

/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class NormalizedToken<I extends Enum<I>> {
    private String image;
    private List<ClueWord<I>> clueWords = new ArrayList<ClueWord<I>>();

    private int start;
    private int end;

    public NormalizedToken() {}

    public NormalizedToken(final String image, final int start, final int end) {
        this.image = image;
        this.start = start;
        this.end = end;
    }

    public List<ClueWord<I>> getClueWords() {
        return this.clueWords;
    }

    public void addClueWord(final ClueWord<I> clueWord) {
        this.clueWords.add(clueWord);
    }

    public boolean hasType(final I type) {
        for (final ClueWord<I> clueWord : this.clueWords) {
            if (type == clueWord.getType()) {
                return true;
            }
        }
        return false;
    }

    public boolean hasType(final I[] types) {
        for (final I type : types) {
            if (this.hasType(type)) {
                return true;
            }
        }
        return false;
    }

    public void assignClueWords(final ClueWord<I> replacement) {
        this.clueWords = new ArrayList<ClueWord<I>>();
        this.clueWords.add(replacement);
    }

    public void append(final NormalizedToken<I> other) {
        this.image += other.image;
        this.start = other.start;
        this.end = other.end;
        this.clueWords = new ArrayList<ClueWord<I>>(other.clueWords);
    }

    public void copyTo(final NormalizedToken<I> other) {
        other.image  = this.image;
        other.clueWords = new ArrayList<ClueWord<I>>(this.clueWords);
        other.start = this.start;
        other.end = this.end;
    }

    public void copyFrom(final NormalizedToken<I> other) {
        this.image = other.image;
        this.clueWords = new ArrayList<ClueWord<I>>(other.clueWords);
        this.start = other.start;
        this.end = other.end;
    }
    
    public int getClueWordId(final I inputTokenType) throws MdmStandardizationException {
        for (ClueWord<I> clueWord: this.getClueWords()) {
            if (clueWord.getType()== inputTokenType) {
                return clueWord.getId();
            }
        }

        throw new MdmStandardizationException("Token with image '" + this.getImage() + "' has no corresponding clue word for input token type '" + inputTokenType + "'");
    }

    public String getImage() {
        return this.image;
    }

    public ClueWord<I> getClueWord(final int index) {
        return this.clueWords.get(index);
    }

    public int getClueWordCount() {
        return this.clueWords.size();
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public void setStart(final int beg) {
        this.start = beg;
    }

    public void setEnd(final int end) {
        this.end = end;
    }
}
