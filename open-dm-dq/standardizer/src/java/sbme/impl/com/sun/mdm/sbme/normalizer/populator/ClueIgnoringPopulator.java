package com.sun.mdm.sbme.normalizer.populator;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.parser.Tokenization;

public abstract class ClueIgnoringPopulator<I extends Enum<I>> extends NonNullCluePopulator<I> {

	@Override
	public int doPopulate(List<NormalizedToken<I>> normalizedTokens, Clue<I> clue, Tokenization tokenization, int startIndex, int endIndex, Record contextRecord) {
        return this.doPopulate(normalizedTokens, tokenization, startIndex, endIndex, contextRecord);
	}

    protected abstract int doPopulate(List<NormalizedToken<I>> normalizedTokens, Tokenization tokenization, int startIndex, int endIndex, Record contextRecord);
}
