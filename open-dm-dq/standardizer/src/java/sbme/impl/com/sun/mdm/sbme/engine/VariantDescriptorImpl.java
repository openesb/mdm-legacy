package com.sun.mdm.sbme.engine;

import com.sun.mdm.sbme.StandardizerFactory;
import com.sun.mdm.sbme.VariantDescriptor;

public class VariantDescriptorImpl extends Descriptor implements VariantDescriptor {
	private String dataTypeName;
	private StandardizerFactory standardizerFactory;

	@Override
	public String toString() {
		return "Name: " + this.getName() + ", " +
			   "Description: " + this.getDescription() + ", " +
			   "Data type: " + this.dataTypeName + ", " +
			   "Factory : " + this.standardizerFactory.getClass().getName();
	}

	public String getDataTypeName() {
		return dataTypeName;
	}
	public void setDataTypeName(String dataTypeName) {
		this.dataTypeName = dataTypeName;
	}
	
	public StandardizerFactory getStandardizerFactory() {
		return this.standardizerFactory;
	}
	public void setStandardizerFactory(StandardizerFactory standardizerFactory) {
		this.standardizerFactory = standardizerFactory;
	}
}
