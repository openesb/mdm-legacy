/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.parser;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class Tokenization {
    private final List<BaseToken> tokens = new ArrayList<BaseToken>();

    public void addToken(final String image, final boolean wordSplit) {
        int wordSplitCount = 0;
        if (this.tokens.size() > 0 && this.tokens.get(this.tokens.size() - 1).isWordSplit()) {
            wordSplitCount++;
        }
        if (wordSplit) {
            wordSplitCount++;
        }
        final BaseToken baseToken = new BaseToken(image, wordSplit, wordSplitCount);
        this.tokens.add(baseToken);
    }

    public BaseToken getToken(final int i) {
        return this.tokens.get(i);
    }

    public List<String> getTokenImages() {
        final List<String> images = new ArrayList<String>();
        for (final BaseToken token : this.tokens) {
            images.add(token.getImage());
        }
        return images;
    }

    public String concatenateTokens(final int start, final int end) {
    	// TODO Validate ranges
        final StringBuilder result = new StringBuilder();
        for (int i = start; i < end; i++) {
            if (i > start) {
                result.append(' ');
            }
            result.append(this.tokens.get(i).getImage());
        }
        return result.toString();
    }

    public int size() {
        return this.tokens.size();
    }
}
