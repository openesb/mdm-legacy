package com.sun.mdm.sbme.pattern;

import java.util.List;

public interface Pattern<I extends Enum<I>, O extends Enum<O>> {
    public List<I> getInputTokenTypes();
    public List<O> getOutputTokenTypes();
    public void setInputTokenType(int index, I inputTokenType);
    public void setOutputTokenType(int index, O outputTokenType);
}
