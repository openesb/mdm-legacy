/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.StandardizerRegistry;


/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class ApplicationContextStandardizerRegistry implements StandardizerRegistry {
    private static final String beanName = "standardizer";
    private final static String resourceFormat = "/META-INF/mdm/%s/standardizerBeans%s.xml";
    
    private final Map<String, Map<String, ApplicationContext>> contexts = new HashMap<String, Map<String, ApplicationContext>>();

    public Standardizer lookup(String type, String variant) {
    	Map<String, ApplicationContext> contextMap = this.contexts.get(type);
    	if (contextMap == null) {
    		contextMap = new HashMap<String, ApplicationContext>();
    		this.contexts.put(type, contextMap);
    	}
    	
        ApplicationContext context = contextMap.get(variant);
        if (context == null) {
            final String resourceLocation = String.format(resourceFormat, type, variant.toUpperCase());
            context = new ClassPathXmlApplicationContext(resourceLocation);
            contextMap.put(variant, context);
        }

        final Standardizer standardizer = (Standardizer) context.getBean(beanName);
        return standardizer;
    }
}
