package com.sun.mdm.sbme;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.config.java.context.AnnotationApplicationContext;
import org.springframework.context.ApplicationContext;

import com.sun.inti.components.component.ComponentException;
import com.sun.inti.components.component.ComponentManager;
import com.sun.inti.components.component.ComponentManagerFactory;
import com.sun.inti.components.util.Parametrizable;

public abstract class AnnotationContextStandardizerFactory  implements StandardizerFactory, ComponentManagerFactory {
	public static final String BEAN_NAME = "standardizer";
	
	private ApplicationContext context;
	
	public AnnotationContextStandardizerFactory() {
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		try {
			currentThread.setContextClassLoader(this.getClass().getClassLoader());
			this.context = new AnnotationApplicationContext(this.getConfigurationLocations());
		} catch (Throwable t) {
			throw new RuntimeException("Error creating application context: " + t, t);
		} finally {
			currentThread.setContextClassLoader(contextClassLoader);
		}
	}
	
	public Standardizer newStandardizer() {
		return (Standardizer) this.context.getBean(BEAN_NAME);
	}

    public ComponentManager newInstance(ClassLoader classLoader) throws ComponentException {
        return newInstance(classLoader, null);
    }
    public ComponentManager newInstance(ClassLoader classLoader, Map<String, Object> parameters) throws ComponentException {
        return new ComponentManager() {
            public Object getComponent(String componentName) throws ComponentException {
                Object component = context.getBean(componentName);
                
                if (BEAN_NAME.equals(componentName)) {
                    component = new StandardizerWrapper((Standardizer) component);
                }
                
                return component;
            }
        };
    }

    protected abstract String[] getConfigurationLocations();
    
    private class StandardizerWrapper implements com.sun.mdm.standardizer.Standardizer, Parametrizable {
        private Standardizer standardizer;
        private Map<String, Object> parameters;
        
        private StandardizerWrapper(Standardizer standardizer) {
            this.standardizer = standardizer;
        }

        public List<com.sun.mdm.standardizer.StandardizedRecord> standardize(String record) {
            List<com.sun.mdm.standardizer.StandardizedRecord> standardizedRecords = new ArrayList<com.sun.mdm.standardizer.StandardizedRecord>(1);
            com.sun.mdm.sbme.StandardizedRecord standardizedRecord = standardizer.standardize(record);
            standardizedRecords.add(new StandardizedRecordWrapper(standardizedRecord));
            return standardizedRecords;
        }

        public String normalize(String fieldName, String fieldValue) {
            return null;
        }
        
        public void setParameters(Map<String, Object> parameters) {
            this.parameters = parameters;
        }

        private class StandardizedRecordWrapper implements com.sun.mdm.standardizer.StandardizedRecord {
            private com.sun.mdm.sbme.StandardizedRecord record;
            
            private StandardizedRecordWrapper(com.sun.mdm.sbme.StandardizedRecord record) {
                this.record = record;
            }
            
            public Set<String> getFieldNames() {
                return record.propertyNames();
            }

            public List<StandardizedField> getFields(String fieldName) {
                List<String> fieldValues = new ArrayList<String>(1);
                fieldValues.add(record.get(fieldName));
                StandardizedField field = new StandardizedField(fieldValues, null, fieldName);
                List<com.sun.mdm.standardizer.StandardizedRecord.StandardizedField> fields = new ArrayList<com.sun.mdm.standardizer.StandardizedRecord.StandardizedField>(1);
                fields.add(field);
                return fields;
            }

            public String getFieldValue(String fieldName) {
                return record.get(fieldName);
            }

            public List<String> getFieldValues(String fieldName) {
                List<String> fieldValues = new ArrayList<String>(1);
                fieldValues.add(record.get(fieldName));
                return fieldValues;
            }

            public Map<String, Object> getParameters() {
                return parameters;
            }
            
            public String toString() {
                return record.toString();
            }
        }
    }
}
