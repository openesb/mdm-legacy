package com.sun.mdm.sbme.builder;

import com.sun.mdm.sbme.StandardizedRecord;

public interface StandardizedRecordFactory {
	public StandardizedRecord newStandardizedRecord();
}
