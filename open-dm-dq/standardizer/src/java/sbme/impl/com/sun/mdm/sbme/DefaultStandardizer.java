/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme;

import java.util.List;

import com.stc.sbme.api.SbmeStandRecord;
import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.builder.StandardizedRecordBuilder;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.Normalizer;
import com.sun.mdm.sbme.parser.Parser;
import com.sun.mdm.sbme.parser.Tokenization;
import com.sun.mdm.sbme.pattern.OutputPattern;
import com.sun.mdm.sbme.pattern.PatternFinder;

/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class DefaultStandardizer<I extends Enum<I>, O extends Enum<O>> implements Standardizer {
    private StringTransformer cleanser;

    private Parser parser;

    private Normalizer<I> normalizer;

    private PatternFinder<I, O> patternFinder;

    private StandardizedRecordBuilder<I, O> standardizedRecordBuilder;
    
    private String standardizationType = "";
    private String standardizationProperty = "";
    
    private StandardizedRecord defaultRecord(final String recordString) {
        StandardizedRecord record = new StandardizedRecord() {
            {
                set(standardizationProperty, recordString);
            }
            @Override
            public String getSignature() {
                return "";
            }
            @Override
            public String getType() {
                return standardizationType;
            }
            @Override
            public SbmeStandRecord toSbmeStandRecord() {
                return null;
            }
        };
        
        return record;
    }
    
    public StandardizedRecord standardize(String recordString) {
        recordString = this.cleanser.transform(recordString);

        if (recordString == null || recordString.trim().length() == 0) {
            return defaultRecord(recordString);
            // throw new IllegalArgumentException("Cannot standardize empty record :" + originalRecord);
        }

        try {
            final Tokenization tokenization = this.parser.parse(recordString);

            final List<NormalizedToken<I>> normalizedTokens = this.normalizer.normalize(tokenization);

            /*
             * Identify the best pattern (the order in the arrangement of fields)
             * for the record, and also the fields to be returned.
             */
            final List<? extends OutputPattern<I, O>> patternsFound = this.patternFinder.findPatterns(normalizedTokens);

            /*
             * Finally, identify the best pattern (the order in the arrangement of
             * fields) for the record, and also the fields to be returned.
             */
            final StandardizedRecord standardizedRecord = this.standardizedRecordBuilder.buildRecord(normalizedTokens, patternsFound);

            return standardizedRecord;
        } catch (Exception e) {
            return defaultRecord(recordString);
        }
    }

    public StringTransformer getCleanser() {
        return this.cleanser;
    }

    public void setCleanser(final StringTransformer cleanser) {
        this.cleanser = cleanser;
    }

    public Parser getParser() {
        return this.parser;
    }

    public void setParser(final Parser parser) {
        this.parser = parser;
    }

    public Normalizer<I> getNormalizer() {
        return this.normalizer;
    }

    public void setNormalizer(final Normalizer<I> normalizer) {
        this.normalizer = normalizer;
    }

    public PatternFinder<I, O> getPatternFinder() {
        return this.patternFinder;
    }

    public void setPatternFinder(final PatternFinder<I, O> patternFinder) {
        this.patternFinder = patternFinder;
    }

    public StandardizedRecordBuilder<I, O> getBuilder() {
        return this.standardizedRecordBuilder;
    }

    public void setBuilder(final StandardizedRecordBuilder<I, O> standardizedRecordBuilder) {
        this.standardizedRecordBuilder = standardizedRecordBuilder;
    }

    public String getStandardizationType() {
        return standardizationType;
    }

    public void setStandardizationType(String standardizationType) {
        this.standardizationType = standardizationType;
    }

    public String getStandardizationProperty() {
        return standardizationProperty;
    }

    public void setStandardizationProperty(String standardizationProperty) {
        this.standardizationProperty = standardizationProperty;
    }
}
