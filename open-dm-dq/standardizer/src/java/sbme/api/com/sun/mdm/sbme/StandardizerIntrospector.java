package com.sun.mdm.sbme;

import java.io.File;
import java.util.jar.JarFile;

// Deprecated
public interface StandardizerIntrospector {
	public DataTypeDescriptor[] setRepository(File repositoryDirectory) throws Exception;

	public DataTypeDescriptor[] importDirectory(File jarDirectory) throws Exception;
	
	public DataTypeDescriptor[] dataTypes() throws Exception;
	public DataTypeDescriptor getDataType(String dataTypeName) throws Exception;
	public VariantDescriptor getVariant(String dataTypeName, String variantName) throws Exception;

	public Descriptor importJar(JarFile jarFile) throws Exception;
	public DataTypeDescriptor importDataType(JarFile jarFile) throws Exception;
	public VariantDescriptor importVariant(JarFile jarFile) throws Exception;
	
	public DataTypeDescriptor removeDataType(String dataTypeName) throws Exception;
	public VariantDescriptor removeVariant(String dataTypeName, String variantName) throws Exception;
	
	public void takeSnapshot(File resourceJarDirectory) throws Exception;
	
	public void close() throws Exception;
}
