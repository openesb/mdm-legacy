package com.sun.mdm.sbme;


public interface VariantDescriptor extends Descriptor {
	public String getDataTypeName();
}