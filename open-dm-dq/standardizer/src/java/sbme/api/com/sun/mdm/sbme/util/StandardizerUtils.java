package com.sun.mdm.sbme.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import com.sun.mdm.sbme.StandardizerIntrospector;

public class StandardizerUtils {
	
	private static Logger logger = Logger.getLogger(StandardizerUtils.class.getName());
	
	public static StandardizerIntrospector getStandardizerIntrospector() throws Exception {
		return (StandardizerIntrospector) loadService(StandardizerIntrospector.class);
	}

	// TODO Inspect all occurrences of resource
	public static Object loadService(Class<?> serviceClass) throws Exception {
		String serviceQName = serviceClass.getName();
		InputStream is = StandardizerUtils.class.getClassLoader().getResourceAsStream("META-INF/services/" + serviceQName);
		if (is == null) {
			throw new IllegalArgumentException("Can't open: META-INF/services/" + serviceQName);
		}
		String line;
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		try {
			while ((line = in.readLine()) != null) {
				line = line.replaceAll("#.*", "").trim();
				if (line.length() > 0) {
					logger.info("Trying to load class '" + line + "'");
					Class<?> clazz = Class.forName(line);
					return clazz.newInstance();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.severe("Error processing service configuration file: " + e);
			throw e;
		} finally {
			try { in.close(); } catch (Exception x) {}
		}
		
		throw new IllegalArgumentException("No suitable implementation for service '" + serviceQName + "'");
	}

	public static void delete(File file) {
		if (file.isDirectory()) {
			for (File childFile: file.listFiles()) {
				delete(childFile);
			}
		}
		if (!file.delete()) {
			logger.warning("Can't delete: " + file);
		}
	}
	
	public static void copy(File input, File output) throws IOException {
		copy(new FileInputStream(input), new FileOutputStream(output));
	}

	private static final int BUFFER_SIZE = 4096;
	public static void copy(InputStream is, OutputStream os) throws IOException {
		int count;
		byte[] buffer = new byte[BUFFER_SIZE];
		while ((count = is.read(buffer)) != -1) {
			os.write(buffer, 0, count);
		}
		is.close();
		os.flush();
		os.close();
	}

	public static void extract(JarFile jarFile, File directory) throws Exception {
		Enumeration<JarEntry> i = jarFile.entries();
		while (i.hasMoreElements()) {
			JarEntry jarEntry = i.nextElement();
			if (!jarEntry.isDirectory()) {
				File file = new File(directory, jarEntry.getName());
				file.getParentFile().mkdirs();
				InputStream is = jarFile.getInputStream(jarEntry);
				FileOutputStream os = new FileOutputStream(file);
				copy(is, os);
			}
		}
	}
}
