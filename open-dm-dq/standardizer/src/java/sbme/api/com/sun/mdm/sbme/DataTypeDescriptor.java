package com.sun.mdm.sbme;


public interface DataTypeDescriptor extends Descriptor {
	public String[] getFieldNames();

	public VariantDescriptor[] variants();
	public VariantDescriptor getVariant(String variantName);
}