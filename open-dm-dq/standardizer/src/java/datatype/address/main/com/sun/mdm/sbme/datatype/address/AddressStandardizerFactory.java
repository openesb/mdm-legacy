package com.sun.mdm.sbme.datatype.address;

import com.sun.mdm.sbme.ApplicationContextStandardizerFactory;

public abstract class AddressStandardizerFactory extends ApplicationContextStandardizerFactory {
	protected String getType() {
		return "address";
	}
}
