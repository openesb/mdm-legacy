/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.builder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.format.EnumerationFormatter;
import com.sun.inti.components.string.format.IntegerFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

/**
 *
 */
public class MasterClueRegistry {
    private Map<Integer, Map<InputTokenType, MasterClue>> masterClues = new HashMap<Integer, Map<InputTokenType, MasterClue>>();
    private InputTokenTypeMapper inputTokenTypeMapper;
    
    public MasterClueRegistry() {}
    
    public MasterClueRegistry(Map<Integer, Map<InputTokenType, MasterClue>> masterClues, InputTokenTypeMapper inputTokenTypeMapper) {
    	this.masterClues = masterClues;
    	this.inputTokenTypeMapper = inputTokenTypeMapper;
    }
    
    public MasterClue lookup(final int id, InputTokenType type) {
        Map<InputTokenType, MasterClue> masterClues = this.masterClues.get(id);
        if (masterClues != null) {
        	if (inputTokenTypeMapper != null) {
        		type = inputTokenTypeMapper.map(type);
        	}
                        
            MasterClue masterClue = masterClues.get(type);
            return masterClue;
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
    	if (args.length != 3) {
    		usage();
    	}
    	    	
    	String inputFileName = args[0];
    	File inputFile = new File(inputFileName);
    	if (!inputFile.canRead()) {
    		usage("Can't find input file: " + inputFileName);
    	}
    	
    	String outputFileName = args[1];
    	File outputFile = new File(outputFileName);
        outputFile.getParentFile().mkdirs();
    	if (!outputFile.getParentFile().canWrite()) {
    		usage("Can't open output file: " + outputFileName);
    	}
    	
    	FieldDescriptor[] masterClueFieldDescriptors = null;
    	
    	if (!args[2].equals("fr")) {
    		masterClueFieldDescriptors = englishFieldDescriptors();
    	} else {
    		masterClueFieldDescriptors = frenchFieldDescriptors();
    	}
    	
        FixedFieldTokenizer masterClueTokenizer = new FixedFieldTokenizer();
        masterClueTokenizer.setFieldDescriptors(masterClueFieldDescriptors);

        PropertyDescriptor[] masterCluePropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("id", new IntegerFormatter(), null),  
                new PropertyDescriptor("fullName", null, null),  
                new PropertyDescriptor("standardAbbreviation", null, null),   
                new PropertyDescriptor("shortAbbreviation", null, null),
                new PropertyDescriptor("uspsAbbreviation", null, null),
                new PropertyDescriptor("type", new EnumerationFormatter(InputTokenType.class, true), null),
        };
        
        RecordPopulator[] masterClueRecordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(masterClueTokenizer, masterCluePropertyDescriptors),   
        };
        
        Reader masterClueReader = new FileReader(inputFile);
        Iterator<String> masterClueLineReader = new LineReader(masterClueReader, true);
        
        RecordSource masterClueRecordSource = 
            new DefaultRecordSource(masterClueRecordPopulators, masterClueLineReader);
        
        Record masterClueRecord;
        Map<Integer, Map<InputTokenType, MasterClue>> masterClues = new LinkedHashMap<Integer, Map<InputTokenType, MasterClue>>();
        while ((masterClueRecord = masterClueRecordSource.nextRecord()) != null) {
        	MasterClue masterClue = new MasterClue();
        	PropertyDescriptor.populate(masterClue, masterClueRecord);
        	Map<InputTokenType, MasterClue> clueMap = masterClues.get(masterClue.getId());
        	if (clueMap == null) {
        		clueMap = new LinkedHashMap<InputTokenType, MasterClue>();
        		masterClues.put(masterClue.getId(), clueMap);
        	}
        	clueMap.put(masterClue.getType(), masterClue);
        }
        
        FileOutputStream fos = new FileOutputStream(outputFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(masterClues);
        oos.flush();
        oos.close();
    }
    
    private static FieldDescriptor[] englishFieldDescriptors() {
    	return new FieldDescriptor[] {
                new FieldDescriptor(0, 4, true), // Id
                new FieldDescriptor(4, 23, true), // fullName
                new FieldDescriptor(28, 13, true), // standardAbbreviation
                new FieldDescriptor(42, 5, true), // shortAbbreviation
                new FieldDescriptor(48, 4, true), // uspsAbbreviation
                new FieldDescriptor(60, 2, true), // type
        };
    }
    
    private static FieldDescriptor[] frenchFieldDescriptors() {
    	return new FieldDescriptor[] {
                new FieldDescriptor(0, 4, true), // Id
                new FieldDescriptor(4, 33, true), // fullName
                new FieldDescriptor(38, 13, true), // standardAbbreviation
                new FieldDescriptor(52, 5, true), // shortAbbreviation
                new FieldDescriptor(58, 4, true), // uspsAbbreviation
                new FieldDescriptor(70, 2, true), // type
        };
    }
    
    private static void usage(String message) {
    	System.err.println(message);
    	usage();
    } 

    private static void usage() {
    	System.err.println("Usage: java " + MasterClueRegistry.class.getName() + " [-e|-f] inputFile outputFile");
    	System.exit(1);
    }
    
    public void setMasterClues(final Map<Integer, Map<InputTokenType, MasterClue>> clues) {
        this.masterClues = clues;
    }

	public void setInputTokenTypeMapper(InputTokenTypeMapper inputTokenTypeMapper) {
		this.inputTokenTypeMapper = inputTokenTypeMapper;
	}
}
