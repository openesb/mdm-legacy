package com.sun.mdm.sbme.datatype.address.configuration;

import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;

import com.sun.mdm.sbme.builder.OutputTokenTypeHandler;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption;
import com.sun.mdm.sbme.datatype.address.builder.CombinationOption;
import com.sun.mdm.sbme.datatype.address.builder.MasterClueRegistry;
import com.sun.mdm.sbme.datatype.address.builder.property.AddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.ContextAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.SimpleAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;

@Configuration
public abstract class BuilderConfiguration {
    final protected AddressLocale locale = locale();
    final protected ClueRegistry<InputTokenType> clueRegistry = clueRegistry();
    final protected MasterClueRegistry masterClueRegistry = masterClueRegistry();
    
    @ExternalBean
    public abstract AddressLocale locale();
    
    @ExternalBean
    public abstract ClueRegistry<InputTokenType> clueRegistry();
    
    @ExternalBean
    public abstract MasterClueRegistry masterClueRegistry();
    
    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> handler(AddressPropertyBuilder builder, OutputTokenType... types) {
    	builder.setLocale(locale); 
    	builder.setClueRegistry(clueRegistry); 
    	builder.setMasterClueRegistry(masterClueRegistry);
    	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
    }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> simpleHandler(String propertyName, OutputTokenType... types) {
    	SimpleAddressPropertyBuilder builder = new SimpleAddressPropertyBuilder();
    	builder.setLocale(locale); 
    	builder.setClueRegistry(clueRegistry); 
    	builder.setMasterClueRegistry(masterClueRegistry);
    	builder.setPropertyName(propertyName);
    	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
    }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> simpleHandler(SimpleAddressPropertyBuilder builder, String propertyName, OutputTokenType... types) {
    	builder.setLocale(locale); 
    	builder.setClueRegistry(clueRegistry); 
    	builder.setMasterClueRegistry(masterClueRegistry);
    	builder.setPropertyName(propertyName);
    	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
    }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> contextHandler(String propertyName, OutputTokenType... types) {
    	ContextAddressPropertyBuilder builder = new ContextAddressPropertyBuilder();
    	builder.setLocale(locale); 
    	builder.setClueRegistry(clueRegistry); 
    	builder.setMasterClueRegistry(masterClueRegistry);
    	builder.setContextPropertyName(propertyName);
    	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
    }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> simpleHandler(AbbreviationOption abbreviationOption, String propertyName, OutputTokenType... types) {
    	SimpleAddressPropertyBuilder builder = new SimpleAddressPropertyBuilder();
    	builder.setLocale(locale); 
    	builder.setClueRegistry(clueRegistry); 
    	builder.setMasterClueRegistry(masterClueRegistry);
    	builder.setPropertyName(propertyName);
    	builder.setAbbreviationOption(abbreviationOption);
    	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
    }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> simpleHandler(AbbreviationOption abbreviationOption, CombinationOption combinationOption, String propertyName,
            OutputTokenType... types) {
            	SimpleAddressPropertyBuilder builder = new SimpleAddressPropertyBuilder();
            	builder.setLocale(locale); 
            	builder.setClueRegistry(clueRegistry); 
            	builder.setMasterClueRegistry(masterClueRegistry);
            	builder.setPropertyName(propertyName);
            	builder.setAbbreviationOption(abbreviationOption);
            	builder.setCombinationOption(combinationOption);
            	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
            }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> contextHandler(AbbreviationOption abbreviationOption, String propertyName, OutputTokenType... types) {
    	ContextAddressPropertyBuilder builder = new ContextAddressPropertyBuilder();
    	builder.setLocale(locale); 
    	builder.setClueRegistry(clueRegistry); 
    	builder.setMasterClueRegistry(masterClueRegistry);
    	builder.setContextPropertyName(propertyName);
    	builder.setAbbreviationOption(abbreviationOption);
    	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
    }

    protected OutputTokenTypeHandler<InputTokenType, OutputTokenType> contextHandler(ContextAddressPropertyBuilder builder, CombinationOption combinationOption, String propertyName,
            OutputTokenType... types) {
            	builder.setLocale(locale); 
            	builder.setClueRegistry(clueRegistry); 
            	builder.setMasterClueRegistry(masterClueRegistry);
            	builder.setContextPropertyName(propertyName);
            	builder.setCombinationOption(combinationOption);
            	return new OutputTokenTypeHandler<InputTokenType, OutputTokenType>(builder, types);
            }

}
