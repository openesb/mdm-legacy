package com.sun.mdm.sbme.datatype.address.pattern;

import com.sun.inti.components.util.Abbreviated;

public enum AddressPatternClass implements Abbreviated {
	HOUSE("H"),
	BUILDING("B"),
	UNIT_WITHIN_STRUCTURE("W"),
	STREET_TYPE("T"),
	RURAL_ROUTE("R"),
	POST_OFFICE_BOX("P"),
	MOSTLY_NUMERIC("N"),
	EXTRA_INFORMATION("I");

	private String abbreviation;
	AddressPatternClass(String abbreviation) {
		this.abbreviation = abbreviation;
	}
    
    public String getAbbreviation() {
    	return this.abbreviation;
    }
}
