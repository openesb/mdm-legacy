package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.STANDARD_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.POST_OFFICE_BOX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public class EnglishPostOfficeBoxAddressPostprocessor extends AddressPostprocessor {
    // FIXME A table of clue word ids is needed
    private static final int BOX = 1;

    protected void postprocess(Address address, Record context) {
        final String Box = "Box";
        final InputTokenType inputTokenType = POST_OFFICE_BOX;

        // If bxi has a value and bxd is blank then place "box" in bxd
        String postOfficeBoxDescriptor = address.get(POST_OFFICE_BOX_DESCRIPTOR);
        String postOfficeBoxIdentifier = address.get(POST_OFFICE_BOX_IDENTIFIER);
        if (postOfficeBoxIdentifier != null && postOfficeBoxDescriptor == null) {
            this.loadField(address, POST_OFFICE_BOX_DESCRIPTOR, Box, BOX, inputTokenType, STANDARD_ABBREVIATION, SPACE);
        }
    }
}
