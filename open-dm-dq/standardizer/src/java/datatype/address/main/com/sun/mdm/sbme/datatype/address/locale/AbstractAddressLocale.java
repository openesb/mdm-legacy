/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.locale;

import java.util.List;

import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.datatype.address.pattern.AddressOutputPattern;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public abstract class AbstractAddressLocale implements AddressLocale {
    public boolean isCardinalPointAbbreviation(final String point) {
        for (final String cardinalPoint : this.getCardinalPoints()) {
            if (cardinalPoint.equalsIgnoreCase(point)) {
                return true;
            }
        }
        return false;
    }

    public void beforeParsing(final String string) {
    }

    public void afterParsing(final String string, final Tokenization tokenization) {
    }

    public void beforeNormalization(final Tokenization tokenization) {
    }

    public void afterNormalization(final Tokenization tokenization, final List<NormalizedToken<InputTokenType>> normalizedTokens) {
    }

    public void beforeFindingPatterns() {
    }

    public void afterFindingPatterns(List<AddressOutputPattern> patternsFound) {
    }

    public void beforeBuildingAddress(final List<NormalizedToken<InputTokenType>> tokensSt, final List<AddressOutputPattern> addressOutputPatterns) {
    }

    public void afterBuildingAddress(final List<NormalizedToken<InputTokenType>> tokensSt, final List<AddressOutputPattern> addressOutputPatterns, StandardizedRecord record) {
    }
}
