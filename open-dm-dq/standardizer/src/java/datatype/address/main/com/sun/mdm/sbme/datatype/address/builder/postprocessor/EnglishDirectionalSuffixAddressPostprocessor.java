package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import static com.sun.mdm.sbme.datatype.address.Address.ORIGINAL_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_SUFFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;

public class EnglishDirectionalSuffixAddressPostprocessor extends AddressPostprocessor {
	/*
     * Only needed if both a prefix and a suffix direction were loaded, and if
     * the suffix direction is n, s, w, or e AND the street name isn't ALL_FIELDS
     * numeric (this check saves UTAH addresses) and no wsi currently exists. If
     * so, the suffix direction is moved over to the within structure identifier
     * field. This will prevent the following address from having 2 directions:
     * 
     * 123 n main ave s: INPUT TOKENS - NU DR AU TY | DR OUTPUT TOKENS - HN PD
     * NA ST | SD PATTERN TYPES - H* | H+
     */
    protected void postprocess(Address address, Record context) {
        String directionalSuffix = context.getString("directionalSuffix");

        String originalStreetName = address.get(ORIGINAL_STREET_NAME);
    	String streetNamePrefixDirection = address.get(STREET_NAME_PREFIX_DIRECTION);
    	String streetNameSuffixDirection = address.get(STREET_NAME_SUFFIX_DIRECTION);

        if (streetNamePrefixDirection != null &&
        	streetNameSuffixDirection != null &&
            (originalStreetName == null || !isAllDigits(originalStreetName)))
        {
            // FIXME This should be this.locale.isCardinalPoint(directionalSuffix)
            if ("N".equalsIgnoreCase(directionalSuffix) ||
                "S".equalsIgnoreCase(directionalSuffix) ||
                "E".equalsIgnoreCase(directionalSuffix) ||
                "W".equalsIgnoreCase(directionalSuffix))
            {
            	StringBuilder wsi = new StringBuilder(address.get(WITHIN_STRUCTURE_IDENTIFIER, ""));
                if (wsi.length() != 0) {
                    wsi.append('*');
                }
                wsi.append(directionalSuffix);
                address.set(WITHIN_STRUCTURE_IDENTIFIER, wsi.toString());
                
                address.set(STREET_NAME_SUFFIX_DIRECTION, "");
            }
        }
    }
}
