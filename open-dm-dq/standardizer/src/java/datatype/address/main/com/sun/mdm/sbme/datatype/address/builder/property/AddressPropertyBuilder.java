/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 
package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.NUMERIC_VALUE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ORDINAL_TYPE;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.builder.property.StandardizedRecordPropertyBuilder;
import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.builder.AddressBuildingComponent;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

/**
 *
 */
public abstract class AddressPropertyBuilder extends AddressBuildingComponent implements StandardizedRecordPropertyBuilder<InputTokenType, OutputTokenType> {
    public void buildProperty(StandardizedRecord standardizedRecord,
            				  NormalizedToken<InputTokenType> normalizedToken,
            				  InputTokenType inputTokenType,
            				  OutputTokenType outputTokenType,
            				  Record context)
    {
    	this.buildProperty((Address) standardizedRecord, normalizedToken, inputTokenType, outputTokenType, context);
    }

	protected abstract void buildProperty(Address address,
            							  NormalizedToken<InputTokenType> normalizedToken,
            							  InputTokenType inputTokenType,
            							  OutputTokenType outputTokenType,
            							  Record context);

    /*
     * NOTE: when loading certain fields, you must check to see if a
     * fraction was earlier combined with the field. If so, remove
     * it and load it in its appropriate field.
     */
     // FIXME checkForSpelledOutNumber is specific to the US locale
     protected void checkForSpelledOutNumber(final NormalizedToken<InputTokenType> normalizedToken) {
         if (normalizedToken.getClueWords().size() > 1 &&
             (normalizedToken.getClueWords().get(1).getType() == NUMERIC_VALUE ||
              normalizedToken.getClueWords().get(1).getType() == ORDINAL_TYPE))
         {
             Clue<InputTokenType> clue  = getClueRegistry().lookup(normalizedToken.getImage());
             if (clue != null) {
                 normalizedToken.setImage(clue.getTranslation());
             }
         }
     }
     
     // FIXME fractionWithinNumber is specific to the US locale
     private static final Pattern fractionPattern = Pattern.compile("([0-9]+)\\s+([0-9]/[0-9]+)");
     protected static String[] fractionWithinNumber(final NormalizedToken<InputTokenType> token) {
         Matcher matcher = fractionPattern.matcher(token.getImage());
         if (matcher.find()) {
             MatchResult result = matcher.toMatchResult();
             return new String[] {
                     token.getImage().substring(result.start(1), result.end(1)),
                     token.getImage().substring(result.start(2), result.end(2))
             };
         }
         return null;
     }
}
