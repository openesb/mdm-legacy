/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.parser;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.parser.Parser;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class AddressParser implements Parser {
    private AddressLocale locale;

    public AddressParser() {}

    public AddressParser(AddressLocale locale) {
    	this.locale = locale;
    }

    /**
     * This method is sent a cleansed address string. Words are delineated by
     * spaces, special characters, or changes from alpha to digit, or vice
     * versa. The one exception to the last statement is "1st" and "3rd". For
     * example, if "123 1st" is the incoming address, then we are pretty sure
     * that "1st" is the street name, hence we never want to split "1" and "st",
     * which could subsequently identify "st" as a type.
     * 
     *            the input string
     *            an instance of the AddressVariables class
     */
    public Tokenization parse(final String record) {
        int startIndex = 0;
        final Tokenization tokenization = new Tokenization();
        for (int i = 1; i < record.length(); i++) {
            char currentChar = record.charAt(i);

            final boolean alphaNum = Character.isLetter(record.charAt(i - 1)) && Character.isDigit(currentChar);
            final boolean numAlpha = Character.isDigit(record.charAt(i - 1)) && Character.isLetter(currentChar);

            if (currentChar == ' ' || alphaNum ||
               (numAlpha && !this.locale.isOrdinalPrefix(record.substring(i - 1)))) // European language-specific
            {
                if (startIndex < i) {
                    tokenization.addToken(record.substring(startIndex, i), alphaNum || numAlpha);
                }

                startIndex = i;
                if (currentChar == ' ') {
                    startIndex++;
                }
            }
        }

        tokenization.addToken(record.substring(startIndex), false);

        return tokenization;
    }

    public AddressLocale getLocale() {
        return this.locale;
    }

    public void setLocale(final AddressLocale locale) {
        this.locale = locale;
    }
}
