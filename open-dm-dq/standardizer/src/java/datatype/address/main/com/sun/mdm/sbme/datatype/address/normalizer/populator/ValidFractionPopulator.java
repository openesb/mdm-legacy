/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.populator;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ALPHA_ONE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.NUMERIC_FRACTION;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.NUMERIC_VALUE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import java.util.List;
import java.util.regex.Pattern;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.populator.NonNullCluePopulator;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class ValidFractionPopulator extends NonNullCluePopulator<InputTokenType> {
    private static final Pattern allDigits = Pattern.compile("^[0-9]*$");

    protected int doPopulate(final List<NormalizedToken<InputTokenType>> tokens, Clue<InputTokenType> clue, Tokenization tokenization, int startIndex, int endIndex, final Record contextRecord) {
        if (validFraction(startIndex,
                          startIndex + 1,
                          tokens.get(0).getClueWordCount() > 0,
                          tokens.size() - 1 == 0 ? null : tokens.get(tokens.size() - 2),
                          clue.getWords().get(0).getType(),
                          tokenization))
        {
            /*
             * Combine the fraction with the previous word in the word array
             * before placing it into the inputImage
             */
            final StringBuilder sb = new StringBuilder();
            sb.append(tokenization.getToken(startIndex - 1).getImage());
            sb.append(' ');
            sb.append(clue.getTranslation());
            this.setImage(tokens, sb.toString());

            this.addClueWord(tokens, NUMERIC_VALUE);

            return 4;
        }
        return 0;
    }

    /**
     * Receives the variables.wordArray (positioned at the current word), the
     * previously determined token struct, and the current word number, and will
     * return TRUE if: 1. The current word is a fraction, 2. The previous word
     * is a numeric, and 3. The following word is NOT a TY or A1. 4. The current
     * word number is not 1, ie. "1/2 street" has variables.NOTHING to combine
     * 1/2 with.
     * 
     * EX: 123 1/2 main st. will be combined to: NU - 1231/2 AU - main TY - st
     * This is done because for every NU... pattern, there is a NU FC ...
     * pattern. This eliminates the second set of patterns.
     */
    private static boolean validFraction(final int index,
                                         final int currentWordIndex,
                                         final boolean clueWordTokenLoaded,
                                         final NormalizedToken<InputTokenType> normalizedToken,
                                         final InputTokenType tokenType,
                                         final Tokenization tokenization)
    {
        if (currentWordIndex <= 1 || tokenType != NUMERIC_FRACTION) {
            return false;
        }

        if (!allDigits(tokenization.getToken(index - 1).getImage())) {
            return false;
        }

        // FIXME clueWordTokenLoaded forces List<NormalizedToken> tokens to be
        // an argument to ClueWordPopulator.populate
        if (clueWordTokenLoaded) {
            if (normalizedToken.getClueWord(0).getType() == STREET_TYPE || normalizedToken.getClueWord(0).getType() == ALPHA_ONE) {
                return false;
            }
        }
        return true;
    }

    private static boolean allDigits(final String string) {
        return allDigits.matcher(string).find();
    }
}
