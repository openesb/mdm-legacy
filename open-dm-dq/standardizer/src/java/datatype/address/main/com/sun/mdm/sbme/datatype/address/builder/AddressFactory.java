package com.sun.mdm.sbme.datatype.address.builder;

import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.builder.StandardizedRecordFactory;
import com.sun.mdm.sbme.datatype.address.Address;

public class AddressFactory implements StandardizedRecordFactory {
	public StandardizedRecord newStandardizedRecord() {
		return new Address();
	}
}
