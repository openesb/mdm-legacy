package com.sun.mdm.sbme.datatype.address.configuration;

import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.address.builder.EnglishInputTokenTypeMapper;
import com.sun.mdm.sbme.datatype.address.builder.InputTokenTypeMapper;

public abstract class EnglishMasterClueRegistryConfiguration extends MasterClueRegistryConfiguration {
	public FieldDescriptor[] getFieldDescriptors() {
		return new FieldDescriptor[] {
                new FieldDescriptor(0, 4, true), // Id
                new FieldDescriptor(4, 23, true), // fullName
                new FieldDescriptor(28, 13, true), // standardAbbreviation
                new FieldDescriptor(42, 5, true), // shortAbbreviation
                new FieldDescriptor(48, 4, true), // uspsAbbreviation
                new FieldDescriptor(60, 2, true), // type
        };
	}

	protected InputTokenTypeMapper getInputTokenTypeMapper() {
		return new EnglishInputTokenTypeMapper();
	}
}
