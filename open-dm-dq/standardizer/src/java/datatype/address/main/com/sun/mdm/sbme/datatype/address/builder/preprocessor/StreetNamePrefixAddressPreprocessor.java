/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 
package com.sun.mdm.sbme.datatype.address.builder.preprocessor;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.GENERIC_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.HIGHWAY_ROUTE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_TYPE;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.builder.preprocessor.StandardizedRecordBuilderPreprocessor;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.pattern.OutputPattern;

/**
 *
 */
public class StreetNamePrefixAddressPreprocessor implements StandardizedRecordBuilderPreprocessor<InputTokenType, OutputTokenType> {
    private static final int BUS_ROUTE = 33;

    public void preprocess(List<NormalizedToken<InputTokenType>> normalizedTokens, List<?extends OutputPattern<InputTokenType, OutputTokenType>> addressOutputPatterns, Record context) {
        for (OutputPattern<InputTokenType, OutputTokenType> addressOutputPattern: addressOutputPatterns) {
            final List<InputTokenType> inputTokenTypes = addressOutputPattern.getInputTokenTypes();
            final List<OutputTokenType> outputTokenTypes = addressOutputPattern.getOutputTokenTypes();

            for (int j = 0; j <= addressOutputPattern.getEnd() - addressOutputPattern.getBegin(); j++) {
                InputTokenType inputTokenType = inputTokenTypes.get(j);
                OutputTokenType outputTokenType = outputTokenTypes.get(j);
                
                final int tokenIndex = addressOutputPattern.getBegin() + j - 1;
                final NormalizedToken<InputTokenType> normalizedToken = normalizedTokens.get(tokenIndex);
                final int clueWordId = normalizedToken.getClueWordId(inputTokenType);

                if (outputTokenType == STREET_NAME_PREFIX_TYPE) {
                    if (!(clueWordId == BUS_ROUTE && inputTokenType == HIGHWAY_ROUTE)) {
                        /*
                         * checkPTtoNA will change:
                         *      IMAGE: ST ST
                         *      CLUE WORD ID: 263 79
                         *      INPUT TOKEN: TY -to- AU
                         *      OUTPUT TOKEN: PT NA
                         */
                        // FIXME What do "LE" and "LA" mean?
                        if ("ST".equals(normalizedToken.getImage()) ||
                            "LE".equals(normalizedToken.getImage()) ||
                            "LA".equals(normalizedToken.getImage()))
                        {
                            addressOutputPattern.setInputTokenType(j, GENERIC_WORD);
                            addressOutputPattern.setOutputTokenType(j, STREET_NAME);
                        }
                    }
                }
            }
        }
    }
}
