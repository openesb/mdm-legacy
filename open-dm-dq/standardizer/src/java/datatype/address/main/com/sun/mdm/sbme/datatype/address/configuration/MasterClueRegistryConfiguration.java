package com.sun.mdm.sbme.datatype.address.configuration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.format.EnumerationFormatter;
import com.sun.inti.components.string.format.IntegerFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.address.builder.InputTokenTypeMapper;
import com.sun.mdm.sbme.datatype.address.builder.MasterClue;
import com.sun.mdm.sbme.datatype.address.builder.MasterClueRegistry;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public abstract class MasterClueRegistryConfiguration {
	public MasterClueRegistry masterClueRegistry() {
        FixedFieldTokenizer masterClueTokenizer = new FixedFieldTokenizer();
        masterClueTokenizer.setFieldDescriptors(this.getFieldDescriptors());

        PropertyDescriptor[] masterCluePropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("id", new IntegerFormatter(), null),  
                new PropertyDescriptor("fullName", null, null),  
                new PropertyDescriptor("standardAbbreviation", null, null),   
                new PropertyDescriptor("shortAbbreviation", null, null),
                new PropertyDescriptor("uspsAbbreviation", null, null),
                new PropertyDescriptor("type", new EnumerationFormatter(InputTokenType.class, true), null),
        };
        
        RecordPopulator[] masterClueRecordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(masterClueTokenizer, masterCluePropertyDescriptors),   
        };
        
        String masterClueFile = "META-INF/mdm/sbme/address/" + this.getVariantName() + "/masterClues.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(masterClueFile);
        
        if (is == null) {
        	throw new IllegalArgumentException("Could not load master clue file: " + masterClueFile);
        }
        
        Reader masterClueReader = new InputStreamReader(is);
        Iterator<String> masterClueLineReader = new LineReader(masterClueReader, true);
        
        RecordSource masterClueRecordSource = 
            new DefaultRecordSource(masterClueRecordPopulators, masterClueLineReader);
        
        Record masterClueRecord;
        Map<Integer, Map<InputTokenType, MasterClue>> masterClues = new LinkedHashMap<Integer, Map<InputTokenType, MasterClue>>();
        while ((masterClueRecord = masterClueRecordSource.nextRecord()) != null) {
        	MasterClue masterClue = new MasterClue();
        	PropertyDescriptor.populate(masterClue, masterClueRecord);
        	Map<InputTokenType, MasterClue> clueMap = masterClues.get(masterClue.getId());
        	if (clueMap == null) {
        		clueMap = new LinkedHashMap<InputTokenType, MasterClue>();
        		masterClues.put(masterClue.getId(), clueMap);
        	}
        	clueMap.put(masterClue.getType(), masterClue);
        }
        
        return new MasterClueRegistry(masterClues, this.getInputTokenTypeMapper());
	}
	
	protected abstract String getVariantName();
	protected abstract FieldDescriptor[] getFieldDescriptors();
	protected abstract InputTokenTypeMapper getInputTokenTypeMapper();
}
