package com.sun.mdm.sbme.datatype.address.configuration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.format.BooleanFormatter;
import com.sun.inti.components.string.format.EnumerationFormatter;
import com.sun.inti.components.string.format.EnumerationListFormatter;
import com.sun.inti.components.string.format.IntegerFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.address.pattern.AddressPattern;
import com.sun.mdm.sbme.datatype.address.pattern.AddressPatternClass;
import com.sun.mdm.sbme.datatype.address.pattern.AddressScoringOption;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.PatternRegistry;

public abstract class PatternRegistryConfiguration {
	public PatternRegistry patternRegistry() {

    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 36, true), // Input token types
                new FieldDescriptor(37, 35, true), // Example
        };
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("inputTokenTypes", new EnumerationListFormatter(InputTokenType.class, true), null),
                new PropertyDescriptor("example", null, null),
        };
        
        FixedFieldTokenizer secondTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] secondFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 36, true), // Output token types
                new FieldDescriptor(37, 1, true), // Pattern class
                new FieldDescriptor(38, 1, true), // Scoring option
                new FieldDescriptor(39, 3, true), // Priority
                new FieldDescriptor(57, 1, true), // Usage flag
                new FieldDescriptor(58, 1, true), // Exclude option
        };
        secondTokenizer.setFieldDescriptors(secondFieldDescriptors);
        PropertyDescriptor[] secondPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("outputTokenTypes", new EnumerationListFormatter(OutputTokenType.class, true), null),  
                new PropertyDescriptor("patternClass", new EnumerationFormatter(AddressPatternClass.class, true), null),  
                new PropertyDescriptor("scoringOption", new EnumerationFormatter(AddressScoringOption.class, true), null),   
                new PropertyDescriptor("priority", new IntegerFormatter(), null),   
                new PropertyDescriptor("usageFlag", null, ""),
                new PropertyDescriptor("exclude", new BooleanFormatter("X", ""), ""),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
                new DefaultRecordPopulator(secondTokenizer, secondPropertyDescriptors), 
        };
        
        String patternFile = "META-INF/mdm/sbme/address/" + this.getVariantName() + "/patterns.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(patternFile);
        
        if (is == null) {
        	throw new IllegalArgumentException("Could not load pattern data file: " + patternFile);
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<List<InputTokenType>, List<Record>> recordMap = new LinkedHashMap<List<InputTokenType>, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
            if (!record.getBoolean("exclude")) {
                @SuppressWarnings("unchecked")
                List<InputTokenType> inputTokenTypes = (List<InputTokenType>) record.get("inputTokenTypes");
                List<Record> recordList = recordMap.get(inputTokenTypes);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(inputTokenTypes, recordList);
                }
                recordList.add(record);
            }
        }

        Map<List<InputTokenType>, AddressPattern> patternMap = new LinkedHashMap<List<InputTokenType>, AddressPattern>();
        for (List<InputTokenType> inputTokenTypes: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(inputTokenTypes);
            for (Record candidateRecord: recordList) {
                record = candidateRecord;
                if ("".equals(record.getString("usageFlag"))) {
                    break;
                }
            }
            @SuppressWarnings("unchecked")
            List<OutputTokenType> outputTokenTypes = (List<OutputTokenType>) record.get("outputTokenTypes");
            int priority = record.getInt("priority");
            AddressPatternClass addressPatternClass = (AddressPatternClass) record.get("patternClass");
            AddressScoringOption addressScoringOption = (AddressScoringOption) record.get("scoringOption");

            AddressPattern addressPattern = new AddressPattern(inputTokenTypes, outputTokenTypes, priority, addressPatternClass, addressScoringOption);
            patternMap.put(inputTokenTypes, addressPattern);
        }
        
        return new PatternRegistry(patternMap);
	}
	
	protected abstract String getVariantName();
}
