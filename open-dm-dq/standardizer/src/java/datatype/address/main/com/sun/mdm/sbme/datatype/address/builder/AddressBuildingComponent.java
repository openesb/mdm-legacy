package com.sun.mdm.sbme.datatype.address.builder;

import java.util.logging.Logger;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;

public abstract class AddressBuildingComponent {
    private AddressLocale locale;
    private ClueRegistry<InputTokenType> clueRegistry;
    private MasterClueRegistry masterClueRegistry;

    protected final Logger logger = Logger.getLogger(this.getClass().getName());
    
    protected void loadField(final Address address,
                             final OutputTokenType outputTokenType,
                             final String image,
                             final int clueWordId,
                             final InputTokenType inputTokenType,
                             final AbbreviationOption abbreviationOption,
                             final CombinationOption combinationOption)
            throws MdmStandardizationException
    {
    	this.loadField(address, outputTokenType.getAlias(), image, clueWordId, inputTokenType, abbreviationOption, combinationOption);
    }
    
    protected void loadField(final Address address,
                             final String propertyName,
                             final String image,
                             final int clueWordId,
                             final InputTokenType inputTokenType,
                             final AbbreviationOption abbreviationOption,
                             final CombinationOption combinationOption)
            throws MdmStandardizationException
    {
        final StringBuilder field = new StringBuilder();
        final String propertyValue = address.get(propertyName);
        if (propertyValue != null) {
        	field.append(propertyValue);
        }
        this.loadField(field, image, clueWordId, inputTokenType, abbreviationOption, combinationOption);
        address.set(propertyName, field.toString());
    }
    
    protected void loadField(final StringBuilder field,
                             final String image,
                             final int clueWordId,
                             final InputTokenType inputTokenType,
                             final AbbreviationOption abbreviationOption,
                             final CombinationOption combinationOption)
            throws MdmStandardizationException
	{
        // If the clue word id for the token is 0 (ex: AU's, NU's, etc), then
        // the master clues table cannot be searched
        String appendedField = "";
        if (clueWordId == 0) {
            appendedField = image;
        } else {
            // If the clue type is AU and the clue word id non-zero, then this
            // could be possibly consecutive AUs that were combined. In this case,
            // you want to get the abbreviations from the master clue table of all
            // words in the string...
            // eg: 123 mountain beach rd 123 mt bch rd
            final MasterClue masterClue = this.masterClueRegistry.lookup(clueWordId, inputTokenType);
            if (masterClue == null) {
                logger.severe("Master entry wasn't found for clue word id '" + clueWordId + "' and input token type '" + inputTokenType + "'");
                throw new MdmStandardizationException("Master entry wasn't found for clue word id '" + clueWordId + "' and input token type '" + inputTokenType + "'");
            }

            // If abbrevOpt is 'E', 'F', 'S', 'H', or 'U' then get the abbrev.
            // from the master clue table. EXCEPTION: if using abbrev opt 'F',
            // then dont expand directions. Example: "E street" was incorrectly
            // matching to "East street"
            switch (abbreviationOption) {
                case FULL_NAME:
                    appendedField = masterClue.getFullName();
                    break;
                case STANDARD_ABBREVIATION:
                    appendedField = masterClue.getStandardAbbreviation();
                    break;
                case SHORT_ABBREVIATION:
                    appendedField = masterClue.getShortAbbreviation();
                    break;
                case USPS_ABBREVIATION:
                    appendedField = masterClue.getUspsAbbreviation();
                    break;
                case EXPANDED:
                    appendedField = masterClue.getUspsAbbreviation();
                    break;
                default:
                    throw new IllegalStateException("Unknown abbreviation option: " + abbreviationOption);
            }
        }

        // if combine == SPACE then add a space between concatenations,
        // else if combine == SQUASH, then add nothing,
        // else if combine == ASTERISK, then add an ASTERISK,
        // else if combine == DASH then add a DASH ONLY WHEN concatenating
        // integers, otherwise add nothing.
        final int fieldLength = field.length();
        if (fieldLength != 0) {
            switch (combinationOption) {
                case SQUASH:
                    break;
                case SPACE:
                    field.append(' ');
                    break;
                case ASTERISK:
                    field.append('*');
                    break;
                case DASH:
                    if (Character.isDigit(field.charAt(fieldLength - 1)) && Character.isDigit(appendedField.charAt(0))) {
                        if (Character.isDigit(field.charAt(fieldLength - 1)) && Character.isDigit(appendedField.charAt(0))) {
                            field.append('-');
                        }
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown combination option: " + combinationOption);
            }
        }
        field.append(appendedField);
    }

    public AddressLocale getLocale() {
        return locale;
    }

    public void setLocale(AddressLocale locale) {
        this.locale = locale;
    }

    public ClueRegistry<InputTokenType> getClueRegistry() {
        return clueRegistry;
    }

    public void setClueRegistry(ClueRegistry<InputTokenType> clueRegistry) {
        this.clueRegistry = clueRegistry;
    }

    public MasterClueRegistry getMasterClueRegistry() {
        return masterClueRegistry;
    }

    public void setMasterClueRegistry(MasterClueRegistry masterClueRegistry) {
        this.masterClueRegistry = masterClueRegistry;
    }
}
