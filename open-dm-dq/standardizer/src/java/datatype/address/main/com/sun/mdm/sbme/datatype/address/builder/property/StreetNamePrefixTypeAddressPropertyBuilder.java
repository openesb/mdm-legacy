package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.EXTENSION;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.HIGHWAY_ROUTE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_EXTENSION_INDEX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_TYPE;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class StreetNamePrefixTypeAddressPropertyBuilder extends AddressPropertyBuilder {
    // FIXME A table of clue word ids is needed
    private static final int BUS_ROUTE = 33;
    private static final int ROUTE_NUMBER = 227;
    private static final int BUSINESS = 6;

    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);

        /*
         * If the prefix type is Bsrt (or some other deviation), then the type is changed to rt and "business" is placed in the
         * extension field.
         */
        if (clueWordId == BUS_ROUTE && inputTokenType == HIGHWAY_ROUTE) {
            this.loadField(address, STREET_NAME_EXTENSION_INDEX, " ", BUSINESS, EXTENSION, getLocale().getAbbreviationOption(), SPACE);
            this.loadField(address, STREET_NAME_PREFIX_TYPE, " ", ROUTE_NUMBER, HIGHWAY_ROUTE, getLocale().getAbbreviationOption(), SPACE);
        } else {
            this.loadField(address, STREET_NAME_PREFIX_TYPE, normalizedToken.getImage(), clueWordId, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
        }
    }
}
