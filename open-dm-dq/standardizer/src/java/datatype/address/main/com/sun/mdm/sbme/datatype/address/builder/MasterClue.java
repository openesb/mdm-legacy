/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.builder;

import java.io.Serializable;

import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

/**
 *
 */
public class MasterClue implements Serializable {
    private int id;
    private String fullName;
    private String standardAbbreviation;
    private String shortAbbreviation;
    private String uspsAbbreviation;
    private InputTokenType type;

    public MasterClue() {}
    
    public MasterClue(int id, String fullName, String standardAbbreviation, String shortAbbreviation, String uspsAbbreviation, InputTokenType type) {
    	this.id = id;
    	this.fullName = fullName;
    	this.standardAbbreviation = standardAbbreviation;
    	this.shortAbbreviation = shortAbbreviation;
    	this.uspsAbbreviation = uspsAbbreviation;
    	this.type = type;
    }
    
	public String toString() {
    	return this.id + "|" +
    		   this.fullName + "|" +
    		   this.type;
    }

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
        return this.fullName;
    }
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public String getStandardAbbreviation() {
        return this.standardAbbreviation;
    }

    public void setStandardAbbreviation(final String standardAbbreviation) {
        this.standardAbbreviation = standardAbbreviation;
    }

    public String getShortAbbreviation() {
        return this.shortAbbreviation;
    }
    public void setShortAbbreviation(final String shortAbbreviation) {
        this.shortAbbreviation = shortAbbreviation;
    }

    public String getUspsAbbreviation() {
        return this.uspsAbbreviation;
    }

    public void setUspsAbbreviation(final String uspsAbbreviation) {
        this.uspsAbbreviation = uspsAbbreviation;
    }

    public InputTokenType getType() {
        return this.type;
    }

    public void setType(InputTokenType type) {
        this.type = type;
    }
}
