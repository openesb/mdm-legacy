/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.populator;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ALPHA_ONE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ALPHA_TWO;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.GENERIC_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.NUMERIC_VALUE;

import java.util.List;
import java.util.regex.Pattern;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.populator.NullCluePopulator;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class DefaultNullCluePopulator extends NullCluePopulator<InputTokenType> {
    private static final Pattern twoAlphas = Pattern.compile("^\\p{Alpha}{2}$");
    private static final Pattern numericValue = Pattern.compile("^[0-9]+([-\\00d2][0-9]+)*$");

    protected int doPopulate(final List<NormalizedToken<InputTokenType>> normalizedTokens, final Tokenization tokenization, final int startIndex, final int endIndex, final Record contextRecord) {
    	final String image = tokenization.concatenateTokens(startIndex, endIndex);
    	
        if (twoAlphas.matcher(image).find()) {
            this.addClueWord(normalizedTokens, ALPHA_TWO);
        }

        this.addClueWord(normalizedTokens, tokenifyWord(image));

        return 1;
    }

    private static InputTokenType tokenifyWord(final String word) throws MdmStandardizationException {
        final int len = word.length();
        if (word.length() == 0) {
            throw new MdmStandardizationException("Empty word, cannot determine token type");
        }

        char currentChar = word.charAt(0);

        if (len == 1 && (Character.isLetter(currentChar) || currentChar == '-' || currentChar == '\u00d2')) {
            return ALPHA_ONE;
        }
        
        if (numericValue.matcher(word).find()) {
        	return NUMERIC_VALUE;
        }
        
        return GENERIC_WORD;
    }
}
