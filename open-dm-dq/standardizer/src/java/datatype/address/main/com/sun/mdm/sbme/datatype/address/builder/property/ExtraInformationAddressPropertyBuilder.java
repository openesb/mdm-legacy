package com.sun.mdm.sbme.datatype.address.builder.property;


import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.EXTRA_INFORMATION;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class ExtraInformationAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        this.loadField(address, EXTRA_INFORMATION, normalizedToken.getImage(), 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
    }
}
