/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.populator;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ORDINAL_TYPE;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.ClueWord;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.populator.AbstractPopulator;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class DefaultNonNullCluePopulator extends AbstractPopulator<InputTokenType> {
    public int populate(final List<NormalizedToken<InputTokenType>> tokens, Clue<InputTokenType> clue, Tokenization tokenization, int startIndex, int endIndex, final Record contextRecord) {
        // FIXME This populator never assigns a 9999 clue id
        /*
         * Load the token struct with all tokens returned from the clue word
         * abbreviation struct, EXCEPT if any clue word type is OT (we already
         * handled those cases). Also, if the current clue has only partially
         * acceptable tokens, then make the clue word id's for the invalid one
         * be 9999.
         */
        for (final ClueWord<InputTokenType> clueWord : clue.getWords()) {
            /*
             * Continue if an OT has been found, because we've already handled
             * them, UNLESS the variable oTfound is TRUE (meaning we've found a
             * valid OT for this clue and it was already combined, hence, we
             * don't need the OT token).
             */
            if (clueWord.getType() == ORDINAL_TYPE && contextRecord.getBoolean("ordinalTypeFound")) {
                continue;
            }

            this.addClueWord(tokens, clueWord);
        }

        return endIndex - startIndex;
    }
}
