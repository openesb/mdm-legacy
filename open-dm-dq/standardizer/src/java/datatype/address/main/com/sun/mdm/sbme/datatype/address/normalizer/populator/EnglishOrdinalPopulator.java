/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.populator;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.GENERIC_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ORDINAL_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.populator.ClueIgnoringPopulator;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class EnglishOrdinalPopulator extends ClueIgnoringPopulator<InputTokenType> {
    protected int doPopulate(final List<NormalizedToken<InputTokenType>> tokens, Tokenization tokenization, int startIndex, int endIndex, final Record contextRecord) {
        final NormalizedToken<InputTokenType> token = tokens.size() == 1 ? null : tokens.get(tokens.size() - 2);
        final boolean tokenLoaded = tokens.get(0).getClueWords().size() > 0;

        if (startIndex > 0 && this.isValidOrdinalTypeCombo(startIndex, token, tokenLoaded, tokenization)) {    	
            contextRecord.set("ordinalTypeFound", true);

            /*
             * Combine the OT (previous word + current word) before placing it
             * into the inputImage
             */
            this.setImage(tokens,
                          tokenization.getToken(startIndex - 1).getImage() +
                          tokenization.getToken(startIndex).getImage());

            this.addClueWord(tokens, GENERIC_WORD);
            this.addClueWord(tokens, ORDINAL_TYPE);

            return 2;
        }
        return 0;
    }

    /**
     * Receives variables.wordArray (positioned at the current word), the
     * previously determined token struct, and the clues table struct for the
     * current word. Returns true if: 1. The current word is an ordinal type 2.
     * The previous word in the string is a matching numeric (eg. 1 for st, 2
     * for nd, 3 for rd, and 0,4-9 for th). 3. If the current word is st or rd,
     * then the next word in the string must be a TY For example: 123 1 st ave;
     * if st is the current word being processed then we know it's an ordinal
     * type.
     */
    // FIXME Method isValidOrdinalTypeCombo ignores the 2nd ordinal altogether
    // This may be so because "st" can be confused with "street" and
    // "rd" can be confused with road.
    // In both cases, this logic is English-specific
    private static final String[] ordinalSuffixes = new String[] {
    	"ST", "ND", "RD", "TH"
    };
    private boolean isValidOrdinalTypeCombo(final int index, final NormalizedToken<InputTokenType> token, final boolean tokenLoaded, final Tokenization tokenization) {
        final String st = "st";
        final String rd = "rd";

        // Make sure the current word is ST, ND, RD, or TH
        boolean isOrdinalSuffix = false;
        for (String suffix: ordinalSuffixes) {
        	if (suffix.equals(tokenization.getToken(index).getImage())) {
        		isOrdinalSuffix = true;
        		break;
        	}
        }
        if (!isOrdinalSuffix) {
        	return false;
        }

        // Check to see if the previous word is all digits
        if (!allDigits(tokenization.getToken(index - 1).getImage())) {
            return false;
        }

        /*
         * Examine the previous word to see what its OT complement is. If the
         * word is "TH" or "ND", then change the OT to match the number in the
         * previous, if necessary.
         */
        String lowerCaseImage = tokenization.getToken(index).getImage().toLowerCase();
        final String ordinalSuffix = getOrdinalSuffix(tokenization.getToken(index - 1).getImage());
        if (!st.equals(lowerCaseImage) && !rd.equals(lowerCaseImage)) {
            lowerCaseImage = ordinalSuffix.toLowerCase();
        }
        tokenization.getToken(index).setImage(lowerCaseImage);

        // If the OT is "ST" or "RD" then the word following the OT
        // must be a type ("TY") token.
        if (tokenization.getToken(index).getImage().equalsIgnoreCase(st) || tokenization.getToken(index).getImage().equalsIgnoreCase(rd)) {
            if (tokenLoaded) {
                if (!token.hasType(STREET_TYPE)) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private static String getOrdinalSuffix(final String number) {
        final String th = "th";
        final String nd = "nd";
        final String st = "st";
        final String rd = "rd";

        final char lastDigit = number.charAt(number.length() - 1);
        final char charBeforeLast = number.length() > 1 ? number.charAt(number.length() - 2) : ' ';

        if (lastDigit == '1' && charBeforeLast != '1') {
            return st;
        } else if (lastDigit == '2' && charBeforeLast != '1') {
            return nd;
        } else if (lastDigit == '3' && charBeforeLast != '1') {
            return rd;
        } else {
            return th;
        }
    }

    static boolean allDigits(final String string) {
        for (int i = 0; i < string.length(); i++) {
            if (!Character.isDigit(string.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
