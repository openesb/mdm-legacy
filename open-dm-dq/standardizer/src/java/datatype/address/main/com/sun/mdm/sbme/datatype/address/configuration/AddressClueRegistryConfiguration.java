package com.sun.mdm.sbme.datatype.address.configuration;

import com.sun.mdm.sbme.configuration.DefaultClueRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public abstract class AddressClueRegistryConfiguration extends DefaultClueRegistryConfiguration {
	protected String getDataTypeName() {
		return "address";
	}
	
	protected Class<?extends Enum<?>> getTokenTypeClass() {
		return InputTokenType.class;
	}
}
