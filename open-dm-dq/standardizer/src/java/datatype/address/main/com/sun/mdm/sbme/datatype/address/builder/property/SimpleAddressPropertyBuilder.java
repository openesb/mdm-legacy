/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 
package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

/**
 *
 */
public class SimpleAddressPropertyBuilder extends BaseAddressPropertyBuilder {
    private String propertyName;

    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        if (this.abbreviationOption == null) {
            this.abbreviationOption = getLocale().getAbbreviationOption();
        }
        
        if (this.combinationOption == null) {
            this.combinationOption = SPACE;
        }
        
        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);
        this.loadField(address, this.propertyName, normalizedToken.getImage(), clueWordId, inputTokenType, abbreviationOption, combinationOption);
    }
    
    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
