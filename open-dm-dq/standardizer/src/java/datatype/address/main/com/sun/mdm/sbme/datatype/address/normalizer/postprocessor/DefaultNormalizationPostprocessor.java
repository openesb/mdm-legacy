/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.postprocessor;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ALPHA_NUM;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ALPHA_ONE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.BUILDING_PROPERTY;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.DIGIT;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.GENERIC_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.HIGHWAY_ROUTE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.NUMERIC_VALUE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.POST_OFFICE_BOX;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.PREFIX_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.RURAL_ROUTE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STRUCTURE_DESCRIPTOR;

import java.util.List;

import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.ClueWord;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.postprocessor.NormalizationPostprocessor;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class DefaultNormalizationPostprocessor implements NormalizationPostprocessor<InputTokenType> {
	private static final InputTokenType[] accessoryTokenTypes = new InputTokenType[] {
		POST_OFFICE_BOX,
		HIGHWAY_ROUTE,
		STRUCTURE_DESCRIPTOR,
		RURAL_ROUTE,
		PREFIX_TYPE,
		BUILDING_PROPERTY
	};
	
    public void postprocess(final List<NormalizedToken<InputTokenType>> normalizedTokens, final Tokenization tokenization) {
        boolean alphaNumFound = false;

        int tokenCount = normalizedTokens.size();
        for (int i = 0; i < tokenCount; i++) {
            if (normalizedTokens.get(i).hasType(accessoryTokenTypes)) {
            	/*
                 * Start at the word after the current word and make the
                 * subsequent words AN's if the following cases are true: 1. All
                 * subsequent words that are potential ANs must be adjacent. 2.
                 * The first word must be an NU or A1. 3. All other words must
                 * be an NU, A1, or AU and if we are processing an HR, then they
                 * CANT be D1 (this prevents 123 hwy 1e from grouping 1e
                 * together)
                 */
                int currentIndex;
                for (currentIndex = i + 1; currentIndex < tokenCount; currentIndex++) {
                	// ATTN This is the only context where Tokenization.getEnd() is used
                    if (tokenization.getToken(normalizedTokens.get(currentIndex).getEnd() - 1).getWordSplitCount() == 0) {
                        break;
                    }

                    if (currentIndex == i + 1) {

                        // First word after
                        if (!normalizedTokens.get(currentIndex).hasType(NUMERIC_VALUE) && !normalizedTokens.get(currentIndex).hasType(ALPHA_ONE)) {

                            break;
                        }

                        // Next check solves: 123 hwy i80
                        if (normalizedTokens.get(currentIndex).hasType(HIGHWAY_ROUTE)) {
                            break;
                        }
                    } else {

                        if (normalizedTokens.get(currentIndex).hasType(new InputTokenType[] { NUMERIC_VALUE, ALPHA_ONE, GENERIC_WORD })
                                && (!normalizedTokens.get(currentIndex).hasType(DIGIT) || !normalizedTokens.get(currentIndex).hasType(HIGHWAY_ROUTE))) {

                            // Make the 1st word an AN
                            normalizedTokens.get(i + 1).assignClueWords(new ClueWord<InputTokenType>(ALPHA_NUM));

                            // Make every subsequent word an AN
                            normalizedTokens.get(currentIndex).assignClueWords(new ClueWord<InputTokenType>(ALPHA_NUM));

                            alphaNumFound = true;
                        } else {
                            break;
                        }
                    }
                }
                i = currentIndex - 1;
            }
        }

        /*
         * If any ANs were found then you must now collapse the token structure
         * together.
         */
        if (alphaNumFound) {
            for (int i = 0; i < tokenCount - 1; i++) {
                if (normalizedTokens.get(i).hasType(ALPHA_NUM) && normalizedTokens.get(i + 1).hasType(ALPHA_NUM)) {

                    normalizedTokens.get(i).append(normalizedTokens.get(i + 1));

                    for (int j = i + 1; j < tokenCount - 1; j++) {
                        normalizedTokens.get(j).copyFrom(normalizedTokens.get(j + 1));
                    }

                    --tokenCount;
                    --i;
                }
            }
        }

        int tokenIndex = normalizedTokens.size();
        while (tokenIndex > tokenCount) {
            normalizedTokens.remove(--tokenIndex);
        }
    }
}
