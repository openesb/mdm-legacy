package com.sun.mdm.sbme.datatype.address.builder.property;

import com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption;
import com.sun.mdm.sbme.datatype.address.builder.CombinationOption;

public abstract class BaseAddressPropertyBuilder extends AddressPropertyBuilder {
    protected AbbreviationOption abbreviationOption;
    protected CombinationOption combinationOption;

    public AbbreviationOption getAbbreviationOption() {
        return abbreviationOption;
    }

    public void setAbbreviationOption(AbbreviationOption abbreviationOption) {
        this.abbreviationOption = abbreviationOption;
    }

    public CombinationOption getCombinationOption() {
        return combinationOption;
    }

    public void setCombinationOption(CombinationOption combinationOption) {
        this.combinationOption = combinationOption;
    }

}
