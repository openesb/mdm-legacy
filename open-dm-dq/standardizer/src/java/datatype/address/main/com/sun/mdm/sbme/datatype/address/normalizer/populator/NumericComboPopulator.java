/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.populator;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.ALPHA_ONE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.DIGIT;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.normalizer.populator.ClueIgnoringPopulator;
import com.sun.mdm.sbme.parser.BaseToken;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
public class NumericComboPopulator extends ClueIgnoringPopulator<InputTokenType> {
    private AddressLocale locale;

    public NumericComboPopulator() {}
    
    public NumericComboPopulator(AddressLocale locale) {
    	this.locale = locale;
    }
    
    /**
     * If the current word is N, S, W, or E AND if it was previously split up
     * from a numeric, then make it only a 0A1... EX: 123 main st e6 Here, we
     * don't want "e" to be the suffix direction EAST.
     */
    protected int doPopulate(final List<NormalizedToken<InputTokenType>> tokens, Tokenization tokenization, int startIndex, int endIndex, final Record contextRecord) {
    	// FIXME This clue assumes cardinal point abbreviations are single-letter for all locales
    	if (startIndex == endIndex - 1) {
            final BaseToken token = tokenization.getToken(startIndex);

            if (this.locale.isCardinalPointAbbreviation(token.getImage()) && token.getWordSplitCount() > 0) {
                this.addClueWord(tokens, ALPHA_ONE);
                this.addClueWord(tokens, DIGIT);
                return 1;
            }
    	}

        return 0;
    }

    public AddressLocale getLocale() {
        return this.locale;
    }

    public void setLocale(final AddressLocale locale) {
        this.locale = locale;
    }
}
