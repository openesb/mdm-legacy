package com.sun.mdm.sbme.datatype.address.pattern;

import static com.sun.mdm.sbme.datatype.address.pattern.AddressScoringOption.AVERAGE;
import static com.sun.mdm.sbme.datatype.address.pattern.AddressScoringOption.UNMATCHED;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.CONJUNCTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_BUILDING_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.pattern.OutputPattern;
import com.sun.mdm.sbme.pattern.OutputPatternBuilder;

public class AddressOutputPatternBuilder implements OutputPatternBuilder<InputTokenType, OutputTokenType> {
    // TODO Determine and document what priorityValues values are
    private int[] priorityValues = { 80, 40 };

    // TODO Determine and document values for maximum and minimum priority
    private final int maxPriority = 60;
    private final int minPriority = 40;

    private PatternRegistry patternRegistry;

    // FIXME Range.from is base 1
    private static class Range {
        int from;
        int to;
        private Range(int from, int to) {
            this.from = from;
            this.to = to;
        }
    }
    
    public AddressOutputPatternBuilder() {}
    
    public AddressOutputPatternBuilder(PatternRegistry patternRegistry) {
    	this.patternRegistry = patternRegistry;
    }

    /*
     * Main function that processes each pattern string, searching for
     * combinations or whole pattern strings in the pattern table.
     */
	public List<AddressOutputPattern> buildOutputPatterns(List<InputTokenType> inputTokenTypes) {
        int highPriority = 0;
        Range range = new Range(1, inputTokenTypes.size());
        final Set<OutputTokenType> outputTypesUsed = new HashSet<OutputTokenType>();
        List<AddressOutputPattern> patternsFound = new ArrayList<AddressOutputPattern>();

        // First, search for the entire token array, calling findNextPattern
        // up to 3 (2?) times; each time lowering the priority if nothing was
        // found.
        for (int priority: this.priorityValues) {
            AddressOutputPattern pattern = this.findNextPattern(inputTokenTypes, outputTypesUsed, priority, range);
            if (pattern != null) {
                highPriority = pattern.getPriority();
                patternsFound.add(pattern);
                break;
            }
        }

        // Loop through the unseen patterns, processing everything
        // that wasn't found before.
        while (advanceToNextPattern(inputTokenTypes, range)) {
            int priority = 0;
            if (highPriority >= this.maxPriority) {
                priority = this.minPriority;
            }

            // Search for the next sub-pattern. ONLY if findPatt returns
            // true (ie. not NULL) you should check tokenArrayTo re-set the
            // highest priority found.
            AddressOutputPattern pattern = this.findNextPattern(inputTokenTypes, outputTypesUsed, priority, range);

            if (pattern != null) {
                patternsFound.add(pattern);

                if (pattern.getPriority() > highPriority) {
                    highPriority = pattern.getPriority();
                }
            } else {
                // Set tokenArrayTo = tokenArrayFrom because I only want to add
                // single EI strings to the patternsFound structure
                range.to = range.from;
                
                List<InputTokenType> inputTypes = new ArrayList<InputTokenType>();
                inputTypes.add(inputTokenTypes.get(range.from - 1));
                
                List<OutputTokenType> outputTypes = new ArrayList<OutputTokenType>();
                outputTypes.add(OutputTokenType.EXTRA_INFORMATION);
                
                patternsFound.add(new AddressOutputPattern(inputTypes,
                                                    outputTypes,
                                                    0,
                                                    AddressPatternClass.EXTRA_INFORMATION,
                                                    UNMATCHED,
                                                    range.from,
                                                    range.to));

                markInputTokenTypesAsProcessed(inputTokenTypes, range.from, range.to);
            }
        }

        // A problem could potentially exist with order of elements in the array
        // of structures: the first element could contain tokens 2 - 4 and the
        // second element could contain token 1. We want these switched, hence,
        // building a "position array" will resolve this.
        Collections.sort(patternsFound, new Comparator<OutputPattern<InputTokenType, OutputTokenType>>() {
            public int compare(OutputPattern<InputTokenType, OutputTokenType> left, OutputPattern<InputTokenType, OutputTokenType> right) {
                return left.getBegin() - right.getBegin();
            }
         });

        // for each pattern string, invoke searchComboPatterns
        // TWICE (hence giving it 2 chances to find combinations of positions)
        // that will search for sub-pattern combinations,
        // and a function (calculateScore) which calculates a score for the
        // current pattern string.
        this.findPatternCombinations(patternsFound, false);
        this.findPatternCombinations(patternsFound, true);

        // Resolve NL tokens, if any
        checkForConjunctions(patternsFound);

        return patternsFound;
	}

    /*
     * findPatt is the main searching function that tries to find any
     * sub-pattern in the pattern table.
     */
    private AddressOutputPattern findNextPattern(final List<InputTokenType> inputTokenTypes,
                                          final Set<OutputTokenType> outputTokenTypesUsed,
                                          final int minimumPriority,
                                          final Range range)
    {
        /*
         * The first pattern search (ie. when you are initially searching for
         * the entire pattern) is different than subsequent searches.
         * 
         * Anytime a pattern is found, you must make sure that the following
         * conditions are met: (1). The priority is >= minpri (2). The pattern
         * type hasn't already been found.
         * 
         * 
         * 1ST CHECK: Bias at the beginning of the pattern EXAMPLE: Assume 123
         * main ave apt 2 a, then the first searches are: 1 2 3 4 5 6 1 2 3 4 5
         * 1 2 3 4 1 2 3 1 2 1
         */
        for (int start = range.from, end = range.to; end - start >= 0; end--) {
            final List<InputTokenType> candidatePatternTypes = new ArrayList<InputTokenType>(inputTokenTypes.subList(start - 1, end));
            final AddressPattern addressPattern = this.patternRegistry.lookup(candidatePatternTypes);

            if (addressPattern != null &&
                addressPattern.getPriority() >= minimumPriority &&
                !this.outputTokenAlreadyUsed(addressPattern.getOutputTokenTypes(), outputTokenTypesUsed))
            {
                // If it is found, then set the dirty bit in outToksUsed, and return
                markOutputTokensAsUsed(addressPattern.getOutputTokenTypes(), outputTokenTypesUsed);

                markInputTokenTypesAsProcessed(inputTokenTypes, start, end);

                range.from = start;
                range.to = end;

                return new AddressOutputPattern(addressPattern, range.from, range.to);
            }
        }

        /*
         * 2ND CHECK: Bias at the end of the pattern EXAMPLE (from above): 2 3 4
         * 5 6 3 4 5 6 4 5 6 5 6 6
         */
        for (int start = range.from + 1, end = range.to; end - start >= 0; start++) {
            final List<InputTokenType> candidatePatternTypes = new ArrayList<InputTokenType>(inputTokenTypes.subList(start - 1, end));
            final AddressPattern addressPattern = this.patternRegistry.lookup(candidatePatternTypes);

            if (addressPattern != null &&
                addressPattern.getPriority() >= minimumPriority &&
                !this.outputTokenAlreadyUsed(addressPattern.getOutputTokenTypes(), outputTokenTypesUsed))
            {
                // If its found, then set the dirty bit in outToksUsed, and return
                markOutputTokensAsUsed(addressPattern.getOutputTokenTypes(), outputTokenTypesUsed);

                markInputTokenTypesAsProcessed(inputTokenTypes, start, end);

                range.from = start;
                range.to = end;

                return new AddressOutputPattern(addressPattern, range.from, range.to);
            }
        }

        /*
         * FINAL CHECK: eliminate the beginning and ending tokens, and search
         * for the longest pattern in between.
         * 
         * EXAMPLE (from above): 2 3 4 5 search all "4"s
         * 
         * 2 3 4 search all "3"s 3 4 5
         * 
         * 2 3 search all "2"s 3 4 4 5
         * 
         * 2 search all "1"s 3 4 5
         */
        final int end = range.to - 1;
        final int start = range.from + 1;

        int gap = end - start;
        final int origGap = gap;

        while (gap >= 0) {

            final int numIts = origGap - gap + 1;

            for (int i = 0; i < numIts; i++) {
                final List<InputTokenType> candidatePatternTypes = new ArrayList<InputTokenType>(inputTokenTypes.subList(start + i - 1, start + i + gap));
                final AddressPattern addressPattern = this.patternRegistry.lookup(candidatePatternTypes);

                if (addressPattern != null &&
                    addressPattern.getPriority() >= minimumPriority &&
                    !this.outputTokenAlreadyUsed(addressPattern.getOutputTokenTypes(), outputTokenTypesUsed))
                {

                    // If it is found, then set the dirty bit in outToksUsed, and return
                    markOutputTokensAsUsed(addressPattern.getOutputTokenTypes(), outputTokenTypesUsed);

                    markInputTokenTypesAsProcessed(inputTokenTypes, start + i, start + i + gap);

                    range.from = start + i;
                    range.to = start + i + gap;

                    return new AddressOutputPattern(addressPattern, range.from, range.to);
                }
            }
            gap--;
        }

        // if nothing was found, then dirty the Token between the original
        // starting and ending tokens and return NULL
        return null;
    }

    /*
     * Retrieves the input tokens from "st" tokenArrayTo "end" in the token
     * array "tokenArray" and change their values tokenArrayTo "--", meaning
     * they've been processed.
     */
    private static void markInputTokenTypesAsProcessed(final List<InputTokenType> types, int st, final int end) {
        while (st <= end) {
            types.set(st - 1, null);
            st++;
        }
    }

    /*
     * Uses the token array "tokenArray" to determine the next
     * consecutive set of input tokens that haven't been used. Returns
     * their position in "tokenArrayFrom" and "tokenArrayTo".
     */
    private static boolean advanceToNextPattern(final List<InputTokenType> types, final Range range) {
        range.from = 0;
        range.to = 0;

        for (int i = 0; i < types.size(); i++) {
            if (types.get(i) ==  null) {
                if (range.from != 0) {
                    break;
                } else {
                    continue;
                }
            }

            if (range.from == 0) {
                range.from = i + 1;
            }
            range.to = i + 1;
        }

        return range.from != 0 && range.to != 0;
    }

    /*
     * Returns true if any output token in the string has been already used or
     * found in previous searches.
     * 
     */
    private boolean outputTokenAlreadyUsed(final List<OutputTokenType> types, final Set<OutputTokenType> outputTokensUsed) {
        for (OutputTokenType type: types) {
            if (outputTokensUsed.contains(type)) {
                return true;
            }
        }
        return false;
    }

    /*
     * Receives an output token string just found and process each output token
     * in the string, and "dirty" it's respective element in the array,
     * outToksUsed; hence indicating that output token is not allowed in any
     * pattern searching.
     */
    private static void markOutputTokensAsUsed(final List<OutputTokenType> types, final Set<OutputTokenType> outputTokensUsed) throws MdmStandardizationException {
        for (OutputTokenType type: types) {
            outputTokensUsed.add(type);
        }
    }

    private static void checkForConjunctions(final List<AddressOutputPattern> patternsFound) {
        for (OutputPattern<InputTokenType, OutputTokenType> aPattern: patternsFound) {
        	AddressOutputPattern pattern = (AddressOutputPattern) aPattern;
            if (pattern.getOutputTokenTypes().contains(CONJUNCTION)) {
                for (int i = 0; i < pattern.getOutputTokenTypes().size(); i++) {
                    if (pattern.getOutputTokenType(i) == CONJUNCTION) {
                        checkForConjunctions(pattern, i);
                    }
                }
            }
        }
    }
    private static void checkForConjunctions(AddressOutputPattern pattern, int startIndex) {
        for (int k = startIndex + 1; k < pattern.getOutputTokenTypes().size(); k++) {
            if (pattern.getOutputTokenType(k) != CONJUNCTION) {
                switch (pattern.getOutputTokenType(k)) {
                    case STREET_NAME:
                        pattern.setOutputTokenType(startIndex, STREET_NAME);
                        break;
                    case PROPERTY_BUILDING_NAME:
                        pattern.setOutputTokenType(startIndex, PROPERTY_BUILDING_NAME);
                        break;
                    case EXTRA_INFORMATION:
                        // TODO Move this logic to locale class
                        pattern.setOutputTokenType(startIndex, STREET_NAME);
                        pattern.setOutputTokenType(k, STREET_NAME);
                        break;
                    default:
                        return;
                }
            }
        }
    }

    private void findPatternCombinations(final List<AddressOutputPattern> patternsFound,
                                         final boolean averagingAllowed)
    {
        int previousSize = 0;
        boolean patternAllowsAveraging = false;
        boolean patternDisallowsAveraging = false;
        final List<InputTokenType> inputTokenTypes = new ArrayList<InputTokenType>();
        
        for (int from = 1, to = patternsFound.size(); to - from > 0; to--) {
            // First, make sure that at least 1 '*' token pattern exists
            boolean averagingTypeFound = false;
            for (int i = from; i <= to; i++) {
                if (patternsFound.get(i - 1).getScoringOption() == AVERAGE) {
                    averagingTypeFound = true;
                    break;
                }
            }
            if (!averagingTypeFound) {
                return;
            }

            // This method appends to previously examined input pattern strings
            populateTypesFromPatterns(inputTokenTypes, // input pattern strings
                                      patternsFound,
                                      from,
                                      to);

            // FIXME Get rid of [dis]allowsAveraging!
            for (int i = previousSize; i < inputTokenTypes.size(); i++) {
                if (inputTokenTypes.get(i).allowsAveraging()) {
                    patternAllowsAveraging = true;
                }
                if (inputTokenTypes.get(i).disallowsAveraging()) {
                    patternDisallowsAveraging = true;
                }
            }

            // FOR ALL PASSES: Only search for the string if at least one "*"
            // pattern exists in the current string.
            // FOR THE 1st PASS: patterns containing "+" pattern tokenTypes ARE allowed
            // FOR THE 2nd PASS: patterns containing "+" pattern tokenTypes ARE NOT allowed
            AddressPattern inputPattern;
            if (
                patternAllowsAveraging &&
                (!(averagingAllowed && patternDisallowsAveraging)) &&
                (inputPattern = this.patternRegistry.lookup(inputTokenTypes)) != null &&
                !averagingOptionAlreadyUsed(inputPattern, patternsFound, from) &&
                !outputTokenAlreadyUsed(inputPattern, patternsFound, from))
            {
                modifyStructure(inputPattern, patternsFound, from, to);
            } else if (to - from <= 1) {
                from++;
                to = patternsFound.size() + 1;
            }
        }
    }

    /**
     * buildStringFromPatterns will construct a character string from elements
     * "fr" to "to" in the array of structs, pattsfound.
     */
    // This method appends to previously examined pattern strings
    public static void populateTypesFromPatterns(final List<InputTokenType> tokenTypes,
                                                 final List<AddressOutputPattern> patternsFound,
                                                 int from,
                                                 final int to)
    {
        int endIndex;
        for (endIndex = 0; from <= to; from++) {
            AddressOutputPattern patternFound = patternsFound.get(from - 1);

            InputTokenType tokenType = null;
            if (patternFound.getScoringOption() == UNMATCHED) {
                tokenType = patternFound.getInputTokenTypes().get(0); // size() == 0 in all cases 
            } else {
                switch (patternFound.getPatternClass()) {
                    case HOUSE:
                        tokenType = InputTokenType.HOUSE_CLASS;
                        break;
                    case BUILDING:
                        tokenType = InputTokenType.BUILDING_CLASS;
                        break;
                    case UNIT_WITHIN_STRUCTURE:
                        tokenType = InputTokenType.UNIT_WITHIN_STRUCTURE_CLASS;
                        break;
                    case STREET_TYPE:
                        tokenType = InputTokenType.STREET_TYPE_CLASS;
                        break;
                    case RURAL_ROUTE:
                        tokenType = InputTokenType.RURAL_ROUTE_CLASS;
                        break;
                    case POST_OFFICE_BOX:
                        tokenType = InputTokenType.POST_OFFICE_BOX_CLASS;
                        break;
                    case MOSTLY_NUMERIC:
                        tokenType = InputTokenType.MOSTLY_NUMERIC_CLASS;
                        break;
                    case EXTRA_INFORMATION:
                        tokenType = InputTokenType.EXTRA_INFORMATION;
                        break;
                    default:
                        throw new MdmStandardizationException("Invalid or null pattern class");
                }
            }

            tokenTypes.add(tokenType);
            endIndex++;
        }
        
        // Remove extra tokens
        while (endIndex < tokenTypes.size()) {
            tokenTypes.remove(endIndex);
        }
    }

    private static boolean outputTokenAlreadyUsed(final AddressPattern addressPattern,
                                                  final List<AddressOutputPattern> addressOutputPatterns,
                                                  final int index)
    {
        for (int i = 0; i <= index; i++) {
            for (OutputTokenType tokenType: addressPattern.getOutputTokenTypes()) {
                if (addressOutputPatterns.get(i).getOutputTokenTypes().contains(tokenType)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean averagingOptionAlreadyUsed(final AddressPattern addressPattern,
                                               		  final List<AddressOutputPattern> patterns,
                                               		  final int index)
    {
        for (int i = 0; i <= index; i++) {
        	AddressOutputPattern pattern = patterns.get(i);
            if (addressPattern.getScoringOption() == pattern.getScoringOption()) {
                return true;
            }
        }
        return false;
    }

    /**
     * modifyStructure will modify the proper element in the pattsfound
     * structure if a valid combination pattern was found.
     */
    public static void modifyStructure(final AddressPattern addressPattern,
                                       final List<AddressOutputPattern> patternsFound,
                                       int from,
                                       final int to)
    {
        for (int index = 0; to - from >= 0; from++, index++) {
            int patternIndex = from - 1;
            AddressOutputPattern pattern = patternsFound.get(patternIndex);
            if (pattern.getScoringOption() != AVERAGE) {
                pattern.setScoringOption((addressPattern.getScoringOption()));
                pattern.setOutputTokenTypes(addressPattern.getOutputTokenTypes().subList(index, index + 1));
                pattern.setPriority(addressPattern.getPriority());
            }
        }
    }

	public int[] getPriorityValues() {
		return priorityValues;
	}

	public void setPriorityValues(int[] priorityValues) {
		this.priorityValues = priorityValues;
	}

	public PatternRegistry getPatternRegistry() {
		return patternRegistry;
	}

	public void setPatternRegistry(PatternRegistry patternRegistry) {
		this.patternRegistry = patternRegistry;
	}

	public int getMaxPriority() {
		return maxPriority;
	}

	public int getMinPriority() {
		return minPriority;
	}

}
