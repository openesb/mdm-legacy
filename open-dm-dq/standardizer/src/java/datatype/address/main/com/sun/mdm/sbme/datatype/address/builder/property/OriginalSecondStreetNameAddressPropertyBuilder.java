package com.sun.mdm.sbme.datatype.address.builder.property;


import static com.sun.mdm.sbme.datatype.address.Address.ORIGINAL_SECOND_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class OriginalSecondStreetNameAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        this.loadField(address, ORIGINAL_SECOND_STREET_NAME, normalizedToken.getImage(), 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
    }
}
