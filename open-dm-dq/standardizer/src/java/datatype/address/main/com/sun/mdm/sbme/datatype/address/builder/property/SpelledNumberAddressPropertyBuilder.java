package com.sun.mdm.sbme.datatype.address.builder.property;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class SpelledNumberAddressPropertyBuilder extends SimpleAddressPropertyBuilder {
	protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        this.checkForSpelledOutNumber(normalizedToken);
        super.buildProperty(address, normalizedToken, inputTokenType, outputTokenType, context);
    }
}
