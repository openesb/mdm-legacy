package com.sun.mdm.sbme.datatype.address.variant.fr.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.variant.fr.FRAddressLocale;

@Configuration
public class FRLocaleConfiguration {
	@Bean
	public AddressLocale locale() {
		return new FRAddressLocale();
	}
}
