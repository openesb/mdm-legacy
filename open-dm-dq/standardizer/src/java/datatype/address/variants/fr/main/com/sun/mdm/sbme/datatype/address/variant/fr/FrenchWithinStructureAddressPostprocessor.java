package com.sun.mdm.sbme.datatype.address.variant.fr;


import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.STANDARD_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.UNSPECIFIED;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.EXTRA_INFORMATION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.AbstractWithinStructureAddressPostprocessor;

public class FrenchWithinStructureAddressPostprocessor extends AbstractWithinStructureAddressPostprocessor {
	/*
     * Places a "#" in the wsd field if wsd is blank and wsi is non-blank. Also,
     * the wsi field is squashed if needed. If no wsi exists and wsd != blank,
     * then copy wsd to wsi.
     */
    @Override
    public void postprocess(Address address, Record context) {
    	String withinStructureDescriptor = address.get(WITHIN_STRUCTURE_DESCRIPTOR);
    	String withinStructureIdentifier = address.get(WITHIN_STRUCTURE_IDENTIFIER);
    	
        if (withinStructureIdentifier != null) {
            if (withinStructureDescriptor == null) {
                // FIXME What do "G", "UG" and "LG" mean as structure identifier?
                if ("G".equals(withinStructureIdentifier) ||
                    "UG".equals(withinStructureIdentifier) ||
                    "LG".equals(withinStructureIdentifier))
                {
                    // FIXME What does "Fl"  mean as structure descriptor?
                    this.loadField(address, WITHIN_STRUCTURE_DESCRIPTOR, "Fl", 0, UNSPECIFIED, getLocale().getAbbreviationOption(), SPACE);
                } else {
                    // FIXME What does "#"  mean as structure descriptor?
                    this.loadField(address, WITHIN_STRUCTURE_DESCRIPTOR, "#", 0, UNSPECIFIED, getLocale().getAbbreviationOption(), SPACE);
                }
            }

            String descriptor = searchStandIden(new StringBuilder(withinStructureIdentifier));
            address.set(WITHIN_STRUCTURE_IDENTIFIER, descriptor);
        } else if (withinStructureDescriptor != null && withinStructureDescriptor.length() != 0) {
            // FIXME What does "#" mean as within structure descriptor?
            if (withinStructureDescriptor.charAt(0) == '#') {
                this.loadField(address, EXTRA_INFORMATION, "#", 0, STRUCTURE_DESCRIPTOR, STANDARD_ABBREVIATION, SPACE);
                // FIXME What does "-" mean as within structure descriptor?
                address.set(WITHIN_STRUCTURE_DESCRIPTOR, "-");
            } else if ("G".equals(withinStructureDescriptor) ||
                       "UG".equals(withinStructureDescriptor) ||
                       "UL".equals(withinStructureDescriptor))
            {
                address.set(WITHIN_STRUCTURE_IDENTIFIER, withinStructureDescriptor);
                address.set(WITHIN_STRUCTURE_DESCRIPTOR, "Fl");
            } else {
                address.set(WITHIN_STRUCTURE_IDENTIFIER, "-");
            }
        }
    }
}
