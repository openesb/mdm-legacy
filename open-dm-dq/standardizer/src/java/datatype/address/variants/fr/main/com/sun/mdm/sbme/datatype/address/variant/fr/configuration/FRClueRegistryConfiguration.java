package com.sun.mdm.sbme.datatype.address.variant.fr.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.configuration.ClueRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.ClueWord.ClueWordListFormatter;

@Configuration
public class FRClueRegistryConfiguration extends ClueRegistryConfiguration {
	@Bean
	public ClueRegistry<InputTokenType> clueRegistry() throws Exception {
		return new ClueRegistry(super.getClues());
	}
	
	@Override
	protected String getDataTypeName() {
		return "address";
	}

	@Override
	protected String getVariantName() {
		return "fr";
	}

//	@Override
	protected FieldDescriptor[] getFieldDescriptors() {
		return new FieldDescriptor[] {
				new FieldDescriptor(0, 34, true), // Name
				new FieldDescriptor(34, 14, true), // Translation
				new FieldDescriptor(50, 20, false), // Words
			};
	}

//	@Override
	protected PropertyDescriptor[] getPropertyDescriptors() {
		return  new PropertyDescriptor[] {
                new PropertyDescriptor("name", null, null),
                new PropertyDescriptor("translation", null, null),
                new PropertyDescriptor("words", new ClueWordListFormatter(InputTokenType.class, true), null),
			};
	}
}
