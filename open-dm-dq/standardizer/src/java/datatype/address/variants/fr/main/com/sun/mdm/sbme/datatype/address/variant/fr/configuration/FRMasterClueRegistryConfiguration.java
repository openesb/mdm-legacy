package com.sun.mdm.sbme.datatype.address.variant.fr.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.address.builder.InputTokenTypeMapper;
import com.sun.mdm.sbme.datatype.address.builder.MasterClueRegistry;
import com.sun.mdm.sbme.datatype.address.configuration.MasterClueRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.variant.fr.FrenchInputTokenTypeMapper;

@Configuration
public class FRMasterClueRegistryConfiguration extends MasterClueRegistryConfiguration {
	@Override
	@Bean
	public MasterClueRegistry masterClueRegistry() {
		return super.masterClueRegistry();
	}
	
	@Override
	protected FieldDescriptor[] getFieldDescriptors() {
		return new FieldDescriptor[] {
                new FieldDescriptor(0, 4, true), // Id
                new FieldDescriptor(4, 33, true), // fullName
                new FieldDescriptor(38, 13, true), // standardAbbreviation
                new FieldDescriptor(52, 5, true), // shortAbbreviation
                new FieldDescriptor(58, 4, true), // uspsAbbreviation
                new FieldDescriptor(70, 2, true), // type
        };
	}

	@Override
	protected InputTokenTypeMapper getInputTokenTypeMapper() {
		return new FrenchInputTokenTypeMapper();
	}

	@Override
	protected String getVariantName() {
		return "fr";
	}
}
