package com.sun.mdm.sbme.datatype.address.variant.fr;

import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.STANDARD_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.BUILDING_PROPERTY;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.AddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public class FrenchPostOfficeBoxAddressPostprocessor extends AddressPostprocessor {
    // FIXME A table of clue word ids is needed
    private static final int BOX = 1;

	@Override
    protected void postprocess(Address address, Record context) {
        final String Box = "Boite";
        final InputTokenType inputTokenType = BUILDING_PROPERTY;

        // If bxi has a value and bxd is blank then place "box" in bxd
        String postOfficeBoxDescriptor = address.get(POST_OFFICE_BOX_DESCRIPTOR);
        String postOfficeBoxIdentifier = address.get(POST_OFFICE_BOX_IDENTIFIER);
        if (postOfficeBoxIdentifier != null && postOfficeBoxDescriptor == null) {
            this.loadField(address, POST_OFFICE_BOX_DESCRIPTOR, Box, BOX, inputTokenType, STANDARD_ABBREVIATION, SPACE);
        }
    }
}
