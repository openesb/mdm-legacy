package com.sun.mdm.sbme.datatype.address.variant.fr.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.parser.AddressParser;
import com.sun.mdm.sbme.parser.Parser;

@Configuration
public abstract class FRParserConfiguration {
	@Bean
	public Parser parser() {
		return new AddressParser(locale());
	}
	
	@ExternalBean
	public abstract AddressLocale locale();
}
