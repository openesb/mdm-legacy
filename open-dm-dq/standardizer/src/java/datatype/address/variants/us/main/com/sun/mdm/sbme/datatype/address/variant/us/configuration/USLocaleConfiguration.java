package com.sun.mdm.sbme.datatype.address.variant.us.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.locale.english.USAddressLocale;

@Configuration
public class USLocaleConfiguration {
	@Bean
	public AddressLocale locale() {
		return new USAddressLocale();
	}
}
