package com.sun.mdm.sbme.datatype.address.variant.us.configuration;

import java.util.LinkedList;
import java.util.List;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;

import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.DefaultNonNullCluePopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.DefaultNullCluePopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.EnglishOrdinalPopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.NumericComboPopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.ValidFractionPopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.postprocessor.DefaultNormalizationPostprocessor;
import com.sun.mdm.sbme.datatype.address.normalizer.validator.HighwayRoadComboClueValidator;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.DefaultNormalizer;
import com.sun.mdm.sbme.normalizer.Normalizer;
import com.sun.mdm.sbme.normalizer.populator.ClueWordPopulator;
import com.sun.mdm.sbme.normalizer.validator.ClueValidator;

@Configuration
public abstract class USNormalizerConfiguration {
	@Bean
	public Normalizer<InputTokenType> normalizer() {
		DefaultNormalizer<InputTokenType> normalizer = new DefaultNormalizer<InputTokenType>();
		normalizer.setClueRegistry(clueRegistry());
		normalizer.setClueValidators(clueValidators());
		normalizer.setClueWordTokenPopulators(cluePopulators());
		normalizer.setNormalizationPostprocessor(new DefaultNormalizationPostprocessor());
		return normalizer;
	}

	@ExternalBean
	public abstract ClueRegistry<InputTokenType> clueRegistry();
	
	@ExternalBean
	public abstract AddressLocale locale();
	
	private List<ClueValidator<InputTokenType>> clueValidators() {
		List<ClueValidator<InputTokenType>> clueValidators = new LinkedList<ClueValidator<InputTokenType>>();
		clueValidators.add(new HighwayRoadComboClueValidator(locale(), clueRegistry()));
		return clueValidators;
	}
	
	private List<ClueWordPopulator<InputTokenType>> cluePopulators() {
		List<ClueWordPopulator<InputTokenType>> cluePopulators = new LinkedList<ClueWordPopulator<InputTokenType>>();
		cluePopulators.add(new DefaultNullCluePopulator());
		cluePopulators.add(new EnglishOrdinalPopulator());
		cluePopulators.add(new NumericComboPopulator(locale()));
		cluePopulators.add(new ValidFractionPopulator());
		cluePopulators.add(new DefaultNonNullCluePopulator());
		return cluePopulators;
	}
}
