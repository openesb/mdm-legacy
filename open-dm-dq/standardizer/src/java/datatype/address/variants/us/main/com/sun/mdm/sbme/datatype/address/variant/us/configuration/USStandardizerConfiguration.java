package com.sun.mdm.sbme.datatype.address.variant.us.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;
import org.springframework.config.java.context.AnnotationApplicationContext;
import org.springframework.context.ApplicationContext;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.DefaultStandardizer;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.builder.StandardizedRecordBuilder;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.Normalizer;
import com.sun.mdm.sbme.parser.Parser;
import com.sun.mdm.sbme.pattern.PatternFinder;

@Configuration
public abstract class USStandardizerConfiguration {
	@Bean
	public Standardizer standardizer() {
		DefaultStandardizer<InputTokenType, OutputTokenType> standardizer = new DefaultStandardizer<InputTokenType, OutputTokenType>();
		
		standardizer.setCleanser(cleanser());
		standardizer.setParser(parser());
		standardizer.setNormalizer(normalizer());
		standardizer.setPatternFinder(patternFinder());
		standardizer.setBuilder(builder());
        
        standardizer.setStandardizationType("Address");
        standardizer.setStandardizationProperty(OutputTokenType.EXTRA_INFORMATION.getAlias());
		
		return standardizer;
	}
	
	public static void main(String[] args) throws Exception {
//		ApplicationContext xmlContext = new FileSystemXmlApplicationContext(args[0]);
		ApplicationContext annotationContext = new AnnotationApplicationContext("/com/sun/mdm/sbme/datatype/address/configuration/us/*Configuration.class");
		annotationContext.getBean("standardizer");
		
	}
	
	@ExternalBean
	public abstract StringTransformer cleanser();
	
	@ExternalBean
	public abstract Parser parser();
	
	@ExternalBean
	public abstract Normalizer<InputTokenType> normalizer();
	
	@ExternalBean
	public abstract PatternFinder<InputTokenType, OutputTokenType> patternFinder();
	
	@ExternalBean
	public abstract StandardizedRecordBuilder<InputTokenType, OutputTokenType> builder();
}
