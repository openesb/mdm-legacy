package com.sun.mdm.sbme.datatype.address.variant.au.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.mdm.sbme.datatype.address.configuration.PatternRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.PatternRegistry;

@Configuration
public class AUPatternRegistryConfiguration extends PatternRegistryConfiguration {
	@Override
	@Bean
	public PatternRegistry patternRegistry() {
		return super.patternRegistry();
	}
	
	@Override
	protected String getVariantName() {
		return "au";
	}
}
