package com.sun.mdm.sbme.datatype.address.variant.au.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.locale.english.AUAddressLocale;

@Configuration
public class AULocaleConfiguration {
	@Bean
	public AddressLocale locale() {
		return new AUAddressLocale();
	}
}
