package com.sun.mdm.sbme.datatype.address.variant.uk.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.configuration.AddressClueRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

@Configuration
public class UKClueRegistryConfiguration  extends AddressClueRegistryConfiguration {
	@Bean
	public ClueRegistry<InputTokenType> clueRegistry() throws Exception {
		return new ClueRegistry(super.getClues());
	}
	
 	@Override
	protected String getVariantName() {
		return "uk";
	}
}
