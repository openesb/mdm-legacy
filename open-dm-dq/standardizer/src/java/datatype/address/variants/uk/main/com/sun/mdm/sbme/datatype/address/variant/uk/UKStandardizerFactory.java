package com.sun.mdm.sbme.datatype.address.variant.uk;

import com.sun.mdm.sbme.AnnotationContextStandardizerFactory;


public class UKStandardizerFactory extends AnnotationContextStandardizerFactory {
	@Override
	protected String[] getConfigurationLocations() {
		return new String[] {
				"/com/sun/mdm/sbme/configuration/*Configuration.class",
				"/com/sun/mdm/sbme/datatype/address/configuration/*Configuration.class",
				"/com/sun/mdm/sbme/datatype/address/variant/uk/configuration/*Configuration.class",
		};
	}
}
