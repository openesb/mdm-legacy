package com.sun.mdm.sbme.datatype.address.variant.uk.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.locale.english.UKAddressLocale;

@Configuration
public class UKLocaleConfiguration {
	@Bean
	public AddressLocale locale() {
		return new UKAddressLocale();
	}
}
