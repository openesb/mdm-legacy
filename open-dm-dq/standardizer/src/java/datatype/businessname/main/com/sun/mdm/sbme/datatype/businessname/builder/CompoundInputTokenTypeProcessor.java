package com.sun.mdm.sbme.datatype.businessname.builder;

import java.util.List;

/**
 * Processes an token with a list of <code>InputTokenType</code> processors.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CompoundInputTokenTypeProcessor implements InputTokenTypeProcessor {

	/**
	 * List of processors
	 */
    List<InputTokenTypeProcessor> processors;
    
    /**
     * The empty constructor.
     */
    public CompoundInputTokenTypeProcessor() {}
    
    /**
     * Construct the object with a list of <code>InputTokenTypeProcessor's</code>.
     * 
     * @param processors the list of <code>InputTokenType</code> processors
     */
    public CompoundInputTokenTypeProcessor(List<InputTokenTypeProcessor> processors) {
    	this.processors = processors;
    }
    
    /**
     * Processor the token based on the list of <code>InputTokenTypeProcessor's</code>.
     * 
     * @param token the image after pre-processing and cleansing
     * @param image the raw image
     * @param context context which holds values
     * 
     * @return true if any of the <code>InputTokenTypeProcessor's</code> processed the token
     */
    public boolean process(String token, String image, BusinessNameContext context) {
        for (InputTokenTypeProcessor processor: processors) {
            if (processor.process(token, image, context)) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Set the processors for this processor.
     * 
     * @param processors a list of <code>InputTokenTypeProcessor's</code>
     */
    public void setProcessors(List<InputTokenTypeProcessor> processors) {
        this.processors = processors;
    }

}
