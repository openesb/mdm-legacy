package com.sun.mdm.sbme.datatype.businessname.configuration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.java.hulp.i18n.LocalizedString;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.Localizer;
import com.sun.mdm.sbme.datatype.businessname.Pattern;

public abstract class PatternRegistryConfiguration {

	private static net.java.hulp.i18n.Logger logger;
	private static Localizer localizer;
	
	static {
		logger = net.java.hulp.i18n.Logger.getLogger(InputTokenType.class);
		localizer = Localizer.get();
	}
	
	/**
	 * Configures a map based pattern registry
	 * 
	 * @return a map consisting of a pattern registry
	 */
	public Map<String, Pattern> patternRegistry() {

    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 1, true), // Pattern size
                new FieldDescriptor(2, 100, true), // Input token types
        };
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("patternSize", null, null),
                new PropertyDescriptor("inputPattern", null, null),
        };
        
        FixedFieldTokenizer secondTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] secondFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 100, true), // Output token types

        };
        secondTokenizer.setFieldDescriptors(secondFieldDescriptors);
        PropertyDescriptor[] secondPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("outputPattern", null, null),  

        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
                new DefaultRecordPopulator(secondTokenizer, secondPropertyDescriptors), 
        };
        
        String patternFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizPatterns.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(patternFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", patternFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String patternSize = (String) record.get("patternSize");
                String inputPattern = (String) record.get("inputPattern");
                List<Record> recordList = recordMap.get(patternSize + "|" + inputPattern);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(patternSize + "|" + inputPattern, recordList);
                }
                recordList.add(record);
        }

        Map<String, Pattern> patternMap = new TreeMap<String, Pattern>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
                record = candidateRecord;
                if ("".equals(record.getString("usageFlag"))) {
                    break;
                }
            }
            @SuppressWarnings("unchecked")
            String outputPattern = (String) record.get("outputPattern");
            String[] patterns = key.split("\\|");
            int patternSize = Integer.parseInt(patterns[0]);
            String inputPattern = patterns[1];

            Pattern businessPattern = new Pattern(patternSize, inputPattern, outputPattern);
            patternMap.put(key, businessPattern);
        }
        
        return patternMap;
	}
	
	/**
	 * Returns the variant name.
	 * 
	 * @return the variant name
	 */
	protected abstract String getVariantName();
}
