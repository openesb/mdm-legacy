/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer.handler;

import java.util.List;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * <code>TokenHandler</code> implementation consisting of a list of <code>TokenHandler's</code>.
 *  
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CompoundTokenHandler implements TokenHandler {

	/**
	 * The list of token handlers.
	 * 
	 */
    List<TokenHandler> handlers;

    /**
     * Invoke each token handler in the list.
     * 
     * @param tokens list of tokens
     * @param normalizedField the normalized field
     */
    public void handle(final List<Token> tokens, final NormalizedField normalizedField) {

        for (final TokenHandler handler : this.handlers) {
            handler.handle(tokens, normalizedField);
        }
    }

    /**
     * Set the <code>TokenHandler's</code> to be invoked.
     * 
     * @param handlers the list of <code>TokenHandler's</code>
     */
    public void setHandlers(final List<TokenHandler> handlers) {
        this.handlers = handlers;
    }

}
