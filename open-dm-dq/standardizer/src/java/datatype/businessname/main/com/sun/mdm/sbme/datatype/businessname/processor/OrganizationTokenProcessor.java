/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.ORT;

import java.util.Map;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component used to perform transformations on tokens containing organization
 * types.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class OrganizationTokenProcessor implements TokenProcessor {

	/**
	 * The map of organizations.
	 * 
	 */
    private Map<String, String> organizations;

    /**
     * The empty constructor.  Must use setter methods to register components.
     * 
     */
    public OrganizationTokenProcessor() {}
    
    /**
     * Construct a new <code>OrganizationTokenProcessor</code> configured with a map of organizations.
     * 
     * @param organizations the map of organizations
     */
    public OrganizationTokenProcessor(Map<String, String> organizations) {
    	this.organizations = organizations;
    }
    
    /**
     * Looks up the tokens' organization from the map of organization, and if found,
     * sets it as the tokens image.
     * 
     * @param token a fragment of the business name
     * @param tokenImage a pre-processed and possibly transformed image of the token
     * @param normalizedField a normalized field for storing normalized data 
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        token.setImage(tokenImage);
        final String organization = this.organizations.get(tokenImage);

        // only apply this value if it's a single token
        if (!organization.equals("0") && tokenImage.indexOf(" ") == -1) {
            token.setImage(organization);
        }

        normalizedField.setIncompleteAlias(true);

        // Update the list that holds the different types of tokens
        token.setInputTokenType(ORT);
    }

    /**
     * Registers the map of organizations used by the processor.
     * 
     * @param organizations the map of organizations
     */
    public void setOrganizations(final Map<String, String> organizations) {
        this.organizations = organizations;
    }

}
