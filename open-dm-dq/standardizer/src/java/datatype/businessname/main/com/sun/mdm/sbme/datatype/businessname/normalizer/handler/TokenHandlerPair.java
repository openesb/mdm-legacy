/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer.handler;

import com.sun.mdm.sbme.datatype.businessname.matcher.TokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.processor.TokenProcessor;

/**
 * Component consisting of a <code>TokenMatcher</code> and <code>TokenProcessor</code>.
 * If the <code>TokenMatcher</code> matches a particular token, then do some
 * processing with <code>TokenProcessor</code>.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class TokenHandlerPair {

	/**
	 * <code>TokenMatcher</code> used to match against a particular token.
	 * 
	 */
    private TokenMatcher matcher;

    /**
     * <code>TokenProcessor</code> used to process a particular token if matched
     * by the <code>TokenMatcher</code>.
     * 
     */
    private TokenProcessor processor;

    /**
     * Flag indicating if the token consists of more than 1 element.
     * 
     */
    private boolean multiTokenProcessor;

    /**
     * The empty constructor, if used, setter methods have to be used to populate this object.
     * 
     */
    public TokenHandlerPair() {}
    
    /**
     * Construct a new <code>TokenHandlerPair</code> with a <code>TokenMatcher</code>
     * and a <code>TokenProcessor</code>.
     * 
     * @param matcher the token matcher responsible for matching a token
     * @param processor the token processor responsible for processing a token
     */
    public TokenHandlerPair(TokenMatcher matcher, TokenProcessor processor) {
    	this(matcher, processor, false);
    }
    
    /**
     * Construct a new <code>TokenHandlerPair</code> with a <code>TokenMatcher</code>
     * and a <code>TokenProcessor</code>.
     * 
     * @param matcher the token matcher responsible for matching a token
     * @param processor the token processor responsible for processing a token
     * @param multiTokenProcessor indicates whether or not token has more than 1 element
     */
    public TokenHandlerPair(TokenMatcher matcher, TokenProcessor processor, boolean multiTokenProcessor) {
    	this.matcher = matcher;
    	this.processor = processor;
    	this.multiTokenProcessor = multiTokenProcessor;
    }
    
    /**
     * Gets the <code>TokenMatcher</code> associated with this <code>TokenHandlerPair</code>.
     * 
     * @return the token matcher
     */
    public TokenMatcher getMatcher() {
        return this.matcher;
    }

    /**
     * Sets the <code>TokenMatcher</code> associated with this <code>TokenHandlerPair</code>.
     * 
     * @param matcher the token matcher
     */
    public void setMatcher(final TokenMatcher matcher) {
        this.matcher = matcher;
    }

    /**
     * Gets the <code>TokenProcessor</code> associated with this <code>TokenHandlerPair</code>.
     * 
     * @return the token processor
     */
    public TokenProcessor getProcessor() {
        return this.processor;
    }

    /**
     * Sets the <code>TokenProcessor</code> associated with this <code>TokenHandlerPair</code>.
     * 
     * @param processor the token processor
     */
    public void setProcessor(final TokenProcessor processor) {
        this.processor = processor;
    }

    /**
     * Tests whether or not this <code>TokenHandlerPair</code> handles tokens with
     * more than one element.
     * 
     * @return true if this <code>TokenHandlerPair</code> handles tokens with more than
     * one element, false otherwise
     */
    public boolean isMultiTokenProcessor() {
        return this.multiTokenProcessor;
    }

    /**
     * Sets whether or not this <code>TokenHandlerPair</code> handles tokens with
     * more than one element.
     * 
     * @param multiTokenProcessor true if this <code>TokenHandlerPair</code> handles tokens
     * with more than one element
     */
    public void setMultiTokenProcessor(final boolean multiTokenProcessor) {
        this.multiTokenProcessor = multiTokenProcessor;
    }
}
