/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.matcher;

import java.util.List;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * A <code>TokenMatcher</code> that matches with a configured list of 
 * <code>TokenMatcher's</code>.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CompoundTokenMatcher implements TokenMatcher {

	/**
	 * The list of <code>TokenMatcher's</code> to match against.
	 * 
	 */
    private List<TokenMatcher> matchers;
    
    /**
     * The empty constructor.
     * 
     */
    public CompoundTokenMatcher() {}
    
    /**
     * Construct a <code>CompoundTokenMatcher</code> with a list of
     * <code>TokenMatcher</code>.
     * 
     * @param matchers the list of <code>TokenMatcher's</code>
     */
    public CompoundTokenMatcher(List<TokenMatcher> matchers) {
    	this.matchers = matchers;
    }
    
    /**
     * Matches against all of the <code>TokenMatcher's</code>.
     * 
     * @param token token
     * @param string the pre-processed string
     * @param normalizedField the normalized field
     * 
     * @return true only if the given parameters match all of the <code>TokenMatcher's</code>, false otherwise
     */
    public boolean matches(Token token, String string,
            NormalizedField normalizedField) {
        for(TokenMatcher matcher: matchers) {
            if (! matcher.matches(token, string, normalizedField)) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Sets the list of <code>TokenMatcher's</code>.
     * 
     * @param matchers the list of <code>TokenMatcher's</code>
     */
    public void setMatchers(List<TokenMatcher> matchers) {
        this.matchers = matchers;
    }

}
