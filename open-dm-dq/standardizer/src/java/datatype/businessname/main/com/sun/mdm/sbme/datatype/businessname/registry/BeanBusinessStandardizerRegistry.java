/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.registry;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sun.mdm.sbme.Standardizer;

/**
 * This class loads a configured spring beans definitions xml file and
 * populates the application context for bean retrieval.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class BeanBusinessStandardizerRegistry implements BusinessStandardizerRegistry {
    private static final String beanName = "businessNameStandardizer";
    private final static String resourceFormat = "/META-INF/mdm/sbme/businessname/standardizerBeans%s.xml";
    private final Map<String, ApplicationContext> contexts = new HashMap<String, ApplicationContext>();

    /**
     * Looks for a <code>Standardizer</code> given an id.
     * 
     * @param id the variant type to lookup
     */
    public Standardizer lookup(final String id) {
        ApplicationContext context = this.contexts.get(id.toUpperCase());

        if (context == null) {
            final String resourceLocation = String.format(resourceFormat, id.toUpperCase());
            context = new ClassPathXmlApplicationContext(resourceLocation);
            this.contexts.put(id.toUpperCase(), context);
        }

        final Standardizer standardizer = (Standardizer) context.getBean(beanName);
        return standardizer;
    }

    /**
     * Retrieves a bean from the application context.
     * 
     * @param id the id of the application context
     * @param beanName the name of the bean to return
     * @return an objectified form of the bean
     */
    public Object getBean(final String id, final String beanName) {
        return this.contexts.get(id).getBean(beanName);
    }
}
