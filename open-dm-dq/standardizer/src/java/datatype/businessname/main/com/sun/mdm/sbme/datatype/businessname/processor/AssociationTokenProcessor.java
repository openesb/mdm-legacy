/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.AST;

import java.util.Map;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component used to do transformations on tokens with association type keywords.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class AssociationTokenProcessor implements TokenProcessor {

    /**
     * A map of configured associations data.
     */
    private Map<String, String> associations;

    /**
     * Constructs a new <code>AssociationTokenProcessor</code>.  Components must
     * be configured by using setter methods.
     * 
     */
    public AssociationTokenProcessor() {}
    
    /**
     * Constructs a new <code>AssociationTokenProcessor</code> configured with
     * a map of associations.
     * 
     * @param associations a map of association to configure with this component
     */
    public AssociationTokenProcessor(Map<String, String> associations) {
    	this.associations = associations;
    }
    
    /**
     * Checks to see if the token image appears in the association map, if it does, replace
     * the token image with the association.
     * 
     * @param token the token image to check
     * @param tokenImage the pre-processed and possibly transformed token image
     * @param normalizedField the normalized field
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {

    	token.setImage(tokenImage);
        /* Check if there is a standard form for this token */
        final String value = this.associations.get(tokenImage);
        if (!value.equals("0")) {
            token.setImage(value);
        }

        normalizedField.setIncompleteAlias(true);

        // Update the list that holds the different types of the tokens
        token.setInputTokenType(AST);
    }

    /**
     * Registers a map of association to be used by this component.
     * 
     * @param associations a map of associations
     */
    public void setAssociations(final Map<String, String> associations) {
        this.associations = associations;
    }

}
