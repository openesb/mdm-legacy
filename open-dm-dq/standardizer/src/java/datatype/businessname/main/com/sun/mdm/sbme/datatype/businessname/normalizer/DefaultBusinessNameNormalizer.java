/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer;

import java.util.List;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.TokenHandler;

/**
 * Default <code>BusinessNameNormalizer</code> implementation.
 * 
 */
public class DefaultBusinessNameNormalizer implements BusinessNameNormalizer {

    private TokenHandler tokenHandler;

    /**
     * The empty constructor.
     * 
     */
    public DefaultBusinessNameNormalizer() {}
    
    /**
     * Construct a <code>DefaultBusinessNameNormalized</code>.
     * 
     * @param tokenHandler the token handler used for normalization
     */
    public DefaultBusinessNameNormalizer(TokenHandler tokenHandler) {
    	this.tokenHandler = tokenHandler;
    }
    
    /**
     * Normalize raw tokens with normalized ones, if the normalized form exists.
     * 
     * @param tokens list of individual tokens to be normalized
     * @return a normalized field
     */
    public NormalizedField normalize(final List<Token> tokens) {
        final NormalizedField normalizedField = new NormalizedField();
        
        this.tokenHandler.handle(tokens, normalizedField);

        return normalizedField;
    }

    /**
     * Sets the token handler.
     * 
     * @param tokenHandler the token handler responsible for normalization
     */
    public void setTokenHandler(final TokenHandler tokenHandler) {
        this.tokenHandler = tokenHandler;
    }
}