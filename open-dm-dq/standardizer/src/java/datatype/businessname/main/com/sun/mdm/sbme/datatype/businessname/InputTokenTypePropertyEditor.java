/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import java.beans.PropertyEditorSupport;

/**
 * Property editor to help spring framework recognize strings as InputTokenTypes.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class InputTokenTypePropertyEditor extends PropertyEditorSupport {
    
    @Override
    /**
     * Given a string, convert to an InputTokenType.
     * 
     * @param text the string to convert to an InputTokenType
     */
    public void setAsText(final String text) {     
        if (text == null) {          
            this.setValue(null);
        } else {
            this.setValue(InputTokenType.getImageType(text));
        }
    }

    @Override
    /**
     * Return the InputTokenType as a string.
     * 
     * @return the string representation of the InputTokenType
     */
    public String getAsText() {
        final Object value = this.getValue();
        return value != null ? value.toString() : "";
    }
}
