/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.pattern;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.NF;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import net.java.hulp.i18n.LocalizedString;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.datatype.businessname.Localizer;
import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.Token;

/**
 * Default implementation of <code>BusinessNamePatternFinder</code> used to find the
 * best pattern with the given data sets.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class DefaultBusinessNamePatternFinder implements BusinessNamePatternFinder {

	private net.java.hulp.i18n.Logger logger;
	private Localizer localizer;

    private Map<String, Pattern> patterns;

    /**
     * The empty constructor.  Setter methods have to be used to configure components.
     */
    public DefaultBusinessNamePatternFinder() {}
    
    /**
     * Construct a new <code>DefaultbusinessNamePatternFinder</code> based on a map
     * of strings and <code>Patterns</code>.
     * 
     * @param patterns the map of patterns as loaded from the pattern table
     */
    public DefaultBusinessNamePatternFinder(Map<String, Pattern> patterns) {
    	this.patterns = patterns;
    }
    
    /**
     * Search for the best patterns (we can have more than one pattern per
     * string) for a list of tokens representing business names. The resulting
     * pattern is a subset of  the pattern table.
     * 
     * @param tokens the list of tokens comprising the business name
     * @return the best pattern given the list of tokens, according to a pre-defined
     *         pattern table
     * @throws MdmStandardizationException if the number of tokens does not match the count
     *                                     of the input pattern
     */
    public Pattern getBestPatterns(final List<Token> tokens) throws MdmStandardizationException {

        /* Number of tokens inside the record */
        final int numberOfKeys = tokens.size();
        final List<String> inputPattern = new ArrayList<String>();
        final List<String> outputPattern = new ArrayList<String>();
        boolean found = false;

        /*
         * First, search for the patterns that have the same number of tokens as
         * the input string fieldsType
         */
        for (final Pattern pattern : this.patterns.values()) {
            if (pattern.getPatternCount() == numberOfKeys) {
                inputPattern.add(pattern.getInputPattern());
                outputPattern.add(pattern.getOutputPattern());

                found = true;
            } else if (found) {
                break;
            }
        }

        final List<Integer> patternIndex = this.getPatternIndex(tokens, inputPattern);

        // The number of patterns that match the actual pattern
        final int patternSize = patternIndex.size();
        
        /*
         * Now, read-in the winning patterns. But first, verify how many winning
         * patterns we have per record. First, we handle the simple case of 1
         * pattern, then the case of multiple patterns
         */
        if (patternSize > 0) {
            // Resolve the conflict. Choose the best pattern.
            int theIndex = this.chooseBestPattern(patternIndex, inputPattern);

            return new Pattern(numberOfKeys, inputPattern.get(theIndex), outputPattern.get(theIndex));
        } else {
            final StringBuilder notFound = new StringBuilder();

            // Consider all the fields as not found
            for (int i = 0; i < numberOfKeys; i++) {
                notFound.append(NF + " ");
            }

            // There is no pattern for this business entity string
            outputPattern.add(0, notFound.toString());

            return new Pattern(numberOfKeys, null, outputPattern.get(0));
        }
    }

    /**
     * Compare the tokens' image type with the list of input patterns to find a match.
     * When a match is found, populate a list with the index of the pattern and return it.
     * 
     * @param tokens the list of tokens comprising the business name
     * @param inputPattern a list of input patterns to compare against
     * @return a list containing the index of the matched patterns
     */
    private List<Integer> getPatternIndex(final List<Token> tokens, final List<String> inputPattern) {
        boolean differentPattern = false;

        final int numberOfInputPatterns = inputPattern.size();
        final int numberOfKeys = tokens.size();
        final List<Integer> patternIndex = new ArrayList<Integer>();

        // Loop over all the input patterns and compare them with the types of
        // the string
        for (int i = 0, k = 0; i < numberOfInputPatterns; i++) {
            /* Separate the string into a list of types */
            final StringTokenizer stokenizedPattern = new StringTokenizer(inputPattern.get(i));

            /*
             * Count the number of types inside the string to insure that there
             * is no mistake
             */
            final int numberOfTypes = stokenizedPattern.countTokens();

            if (numberOfTypes != numberOfKeys) {
        		LocalizedString message = localizer.x("STD003: Number of keys in input pattern ({0}) does" +
        				" not match number of tokens ({1}).  ", numberOfTypes, numberOfKeys);
        		logger.severe(message); 
                throw new MdmStandardizationException(message.toString());
            }

            // Search for the corresponding pattern in the patterns table
            for (int j = 0; j < numberOfTypes; j++) {
                final String patternImageType = stokenizedPattern.nextToken();

                // compare the types                
                if (patternImageType.equals(tokens.get(j).getInputTokenType().toString())) {
                    differentPattern = false;
                } else {
                    differentPattern = true;
                    break;
                }
            }

            /* In case the pattern is not found, don't continue */
            if (differentPattern) {
                continue;
            }

            /* If the compared patterns match, then keep the index of the string */
            patternIndex.add(k, i);
            k++;
        }
        
        return patternIndex;
    }

    /**
     * Choose the best pattern from the pattern index list given a list of input patterns.
     * 
     * @param patternIndex a list containing indexes for possible best patterns
     * @param inputPatt a list of input patterns
     * 
     * @return an index to the best pattern 
     */
    private int chooseBestPattern(final List<Integer> patternIndex, final List<String> inputPatt) {
        return patternIndex.get(0);
    }

    /**
     * Register a map of patterns with this <code>BusinessNamePatternFinder</code>.
     * 
     * @param patterns the map of patterns to register
     */
    public void setPatterns(final Map<String, Pattern> patterns) {
        this.patterns = patterns;
    }

}
