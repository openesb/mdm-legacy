package com.sun.mdm.sbme.datatype.businessname.pattern;

import java.util.List;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.Token;

/**
 * Finds the best pattern for a given set of tokens.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public interface BusinessNamePatternFinder {

	/**
	 * Find the best pattern given a list of tokens.
	 * 
	 * @param tokens the tokens to analyze
	 * @return the best pattern for the given list of tokens
	 * @throws MdmStandardizationException on error
	 */
    public Pattern getBestPatterns(final List<Token> tokens) throws MdmStandardizationException;
}
