/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.NFG;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component which handles the case of an null or empty <code>InputTokenType</code>.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class GlueTokenProcessor implements TokenProcessor {

	/**
	 * Checks to see if the token's <code>InputTokenType</code> is null or empty, if so,
	 * marks the token's <code>InputTokenTYpe</code> as not found.
	 * 
	 * @param token the token potentially containing a null or empty <code>InputTokenType</code>
	 * @param tokenImage the pre-processed and possibly transformed token image
	 * @param normalizedField the normalized field
	 */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        if (token.getInputTokenType() == null || token.getInputTokenType().toString().length() <= 0) {
            token.setInputTokenType(NFG);
        }
        token.setImage(tokenImage);
        normalizedField.setIncompleteAlias(true);
    }

}
