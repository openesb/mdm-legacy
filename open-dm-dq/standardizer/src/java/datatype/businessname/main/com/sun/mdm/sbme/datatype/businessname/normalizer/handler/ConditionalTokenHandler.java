/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer.handler;

import java.util.List;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * <code>TokenHandler</code> implementation which conditionally invokes handle() method.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class ConditionalTokenHandler implements TokenHandler {

    private List<TokenHandlerPair> pairs;

    private int maxImageSize;
    
    private StringTransformer transformer;

    /**
     * For each token in the list of tokens, apply a <code>TokenHandlerPair</code> to that token,
     * and if that <code>TokenHandlerPair</code> matches the token, do associated processing and move
     * on to the next token.
     * 
     * @param tokens the list of tokens
     * @param normalizedField the normalized field
     */
    public void handle(final List<Token> tokens, final NormalizedField normalizedField) {

        for (int i = 0; i < tokens.size(); i++) {
            boolean handled = false;

            for (final TokenHandlerPair pair : this.pairs) {
                if (handled) {
                    break;
                }

                String originalImage = "";
                String transformedImage = "";
                if (pair.isMultiTokenProcessor()) {
                    int currentImageSize = this.maxImageSize;
                    while (currentImageSize > 0) {
                        if (i < tokens.size() - (currentImageSize - 1)) {
                        	originalImage = this.build(tokens, i, currentImageSize);
                            transformedImage = transformer.transform(originalImage);
                            if (pair.getMatcher().matches(tokens.get(i), transformedImage, normalizedField)) {
                                pair.getProcessor().process(tokens.get(i), originalImage, normalizedField);
                                this.remove(tokens, i, currentImageSize);
                                handled = true;
                                break;
                            }
                        }
                        currentImageSize--;
                    }
                } else {
                    // handle only the single token pairs
                	originalImage = tokens.get(i).getImage();
                    transformedImage = transformer.transform(originalImage);
                    if (pair.getMatcher().matches(tokens.get(i), transformedImage, normalizedField)) {
                        pair.getProcessor().process(tokens.get(i), originalImage, normalizedField);
                        break;
                    }
                }
            }
        }
    }

    private String build(final List<Token> tokens, final int from, final int to) {
        final StringBuilder builder = new StringBuilder();
        for (int k = from; k < to + from; k++) {
            builder.append(tokens.get(k).getImage());
            builder.append(" ");
        }
        return builder.toString().trim();
    }

    private void remove(final List<Token> tokens, final int from, final int to) {
        for (int k = from + 1; k < to + from; k++) {
            tokens.remove(k);
        }
    }

    /**
     * Sets the maximum image size to check for (i.e. if 2, "Banana Republic" would
     * be considered as 1 token, if 3, "International Business Machines" would be
     * considered at 1 token, etc.
     * 
     * @param maxImageSize the maximum image size
     */
    public void setMaxImageSize(final int maxImageSize) {
        this.maxImageSize = maxImageSize;
    }

    /**
     * Sets the token handler pairs.
     * 
     * @param pairs the list of <code>TokenHandlerPair's</code>.
     */
    public void setPairs(final List<TokenHandlerPair> pairs) {
        this.pairs = pairs;
    }

    /**
     * Sets a <code>StringTransformer</code>
     * 
     * @param transformer a <code>StringTransformer</code>
     */
    public void setTransformer(StringTransformer transformer) {
        this.transformer = transformer;
    }
}
