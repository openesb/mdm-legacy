/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.ALT;
import static com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType.CompanyAcronym;

import java.util.Collections;
import java.util.List;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component which transforms a token based on a registry of alias'.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class AliasTypeTokenProcessor implements TokenProcessor {

    /**
     * A registry of alias types used by this processor.
     * 
     */
    private AliasTypeRegistry aliasTypeRegistry;
    
    /**
     * A <code>StringTransformer</code> used by this processor to make any
     * necessary transformations of the token image.
     * 
     */
    private StringTransformer transformer;
    
    /**
     * Constructs a new <code>AliasTypeTokenProcessor</code>.  Components must
     * be configured by using the appropriate setter methods.
     * 
     */
    public AliasTypeTokenProcessor() {}
    
    /**
     * Constructs a new <code>AliasTypeTokenProcessor</code> configured with a alias registry
     * and a string transformer.
     * 
     * @param aliasTypeRegistry an alias registry
     * @param transformer a string transformer
     */
    public AliasTypeTokenProcessor(AliasTypeRegistry aliasTypeRegistry, StringTransformer transformer) {
    	this.aliasTypeRegistry = aliasTypeRegistry;
    	this.transformer = transformer;
    }

    /**
     * Checks if an actual name exists for the alias token image, if it does, replace the token image
     * with the actual name.
     * 
     * @param token the token image to check
     * @param tokenImage the pre-processed and possibly transformed token image
     * @param normalizedField the normalized field
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        final String transformedImage = transformer.transform(tokenImage);
        final List<String> aliasTypeList = this.aliasTypeRegistry.getActualName(transformedImage);
        Collections.reverse(aliasTypeList);

        // Append the list of companies' names to the HashMap
        for (String acronym: aliasTypeList) {
        	normalizedField.addAlias(CompanyAcronym, acronym);
        }

        token.setImage(tokenImage);

        normalizedField.setIncompleteAlias(true);
        token.setInputTokenType(ALT);
    }

    /**
     * Registers a registry of alias types to be used by this processor.
     * 
     * @param aliasTypeRegistry a registry of alias'
     */
    public void setAliasTypes(final AliasTypeRegistry aliasTypeRegistry) {
        this.aliasTypeRegistry = aliasTypeRegistry;
    }

    /**
     * Registers a <code>StringTransformer</code> to be used by this processor.
     * 
     * @param transformer a <code>StringTransformer</code>
     */
    public void setTransformer(StringTransformer transformer) {
        this.transformer = transformer;
    }

}
