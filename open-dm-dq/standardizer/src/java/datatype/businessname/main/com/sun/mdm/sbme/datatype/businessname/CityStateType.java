package com.sun.mdm.sbme.datatype.businessname;

/**
 * Describes a city/state and country code as a single entity.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CityStateType {

	private String name;
	private EntityType type;
	private String countryCode;
	
	public enum EntityType {
		CT,
		ST
	}
	
	/**
	 * The default constructor
	 * 
	 */
	public CityStateType() {}
	
	/**
	 * Parameterized constructor.
	 * 
	 * @param name city/state name
	 * @param type city or state type
	 * @param countryCode the country code belonging to the city or state
	 */
	public CityStateType(String name, EntityType type, String countryCode) {
		this.name = name;
		this.type = type;
		this.countryCode = countryCode;
	}

	/**
	 * Returns the name of the city or state.
	 * 
	 * @return name of city or state
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the city or state
	 * 
	 * @param name city or state name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the type of the entity, either a city or state
	 * 
	 * @return entity type describing whether it's a city or state 
	 */
	public EntityType getType() {
		return type;
	}

	/**
	 * Set the type of the entity, either a city or state
	 * 
	 * @param type city or state type
	 */
	public void setType(EntityType type) {
		this.type = type;
	}

	/**
	 * Gets the country code associated with the city or state
	 * 
	 * @return the country code
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the country code associated with the city or state
	 * 
	 * @param country the country code
	 */
	public void setCountryCode(String country) {
		this.countryCode = country;
	}
	
	/**
	 * Beautify.
	 * 
	 */
	public String toString() {
		return this.name + "|" + this.type + "|" + this.countryCode;
	}
}
