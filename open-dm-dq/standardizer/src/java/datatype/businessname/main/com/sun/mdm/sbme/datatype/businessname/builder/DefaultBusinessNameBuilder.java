package com.sun.mdm.sbme.datatype.businessname.builder;


import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.AST;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.IDT;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.NF;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.ORT;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.PNT;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.URL;
import static com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType.CompanyAcronym;

import java.util.List;
import java.util.StringTokenizer;

import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.datatype.businessname.BusinessName;
import com.sun.mdm.sbme.datatype.businessname.MdmBusinessName;
import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

public class DefaultBusinessNameBuilder implements BusinessNameBuilder {

    private InputTokenTypeProcessor processor;

    /**
     * Sets an <code>InputTokenTypeProcessor</code>
     * 
     * @param processor the <code>InputTokenTypeProcessor</code>
     */
    public void setProcessor(InputTokenTypeProcessor processor) {        
        this.processor = processor;
    }
	
    /**
     * Builds a <code>StandardizedRecord</code> based on a list of tokens and
     * the <code>NormalizedField</code> object.
     * 
     * @param pattern the input and output patterns
     * @param tokens a list of tokens
     * @param normalizedField the normalized field
     * @return a <code>StandardizedRecord</code> based on a list of tokens
     */
	public StandardizedRecord buildStandardizedRecord(Pattern pattern, List<Token> tokens,
            NormalizedField normalizedField) {
        StandardizedRecord record = new BusinessName();
        
        final String inputPattern = pattern.getInputPattern();
        final String outputPattern = pattern.getOutputPattern();

        /* Separate the string into a list of types */
        final StringTokenizer stok = new StringTokenizer(outputPattern);
        final int numberOfTypes = stok.countTokens();

        final BusinessNameContext context = new BusinessNameContext();
        /*
         * Loop Over all the types inside the pattern and store them inside the
         * StandBusinessName object
         */
        for (int i = 0; i < numberOfTypes; i++) {
            final String token = stok.nextToken();
            final String image = tokens.get(i).getImage();

            processor.process(token, image, context);

        }
        
        if (context.getValue(PNT).trim().length() > 0) {
        	record.set(MdmBusinessName.PRIMARY_NAME, context.getValue(PNT).trim());
        }
        
        if (context.getValue(ORT).trim().length() > 0) {
        	record.set(MdmBusinessName.ORG_TYPE_KEY, context.getValue(ORT).trim());
        }
        
        if (context.getValue(AST).trim().length() > 0) {
        	record.set(MdmBusinessName.ASSOC_TYPE_KEY, context.getValue(AST).trim());
        }
        
        if (context.getValue(IDT).trim().length() > 0) {
        	record.set(MdmBusinessName.INDUSTRY_TYPE_KEY, context.getValue(IDT).trim());
        }
        
        if (context.getValue(URL).trim().length() > 0) {
        	record.set(MdmBusinessName.URL_NAME, context.getValue(URL).trim());
        }
        
        final String notFoundType = context.getValue(NF).trim();
        if (!notFoundType.equals("")) {
            if (context.getValue(PNT).trim().equals("")) {
                record.set(MdmBusinessName.PRIMARY_NAME, notFoundType);
            } else {
                record.set(MdmBusinessName.NOT_FOUND_TYPE, notFoundType);
            }
        }

        final List<String> sectorList = normalizedField.getIndustrySectorList();        

        if (sectorList.size() > 0) {
        	//TODO: Currently, sbme takes only the last industry sector in the list.
        	//Fix this so it returns all the sectors, or the best one.
        	//For now, to be compatible with sbme, return the last one.
        	record.set(MdmBusinessName.INDUSTRY_SECTOR_LIST, sectorList.get(sectorList.size() - 1));
//        	String sectors = "";
//        	for (String sector: sectorList) {
//        		sectors += sector + ", ";
//        	}
//        	record.set(MdmBusinessName.INDUSTRY_SECTOR_LIST, sectors);
        }

        String list = MdmBusinessName.ALIAS_LIST;
        if (sectorList.size() > 1) {
            list = MdmBusinessName.INDUSTRY_SECTOR_LIST;
        }
        
        if (normalizedField.isIncompleteAlias()) {
        	if (normalizedField.containsAlias(CompanyAcronym)) {
        		record.set(list, normalizedField.getAlias(CompanyAcronym));
        	}
        }

        final StringBuilder inputPatterns = new StringBuilder("");
        String inputTypeString = "";

        if (inputPattern != null) {
            /* Separate the string into a list of types */
            final StringTokenizer inputStok = new StringTokenizer(inputPattern);
            final int numberOfInputTypes = inputStok.countTokens();

            String inputToken;
            for (int i = 0; i < numberOfInputTypes; i++) {
                inputToken = inputStok.nextToken();
                inputPatterns.append(inputToken);
                inputPatterns.append(" ");
            }

            inputTypeString = inputPatterns.toString().trim() + "|";
        }

        ((BusinessName) record).setSignature(inputTypeString.trim() + outputPattern);
        // Return the standardized object
        return record;
    }

}
