/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.parser;

import java.util.ArrayList;
import java.util.List;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.TokenPostprocessor;

/**
 * Parses the business name string into its individual components
 * 
 */
public class DefaultBusinessNameParser implements BusinessNameParser {

	/**
	 * Used to do any post processing to handle special characters.
	 */
    private TokenPostprocessor postprocessor;
    
    /**
     * Used to perform transformations on the whole record (i.e. trim spaces, 
     * upper case, etc.) before parsing.
     * 
     */
    private StringTransformer recordTransformer;

    /**
     * Used to perform transformations on individual fields (i.e. trim spaces, 
     * upper case, etc.) before parsing.
     * 
     */
    private StringTransformer fieldTransformer;

    /**
     * Breaks down the record into its basic components including any special
     * characters
     * 
     * @param rec
     *            the string to be parsed
     * @return the parsed fields
     * @throws MdmStandardizationException
     *             a generic standardization exception
     */
    public List<Token> parse(String rec) throws MdmStandardizationException {

        /*
         * Before tokenizing the string, separate commas at the beginning or end
         * of a token from the token itself
         */
        rec = recordTransformer.transform(rec);

        /* Perform a preliminary parsing */
        final List<Token> tokens = this.performPrelimParsing(rec);

        /*
         * Handle certain special characters that help pre-identify the types of
         * certain keywords
         */
        this.handleSpecialChars(tokens);

        /* Perform preliminary parsing and data typing */
        this.performFinalParsing(tokens);

        return tokens;
    }

    /**
     * Preliminary parsing that helps in handling the search for characters that
     * help identify the parsing
     * 
     * @param str
     *            the string to be pre-parsed
     * @return a list of components from the string
     */
    private List<Token> performPrelimParsing(String str) {
        final List<Token> fields = new ArrayList<Token>();
        /*
         * Parse the string and search for special characters that are
         * considered as delimiters (the space character, the tab character, 
         * the newline character, the carriage-return character, and the form-feed character).
         */
        final String[] parsedStrings = str.split("[ \t\n\r\f]");

        for (int i = 0; i < parsedStrings.length; i++) {
        	String image = fieldTransformer.transform(parsedStrings[i]).trim();
        	
        	if (image.length() > 0) {
        		fields.add(new Token(image));
        	}
        }

        return fields;
    }

    /**
     * Final parsing handles the identification of tokens after certain special
     * characters transformation
     * 
     * @param tokens list of tokens
     * @return a list of components from the string
     */
    private void performFinalParsing(final List<Token> tokens) {

    }

    /**
     * Special characters that help in the identification of the types of the
     * different tokens based on some flag variables
     * 
     * @param tokens the list of tokens
     */
    private void handleSpecialChars(final List<Token> tokens) {
        for (final Token token : tokens) {
            this.postprocessor.postprocess(token);
        }
    }
    
    /**
     * Registers a <code>TokenPostprocessor</code> component.  Used to handle
     * special characters in the record.
     * 
     * @param postprocessor the <code>TokenPostprocessor</code> to register
     */
    public void setPostprocessor(final TokenPostprocessor postprocessor) {
        this.postprocessor = postprocessor;
    }

    /**
     * Registers a <code>StringTransformer</code> component.  Used to transform
     * the record. 
     *
     * @param recordTransformer the <code>StringTransformer</code> to register
     */
    public void setRecordTransformer(StringTransformer recordTransformer) {
        this.recordTransformer = recordTransformer;
    }

    /**
     * Registers a <code>StringTransformer</code> component.  Used to transform
     * individual fields in the record.
     * 
     * @param fieldTransformer the <code>StringTransformer</code> to register
     */
    public void setFieldTransformer(StringTransformer fieldTransformer) {
        this.fieldTransformer = fieldTransformer;
    }
}