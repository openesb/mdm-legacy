package com.sun.mdm.sbme.datatype.businessname.builder;

/**
 * Interface which defines an InputTokenTypeProcessor.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public interface InputTokenTypeProcessor {

	/**
	 * Process token if matcher matches the token and add values to the context.
	 * 
	 * @param token the image after pre-processing and cleansing
	 * @param image the raw image
	 * @param context context which holds values
	 * 
	 * @return true if token is processed
	 */
    public boolean process(String token, String image, BusinessNameContext context);
}
