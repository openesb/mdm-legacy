/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;


import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component which registers an <code>InputTokenType</code> with a token.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class InputTokenTypeProcessor implements TokenProcessor {

	/**
	 * An <code>InputTokenType</code> to register.
	 */
    private InputTokenType inputTokenType;

    /**
     * The empty constructor.  Setter methods have to be used to configured components.
     */
    public InputTokenTypeProcessor() {}
    
    public InputTokenTypeProcessor(InputTokenType inputTokenType) {
    	this.inputTokenType = inputTokenType;
    }
    
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        token.setInputTokenType(this.inputTokenType);
        token.setImage(tokenImage);
    }

    public void setImageType(final InputTokenType inputTokenType) {
        this.inputTokenType = inputTokenType;
    }

}
