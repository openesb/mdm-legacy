package com.sun.mdm.sbme.datatype.businessname.matcher;

import java.util.List;
import java.util.Map;

import com.sun.mdm.sbme.datatype.businessname.CityStateType;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Responsible for matching tokens against a map of city/state types.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CityStateTokenMatcher implements TokenMatcher {

	/**
	 * The map of city/state types to match against.
	 * 
	 */
	Map<String, List<CityStateType>> types;
	
	/**
	 * The empty constructor.
	 * 
	 */
	public CityStateTokenMatcher() {}
	
	/**
	 * Construct a <code>CityStateTokenMatcher</code> based on a map of city/state types.
	 * 
	 * @param types the map of city/state types
	 */
	public CityStateTokenMatcher(Map<String, List<CityStateType>> types) {
		this.types = types;
	}
	
	/**
	 * Matches if the city/state map contains the given string.
	 * 
	 * @param token token
	 * @param string the pre-processed string
	 * @param normalizedField the normalized field
	 * 
	 * @return true if given string appears in the map of city/state types, false otherwise
	 */
	public boolean matches(Token token, String string, NormalizedField normalizedField) {
		return types.containsKey(string);
	}

	/**
	 * Sets the city/state types map.
	 * 
	 * @param types a map of city/state types
	 */
	public void setTypes(Map<String, List<CityStateType>> types) {
		this.types = types;
	}

}
