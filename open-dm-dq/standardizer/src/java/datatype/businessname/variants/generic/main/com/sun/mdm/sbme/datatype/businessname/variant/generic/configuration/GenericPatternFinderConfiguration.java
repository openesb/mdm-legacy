package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.Map;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;

import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.pattern.BusinessNamePatternFinder;
import com.sun.mdm.sbme.datatype.businessname.pattern.DefaultBusinessNamePatternFinder;

@Configuration
/**
 * Configuration class responsible for constructing a <code>BusinessNamePatternFinder</code>.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public abstract class GenericPatternFinderConfiguration {
	@Bean
   /**
     * A <code>BusinessNamePatternFidner</code> configured with map
     * consisting of <code>Pattern's</code>.
     * 
     * @return a configured <code>BusinessNamePatternFinder</code>
     */
	public BusinessNamePatternFinder patternFinder() {
		return new DefaultBusinessNamePatternFinder(patternRegistry());
	}
	
	@ExternalBean
	public abstract Map<String, Pattern> patternRegistry();
}
