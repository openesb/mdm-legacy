package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;


import java.util.ArrayList;
import java.util.List;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;

import com.sun.inti.components.string.match.OrCompositeStringMatcher;
import com.sun.inti.components.string.match.RegexStringMatcher;
import com.sun.inti.components.string.match.StringMatcher;
import com.sun.inti.components.string.transform.CompoundStringTransformer;
import com.sun.inti.components.string.transform.SimpleRegexStringTransformer;
import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.inti.components.string.transform.TrimStringTransformer;
import com.sun.inti.components.string.transform.UpperCaseStringTransformer;
import com.sun.mdm.sbme.datatype.businessname.parser.BusinessNameParser;
import com.sun.mdm.sbme.datatype.businessname.parser.DefaultBusinessNameParser;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.AmpersandTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.CommaTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.CompoundTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.ConditionalTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.DashTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.DotTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.SlashTokenPostprocessor;
import com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.TokenPostprocessor;

@Configuration
/**
 * Configuration class responsible for constructing a <code>BusinessNameParser</code>.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public abstract class GenericParserConfiguration {
	private DefaultBusinessNameParser parser;
	
	@Bean
	/**
     * A <code>BusinessNameParser</code> configured with a set of
     * <code>TokenPostprocessors'</code> and <code>StringTransformer's</code>.
     * 
     * @return a configured <code>BusinessNameParser</code>
     */
	public BusinessNameParser parser() {	
		this.parser = new DefaultBusinessNameParser();
		CompoundTokenPostprocessor postprocessor = new CompoundTokenPostprocessor();
		List<TokenPostprocessor> postprocessors = new ArrayList<TokenPostprocessor>();
		
		postprocessors.add(new ConditionalTokenPostprocessor(new RegexStringMatcher("\\."), new DotTokenPostprocessor()));
		postprocessors.add(new ConditionalTokenPostprocessor(new RegexStringMatcher("^,"), new CommaTokenPostprocessor()));
		
		List<StringMatcher> matchers = new ArrayList<StringMatcher>();
		matchers.add(new RegexStringMatcher("-"));
		matchers.add(new RegexStringMatcher("�"));
		postprocessors.add(new ConditionalTokenPostprocessor(new OrCompositeStringMatcher(matchers), new DashTokenPostprocessor()));
		
		postprocessors.add(new ConditionalTokenPostprocessor(new RegexStringMatcher("/"), new SlashTokenPostprocessor()));
		postprocessors.add(new ConditionalTokenPostprocessor(new RegexStringMatcher("\\&"), new AmpersandTokenPostprocessor()));
		
		List<StringTransformer> recordTransformers = new ArrayList<StringTransformer>();
		
		recordTransformers.add(new TrimStringTransformer());
		recordTransformers.add(new UpperCaseStringTransformer());
		recordTransformers.add(new SimpleRegexStringTransformer("[\\[\\]{}<>?*^#!~\\|\":;()]", "", true));
		recordTransformers.add(new SimpleRegexStringTransformer("^,(\\S)", ", $1", false));
		recordTransformers.add(new SimpleRegexStringTransformer("(\\S),$", "$1 ,", false));
		recordTransformers.add(new SimpleRegexStringTransformer("(\\S),(\\S)", "$1 , $2", true));
		recordTransformers.add(new SimpleRegexStringTransformer("(\\S),(\\s)", "$1 ,$2", true));
		recordTransformers.add(new SimpleRegexStringTransformer("(\\s),(\\S)", "$1, $2", true));
		
		CompoundStringTransformer recordTransformer = new CompoundStringTransformer(recordTransformers);
		
		List<StringTransformer> fieldTransformers= new ArrayList<StringTransformer>();
		
		fieldTransformers.add(new TrimStringTransformer());
		fieldTransformers.add(new SimpleRegexStringTransformer("^\\.(\\p{Alpha})", "$1", false));
		fieldTransformers.add(new SimpleRegexStringTransformer("(\\p{Alpha})\\.$", "$1", true));
		
		CompoundStringTransformer fieldTransformer = new CompoundStringTransformer(fieldTransformers);
		
		this.parser.setRecordTransformer(recordTransformer);
		this.parser.setFieldTransformer(fieldTransformer);
		
		postprocessor.setPostprocessors(postprocessors);
		this.parser.setPostprocessor(postprocessor);
		return this.parser;
	}

}
