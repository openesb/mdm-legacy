package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;

import com.sun.inti.components.string.transform.DiacriticalMarkStringTransformer;
import com.sun.mdm.sbme.datatype.businessname.BusinessNameIndustryType;
import com.sun.mdm.sbme.datatype.businessname.CityStateType;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.matcher.AliasTypeTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.AmpersandTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.CityStateTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.CompoundTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.ConnectorTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.GlueTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.ListBasedTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MapBasedTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MergerNameTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MergerTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MultiCharacterOrLetterTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.NullInputTokenTypeMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.SingleCharacterTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.StringBasedTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.TokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;
import com.sun.mdm.sbme.datatype.businessname.normalizer.BusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.normalizer.DefaultBusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.CompoundTokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.ConditionalTokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.IndustrySectorTokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.TokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.TokenHandlerPair;
import com.sun.mdm.sbme.datatype.businessname.processor.AliasTypeTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.AssociationTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.GlueTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.IndustryTypeTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.InputTokenTypeProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.MergerNameTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.MergerTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.OrganizationTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.PrimaryMergerNameTokenProcessor;

@Configuration
/**
 * Configuration class responsible for constructing a <code>BusinessNameNormalizer</code>.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public abstract class GenericNormalizerConfiguration {

	@Bean
	/**
	 * A <code>BusinessNameNormalizer</code> configured with a set of
	 * <code>TokenHandler's</code>.
	 * 
	 * @return a configured <code>BusinessNameNormalizer</code>
	 */
	public BusinessNameNormalizer normalizer() {
		DefaultBusinessNameNormalizer normalizer = new DefaultBusinessNameNormalizer();
		CompoundTokenHandler compoundTokenHandler = new CompoundTokenHandler();
		List<TokenHandler> handlers = new ArrayList<TokenHandler>();
		
		ConditionalTokenHandler conditionalTokenHandler = new ConditionalTokenHandler();
		conditionalTokenHandler.setMaxImageSize(2);
		conditionalTokenHandler.setTransformer(diacriticalMarkStringTransformer());
		
		conditionalTokenHandler.setPairs(getPairs());
		
		IndustrySectorTokenHandler industrySectorTokenHandler = new IndustrySectorTokenHandler();
		industrySectorTokenHandler.setIndustryCategoryCodes(industryCatagoryCodes());
		
		handlers.add(conditionalTokenHandler);
		handlers.add(industrySectorTokenHandler);
		
		compoundTokenHandler.setHandlers(handlers);
		normalizer.setTokenHandler(compoundTokenHandler);
		
		return normalizer;
	}
	
	private List<TokenHandlerPair> getPairs() {
		List<TokenHandlerPair> pairs = new ArrayList<TokenHandlerPair>();
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new ConnectorTokenMatcher(connectorTokens()))), new InputTokenTypeProcessor(InputTokenType.CTT)));
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new StringBasedTokenMatcher("AND"))), new InputTokenTypeProcessor(InputTokenType.GLU)));
		
		TokenHandlerPair pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>(). 
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new MapBasedTokenMatcher(primaryNames()))), 
				new PrimaryMergerNameTokenProcessor(primaryNames(), newNames(), aliasTypeRegistry(), diacriticalMarkStringTransformer()));
		pair.setMultiTokenProcessor(true);
		pairs.add(pair);
		
		pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>(). 
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new MergerNameTokenMatcher(mergerNames()))), 
				new MergerNameTokenProcessor(mergerNames(), aliasTypeRegistry(), newNames(), diacriticalMarkStringTransformer()));
		pair.setMultiTokenProcessor(true);
		pairs.add(pair);
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new ListBasedTokenMatcher(generalTerms()))), new InputTokenTypeProcessor(InputTokenType.BCT)));
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new AliasTypeTokenMatcher(aliasTypeRegistry()))), new AliasTypeTokenProcessor(aliasTypeRegistry(), diacriticalMarkStringTransformer())));

		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new ListBasedTokenMatcher(countryNames()))), new InputTokenTypeProcessor(InputTokenType.CNT)));
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new ListBasedTokenMatcher(countryNationalities()))), new InputTokenTypeProcessor(InputTokenType.NAT)));

		pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>(). 
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new CityStateTokenMatcher(cityStateNames()))), 
				new InputTokenTypeProcessor(InputTokenType.CST));
		pair.setMultiTokenProcessor(true);
		pairs.add(pair);
		
		pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>(). 
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new MapBasedTokenMatcher(industryRegistry()))), 
				new IndustryTypeTokenProcessor(industryRegistry(), adjectiveTypes(), diacriticalMarkStringTransformer()));
		pair.setMultiTokenProcessor(true);
		pairs.add(pair);
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new ListBasedTokenMatcher(adjectiveTypes()))), new InputTokenTypeProcessor(InputTokenType.AJT)));
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new MapBasedTokenMatcher(associations()))), new AssociationTokenProcessor(associations())));
		
		pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new MapBasedTokenMatcher(organizations()))), new OrganizationTokenProcessor(organizations()));
		pair.setMultiTokenProcessor(true);
		pairs.add(pair);
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new MergerTokenMatcher())), new MergerTokenProcessor(primaryNames(), mergerNames())));
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new GlueTokenMatcher())), new GlueTokenProcessor()));
		
		pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new SingleCharacterTokenMatcher()).
				addExtended(new AmpersandTokenMatcher(associations()))), new AssociationTokenProcessor(associations()));
		pair.setMultiTokenProcessor(true);
		pairs.add(pair);
		
		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new SingleCharacterTokenMatcher()).
				addExtended(new NullInputTokenTypeMatcher())), new InputTokenTypeProcessor(InputTokenType.NFC)));

		pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().
				addExtended(new MultiCharacterOrLetterTokenMatcher()).
				addExtended(new NullInputTokenTypeMatcher())), new InputTokenTypeProcessor(InputTokenType.NF)));

		return pairs;
	}
	
	
	private class ArrayListExtended<E> extends ArrayList<E> {
		private static final long serialVersionUID = 1L;

    	public ArrayListExtended<E> addExtended(E o) {
    		this.add(o);
    		return this;
    	}
    }
	
	@ExternalBean
	public abstract DiacriticalMarkStringTransformer diacriticalMarkStringTransformer();
	
	@ExternalBean
	public abstract List<String> adjectiveTypes();
	
	@ExternalBean
	public abstract AliasTypeRegistry aliasTypeRegistry();
	
	@ExternalBean
	public abstract Map<String, String> associations();
	
	@ExternalBean
	public abstract List<String> generalTerms();
	
    @ExternalBean
	public abstract Map<String, List<CityStateType>> cityStateNames();
	
    @ExternalBean
	public abstract Map<String, String> formerNames();
	
    @ExternalBean
	public abstract Map<String, String> primaryNames();
	
    @ExternalBean
	public abstract Map<String, String> mergerNames();
	
    @ExternalBean
	public abstract Map<String, String> newNames();
	
    @ExternalBean
	public abstract List<String> countryNationalities();
	
    @ExternalBean
	public abstract List<String> countryNames();
	
    @ExternalBean
	public abstract Map<String, String> industryCatagoryCodes();
	
    @ExternalBean
	public abstract Map<String, BusinessNameIndustryType> industryRegistry();
	
    @ExternalBean
	public abstract Map<String, String> organizations();
    
    @ExternalBean
    public abstract List<String> connectorTokens();
}
