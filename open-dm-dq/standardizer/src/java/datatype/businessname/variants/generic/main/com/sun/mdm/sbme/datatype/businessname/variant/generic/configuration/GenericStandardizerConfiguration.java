package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import org.springframework.config.java.annotation.Bean;
import org.springframework.config.java.annotation.Configuration;
import org.springframework.config.java.annotation.ExternalBean;

import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.datatype.businessname.DefaultBusinessNameStandardizer;
import com.sun.mdm.sbme.datatype.businessname.builder.BusinessNameBuilder;
import com.sun.mdm.sbme.datatype.businessname.normalizer.BusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.parser.BusinessNameParser;
import com.sun.mdm.sbme.datatype.businessname.pattern.BusinessNamePatternFinder;

@Configuration
/**
 * Configuration class responsible constructing a <code>Standardizer</code>.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public abstract class GenericStandardizerConfiguration {
	@Bean
	/**
     * A <code>Standardizer</code> configured with components necessary
     * to standardize a free form business name.
     * 
     * @return a configured <code>Standardizer</code> capable of standardizing
     * free form business names
     */
	public Standardizer standardizer() {
		DefaultBusinessNameStandardizer standardizer = new DefaultBusinessNameStandardizer();
		
		standardizer.setNormalizer(normalizer());
		standardizer.setParser(parser());
		standardizer.setPatternFinder(patternFinder());
		standardizer.setNameBuilder(nameBuilder());
		
		return standardizer;
	}
	
	@ExternalBean
	public abstract BusinessNameNormalizer normalizer();
	
	@ExternalBean
	public abstract BusinessNameParser parser();
	
	@ExternalBean
	public abstract BusinessNamePatternFinder patternFinder();
	
	@ExternalBean
	public abstract BusinessNameBuilder nameBuilder();
}
