package com.sun.mdm.sbme.datatype.businessname.variant.generic;


import net.java.hulp.i18n.LocalizedString;

import org.springframework.config.java.context.AnnotationApplicationContext;
import org.springframework.context.ApplicationContext;

import com.sun.mdm.sbme.AnnotationContextStandardizerFactory;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.Localizer;

/**
 * This configuration class is responsible loading all respective configurations
 * of the business name data type.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class GenericStandardizerFactory extends AnnotationContextStandardizerFactory {
	@Override
	/**
	 * Points to the packages which contain configuration classes.
	 * 
	 */
	protected String[] getConfigurationLocations() {
		return new String[] {
				"/com/sun/mdm/sbme/configuration/*Configuration.class",
				"/com/sun/mdm/sbme/datatype/businessname/configuration/*Configuration.class",
				"/com/sun/mdm/sbme/datatype/businessname/variant/generic/configuration/*Configuration.class",
		};
	}
	
	private ApplicationContext context;
    private final static String beanName = "standardizer";
	
    /**
     * Constructs a new <code>GenericStandardizerFactory</code> based on the
     * configuration classes.
     * 
     */
	public GenericStandardizerFactory() {
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		try {
			currentThread.setContextClassLoader(this.getClass().getClassLoader());
			this.context = new AnnotationApplicationContext(this.getConfigurationLocations());
		} catch (Throwable t) {
		    net.java.hulp.i18n.Logger sLog = net.java.hulp.i18n.Logger.getLogger(InputTokenType.class);
            Localizer localizer = Localizer.get();
            LocalizedString message = localizer.x("STD004: Error creating application context: {0}", t.getMessage());
            sLog.severe(message); 
			throw new RuntimeException(message.toString(), t);
		} finally {
			currentThread.setContextClassLoader(contextClassLoader);
		}
	}
	
	/**
	 * Returns a new standardizer from the application context.
	 * 
	 * @return a <code>Standardizer</code> capable of standardizing free
	 * form business names
	 */
	public Standardizer newStandardizer() {
		return (Standardizer) this.context.getBean(beanName);
	}

}
