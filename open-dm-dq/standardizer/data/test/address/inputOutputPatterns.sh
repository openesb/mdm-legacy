#!/bin/bash
grep \| test-mdm-??.txt |\
awk 'BEGIN{FS="\t"}{print $2}' |\
awk 'BEGIN{FS="|"}{for(i=1;i<=NF;i++)print $i}' |\
awk 'BEGIN{FS=":"}
    {cnt=split($1,input," ");split($2,output, " ");
     for(i=1;i<=cnt;i++)print input[i], output[i]}' |\
sort |\
uniq -c |\
awk '{print $2, $3, $1}'

