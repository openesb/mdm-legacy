#!/bin/bash

set -e

echo Normalizing and sorting input files...
for locale in au fr uk us
do
    inputFile=input-$locale.txt
    echo "    $inputFile..."

    if [ -f $inputFile.bz2 ]
    then
        bunzip2 $inputFile.bz2
    else
        tr -d '\015' < $inputFile |\
        sort -t'	' -f -u -o $inputFile
    fi
done

echo Generating pattern files...
time bash -c '(cd ../../../ ; ant build-mdm-address-pattern-files) > ant-mdm.log 2>&1'
time bash -c '(cd ../../../ ; ant build-sbme-address-pattern-files) > ant-sbme.log 2>&1'

echo Generating match files...
statFile=statFile.txt
> $statFile
for locale in au fr uk us
do
    echo "    Locale: $locale..."

    inputFile=input-$locale.txt
    matchFile=matches-$locale.txt
    mismatchFile=mismatches-$locale.txt

    sbmeFile=pattern-sbme-$locale.txt
    mdmFile=pattern-mdm-$locale.txt

    sort -t'	' -f -u $sbmeFile -o $sbmeFile
    sort -t'	' -f -u $mdmFile -o $mdmFile

    join -t'	' -1 1 -2 1 -o 1.1,1.2,2.2 "$sbmeFile" "$mdmFile" |\
    awk -v matchFile=$matchFile -v mismatchFile=$mismatchFile '
        BEGIN { FS = "\t" }
        $2 == $3 { print $1 > matchFile}
        $2 != $3 { print > mismatchFile}
    '

    totalCount=`wc -l < $inputFile`
    matchCount=`wc -l < $matchFile`
    mismatchCount=`wc -l < $mismatchFile`
    echo $locale $totalCount $matchCount $mismatchCount >> $statFile

    bzip2 -f $inputFile $sbmeFile $mdmFile
done
