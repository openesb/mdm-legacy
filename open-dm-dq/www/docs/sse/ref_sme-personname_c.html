<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
   <title>Understanding the Mural Standardization Engine</title>

   <meta http-equiv="Content-type" content="text/html; charset=iso-8859-1">
   <meta http-equiv="content-language" content="en-US">
   <meta name="keywords" content="">
   <meta name="description" content="Understanding the Mural Standardization Engine provides information to help you understand and work with the Mural Standardization Engine. It describes the two different standardization frameworks, the standardization files, how the standardization engine works with Master Index Studio and how you can customize the way data is standardized.">
   <meta name="date" content="2008-06-01">
   <meta name="author" content="Carol Thom">

 <link rel="stylesheet" type="text/css" href="https://mural.dev.java.net/css/muraldoc.css">
 </head>

<body>

   <!-- Copyright (c) 2008 Sun Microsystems, Inc. All rights reserved. -->
   <!-- Use is subject to license terms. -->

   <a name="top"></a>
   <h1>Understanding the Mural Standardization Engine</h1>
   <div class="articledate" style="margin-left: 0px;">Last Updated: June 2008</div>

   <div class="embeddedtocpane">
     <h5><span class="tocpageleft"><a href="ref_sme-standardization_c.html">Previous</a></span>
         <span class="tocpageright"><a href="ref_sme-phoneno_c.html">Next</a></span></h5>

     <h5><span class="toctitle">Contents</span></h5>
     <div class="embeddedtoc">
       <p class="toc level1"><a href="jcapssemidx.html">Understanding the Mural Standardization Engine</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-overview_c.html">Mural Standardization Engine Overview</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-standardization_c.html">Finite State Machine Framework Configuration</a></p>
<div class="onpage">
<p class="toc level1 tocsp"><a href="">FSM-Based Person Name Configuration</a></p>
<p class="toc level2"><a href="#ref_sme-pn-overview_c">Person Name Standardization Overview</a></p>
<p class="toc level2"><a href="#ref_sme-pn-fieldids_c">Person Name Standardization Components</a></p>
<p class="toc level2"><a href="#ref_sme-pn-stand_c">Person Name Standardization Files</a></p>
<p class="toc level3"><a href="#ref_sme-pn-lexicon_c">Person Name Lexicon Files</a></p>
<p class="toc level3"><a href="#ref_sme-pn-normalize_c">Person Name Normalization Files</a></p>
<p class="toc level3"><a href="#ref_sme-pn-procdef_c">Person Name Process Definition Files</a></p>
<p class="toc level2 tocsp"><a href="#ref_sme-pn-mtch-svc_c">Person Name Standardization and Master Index Studio</a></p>
<p class="toc level3"><a href="#ref_sme-person-fld_c">Person Name Processing Fields</a></p>
<p class="toc level4"><a href="#ahgcy">Person Name Standardized Fields</a></p>
<p class="toc level4"><a href="#ahgcz">Person Name Object Structure</a></p>
<p class="toc level3 tocsp"><a href="#ref_sme-pn-normcfg_c">Configuring a Normalization Structure for Person Names</a></p>
<p class="toc level3"><a href="#ref_sme-pn-stand-cfg_c">Configuring a Standardization Structure for Person Names</a></p>
<p class="toc level3"><a href="#ref_sme-pn-phoncfg_c">Configuring Phonetic Encoding for Person Names</a></p>
</div>
<p class="toc level1 tocsp"><a href="ref_sme-phoneno_c.html">FSM-Based Telephone Number Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-address_c.html">Rules-Based Address Data Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-business_c.html">Rules-Based Business Name Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-customizing_c.html">Custom FSM-Based Data Types and Variants</a></p>

     </div>
   </div>


   <div class="maincontent">
      <a name="ref_sme-personname_c"></a><h3>FSM&ndash;Based Person Name Configuration</h3><p>By default, person name data is standardized using the finite state machine (FSM)
framework. Processing person data might involve parsing free-form data fields, but normally involves
normalizing and phonetically encoding certain fields prior to matching. The following topics describe
the default configuration that defines person processing logic and provide information about modifying
mefa.xml in a master index application for processing person data.</p>
<ul><li><p><a href="#ref_sme-pn-overview_c">Person Name Standardization Overview</a></p></li>
<li><p><a href="#ref_sme-pn-fieldids_c">Person Name Standardization Components</a></p></li>
<li><p><a href="#ref_sme-pn-stand_c">Person Name Standardization Files</a></p></li>
<li><p><a href="#ref_sme-pn-mtch-svc_c">Person Name Standardization and Master Index Studio</a></p></li></ul>


<a name="ref_sme-pn-overview_c"></a>

<h2>Person Name Standardization Overview</h2>
<p>Processing data with the PersonName data type includes standardizing and matching a person&rsquo;s
demographic information. The Mural Standardization Engine can the create the parsed, normalized, and
phonetic values for person data. These values are needed for accurate searching and
matching on person data. Several configuration files designed specifically to handle person data
are included to provide processing logic for the standardization and phonetic encoding process. The
Mural Standardization Engine can phonetically encode any field you specify.</p><p>In addition, when processing person information, you might want to standardize addresses to
enable searching against address information. This requires working with the address configuration files
described in <a href="ref_sme-address_c.html">Rules&ndash;Based Address Data Configuration</a>.</p>

<a name="ref_sme-pn-fieldids_c"></a>

<h2>Person Name Standardization Components</h2>
<p>Standardization engines use tokens to determine how each field is standardized into its
individual field components and to determine how to normalize a field value. Tokens
also identify the field components to external applications like a master index application.
The following table lists each token generated by the Mural Standardization Engine for
person data along with the standardization component they represent. These correspond to the
output symbols in the process definition file and to the output fields listed
in the service type definition file. For names, you can only specify the
predefined field IDs that are listed in this table unless you customize an
existing variant or create a new one.</p><a name="ahgca"></a><h6>Table&nbsp;1 Person Name Tokens</h6><table><col width="35%"><col width="65%"><tr><th align="left" valign="top" scope="column"><p>Token</p></th>
<th align="left" valign="top" scope="column"><p>Description</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-41"></a><a name="indexterm-42"></a>firstName</p></td>
<td align="left" valign="top" scope="row"><p>Represents a first name field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-43"></a><a name="indexterm-44"></a>generation</p></td>
<td align="left" valign="top" scope="row"><p>Represents a field
containing generational information, such as Junior, II, or 3rd.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-45"></a><a name="indexterm-46"></a>lastName</p></td>
<td align="left" valign="top" scope="row"><p>Represents a last name field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-47"></a><a name="indexterm-48"></a>middleName</p></td>
<td align="left" valign="top" scope="row"><p>Represents
a middle name field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-49"></a><a name="indexterm-50"></a>nickname</p></td>
<td align="left" valign="top" scope="row"><p>Represents a nickname field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-51"></a><a name="indexterm-52"></a>salutation</p></td>
<td align="left" valign="top" scope="row"><p>Represents a field containing prefix information
for a name, such as Mr., Miss, or Mrs.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-53"></a><a name="indexterm-54"></a>title</p></td>
<td align="left" valign="top" scope="row"><p>Represents a field containing a
title, such as Doctor, Reverend, or Professor.</p></td>
</tr>
</table>

<a name="ref_sme-pn-stand_c"></a>

<h2>Person Name Standardization Files</h2>
<p><a name="indexterm-55"></a><a name="indexterm-56"></a>Several configuration files are used to define standardization logic for processing person names. You
can customize any of the configuration files described in this section to fit
your processing and standardization requirements for person data. There are three types of
standardization files for person data: process definition, lexicon, and normalization. Four default variants
on the PersonName data type are provided that are specialized for standardizing data
from France, Australia, the United Kingdom, or the United State. In a master
index project, these files appear under PersonName in the Standardization Engine node. Files
for each variant appear within sub-folders of PersonName and each corresponds to a specific
national variant.</p><p>You can customize these files to add entries of other nationalities or languages,
including those containing diacritical marks. You can also create new variants to process
data of other nationalities. For more information, see <a href="ref_sme-customizing_c.html">Custom Data Types and Variants</a>.</p><p>The following topics provide information about each type of person name standardization file:</p>
<ul><li><p><a href="#ref_sme-pn-lexicon_c">Person Name Lexicon Files</a></p></li>
<li><p><a href="#ref_sme-pn-normalize_c">Person Name Normalization Files</a></p></li>
<li><p><a href="#ref_sme-pn-procdef_c">Person Name Process Definition Files</a></p></li></ul>


<div class="indent"><a name="ref_sme-pn-lexicon_c"></a><h3>Person Name Lexicon Files</h3>
<p>Each PersonName variant contains a set of lexicon files. Each lexicon file contains
a list of possible values for a field. The standardization engine matches input
values against the values listed in these files to recognize input symbols and
ensure correct tokenization. The Mural Standardization Engine uses these files when processing input
symbols as defined in the process definition file (<tt>standardizer.xml</tt>). They are primarily used
during the token matching portion of parsing. You can modify these files as
needed by adding, deleting, or modifying values in the list. You can also
create additional lexicon files.</p><p>The PersonName data type includes the following lexicon files:</p>
<ul><li><p><tt>generation.txt</tt></p></li>
<li><p><tt>givenNames.txt</tt></p></li>
<li><p><tt>salutation.txt</tt></p></li>
<li><p><tt>surnames.txt</tt></p></li>
<li><p><tt>titles.txt</tt></p></li></ul>
<p>These files are located in the <tt>resource</tt> folder under each variant name.</p></div>


<div class="indent"><a name="ref_sme-pn-normalize_c"></a><h3>Person Name Normalization Files</h3>
<p>Each PersonName variant contains a set of normalization files that are used to
normalize input values. The Mural Standardization Engine uses these files when processing input
symbols as defined in the process definition file (<tt>standardizer.xml</tt>). Each normalization file
contains a column of unnormalized values, such as nicknames or abbreviations, and a
second column that contains the corresponding normalized values. The values in each column
are separated by a pipe symbol (|). You can modify these files as
needed by adding, deleting, or modifying values in the list. You can also
create additional normalization files to reference from the process definition file.</p><p>The PersonName data type includes the following normalization files:</p>
<ul><li><p><tt>generationNormalization.txt</tt></p></li>
<li><p><tt>givenNameNormalization.txt</tt></p></li>
<li><p><tt>salutationNormalization.txt</tt></p></li>
<li><p><tt>surnameNormalization.txt</tt></p></li>
<li><p><tt>titleNormalization.txt</tt></p></li></ul>
<p>These files are located in the <tt>resource</tt> folder under each variant name.</p></div>


<div class="indent"><a name="ref_sme-pn-procdef_c"></a><h3>Person Name Process Definition Files</h3>
<p>Each variant has its own process definition file (<tt>standardizer.xml</tt>) that defines the
state model for standardizing free-form person names. Each of these files also includes
a section that defines just normalization without parsing for person names. The process
definition file is located in the <tt>resource</tt> folder under each variant name. For
information about the structure of this file, see <a href="ref_sme-standardization_c.html#ref_sme-fsm-proc_c">Process Definition File</a>.</p><p>Person name standardization has several states, each defining how to process tokens when
they are found in certain orders. The default file defines states for salutations,
first names, middle names, last names, titles, suffixes, and separators. It defines provisions
for instances when the fields do not appear in order or when the
input string does not contain complete data. For example, the current definition handles
instances where the input string is &ldquo;FirstName, MiddleName, LastName&rdquo; as well as instances
where the input string is &ldquo;LastName, FirstName, MiddleName&rdquo;. </p><p>The process definition files for person names define several parsing rules for each
field component.  This file defines a set of cleansing rules to prepare
the input string prior to any processing. Then the data is passed to
the start state of the FSM. Most fields are preprocessed and then
matched against regular expressions or against a list of values in a lexicon
file (described in <a href="#ref_sme-pn-lexicon_c">Person Name Lexicon Files</a>). Postprocessing includes replacing regular expressions or normalizing the field value
based on a normalization file (described in <a href="#ref_sme-pn-normalize_c">Person Name Normalization Files</a>). The process definition files also
define a set of normalization rules, which are followed when the incoming data
already contains name information in separate fields and does not need to be
parsed. </p></div>


<a name="ref_sme-pn-mtch-svc_c"></a>

<h2>Person Name Standardization and Master Index Studio</h2>
<p>Master index applications rely on the Mural Standardization Engine to process person name
data. <a name="indexterm-57"></a><a name="indexterm-58"></a>To ensure correct processing of person information, you need to customize the
Matching Service for the master index application according to the rules defined for
the standardization engine. This includes modifying mefa.xml to define normalization or parsing and phonetic
encoding of the appropriate fields. You can modify mefa.xml with the Master Index
Configuration Editor in the master index project. </p><p><a name="indexterm-59"></a>Standardization is defined in the StandardizationConfig section of mefa.xml, which is described in detail
in <a href="https://open-dm-mi.dev.java.net/docs/config_concepts/cnfg_index-mefa_c.html">Match Field Configuration</a>. To configure the required fields for normalization, modify the normalization structure
in mefa.xml. To configure the required fields for parsing and normalization, modify the
standardization structure. To configure phonetic encoding, modify the phonetic encoding structure. These tasks can
all be performed using the Master Index Configuration Editor.</p><p>Generally, the person data type processes data that is parsed prior to processing,
so you should not need to configure fields to parse unless your person
data is stored in free-form text fields with all name information in one
field. When processing person data, you might also want to search on address
information. In that case, you need to configure the address fields to parse
and normalize. </p><p>The following topics provide information about the fields used in processing person data
and how to configure person data standardization for a master index application. The
information provided in these topics is based on the default configuration.</p>
<ul><li><p><a href="#ref_sme-person-fld_c">Person Name Processing Fields</a></p></li>
<li><p><a href="#ref_sme-pn-normcfg_c">Configuring a Normalization Structure for Person Names</a></p></li>
<li><p><a href="#ref_sme-pn-stand-cfg_c">Configuring a Standardization Structure for Person Names</a></p></li>
<li><p><a href="#ref_sme-pn-phoncfg_c">Configuring Phonetic Encoding for Person Names</a></p></li></ul>


<div class="indent"><a name="ref_sme-person-fld_c"></a><h3>Person Name Processing Fields</h3>
<p><a name="indexterm-60"></a><a name="indexterm-61"></a><a name="indexterm-62"></a>When standardizing person data, not all fields in a record need to be
processed by the Mural Standardization Engine. The standardization engine only needs to process
fields that must be parsed, normalized, or phonetically converted. For a master index
application, these fields are defined in mefa.xml and processing logic for each field
is defined in the standardization engine configuration files.</p>

<div class="indent"><a name="ahgcy"></a><h3>Person Name Standardized Fields</h3>
<p><a name="indexterm-63"></a><a name="indexterm-64"></a>The Mural Standardization Engine can process person data that is provided in separate
fields within a single record, meaning that no parsing is required of the
name fields prior to normalization. It can also process person data contained in
one long free-form field and parse the field into its individual components, such
as first name, last name, title, and so on. Typically, only first and
last names are normalized and phonetically encoded when standardizing person data, but the
standardization engine can normalize and phonetically encode any field you choose. By default,
the standardization engine processes these fields: first name, middle name, last name, nickname, salutation,
generational suffix, and title.</p></div>


<div class="indent"><a name="ahgcz"></a><h3>Person Name Object Structure</h3>
<p><a name="indexterm-65"></a><a name="indexterm-66"></a>The fields you specify for person name matching in the Master Index wizard
are automatically defined for standardization and phonetic encoding. If you specify the PersonFirstName or
PersonLastName match type in the  wizard, the following fields are automatically added
to the object structure and database creation script:</p>
<ul><li><p><i>field_name</i>_Std</p></li>
<li><p><i>field_name</i>_Phon</p><p>where <i>field_name</i> is the name of the field for which you specified person name matching. </p></li></ul>
<p>For example, if you specify the PersonFirstName match type for the FirstName field,
two fields, FirstName_Std and FirstName_Phon, are automatically added to the structure. You can
also add these fields manually if you do not specify match types in
the wizard. If you are parsing free-form person data, be sure all output
fields from the standardization process are included in the master index object structure.
If you store additional names in the database, such as alias names, maiden
names, parent names, and so on, you can modify the phonetic structure to
phonetically encode those names as well.</p></div>
</div>


<div class="indent"><a name="ref_sme-pn-normcfg_c"></a><h3>Configuring a Normalization Structure for Person Names</h3>
<p><a name="indexterm-67"></a><a name="indexterm-68"></a>The fields defined for normalization for the PersonName data type can include any
name fields. By default, normalization rules are defined in the process definition file for
first, middle, and last name fields, and you can easily define additional fields.
You only need to define a normalization structure for person data if you
are processing individual fields that do not require parsing. Follow the instructions under
 <a href="https://open-dm-mi.dev.java.net/docs/cnfg_index-norm_p.html">Defining Normalization Rules</a> to define fields for normalization. For the <i>standardization-type</i> element, enter <tt><b>PersonName</b></tt>. For
a list of field IDs to use in the <i>standardized-object-field-id</i> element, see <a href="#ref_sme-pn-fieldids_c">Person Name Standardization Components</a>.</p><p>A sample normalization structure for person data is shown below. This sample specifies
that the PersonName standardization type is used to normalize the first name, alias
first name, last name, and alias last name fields. For all name fields,
both United States and United Kingdom domains are defined for standardization.</p><pre>&lt;structures-to-normalize>
   &lt;group standardization-type="PersonName"
    domain-selector="com.sun.mdm.index.matching.impl.MultiDomainSelector">
      &lt;locale-field-name>Person.PobCountry&lt;/locale-field-name>
      &lt;locale-maps>
         &lt;locale-codes>
            &lt;value>UNST&lt;/value>
            &lt;locale>US&lt;/locale>
         &lt;/locale-codes>
         &lt;locale-codes>
            &lt;value>GB&lt;/value>
            &lt;locale>UK&lt;/locale>
            &lt;/locale-codes>
      &lt;/locale-maps>
      &lt;unnormalized-source-fields>
         &lt;source-mapping>
            &lt;unnormalized-source-field-name>Person.FirstName
            &lt;/unnormalized-source-field-name>
            &lt;standardized-object-field-id>FirstName
            &lt;/standardized-object-field-id>
         &lt;/source-mapping>
         &lt;source-mapping>
            &lt;unnormalized-source-field-name>Person.LastName
            &lt;/unnormalized-source-field-name>
            &lt;standardized-object-field-id>LastName
            &lt;/standardized-object-field-id>
         &lt;/source-mapping>
      &lt;/unnormalized-source-fields>
         &lt;normalization-targets>
            &lt;target-mapping>
               &lt;standardized-object-field-id>FirstName
               &lt;/standardized-object-field-id>
               &lt;standardized-target-field-name>Person.FirstName_Std
               &lt;/standardized-target-field-name>
            &lt;/target-mapping>
            &lt;target-mapping>
               &lt;standardized-object-field-id>LastName
               &lt;/standardized-object-field-id>
               &lt;standardized-target-field-name>Person.LastName_Std
               &lt;/standardized-target-field-name>
            &lt;/target-mapping>
         &lt;/normalization-targets>
      &lt;/group>
   &lt;group standardization-type="PersonName" domain-selector=
     "com.sun.mdm.index.matching.impl.MultiDomainSelector">
      &lt;locale-field-name>Person.PobCountry&lt;/locale-field-name>
      &lt;locale-maps>
         &lt;locale-codes>
            &lt;value>UNST&lt;/value>
            &lt;locale>US&lt;/locale>
         &lt;/locale-codes>
         &lt;locale-codes>
            &lt;value>GB&lt;/value>
            &lt;locale>UK&lt;/locale>
         &lt;/locale-codes>
      &lt;/locale-maps>
      &lt;unnormalized-source-fields>
         &lt;source-mapping>
            &lt;unnormalized-source-field-name>Person.Alias[*].FirstName
            &lt;/unnormalized-source-field-name>
            &lt;standardized-object-field-id>FirstName
            &lt;/standardized-object-field-id>
         &lt;/source-mapping>
         &lt;source-mapping>
            &lt;unnormalized-source-field-name>Person.Alias[*].LastName
            &lt;/unnormalized-source-field-name>
            &lt;standardized-object-field-id>LastName
            &lt;/standardized-object-field-id>
         &lt;/source-mapping>
      &lt;/unnormalized-source-fields>
      &lt;normalization-targets>
         &lt;target-mapping>
            &lt;standardized-object-field-id>FirstName
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>
            Person.Alias[*].FirstName_Std
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
         &lt;target-mapping>
            &lt;standardized-object-field-id>LastName
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>
            Person.Alias[*].LastName_Std
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
      &lt;/normalization-targets>
   &lt;/group>
&lt;/structures-to-normalize></pre></div>


<div class="indent"><a name="ref_sme-pn-stand-cfg_c"></a><h3>Configuring a Standardization Structure for Person Names</h3>
<p><a name="indexterm-69"></a><a name="indexterm-70"></a>For free&ndash;form name fields, the source fields that are defined for standardization should
include the predefined standardization components. For example, fields containing person name information can include
the first name, middle name, last name, suffix, title, and salutation. The target
fields you define can include any of these parsed components. Follow the instructions
under <a href="https://open-dm-mi.dev.java.net/docs/config/cnfg_index-stand_p.html">Defining Standardization Rules</a> to define fields for standardization. For the <i>standardization-type</i> element, enter <tt><b>PersonName</b></tt>. For
a list of field IDs to use in the <i>standardized-object-field-id</i> element, see <a href="#ref_sme-pn-fieldids_c">Person Name Standardization Components</a>.</p><p>A sample standardization structure for person name data is shown below. Only the
United States variant is defined in this structure.</p><pre>free-form-texts-to-standardize>
   &lt;group standardization-type="PERSONNAME"
    domain-selector="com.sun.mdm.index.matching.impl.SingleDomainSelectorUS">
      &lt;unstandardized-source-fields>
         &lt;unstandardized-source-field-name>Person.Name
         &lt;/unstandardized-source-field-name>
      &lt;/unstandardized-source-fields>
      &lt;standardization-targets>
         &lt;target-mapping>
            &lt;standardized-object-field-id>salutation
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>Person.Prefix
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
         &lt;target-mapping>
            &lt;standardized-object-field-id>firstName
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>Person.FirstName
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
         &lt;target-mapping>
            &lt;standardized-object-field-id>middleName
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>Person.MiddleName
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
         &lt;target-mapping>
            &lt;standardized-object-field-id>lastName
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>Person.LastName
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
         &lt;target-mapping>
            &lt;standardized-object-field-id>suffix
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>Person.Suffix
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
         &lt;target-mapping>
            &lt;standardized-object-field-id>title
            &lt;/standardized-object-field-id>
            &lt;standardized-target-field-name>Person.Title
            &lt;/standardized-target-field-name>
         &lt;/target-mapping>
      &lt;/standardization-targets>
   &lt;/group>
&lt;/free-form-texts-to-standardize></pre></div>


<div class="indent"><a name="ref_sme-pn-phoncfg_c"></a><h3>Configuring Phonetic Encoding for Person Names</h3>
<p><a name="indexterm-71"></a><a name="indexterm-72"></a><a name="indexterm-73"></a>When you specify a first, middle, or last name field for person name
matching in the Master Index wizard, that field is automatically defined for phonetic
encoding. You can define additional names, such as maiden names or alias names,
for phonetic encoding as well. Follow the instructions under <a href="https://open-dm-mi.dev.java.net/docs/config/cnfg_index-phon_p">Defining Phonetic Encoding</a> to define
fields for phonetic encoding.</p><p>A sample of fields defined for phonetic encoding is shown below. This sample
converts name and alias name fields, as well as the street name.</p><pre>&lt;phoneticize-fields>
   &lt;phoneticize-field>
      &lt;unphoneticized-source-field-name>Person.FirstName_Std
      &lt;/unphoneticized-source-field-name>
      &lt;phoneticized-target-field-name>Person.FirstName_Phon
      &lt;/phoneticized-target-field-name>
      &lt;encoding-type>Soundex&lt;/encoding-type>
   &lt;/phoneticize-field>
   &lt;phoneticize-field>
      &lt;unphoneticized-source-field-name>Person.LastName_Std
      &lt;/unphoneticized-source-field-name>
      &lt;phoneticized-target-field-name>Person.LastName_Phon
      &lt;/phoneticized-target-field-name>
      &lt;encoding-type>NYSIIS&lt;/encoding-type>
   &lt;/phoneticize-field>
   &lt;phoneticize-field>
      &lt;unphoneticized-source-field-name>Person.Alias[*].FirstName_Std
      &lt;/unphoneticized-source-field-name>
      &lt;phoneticized-target-field-name>Person.Alias[*].FirstName_Phon
      &lt;/phoneticized-target-field-name>
      &lt;encoding-type>Soundex&lt;/encoding-type>
   &lt;/phoneticize-field>
   &lt;phoneticize-field>
      &lt;unphoneticized-source-field-name>Person.Alias[*].LastName_Std
      &lt;/unphoneticized-source-field-name>
      &lt;phoneticized-target-field-name>Person.Alias[*].LastName_Phon
      &lt;/phoneticized-target-field-name>
      &lt;encoding-type>NYSIIS&lt;/encoding-type>
   &lt;/phoneticize-field>
   &lt;phoneticize-field>
      &lt;unphoneticized-source-field-name>Person.Address[*].AddressLine1_StName
      &lt;/unphoneticized-source-field-name>
      &lt;phoneticized-target-field-name>Person.Address[*].AddressLine1_StPhon
      &lt;/phoneticized-target-field-name>
      &lt;encoding-type>NYSIIS&lt;/encoding-type>
   &lt;/phoneticize-field>&lt;/phoneticize-fields></pre></div>

   </div>

   <div class="BottomPageControlPane">
      <table class="pagecontrol">
         <tr>
            <td class="pageleft">
               <a href="ref_sme-standardization_c.html">Previous</a>
            </td>
            <td class="pageright">
               <a href="ref_sme-phoneno_c.html">Next</a>
            </td>
         </tr>
      </table>

   </div>

</body>
</html>
