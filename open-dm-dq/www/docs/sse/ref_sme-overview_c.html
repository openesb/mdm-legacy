<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
   <title>Understanding the Mural Standardization Engine</title>

   <meta http-equiv="Content-type" content="text/html; charset=iso-8859-1">
   <meta http-equiv="content-language" content="en-US">
   <meta name="keywords" content="">
   <meta name="description" content="Understanding the Mural Standardization Engine provides information to help you understand and work with the Mural Standardization Engine. It describes the two different standardization frameworks, the standardization files, how the standardization engine works with Master Index Studio and how you can customize the way data is standardized.">
   <meta name="date" content="2008-06-01">
   <meta name="author" content="Carol Thom">

 <link rel="stylesheet" type="text/css" href="https://mural.dev.java.net/css/muraldoc.css">
 </head>

<body>

   <!-- Copyright (c) 2008 Sun Microsystems, Inc. All rights reserved. -->
   <!-- Use is subject to license terms. -->

   <a name="top"></a>
   <h1>Understanding the Mural Standardization Engine</h1>
   <div class="articledate" style="margin-left: 0px;">Last Updated: June 2008</div>

   <div class="embeddedtocpane">
     <h5><span class="tocpageleft"><a href="jcapssemidx.html">Previous</a></span>
         <span class="tocpageright"><a href="ref_sme-standardization_c.html">Next</a></span></h5>

     <h5><span class="toctitle">Contents</span></h5>
     <div class="embeddedtoc">
       <p class="toc level1"><a href="jcapssemidx.html">Understanding the Mural Standardization Engine</a></p>
<div class="onpage">
<p class="toc level1 tocsp"><a href="">Mural Standardization Engine Overview</a></p>
<p class="toc level2"><a href="#ref_sme-stand-concept_c">Standardization Concepts</a></p>
<p class="toc level3"><a href="#ref_sme-stand-parse_c">Data Parsing or Reformatting</a></p>
<p class="toc level3"><a href="#ref_sme-stand-norm_c">Data Normalization</a></p>
<p class="toc level3"><a href="#ref_sme-stand-phon_c">Phonetic Encoding</a></p>
<p class="toc level2 tocsp"><a href="#ref_sme-frame-over_c">How the Mural Standardization Engine Works</a></p>
<p class="toc level3"><a href="#ref_sme-data-type_c">Mural Standardization Engine Data Types and Variants</a></p>
<p class="toc level3"><a href="#ref_sme-fieldid_c">Mural Standardization Engine Standardization Components</a></p>
<p class="toc level3"><a href="#ggqeh">Finite State Machine Framework</a></p>
<p class="toc level4"><a href="#ggqfc">About the Finite State Machine Framework</a></p>
<p class="toc level4"><a href="#ggqfc2">FSM-Based Configuration</a></p>
<p class="toc level3 tocsp"><a href="#ggqie">Rules-Based Framework</a></p>
<p class="toc level4"><a href="#ggqfr">About the Rules-Based Framework</a></p>
<p class="toc level4"><a href="#ggqfr2">Rules-Based Configuration</a></p>
<p class="toc level2 tocsp"><a href="#ref_sme-stand-match_c">Master Index Studio Standardization and Matching Process</a></p>
<p class="toc level2"><a href="#ref_sme-domains_c">Mural Standardization Engine Internationalization</a></p>
</div>
<p class="toc level1 tocsp"><a href="ref_sme-standardization_c.html">Finite State Machine Framework Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-personname_c.html">FSM-Based Person Name Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-phoneno_c.html">FSM-Based Telephone Number Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-address_c.html">Rules-Based Address Data Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-business_c.html">Rules-Based Business Name Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-customizing_c.html">Custom FSM-Based Data Types and Variants</a></p>

     </div>
   </div>


   <div class="maincontent">
      <a name="ref_sme-overview_c"></a><h3>Mural Standardization Engine Overview</h3><p>The Mural Standardization Engine is designed to work with the master index
applications created by Master Index Studio. The standardization engine can also be called
from other applications, such as Data Integrator, web services, web applications, and so on.
It is highly configurable in the Master Index Studio environment and can be
used to standardize various types of data. The Mural Standardization Engine works in
conjunction with the Mural Match Engine to improve the quality of your
data.</p><p>The following topics provide information about standardization concepts, the standardization process, and the
Mural Standardization Engine frameworks.</p>
<ul><li><p><a href="#ref_sme-stand-concept_c">Standardization Concepts</a></p></li>
<li><p><a href="#ref_sme-frame-over_c">How the Mural Standardization Engine Works</a></p></li>
<li><p><a href="#ref_sme-stand-match_c">Master Index Studio Standardization and Matching Process</a></p></li>
<li><p><a href="#ref_sme-domains_c">Mural Standardization Engine Internationalization</a></p></li></ul>


<a name="ref_sme-stand-concept_c"></a>

<h2>Standardization Concepts</h2>
<p>Data standardization transforms input data into common representations of values to give you
a single, consistent view of the data stored in and across organizations. Standardizing
the data stored in disparate systems provides a common representation of the data
so you can easily and accurately compare data between systems. </p><p>Data <a name="indexterm-2"></a>standardization applies three transformations against the data: parsing into individual components, normalization, and
phonetic encoding. These actions help cleanse data to prepare it for matching and
searching. Some fields might require all three steps, some just normalization and phonetic
conversion, and other data might only need phonetic encoding. Typically data is first
parsed, then normalized, and then phonetically encoded, though some cleansing might be needed
prior to parsing.</p><p>Standardization can include any one or any combination of the following phases.</p>
<ul><li><p><a href="#ref_sme-stand-parse_c">Data Parsing or Reformatting</a></p></li>
<li><p><a href="#ref_sme-stand-norm_c">Data Normalization</a></p></li>
<li><p><a href="#ref_sme-stand-phon_c">Phonetic Encoding</a></p></li></ul>


<div class="indent"><a name="ref_sme-stand-parse_c"></a><h3>Data Parsing or Reformatting</h3>
<p><a name="indexterm-3"></a><a name="indexterm-4"></a><a name="indexterm-5"></a><a name="indexterm-6"></a>If incoming records contain data that is not formatted properly, it must be
reformatted before it can be normalized. This process identifies and separates each component
of a free-form text field that contains multiple pieces of information. Reformatting can
also include removing characters or strings from a field that are not relevant
to the data. A good example is standardizing free-form text address fields. If
you are comparing or searching on street addresses that are contained in one
or more free-form text fields (that is, the street address is contained in
one field, apartment number in another, and so on), those fields need to
be parsed into their individual components, such as house number, street name, street
type, and street direction. Then certain components of the address, such as the
street name and type, can be normalized. Field components are also known as
tokens, and the process of separating data into its tokens is known as
tokenization.</p></div>


<div class="indent"><a name="ref_sme-stand-norm_c"></a><h3>Data Normalization</h3>
<p><a name="indexterm-7"></a><a name="indexterm-8"></a>Normalizing data converts it into a standard or common form. A common use
for normalization is to convert nicknames into their standard names, such as converting
&ldquo;Rich&rdquo; to &ldquo;Richard&rdquo; or &ldquo;Meg&rdquo; to &ldquo;Margaret&rdquo;. Another example is normalizing street address
components. For example, both &ldquo;Dr.&rdquo; or &ldquo;Drv&rdquo; in a street address might be normalized
to &ldquo;Drive&rdquo;. Normalized values are obtained from lookup tables. Once a field value
is normalized, that value can be more accurately compared against values in other
records to determine whether they are a match.</p></div>


<div class="indent"><a name="ref_sme-stand-phon_c"></a><h3>Phonetic Encoding</h3>
<p><a name="indexterm-9"></a><a name="indexterm-10"></a><a name="indexterm-11"></a><a name="indexterm-12"></a>Once data has gone through any necessary reformatting and normalization, it can be
phonetically encoded. In a master index application, phonetic values are generally used in
blocking queries in order to obtain all possible matches to an incoming record.
They are also used to perform searches from the Master Index Data Manager
(MIDM) that allow for misspellings and typographic errors. Typically, first names use Soundex
encoding and last names and street names use NYSIIS encoding, but the Mural
Standardization Engine supports several additional phonetic encoders as well.</p></div>


<a name="ref_sme-frame-over_c"></a>

<h2>How the Mural Standardization Engine Works</h2>
<p><a name="indexterm-13"></a><a name="indexterm-14"></a><a name="indexterm-15"></a>The Mural Standardization Engine uses two frameworks to define standardization logic. One framework
is based on a finite state machine (FSM) model and the other is
based on rules programmed in Java. In the current implementation, the person names
and telephone numbers are processed using the FSM framework, and addresses and business
names are processed using the rules-based framework. The Mural Standardization Engine includes several
sets of files that define standardization logic for all supported data types. For
person data and addresses, one set of standardization files is provided for the
following national variants: Australia, France, Great Britain, and the United States. You can customize
these files to adapt the standardization and matching logic to your specific needs
or you can create new data types or variants for even more customized
processing. With pluggable standardization sets, you can define custom standardization processing for most
types of data.</p><p>The following topics provide information about the Mural Standardization Engine, the standardization frameworks,
and data is standardized:</p>
<ul><li><p><a href="#ref_sme-data-type_c">Mural Standardization Engine Data Types and Variants</a></p></li>
<li><p><a href="#ref_sme-fieldid_c">Mural Standardization Engine Standardization Components</a></p></li>
<li><p><a href="#ggqeh">Finite State Machine Framework</a></p></li>
<li><p><a href="#ggqie">Rules-Based Framework</a></p></li></ul>


<div class="indent"><a name="ref_sme-data-type_c"></a><h3>Mural Standardization Engine Data Types and Variants</h3>
<p>A data type is the primary kind of data you are processing,
such as person names, addresses, business names, automotive parts, and so on. A
variant is a subset of a data type that is designed to standardize
a specific kind of data. For example, for addresses and names, the variants
typically define rules for the different countries in which the data originates. For
automotive parts, the variants might be different manufacturers. Each data type and variant uses
its own configuration files to define how fields in incoming records are parsed,
standardized, and classified for processing. Data types are sometimes referred to as <b>standardization types</b>.</p><p><a name="indexterm-16"></a>In the default implementation with a master index application, the engine supports data
standardization on the following types of data:</p>
<ul><li><p>Person Information (described in <a href="ref_sme-personname_c.html">FSM&ndash;Based Person Name Configuration</a>)</p></li>
<li><p>Telephone Numbers (described in <a href="ref_sme-phoneno_c.html">FSM&ndash;Based Telephone Number Configuration</a>)</p></li>
<li><p>Street Addresses (described in <a href="ref_sme-address_c.html">Rules&ndash;Based Address Data Configuration</a>)</p></li>
<li><p>Business Names (described in <a href="ref_sme-business_c.html">Rules-Based Business Name Configuration</a>)</p></li></ul>
<p>In the default configuration, the standardization engine expects street address and business names
to be in free-form text fields that need to be parsed prior to
normalization and phonetic encoding. Person and phone information can also be contained in
free-form text fields, but theses types of information can also be processed if
the data is already parsed into its individual components. Each data type requires
specific customization to mefa.xml in the master index project. This can be done
by modifying the file directly or by using the Master Index Configuration Editor.</p></div>


<div class="indent"><a name="ref_sme-fieldid_c"></a><h3>Mural Standardization Engine Standardization Components</h3>
<p>The Mural Standardization Engine breaks down fields into various components during the parsing
process. This is known an tokenization. For example, it breaks addresses into floor
number, street number, street name, street direction, and so on. Some of these
components are similar and might be stored in the same output field. In
the default configuration for a master index application, for example, when the standardization
engine finds a house number, rural route number, or PO box number, the
value is stored in the HouseNumber database field. You can customize this as
needed, as long as any field you specify to store a component is
also included in the object structure defined for the master index application.</p><p><a name="indexterm-17"></a><a name="indexterm-18"></a>The standardization engine uses tokens to determine how to process fields that are defined
for normalization or parsing into their individual standardization components. For FSM-based data types,
the tokens are defined as output symbols in the process definition files and
are referenced in the standardization structures in the Master Index Configuration Editor and
in mefa.xml. The tokens determine how each field is normalized or how a
free-form text field is parsed and normalized. For rules-based data types, the tokens
are defined internally in the Java code. The tokens for business names specify
which business type key file to use to normalize a specific standardization component.
The tokens for addresses determine which database fields store each standardization component and how
each component is standardized. </p></div>


<div class="indent"><a name="ggqeh"></a><h3>Finite State Machine Framework</h3>
<p>A finite state machine (FSM) is composed of one or more states
and the transitions between those states. The Mural Standardization Engine FSM framework is
designed to be highly configurable and can be easily extended with no Java
coding. The following topics describe the FSM framework and the configuration files that define
FSM&ndash;based standardization.</p>

<div class="indent"><a name="ggqfc"></a><h3>About the Finite State Machine Framework</h3>
<p>In an FSM framework, the standardization process is defined as one or more
states. In a state, only the input symbols defined for that state
are recognized. When one of those symbols is recognized, the following action or transition
is based on configurable processing rules. For example, when an input symbol is
recognized, it might be preprocessed by removing punctuation, matched against a list of
tokens, and then postprocessed by normalizing the input value. Once this has been
completed for all input symbols, the standardization engine determines which token is the
most likely match.</p><p>FSM-based processing includes the following steps:</p>
<ul><li><p><b>Cleansing</b> &ndash; The entire input string is modified to make sure it is broken down into its individual components correctly.</p></li>
<li><p><b>Tokenization</b> &ndash; The input string is broken down into its individual components.</p></li>
<li><p><b>Parsing</b> &ndash; The individual field components are processed according to configurable rules. Parsing can include any combination of the following three stages:</p>
<ul><li><p><b>Preprocessing</b> &ndash; Each token is cleansed prior to matching to make the value more uniform. </p></li>
<li><p><b>Matching</b> &ndash; The cleansed token is matched against patterns or value lists.</p></li>
<li><p><b>Postprocessing</b> &ndash; The matched token is normalized.</p>
<hr><p><b>Note - </b>Several parsing sequences might be performed against one field component in order to best match it with a token. Each sequence is carried out until a match is made.</p>
<hr>
</li></ul>
</li>
<li><p><b>Ambiguity Resolution</b> &ndash; Some input strings might match more than one processing rule, so the FSM framework includes a probability-based mechanism for determining the correct state transition.</p></li></ul>
<p>Using the person data type, for example, first names such as &ldquo;Bill&rdquo; and
&ldquo;Will&rdquo; are normalized to &ldquo;William&rdquo;, which is then converted to its phonetic equivalent.
Standardization logic is defined in the standardization engine configuration files and in the
Master Index Configuration Editor or mefa.xml in a master index project. </p></div>


<div class="indent"><a name="ggqfc2"></a><h3>FSM-Based Configuration</h3>
<p>The FSM-based standardization configuration files are stored in the master index project and
appear in the Standardization Engine node of the project. These files are separated
into groups based on the primary data types being processed. Data type groups
have further subsets of configuration files based on the variants for each data
type. FSM-based data types and variants, such as PersonName and PhoneNumber, include the
following configuration file types.</p>
<ul><li><p><b>Service Definition Files</b> &ndash; Each data type and data type variant is defined by a service definition file. Service <b>type</b> files define the fields to be standardized for a data type and service <b>instance</b> files define the variant and Java factory class for the variant. Both files are in XML format and should not be modified unless the data type is extended to include more output symbols.</p></li>
<li><p><b>Process Definition Files</b> &ndash; These files define the different stages of processing data for the data type or variant. It defines the FSM states, input and output symbols, patterns, and data cleansing rules. These files use a domain-specific language (DSL) to define how the data fields are processed.</p></li>
<li><p><b>Lexicon Files</b> &ndash; The standardization engine uses these files to recognize input data. A lexicon provides a list of possible values for a specific field, and one lexicon file should be defined for each field on which standardization is performed.</p></li>
<li><p><b>Normalization Files</b> &ndash; The standardization engine uses these files to convert nonstandard values into a common form. For example, a nickname file provides a list of nicknames along with the common version of each name. For example, &ldquo;Beth&rdquo; and &ldquo;Liz&rdquo; might both be normalized to &ldquo;Elizabeth&rdquo;. Each row in the file contains a nickname and its corresponding normalized version separated by a pipe character (|).</p></li></ul>
</div>
</div>


<div class="indent"><a name="ggqie"></a><h3>Rules-Based Framework</h3>
<p>In the rules-based framework, the standardization process is define in the underlying Java
code. You can configure several aspects of the standardization process, such as the
detectable patterns for each data type, how values are normalized, and how the
input string is cleansed and parsed. You can define custom rules-based data types
and variants by creating custom Java packages that define processing. </p>

<div class="indent"><a name="ggqfr"></a><h3>About the Rules-Based Framework</h3>
<p>In the rules-based framework, individual field components are recognized by the patterns defined
for each data type and by information provided in configurable files about how
to preprocess, match, and postprocess each field components. The rules-based framework processes data
in the following stages. </p>
<ul><li><p><a name="indexterm-19"></a><a name="indexterm-20"></a><b>Parsing</b> - A free-form text field is separated into its individual components, such as street address information or a business name. This process takes into account logic you can customize, such as token patterns, special characters, and priority weights for patterns. </p></li>
<li><p><a name="indexterm-21"></a><a name="indexterm-22"></a><b>Normalization</b> - Once a field is parsed, individual components of the field are normalized based on the configuration files. This can include changing the input street name to a common form or changing the input business name to its official form.</p></li>
<li><p><a name="indexterm-23"></a><a name="indexterm-24"></a><b>Phonetic Encoding</b> - After a field is parsed and optionally normalized, the value of a field is converted to its phonetic version. The value to be converted can be the original input value, a parsed value, a normalized value, or a parsed and normalized value.</p></li></ul>
<p>Using the street address data type, for example, street addresses are parsed into
their component parts, such as house numbers, street names, and so on. Certain
fields are normalized, such as street name, street type, and street directions. The
street name is then phonetically converted. Standardization logic is defined in the standardization
engine configuration files and in the Master Index Configuration Editor or mefa.xml in
a master index project. </p></div>


<div class="indent"><a name="ggqfr2"></a><h3>Rules-Based Configuration</h3>
<p>The rules-based standardization configuration files are stored in the master index project and
appear as nodes in the Standardization Engine node of the project. These files
are separated into groups based on the primary data types and variants being
processed. <a name="indexterm-25"></a>Rules-based data types and variants, such as the default Address and Business
Name types, use the following configuration file types.</p>
<ul><li><p><b>Service Definition Files</b> &ndash; Each data type and data type variant is configured by a service definition file. Service type files define the fields to be standardized for a data type, and service instance definition files define the variant and Java factory class for the variant. Both files are in XML format. These files should not be modified.</p></li>
<li><p><a name="indexterm-26"></a><a name="indexterm-27"></a><b>Category Files</b> - The standardization engine uses category files when processing business names. These files list common values for certain types of data, such as industries and organizations for business names. Category files also define standardized versions of each term or classify the terms into different categories, and some files perform both functions. When processing address files, category files named <b>clues files</b> are used.</p></li>
<li><p><a name="indexterm-28"></a><a name="indexterm-29"></a><b>Clues Files</b> - The standardization engine uses clues files when processing address data types. These files list general terms used in street address fields, define standardized versions of each term, and classify the terms into various component types using predefined address tokens. These files are used by the standardization engine to determine how to parse a street address into its various components. Clues files provide clues in the form of tokens to help the engine recognize the component type of certain values in the input fields.</p></li>
<li><p><a name="indexterm-30"></a><a name="indexterm-31"></a><a name="indexterm-32"></a><b>Patterns Files</b> - The patterns files specify how incoming data should be interpreted for standardization based on the format, or pattern, of the data. These files are used only for processing data contained in free-form text fields that must be parsed prior to matching (such as street address fields or business names). Patterns files list possible input data patterns, which are encoded in the form of tokens. Each token signifies a specific component of the free-form text field. For example, in a street address field, the house number is identified by one token, the street name by another, and so on. Patterns files also define the format of the output fields for each input pattern.</p></li>
<li><p><a name="indexterm-33"></a><a name="indexterm-34"></a><b>Key Type Files</b> - For business name processing, the standardization engine refers to a number of key type files for processing data. These files generally define standard versions of terms commonly found in business names and some classify these terms into various components or industries. These files are used by the standardization engine to determine how to parse a business name into its different components and to recognize the component type of certain values in the input fields.</p></li>
<li><p><a name="indexterm-35"></a><a name="indexterm-36"></a><b>Reference Files</b> - Reference files define general terms that appear in input fields for each data type. Some reference files define terms to ignore and some define terms that indicate the business name is continuing. For example, in business name processing &ldquo;and&rdquo; is defined as a joining term. This helps the standardization engine to recognize that the primary business name in &ldquo;Martin and Sons, Inc.&rdquo; is &ldquo;Martin and Sons&rdquo; instead of just &ldquo;Martin&rdquo;. Reference files can also define characters to be ignored by the standardization engine.</p></li></ul>
</div>
</div>


<a name="ref_sme-stand-match_c"></a>

<h2>Master Index Studio Standardization and Matching Process</h2>
<p><a name="indexterm-37"></a><a name="indexterm-38"></a>In a default Master Index Studio implementation, the master index application uses the
Mural Match Engine and the Mural Standardization Engine to cleanse data in real
time. The standardization engine uses configurable pattern-matching logic to identify data and reformat
it into a standardized form. The match engine uses a matching algorithm with
a proven methodology to process and weight records in the master index database.
By incorporating both standardization and matching capabilities, you can condition data prior to
matching. You can also use these capabilities to review legacy data prior to
loading it into the database. This review helps you determine data anomalies, invalid
or default values, and missing fields.</p><p>In a master index application, both matching and standardization occur when two records
are analyzed for the probability of a match. Before matching, certain fields are
normalized, parsed, or converted into their phonetic values if necessary. The match fields
are then analyzed and weighted according to the rules defined in a match
configuration file. The weights for each field are combined to determine the overall
matching weight for the two records. After these steps are complete, survivorship is
determined by the master index application based on how the overall matching weight
compares to the duplicate and match thresholds of the master index application.</p><p>In a master index application, the standardization and matching process includes the following
steps:</p>
<ol><li><p>The master index application receives an incoming record.</p></li>
<li><p>The Mural Standardization Engine standardizes the fields specified for parsing, normalization, and phonetic encoding. These fields are defined in mefa.xml and the rules for standardization are defined in the standardization engine configuration files.</p></li>
<li><p>The master index application queries the database for a candidate selection pool (records that are possible matches) using the blocking query specified in master.xml. If the blocking query uses standardized or phonetic fields, the criteria values are obtained from the database.</p></li>
<li><p>For each possible match, the master index application creates a match string (based on the match columns in mefa.xml) and sends the string to the Mural Match Engine.</p></li>
<li><p>The Mural Match Engine checks the incoming record against each possible match, producing a matching weight for each. Matching is performed using the weighting rules defined in the match configuration file.</p></li></ol>


<a name="ref_sme-domains_c"></a>

<h2>Mural Standardization Engine Internationalization</h2>
<p>By default, the Mural Standardization Engine is configured for addresses and names originating
from Australia, France, Great Britain, and the United States, and for telephone numbers
and business names of any origin. Each national variant for each data type
uses a specific subset of configuration files. In addition, you can define custom
national variants for the standardization engine to support addresses and names from other
countries and to support other data types. You can process with your data
using the standardization files for a single variant or you can use multiple
variants depending on how the master index application is configured.</p>
   </div>

   <div class="BottomPageControlPane">
      <table class="pagecontrol">
         <tr>
            <td class="pageleft">
               <a href="jcapssemidx.html">Previous</a>
            </td>
            <td class="pageright">
               <a href="ref_sme-standardization_c.html">Next</a>
            </td>
         </tr>
      </table>

   </div>

</body>
</html>
