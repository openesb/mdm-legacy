<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
   <title>Understanding the Mural Match Engine </title>

   <meta http-equiv="Content-type" content="text/html; charset=iso-8859-1">
   <meta http-equiv="content-language" content="en-US">
   <meta name="keywords" content="">
   <meta name="description" content="Understanding the Mural Match Engine provides information to help you understand and work with the Mural Match Engine. It describes the configuration files and how you can customize the way data is matched. It also lists and describes each matching comparator function and explains how they can be customized. This guide also provides general information on determining match and duplicate weight thresholds.">
   <meta name="date" content="2008-06-01">
   <meta name="author" content="Carol Thom">

   <link rel="stylesheet" type="text/css" href="https://mural.dev.java.net/css/muraldoc.css">
   </head>

<body>

   <!-- Copyright (c) 2008 Sun Microsystems, Inc. All rights reserved. -->
   <!-- Use is subject to license terms. -->

   <a name="top"></a>
   <h1>Understanding the Mural Match Engine </h1>
   <div class="articledate" style="margin-left: 0px;">Last Updated: June 2008</div>

   <div class="embeddedtocpane">
     <h5><span class="tocpageleft"><a href="ref_sme-plugins_c.html">Previous</a></span>
         <span class="tocpageright"><a href="ref_sme-analysis_c.html">Next</a></span></h5>

     <h5><span class="toctitle">Contents</span></h5>
     <div class="embeddedtoc">
       <p class="toc level1"><a href="landingpage.html">Understanding the Mural Match Engine</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-overview_c.html">Mural Match Engine Overview</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-match_c.html">Mural Match Engine Matching Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-comparators_c.html">Mural Match Engine Comparison Functions</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-plugins_c.html">Creating Custom Comparators for the Mural Match Engine</a></p>
<div class="onpage">
<p class="toc level1 tocsp"><a href="">Mural Match Engine Configuration for Common Data Types</a></p>
<p class="toc level2"><a href="#ref_sme-matchstring_c">The Master Index Match String</a></p>
<p class="toc level2"><a href="#ref_sme-mstring-fld_c">Mural Match Engine Match String Fields</a></p>
<p class="toc level3"><a href="#ref_sme-pn-string_c">Person Data Match String Fields</a></p>
<p class="toc level3"><a href="#ref_sme-add-string_c">Address Data Match String Fields</a></p>
<p class="toc level3"><a href="#ref_sme-bn-string_c">Business Name Match String Fields</a></p>
<p class="toc level2 tocsp"><a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a></p>
<p class="toc level2"><a href="#ref_sme-mtch_svc_c">Configuring the Match String for a Master Index Application</a></p>
<p class="toc level3"><a href="#ref_sme-prsn-mtchstr_c">Configuring the Match String for Person Data</a></p>
<p class="toc level3"><a href="#ref_sme-add-mtchstr_c">Configuring the Match String for Address Data</a></p>
<p class="toc level3"><a href="#ref_sme-bus-mistring_c">Configuring the Match String for Business Names</a></p>
</div>
<p class="toc level1 tocsp"><a href="ref_sme-analysis_c.html">Fine-Tuning Weights and Thresholds for Master Index Studio</a></p>

     </div>
   </div>


   <div class="maincontent">
      <a name="ref_sme-person_c"></a><h3>Mural Match Engine Configuration for Common Data Types</h3><p>The Mural Match Engine can match on any type of data. Common
data types for matching include person names, addresses, and business names. Configuring the match
engine for matching on these data types in a master index application includes
modifying the match configuration file (<tt>matchConfigFile.cfg</tt>) and mefa.xml.</p><p>The following topics provides information about configuring the Mural Match Engine and the
master index application:</p>
<ul><li><p><a href="#ref_sme-matchstring_c">The Master Index Match String</a></p></li>
<li><p><a href="#ref_sme-mstring-fld_c">Mural Match Engine Match String Fields</a></p></li>
<li><p><a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a></p></li>
<li><p><a href="#ref_sme-mtch_svc_c">Configuring the Match String for a Master Index Application</a></p></li></ul>


<a name="ref_sme-matchstring_c"></a>

<h2>The Master Index Match String</h2>
<p><a name="indexterm-323"></a><a name="indexterm-324"></a><a name="indexterm-325"></a><a name="indexterm-326"></a><a name="indexterm-327"></a>The data string that is passed to the Mural Match Engine for
match processing is called the <b>match string</b>. For a master index application , the match
string is defined in the MatchingConfig section of mefa.xml. The match and standardization
engine configuration files, the blocking query, and the matching configuration are closely linked
in the search and matching processes. The blocking query defines the select statements
for creating the candidate selection pool during the matching process. The matching configuration defines
the match string that is passed to the match engine from the
records in the candidate selection pool. Finally, the Mural Match Engine configuration files define
how the match string is processed.</p><p>The Mural Match Engine configuration files are dependent upon the match string, and
it is very important when you modify the match string to ensure
that the match type you specify corresponds to the correct row in the
match configuration file (<tt>matchConfigFile.cfg</tt>). For example, if you are using person matching and add
&ldquo;MaritalStatus&rdquo; as a match field, you need to specify a match type for
the MaritalStatus field that is listed in the first column of the match
configuration file. You must also make sure that the matching logic defined in
the corresponding row of the match configuration file is defined appropriately for matching
on the MaritalStatus field. For more information about match types, see <a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a>).</p>

<a name="ref_sme-mstring-fld_c"></a>

<h2>Mural Match Engine Match String Fields</h2>
<p><a name="indexterm-328"></a><a name="indexterm-329"></a><a name="indexterm-330"></a>In a master index application, the match string processed by the Mural Match
Engine is defined by the match fields specified in mefa.xml, and the logic
for how the fields are matched is defined in the match configuration file
(<tt>matchConfigFile.cfg</tt>). The match engine can process any combination of fields you specify for
matching using the predefined comparators or any new comparators you define. Not all fields
in a record need to be processed by the Mural Match Engine.
Before you define the match string, analyze your data to determine the fields
that are most likely to indicate a match or non-match between two records.
</p><p>The following topics provide additional information about the match string for different data
types:</p>
<ul><li><p><a href="#ref_sme-pn-string_c">Person Data Match String Fields</a></p></li>
<li><p><a href="#ref_sme-add-string_c">Address Data Match String Fields</a></p></li>
<li><p><a href="#ref_sme-bn-string_c">Business Name Match String Fields</a></p></li></ul>


<div class="indent"><a name="ref_sme-pn-string_c"></a><h3>Person Data Match String Fields</h3>
<p><a name="indexterm-331"></a><a name="indexterm-332"></a><a name="indexterm-333"></a>By default, the match configuration file (<tt>matchConfigFile.cfg</tt>) includes rows specifically for matching
on first name, last name, social security numbers, and dates (such as a
date of birth). It also includes a row for matching a single character
with logic specialized for a gender field. You can use any of the
existing rows for matching or you can add rows for the fields you
want to match. When matching on person names, determine whether you want to
use the original field values, the normalized field values, or the phonetic values.
The match engine can handle any of these types of fields, but the
best comparator for each type might be different. Also determine how much weight
you want to give each field type and configure the match configuration file
accordingly.</p></div>


<div class="indent"><a name="ref_sme-add-string_c"></a><h3>Address Data Match String Fields</h3>
<p>By default, the match configuration file (<tt>matchConfigFile.cfg</tt>) includes rows specifically for matching on
the fields that are parsed from the street address fields, such as the
street number, street direction, and so on. The file also defines several generic
match types you can configure for address fields. You can use any of
the existing rows for matching or you can add rows for the fields
you want to match.  If you specify an &ldquo;Address&rdquo; match type for
any field in the Master Index Wizard, the default fields that store the
parsed data are automatically added to the match string in mefa.xml. These fields
include the house number, street direction, street type, and street name. You can
remove any of these fields from the match string.</p><p>When matching on address fields, determine whether you want to use the original
field values, the standardized field values, or the phonetic values. The match engine
can handle any of these types of fields, but the best comparator for
each type might be different. Also determine how much weight you want to
give each field type and configure the match configuration file accordingly.</p></div>


<div class="indent"><a name="ref_sme-bn-string_c"></a><h3>Business Name Match String Fields</h3>
<p>By default, the match configuration file (<tt>matchConfigFile.cfg</tt>) includes rows specifically for matching on
the fields that are parsed from the business name fields. The file also
defines several generic match types you can customize to use with business name
fields. You can use any of the existing rows for matching or you
can add rows for the fields you want to match. If you specify
a &ldquo;BusinessName&rdquo; match type for any field in the  wizard, most of
the parsed business name fields are automatically added to the match string in
mefa.xml, including the name, organization type, association type, sector, industry, and URL. You
can remove any of these fields from the match string.</p><p>When matching on business name fields, determine whether you want to use the
original field values, the standardized field values, or the phonetic values. The match
engine can handle any of these types of fields, but the best comparator
for each type might be different. Also determine how much weight you want
to give each field type and configure the match configuration file accordingly.</p></div>


<a name="ref_sme-datatype-mtch_c"></a>

<h2>Mural Match Engine Match Types</h2>
<p>The default match configuration file, <tt>matchConfigFile.cfg</tt>, defines several rules that you can customize
for the type of data being processed. Each rule is identified by a
<b>match type</b> in the first column of each row. This value identifies the type
of matching to perform to the match engine. In a master index application,
the match type is entered for each field in the match string section
of mefa.xml.</p><p>The match configuration Mural Match Enginefile appears under the Match Engine node of
the master index project. For more information about the comparison functions used for
each match type and how the weights are tuned, see <a href="ref_sme-analysis_c.html#ref_sme-matchconfig_c">Customizing the Match Configuration</a> and
<a href="ref_sme-comparators_c.html">Mural Match Engine Comparison Functions</a>.</p><p>The following four tables list match types that are typically used in processing
different data types, including:</p>
<ul><li><p><a href="#ggoso">Person Data Match Types</a></p></li>
<li><p><a href="#ggopq">Address Match Types</a></p></li>
<li><p><a href="#ggoom">Business Name Match Types</a></p></li>
<li><p><a href="#ggopk">Miscellaneous Match Types</a></p></li></ul>
<p><a name="indexterm-334"></a><a name="indexterm-335"></a><a name="indexterm-336"></a>The following match types are designed for matching on person data.</p><a name="ggoso"></a><h6>Table&nbsp;1 Person Data Match Types</h6><table><col width="25%"><col width="75%"><tr><th align="left" valign="top" scope="column"><p>This indicator ...</p></th>
<th align="left" valign="top" scope="column"><p>processes
this data type ...</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-337"></a><a name="indexterm-338"></a>FirstName</p></td>
<td align="left" valign="top" scope="row"><p>A first name field, including middle name, alias first
name, and alias middle name fields.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-339"></a><a name="indexterm-340"></a>LastName</p></td>
<td align="left" valign="top" scope="row"><p>A last name field, including alias last name
fields.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-341"></a><a name="indexterm-342"></a>SSN</p></td>
<td align="left" valign="top" scope="row"><p>A field containing a social security number.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-343"></a><a name="indexterm-344"></a>Gender</p></td>
<td align="left" valign="top" scope="row"><p>A field containing a gender code.</p></td>
</tr>
</table><p>The following match types are designed for matching on address data.</p><a name="ggopq"></a><h6>Table&nbsp;2 Address Match Types</h6><table><col width="25%"><col width="75%"><tr><th align="left" valign="top" scope="column"><p>This indicator
...</p></th>
<th align="left" valign="top" scope="column"><p>processes this data type ...</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-345"></a><a name="indexterm-346"></a>StreetName</p></td>
<td align="left" valign="top" scope="row"><p>The parsed street name field of a street
address.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-347"></a><a name="indexterm-348"></a>HouseNumber</p></td>
<td align="left" valign="top" scope="row"><p>The parsed house number field of a street address.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-349"></a><a name="indexterm-350"></a>StreetDir</p></td>
<td align="left" valign="top" scope="row"><p>The parsed street direction
field of a street address.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-351"></a><a name="indexterm-352"></a>StreetType</p></td>
<td align="left" valign="top" scope="row"><p>The parsed street type field of a street
address.</p></td>
</tr>
</table><p>The following match types are designed for matching on business names.</p><a name="ggoom"></a><h6>Table&nbsp;3 Business Name Match Types</h6><table><col width="25%"><col width="75%"><tr><th align="left" valign="top" scope="column"><p>This match
type ...</p></th>
<th align="left" valign="top" scope="column"><p>processes this data type ...</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-353"></a><a name="indexterm-354"></a>PrimaryName</p></td>
<td align="left" valign="top" scope="row"><p>The parsed name field of a business
name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-355"></a><a name="indexterm-356"></a>OrgTypeKeyword</p></td>
<td align="left" valign="top" scope="row"><p>The parsed organization type field of a business name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-357"></a><a name="indexterm-358"></a>AssocTypeKeyword</p></td>
<td align="left" valign="top" scope="row"><p>The parsed association type
field of a business name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-359"></a><a name="indexterm-360"></a>LocationTypeKeyword</p></td>
<td align="left" valign="top" scope="row"><p>The parsed location type field of a business
name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-361"></a><a name="indexterm-362"></a>AliasList</p></td>
<td align="left" valign="top" scope="row"><p>The parsed alias type field of a business name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-363"></a><a name="indexterm-364"></a>IndustrySectorList</p></td>
<td align="left" valign="top" scope="row"><p>The parsed industry sector
field of a business name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-365"></a><a name="indexterm-366"></a>IndustryTypeKeyword</p></td>
<td align="left" valign="top" scope="row"><p>The parsed industry type field of a business
name.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-367"></a><a name="indexterm-368"></a>Url</p></td>
<td align="left" valign="top" scope="row"><p>The parsed URL field of a business name.</p></td>
</tr>
</table><p>Miscellaneous match types provide additional logic for matching on a variety of data
types, such as date, numeric, string, and character fields.</p><a name="ggopk"></a><h6>Table&nbsp;4 Miscellaneous Match Types</h6><table><col width="25%"><col width="75%"><tr><th align="left" valign="top" scope="column"><p>This indicator ...</p></th>
<th align="left" valign="top" scope="column"><p>processes this
data type ...</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-369"></a><a name="indexterm-370"></a>Date</p></td>
<td align="left" valign="top" scope="row"><p>The year of a date field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-371"></a><a name="indexterm-372"></a>DateDays</p></td>
<td align="left" valign="top" scope="row"><p>The day, month, and year
of a date field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-373"></a><a name="indexterm-374"></a>DateMonths</p></td>
<td align="left" valign="top" scope="row"><p>The month and year of a date field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-375"></a><a name="indexterm-376"></a>DateHours</p></td>
<td align="left" valign="top" scope="row"><p>The hour,
day, month, and year of a date field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-377"></a><a name="indexterm-378"></a>DateMinutes</p></td>
<td align="left" valign="top" scope="row"><p>The minute, hour, day, month,
and year of a date field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-379"></a><a name="indexterm-380"></a>DateSeconds</p></td>
<td align="left" valign="top" scope="row"><p>The seconds, minute, hour, day, month, and
year of a date field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-381"></a><a name="indexterm-382"></a>String</p></td>
<td align="left" valign="top" scope="row"><p>A generic string field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-383"></a><a name="indexterm-384"></a>Unistring</p></td>
<td align="left" valign="top" scope="row"><p>A generic Unicode string field.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-385"></a><a name="indexterm-386"></a>Integer</p></td>
<td align="left" valign="top" scope="row"><p>A
field containing integers.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-387"></a><a name="indexterm-388"></a>Real</p></td>
<td align="left" valign="top" scope="row"><p>A field containing real numbers.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-389"></a><a name="indexterm-390"></a>Char</p></td>
<td align="left" valign="top" scope="row"><p>A field containing a single character.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-391"></a><a name="indexterm-392"></a>pro</p></td>
<td align="left" valign="top" scope="row"><p>Any
field on which you want the Mural Match Engine to use prorated
weights.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><a name="indexterm-393"></a><a name="indexterm-394"></a>Exac</p></td>
<td align="left" valign="top" scope="row"><p>Any field you want the Mural Match Engine to match character for
character.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>CSC</p></td>
<td align="left" valign="top" scope="row"><p> A generic string.</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>DOB</p></td>
<td align="left" valign="top" scope="row"><p> A date of birth in string rather than
date format.</p></td>
</tr>
</table>

<a name="ref_sme-mtch_svc_c"></a>

<h2>Configuring the Match String for a Master Index Application</h2>
<p><a name="indexterm-395"></a><a name="indexterm-396"></a><a name="indexterm-397"></a>The <b>MatchingConfig</b> section of mefa.xml determines which fields are passed to the Mural
Match Engine for matching (the match string). The match types specified in this
section help the match engine determine the algorithm and custom logic to use
for matching on each field.</p><p>If you are matching on fields parsed from a free-form text field,
define each individual parsed field you want to use for matching in the
Master Index Wizard or Configuration Editor. The match types you can use for
each field in this section are defined in the first column of the
match configuration file (<tt>matchConfigFile.cfg</tt>). Make sure the match type you specify has the correct
matching logic defined in the match configuration file. See <a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a> for more information.</p><p>The following topics provide more information about matching on different types of data:</p>
<ul><li><p><a href="#ref_sme-prsn-mtchstr_c">Configuring the Match String for Person Data</a></p></li>
<li><p><a href="#ref_sme-add-mtchstr_c">Configuring the Match String for Address Data</a></p></li>
<li><p><a href="#ref_sme-bus-mistring_c">Configuring the Match String for Business Names</a></p></li></ul>


<div class="indent"><a name="ref_sme-prsn-mtchstr_c"></a><h3>Configuring the Match String for Person Data</h3>
<p>When matching on person data, you can include any field stored in
the database for matching. To configure the match string, follow the instructions under <a href="https://open-mi-dm.dev.java.net/docs/config/cnfg_index-match-string_p.html">Defining the Match String</a>.
For the Mural Match Engine, each data type has a different match type
(specified by the <i>match-type</i> element in the matching configuration file). The FirstName,
LastName, SSN, Gender, and DOB match types are specific to person matching. You
can specify any of the other match types defined in the match configuration
file as well. For more information, see <a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a>.</p><p>A sample match string for person matching is shown below. This sample matches
on first and last names, date of birth, social security number, gender, and
the street name of the address.</p><pre>&lt;match-system-object>
   &lt;object-name>Person&lt;/object-name>
   &lt;match-columns>
      &lt;match-column>
         &lt;column-name>
            Enterprise.SystemSBR.Person.FirstName_Std
         &lt;/column-name>
         &lt;match-type>FirstName&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.LastName_Std
         &lt;/column-name>
         &lt;match-type>LastName&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.SSN
         &lt;/column-name>
         &lt;match-type>SSN&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.DOB
         &lt;/column-name>
         &lt;match-type>DateDays&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.Gender
         &lt;/column-name>
         &lt;match-type>Char&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.Address.StreetName
         &lt;/column-name>
         &lt;match-type>StreetName&lt;/match-type>
      &lt;/match-column>
   &lt;/match-columns>
&lt;/match-system-object></pre></div>


<div class="indent"><a name="ref_sme-add-mtchstr_c"></a><h3>Configuring the Match String for Address Data</h3>
<p><a name="indexterm-398"></a><a name="indexterm-399"></a><a name="indexterm-400"></a>For matching on street address fields, make sure the match string you specify
in the MatchingConfig section of mefa.xml contains all or a subset of the
fields that contain the standardized data (the original text in street address fields
is generally too inconsistent to use for matching). You can include additional fields
for matching, such as the city name or postal code.</p><p>To configure the match string, follow the instructions under <a href="https://open-dm-mi.dev.java.net/docs/config/cnfg_index-match-string_p">Defining the Match String</a>. For the
Mural Match Engine, each component of a street address has a different match
type (specified by the <i>match-type</i> element in the matching configuration file). The default match
types for addresses are StreetName, HouseNumber, StreetDir, and StreetType. You can specify any
of the other match types defined in the match configuration file, as well.
For more information, see <a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a>.</p><p><a name="indexterm-401"></a>A sample match string for address matching is shown below.</p><pre>&lt;match-system-object>
   &lt;object-name>Person&lt;/object-name>
   &lt;match-columns>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.Address.StreetName
         &lt;/column-name>
         &lt;match-type>StreetName&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.Address.HouseNumber
         &lt;/column-name>
         &lt;match-type>HouseNumber&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.Address.StreetDir
         &lt;/column-name>
         &lt;match-type>StreetDir&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Person.Address.StreetType
         &lt;/column-name>
         &lt;match-type>StreetType&lt;/match-type>
   &lt;/match-column>
   &lt;/match-columns>
&lt;/match-system-object></pre></div>


<div class="indent"><a name="ref_sme-bus-mistring_c"></a><h3>Configuring the Match String for Business Names</h3>
<p><a name="indexterm-402"></a>For matching on business name fields, make sure the match string you specify
in the MatchingConfig section of mefa.xml contains all or a subset of the
fields that contain the standardized data (the unparsed business names are typically too
inconsistent for matching). You can include additional fields for matching if required.</p><p>To configure the match string, follow the instructions under <a href="https://open-dm-mi.dev.java.net/docs/config/cnfg_index-match-string_p">Defining the Match String</a>. For the
Mural Match Engine, each data type has a different match type (specified by
the <i>match-type</i> element of the matching configuration file). The PrimaryName, OrgTypeKeyword, AssocTypeKeyword, IndustrySectorList,
IndustryTypeKeyword, and Url match types are specific to business name matching. You can
specify any of the other match types defined in the match configuration file,
as well. For more information, see <a href="#ref_sme-datatype-mtch_c">Mural Match Engine Match Types</a>.</p><p><a name="indexterm-403"></a>A sample match string for business name matching is shown below. This sample
matches on the company name, the organization type, and the sector.</p><pre>&lt;match-system-object>
   &lt;object-name>Company/object-name>
   &lt;match-columns>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Company.Name_PrimaryName
         &lt;/column-name>
         &lt;match-type>PrimaryName&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Company.Name_OrgType
         &lt;/column-name>
         &lt;match-type>OrgTypeKeyword&lt;/match-type>
      &lt;/match-column>
      &lt;match-column>
         &lt;column-name>Enterprise.SystemSBR.Company.Name_Sector
         &lt;/column-name>
         &lt;match-type>IndustryTypeKeyword&lt;/match-type>
      &lt;/match-column>
   &lt;/match-columns>
&lt;/match-system-object></pre></div>

   </div>

   <div class="BottomPageControlPane">
      <table class="pagecontrol">
         <tr>
            <td class="pageleft">
               <a href="ref_sme-plugins_c.html">Previous</a>
            </td>
            <td class="pageright">
               <a href="ref_sme-analysis_c.html">Next</a>
            </td>
         </tr>
      </table>

   </div>

</body>
</html>
