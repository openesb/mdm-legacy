/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.addon;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.comparators.base.BigramComparator;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import com.sun.mdm.matcher.api.*;
import com.sun.mdm.matcher.api.impl.MatchConfigFilesAccessImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * AdvancedBigramComparatorTest unit testing
 * @author  sofiane Ouaguenouni
 * @version $Revision: 1.3 $
 */
public class PluginDOBRangeComparatorLevel2Test extends TestCase {

    static double[][] resultWeightNew;
    static double[][] resultWeightNew2, resultWeightNew3;
    static final double DELTA = 0.0001;
    
    static String[] matchFieldsIDs = {"PlugDate2"};   

    static String[][] candRecArrayVals = {{"12/22/2003"}};
    static String[][] refRecArrayVals = {{"11/22/2003"}}; 
    static String[][] candRecArrayVals2 = {{"11/22/2003"}};
    static String[][] refRecArrayVals2 = {{"03/22/2004"}};   
    static String[][] candRecArrayVals3 = {{"11/22/2003"}};
    static String[][] refRecArrayVals3 = {{"06/22/2004"}};   
    
    private static double extectedWeight1 = 10;   
    private static double extectedWeight2 = 0;
    private static double extectedWeight3 = -10;
    
    static {
        resultWeightNew = new double[1][1];   
        resultWeightNew2 = new double[1][1];     
        resultWeightNew3 = new double[1][1];     
//        resultWeightOld = new double[2][2];
        int i;
        int j;
    }    
    
    /** 
     * Creates new Bigram
     * @see junit.framework.TestCase
     */
    public PluginDOBRangeComparatorLevel2Test(String name) {
        super(name);        
    }

    /** 
     * Set up the unit test
     * @see junit.framework.TestCase
     */    
    protected void setUp() {
        
    }

    /** 
     * Tear down the unit test
     * @see junit.framework.TestCase
     */
    protected void tearDown() {
        // cleanup code
    }

    /**
     * Test the Bigram class methods
     * 
     */
    public void testAdvancedBigramComparator() 
        throws MatchComparatorException, IOException,
               MatcherException, ParseException, InstantiationException {
        
        int i;
        int j;
        int k;
        String path = "match/";

        MatchConfigFilesAccess filesAccess = new MatchConfigFilesAccessImpl(path); 

        // The new engine
        MatchingEngine newME = new MatchingEngine(filesAccess);
        newME.upLoadConfigFile();      
	resultWeightNew = newME.matchWeight(matchFieldsIDs, candRecArrayVals, refRecArrayVals);
        resultWeightNew2 = newME.matchWeight(matchFieldsIDs, candRecArrayVals2, refRecArrayVals2);
        resultWeightNew3 = newME.matchWeight(matchFieldsIDs, candRecArrayVals3, refRecArrayVals3);
                      		
        assertEquals((float) resultWeightNew[0][0], (float) extectedWeight1, (float) DELTA);
        assertEquals((float) resultWeightNew2[0][0], (float) extectedWeight2, (float) DELTA);
        assertEquals((float) resultWeightNew3[0][0], (float) extectedWeight3, (float) DELTA);
      
    }
    
    /**
     * Main method needed to make a self runnable class
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(PluginDOBRangeComparatorLevel2Test.class));
    }   
}

