/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.validator.datasources;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.io.File;
import java.util.ArrayList;
import com.sun.mdm.matcher.api.MatcherException;
import java.io.IOException;
import com.sun.mdm.matcher.comparators.validator.DataSourcesHandler;
import com.sun.mdm.matcher.comparators.validator.DataSourcesProperties;
import org.apache.lucene.index.IndexReader;


/**
 *
 * @author souaguenouni
 */
public class DOBRangeComparatorLevel5SourcesHandler implements DataSourcesHandler {

    /**
     * Provides logic handling for data sources associated with any given comparator
     */
//    public Object handleComparatorsDataSources(Map<String, Map> dataSources)
    public Map<String, Object> handleComparatorsDataSources(DataSourcesProperties dataSources)            
        throws MatcherException, IOException {

        // Data files directory path
        HashMap<Integer, String> dataPath;
        // Track the files loaded
        ArrayList loadedFiles = null; 
        String srcPath;
        
        Map<String, Object> mIndexReader = new HashMap();        
System.out.println("datasources----333 "+dataSources);

        // The list of data sources paths
        List<List<String>> s1 = dataSources.getDataSourcesList("zd");
        List<String> s2;
System.out.println("----333 "+s1);            
        // Iterator 
        Iterator<List<String>> iter = s1.iterator();
        while (iter.hasNext()) {
            //
            s2 = iter.next();            
            Iterator<String> iter2 = s2.iterator();
            
            while (iter2.hasNext()) {                 
                srcPath = iter2.next();   
            
                // Insure that the file does not get loaded more than ones
                if (dataSources.isDataSourceLoaded(srcPath)) {
                    // Get access to the object
                    IndexReader Iread = (IndexReader) dataSources.getDataSourceObject(srcPath);
                    mIndexReader.put(srcPath, Iread);
                    continue;
                }  
    System.out.println("filleeeeeee----333//// "+dataSources.isDataSourceLoaded(srcPath));            
                File f = new File(srcPath);
                if (!f.exists()) {
                    throw new IOException("Invalid path (does not exist): " + srcPath);
                }

                try {           
                    mIndexReader.put(srcPath, IndexReader.open(f));   
                    dataSources.setDataSourceLoaded(srcPath, true);
                } catch (Exception e) {
                    throw new IOException("Failed to open index on path [" + srcPath + "]: " + e.toString());
                }               
            }
        }
/*        
        List<String> s2;                
        String dPath;
//        int index;
        String sourceName;
        String fieldName;
        
        while (iter.hasNext()) {
            fieldName = iter.next();          
            // The list of names
            s2 = dataSources.get(fieldName);  
//System.out.println("field name "+fieldName+"|"+s2);    
 

            Iterator<String> iter2 = s2.keySet().iterator();
            
            while (iter2.hasNext()) {
                sourceName = iter2.next();
                dPath = s2.get(sourceName);               
             
                // Insure that the file does not get loaded more than ones
                if (loadedFiles.contains(dPath)) {
                    continue;
                }

                File f = new File(dPath);
                if (!f.exists()) {
                    throw new IOException("Invalid path (does not exist): " + dPath);
                }
             
                try {           
                    mIndexReader.put(sourceName, IndexReader.open(f));
                    loadedFiles.add(dPath);
                } catch (Exception e) {
                    throw new IOException("Failed to open index on path [" + dPath + "]: " + e.toString());
                }   
System.out.println("mIndexReader "+mIndexReader.keySet());             
            }
 
        }
*/    
        return mIndexReader;
    }

}
