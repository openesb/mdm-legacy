/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;

import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;

import com.sun.mdm.matcher.comparators.configurator.ComparatorsConfigurationService;
import com.sun.mdm.matcher.comparators.configurator.ComparatorsConfigBean;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.api.MatchingEngine;

/**
 * Manages all match comparators instances and provides access to their
 * functionality by name
 *
 * @author souaguenouni
 */
public class ComparatorsManager {

    // A map from the comparator type to the comparators instance
    private Map comparators;  
    private Map classComparators;
    private ComparatorsConfigBean comparatorsConfig = null;
    
    // The dependency classes within match comparators
    private static final String DEPEND_CLASSES = MatchVariables.DEPEND_CLASSES;
    // The class name
    private static final String NAME = MatchVariables.NAME;   
    // The match field name
    private static final String MATCH_FIELD = MatchVariables.MATCH_FIELD; 
    
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();    

    /** 
     * Creates new ComparatorsManager 
     * @param configPath the comparators' list path
     * @throws MatchComparatorException if the configured comparators can not be registred
     */
    public ComparatorsManager(String configPath)
        throws MatchComparatorException {
        registerMatchComparators(configPath);
    }
    
    /** 
     * Creates new ComparatorsManager 
     * @param configPath1 the first possible comparators' list path
     * @param classSearch the option to search for the associated classes
     * @param meg an instance of the Match engine
     * @throws MatchComparatorException if the configured comparators can not be registred
     */
    public ComparatorsManager(String configPath, boolean classSearch, MatchingEngine meg)
        throws MatchComparatorException {
        registerMatchComparators(configPath, classSearch, meg);
    }  
    
    /** 
     * Provides the instance of the specified comparator 
     * @param codeName the code name of the comparator
     * @return the instance of the comparator. 
     * @throws MatchComparatorException if the comparator does not exist
     */
    public MatchComparator getComparatorInstance(String codeName)
        throws MatchComparatorException {
        return (MatchComparator)comparators.get(codeName);
    }
    /**
     * Read the available comparators from a Configuration, instantiate them
     * and populate the internal map of encoding type to MatchComparator
     * @throws MatchComparatorException if the comparators could not be loaded or registered
     */
    public void initiate(MatchVariables mVar, Map<String, Map> comparatorDetails, 
                         Map<String, String> fieldNameCode) 
        throws MatchComparatorException { 
        //
        String comparisonCode;
        String className;
        int index;
        int classInd = 0;
        int i;
        int mapLen;
        MatchComparator comparatorInstance;
        // For parameters
        Map<Integer, Map> mVarParams;
        Map<String, Map> paramsList = new HashMap();  
        Map<String, Map> fieldParamList = new HashMap(); 
        // For data sources
        Map<String, Object> mVarSources;
        
        Map<String, Map> sourcesList = new HashMap(); 
        // Dependency classes list
        Map<String, Map> dependClassList = new HashMap(); 
        Map compdepClasses = new HashMap();
        
        Map<String, Map> fieldSourcesList = new HashMap(); 
        Map<String, Integer> classIndex = new HashMap(); 
        
        // The length of the list
        mVarParams = mVar.params;
        mapLen = mVarParams.size();
        // First, handle the 'normal' parameters
        Iterator indexIter = mVarParams.keySet().iterator();
        while (indexIter.hasNext()) { 
   
            index = Integer.parseInt(indexIter.next().toString());
            // The code name
            comparisonCode = mVar.comparatorType[index];
            // The class name
            className = mVar.compMap.get(comparisonCode);
            // Case where the code is secondary one
            if (className == null) {
                className = mVar.compMap.get(mVar.secondaryCodes.get(comparisonCode));
            }            
           
            if (classIndex.containsKey(className)) {   
                // Skip this iteration
                continue;
            }      
            
            // Temporary map class name/ code name
            if (!classIndex.containsKey(className)) {
                
                int ind;
                String compCode;
                String clName; 
                String fldName;
                
                // Loop through the entire list
                Iterator iIter = mVarParams.keySet().iterator();
                while (iIter.hasNext()) { 

                    ind = Integer.parseInt(iIter.next().toString());
                    // The code name
                    compCode = mVar.comparatorType[ind];
                    // The class name
                    clName = mVar.compMap.get(compCode);  
                    // Case where the code is secondary one
                    if (clName == null) {
                        clName = mVar.compMap.get(mVar.secondaryCodes.get(compCode));
                    }   
                    // Field's name
                    fldName = mVar.fieldName.get(ind).toString();  
                    
                    // Search for match fields within the same match comparator
                    if (clName.compareTo(className) == 0) {
                        // Group same-class parameters together
                        fieldParamList.put(fldName, mVarParams.get(ind));                      
                    }                 
                }
                // Store all the parameters associated with one class
                paramsList.put(className, (Map)((HashMap)fieldParamList).clone());
                
                classIndex.put(className, classInd++);           
            }   
            //
            fieldParamList.clear();
        }


        classIndex.clear();
        // The length of the list
        
        if(!mVar.sources.isEmpty()) {
            mVarSources = mVar.sourcesData;
            mapLen = mVarSources.size();      

            // Second, handle the data sources parameters
            Iterator<String> indexIter2 = mVarSources.keySet().iterator();
            while (indexIter2.hasNext()) { 

                // The code name
                comparisonCode = indexIter2.next();//mVar.comparatorType[index];
                // The class name
                className = mVar.compMap.get(comparisonCode);

                if (classIndex.containsKey(className)) {   
                    // Skip this iteration
                    continue;
                }  

                // Temporary map class name/ code name
                if (!classIndex.containsKey(className)) {
       
                    // Store all the parameters associated with one class
                    sourcesList.put(className, (Map)mVarSources.get(comparisonCode)); 

                    classIndex.put(className, classInd++);   
                }
                //
                fieldSourcesList.clear();
            }            
        }
        
        // Consider dependent classes in match comparators
        Map<String, List<Map<String, String>>> details;
        Map<String, Map<String, String>> fieldList;
        List<Map<String, String>> dependClasses;
        String clName;
        String matchFieldName;

        indexIter = comparatorDetails.keySet().iterator();
        while (indexIter.hasNext()) { 
            
            className = indexIter.next().toString();       
            // Check if it has dependent classes
            details = comparatorDetails.get(className);
            dependClasses = details.get(DEPEND_CLASSES);
            
            //
            if(dependClasses != null) {

                int len = dependClasses.size();
                for(int j = 0; j < len; j++) {
                    clName = dependClasses.get(j).get(NAME);   
                    matchFieldName = dependClasses.get(j).get(MATCH_FIELD);  
                    comparatorInstance = (MatchComparator) classComparators.get(clName); 
                    // Instead of using the class name, we use the match field name because
                    // different match fields using the same class might use different parameters.
                    compdepClasses.put(matchFieldName, comparatorInstance);    
                    // Handle the logic related to dependecy complexity (i.e.,  multi-level dependecies)
                    fieldList = provideDependencyClassContextInfo(clName, mVar, fieldNameCode);
                    
                    compdepClasses.put("dependencyContext", fieldList);
                }
                dependClassList.put(className, (Map)((HashMap)compdepClasses).clone());
                // Clear the object for the next round
                compdepClasses.clear();
            }
        }

        // Loop through the comparators instances and initiate the parameters/sources/dependency-classes
        Iterator classIter = classComparators.keySet().iterator();
        while (classIter.hasNext()) { 
            className = classIter.next().toString();             
                    
            comparatorInstance = (MatchComparator) classComparators.get(className);
            // Initiate the parameters/sources/dependency-classes data
            comparatorInstance.initialize(paramsList.get(className), sourcesList.get(className), 
                                          dependClassList.get(className)); 
        } 
    }
 
    // Handle the logic related to dependecy complexity (i.e.,  multi-level dependecies)
    private Map<String, Map<String, String>> provideDependencyClassContextInfo(String className, 
                                               MatchVariables mVar, Map<String, String> fieldNameCode) {
        // Search for comparators code names that correspond to the comparator name
        Map<String, String> compMap = mVar.compMap;
        // 
        List<String> codeList = new ArrayList();
        Map<String, Map<String, String>> fieldList = new HashMap();
        
        Iterator<String> iter = compMap.keySet().iterator();
        // Search for the corresponding code names
        while(iter.hasNext()) {
            String code = iter.next();
            if (compMap.containsKey(code)) {
                if (compMap.get(code).compareTo(className) == 0 
                    || mVar.secondaryCodes.containsKey(code)) {
                    codeList.add(code);
                }                 
            }
        }
        // Convert the code names into match field names
        Iterator<String> iter2 = fieldNameCode.keySet().iterator();
        while(iter2.hasNext()) {
            String field = iter2.next();  
            String codeName = fieldNameCode.get(field);
            if (codeList.contains(codeName)
                    || (mVar.secondaryCodes.containsKey(codeName) 
                        && codeList.contains(mVar.secondaryCodes.get(codeName)))) {             
                fieldList.put(field, mVar.fieldNameInfo.get(mVar.fieldNameIndex.get(field)));  
            }
        }
        return fieldList;
    }   
    
     /**
     * Reads two strings and measure how close they are relying on an algorithm
     * that compare the proximity of the two strings (zero being very different and
     * one being identical)
     *
     * @param  firstField  Candidate's string record.
     * @param  secondField  Reference's string record.
     * @param  comparisonCode  the comparator's unique code name   
     * @param mVar an instance of the match variables class
     * @return a real number between zero and one that measures the degree of similarity.
     */
    public double compareFields(String firstField, String secondField, Map<String, String> context, String comparisonCode, MatchVariables mVar) 
            throws MatchComparatorException {
        
        MatchComparator searchComparator = (MatchComparator) comparators.get(comparisonCode);

        if (searchComparator == null) {            
            searchComparator = (MatchComparator) comparators.get(mVar.secondaryCodes.get(comparisonCode));            
            if (searchComparator == null) {      
                mLogger.severe(mLocalizer.x("MEG505: Unknown match comparator type: {0}", comparisonCode));            
                throw new MatchComparatorException("Unknown match comparator type: " + comparisonCode);
            }
        }
        return searchComparator.compareFields(firstField, secondField, context);
    }
    
    /**
     * Returns an instance of the ComparatorsConfigBean
     */
    public ComparatorsConfigBean getComparatorsConfigBean() {
        return comparatorsConfig;
    }    

    /**
     * Read the available comparators from a Configuration, instantiate them
     * and populate the internal map of encoding type to MatchComparator
     *
     * @param compPath the comparators list path
     * @throws MatchComparatorException if the comparators could not be loaded or registered
     */
    void registerMatchComparators(String compPath) 
            throws MatchComparatorException {

        // Use Configuration factory to retrieve match comparators configuration
        try {
            ComparatorsConfigurationService cfgFactory = ComparatorsConfigurationService.getInstance(compPath, false, null);
            comparatorsConfig = (ComparatorsConfigBean) cfgFactory.getMatchComparatorsConfigBean();
        } catch (InstantiationException ex) {
            mLogger.severe(mLocalizer.x("MEG506: Failed to load preconfigured match comparators "+
                             "configuration file. {0}", ex.getMessage()));                
            throw new MatchComparatorException (
                    "Failed to load preconfigured match comparators configuration file. " 
                    + ex.getMessage(), ex);
        }        
    }
    /**
     * Read the available comparators from a Configuration, instantiate them
     * and populate the internal map of encoding type to MatchComparator
     *
     * @param compPath the comparators list path
     * @throws MatchComparatorException if the comparators could not be loaded or registered
     */
    void registerMatchComparators(String compPath, boolean classSearch, MatchingEngine meg) 
            throws MatchComparatorException {
        
        comparators = new HashMap();
        classComparators = new HashMap();
        //
        HashMap classComparatorsList = new HashMap();
        // Use Configuration factory to retrieve match comparators configuration
        try {
            ComparatorsConfigurationService cfgFactory = ComparatorsConfigurationService.getInstance(compPath, classSearch, meg);
            comparatorsConfig = (ComparatorsConfigBean) cfgFactory.getMatchComparatorsConfigBean();
            
        } catch (InstantiationException ex) {
            mLogger.severe(mLocalizer.x("MEG507: Failed to load preconfigured match comparators "+
                             "configuration file. {0}", ex.getMessage()));                
            throw new MatchComparatorException (
                    "Failed to load preconfigured match comparators configuration file. " 
                    + ex.getMessage(), ex);
        }
        // Go through all the configured comparators and instantiate each one, populating the comparators HashMap
        if (classSearch) {
            Map comparatorsClasses = comparatorsConfig.getClassNamesMap();
            Object[] comparisonCodes = (Object[])comparatorsClasses.keySet().toArray();
            int len = comparisonCodes.length;
            int i;
            int j;
            String comparisonCode;

            for (i = 0; i < len; i++) {

                MatchComparator comparatorInstance = null;    
                comparisonCode = comparisonCodes[i].toString();
                String comparatorClassName = (String) comparatorsClasses.get(comparisonCode);  

                try {  
                    // If the associated class did not get instantiated yet         
                    if (!classComparatorsList.containsValue(comparatorClassName)) {   
                        // Read the relative path
                        String relPath = compPath.substring(0, compPath.lastIndexOf('/') + 1); 
                        
                        String theClassName = comparatorClassName.split("\\|")[1];
                        
                        Class comparatorClass = Class.forName(theClassName); 
                        if(comparatorClass == null) {
                            comparatorClass = Class.forName(relPath + theClassName);  
                        }
                        comparatorInstance = (MatchComparator) comparatorClass.newInstance(); 
                        classComparatorsList.put(comparisonCode, comparatorClassName);
                        // Store the instance
                        comparators.put(comparisonCode, comparatorInstance);
                        classComparators.put(theClassName, comparatorInstance);                    
                        // Copy the instance to the other bundle of related code names
                        for (j = i; j < len; j++) {

                            // Check if the class has more than one code name
                            String compCode = comparisonCodes[j].toString();
                            String comparatorClassName2 = (String) comparatorsClasses.get(compCode);
                            if (comparatorClassName.compareTo(comparatorClassName2) == 0) {
                                comparators.put(compCode, comparatorInstance);
                            }
                        }
                    }             
                } catch (ClassNotFoundException ex) {
                    mLogger.severe(mLocalizer.x("MEG508: Failed to load configured match comparator class: {0}"
                                    , ex.getMessage()));                  
                    throw new MatchComparatorException ("Failed to load configured match comparator class: " 
                            + comparatorClassName + " msg: " + ex.getMessage(), ex);
                } catch (Exception ex) {
                    mLogger.severe(mLocalizer.x("MEG509: Failed to instantiate the configured match comparator class: {0}"
                                    , ex.getMessage()));                      
                    throw new MatchComparatorException ("Failed to instantiate the configured match comparator class: " 
                            + comparatorClassName + " msg: " + ex.getMessage(), ex);
                }               
                comparatorsClasses.put(comparisonCode, comparatorClassName.split("\\|")[1]);
            }
        }
    }
}
