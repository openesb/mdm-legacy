/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.validator;

import java.util.List;
import java.util.Map;
import com.sun.mdm.matcher.api.MatcherException;
import java.io.IOException;

/**
 * Interface to be implemented by any comparator that reads from external
 * data sources and needs to process some related logic
 *
 * @author souaguenouni
 */
public interface DataSourcesHandler {

    /**
     * Provides logic handling for data sources associated with any given comparator
     * 
     * @param dataSources the data sources properties object
     */
    public Object handleComparatorsDataSources(DataSourcesProperties dataSources)        
        throws MatcherException, IOException;    
}
