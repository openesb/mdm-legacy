/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.util.Map;

/**
 * This code computes the ratio of the number of bigrams in common
 * in two different strings to the average length of the strings.
 * It is then adjusted to be conform to a similar value in a standard
 * string comparators.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class AdvancedBigramComparator implements MatchComparator {
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {}

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
    }    
    
    /**
     * Close any related data sources streams
     */
    public void stop() {}   
    
    /**
     * Reads two strings and compute how similar they are relying on an algorithm
     * that compare the bigrams of the two strings.
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @return     a real number that measures the similarity
     */
    public double compareFields(String recordA, String recordB, Map context)
        throws MatchComparatorException {
        
        double returnWeight;
        
        int recALength;
        int recBLength;
        int i;
        int j;
        int k;
        int halfCheck = 0;
        int similar = 0;
        int halves = 0;
        
        // Calculate their new lengths
        recALength = recordA.length();
        recBLength = recordB.length();

        char c; 
        char c1;
        
        // Loop over all the characters of the fieldA up to one character
        // before the end (we compare 2 characters at a time).
        for (i = 0; i < (recALength - 1); i++) {

            halfCheck = 0;
            // Opimization         
            c = recordA.charAt(i);
            c1 = recordA.charAt(i + 1);

            for (j = 0; j < (recBLength - 1); j++) {

                // Test if every two successive characters are equivalent.
                // Jump out of the inner loop ones a similarity is found
                if ((c == recordB.charAt(j)) && (c1 == recordB.charAt(j + 1))) {

                    // Increment the count of similar pairs
                    similar++;
                    halfCheck = 0;
                    break;

                } else if ((c == recordB.charAt(j + 1))
                    && (c1 == recordB.charAt(j))) {
                    
                    // Switch the pairs of characters and retest if there is
                    // similarities

                    halfCheck = 1;
                }
            }

            // If the pairs compared are reversely similar, increment the
            // corresponding index
            if (halfCheck != 0) {
                halves++;
            }
        }

        // Loop over all the characters of the fieldB up to one character
        // before the end (we compare 2 characters at a time)
        for (i = 0; i < (recBLength - 1); i++) {

            halfCheck = 0;
            // Opimization         
            c = recordB.charAt(i);
            c1 = recordB.charAt(i + 1);
            
            //           
            for (j = 0; j < (recALength - 1); j++) {

                // Test if every two successive characters are equivalent.
                // Jump out of the inner loop ones a similarity is found
                if ((c == recordA.charAt(j)) && (c1 == recordA.charAt(j + 1))) {

                    // Increment the count of similar pairs
                    similar++;
                    halfCheck = 0;
                    break;

                } else if ((c == recordA.charAt(j + 1))
                    && (c1 == recordA.charAt(j))) {
                    
                    // Switch the pairs of characters and retest if there is
                    // similarities

                    halfCheck = 1;
                }
            }

            // If the pairs compared are reversely similar, increment the
            // corresponding index
            if (halfCheck != 0) {
                halves++;
            }
        }

        // The weight is computed by taking the average value of both
        // similar and Reverse-similar pairs of characters
        returnWeight = ((double) (similar + halves))
                  / ((double) (recALength + recBLength) - 2.0);
        // In order to get the bigram weight on the same scale as the other
        // string comparators, we raise the weight to the power of 0.2435
        returnWeight = (double) Math.pow(returnWeight, 0.2435);        
        
       // Justified by some extensive studies in computer science.
       if (returnWeight < 0.8) {
            return 0;
       }      
       return returnWeight;        
    }            
}
