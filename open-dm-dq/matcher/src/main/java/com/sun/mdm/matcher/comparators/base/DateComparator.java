/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.comparators.MatchComparator;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;
import java.text.ParseException;

import java.util.Map;

/**
 * This class handles comparison between dates and times in a standard format  
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class DateComparator implements MatchComparator {

    private Map<String, Map> params;
    private Map<String, Map> dependClassList;
    // The list of parameters
    private Map<String, String> theParams;    
    // The variable that holds all the additional real-time parameters
    private Map<String, String> argumentsRT = new HashMap();       
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;
        this.dependClassList = dependClassList;
    }

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
        this.argumentsRT.put(key, value);        
    }    
    
    /**
     * Close any related data sources streams
     */
    public void stop() {
        argumentsRT.clear();
    }   
    
    /**
     * Reads two strings and compute how similar they are relying on an algorithm
     * that compare the bigrams of the two strings.
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @return     a real number that measures the similarity
     */
    public double compareFields(String dateA, String dateB, Map context) 
        throws MatchComparatorException {

        boolean secondChar = true;
        char theUnit = '0';
        
        MatchComparator classInstance = (MatchComparator) dependClassList.get("String");
        Map<String, Map> depContext = dependClassList.get("dependencyContext");
        Map stringContext = depContext.get("String");        

        String codeName = context.get("codeName").toString();
        
        // The list of parameters
        theParams = this.params.get(context.get("fieldName"));   
        
//        if (classInstance instanceof JaroAdvancedSSNComparator) {
//            throw new MatchComparatorException("Wrong instance. We need JaroAdvancedSSNComparator class");
//        }
                
        int i = 0;
        int[] unitsListA = {-1, -1, -1, -1, -1, -1};
        int[] unitsListB = {-1, -1, -1, -1, -1, -1};           

        if (codeName.compareTo("d") == 0) {
            theUnit = 'D';
            secondChar = false;
        } else {
            theUnit = codeName.charAt(1);
        } 

        // Handle the date format for both records
        String dateFormat;
        Locale locale;
        if ((dateFormat = this.argumentsRT.get("DateFormat")) == null) {
            // Default date format
            dateFormat = "yyyyMMdd";
        }
        if (this.argumentsRT.get("Locale") != null) {
            // Custom date format
            locale = new Locale(this.argumentsRT.get("Locale"));            
        } else {
            // Default date format
            locale = Locale.US;
        }        
        DateFormat df = new SimpleDateFormat(dateFormat, locale);   
//        DateFormat df = new SimpleDateFormat("MMddyyyy", Locale.US);           
        Calendar cal1 = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = df.parse(dateA);
            date2 = df.parse(dateB);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        cal1.setTime(date1);
        cal2.setTime(date2);

        // For use in the transposer
        if ((theParams != null) && theParams.get("switch").compareTo("n") == 0) {
        
            StringTokenizer sTA = new StringTokenizer(date1.toString(), " :");
            StringTokenizer sTB = new StringTokenizer(date2.toString(), " :");
            
            String[] timeSA = new String[8];
            String[] timeSB = new String[8];
            
            while (sTA.hasMoreTokens()) { 
            
                if (i == 0) {
                    sTA.nextToken();

                } else if (i == 1) {                    
                    unitsListA[1] = cal1.get(Calendar.MONTH);
                    
                    if (unitsListA[1] < 10) {
                        timeSA[1] = "0".concat(Integer.toString(unitsListA[1] + 1));
                    } else {
                        timeSA[1] = Integer.toString(unitsListA[1] + 1);
                    }
                    
                    sTA.nextToken();
                    
                } else if (i == 2) {
                    timeSA[2] = sTA.nextToken();
                } else if (i == 3) {
                    timeSA[3] = sTA.nextToken();
                } else if (i == 4) {
                    timeSA[4] = sTA.nextToken();
                } else if (i == 5) {
                    timeSA[5] = sTA.nextToken();
                } else if (i == 7) {
                    timeSA[0] = sTA.nextToken();
                } else {
                    sTA.nextToken();
                }
                
                i++;
            }
            
            i = 0;
            while (sTB.hasMoreTokens()) {
            
                if (i == 0) {
                    timeSB[0] = sTB.nextToken();

                } else if (i == 1) {
                    unitsListB[1] = cal2.get(Calendar.MONTH);
                    
                    if (unitsListB[1] < 10) { 
                        timeSB[1] = "0".concat(Integer.toString(unitsListB[1] + 1));
                    } else {
                        timeSB[1] = Integer.toString(unitsListB[1] + 1);
                    }
                    
                    sTB.nextToken();
                    
                } else if (i == 2) {
                    timeSB[2] = sTB.nextToken();
                } else if (i == 3) {
                    timeSB[3] = sTB.nextToken();
                } else if (i == 4) {
                    timeSB[4] = sTB.nextToken();
                } else if (i == 5) {
                    timeSB[5] = sTB.nextToken();
                } else if (i == 7) { 
                    timeSB[0] = sTB.nextToken();
                } else {
                    sTB.nextToken();
                }
                
                i++;
            }
            
            // Call the transposer and return a standard date objects
            return transposeAndCompareDates(timeSA, timeSB, stringContext, theUnit, classInstance);
            
        } else {
        
            if (theUnit == 'Y') {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);
            
            } else if (theUnit == 'M') {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);
            
                unitsListA[1] = cal1.get(Calendar.MONTH);
                unitsListB[1] = cal2.get(Calendar.MONTH);
            
            } else if (theUnit == 'D' || !secondChar) {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);
            
                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);
            
            } else if (theUnit == 'H') {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);

                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);
            
                unitsListA[3] = cal1.get(Calendar.HOUR_OF_DAY);
                unitsListB[3] = cal2.get(Calendar.HOUR_OF_DAY);
                
            } else if (theUnit == 'm') {
            
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);

                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);
            
                unitsListA[3] = cal1.get(Calendar.HOUR_OF_DAY);
                unitsListB[3] = cal2.get(Calendar.HOUR_OF_DAY);
            
                unitsListA[4] = cal1.get(Calendar.MINUTE);
                unitsListB[4] = cal2.get(Calendar.MINUTE);
            
            } else if (theUnit == 's') {
            
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);

                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);

                unitsListA[3] = cal1.get(Calendar.HOUR_OF_DAY);
                unitsListB[3] = cal2.get(Calendar.HOUR_OF_DAY);
            
                unitsListA[4] = cal1.get(Calendar.MINUTE);
                unitsListB[4] = cal2.get(Calendar.MINUTE);
            
                unitsListA[5] = cal1.get(Calendar.SECOND);
                unitsListB[5] = cal2.get(Calendar.SECOND);
            
            } else {
                throw new MatchComparatorException("The comparator you are using does not exist."
                    + "you must use one of the following: 'dY', 'dM', 'dD', 'dH', 'dm' and 'ds'");
            }

        
            // Don't check for transpositions
            // Evaluate the dates measured in the selected unit
            return compareDatesDist(unitsListA, unitsListB, theUnit, cal1, cal2);
        }
    }


    /*
     * Consider the records as strings and measure how close they are from each other 
     */
    private double transposeAndCompareDates(String[] listA, String[] listB, Map context, char unit,
                                                   MatchComparator classInstance)
            throws MatchComparatorException {
            
        double yearW;
        double monthW;
        double dayW;
        double hourW;
        double minuteW;
        double secW;      
        double weight = 0;
        
        // Compare only the year components
        if (unit == 'Y') { 
            
            // Compare only year
            weight = classInstance.compareFields(listA[0], listB[0], context);
            
        // Compare the year and months elements
        } else if (unit == 'M') {
            
            // Combine months and years into the weight
            yearW = classInstance.compareFields(listA[0], listB[0], context);
            monthW = classInstance.compareFields(listA[1], listB[1], context);

            weight = (yearW + monthW) / 2.;
        
        // Compare the year, the months and the day elements
        } else if (unit == 'D') {

            // Combine months and years and days into the weight
            yearW = classInstance.compareFields(listA[0], listB[0], context);
            monthW = classInstance.compareFields(listA[1], listB[1], context);
            dayW = classInstance.compareFields(listA[2], listB[2], context);
            
            weight = (yearW + monthW + dayW) / 3.;
        
        } else if (unit == 'H') {

            // Combine months and years and days into the weight
            yearW = classInstance.compareFields(listA[0], listB[0], context);
            monthW = classInstance.compareFields(listA[1], listB[1], context);
            dayW = classInstance.compareFields(listA[2], listB[2], context);
            hourW = classInstance.compareFields(listA[3], listB[3], context);
            
            weight = (yearW + monthW + dayW + hourW) / 4.;
            
        } else if (unit == 'm') {

            // Combine months and years and days into the weight
            yearW = classInstance.compareFields(listA[0], listB[0], context);
            monthW = classInstance.compareFields(listA[1], listB[1], context);
            dayW = classInstance.compareFields(listA[2], listB[2], context);
            hourW = classInstance.compareFields(listA[3], listB[3], context);
            minuteW = classInstance.compareFields(listA[4], listB[4], context);
            
            weight = (yearW + monthW + dayW + hourW + minuteW) / 5.;
            
        } else if (unit == 's') {
            
            // Combine months and years and days into the weight
            yearW = classInstance.compareFields(listA[0], listB[0], context);
            monthW = classInstance.compareFields(listA[1], listB[1], context);
            dayW = classInstance.compareFields(listA[2], listB[2], context);
            hourW = classInstance.compareFields(listA[3], listB[3], context);
            minuteW = classInstance.compareFields(listA[4], listB[4], context);
            secW = classInstance.compareFields(listA[5], listB[5], context);
            
            weight =  (yearW + monthW + dayW + hourW + minuteW + secW) / 6.;
        }
        
        return weight;
    }


    /*
     * Measure the relative distance between the compared dates
     */
    private double compareDatesDist(int[] listA, int[] listB, char unit, Calendar cal1,
                                       Calendar cal2)
            throws MatchComparatorException {
        
        int i;
        int maxDayInYearA;
        int maxDayInYearB;
        double weight = 0;
        int delY = listB[0] - listA[0];

        // Compare only the year components
        if (unit == 'Y') { 
            
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delY);
            
        // Compare the year and months elements
        } else if (unit == 'M') {
        
            int delM = listB[1] - listA[1];          
            delM += (12 * delY);

            // Compute the time-distance comparator
            weight = computeRelativeDiff(delM);
        
        // Compare the year, the months and the day elements
        } else if (unit == 'D') {

            int delD = 0;
            
            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);
            
            // If in the same year
            if (delY == 0) { 
                delD = listB[2] - listA[2];
                
            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];
                
            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
                
            } else if (delY > 1) {
                
                delD = maxDayInYearA - listA[2] + listB[2];
       
                int interYear;
                GregorianCalendar cal = new GregorianCalendar();

                // If the years are different, then we need to compute the maximum number
                // of days relative to the respective years
                // Loop over the difference of years
                for (i = 0; i < Math.abs(delY) - 1; i++) {
                   
                   interYear = listA[0] + i + 1;
                   
                   if (cal.isLeapYear(interYear)) { 
                       delD += 366;
                   } else {
                       delD += 365;
                   }
                }
 
            } else if (delY < -1) {
                
                delD = listB[2] - maxDayInYearB - listA[2];

                int interYear;
                GregorianCalendar cal = new GregorianCalendar();

                // If the years are different, then we need to compute the maximum number
                // of days relative to the respective years
                // Loop over the difference of years
                for (i = 0; i < Math.abs(delY) - 1; i++) {
                   
                   interYear = listB[0] + i + 1;
                   
                   if (cal.isLeapYear(interYear)) { 
                       delD -= 366;
                   } else {
                       delD -= 365;
                   }
                }
            }
 
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delD);
        
        } else if (unit == 'H') {
            
            int delD = 0;
            int delH = 0;

            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);

            // If in the same year
            if (delY == 0) {
                delD = listB[2] - listA[2];
                
            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];
                
            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
            }
       
            // Check first if the compared times are within the same day
            if (delD == 0) {
            
                delH = listB[3] - listA[3];
                
            } else {
                delH = listB[3] - listA[3] + 24 * delD;              
            }
            
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delH);
            
        } else if (unit == 'm') {
        
            int delD = listB[2] - listA[2];
            int delH = listB[3] - listA[3];
            int delm = listB[4] - listA[4];
            
            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);

            // If in the same year
            if (delY == 0) {
                delD = listB[2] - listA[2];

            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];

            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
            }
  
            // Check first if the compared times are within the same day
            if (delD == 0) {
                delH = listB[3] - listA[3];
                
            } else {
                delH = listB[3] - listA[3] + 24 * delD;
            }

            if (delH == 0) {
                delm = listB[4] - listA[4];
                
            } else {
                delm = listB[4] - listA[4] + 60 * delH;
            }
                
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delm);
            
        } else if (unit == 's') {

            int delD = listB[2] - listA[2];
            int delH = listB[3] - listA[3];
            int delm = listB[4] - listA[4];
            int dels = listB[5] - listA[5];
 
            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);

            // If in the same year
            if (delY == 0) {
                delD = listB[2] - listA[2];

            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];

            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
            }

            // Check first if the compared times are within the same day
            if (delD == 0) {
                delH = listB[3] - listA[3];
                
            } else {
                delH = listB[3] - listA[3] + 24 * delD;
            }

            if (delH == 0) {
                delm = listB[4] - listA[4];
                
            } else {
                delm = listB[4] - listA[4] + 60 * delH;
            }

            if (delm == 0) {
                dels = listB[5] - listA[5];
                
            } else {
                dels = listB[5] - listA[5] + 60 * delm;
            }

            // Compute the time-distance comparator
            weight =  computeRelativeDiff(dels);
        }

        return weight;
    }
    

    /*
     *
     */
    private double computeRelativeDiff(int delX) {
        
        double compV;
        double abDelX;
        int lowLimit = Integer.parseInt(theParams.get("lLimit"));
        int upLimit = Integer.parseInt(theParams.get("uLimit"));
        
        // Case when the reference date is ahead of the candidate date
        if (delX >= 0) {
            
            if (delX <= upLimit) {
                compV = 1.0 - ((double) delX) / (upLimit + 1);
            } else {
                compV = 0.0;
            }
        // Case when the candidate date is ahead of the reference date
        } else {
            
            abDelX = Math.abs(delX);
            
            if (abDelX <= lowLimit) {
                compV = 1.0 - ((double) abDelX) / (lowLimit + 1);
            } else {
                compV = 0.0;
            }
        }
        
        return compV;
    }
}
