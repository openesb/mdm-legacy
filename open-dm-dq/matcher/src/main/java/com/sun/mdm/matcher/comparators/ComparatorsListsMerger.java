/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.mdm.matcher.comparators;

import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;


import com.sun.mdm.matcher.util.IOHelperClass;
import com.sun.mdm.matcher.api.MatcherException;

/**
 *
 * @author souaguenouni
 */
public class ComparatorsListsMerger {
    
    private String mMasterConfigFile;
    private String mZipFile;
    private File mergedFile;
    
    /** 
     * Creates new ComparatorsManager 
     * @param configPath the comparators' list path
     * @param zipFile a zip file instance
     * @throws MatchComparatorException if the configured comparators can not be registred
     */
    public ComparatorsListsMerger(String configPath, String zipFile)
        throws IOException {
        
        mMasterConfigFile = configPath;
        mZipFile = zipFile;
    }

    /**
     * Returns the comparators list file
     *
     * @return the comparators list file
     */
    public File getMergedComparatorsListFile() {
        return mergedFile;
    }    
    
    /** 
     * Merges two config file (the master comparators list & the plugin one) 
     * @param validateGroupList option to perform validation of the groups 
     *        within the master comparators list
     * @param validateCodeList option to ensure that the comparators' code names  
     *        are unique
     * @throws MatchComparatorException if the comparator does not exist
     */
    public void mergeComparatorsList(boolean validateGroupList, boolean validateCodeList)
        throws MatcherException, IOException {        
        // First, read from the plugin comparator list configuration file from the zip
        BufferedReader pluginConfigFile = null;
        if (mZipFile != null) {
            pluginConfigFile = IOHelperClass.openBufferedReaderFromZip(mZipFile, "comparatorsList.xml");
        }
        // Second, read from the master comparators list
        BufferedReader masterConfigFile = IOHelperClass.openBufferedReader(new File(mMasterConfigFile));

        // Merge the Files
        File ff = new File("comparatorList.xml");
        BufferedWriter fr = new BufferedWriter(new FileWriter(ff));        
        
        String tempS;
        // Master file portion
        while(masterConfigFile.ready()) {
            tempS = masterConfigFile.readLine();  
            //
            if (pluginConfigFile == null) {
                fr.write(tempS, 0, tempS.length());
                fr.newLine();
            } else if (!tempS.contains("</comparators-list>")) {
                fr.write(tempS, 0, tempS.length());
                fr.newLine();                
            }                           
        }
        masterConfigFile.close();      
        
        if (mZipFile != null) {
            boolean option = false;
            // Plugin file
            while(pluginConfigFile.ready()) {
                tempS = pluginConfigFile.readLine(); 
                if (tempS.contains("<group ")) {
                    option = true;
                }
                if (option) {
                    fr.write(tempS, 0, tempS.length());
                    fr.newLine();       
                }
            } 
            pluginConfigFile.close(); 
        }
        // Close the stream
        fr.close();
        
        // Validation of the groups of comparators
        if (validateGroupList) {
            validateGroupList(ff);
        }
        // Unicity of the comparators' code names
        if(validateCodeList) {
            validateCodenameList(ff);
        }

        mergedFile = ff;
    }        

    /** 
     * Validates the group list 
     * @param the merged comparators list file
     * @throws MatchComparatorException if the comparator does not exist
     */
    private void validateGroupList(File ff)
        throws MatcherException, IOException {  
   
        BufferedReader br = IOHelperClass.openBufferedReader(ff);
        String path = "path=";
        String subS;
        ArrayList arr = new ArrayList();
        
        while(br.ready()) {
            String ss = br.readLine();
            // Check if thegroup's path is unique
            if (ss.contains(path)) {
                subS = ss.substring(ss.lastIndexOf(path)+6 , ss.lastIndexOf("\""));
                if (!arr.contains(subS)) {
                    arr.add(subS);
                } else {
                    // clase the stream
                    br.close();
                    throw new MatcherException (
                    "There is conflicting groups with the same base path in the comparatorsList file."+ 
                    " The duplicate path is: "+subS);                    
                }            
//            System.out.println("inside validateGroup  "+"|"+subS);
            }
        }
        // clase the stream
        br.close();        
    } 
    
    /** 
     * Validates the list of code names. Unsure, it is unique 
      * @param the merged comparators list file
     * @throws MatchComparatorException if the comparator does not exist
     */
    private void validateCodenameList(File ff)
        throws MatcherException, IOException {  
        
        BufferedReader br = IOHelperClass.openBufferedReader(ff);
        String codeName = "name=\"";
        String code = "<code ";
        String subS;
        ArrayList arr = new ArrayList();        

        while(br.ready()) {
            String ss = br.readLine();
           
            // Check if thegroup's path is unique
            if (ss.contains(code)) {

                subS = ss.substring(ss.lastIndexOf(codeName)+6 , ss.lastIndexOf("\""));
                if (!arr.contains(subS)) {
                    arr.add(subS);
                } else {
                    // clase the stream
                    br.close();
                    throw new MatcherException (
                    "The following comparator's code name is used multiple times."+ 
                    " The code name should be unique: "+subS);                    
                }
            
//            System.out.println("inside validateCode names  "+"|"+subS);
            }
        }
        // clase the stream
        br.close();               
    }              
}
