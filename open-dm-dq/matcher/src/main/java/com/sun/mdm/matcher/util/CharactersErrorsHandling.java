/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.util;

/**
 *
 * @author souaguenouni
 */
public class CharactersErrorsHandling {

    // The adjwt values are used to give partial credit for characters that
    // may be in errors due to known phonetic or character recognition errors.
    // A typical example is to match the letter "O" with the number "0"
    public static final char[][] SP  = {
        {'A', 'E'}, {'A', 'I'}, {'A', 'O'}, {'A', 'U'},
        {'B', 'V'}, {'E', 'I'}, {'E', 'O'}, {'E', 'U'}, 
        {'I', 'O'}, {'I', 'U'}, {'O', 'U'}, {'I', 'Y'}, 
        {'E', 'Y'}, {'C', 'G'}, {'E', 'F'}, {'W', 'U'}, 
        {'W', 'V'}, {'X', 'K'}, {'S', 'Z'}, {'X', 'S'}, 
        {'Q', 'C'}, {'U', 'V'}, {'M', 'N'}, {'L', 'I'}, 
        {'Q', 'O'}, {'P', 'R'}, {'I', 'J'}, {'2', 'Z'}, 
        {'5', 'S'}, {'8', 'B'}, {'1', 'I'}, {'1', 'L'}, 
        {'0', 'O'}, {'0', 'Q'}, {'C', 'K'}, {'G', 'J'}, 
        {'E', ' '}, {'Y', ' '}, {'S', ' '}, 
        {'1', '2'}, {'2', '3'}, {'3', '4'}, {'4', '5'}, {'5', '6'},
        {'6', '7'}, {'7', '8'}, {'8', '9'}, {'9', '0'}
    };    

    // The adjwt values are used to give partial credit for characters that
    // may be in errors due to known phonetic or character recognition errors.
    // A typical example is to match the letter "O" with the number "0"
    public static final int[][] SP_UNICODE  = {
        {82, 87}, {82, 91}, {82, 97}, {82, 103},
        {83, 104}, {87, 91}, {87, 97}, {87, 103}, 
        {91, 97}, {91, 103}, {97, 103}, {91, 107}, 
        {87, 107}, {84, 89}, {87, 88}, {105, 103}, 
        {105, 104}, {106, 93}, {101, 108}, {106, 101}, 
        {99, 84}, {103, 104}, {95, 96}, {94, 91}, 
        {99, 97}, {98, 100}, {91, 92}, {71, 108}, 
        {74, 101}, {77, 83}, {70, 91}, {70, 94}, 
        {69, 97}, {69, 99}, {84, 93}, {89, 92}, 
        {87, 0}, {107, 0}, {101, 0}, 
        {70, 71}, {71, 72}, {72, 73}, {73, 74}, {74, 75},
        {75, 76}, {76, 77}, {77, 78}, {78, 69}
    };   
    
    private static int[][] adjustWt = new int[255][255];;
    
    static {
        int set1;
        int set2;
        int i;
        int arrLen = SP.length;  
              
        for (i = 0; i < 111; i++) {            
            if (i < 42) {          
                set1 = 48 + i;                
            } else {
                set1 = 144 + i; 
            }
                              
            for (int j = 0; j < 111; j++) {                 
                if (j < 42) {          
                    set2 = 48 + j;               
                } else {
                    set2 = 144 + j; 
                }                                                   
                adjustWt[set1][set2] = 0;
            }
        } 
        //
        for (i = 0; i < arrLen; i++) {      
            adjustWt[SP[i][0]][SP[i][1]] = 3;
            adjustWt[SP[i][1]][SP[i][0]] = 3;
        }      
    }
    
    /**
     * 
     *
     * @param intA the first value
     * @param intB the second value
     * @return give partial credit for characters that
     * may be in errors due to known phonetic or character recognition errors.
     */ 
    public static int correctCharactersErrors(int intA, int intB) {
       return adjustWt[intA][intB];
    }
        
    
}
