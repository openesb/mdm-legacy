/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.util;

/**
 *
 * @author souaguenouni
 */
public class StringsManipulation {   

    /**
     * 
     * 
     * @param sb aaaa
     * @param fs aaaa
     * @param length aaaa
     * @return a StringBuilder
     */
    public static StringBuilder strncat(StringBuilder sb, String fs, int length) {

        int fsLength = fs.length();
        
        if (fsLength > length) {
            sb.append(fs.toCharArray(), 0, length);          
        } else {
            sb.append(fs);
        }
        return sb;
    }
    
    
    /**
     * 
     * 
     * @param sb aaaa
     * @param sb2 aaaa
     * @param sb2Index aaaa
     * @param length aaaa
     * @return a StringBuilder
     */
    public static StringBuilder strncat(StringBuilder sb, String sb2, int sb2Index, 
                                       int length) {
        
        int sb2Length = sb2.length();
        
        if (sb2Length >= (length + sb2Index)) {
            return sb.append(sb2.substring(sb2Index, sb2Index+length));                           
        } else {
            return sb.append(sb2);
        }
    }    

    
    /**
     * 
     * 
     * @param intSb aaaa
     * @param sbIndex aaa
     * @param finSb aaa
     * @param length aaaa
     * @return a StringBuilder
     */
    public static StringBuilder strncat(StringBuilder intSb, int sbIndex, StringBuilder finSb,
                                       int length) {

        int finLength = finSb.length();      
        
        if (finLength > length) {
            return (new StringBuilder(intSb.substring(sbIndex)).append(finSb.substring(0, length)));            
        } else {
            return (new StringBuilder(intSb.substring(sbIndex)).append(finSb));
        }
    }    

    
    /**
     * 
     * 
     * @param intS aaaa
     * @param finS aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, String finS, int len) {

        if ((intS == null) || (finS == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finS.length();

        String s1;
        String s2;

        if (len1 < len) { 
            s1 = intS;
        } else {
            s1 = intS.substring(0, len);
        }

        if (len2 < len) {
            s2 = finS;          
        } else { 
            s2 = finS.substring(0, len);
        }

        if (s1.compareTo(s2) > 0) {
            return 1;
            
        } else if (s1.compareTo(s2) < 0) {
            return -1;
            
        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finSb aaaa
     * @param index aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, StringBuilder finSb, int index, int len) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finSb.length();

        String s1;
        String s2;
        
        // First check if the index is within the string itself and 
        // keep only the needed substring
        if (index < len2) { 
            s2 = finSb.substring(index);
        } else {
            s2 = finSb.toString();
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (len1 < len) { 
            s1 = intS;
        } else {
            s1 = intS.substring(0, len);
        }

        if (s2.length() > len) {
            s2 = s2.substring(0, len);
        }
        
        
        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }            
    
    
    /**
     * Compares a string to a StringBuilder
     *zzzzzzz
     * @param s1 the first string
     * @param s2 the second string
     * @return the comparison result 
     */
    public static boolean compareStrings(String s1, StringBuilder s2) {
        
        int len1 = s1.length();
        int len2 = s2.length();
        int i;
        
        if (len1 != len2) {
            return false;
        }        
                
        for (i = 0; i < len1; i++) {
            
            if (s1.charAt(i) !=  s2.charAt(i)) {
                return false;
            }
        }     
        return true;       
    }    
    
    /**
     * Compares two strings over a length len
     *
     * @param s1 the first string
     * @param s2 the second string
     * @param ind the length over which we compare the strings
     * @return true if they match over length len, false otherwise 
     */
    public static boolean compareNStrings(String s1, String s2, int len) {
        
        int len1 = s1.trim().length();
        int len2 = s2.trim().length();
        int minL = Math.min(len1, len2);
        int i;
        
        // If the two strings have different lengths and the scale over which we 
        // compare is longer than the min
        if ((len1 != len2) & minL < len) {
            return false;
        }        
        
        for (i = 0; i < minL; i++) {    
            if (s1.charAt(i) !=  s2.charAt(i)) {
                return false;
            }
        }      
        return true;       
    }

    /**
     * Compares two strings over a length len
     *
     * @param s1 the first string
     * @param len1 the length of string s1
     * @param s2 the second string
     * @param len2 the length of string s2
     * @param ind the length over which we compare the strings
     * @return true if they match over length len, false otherwise 
     */
    public static boolean compareNStrings(String s1, int len1, String s2, int len2, int len) {
        
        int minL = Math.min(len1, len2);
        int i;
        
        // If the two strings have different lengths and the scale over which we 
        // compare is longer than the min
        if ((len1 != len2) & minL < len) {
            return false;
        }        
        
        for (i = 0; i < minL; i++) {    
            if (s1.charAt(i) !=  s2.charAt(i)) {
                return false;
            }
        }      
        return true;       
    }    
    /**
     * Copy the string finS into the original string intSb
     * 
     * @param intSb the original string
     * @param finS the replacement string
     * @return the resulted string
     */
    public static StringBuilder strcpy(StringBuilder intSb, String finS) {

        intSb.setLength(0);
        return intSb.append(finS);
    }    
    
    /**
     * Reduce the StringBuilder to a length ind
     * 
     * @param sa aaaa
     * @param ind aaaa
     * @return a StringBuilder
     */
    public static StringBuilder setEndSb(StringBuilder sa, int ind) {

        sa.setLength(ind);
        return sa;
    }        
}
