/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.weightcomp;

import com.sun.mdm.matcher.comparators.ComparatorsManager;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.util.StringsManipulation;
import java.text.ParseException;
import java.util.Map;
import com.sun.mdm.matcher.api.MatchingEngine;
import com.sun.mdm.matcher.api.MatcherException;

/**
 * Compares the A-field and the B-field, and based on the type of
 * comparison chosen, returns the associated match weight
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.2 $
 */
public class WeightComputation {

    /**
     * Reads two fields respectively from the candidate and the reference record and
     * compute the associated matching weight
     *
     * @param candRec the candidate record.
     * @param fieldName    The index of a given field in the reference record.
     * @param j the index of the record
     * @param jIndex the index of the key in the config file
     * @param refRec thew reference record
     * @param mEng  An instance of MatchVariable class.
     * @return A real number that measures the field's matching weight(j, k)
     * @throws SbmeMatchingException an exception
     * @throws ParseException a text parsing exception
     */
    public static double calculateWeight(String fieldName, String recA, String recB, 
            int jIndex, MatchingEngine mEng, ComparatorsManager comparMgr, Map context)
        throws MatcherException, ParseException {
        
        int fieldLength = 0;

        MatchVariables matchVar = mEng.getMatchVar(mEng);
        // Global frequency flag
        boolean useFrequency = false; //matchVar.freqFlag[jIndex];
        double freqFactor = 0;
        String sFlag = matchVar.nullFlag[jIndex];
        int nFlag = sFlag.length();        
        char cFlag = sFlag.charAt(0);                 
        fieldLength = matchVar.lengthF[jIndex];
        int lenA = 0;
        int lenB = 0;
        
        // Trim the trailing spaces
        if (recA != null) {
            recA = recA.trim();
            lenA = recA.length();
        }
        
        if (recB != null) {
            recB = recB.trim();
            lenB = recB.length();
        }
            

        // Check if the records lengths are smaller than the lengthF. If not 
        // reduce it them to lengthF
        if ((recA != null) && lenA > fieldLength) {
            recA = recA.substring(0, fieldLength);               
            lenA = recA.length();
        } 
 
        if ((recB != null) && lenB > fieldLength) {
            recB = recB.substring(0, fieldLength);               
            lenB = recB.length();
        } 

        
        // Reads the comparator's type associated with index j.
        String codeName = matchVar.comparatorType[jIndex];

        double agreement = matchVar.agrWeight[jIndex];
        double disagreement = matchVar.disWeight[jIndex];
        double[] adjParams = (double[])matchVar.adjCurv.get(codeName);     
        double delAgr = agreement - disagreement;                
        
        double primaryWeight = 0.0;
        double secondaryWeight = 0.0;
        double comparison = 0.0;

        boolean nullA = false;
        boolean nullB = false;
        
        /* test if the candidate record is empty or null */
        if (recA == null || lenA == 0) {
            nullA = true;
        }
        
        /* test if the candidate record is empty or null */
        if (recB == null || lenB == 0) {
            nullB = true;
        }
        
        // Test if one or both records are empty and 
        // the nullFlag has one character only
        if ((nullA || nullB) && (nFlag == 1)) {
            
            if (cFlag == '0') {
                return 0.0;
                
            } else if (cFlag == '1') {
                
                if (nullA & nullB) { 
                    return agreement;
                } else {
                    return disagreement;
                }
            } else if (cFlag == 'a') {
                return agreement / 2.;
                
            } else if (cFlag == 'd') {
                return disagreement / 2.;
                
            }
            
        } else if (nullA || nullB) {

            nFlag = Integer.parseInt(sFlag.substring(1));

            primaryWeight = disagreement / nFlag;
            secondaryWeight = agreement / nFlag;

            // In case we want to return a neutral weight
            if (cFlag == 'a') {
                return secondaryWeight;
            // In case we want to return a negative weight
            } else if (cFlag == 'd') {
                return primaryWeight;
            } else {
                throw new MatcherException("The null flag associated with the match field"+
                                           fieldName+" must start with 'a' or 'd'");
            }
        }       
        
        // If the fields match exactly over a length 'fieldLength' 
        if(StringsManipulation.compareNStrings(recA, lenA, recB, lenB, fieldLength)) {
            return agreement;
        }
        
        // Get comparator's info
        context = matchVar.fieldNameInfo.get(jIndex);            

            // Calculate the comparison value [0, 1]
        comparison = comparMgr.compareFields(recA, recB, context, codeName, matchVar); 

            // Evaluate the associated weights
            // Case where we use curve adjustment parameters
            if (adjParams != null) {      
                // Compare the returned value with the curve-adjustment parameters.
                // If the comparison value is higher than the max adjustment value,
                // return total agreement.
                if (comparison > adjParams[0]) {                        
                    // If frequency-dependency exists, then adjust the primary weight 
                    if (useFrequency && freqFactor < 0) {
                        primaryWeight = agreement + freqFactor;
                        
                        // If the calculated weight is lower than the minimum authorized
                        // limit, replace it with the minimum value.
                        if (primaryWeight < disagreement) {
                            return disagreement;
                        }                               
                    }                    
                    return agreement;
                    
                } else if (comparison <= adjParams[0] && comparison > adjParams[1]) {
                    
                    // If the comparison value is in-between the adjustment parameters,
                    // use it a linear adjustment.                    
                    primaryWeight = agreement + delAgr * (comparison - 1) * adjParams[2];

                } else if (comparison <= adjParams[1]) {
                
                    // If the comparison value is below the min adjustment parameter,
                    // use it a different linear adjustment.                 
                    primaryWeight = agreement + delAgr * (comparison - 1) * adjParams[3];                                                                   
                } 
            } else {
                primaryWeight = disagreement + delAgr * comparison;   
            }
      
            // If frequency-dependency exists, then adjust the primary weight 
            if (useFrequency) {
                primaryWeight += agreement + freqFactor;
            }
            
            // If the calculated weight is lower than the minimum authorized
            // limit, replace it with the minimum value.
            if (primaryWeight < disagreement) {
                return disagreement;
            }                                                
            //
        return primaryWeight;
    }       
}
