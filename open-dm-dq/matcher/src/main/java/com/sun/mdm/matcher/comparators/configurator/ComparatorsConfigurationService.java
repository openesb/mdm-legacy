/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.configurator;

import com.sun.mdm.matcher.configurator.MatchVariables;
import java.util.List;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ArrayList;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.sun.mdm.matcher.api.MatcherConfigurationException;
import com.sun.mdm.matcher.comparators.ComparatorCodeProcessor;
import com.sun.mdm.matcher.api.MatchingEngine;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;
import java.util.logging.Level;

/** 
 * @author souaguenouni
 * @version $Revision: 1.1 $
 */
public class ComparatorsConfigurationService {
    
    // Class name constants
    static final String CLASS_NAME = "className";
    // Class name constants
    static final String CODE_NAMES = "codes";
    // Class name constant
    static final String CODE_NAME = "code";    
    // Class name constants
    static final String PATH = "path";
    // Class name constants
    static final String NAME = MatchVariables.NAME; 
    // Class name constants
    static final String TYPE = MatchVariables.TYPE;     
    // Class name constants
    static final String DESC = "description";
    // Class name constants
    static final String GROUP_INDEX = "groupIndex";    
    // Parameters name
    private static final String PARAMS = MatchVariables.PARAMS;
    // Class name constants
    static final String DATA_SOURCES = MatchVariables.DATA_SOURCES;
    // Class name constants
    static final String PARAM = "param";
    // Class name constants
    static final String DATA_SOURCE = "data-source";
    //
    static final String DEPEND_CLASSES = MatchVariables.DEPEND_CLASSES;
    // 
    static final String DEPEND_CLASS = "dependency-class";
    // 
    static final String CURVE_ADJUST = MatchVariables.CURVE_ADJUST;    
    // 
    static final String STATUS = "status";       
    //
    static final String MATCH_FIELD = MatchVariables.MATCH_FIELD;
    
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();    
    private static transient Logger LOGGER = Logger.getLogger(ComparatorsConfigurationService.class.getName());    
    private static transient Localizer MLOCALIZER = Localizer.get(); 
    
    /** singleton instance */
    private static ComparatorsConfigurationService instance = null;
    
    /** Instance of the Javabean class that holds the comparators config. info */
    private ComparatorsConfigBean configB;
    
    /**
     * Observable to notify of configuration changes that happen after the initial load
     */
    private static ConfigurationServiceObservable configServiceObservable 
            = new ConfigurationServiceObservable();
    
    /**
     * Creates a new ComparatorsConfigurationService instance
     */
    public ComparatorsConfigurationService() {
        // Create the ComparatorsConfigBean
        configB = new ComparatorsConfigBean();
    }
    
    /** 
     * return the configuration bean with the parsed data 
     */
    public ComparatorsConfigBean getMatchComparatorsConfigBean() {
        // Create the ComparatorsConfigBean
        return configB;
    }


    /** 
     * Load the configuration file.
     *
     * @param fileStream input stream reader to the file.
     * @param option 
     * @param meg an instance of the MatchingEngine 
     * @throws IOException if there is an error accessing the file.
     * @throws SAXException if there is an error parsing the file.
     * @throws ParserConfigurationException if there is an invalid parser configuration.
     * @throws DOMException if there is an error accessing the DOM tree.
     * @throws ClassNotFoundException if the parser class not found.
     * @throws InstantiationException if the parser can not be instantiated.
     * @throws IllegalAccessException if it is unable to access parser default
     * constructor.
     * @throws ConfigurationException if it encounters unexpected values.
     */
    public void load(InputStreamReader fileStream, boolean option, MatchingEngine meg)
            throws IOException, SAXException, ParserConfigurationException,
                   DOMException, ClassNotFoundException, InstantiationException,
                   IllegalAccessException, MatcherConfigurationException {
                    
        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Loading the comparators configuration xml file");
        }       
        Document doc;        
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        
        builder.setErrorHandler(new org.xml.sax.ErrorHandler() {
            // ignore fatal errors (an exception is guaranteed)
            public void fatalError(SAXParseException exception)
            throws SAXException {}
            
            // Treat validation errors as fatal
            public void error(SAXParseException e)
            throws SAXParseException {
                throw e;
            }
            
            // log warnings
            public void warning(SAXParseException err)
            throws SAXParseException {
                // use logging facility to capture warning
                mLogger.warn(mLocalizer.x("MEG010: Warning, line {0}, uri {1}", 
                    err.getLineNumber(), err.getSystemId(), err));
            }
        });
        
        org.xml.sax.InputSource input = new org.xml.sax.InputSource(fileStream);
        doc = builder.parse(input);

        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Config file parsed");
        }         
        parse(doc, option, meg);  
    }

    /** 
     * Returns an inputstream from the configuration file.
     *
     * @param compPath the comparators-list file's path
     * @param meg an instance of the MatchingEngine
     * @throws IOException error constructing the URL.
     * @return an inputstream to the configuration file.
     */
    private InputStreamReader getConfigFileStream(String fileName, MatchingEngine meg)
            throws IOException {
        InputStream is;
        if (meg == null) {
            try {
                is = new FileInputStream(fileName);  
                InputStreamReader isr = new java.io.InputStreamReader(is); 
                return isr;                
            } catch(java.io.FileNotFoundException ex) {
                mLogger.severe(mLocalizer.x("MEG011: Unable to find configuration file in classpath: {0}" , fileName, ex));
                throw new IOException("Unable to find configuration file in classpath : " + fileName);            
            }
        } else {       
            is = this.getClass().getClassLoader().getResourceAsStream(fileName);
            if (is == null) {
                mLogger.severe(mLocalizer.x("MEG012: Unable to find configuration file in classpath: {0}" , fileName));
                throw new IOException("Unable to find configuration file in classpath : " + fileName);
            }  
            return new InputStreamReader(is);      
        }   
    }    
    

    /** Returns a reference to the singleton instance.  This is syncronized to
     * prevent multiple initial loading by different threads.
     *
     * @return reference to singleton instance
     */
    public static synchronized ComparatorsConfigurationService getInstance()
        throws InstantiationException {
        return instance;
    }
    
    /** Returns a reference to the singleton instance.  This is syncronized to
     * prevent multiple initial loading by different threads.
     *
     * @param compPath the comparators list path
     * @param option
     * @param meg an instance of the MatchingEngine
     * @return reference to singleton instance
     */
    public static synchronized ComparatorsConfigurationService getInstance(String compPath, boolean option, MatchingEngine meg)
        throws InstantiationException {
        
        // First 
        java.io.InputStreamReader comparatorsConfigStream = null;

        instance = new ComparatorsConfigurationService();
            
        try {             
            // Read the comparators list config file
            comparatorsConfigStream = instance.getConfigFileStream(compPath, meg);
            instance.load(comparatorsConfigStream, option, meg);                   
            comparatorsConfigStream.close();
            comparatorsConfigStream = null;

        } catch (Exception e) {
            LOGGER.severe(MLOCALIZER.x("MEG013: Unable to find match comparators configuration file. ", e));
            instance = null;
            throw new InstantiationException(e.getMessage());
        } finally {
            try {
              if (comparatorsConfigStream != null){
                    comparatorsConfigStream.close();
              }
            } catch (Exception io){
                    throw new InstantiationException(io.getMessage());           		
            }
        } 
        return instance;
    }
   
    
    /**
     * Remove the Observer from being notified of configuration changes from what
     * was originally loaded.
     *
     * @param anObserver the observer to remove.
     */
    public static void deleteConfigChangeObserver(java.util.Observer anObserver) {
        configServiceObservable.deleteObserver(anObserver);
    }
        
    /** 
     * Parses the DOM representation of the XML configuration.
     * Loads and instantiates the parser class to parse each sub-module.
     *
     * @param doc DOM document representing the configuration file.
     * @param option 
     * @param meg an instance of the MatchingEngine
     * @throws ConfigurationException unexpected value
     * @throws DOMException DOM access exception
     * @throws ClassNotFoundException unable to load the parser class
     * @throws InstantiationException unable to create the module specific parser object
     * @throws IllegalAccessException unable to access default constructor for module specific parser
     */
    private void parse(Document doc, boolean option, MatchingEngine meg)
            throws MatcherConfigurationException, DOMException, ClassNotFoundException,
                   InstantiationException, IllegalAccessException {
        
        int gNum = 0;
        String className = null;
        String fullClassName = null;
        String codeName = null;
        // Local variable that map the code name to the class names
        Map<String, String> classNamesMap = new HashMap();
        // The code name - description map for the gui
        Map<String, String> codeNamesDesc = new HashMap();
        Map<String, Map<String, String>> secondCodeNamesDesc = new HashMap();
     
        // Local that stores a given comparators group's attreibutes       
        ArrayList<String> groupAttributes = new ArrayList();
        // 
        Map<String, Map> comparatorDetails = new HashMap();
        //
        Map details;
        // 
        ArrayList<Map> params = new ArrayList();
        Map paramAttr;
        ArrayList<Map> codeNames = new ArrayList();
        Map codeNameAttr;        

        ArrayList<Map> dependClasses = new ArrayList();
        Map dependClassAttr;
        
        ArrayList<Map> curveAdjClasses = new ArrayList();
        Map curveAdjAttr = new HashMap();      
        //
        ArrayList<Map> dataSources = new ArrayList();
        Map dataSourceAttr;
                
        Element elem = doc.getDocumentElement();       
        NodeList nl1 = elem.getChildNodes();
     
        // Group level analysis
        for (int i = 1; i < nl1.getLength(); i++) {
            Node n1 = nl1.item(i);

            if (n1.getNodeType() == Node.ELEMENT_NODE) {
                // if it's element node, by XSD def, it must be of ModuleConfig type
                LOGGER.fine(n1.getNodeName());
                
                // Update the number of groups
                gNum++;
                
                Element e1 = (Element) n1;                
                // 
                groupAttributes.add(gNum-1, e1.getAttribute(DESC)+"|"+e1.getAttribute(PATH));
                
                // Comparators level analysis
                NodeList nl2 = e1.getChildNodes();
                for (int j = 1; j < nl2.getLength(); j++) {
                                       
                    Node n2 = nl2.item(j);
                    if (n2.getNodeType() == Node.ELEMENT_NODE) {
                        Element e2 = (Element) n2;                

                        details = new HashMap(); 
                        // Add the group index to the details
                        details.put(GROUP_INDEX, gNum - 1);
                        // Add the description of the comparator
                        details.put(DESC, e2.getAttribute(DESC));
                        
                        NodeList nl3 = e2.getChildNodes();
                        if(e2.hasChildNodes()) {

                            // Attributes and childs elements of each of the match comparators
                            for (int k = 1; k < nl3.getLength(); k++) {
                                Node n3 = nl3.item(k);
                          
                                if (n3.getNodeType() == Node.ELEMENT_NODE) {
                                    Element e3 = (Element) n3;

                                    // Set the code/className pairs
                                    if(e3.getLocalName().compareTo(CLASS_NAME) == 0) {
                                        className = e3.getTextContent();
                                        fullClassName = e1.getAttribute(PATH)+"."+className;
                                    }

                                    NodeList nl4 = e3.getChildNodes();
                                    if(e3.hasChildNodes()) {
                                        
                                        // Attributes and elements of each element of any match comparator
                                        for (int p = 1; p < nl4.getLength(); p++) {
                                            Node n4 = nl4.item(p);
                                            if (n4.getNodeType() == Node.ELEMENT_NODE) {
                                                Element e4 = (Element) n4;
 
                                                String localTopName = n4.getLocalName();                               
                                                
                                                // Instantiate the temporary lists
                                                paramAttr = new HashMap();
                                                dataSourceAttr = new HashMap();
                                                dependClassAttr = new HashMap();
                                                codeNameAttr = new HashMap();
                                                curveAdjAttr = new HashMap();
                                                
                                                // length of attribute
                                                NamedNodeMap nm4 = e4.getAttributes();
                                                int len4 = nm4.getLength();
                                                for (int z = 0; z < len4; z++) {                                                  
                                                    Node n5 = nm4.item(z);   
                                                    
                                                    String localName = n5.getLocalName();
                                                    String localValue = n5.getTextContent();
                                                    int localValueLen = localValue.length();
        
                                                    if(localName.compareTo(NAME) == 0) {
                                                        if(localTopName.compareTo(PARAM) == 0 && localValueLen > 0) {
                                                            paramAttr.put(NAME, localValue);  
                                                            
                                                        } else if (localTopName.compareTo(CODE_NAME) == 0 && localValueLen > 0) {
                                                            codeNameAttr.put(NAME, localValue); 
                                                    
                                                            // 
                                                            if(option) {
                                                                classNamesMap.put(localValue, e2.getAttribute(DESC)+"|"+fullClassName);
                                                            } else {
                                                                classNamesMap.put(localValue, fullClassName);
                                                            }
                                                            
//                                                        } else if (localTopName.compareTo(DATA_SOURCE) == 0 && localValueLen > 0) {
//                                                            dataSourceAttr.put(NAME, localValue); 
                                                        } else if (localTopName.compareTo(DEPEND_CLASS) == 0 && localValueLen > 0) {
                                                            dependClassAttr.put(NAME, localValue); 
                                                        }                                                      
                                                
                                                    } else  if(localName.compareTo(DESC) == 0) {

                                                        if(localTopName.compareTo(PARAM) == 0 && localValueLen > 0) {
                                                            paramAttr.put(DESC, localValue);  
                                                            
                                                        } else if (localTopName.compareTo(CODE_NAME) == 0 && localValueLen > 0) {
                                                            codeNameAttr.put(DESC, localValue); 
                                                 
                                                        }  else if (localTopName.compareTo(DATA_SOURCE) == 0 && localValueLen > 0) {
                                                            dataSourceAttr.put(DESC, localValue); 
                                                        }  
                                                        
                                                    } else  if(localName.compareTo(TYPE) == 0) {
                                                        if(localTopName.compareTo(PARAM) == 0 && localValueLen > 0) {
                                                            paramAttr.put(TYPE, localValue);  
                                                  
                                                        } else if (localTopName.compareTo(DATA_SOURCE) == 0 && localValueLen > 0) {
                                                            dataSourceAttr.put(TYPE, localValue); 
                                                        }     
                                                        
                                                    } else  if(localName.compareTo(PATH) == 0) {
                                                        if(localTopName.compareTo(PARAM) == 0 && localValueLen > 0) {
                                                            paramAttr.put(PATH, localValue);  
                                                  
//                                                        } else if (localTopName.compareTo(DATA_SOURCE) == 0 && localValueLen > 0) {
//                                                            dataSourceAttr.put(PATH, localValue); 
                                                        }  
                                                        
                                                    } if(localName.compareTo(MATCH_FIELD) == 0) {
                                                        if(localTopName.compareTo(DEPEND_CLASS) == 0 && localValueLen > 0) {
                                                            dependClassAttr.put(MATCH_FIELD, localValue);                                                  
                                                        }
                                                    } if(localName.compareTo(STATUS) == 0) {
                                                        if(localTopName.compareTo(CURVE_ADJUST) == 0 && localValueLen > 0) {
                                                            curveAdjAttr.put(STATUS, localValue);                                                  
                                                        } else {
                                                            curveAdjAttr.put(STATUS, "false");
                                                        }                                                   
                                                    }                              
                                                } 
                                            
                                                if (localTopName.compareTo(DATA_SOURCE) == 0) {                                            
                                                    dataSources.add(dataSourceAttr);
                                        
                                                } else if (localTopName.compareTo(CODE_NAME) == 0) {
                                                    codeNames.add(codeNameAttr);
                                                    // The code name - description map for the gui
                                                    if (codeNameAttr.containsKey(DESC) && codeNameAttr.containsKey(NAME)) {
                                                        codeNamesDesc.put(codeNameAttr.get(NAME).toString(), codeNameAttr.get(DESC).toString());
                                                    }
                                                } else if (localTopName.compareTo(PARAM) == 0) {
                                                    params.add(paramAttr);
                                                } else if (localTopName.compareTo(DEPEND_CLASS) == 0) {
                                                    dependClasses.add(dependClassAttr);
                                                } else if (localTopName.compareTo(CURVE_ADJUST) == 0) {
                                                    curveAdjClasses.add(curveAdjAttr);                                                        
                                                }
                                            }
                                        }                       
                                    }
                            
                                    if(e3.getLocalName().compareTo(PARAMS) == 0) {
                                        details.put(PARAMS, params.clone());      
                                    } else if (e3.getLocalName().compareTo(CODE_NAMES) == 0) {
                                        details.put(CODE_NAMES, codeNames.clone()); 
                                    } else if (e3.getLocalName().compareTo(DATA_SOURCES) == 0) {
                                        details.put(DATA_SOURCES, dataSources.clone()); 
                                    } else if (e3.getLocalName().compareTo(DEPEND_CLASSES) == 0) {
                                        details.put(DEPEND_CLASSES, dependClasses.clone());                              
                                    } else if (e3.getLocalName().compareTo(CURVE_ADJUST) == 0) {
                                        
                                        if(e3.getAttribute(STATUS) != null) {
                                            curveAdjAttr.put(STATUS, e3.getAttribute(STATUS));                                                  
                                        } else {
                                            curveAdjAttr.put(STATUS, "false");
                                        } 
                                        curveAdjClasses.add(curveAdjAttr);                                                          
                                        details.put(CURVE_ADJUST, curveAdjClasses.clone());                                                                                
                                    } 
                                    
                                    params.clear();
                                    codeNames.clear();
                                    dataSources.clear();    
                                    dependClasses.clear();
                                    curveAdjClasses.clear();
                                }
                            }
                            // 
                            comparatorDetails.put(fullClassName, details);                          
                        }
                    }
                }
            }
        }

        // Set the number of groups
        configB.setGroupsNumber(gNum);
            // Adjust the list of codes to account for multi-code
            secondCodeNamesDesc = ComparatorCodeProcessor.decodeComparatorsCode(codeNamesDesc);    
            // Set the code names - description map
            configB.setCodeNamesDesc(secondCodeNamesDesc.get("base"));   
            // Adjust the list of codes to account for multi-code 
            configB.setSecondCodeNamesDesc(secondCodeNamesDesc.get("secondary"));   
            
            // Adjust the list of codes to account for multi-code
            classNamesMap = ComparatorCodeProcessor.decodeComparatorsCode(classNamesMap).get("base");
        // Set the code/class name 
        configB.setClassNamesMap(classNamesMap); 
        //
        configB.setGroupAttributes(groupAttributes);
        // 
        configB.setComparatorDetails(comparatorDetails);   
        
        // Process a mapping between codeName - parameters/data sources info for the GUI 
            configB.setParametersDetailsForGUI(processGUIParameters(comparatorDetails, option, meg));    
    }

    /**
     * Process the parameters associated with all the comparators 
     * Tobe used in the core eIndex GUI.
     *
     * @param comparatorDetails all the configuration info of the comparators
     * @param option
     * @param meg an instance of the MatchingEngine
     * @return a simplified {code name - parameters/data sources/curve adjustments} map
     */   
    private Map<String, Map<String, ArrayList>> processGUIParameters(Map<String, Map> comparatorDetails, boolean option, MatchingEngine meg) {
        
        Iterator iter = comparatorDetails.keySet().iterator();
        String codeName;
        String[] codeNames = null;
        String theCode = null;
        int codeNamesLen;
        ArrayList param;
        ArrayList dSrc;
        ArrayList cAdj;    
        //
        Map<String, Map<String, ArrayList>> theCodeParams = new HashMap();
        Map<String, ArrayList> paramsList = new HashMap();
        
        while(iter.hasNext()) {
            String key = (String) iter.next();
            Map comp = (Map) comparatorDetails.get(key);
        
            ArrayList codes = (ArrayList) comp.get(CODE_NAMES);        
            int codesL = codes.size();
            
            for (int i = 0; i < codesL; i++) {
                // Code name
                codeName = (String)((Map)codes.get(i)).get(NAME);    
                try {
                    // Process composite code names
                   codeNames = ComparatorCodeProcessor.decodeComparatorsCode(codeName, option, meg);
                } catch (Exception e) {              
                    LOGGER.severe(MLOCALIZER.x("MEG014: Unable to find match comparators code name. ", e));   
                }
               codeNamesLen = codeNames.length;
               for (int j = 0; j < codeNamesLen; j++) {
                   theCode = codeNames[j];
                   paramsList.put(PARAMS,  (ArrayList) comp.get(PARAMS));
                   paramsList.put(DATA_SOURCES, (ArrayList) comp.get(DATA_SOURCES));               
                   paramsList.put(CURVE_ADJUST, (ArrayList) comp.get(CURVE_ADJUST));
                   //
                   theCodeParams.put(theCode, new HashMap(paramsList));                                       
               } 
               // Clear the map
               paramsList.clear();                
            }      
        }
        return theCodeParams;
    }
    
    
    /** Ensures the singleton instance will be reloaded with the latest configuration
     * with the next call to getInstance.
     *
     * CAUTION! This should currently ONLY be used for unit testing, because this
     * does NOT ensure that clients using the configuration service instances
     * re-load cached configuration information.
     *
     * To ensure that happens, the configuration service clients should register
     * an Observer and re-load the cached values if appropriate.
     *
     * A call to this method triggers a ConfigChangeObserver update notification.
     */
    public static void resetInstance() {
        synchronized (ComparatorsConfigurationService.class) {
            instance = null;
        }
        configServiceObservable.setChanged();
        configServiceObservable.notifyObservers();
    }
    
    /**
     * Add an Observer to be notified it the configuration changes from what
     * was originally loaded.
     *
     * @param anObserver the observer to notify.
     */
    public static void addConfigChangeObserver(java.util.Observer anObserver) {
        configServiceObservable.addObserver(anObserver);
    }
      
    /**
     * Inner class to notify of configuration changes after the initial load.
     *
     * @see java.util.Observable
     */
    protected static class ConfigurationServiceObservable extends java.util.Observable {
        /**
         * @see java.util.Observable
         */
        protected void setChanged() {
            super.setChanged();
        }
    }
}
