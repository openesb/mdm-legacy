/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.util.CharactersErrorsHandling;
import com.sun.mdm.matcher.util.StringsManipulation;

import java.util.Map;

/**
 * The algorithm comprises the Jaro string comparator plus three additional enhancements
 * due to Winkler and al. Each one makes use of the information from the prior characteristics.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class WinklerJaroStringComparator implements MatchComparator {

    /* */
    private static final String NULL50 = MatchVariables.NULL50;   
    /*  */
    private static int[] indC = {0, 0, 0, 0, 0, 0};
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
    }    

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
    }    
    
   /**
     * Close any related data sources streams
     */
    public void stop() {}   
    
    /**
     * Measure the degree of similarity between recA and recB using a generic
     * string comparator algorithm that accounts for insertions,
     * deletions, transpositions plus three additional enhancements (scanning errors,
     * large strings and linear dependencies of the error rate function of the
     * length)
     *
     * @param      recA   Candidate's string record.
     * @param      recB   Reference's string record.
     * @param      mEng  an instance of the Matching Engine class
     * @return     a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     */
    public double compareFields(String recA, String recB, Map context)
    throws MatchComparatorException {
        
        
        StringBuilder recAHold = new StringBuilder();
        StringBuilder recBHold = new StringBuilder();
        
        StringBuilder recAFlag = new StringBuilder();
        StringBuilder recBFlag = new StringBuilder();
        
        recA = recA.toUpperCase();
        recB = recB.toUpperCase(); 
        
        double weight, numSim;
        int minv;
        int searchRange;
        int lowLim;
        int recALength;
        int hiLim;
        int numTrans;
        int numCom;
        int recBLength;
        int yl1;
        int yiSt;
        int numSimi;
        int i;
        int j;
        int k;


        //Index of the last character in recA
        k = recA.length() - 1;
        
        // Identify the strings to be compared by stripping off all leading and
        // trailing spaces.
        for (j = 0; ((recA.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        for (i = k; ((recA.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        //Index of the last character in recA
        k = recB.length() - 1;
        
        // Start and end indices of field from candidate file
        recALength = i + 1 - j;
        yiSt = j;
        
        for (j = 0; ((recB.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        for (i = k; ((recB.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        recBLength = i + 1 - j;
        
        // If both fields have less than 4 characters, return a zero weight.
        // Subject to review
        if ((recALength < 4) && (recBLength < 4)) {
            return 0.0;
        }
        
        
        // Copy the trimed fields into these 2 variables
        StringsManipulation.strncat(recAHold, recA, yiSt, recALength);
        StringsManipulation.strncat(recBHold, recB, j, recBLength);
        
        // Determine which field is longer
        if (recALength > recBLength) {
            
            searchRange = recALength;
            minv = recBLength;
            
        } else {
            searchRange = recBLength;
            minv = recALength;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }
        
        String nullSub50 = NULL50.substring(0, searchRange);
        // Blank out the flags
        StringsManipulation.strncat(recAFlag, nullSub50, searchRange);
        StringsManipulation.strncat(recBFlag, nullSub50, searchRange);

        
        // Is lower-side of the middle of the largest field     
        searchRange = (searchRange >> 1) - 1;        

        // Conditionally convert all lower case characters to upper case.
        if (indC[1] == 0) {
            
            // Transform all lowercase characters of A-field to uppercase
            for (i = 0; i < recALength; i++) {
                
                if (Character.isLowerCase(recAHold.charAt(i))) {
                    
                    recAHold.setCharAt(i, Character.toUpperCase(
                    recAHold.charAt(i)));
                }
            }
            
            // Transform all lowercase characters of B-field to uppercase
            for (j = 0; j < recBLength; j++) {
                
                if (Character.isLowerCase(recBHold.charAt(j))) {
                    
                    recBHold.setCharAt(j, Character.toUpperCase(recBHold.charAt(j)));
                }
            }
        }
        
        // Looking only within the search range, count and flag the matched pairs.
        numCom = 0;
        // Index of last char in field from B-file
        yl1 = recBLength - 1;
        char hi;
        // Loop over chars of the field from A-file
        for (i = 0; i < recALength; i++) {
            
            lowLim = (i >= searchRange) ? i - searchRange : 0;
            
            hiLim = ((i + searchRange) <= yl1) ? (i + searchRange) : yl1;
            hi = recAHold.charAt(i);
            for (j = lowLim; j <= hiLim; j++) {
                
                if ((recBFlag.charAt(j) != '1')
                && (recBHold.charAt(j) == hi)) {
                    
                    recBFlag.setCharAt(j, '1');
                    recAFlag.setCharAt(i, '1');
                    numCom++;
                    break;
                }
            }
        }
        
        // If there are no characters in common, return zero agreement.
        // The definition of 'common' is that the agreeing characters must be
        // within � the length of the shorter string min (s1, s2).
        if (numCom == 0) {
            return 0.0;
        }
        
        // Count the number of transpositions. The definition of 'transposition' is
        // that the character from one string is out of order with the corresponding
        // common character from the other string (in the sens defined above).
        k = 0;
        numTrans = 0;
        
        // Loop over the A-file field chars to search for transpositions
        for (i = 0; i < recALength; i++) {
            
            // If the actual character has some corresponding commons, then
            // loop over the chars of the B-file field.
            if (recAFlag.charAt(i) == '1') {
                
                for (j = k; j < recBLength; j++) {
                    
                    // If there is a common between indices (i,j), then break the
                    // inner loop and go for next i and start search from j+1.
                    if (recBFlag.charAt(j) == '1') {
                        
                        k = j + 1;
                        break;
                    }
                }
                
                // If char from A-field has no corresponding common from B-field,
                // increment the number of transposition
                if (recAHold.charAt(i) != recBHold.charAt(j)) {
                    
                    numTrans++;
                }
            }
        }
        
        // 1/2 the transpositions
        numTrans = numTrans >> 1;

        // First inhancement by McLaughlin (1993). Adjust for similarities in
        // nonmatched characters.
        // Similar characters might occur because of scanning errors ('1' versus 'l')
        // or keypunch ('V' versus 'C'). The #common (in Jaro) gets // increased by
        // 0.3 for each similar character
        numSimi = 0;
        char hj;  
        // Authorize the use of similar characters weightings
        if (indC[1] == 0) {
            
            // Test if the min(str1, str2) is larger than the number of common
            if (minv > numCom) {
                
                for (i = 0; i < recALength; i++) {
                    
                    hi = recAHold.charAt(i);
                    // Check if the character does not have a commom and is inRange.
                    // See def, at the end.
                    if ((recAFlag.charAt(i) == ' ') && inRange(hi)) {
                        
                        // Loop aver the corresponding B-field
                        for (j = 0; j < recBLength; j++) {
                            
                            hj = recBHold.charAt(j);
                            // Check also if the character does not have
                            // a commom and is inRange.
                            if ((recBFlag.charAt(j) == ' ') && inRange(hj)) {
                                
                                // Test if the pair of chars is in the list of
                                // similar characters defined in array adjwt
                                if (CharactersErrorsHandling.correctCharactersErrors(hi, hj) > 0) {
                                    
                                    // Increment the number of similar pairs
                                    // Need review
                                    numSimi +=
                                    CharactersErrorsHandling.correctCharactersErrors(hi, hj);
                                    
                                    // Flag the B-field. How about the A-field?
                                    recBFlag.setCharAt(j, '2');
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        // Update the number of commons by replacing them with:
        // #common + 0.3 * #similar
        numSim = ((double) numSimi) / 10.0 + (double) numCom;
        
        // Main weight computation.
        // Since the initial formula was: 1/3(#common/length(str1) + #common/
        // length(str2) + 1/2*(#transposition / #common).
        // The new formula (adding similar chars) is:
        // 1/3((#common + 0.3*simi)/length(str1) + (#common + 0.3*simi)/length(str2)
        // + 1/2*(#transposition / #common).
        // DO NOT UNDERSTAND THE LAST PART OF THE FORMULA. A PROBLEM?
        weight = numSim / ((double) recALength) + numSim / ((double) recBLength) 
                 + ((double) (numCom - numTrans)) / ((double) numCom);
        
        // Multiply by 1/3
        weight = weight / 3.0;
        

        // Second enhancement due to Winkler (1990), based on Pollock & Zamora
        // empirical study on very large chemical data (1984).
        // Continue to boost the weight if the strings are similar
        if (weight > 0.7) {
            
            // Adjust for having up to the first 4 characters in common
            // consider up to the first 4 characters
            j = (minv >= 4) ? 4 : minv;
            
            // Consider the first j characters that agree and are not numbers(?)
            for (i = 0; ((i < j) && (recAHold.charAt(i) == recBHold.charAt(i))
            && (notNumber(recAHold.charAt(i)))); i++) {
                continue;
            }
            
            // Increase the weight of agreement  monotonically with the number of
            // agreeing fields. Need review.
            if (i != 0) {
                weight += i * 0.1 * (1.0 - weight);
            }
            
            // The third and final enhancement adjusts the string comparator value if
            // the strings are longer than 6 characters and that more than half the
            // characters after the 4th one agree. This final improvement is based on
            // comparison of tens of thousands of pairs of first names, last names
            // and street names on truly matching records (done by Lynch & Winkler).
            
            // Test if we want to add this enhancement. Test also that the min length
            // is more than 4 and that nmuber of common is more thaa the first
            // agreeing chacters. The last test need to be reviwed
            if ((indC[0] == 0) && (minv > 4) && (numCom > i + 1)
            && ((2 * numCom) >= (minv + i))) {
                
                // The first character should not be a digit
                if (notNumber(recAHold.charAt(0))) {
                    
                    weight += (double) (1.0 - weight) * ((double) (numCom - i - 1.0)
                    / ((double) (recALength + recBLength - i * 2.0 + 2.0)));
                }
            }
        }
        
        return weight;
    }
    
    // Need to be reviewed
    private static boolean inRange(char c) {
        
        return (((int) c > 0)  && ((int) c < 91)
        || ((int) c > 191)  && ((int) c < 256));  
    }
    
    // Check for characters that are not numbers [0,9]
    private static boolean notNumber(char c) {
        
        return !Character.isDigit(c);
    }
}
