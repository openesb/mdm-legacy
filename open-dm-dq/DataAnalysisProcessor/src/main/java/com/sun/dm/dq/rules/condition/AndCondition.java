/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class AndCondition extends ConditionClass {

    private ArrayList<Object> objAndXmlList;
    private ArrayList<Object> objAndStepList;

    public AndCondition(IfStepType.Condition.And objAnd) throws ProcessXMLDataException {

        processXMLObject(objAnd);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        ListIterator rlListItr = objAndStepList.listIterator();
        ConditionValue conditionVal;
        ConditionClass varConditionClass;

        while (rlListItr.hasNext()) {
            varConditionClass = (ConditionClass) rlListItr.next();
            if (varConditionClass != null) {
                conditionVal = varConditionClass.getConditionValue(dataObjHandler);

                if (conditionVal.getClass() == FalseCondition.class) {
                    return conditionVal;
                }
            }
        }

        return new TrueCondition();
    }

    private void processXMLObject(IfStepType.Condition.And objAnd) throws ProcessXMLDataException {

        processXMLObject((ArrayList<Object>) objAnd.getBooleanOperatorGroup());

    }
    /*
     *         @XmlElement(name = "istrue", type = IfStepType.Condition.Istrue.class),
    @XmlElement(name = "not", type = IfStepType.Condition.Not.class),
    @XmlElement(name = "or", type = IfStepType.Condition.Or.class),
    @XmlElement(name = "and", type = IfStepType.Condition.And.class),
    @XmlElement(name = "dataLength", type = IfStepType.Condition.DataLength.class),
    @XmlElement(name = "matches", type = IfStepType.Condition.Matches.class),
    @XmlElement(name = "equals", type = IfStepType.Condition.Equals.class)
     */

    private void processXMLObject(ArrayList<Object> objAndSteps) throws ProcessXMLDataException {


        objAndXmlList = new ArrayList<Object>();
        objAndStepList = new ArrayList<Object>();
        objAndXmlList = objAndSteps;

        ListIterator rlListItr = objAndXmlList.listIterator();
        while (rlListItr.hasNext()) {

            Object var = rlListItr.next();

            if (var.getClass() == IfStepType.Condition.Isnull.class) {
                objAndStepList.add(new IsNullCondition((IfStepType.Condition.Isnull) var));

            } else if (var.getClass() == IfStepType.Condition.And.class) {
                objAndStepList.add(new AndCondition((IfStepType.Condition.And) var));

            } else if (var.getClass() == IfStepType.Condition.DataLength.class) {
                objAndStepList.add(new DataLengthCondition((IfStepType.Condition.DataLength) var));

            } else if (var.getClass() == IfStepType.Condition.Equals.class) {
                objAndStepList.add(new EqualsCondition((IfStepType.Condition.Equals) var));

            } else if (var.getClass() == IfStepType.Condition.Matches.class) {
                objAndStepList.add(new MatchesCondition((IfStepType.Condition.Matches) var));

            } else if (var.getClass() == IfStepType.Condition.Not.class) {
                objAndStepList.add(new NotCondition((IfStepType.Condition.Not) var));

            } else if (var.getClass() == IfStepType.Condition.Or.class) {
                objAndStepList.add(new OrCondition((IfStepType.Condition.Or) var));

            }

        }

    }
}
