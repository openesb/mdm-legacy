/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.mdm.index.dataobject.objectdef.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ValidateForLength implements Rule {

    HashMap<String, String> ePathList;

    /**
     * Creates a new instance of CleansingRuleImpl
     */
    public ValidateForLength(HashMap<String, Field> ePathList) throws ProcessXMLDataException {
        processXMLObject(ePathList);
    }

    public ArrayList<RuleErrorObject> execute(DataObjectHandler dataObjHandler) {
        Set keys = ePathList.keySet();
        Iterator rlListItr = keys.iterator();
        int len = 0;
        int fldLen = 0;
        String valStr;
        List valueList;
        ArrayList<RuleErrorObject> errObj = new ArrayList<RuleErrorObject>();
        while (rlListItr.hasNext()) {
            String ePath = (String) rlListItr.next();
            if (ePath != null) {
                len = Integer.parseInt(ePathList.get(ePath));
                try {
                    valueList = dataObjHandler.getFieldValueList(ePath);
                    for (int i = 0; i < valueList.size(); i++) {
                        if (valueList.get(i) != null) {
                            valStr = (String)valueList.get(i);
                            if (valStr != null && valStr.length() > len) {
                                errObj.add(new RuleErrorObject("EPath :: " + ePath, 
                                        "  ValidateLength", "", "", "  ERR019: Data Length is more than field size", false));
                            } 
                        }
                    }
                } catch (DataObjectHandlerException ex) {
                    errObj.add(new RuleErrorObject("EPath :: " + ePath, "  ValidateLength", String.valueOf(len), String.valueOf(fldLen), ex.getLocalizedMessage(), true));
                }
            }
        }
        return errObj;
    }

    public void executeRuleStep() {
    }

    private void processXMLObject(HashMap<String, Field> pList) throws ProcessXMLDataException {

        ePathList = new HashMap<String, String>();
        Set keys = pList.keySet();
        Iterator rlListItr = keys.iterator();
        String len = "0";
        Field fld = null;

        while (rlListItr.hasNext()) {
            String ePath = (String) rlListItr.next();

            if (ePath != null) {
                try {
                    fld = (Field) pList.get(ePath);
                    ePathList.put(ePath, String.valueOf(fld.getSize()));

                } catch (Exception ex) {
                    throw new ProcessXMLDataException(ex);
                }
            }
        }
    }
}
