/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.IfStepType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class IsNullCondition extends ConditionClass {

    private String ePath;

    public IsNullCondition(IfStepType.Condition.Isnull objIsNull) throws ProcessXMLDataException {

        processXMLObject(objIsNull);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        String strVal;
        try {
            strVal = dataObjHandler.getFieldValue(ePath);

            if (strVal == null) {
                return new TrueCondition();
            }
            if (strVal.trim().length() == 0) {
                return new TrueCondition();
            }
        } catch (DataObjectHandlerException ex) {
            return new ErrorCondition();
        }
        return new FalseCondition();
    }

    private void processXMLObject(IfStepType.Condition.Isnull objIsNull) throws ProcessXMLDataException {

        ePath = VarListProcess.getVarValue(objIsNull.getFieldName());

    }
}
