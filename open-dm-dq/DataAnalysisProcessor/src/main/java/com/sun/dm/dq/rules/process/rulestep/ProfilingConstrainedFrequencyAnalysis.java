/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DBHandler;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.ProfilingRuleType;
import com.sun.dm.dq.schema.FieldListVarType.Field;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ProfilingConstrainedFrequencyAnalysis extends RuleStep implements ProfilerReport {

    private List<ProfilingRuleImpl> ruleList;
    private ArrayList<Field> constrainedFrqList;
    private String sortOrder;
    private int threshHold;
    private boolean threshHoldMore;
    private boolean sortOrderIncreasing;
    private String tblName;
    private HashMap<String, String> mapConstrainedFRQ;
    private PreparedStatement prepStmntConstrained;
    private String[] fieldsConstrainedFRQ;
    private static int objCount = 0;

    public ProfilingConstrainedFrequencyAnalysis(ProfilingRuleType.ConstrainedFrequencyAnalysis objXml, DBHandler dbHandler) throws ProcessXMLDataException {
        objCount = objCount + 1;
        this.tblName = "PROFILE_CONSTRAINED_FRQ" + "_" + String.valueOf(objCount);
        processXMLObject(objXml, dbHandler);
    }

    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis objXml, DBHandler dbHandler) throws ProcessXMLDataException {


        List<ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule> ruleXmlList;
        ruleXmlList = objXml.getRuleList().getRule();

        ListIterator rlListItr = ruleXmlList.listIterator();
        this.ruleList = new ArrayList<ProfilingRuleImpl>();
        while (rlListItr.hasNext()) {
            com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule varRule = (com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule) rlListItr.next();
            if (varRule != null) {
                this.ruleList.add(new ProfilingRuleImpl(varRule));
            }

        }

        this.constrainedFrqList = new ArrayList<Field>();
        try {
            if (objXml.getSortorder() != null) {
                this.sortOrder = VarListProcess.getVarValue(objXml.getSortorder().getFieldName());
                this.sortOrderIncreasing = objXml.getSortorder().isIncreasing();
            }
            if (objXml.getThreshold() != null) {

                this.threshHold = objXml.getThreshold().getValue();
                this.threshHoldMore = objXml.getThreshold().isMore();
            }
            if (objXml.getFields() != null) {
                rlListItr = objXml.getFields().getField().listIterator();

                while (rlListItr.hasNext()) {
                    Field varFld = (Field) rlListItr.next();
                    varFld.setFieldName(VarListProcess.getVarValue(varFld.getFieldName()));
                    if (varFld != null) {
                        this.constrainedFrqList.add(varFld);
                    }
                }

                dbHandler.createConstraintFrequencyTable(this.tblName, this.constrainedFrqList);
                this.mapConstrainedFRQ = dbHandler.getMapConstrainedFRQ();
                this.prepStmntConstrained = dbHandler.getPrepStmntConstrained();
                this.fieldsConstrainedFRQ = dbHandler.getFieldsConstrainedFRQ();
            }
        } catch (DBHandlerException ex) {
            throw new ProcessXMLDataException(ex);
        }

    }

    public void execute(DBHandler dbHandler) throws ProfilerRuleException {


        ListIterator rlListItr = this.ruleList.listIterator();
        ProfilingRuleImpl tmpRuleImpl;
        ArrayList<RuleErrorObject> ruleErr = null;
        try {
            while (rlListItr.hasNext()) {
                tmpRuleImpl = (ProfilingRuleImpl) (rlListItr.next());
                if (tmpRuleImpl != null) {
                    ruleErr = tmpRuleImpl.execute(dbHandler.getDataObjectHandler());
                    if (ruleErr != null && ruleErr.size() > 0) {
                        return ;
                    }
                }
            }
            if (ruleErr == null || ruleErr.size() == 0) {
                dbHandler.insertConstrainedFRQ(this.fieldsConstrainedFRQ, this.prepStmntConstrained, this.mapConstrainedFRQ);
            }
        } catch (DBHandlerException ex) {
            throw new ProfilerRuleException(ex);
        }

    }

    public void getReport(DBHandler dbHandler) throws DBHandlerException {

        dbHandler.writeReportConstrainedFRQ("CF", this.tblName, this.mapConstrainedFRQ, this.threshHold, this.threshHoldMore, this.sortOrderIncreasing, this.sortOrder);

    }
}
