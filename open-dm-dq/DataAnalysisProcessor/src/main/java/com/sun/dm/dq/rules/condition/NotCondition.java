/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class NotCondition extends ConditionClass {

    private ConditionClass conditionVal;

    public NotCondition(IfStepType.Condition.Not objNot) throws ProcessXMLDataException {

        processXMLObject(objNot);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        ConditionValue condVal = conditionVal.getConditionValue(dataObjHandler);

        if (condVal.getClass() == TrueCondition.class) {
            return new FalseCondition();
        } else if (condVal.getClass() == FalseCondition.class) {
            return new TrueCondition();
        }
        return condVal;
    }

    private void processXMLObject(IfStepType.Condition.Not objNot) throws ProcessXMLDataException {


        if (objNot.getAnd() != null) {
            conditionVal = new AndCondition(objNot.getAnd());
        } else if (objNot.getDataLength() != null) {
            conditionVal = new DataLengthCondition(objNot.getDataLength());
        } else if (objNot.getEquals() != null) {
            conditionVal = new EqualsCondition(objNot.getEquals());
        } else if (objNot.getIsnull() != null) {
            conditionVal = new IsNullCondition(objNot.getIsnull());
        } else if (objNot.getMatches() != null) {
            conditionVal = new MatchesCondition(objNot.getMatches());
        } else if (objNot.getNot() != null) {
            conditionVal = new NotCondition(objNot.getNot());
        } else if (objNot.getOr() != null) {
            conditionVal = new OrCondition(objNot.getOr());
        }

    }
}
