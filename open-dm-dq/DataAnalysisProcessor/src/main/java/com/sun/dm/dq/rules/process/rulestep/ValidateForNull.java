/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.mdm.index.dataobject.objectdef.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ValidateForNull implements Rule {

    String[] ePathList;

    /**
     * Creates a new instance of CleansingRuleImpl
     */
    public ValidateForNull(HashMap<String, Field> eList) throws ProcessXMLDataException {
        processXMLObject(eList);

    }

    public ArrayList<RuleErrorObject> execute(DataObjectHandler dataObjHandler) {

        List valueList;
        ArrayList<RuleErrorObject> errObj = new ArrayList<RuleErrorObject>();

        for (int i = 0; i < ePathList.length; i++) {
            try {
                if (ePathList[i] != null) {
                    valueList = dataObjHandler.getFieldValueList(ePathList[i]);
                    if (valueList != null) {
                        for (int j = 0; j < valueList.size(); j++) {
                            if (valueList.get(j) == null) {
                                errObj.add(new RuleErrorObject("EPath :: "+ ePathList[i], "", ePathList[i], "", "ERR016: The value is Null", false));
                            }
                        }
                    } else {
                        errObj.add(new RuleErrorObject("EPath :: "+ ePathList[i], "", "", "", "ERR016: The value is Null", false));
                    }
                } else {
                    errObj.add(new RuleErrorObject("EPath :: "+ ePathList[i], "", "", "", "ERR016: The value is Null", false));
                }
            } catch (DataObjectHandlerException ex) {
                errObj.add(new RuleErrorObject("EPath :: " + ePathList[i], "", "", "", ex.getLocalizedMessage(), true));
            }
        }

        return errObj;
    }

    public void executeRuleStep() {
    }

    private void processXMLObject(HashMap<String, Field> pList) throws ProcessXMLDataException {

        //ePathList = new String[pList.size()];
        ArrayList eList = new ArrayList<String>();
        Set keys = pList.keySet();
        Iterator rlListItr = keys.iterator();
        String len = "0";
        String ePath;
        Field fld = null;
        int i = 0;
        while (rlListItr.hasNext()) {
            ePath = (String) rlListItr.next();

            if (ePath != null) {
                try {
                    fld = (Field) pList.get(ePath);
                    if (fld != null && fld.isRequired() == true) {
                        //ePathList[i] = ePath;
                        eList.add(ePath);
                    }
                    
                } catch (Exception ex) {
                    throw new ProcessXMLDataException(ex);
                }
            }
        }
        ePathList = new String[eList.size()];
        eList.toArray(ePathList);
    }
}
