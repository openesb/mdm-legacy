/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */



package com.sun.dm.dq.process;

import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.DataAnalysisRules;
import com.sun.dm.dq.util.Localizer;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import net.java.hulp.i18n.Logger;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataAnalysisProcess {

    private static CleansingProcess cleansingProcess;
    private static ProfilingProcess profilingProcess;
    private static DataAnalysisRules dataAnalysisRules;
    private String configFilePath = "";
    private transient final Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient final Localizer mLocalizer = Localizer.get();
    
    /**
     * 
     * @param confFilePath 
     */
    public DataAnalysisProcess(String confFilePath) {
        this.configFilePath = confFilePath;
        cleansingProcess = null;
        profilingProcess = null;
        dataAnalysisRules = loadDataAnalysisRules(confFilePath);

    }

    private DataAnalysisRules loadDataAnalysisRules(String confFilePath) {
        JAXBContext jc;
        DataAnalysisRules dr = null;
        try {
            jc = JAXBContext.newInstance("com.sun.dm.dq.schema");
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            File file = new File(confFilePath);
            dr = (DataAnalysisRules) unmarshaller.unmarshal(file);
        } catch (JAXBException ex) {
            mLogger.severe(mLocalizer.x("DAP999: Error " +
                                                "unable to load rule data binding objects: {0}", ex));
        }
        mLogger.info(mLocalizer.x("DAP001: Rules are Loaded !!! "));
        return dr;
    }

    /**
     * 
     * @return 
     */
    public CleansingProcess getCleansingProcess() throws ProcessXMLDataException {
        if (cleansingProcess == null) {
            cleansingProcess = CleansingProcess.getCleansingProcess(dataAnalysisRules.getCleansingRules());
        }
        mLogger.info(mLocalizer.x("DAP002: Received Cleanser Process !!! "));
        return cleansingProcess;
    }

    /**
     * 
     * @param confFile 
     * @return 
     */
    public CleansingProcess getCleansingProcess(String confFile) throws ProcessXMLDataException {
        DataAnalysisRules dr = loadDataAnalysisRules(confFile);
        cleansingProcess = CleansingProcess.getCleansingProcess(dr.getCleansingRules());
        mLogger.info(mLocalizer.x("DAP003: Received Cleanser Process from rule config : {0}",this.configFilePath));
        return cleansingProcess;
    }

    /**
     * 
     * @return 
     */
    public ProfilingProcess getProfilingProcess() throws ProcessXMLDataException, DBHandlerException {
        if (profilingProcess == null) {
            profilingProcess = ProfilingProcess.getProfilingProcess(dataAnalysisRules.getProfilingRules());
        }
        mLogger.info(mLocalizer.x("DAP006: Received Profiler Process !!! "));
        return profilingProcess;
    }

    /**
     *  
     * @param confFile 
     * @return 
     */
    public ProfilingProcess getProfilingProcess(String confFile) throws ProcessXMLDataException, DBHandlerException {
        DataAnalysisRules dr = loadDataAnalysisRules(confFile);
        profilingProcess = ProfilingProcess.getProfilingProcess(dr.getProfilingRules());
        mLogger.info(mLocalizer.x("DAP005: Received Profiler Process from rule config : {0}", this.configFilePath));
        return profilingProcess;
    }
}
