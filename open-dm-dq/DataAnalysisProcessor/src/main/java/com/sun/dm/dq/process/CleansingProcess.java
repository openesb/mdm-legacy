/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.process;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.data.DataReader.CustomDataReader;
import com.sun.dm.dq.rules.data.DataReader.DataObjectDataReader;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.output.RulesOutput;
import com.sun.dm.dq.rules.process.rulestep.CleansingRulesImpl;
import com.sun.dm.dq.rules.process.rulestep.Rule;
import com.sun.dm.dq.rules.process.rulestep.ValidateForLength;
import com.sun.dm.dq.rules.process.rulestep.ValidateForNull;
import com.sun.dm.dq.rules.process.rulestep.ValidateForType;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.mdm.index.dataobject.DataObject;
import com.sun.mdm.index.dataobject.objectdef.Field;
import com.sun.dm.dq.schema.CleansingRuleType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class CleansingProcess {

    private CleansingRuleType cleansingRule;
    private String objDefFilePath;
    private DataObjectHandler dataObjHandler;
    private boolean validateDataType = false;
    private boolean validateDataLength = false;
    private boolean validateDataNull = false;
    private static CleansingProcess cleansingProcess;
    private CleansingRulesImpl cleansingRuleImpl;
    private HashMap<String, String> varsList;
    private ArrayList<Rule> validateRules;
    private String dbConnection;
    private CustomDataReader dbReader;
    private DataObjectDataReader dataObjectdbReader;
    private long cleanser_startcounter;
    private String dateFormatGoodFile;
    private String goodFilePath;
    private String badFilePath;
    private boolean standardizer = true;

    

    /** Creates a new instance of CleansingProcess */
    private CleansingProcess(CleansingRuleType objXml) throws ProcessXMLDataException {

        cleansingRule = objXml;
        VarListProcess cleansingVarList = null;
        if (objXml.getVarList() != null) {
            cleansingVarList = new VarListProcess(objXml);
        }

        processXMLObject(objXml);
        if (cleansingVarList != null) {
            cleansingVarList.removeAll();
            cleansingVarList = null;
        }

        objDefFilePath = cleansingRule.getCleansingVariable().getObjectdefFilePath();
        dbConnection = cleansingRule.getCleansingVariable().getDBconnection();
        goodFilePath = cleansingRule.getCleansingVariable().getGoodFilePath();
        badFilePath = cleansingRule.getCleansingVariable().getBadFilePath();
        validateDataLength = cleansingRule.getCleansingVariable().isValidateLength();
        validateDataNull = cleansingRule.getCleansingVariable().isValidateNull();
        validateDataType = cleansingRule.getCleansingVariable().isValidateType();
        cleanser_startcounter = cleansingRule.getCleansingVariable().getStartcounter();
        if (cleanser_startcounter <= 0) {
            cleanser_startcounter = 1;
        }
        dataObjHandler = new DataObjectHandler(objDefFilePath);
        dateFormatGoodFile = dataObjHandler.getDateFormat();
        if (cleansingRule.getCleansingVariable().isStandardizer() == null) {
            this.standardizer = true;
        }
        else {
            this.standardizer = cleansingRule.getCleansingVariable().isStandardizer() ;
        }
        validateDataObject();

    }
    // Validate all the fields for the first time
    // If new cleansing process is created then do it again
    /**
     *
     * @param parent
     * @return
     */
    public RulesOutput executeRules(DataObject parent) {

        ArrayList<ArrayList<RuleErrorObject>> rulesErr = null;
        dataObjHandler.setParent(parent);

        rulesErr = executeValidateDataObject(dataObjHandler);
        if (rulesErr != null && rulesErr.size() > 0) {
            RulesOutput ruleOutput = new RulesOutput(rulesErr, dataObjHandler.getParent());
            return ruleOutput;
        }

        rulesErr = cleansingRuleImpl.executeRules(dataObjHandler);
        RulesOutput ruleOutput = new RulesOutput(rulesErr, dataObjHandler.getParent());
        return ruleOutput;
    }

    static CleansingProcess getCleansingProcess(CleansingRuleType objXml) throws ProcessXMLDataException {

        cleansingProcess = new CleansingProcess(objXml);
        return cleansingProcess;
    }

    private void processXMLObject(CleansingRuleType objXml) throws ProcessXMLDataException {

        CleansingRuleType.RuleList rl = (CleansingRuleType.RuleList) cleansingRule.getRuleList();

        cleansingRuleImpl = new CleansingRulesImpl(rl);
        if (objXml.getDatareader() != null) {
            dbReader = new CustomDataReader(objXml.getDatareader());
        } else {
            dbReader = null;
        }
        if (objXml.getDataObjectReader() != null) {
            dataObjectdbReader = new DataObjectDataReader(objXml.getDataObjectReader());
        } else {
            dataObjectdbReader = null;
        }

    }

    private void validateDataObject() throws ProcessXMLDataException {
        validateRules = new ArrayList<Rule>();
        HashMap<String, Field> fldMap = dataObjHandler.getFieldsAll();
        if (validateDataLength) {
            validateRules.add(new ValidateForLength(fldMap));
        }
        if (validateDataType) {
            validateRules.add(new ValidateForType(fldMap));
        }
        if (validateDataNull) {
            validateRules.add(new ValidateForNull(fldMap));
        }

    }

    private ArrayList<ArrayList<RuleErrorObject>> executeValidateDataObject(DataObjectHandler dataObjHandler) {

        ArrayList<ArrayList<RuleErrorObject>> rulesErr = new ArrayList<ArrayList<RuleErrorObject>>();
        ArrayList<RuleErrorObject> err;
        Rule rl;
        for (int i = 0; i < validateRules.size(); i++) {
            rl = validateRules.get(i);
            err = rl.execute(dataObjHandler);
            if (err != null && err.size() > 0) {
                rulesErr.add(err);
            }
        }

        return rulesErr;
    }

    public String getDateFormatGoodFile() {
        return dateFormatGoodFile;
    }

    public String getBadFilePath() {
        return badFilePath;
    }

    public long getCleanserStartCounter() {
        return cleanser_startcounter;
    }
    
    public boolean isStandardizer() {
        return standardizer;
    }

    public DataObjectDataReader getDataObjectdbReader() {
        return dataObjectdbReader;
    }

    public CustomDataReader getDbReader() {
        return dbReader;
    }

    public String getDbConnection() {
        return dbConnection;
    }

    public String getGoodFilePath() {
        return goodFilePath;
    }

    public String getObjectDefFilePath() {
        return objDefFilePath;
    }
}
