/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DBHandler;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.ProfilingRuleType;
import com.sun.dm.dq.schema.FieldListVarType.Field;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ProfilingPatternFrequencyAnalysis extends RuleStep implements ProfilerReport {

    private ArrayList<Field> patternFrqList;
    private String sortOrder;
    private int threshHold;
    private ProfilingRuleType.PatternFrequencyAnalysis.TopNpattern topNPattern;
    //private boolean topNPatternMore;
    private boolean threshHoldMore;
    private boolean sortOrderIncreasing;
    private String tblName;
    private HashMap<String, String> mapPatternFRQ;
    private PreparedStatement prepStmntPattern;
    private String[] fieldsPatternFRQ;
    private static int objCount = 0;

    public ProfilingPatternFrequencyAnalysis(ProfilingRuleType.PatternFrequencyAnalysis objPatternFrequencyAnalysis, DBHandler dbHandler) throws ProcessXMLDataException {
        objCount = objCount + 1;
        this.tblName = "PROFILE_PATTERN_FRQ" + "_" + String.valueOf(objCount);
        processXMLObject(objPatternFrequencyAnalysis, dbHandler);
    }

    private void processXMLObject(ProfilingRuleType.PatternFrequencyAnalysis objPatternFrequencyAnalysis, DBHandler dbHandler) throws ProcessXMLDataException {

        this.patternFrqList = new ArrayList<Field>();
        try {

            this.topNPattern = objPatternFrequencyAnalysis.getTopNpattern();

            if (objPatternFrequencyAnalysis.getSortorder() != null) {
                this.sortOrder = VarListProcess.getVarValue(objPatternFrequencyAnalysis.getSortorder().getFieldName());
                this.sortOrderIncreasing = objPatternFrequencyAnalysis.getSortorder().isIncreasing();
            }
            if (objPatternFrequencyAnalysis.getThreshold() != null) {

                this.threshHold = objPatternFrequencyAnalysis.getThreshold().getValue();
                this.threshHoldMore = objPatternFrequencyAnalysis.getThreshold().isMore();
            }
            if (objPatternFrequencyAnalysis.getFields() != null) {
                ListIterator rlListItr = objPatternFrequencyAnalysis.getFields().getField().listIterator();

                while (rlListItr.hasNext()) {
                    Field varFld = (Field) rlListItr.next();
                    varFld.setFieldName(VarListProcess.getVarValue(varFld.getFieldName()));
                    if (varFld != null) {
                        this.patternFrqList.add(varFld);
                    }
                }

                dbHandler.createPatternFrequencyTable(this.tblName, this.patternFrqList);
                this.mapPatternFRQ = dbHandler.getMapPatternFRQ();
                this.prepStmntPattern = dbHandler.getPrepStmntPattern();
                this.fieldsPatternFRQ = dbHandler.getFieldsPatternFRQ();

            }
        } catch (DBHandlerException ex) {
            throw new ProcessXMLDataException(ex);
        }

    }

    public void execute(DBHandler dbHandler) throws ProfilerRuleException {
        try {
            dbHandler.insertPatternFRQ(this.fieldsPatternFRQ, this.prepStmntPattern, this.mapPatternFRQ);

        } catch (DBHandlerException ex) {
            throw new ProfilerRuleException(ex);
        }
    }

    public void getReport(DBHandler dbHandler) throws DBHandlerException {

        if (this.topNPattern != null) {
        dbHandler.writeReportPatternFRQ("PF", this.tblName, this.mapPatternFRQ, this.threshHold, this.threshHoldMore, this.sortOrderIncreasing, this.sortOrder, this.topNPattern.getValue(), this.topNPattern.isShowall());
    } else {
         dbHandler.writeReportPatternFRQ("PF", this.tblName, this.mapPatternFRQ, this.threshHold, this.threshHoldMore, this.sortOrderIncreasing, this.sortOrder);   
    }
    

    }
}
