/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.data.DataReader;

import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.ConstructorArgType;
import com.sun.dm.dq.schema.Parameter;
import com.sun.mdm.index.dataobject.DataObject;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataObjectDataReader extends DataReaderClass {

    private ArrayList<Object> params;
    private String className;
    private boolean isSingleton;
    private Method readmethod;
    private Object objClass;
    private String methodName = "getDataObject";

    /**public class CustomDataReader implements DataReaderClass  {
     * Creates a new instance of ConditionClass
     */
    public DataObjectDataReader(com.sun.dm.dq.schema.DataObjectReader dataReader) throws ProcessXMLDataException {
        processXMLObject(dataReader);
    }

    @Override
    public DataObject readNext() {
        Object result = null;
        try {
            /*if (params != null && params.isEmpty() == false) {
            result = readmethod.invoke(objClass, params.toArray());
            } else {*/
            result = readmethod.invoke(objClass, null);
        //}

        } catch (IllegalAccessException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (DataObject) result;
    }

    private void processXMLObject(com.sun.dm.dq.schema.DataObjectReader dataReader) throws ProcessXMLDataException {
        if (dataReader == null) {
            return;
        }
        className = dataReader.getBeans().getBean().getClazz();
        isSingleton = dataReader.getBeans().getBean().isSingleton();
        if (dataReader.getBeans().getBean().getConstructorArg() != null) {
            processParamList((ArrayList<ConstructorArgType>) dataReader.getBeans().getBean().getConstructorArg());
        }

        loadDataReader();

    }

    private void loadDataReader() throws ProcessXMLDataException {


        try {

            //System.out.println( "***************************" );
            //System.out.println( value);
            Class cls = Class.forName(className);
            Constructor cons[] = cls.getConstructors();
            for (int i = 0; i < cons.length; i++) {
                if (cons[i].getParameterTypes().length == params.size()) {
                    Class paramTypes[] = cons[i].getParameterTypes();
                    for (int j = 0; j < cons[i].getParameterTypes().length; j++) {
                        
                        if (paramTypes[j].getName().compareTo(params.get(j).getClass().getName())== 0) {
                            if (j == params.size() - 1) {
                                objClass = cons[i].newInstance(params.toArray());
                            }

                            continue;
                        } else {
                            break;
                        }
                    }
                }
            }

            //objClass = cls.getConstructors()[0].getParameterTypes()[0].newInstance();

            Method methods[] = cls.getMethods();

            for (int i = 0; i < methods.length; i++) {
                if (methods[i].getName().compareTo(methodName) == 0) {
                    if (methods[i].getGenericParameterTypes().length == 0) {
                        readmethod = methods[i];
                        break;
                    }
                }
            }

        } catch (InstantiationException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(DataObjectDataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            throw new ProcessXMLDataException(ex);
        }

    }

    private void processParamList(ArrayList<ConstructorArgType> paramList) {
        /*
         *<xsd:enumeration value="java.lang.Long"/>
        <xsd:enumeration value="java.lang.Short"/>
        <xsd:enumeration value="java.lang.Byte"/>
        <xsd:enumeration value="java.lang.String"/>
        <xsd:enumeration value="java.lang.Integer"/>
        <xsd:enumeration value="java.lang.Boolean"/>
        <xsd:enumeration value="java.lang.Double"/>
        <xsd:enumeration value="java.lang.Float"/>
         */
        params = new ArrayList<Object>();
        ListIterator rlListItr = paramList.listIterator();
        ConstructorArgType param = null;
        while (rlListItr.hasNext()) {
            param = (ConstructorArgType) (rlListItr.next());
            if (param != null) {
                if (param.getType().compareTo("java.lang.Long") == 0) {
                    params.add(Long.valueOf(param.getValue()));
                } else if (param.getType().compareTo("java.lang.Short") == 0) {
                    params.add(Short.valueOf(param.getValue()));

                } else if (param.getType().compareTo("java.lang.Byte") == 0) {
                    params.add(Byte.valueOf(param.getValue()));

                } else if (param.getType().compareTo("java.lang.String") == 0) {
                    params.add(new String( param.getValue().toString()));

                } else if (param.getType().compareTo("java.lang.Integer") == 0) {
                    params.add(Integer.valueOf(param.getValue()));

                } else if (param.getType().compareTo("java.lang.Boolean") == 0) {
                    params.add(Long.valueOf(param.getValue()));

                } else if (param.getType().compareTo("java.lang.Double") == 0) {
                    params.add(Boolean.valueOf(param.getValue()));

                } else if (param.getType().compareTo("java.lang.Float") == 0) {
                    params.add(Float.valueOf(param.getValue()));

                }
            }
        }
    }
}
    

