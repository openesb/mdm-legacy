/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.CleansingRuleType;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class MatchFromFileRule extends RuleStep {

    private String delimiter;
    private String ePath;
    private String filePath;
    private boolean isExact;
    private ArrayList strArrayList;

    
    public MatchFromFileRule(CleansingRuleType.RuleList.Rule.MatchFromFile objXmlMatchFromFile) throws ProcessXMLDataException {

        processXMLObject(objXmlMatchFromFile);
    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {

        String value = null;

        try {
            value = dataObjHandler.getFieldValue(ePath);

            if (strArrayList.contains(value)) {
                return null;
            } else {
                return new RuleErrorObject("EPath :: " + ePath, " MatchFromFileRule", filePath, value, "ERR007: value is not in the file", false);
            }
        } catch (DataObjectHandlerException ex) {
            return new RuleErrorObject("EPath :: " + ePath, " MatchFromFileRule", filePath, value, ex.getLocalizedMessage(), true);
        }
    }

    private void processXMLObject(CleansingRuleType.RuleList.Rule.MatchFromFile objXmlMatchFromFile)
            throws ProcessXMLDataException {

        delimiter = objXmlMatchFromFile.getDelimiter();
        ePath = VarListProcess.getVarValue(objXmlMatchFromFile.getFieldName());
        filePath = objXmlMatchFromFile.getFilePath();
        isExact = objXmlMatchFromFile.isExact();
        createStringArray();

    }

    private void createStringArray() {

        BufferedReader buffer;
        String strLine = null;
        strArrayList = new ArrayList();
        String[] strArr;

        try {

            buffer = new BufferedReader(new FileReader(filePath));
            strLine = buffer.readLine();

            while (strLine != null) {
                strArr = strLine.split(delimiter + "");
                for (int i = 0; i < strArr.length; i++) {
                    strArrayList.add(strArr[i]);
                }
                strLine = buffer.readLine();
            }

        } catch (FileNotFoundException ex) {
            new RuleErrorObject("EPath :: " + ePath, " MatchFromFileRule", filePath, "", ex.getLocalizedMessage(), true);
        } catch (IOException ex) {
            new RuleErrorObject("EPath :: " + ePath, " MatchFromFileRule", filePath, "", ex.getLocalizedMessage(), true);
        }

    }
}
