/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DBHandler;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.schema.ProfilingRuleType;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ProfilingRulesImpl {

    List<RuleStep> ruleList;

    public ProfilingRulesImpl(ProfilingRuleType objRuleList, DBHandler dbHandler) throws ProcessXMLDataException {

        processXMLObject(objRuleList, dbHandler);
    }

    /* public void executeRule(DBHandler dbHandler) throws ProfilerRuleException {
    ListIterator rlListItr = ruleList.listIterator();
    RuleStep tmpRuleImpl;
    while (rlListItr.hasNext()){
    tmpRuleImpl = (RuleStep)(rlListItr.next());
    if (tmpRuleImpl != null) {
    tmpRuleImpl.execute(dbHandler);
    }
    }
    }*/
    private void processXMLObject(ProfilingRuleType objRuleList, DBHandler dbHandler) throws ProcessXMLDataException {

        ruleList = new ArrayList<RuleStep>();

        List lst = null;
        if (objRuleList.getSimpleFrequencyAnalysis() != null) {
            lst = objRuleList.getSimpleFrequencyAnalysis();
            for (int i = 0; i < lst.size(); i++) {
                ruleList.add(new ProfilingSimpleFrequencyAnalysis((ProfilingRuleType.SimpleFrequencyAnalysis) lst.get(i), dbHandler));
            }
        }
        if (objRuleList.getConstrainedFrequencyAnalysis() != null) {
            lst = objRuleList.getConstrainedFrequencyAnalysis();
            for (int i = 0; i < lst.size(); i++) {
                ruleList.add(new ProfilingConstrainedFrequencyAnalysis((ProfilingRuleType.ConstrainedFrequencyAnalysis) lst.get(i), dbHandler));
            }
        }
        if (objRuleList.getPatternFrequencyAnalysis() != null) {
            lst = objRuleList.getPatternFrequencyAnalysis();
            for (int i = 0; i < lst.size(); i++) {
                ruleList.add(new ProfilingPatternFrequencyAnalysis((ProfilingRuleType.PatternFrequencyAnalysis) lst.get(i), dbHandler));
            }
        }

    }

    public void executeRules(DBHandler dbHandler) throws ProfilerRuleException {

        ListIterator rlListItr = ruleList.listIterator();
        RuleStep tmpRuleImpl;

        while (rlListItr.hasNext()) {
            tmpRuleImpl = (RuleStep) (rlListItr.next());
            if (tmpRuleImpl != null) {
                tmpRuleImpl.execute(dbHandler);

            }
        }

    }

    public void getReport(DBHandler dbHandler) throws ProfilerRuleException, DBHandlerException {

        ListIterator rlListItr = ruleList.listIterator();
        ProfilerReport tmpProfileRep;

        while (rlListItr.hasNext()) {
            tmpProfileRep = (ProfilerReport) (rlListItr.next());
            if (tmpProfileRep != null) {
                tmpProfileRep.getReport(dbHandler);

            }
        }

    }
}
