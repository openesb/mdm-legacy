/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.variablelist;

import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.CleansingRuleType;
import com.sun.dm.dq.schema.ProfilingRuleType;
import com.sun.dm.dq.schema.VarType;
import java.util.HashMap;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class VarListProcess {
    
    private static HashMap<String,String> varsList;
    
    /**
     * Creates a new instance of VarListProcess
     */
    public VarListProcess(CleansingRuleType objCleansingRule) throws ProcessXMLDataException {
        processXMLObject(objCleansingRule);
    }
    public VarListProcess(ProfilingRuleType objProfilingRule) throws ProcessXMLDataException {
        processXMLObject(objProfilingRule);
    }
    
    
    private void processXMLObject(CleansingRuleType objCleansingRule) throws ProcessXMLDataException {
        
        ListIterator rlListItr =  objCleansingRule.getVarList().getVar().listIterator();
        varsList = new HashMap<String,String>();
        
        while (rlListItr.hasNext()){
            VarType var = (VarType) rlListItr.next();
            if (var != null){
                varsList.put(var.getName(),var.getDefault());
            }
        }
    }
    private void processXMLObject(ProfilingRuleType objProfilingRule) throws ProcessXMLDataException {
        
        ListIterator rlListItr =  objProfilingRule.getVarList().getVar().listIterator();
        varsList = new HashMap<String,String>();
        
        while (rlListItr.hasNext()){
            VarType var = (VarType) rlListItr.next();
            if (var != null){
                varsList.put(var.getName(),var.getDefault());
            }
        }
    }
    public static String getVarValue(String var) throws ProcessXMLDataException {
        if (var == null) return null;
        if (varsList == null) return var;
        if (var.startsWith(":[") && var.endsWith("]")) {
            String key = var.substring(2,var.length() -1);
            String varVal = varsList.get(key);
            if (varVal == null) throw new ProcessXMLDataException("The Variable " + var + " not declared");
            return varVal;
        }else return var;
    }
    
    public  String[] getVarList() throws ProcessXMLDataException {
        return (String[]) varsList.values().toArray(new String[0]);
    }
    
    public static void removeAll()  {
        if (varsList == null) return ;
        varsList.clear();
        varsList = null;
    }
}
