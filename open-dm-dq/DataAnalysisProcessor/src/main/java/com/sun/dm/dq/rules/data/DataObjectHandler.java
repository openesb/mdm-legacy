/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.data;

import com.sun.dm.dq.rules.data.ObjectDefModifier.ObjectDefModifier;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.mdm.index.dataobject.DataObject;
import com.sun.mdm.index.dataobject.epath.DOEpath;
import com.sun.mdm.index.dataobject.objectdef.Field;
import com.sun.mdm.index.dataobject.objectdef.Lookup;
import com.sun.mdm.index.dataobject.objectdef.ObjectDefinition;
import com.sun.mdm.index.dataobject.objectdef.ObjectDefinitionBuilder;
import com.sun.mdm.index.objects.epath.EPath;
import com.sun.mdm.index.objects.epath.EPathParser;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataObjectHandler {

    /**
     * Creates a new instance of DataObjectHandler
     */
    private Lookup lookup;
    private DataObject parent;
    private DataObjectHandler doHandler;
    private HashMap<String, Field> ePathList;
    private String dateFormat;

    public DataObjectHandler(String objDefFilePath) {
        FileInputStream fileInputStream = null;

        ePathList = new HashMap<String, Field>();
        try {

            fileInputStream = new FileInputStream(objDefFilePath);
            ObjectDefinitionBuilder objDefBuilder = new ObjectDefinitionBuilder();
            ObjectDefinition objDef = ObjectDefModifier.getModifiedDataObject(objDefBuilder.parse(fileInputStream));
            // ObjectDefinition objDef =  objDefBuilder.parse(fileInputStream);
            dateFormat = objDef.getDateFormat();
            fileInputStream.close();
            lookup = Lookup.createLookup(objDef);

            buildFieldList(objDef, objDef.getName());
            parent = null;


        } catch (FileNotFoundException ex) {
        //ex.printStackTrace();

        } catch (IOException ex) {
        //ex.printStackTrace();
        }

    }

    public HashMap<String, Field> getFieldsAll() {

        return ePathList;

    }

    public String getDateFormat() {
        return dateFormat;
    }

    private void buildFieldList(ObjectDefinition objDef, String strParent) {
        ArrayList<Field> fldArray = objDef.getFields();

        Field tmpField;
        if (fldArray != null) {
            for (int i = 0; i < fldArray.size(); i++) {
                tmpField = (Field) (fldArray.get(i));
                String strKey = "";
                if (tmpField != null) {
                    strKey = strParent + "." + tmpField.getName();
                    ePathList.put(strKey, tmpField);

                }
            }
        }
        ArrayList<ObjectDefinition> objDefArray = objDef.getChildren();
        ObjectDefinition tmpObjDef;
        if (objDefArray != null) {
            for (int i = 0; i < objDefArray.size(); i++) {
                tmpObjDef = (ObjectDefinition) (objDefArray.get(i));
                if (tmpObjDef != null) {
                    String strParentKey = "";
                    strParentKey = strParent + "." + tmpObjDef.getName();
                    buildFieldList(tmpObjDef, strParentKey);

                }
            }
        }

    }

    public void setParent(DataObject dataObject) {

        this.parent = dataObject;

    }

    public DataObject getParent() {

        return this.parent;

    }

    public Field getFieldObject(String ePath) throws DataObjectHandlerException {

        Field fieldObj = null;
        try {

            fieldObj = ePathList.get(ePath);
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
        return fieldObj;

    }

    public String getFieldValue(String ePath) throws DataObjectHandlerException {

        String fieldValue = null;
        try {
            EPath ep = EPathParser.parse(ePath);
            fieldValue = (String) DOEpath.getFieldValue(ep, parent, lookup);
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
        return fieldValue;

    }

    public String getFieldValueTruncated(String ePath) throws DataObjectHandlerException {

        String fieldValue = null;
        try {
            EPath ep = EPathParser.parse(ePath);
            fieldValue = DOEpath.getFieldValue(ep, parent, lookup);
            int fldsize = ePathList.get(replaceStrings(ePath)).getSize();
            if (fieldValue != null && fieldValue.length() > fldsize) {
                fieldValue = fieldValue.substring(0, fldsize - 1);
                fieldValue = fieldValue + "*";
            }
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
        return fieldValue;

    }

    private String replaceStrings(String str) {

        if (str != null) {
            str = str.replaceAll("\\[[0-9*]+\\]", "");
            str = str.replace("[", "");
            str = str.replace("]", "");
        }
        return str;
    }

    public List<String> getFieldValueListTruncated(String ePath) throws DataObjectHandlerException {

        List<String> list = null;
        String fieldValue = null;
        try {
            EPath ep = EPathParser.parse(ePath);
            list = DOEpath.getFieldValueList(ep, parent, lookup);
            if (list == null || list.size() <= 0) {
                return null;
            }
            int fldsize = ePathList.get(replaceStrings(ePath)).getSize();
            for (int i = 0; i < list.size(); i++) {
                fieldValue = list.get(i);
                if (fieldValue != null && fieldValue.length() > fldsize) {
                    fieldValue = fieldValue.substring(0, fldsize - 1);
                    fieldValue = fieldValue + "*";
                    list.set(i, fieldValue);
                }
            }

            return list;
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
    }

    public List<String> getFieldValueList(String ePath) throws DataObjectHandlerException {

        List<String> list = null;
        try {
            EPath ep = EPathParser.parse(ePath);
            list = DOEpath.getFieldValueList(ep, parent, lookup);
            return list;
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
    }

    public void setFieldValue(String ePath, String value) throws DataObjectHandlerException {

        try {
            EPath ep = EPathParser.parse(ePath);
            DOEpath.setFieldValue(ep, parent, value, lookup);
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }

    }

    public DataObject getDataObject(String ePath) throws DataObjectHandlerException {

        DataObject dataObject = null;
        try {
            EPath e = EPathParser.parse(ePath);
            dataObject = DOEpath.getDataObject(e, parent, lookup);
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
        return dataObject;
    }

    public DataObject setDataObject(String ePath, DataObject value) throws DataObjectHandlerException {
        DataObject dataObject = null;
        try {
            EPath ep = EPathParser.parse(ePath);
            dataObject = DOEpath.getDataObject(ep, parent, lookup);
            DOEpath.setDataObject(ep, parent, dataObject, lookup);

        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }
        return dataObject;
    }

    public List<DataObject> getDataObjectList(String ePath) throws DataObjectHandlerException {

        List<DataObject> list = null;
        try {
            EPath ep = EPathParser.parse(ePath);

            list = DOEpath.getDataObjectList(ep, parent, lookup);
            return list;
        } catch (Exception ex) {
            throw new DataObjectHandlerException(ex);
        }

    }
}
