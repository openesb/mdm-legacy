/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.mdm.index.dataobject.objectdef.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ValidateForType implements Rule {

    HashMap<String, String> ePathList;

    public ValidateForType(HashMap<String, Field> pList) throws ProcessXMLDataException {
        processXMLObject(pList);
    }

    public ArrayList<RuleErrorObject> execute(DataObjectHandler dataObjHandler) {
        Set keys = ePathList.keySet();
        Iterator rlListItr = keys.iterator();
        String typeStr = "", value;
        List valueList;
        ArrayList<RuleErrorObject> errObj = new ArrayList<RuleErrorObject>();

        while (rlListItr.hasNext()) {
            String ePath = "";
            try {
                ePath = (String) rlListItr.next();
                value = null;
                if (ePath != null) {
                    typeStr = ePathList.get(ePath);
                    valueList = dataObjHandler.getFieldValueList(ePath);
                    for (int i = 0; i < valueList.size(); i++) {
                        if (valueList.get(i) != null) {

                            value = (String) valueList.get(i);

                            if (typeStr.trim().compareToIgnoreCase("int") == 0) {
                                Integer.parseInt(value);

                            } else if (typeStr.trim().compareToIgnoreCase("date") == 0) {

                                SimpleDateFormat dateVal = null;
                                dateVal = new SimpleDateFormat(value);

                                if (dateVal == null) {
                                    errObj.add(new RuleErrorObject("EPath :: " + ePath, "  ValidateType", ePath, "", "ERR017: The field value is not Date type", false));
                                }

                            } else if (typeStr.trim().compareToIgnoreCase("char") == 0) {
                                if (value == null || value.length() > 1) {
                                    errObj.add(new RuleErrorObject("EPath :: " + ePath, "   ValidateType", ePath, "", "ERR018: The field value is not char type", false));
                                }

                            }
                        }
                    }

                }
            } catch (DataObjectHandlerException ex) {
                errObj.add(new RuleErrorObject("EPath :: " + ePath, "  ValidateType", typeStr, "", ex.getLocalizedMessage(), true));
            } catch (Exception ex) {
                errObj.add(new RuleErrorObject("EPath :: " + ePath, "  ValidateType", typeStr, "", ex.getLocalizedMessage(), true));
            }
        }
        return errObj;
    }

    public void executeRuleStep() {
    }

    private void processXMLObject(HashMap<String, Field> pList) throws ProcessXMLDataException {

        ePathList = new HashMap<String, String>();
        Set keys = pList.keySet();
        Iterator rlListItr = keys.iterator();
        String len = "0";
        Field fld = null;

        while (rlListItr.hasNext()) {
            String ePath = (String) rlListItr.next();

            if (ePath != null) {
                try {
                    fld = (Field) pList.get(ePath);
                    ePathList.put(ePath, fld.getType());

                } catch (Exception ex) {
                    throw new ProcessXMLDataException(ex);
                }
            }
        }
    }
}
