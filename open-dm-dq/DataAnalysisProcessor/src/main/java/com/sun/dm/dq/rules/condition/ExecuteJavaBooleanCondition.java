/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.IfStepType;
import com.sun.dm.dq.schema.Parameter;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ExecuteJavaBooleanCondition extends ConditionClass {

    private String cmpValue;
    private String ePath;
    private boolean isExact;
    private BigInteger timeOut;
    private ArrayList<Object> params;
    private String methodName;
    private String jarPath;
    private String className;

    public ExecuteJavaBooleanCondition(IfStepType.Condition.ExecuteJavaBoolean objExecuteJava) throws ProcessXMLDataException {
        processXMLObject(objExecuteJava);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {

        String strValue;
        ArrayList<Object> tmpParams;

        try {
            strValue = dataObjHandler.getFieldValue(ePath);
            tmpParams = (ArrayList<Object>) params.clone();
            tmpParams.add(0, strValue);

            URL[] urlList = {new File(jarPath).toURL()};
            ClassLoader loader = new URLClassLoader(urlList);

            Class cls = loader.loadClass(className);

            Object obj = cls.newInstance();

            Method methods[] = cls.getMethods();

            for (int i = 0; i < methods.length; i++) {
                if (methods[i].getName().compareTo(methodName) == 0) {
                    Object result = methods[i].invoke(obj, tmpParams.toArray());
                    boolean value = true;
                    if (result.equals(value) == false) {
                        return new FalseCondition();
                    } else if (result.equals(value) == true) {
                        return new TrueCondition();
                    }
                }
            }

        } catch (MalformedURLException ex) {
            return new ErrorCondition();
        } catch (InvocationTargetException ex) {
            return new ErrorCondition();
        } catch (IllegalAccessException ex) {
            return new ErrorCondition();
        } catch (ClassNotFoundException ex) {
            return new ErrorCondition();
        } catch (InstantiationException ex) {
            return new ErrorCondition();
        } catch (DataObjectHandlerException ex) {
            return new ErrorCondition();
        }
        return new FalseCondition();
    }

    private void processXMLObject(IfStepType.Condition.ExecuteJavaBoolean objXmlExecJava) throws ProcessXMLDataException {

        ePath = VarListProcess.getVarValue(objXmlExecJava.getFieldName());
        timeOut = objXmlExecJava.getTimeout();
        processParamList((ArrayList<Parameter>) objXmlExecJava.getParameters().getParameter());

        methodName = objXmlExecJava.getMethodName();
        jarPath = objXmlExecJava.getJarFilePath();
        className = objXmlExecJava.getClassName();

    }

    private void processParamList(ArrayList<Parameter> paramList) {
        /*
         *<xsd:enumeration value="java.lang.Long"/>
        <xsd:enumeration value="java.lang.Short"/>
        <xsd:enumeration value="java.lang.Byte"/>
        <xsd:enumeration value="java.lang.String"/>
        <xsd:enumeration value="java.lang.Integer"/>
        <xsd:enumeration value="java.lang.Boolean"/>
        <xsd:enumeration value="java.lang.Double"/>
        <xsd:enumeration value="java.lang.Float"/>
         */
        params = new ArrayList<Object>();
        ListIterator rlListItr = paramList.listIterator();
        Parameter param = null;
        while (rlListItr.hasNext()) {
            param = (Parameter) (rlListItr.next());
            if (param != null) {
                if (param.getParameterType().compareTo("java.lang.Long") == 0) {
                    params.add(Long.valueOf(param.getParameterValue()));

                } else if (param.getParameterType().compareTo("java.lang.Short") == 0) {
                    params.add(Short.valueOf(param.getParameterValue()));

                } else if (param.getParameterType().compareTo("java.lang.Byte") == 0) {
                    params.add(Byte.valueOf(param.getParameterValue()));

                } else if (param.getParameterType().compareTo("java.lang.String") == 0) {
                    params.add(param.getParameterValue().toString());

                } else if (param.getParameterType().compareTo("java.lang.Integer") == 0) {
                    params.add(Integer.valueOf(param.getParameterValue()));

                } else if (param.getParameterType().compareTo("java.lang.Boolean") == 0) {
                    params.add(Long.valueOf(param.getParameterValue()));

                } else if (param.getParameterType().compareTo("java.lang.Double") == 0) {
                    params.add(Boolean.valueOf(param.getParameterValue()));

                } else if (param.getParameterType().compareTo("java.lang.Float") == 0) {
                    params.add(Float.valueOf(param.getParameterValue()));

                }
            }
        }
    }
}
