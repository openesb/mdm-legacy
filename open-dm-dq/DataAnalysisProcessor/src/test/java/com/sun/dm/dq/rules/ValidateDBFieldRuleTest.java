/*
 * ValidateDBFieldRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:11 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.mdm.index.dataobject.DataObject;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ValidateDBFieldRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    RuleErrorObject expResult = null;
    CleansingRuleType.RuleList.Rule.ValidateDBField  validateDBXmlObj;
    ValidateDBFieldRule instance ;
    RuleErrorObject result ;
    DataObject dObj;
    String ePath1,ePath2;
    String fName,sName;
    DataObjectCreator doCreate;
    
    public ValidateDBFieldRuleTest(String testName) {
        super(testName);
        ePath1 = "Person.FirstName";
        ePath2 = "Person.LastName";
        fName = "First Name for Truncate Test";
        sName = "Second Name for Reject Test";
    }

    protected void setUp() throws Exception { 
        super.setUp();
        
        
         doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        validateDBXmlObj= new CleansingRuleType.RuleList.Rule.ValidateDBField();
        dObj = doCreate.createDataObject(fName + fName + fName +fName +fName + fName,sName+
                sName+sName+sName+sName,"21","M","10/12/2007");
        doHandler.setParent(dObj);
        
    }

    protected void tearDown() throws Exception {
         super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.ValidateDBFieldRule.
     */
    public void testExecute() throws ProcessXMLDataException, DataObjectHandlerException {
        System.out.println("execute");
        
         CleansingRuleType.RuleList.Rule.ValidateDBField objVal = getValidateDBFlds();
         instance = new ValidateDBFieldRule(objVal);
        
        
        System.out.println("First Name :: " + doHandler.getFieldValue(ePath1));
        System.out.println("Last Name :: " + doHandler.getFieldValue(ePath2));
       
         result = instance.execute(doHandler);
         
         System.out.println("First Name :: " + doHandler.getFieldValue(ePath1));
        System.out.println("Last Name :: " + doHandler.getFieldValue(ePath2));
        
        assertNotNull(result);
        
        dObj = doCreate.createDataObject(fName ,sName
                ,"21","M","10/12/2007");
        doHandler.setParent(dObj);
        result = instance.execute(doHandler);
        assertNull(result);
        
        dObj = doCreate.createDataObject(fName ,sName + sName + sName
                ,"21","M","10/12/2007");
        doHandler.setParent(dObj);
        result = instance.execute(doHandler);
        assertNotNull(result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     private CleansingRuleType.RuleList.Rule.ValidateDBField getValidateDBFlds() {
        /*
         *<xsd:enumeration value="java.lang.Long"/>
                <xsd:enumeration value="java.lang.Short"/>
                <xsd:enumeration value="java.lang.Byte"/>
                <xsd:enumeration value="java.lang.String"/>
                <xsd:enumeration value="java.lang.Integer"/>
                <xsd:enumeration value="java.lang.Boolean"/>
                <xsd:enumeration value="java.lang.Double"/>
                <xsd:enumeration value="java.lang.Float"/>
         */
       
         CleansingRuleType.RuleList.Rule.ValidateDBField obj = new CleansingRuleType.RuleList.Rule.ValidateDBField();
        CleansingRuleType.RuleList.Rule.ValidateDBField.Field fld = new CleansingRuleType.RuleList.Rule.ValidateDBField.Field();
        fld.setAction("truncate");
        fld.setFieldName("Person.FirstName");
        obj.addField(fld);
        
        fld = new CleansingRuleType.RuleList.Rule.ValidateDBField.Field();
        fld.setAction("reject");
        fld.setFieldName("Person.LastName");
        obj.addField(fld);
        
        return obj;
    }

}
