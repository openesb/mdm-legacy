/*
 * MatchesConditionTest.java
 * JUnit based test
 *
 * Created on October 13, 2007, 12:40 AM
 */

package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.process.DataObjectCreator;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class MatchesConditionTest extends TestCase {
    DataObjectHandler doHandler;
    String ePath ;
    IfStepType.Condition.Matches  matchesXmlObj;
    MatchesCondition instance ;
    ConditionValue result ;
    public MatchesConditionTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        matchesXmlObj= new IfStepType.Condition.Matches();
        matchesXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getConditionValue method, of class com.sun.di.rules.condition.MatchesCondition.
     */
    public void testGetConditionValue() throws ProcessXMLDataException {
        System.out.println("getConditionValue");
        
        matchesXmlObj.setPattern("My First*");
        // ABHI :: Remove "setExact" attribute from the Schema
        //matchesXmlObj.setExact(true);
         instance = new MatchesCondition(matchesXmlObj);
        
         result = instance.getConditionValue(doHandler);
        assertEquals(result.getClass(), TrueCondition.class);
        
        
        
        matchesXmlObj.setPattern("Second*");
        // ABHI :: Remove "setExact" attribute from the Schema
        //matchesXmlObj.setExact(true);
         instance = new MatchesCondition(matchesXmlObj);
        
         result = instance.getConditionValue(doHandler);
        assertEquals(result.getClass(), FalseCondition.class);
        
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }
    
}
