/*
 * RejectFieldRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:11 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class RejectFieldRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    CleansingRuleType.RuleList.Rule.Reject  rejectXmlObj;
    RejectFieldRule instance ;
    RuleErrorObject result ;
    
    public RejectFieldRuleTest(String testName) {
        super(testName);
         ePath = "Person.Age";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        rejectXmlObj= new CleansingRuleType.RuleList.Rule.Reject();
        rejectXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.RejectFieldRule.
     */
    public void testExecute() throws ProcessXMLDataException {
        System.out.println("execute");
        
          rejectXmlObj = new CleansingRuleType.RuleList.Rule.Reject();
          instance = new RejectFieldRule(rejectXmlObj);
         result = instance.execute(doHandler);
        assertNotNull(result);
        
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
