/*
 * CleansingProcessTest.java
 * JUnit based test
 *
 * Created on October 9, 2007, 1:40 PM
 */

package com.sun.dm.dq.process;

import junit.framework.*;
import com.sun.dm.dq.rules.output.RulesOutput;
import com.sun.mdm.index.dataobject.DataObject;
import java.util.ArrayList;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class CleansingProcessTest extends TestCase {
    
    CleansingProcess instance;
    public CleansingProcessTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        DataAnalysisProcess da = new DataAnalysisProcess(System.getProperty("user.dir") + "/src/test/java/resources/TestDataAnalysisConfig.xml");
        instance = da.getCleansingProcess();
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
        instance = null;
    }
    
    /**
     * Test of executeRules method, of class com.sun.di.process.CleansingProcess.
     */
    public void testExecuteRules() {
        //System.out.println("executeRules");
        DataObjectCreator dc = new DataObjectCreator();
        DataObject parent = dc.createDataObject("My First Name","My Last Name","30","Male","10/11/2005");
        
        ArrayList<DataObject> dataObjectList = new ArrayList<DataObject>();
        RulesOutput output = instance.executeRules(parent);
        
        assertNotNull(output);
        
    }
    
}
