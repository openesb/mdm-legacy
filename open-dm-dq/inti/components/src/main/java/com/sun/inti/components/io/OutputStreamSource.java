package com.sun.inti.components.io;

import java.io.IOException;
import java.io.OutputStream;

public interface OutputStreamSource {
    public OutputStream getOutputStream() throws IOException;
}
