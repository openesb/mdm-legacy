package com.sun.inti.components.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class LogUtils {
    public static String toString(Throwable t) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        t.printStackTrace(new PrintStream(baos));
        return baos.toString();
    }
}
