package com.sun.inti.components.service;

public class ServiceLocationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ServiceLocationException(String message) {
        super(message);
    }

    public ServiceLocationException(Throwable cause) {
        super(cause);
    }

    public ServiceLocationException(String message, Throwable cause) {
        super(message, cause);
    }
}
