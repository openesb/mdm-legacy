/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.record;

import java.util.Iterator;

public class DefaultRecordSource implements RecordSource {
	private Iterator<String> stringIterator;
	private RecordPopulator[] recordPopulators;
	
	public DefaultRecordSource() {}
	
	public DefaultRecordSource(RecordPopulator[] recordDescriptors, Iterator<String> stringIterator) {
		this.recordPopulators = recordDescriptors;
		this.stringIterator = stringIterator;
	}
	
	public Record nextRecord() {
		if (this.stringIterator.hasNext()) {
			Record record = new Record();
			for (RecordPopulator recordPopulator: this.recordPopulators) {
				String recordString = null;
				if (this.stringIterator.hasNext()) {
					recordString = this.stringIterator.next();
					if (recordString == null) {
						throw new IllegalStateException("Missing physical record(s)");
					}
				}
				if (recordPopulator != null) {
					recordPopulator.populateRecord(record, recordString);
				}
			}
			return record;
		}
		return null;
	}

	public Iterator<String> getStringIterator() {
		return stringIterator;
	}

	public void setStringIterator(Iterator<String> stringIterator) {
		this.stringIterator = stringIterator;
	}

	public RecordPopulator[] getRecordPopulators() {
		return recordPopulators;
	}

	public void setRecordPopulators(RecordPopulator[] recordPopulators) {
		this.recordPopulators = recordPopulators;
	}
}