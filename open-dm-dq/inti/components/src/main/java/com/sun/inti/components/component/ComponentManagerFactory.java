package com.sun.inti.components.component;

import java.util.Map;

public interface ComponentManagerFactory {
    public ComponentManager newInstance(ClassLoader classLoader) throws ComponentException;
    public ComponentManager newInstance(ClassLoader classLoader, Map<String, Object> parameters) throws ComponentException;
}
