package com.sun.inti.components.string.format;

import java.util.HashMap;
import java.util.Map;

import com.sun.inti.components.util.Abbreviated;

public class EnumerationFormatter extends AbstractFormatter {
    private Class<? extends Enum<?>> enumType;
    private Map<String, Enum<?>> enumConstants = new HashMap<String, Enum<?>>();
	private Map<Enum<?>, String> enumSymbols = new HashMap<Enum<?>, String>();
	
	public EnumerationFormatter(Class<? extends Enum<?>> enumType) {
		this(enumType, false);
	}
	
	public EnumerationFormatter(Class<? extends Enum<?>> enumType, boolean useAbbreviation) {
        this.enumType = enumType;
        boolean isAbbreviated = false;
		if (useAbbreviation) {
			isAbbreviated = Abbreviated.class.isAssignableFrom(enumType);
			if (!isAbbreviated) {
				throw new IllegalArgumentException("Class " + enumType.getName() + " is not abbreviated");
			}
		}
		
		for (Enum<?> e: enumType.getEnumConstants()) {
			final String symbol;
			if (isAbbreviated) {
				symbol = ((Abbreviated) e).getAbbreviation();
			} else {
				symbol = e.toString();
			}
			
			this.enumConstants.put(symbol, e);
			this.enumSymbols.put(e, symbol);
		}
	}

	public Object parse(String string) {
        Object object = this.enumConstants.get(string);
        if (object == null) {
            throw new IllegalArgumentException("Unknown symbol: " + string + " (" + this.enumType.getName() + ")");
        }
        return object;
	}

	public String format(Object object) {
		String string = this.enumSymbols.get(object);
		if (string == null) {
            throw new IllegalArgumentException("Unknown constant: " + object + " (" + this.enumType.getName() + ")");
		}
		return string;
	}
}

