package com.sun.inti.components.classloading;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.logging.Logger;

public class JarCacheClassLoader extends ClassLoader {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(JarCacheClassLoader.class.getName());

	private final JarCache jarCache;

	public JarCacheClassLoader(final JarCache jarCache, final ClassLoader parent) {
		super(parent);
		this.jarCache = jarCache;
	}

	@Override
	public Class<?> findClass(final String name) throws ClassNotFoundException {
		try {
			return super.findClass(name);
		} catch (final ClassNotFoundException cnfe) {
			final InputStream is = this.getResourceAsStream(name.replace('.', '/') + ".class");
			if (is == null) {
				throw new ClassNotFoundException(name);
			}

			int count;
			final byte[] buf = new byte[8192];
			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				while ((count = is.read(buf, 0, buf.length)) > 0) {
					bos.write(buf, 0, count);
				}
			} catch (final IOException ioe) {
				throw new RuntimeException(ioe);
			}
			final byte[] classBytes = bos.toByteArray();
			return this.defineClass(name, classBytes, 0, classBytes.length);
		}
	}

	@Override
	public InputStream getResourceAsStream(final String fileName) {
		final InputStream is = super.getResourceAsStream(fileName);
		if (is != null) {
			return is;
		}
		return this.jarCache.getResourceAsStream(fileName);
	}

	@Override
	public URL findResource(final String fileName) {
		URL url = this.jarCache.findResource(fileName);
		if (url == null) {
			url = super.findResource(fileName);
		}
		return url;
	}

	@Override
	public Enumeration<URL> findResources(final String fileName) throws IOException {
		final Enumeration<URL> e = this.jarCache.findResources(fileName).elements();
		if (e.hasMoreElements()) {
			return e;
		}
		return super.findResources(fileName);
	}

	public JarCache getJarCache() {
		return this.jarCache;
	}
}
