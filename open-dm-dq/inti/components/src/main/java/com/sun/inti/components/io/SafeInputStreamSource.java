package com.sun.inti.components.io;

import java.io.IOException;
import java.io.InputStream;

public class SafeInputStreamSource implements InputStreamSource {
    private InputStreamSource inputStreamSource;
    private InputStream previousInputStream = null;
    
    public SafeInputStreamSource() {}
    public SafeInputStreamSource(InputStreamSource inputStreamSource) {
        this.inputStreamSource = inputStreamSource;
    }
    
    public InputStream getInputStream()  throws IOException {
        if (previousInputStream != null) {
            try { previousInputStream.close(); } catch (IOException ieo) {};
        }
        previousInputStream = inputStreamSource.getInputStream();
        return previousInputStream;
    }
    
    public InputStreamSource getInputStreamSource() {
        return inputStreamSource;
    }
    public void setInputStreamSource(InputStreamSource inputStreamSource) {
        this.inputStreamSource = inputStreamSource;
    }
}
