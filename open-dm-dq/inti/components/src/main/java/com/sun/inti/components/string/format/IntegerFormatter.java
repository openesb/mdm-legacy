package com.sun.inti.components.string.format;

public class IntegerFormatter extends AbstractFormatter {
	public Integer parse(String string) {
		if (string == null) {
			return null;
		}
		string = string.trim();
		if (string.length() == 0) {
			return 0;
		}
		
		return Integer.parseInt(string);
	}
}
