package com.sun.inti.components.string.match.incremental;

import java.util.Iterator;


public class FixedStringIncrementalMatcher implements IncrementalMatcher {
	private String[] strings;
	
	public FixedStringIncrementalMatcher() {}
	
	public FixedStringIncrementalMatcher(String[] strings) {
        this.strings = strings;
	}

	public int match(Iterator<String> tokens) {
	    if (tokens.hasNext()) {
	        String token = tokens.next();
	        for (String string: strings) {
	            if (string.equals(token)) {
	                return 1;
	            }
	        }
	    }

		return 0;
	}

}
