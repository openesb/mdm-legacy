package com.sun.inti.components.component;


public abstract class AbstractComponentManagerFactory implements ComponentManagerFactory {
    public ComponentManager newInstance(ClassLoader classLoader) throws ComponentException {
        return newInstance(classLoader, null);
    }
}
