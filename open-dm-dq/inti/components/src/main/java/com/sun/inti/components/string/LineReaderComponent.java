package com.sun.inti.components.string;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.logging.Logger;

import com.sun.inti.components.string.transform.StringTransformer;

public abstract class LineReaderComponent {
	private static Logger logger = Logger.getLogger(LineReaderComponent.class.getName());
	
    private static final String DEFAULT_SEPARATOR = "\\s+";
    
    private Reader reader;
    private StringTransformer transformer;
    private String separator;
    
    protected LineReaderComponent() {}
    
    public LineReaderComponent(String resourceName) throws IOException {
        set(fromResource(resourceName), null, DEFAULT_SEPARATOR);
    }
    public LineReaderComponent(String resourceName, StringTransformer stringTransformer, String separator) throws IOException {
        set(fromResource(resourceName), stringTransformer, separator);
    }
    
    public LineReaderComponent(File file) throws IOException {
        set(new FileReader(file), null, DEFAULT_SEPARATOR);
    }
    public LineReaderComponent(File file, StringTransformer stringTransformer, String separator) throws IOException {
        set(new FileReader(file), stringTransformer, separator);
    }
    
    public LineReaderComponent(URL url) throws IOException {
        set(new InputStreamReader(url.openStream()), null, DEFAULT_SEPARATOR);
    }
    public LineReaderComponent(URL url, StringTransformer stringTransformer, String separator) throws IOException {
        set(new InputStreamReader(url.openStream()), stringTransformer, separator);
    }
    
    public LineReaderComponent(InputStream is) throws IOException {
        set(new InputStreamReader(is), null, DEFAULT_SEPARATOR);
    }
    public LineReaderComponent(InputStream is, StringTransformer stringTransformer, String separator) throws IOException {
        set(new InputStreamReader(is), stringTransformer, separator);
    }
    
    public LineReaderComponent(Reader reader) throws IOException {
        set(reader, null, DEFAULT_SEPARATOR);
    }
    public LineReaderComponent(Reader reader, StringTransformer stringTransformer, String separator) throws IOException {
        set(reader, stringTransformer, separator);
    }

    protected void load() {
    	logger.fine("LineReaderComponent loading: " + getClass().getName());
    	
        if (separator == null) {
            separator = DEFAULT_SEPARATOR;
        }
        
        BufferedReader in = null;
        try {
            String line;
            in = new BufferedReader(reader);
            while ((line = in.readLine()) != null) {
                loadLine(line);
            }
        } catch (IOException ioe) {
        	
        } finally {
            if (in != null) {
            	try { in.close(); } catch (Exception e) {}
            }
        }
    }

    private void loadLine(String line) {
        line = line.trim();
        int pos = line.indexOf('#');
        if (pos > 0) {
            line = line.substring(0, pos);
        }
        if (line.length() > 0) {
            if (transformer != null) {
                line = transformer.transform(line);
            }
            String[] words = line.split(separator);
            addWords(words);
        }
    }

    protected abstract void addWords(String[] words);

    private void set(Reader reader, StringTransformer transformer, String separator) {
    	this.reader = reader;
    	this.transformer = transformer;
    	this.separator = separator;
    }
    private Reader fromResource(String resourceName) throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(resourceName);
        if (is == null) {
            throw new FileNotFoundException("Can't locate resource: " + resourceName);
        }
        return new InputStreamReader(is);
    }

	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	public StringTransformer getTransformer() {
		return transformer;
	}

	public void setTransformer(StringTransformer transformer) {
		this.transformer = transformer;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
