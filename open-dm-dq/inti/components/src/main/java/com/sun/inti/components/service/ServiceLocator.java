package com.sun.inti.components.service;

public interface ServiceLocator {
    public Object locateService(Class<?> serviceClass) throws ServiceLocationException;
    public Object locateService(Class<?> serviceClass, ClassLoader classLoader) throws ServiceLocationException;
}
