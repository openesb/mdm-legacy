/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

/**
 * 
 */
package com.sun.inti.components.proxy;

import java.util.List;

import com.sun.inti.components.record.Record;

public class MethodDescriptor {
    private String name;
    List<ArgumentDescriptor> argumentDescriptors;
    private Executor executor;

    public MethodDescriptor() {
    }

    public MethodDescriptor(final String name, final List<ArgumentDescriptor> argumentDescriptors, final Executor executor) {
        this.name = name;
        this.argumentDescriptors = argumentDescriptors;
        this.executor = executor;
    }

    public Object execute(final Object[] args, final Record record) {
        return this.executor.execute(args, record);
    }

    public List<ArgumentDescriptor> getArgumentDescriptors() {
        return this.argumentDescriptors;
    }

    public void setArgumentDescriptors(final List<ArgumentDescriptor> argumentDescriptors) {
        this.argumentDescriptors = argumentDescriptors;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Executor getExecutor() {
        return this.executor;
    }

    public void setExecutor(final Executor executor) {
        this.executor = executor;
    }
}