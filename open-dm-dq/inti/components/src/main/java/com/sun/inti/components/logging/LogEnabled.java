package com.sun.inti.components.logging;

import java.util.logging.Level;

public interface LogEnabled {
	public void setLoggingLevel(Level level);
}
