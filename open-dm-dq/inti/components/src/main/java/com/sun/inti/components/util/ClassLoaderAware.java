package com.sun.inti.components.util;

public interface ClassLoaderAware {
    public void setClassLoader(ClassLoader classLoader);
}
