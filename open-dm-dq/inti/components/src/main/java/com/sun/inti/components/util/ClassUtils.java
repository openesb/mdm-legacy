package com.sun.inti.components.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class ClassUtils {
    @SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(ClassUtils.class.getName());

	public static String getClassPrefix(Class<?> clazz) {
	    return clazz.getPackage().getName().replace('.', '/') + "/";
	}

    public static <S> S loadDefaultService(Class<S> serviceClass) throws IOException {
        return loadDefaultService(serviceClass, serviceClass.getClassLoader());
    }

    public static <S> S loadDefaultService(Class<S> serviceClass, ClassLoader classLoader) throws IOException {
        Iterator<S> iterator = loadServiceClass(serviceClass, classLoader);
        if (!iterator.hasNext()) {
            throw new IllegalStateException("No suitable implementation for service " + serviceClass.getName());
        }
        return iterator.next();
    }
    
    public static <S> Iterator<S> loadServiceClass(Class<S> serviceClass) throws IOException {
        return loadServiceClass(serviceClass, serviceClass.getClassLoader());
    }
    
    // FIXME This method must be replaced by java.util.ServiceLoader.load()
    private static HashMap<String, ServiceLoader<?>> serviceLoaders = new HashMap<String, ServiceLoader<?>>();
    public static <S> Iterator<S> loadServiceClass(Class<S> serviceClass, ClassLoader classLoader) throws IOException {
        synchronized (serviceLoaders) {
            @SuppressWarnings("unchecked")
            ServiceLoader<S> serviceLoader = (ServiceLoader<S>) serviceLoaders.get(serviceClass.getName());
            if (serviceLoader == null) {
                serviceLoader = new ServiceLoader<S>(serviceClass, classLoader);
                serviceLoaders.put(serviceClass.getName(), serviceLoader);
            }
            return serviceLoader.iterator();
        }
    }

	private static class ServiceLoader<S> {
	    private static final Logger logger = Logger.getLogger(ServiceLoader.class.getName());

        private ClassLoader classLoader;

        private List<S> instanceCache;
        private List<String> instanceName;
	    
	    @SuppressWarnings("unchecked")
        private ServiceLoader(Class<S> serviceClass, ClassLoader classLoader) throws IOException {
	        this.classLoader = classLoader;
	        
	        logger.finer("About to retrieve resource URLs");
	        List<URL> urls = new ArrayList<URL>();
	        Enumeration<URL> e = classLoader.getResources("META-INF/services/" + serviceClass.getName());
	        while (e.hasMoreElements()) {
	            URL url = e.nextElement();
	            logger.info("Adding URL: " + url);
	            urls.add(url);
	        }

            instanceName = new ArrayList<String>();
            for (URL url: urls) {
                InputStream is = null;
                BufferedReader in = null;
                try {
                    String line;
                    is = url.openStream();
                    in = new BufferedReader(new InputStreamReader(is));
                    while ((line = in.readLine()) != null) {
                        line = line.replaceAll("#.*", "").trim();
                        if (line.length() > 0) {
                            instanceName.add(line);
                        }
                    }
                } finally {
                    if (is != null) {
                        logger.info("Closing input stream: " + url);
                        is.close();
                    }
                }
            }
            instanceCache = new ArrayList<S>(instanceName.size());

            logger.finer("Done initializing service loader");
	    }

	    public Iterator<S> iterator() {
	        return new Iterator<S>() {
	            private int index = 0;
	            
                @SuppressWarnings("unchecked")
                public boolean hasNext() {
                    if (index >= instanceName.size()) {
                        return false;
                    }

				    synchronized(instanceCache) {
						if (instanceCache.size() < instanceName.size()) {
							String className = instanceName.get(index);
							try {
                                Class<S> serviceClass = (Class<S>) classLoader.loadClass(className);
                                instanceCache.add(serviceClass.newInstance());
							} catch (Exception e) {
								logger.warning("Error loading class '" + className + "': " + e);
								throw new RuntimeException("Error loading class '" + className + "': " + e, e);
							}
                        }
                    }

                    return true;
                }

                public S next() {
                    return instanceCache.get(index++);
                }

                public void remove() {
                    throw new UnsupportedOperationException("Cannot remove service instance");
                }
	        };
	    }
	}
}
