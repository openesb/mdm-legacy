package com.sun.inti.components.url;

import java.io.IOException;
import java.net.URL;

public class SimpleURLSource implements URLSource {
    private URL url;
    
    public SimpleURLSource() {}
    
    public SimpleURLSource(URL url) {
        this.url = url;
    }

    public URL getURL() throws IOException {
        return url;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
}
