/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.proxy;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import com.sun.inti.components.record.Record;

public class ScriptExecutor implements Executor {
    String languageName;
    private String scriptText;
    String[] argumentNames;
    private String contextName;

    private ScriptEngine scriptEngine;

    public ScriptExecutor() {
    }

    public ScriptExecutor(final String languageName, final String scriptText, final String[] argumentNames, final String contextName) {
        this.scriptEngine = new ScriptEngineManager().getEngineByName(languageName);
        this.languageName = languageName;
        this.scriptText = scriptText;
        this.argumentNames = argumentNames;
        this.contextName = contextName;
    }

    private static final Object[] emptyArguments = new Object[0];

    public Object execute(Object[] arguments, final Record record) {
        if (arguments == null) {
            arguments = emptyArguments;
        }
        if (this.argumentNames.length != arguments.length) {
            throw new IllegalArgumentException("Script executor argument count mismatch, expected " + this.argumentNames.length + " got " + arguments.length);
        }

        final Bindings bindings = this.scriptEngine.createBindings();

        for (int i = 0; i < this.argumentNames.length; i++) {
            bindings.put(this.argumentNames[i], arguments[i]);
        }

        if (this.contextName != null && record != null) {
            bindings.put(this.contextName, record);
        }

        this.scriptEngine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);

        try {
            return this.scriptEngine.eval(this.scriptText);
        } catch (final Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public String[] getArgumentNames() {
        return this.argumentNames;
    }

    public void setArgumentNames(final String[] argumentNames) {
        this.argumentNames = argumentNames;
    }

    public String getContextName() {
        return this.contextName;
    }

    public void setContextName(final String contextName) {
        this.contextName = contextName;
    }

    public String getLanguageName() {
        return this.languageName;
    }

    public void setLanguageName(final String languageName) {
        this.languageName = languageName;
    }

    public String getScriptText() {
        return this.scriptText;
    }

    public void setScriptText(final String scriptText) {
        this.scriptText = scriptText;
    }
}
