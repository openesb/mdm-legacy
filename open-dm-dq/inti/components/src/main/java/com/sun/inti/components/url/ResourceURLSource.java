package com.sun.inti.components.url;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

import com.sun.inti.components.util.ClassLoaderAware;

public class ResourceURLSource implements URLSource, ClassLoaderAware {
    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(ResourceURLSource.class.getName());

    private String resourceName;
    private ClassLoader classLoader = getClass().getClassLoader();
    
    public ResourceURLSource() {}
    
    public ResourceURLSource(String resourceName) {
        this.resourceName = resourceName;
    }
    
    public ResourceURLSource(String resourceName, ClassLoader classLoader) {
        this.resourceName = resourceName;
        this.classLoader = classLoader;
    }

    public URL getURL() throws IOException {
        URL url = classLoader.getResource(resourceName);
        if (url == null) {
            throw new IllegalArgumentException("Can't find resource: " + resourceName);
        }
        return classLoader.getResource(resourceName);
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
