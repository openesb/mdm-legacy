package com.sun.inti.components.string.concatenate;

import java.util.List;

public class SeparatorBasedStringConcatenator implements StringConcatenator {
	private String separator = " ";

	public SeparatorBasedStringConcatenator() {}
	
	public SeparatorBasedStringConcatenator(String separator) {
		this.separator = separator;
	}
	
	public String concatenate(List<String> parts) {
		StringBuilder sb = new StringBuilder();
		for (String part: parts) {
			sb.append(part);
            sb.append(separator);
		}
		if (sb.length() > separator.length()) {
		    sb.setLength(sb.length() - separator.length());
		}
		return sb.toString();
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
