package com.sun.inti.components.string.format;

public interface Formatter {
	public String format(Object object);
	public Object parse(String string);
}
