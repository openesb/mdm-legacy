package com.sun.inti.components.component;

public interface ComponentManager {
    public Object getComponent(String componentName) throws ComponentException;
}
