package com.sun.inti.components.string.tokenize;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class RegexTokenizer implements Tokenizer {
	private Pattern pattern;
	
	public RegexTokenizer() {}
	
	public RegexTokenizer(Pattern pattern) {
		this.pattern = pattern;
	}
	
	public RegexTokenizer(String regex) {
		this(Pattern.compile(regex));
	}
	
	public List<String> tokenize(String string) {
	    if (string == null) {
	        return null;
	    }
		return Arrays.asList(this.pattern.split(string));
	}
}
