package com.sun.inti.components.util;

public interface Abbreviated {
	public String getAbbreviation();
}
