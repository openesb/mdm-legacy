package com.sun.inti.components.string.tokenize;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FixedFieldTokenizer implements Tokenizer {
	private FieldDescriptor[] fieldDescriptors;
	
	public static class FieldDescriptor {
		private int offset;
		private int length;
		private boolean trim = true;
		
		public FieldDescriptor(int offset, int length, boolean trim) {
			this.offset = offset;
			this.length = length;
			this.trim = trim;
		}
		
		public void setOffset(int offset) {
			this.offset = offset;
		}
		public void setLength(int length) {
			this.length = length;
		}
		public void setTrim(boolean trim) {
			this.trim = trim;
		}
	}
	
	public List<String> tokenize(String string) {
		List<String> fields = new ArrayList<String>(this.fieldDescriptors.length);
		for (int i = 0; i < this.fieldDescriptors.length; i++) {
			if (fieldDescriptors[i].offset >= string.length()) {
				break;
			}
			
			int endPos = fieldDescriptors[i].offset + fieldDescriptors[i].length;
			if (endPos > string.length()) {
				endPos = string.length();
			}
			
			String field = string.substring(fieldDescriptors[i].offset, endPos);
            if (fieldDescriptors[i].trim) {
                field = field.trim();
            }

            fields.add(field);
		}
		return fields;
	}

	public void setFieldDescriptors(FieldDescriptor[] fieldDescriptors) {
		this.fieldDescriptors = fieldDescriptors;
		Arrays.sort(fieldDescriptors, new Comparator<FieldDescriptor>() {
			public int compare(FieldDescriptor left, FieldDescriptor right) {
				return left.offset - right.offset;
			}
		});
	}
}
