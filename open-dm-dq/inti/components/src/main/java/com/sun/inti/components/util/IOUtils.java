package com.sun.inti.components.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class IOUtils {
    private static final Logger logger = Logger.getLogger(IOUtils.class.getName());
    
    public static FileFilter getDirectoryFilter(String... exclusionPatterns) {
    	final Pattern[] patterns = new Pattern[exclusionPatterns.length];
    	for (int i = 0; i < patterns.length; i++) {
    		patterns[i] = Pattern.compile(exclusionPatterns[i]);
    	}
    	return new FileFilter() {
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    for (Pattern pattern: patterns) {
                        if (pattern.matcher(file.getName()).find()) {
                            return false;
                        }
                    }
                    return true;
                }
                return false;
            }
    	};
    }
    
    public static void recursiveDelete(File file) {
    	doRecursiveDelete(file);
    }
    
    private static void doRecursiveDelete(File file) {
        if (file.isDirectory()) {
            for (File childFile: file.listFiles()) {
            	doRecursiveDelete(childFile);
            }
        }
        if (!file.delete()) {
            logger.warning("Can't delete: " + file.getAbsolutePath());
        }
    }

    public static File[] recursiveListFiles(File directory) {
        return recursiveListFiles(directory, null);
    }
    public static File[] recursiveListFiles(File directory, FileFilter fileFilter) {
        List<File> acceptedFiles = new ArrayList<File>();
        recursiveListFiles(directory, fileFilter, acceptedFiles);
        Collections.sort(acceptedFiles);
        return acceptedFiles.toArray(new File[acceptedFiles.size()]);
    }
    private static void recursiveListFiles(File file, FileFilter fileFilter, List<File> acceptedFiles) {
        if (file.isDirectory()) {
            for (File childFile: file.listFiles()) {
                recursiveListFiles(childFile, fileFilter, acceptedFiles);
            }
        } else {
            if (fileFilter == null || fileFilter.accept(file)) {
                acceptedFiles.add(file);
            }
        }
    }
    
    public static void copyDirectory(File inputDirectory, File outputDirectory) throws IOException {
        File[] inputFiles = recursiveListFiles(inputDirectory, new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        for (File inputFile: inputFiles) {
            copy(inputFile, new File(outputDirectory, inputFile.getPath()));
        }
    }
    
    public static void copy(File input, File output) throws IOException {
        copy(new FileInputStream(input), new FileOutputStream(output));
    }

    private static final int BUFFER_SIZE = 4096;
    
    public static void copy(InputStream is, OutputStream os) throws IOException {
        copy(is, os, true);
    }
    
    public static void copy(InputStream is, OutputStream os, boolean close) throws IOException {
        int count;
        byte[] buffer = new byte[BUFFER_SIZE];
        while ((count = is.read(buffer)) != -1) {
            os.write(buffer, 0, count);
        }
        os.flush();
        if (close) {
            is.close();
            os.close();
        }
    }

    public static void extract(ZipFile zipFile, File directory) throws IOException {
        Enumeration<?extends ZipEntry> i = zipFile.entries();
        while (i.hasMoreElements()) {
            ZipEntry zipEntry = i.nextElement();
            if (!zipEntry.isDirectory()) {
                String entryName = zipEntry.getName();
                File file = new File(directory, normalizeZipSeparator(entryName));
                file.getParentFile().mkdirs();
                InputStream is = zipFile.getInputStream(zipEntry);
                FileOutputStream os = new FileOutputStream(file);
                copy(is, os);
            }
        }
    }
    
    public static void extract(InputStream is, File directory) throws IOException {
        ZipInputStream zis = new ZipInputStream(is);
        ZipEntry zipEntry;
        while ((zipEntry = zis.getNextEntry()) != null) {
            String entryName = normalizeZipSeparator(zipEntry.getName());
            if (zipEntry.isDirectory()) {
                new File(directory, entryName).mkdirs();
            } else {
                File file = new File(directory, entryName);
                file.getParentFile().mkdirs();
                OutputStream os = new FileOutputStream(file);
                copy(zis, os, false);
                os.close();
            }
        }
        zis.close();
    }
    
    // FIXME This is a workaround for a ZipFile bug under Windows
    public static String normalizeZipSeparator(String name) {
        return name.replace('\\', '/');
    }


	public static String basename(String filename) {
	    filename = filename.replace(File.separatorChar, '/');
	    int separatorPosition = filename.lastIndexOf('/');
	    int extensionPosition = filename.lastIndexOf('.');
	    if (extensionPosition < 0) {
	        return filename.substring(separatorPosition + 1);
	    }
	    return filename.substring(separatorPosition + 1, extensionPosition);
	}
}
