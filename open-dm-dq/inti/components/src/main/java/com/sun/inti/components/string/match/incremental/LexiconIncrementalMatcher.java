package com.sun.inti.components.string.match.incremental;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.sun.inti.components.string.LineReaderComponent;
import com.sun.inti.components.string.transform.StringTransformer;

// TODO Configure non-greedy mode that returns matched prefixes
public class LexiconIncrementalMatcher extends LineReaderComponent implements IncrementalMatcher {
	private static Logger logger = Logger.getLogger(LexiconIncrementalMatcher.class.getName());

	private Lexicon lexicon;

	private class Lexicon {
		private Map<String, Lexicon> terms;

		private Lexicon() {}

		private void add(String[] words) {
			Lexicon lexicon = this;
			for (int i = 0; i < words.length; i++) {
				if (lexicon.terms == null) {
					lexicon.terms = new LinkedHashMap<String, Lexicon>();
				}
				if (!lexicon.terms.containsKey(words[i])) {
					lexicon.terms.put(words[i], new Lexicon());
				}
				lexicon = lexicon.terms.get(words[i]);
			}

			if (lexicon.terms == null) {
				lexicon.terms = new LinkedHashMap<String, Lexicon>();
			}
			lexicon.terms.put(null, null);
		}

		private int match(Iterator<String> words) {
			int length = 0;
			int matchLength = 0;
			Lexicon lexicon = this;
			while (words.hasNext()) {
			    String word = words.next();
			    
				if (lexicon.terms.containsKey(word)) {
					length++;
					lexicon = lexicon.terms.get((word));
					if (lexicon.terms.containsKey(null)) {
						matchLength = length;
					}
				} else {
				    break;
				}
			}
			return matchLength;
		}

		public String toString() {
			if (terms == null) {
				return null;
			}
			return terms.toString();
		}
	}

    public int match(Iterator<String> words) {
        if (lexicon == null) {
            lexicon = new Lexicon();
            logger.finest("match: About to load lexicon");
            load();
            logger.finest("match: Done loading lexicon");
        }
        return lexicon.match(words);
    }

    protected void addWords(String[] words) {
        if (words.length > 0) {
            lexicon.add(words);
        }
    }

	public LexiconIncrementalMatcher(String[] lines) throws IOException {
	    lexicon = new Lexicon();
		for (String line : lines) {
			addWords(line.split("\\s+"));
		}
	}

    public LexiconIncrementalMatcher(String resourceName) throws IOException { super(resourceName); }
    public LexiconIncrementalMatcher(String resourceName, StringTransformer stringTransformer, String separator) throws IOException {
        super(resourceName, stringTransformer, separator);
    }
    
    public LexiconIncrementalMatcher(File file) throws IOException { super(file); }
    public LexiconIncrementalMatcher(File file, StringTransformer stringTransformer, String separator) throws IOException {
        super(file, stringTransformer, separator);
    }
    
    public LexiconIncrementalMatcher(URL url) throws IOException { super(url); }
    public LexiconIncrementalMatcher(URL url, StringTransformer stringTransformer, String separator) throws IOException {
        super(url, stringTransformer, separator);
    }
    
    public LexiconIncrementalMatcher(InputStream is) throws IOException { super(is); }
    public LexiconIncrementalMatcher(InputStream is, StringTransformer stringTransformer, String separator) throws IOException {
        super(is, stringTransformer, separator);
    }
    
    public LexiconIncrementalMatcher(Reader reader) throws IOException { super(reader); }
    public LexiconIncrementalMatcher(Reader reader, StringTransformer stringTransformer, String separator) throws IOException {
        super(reader, stringTransformer, separator);
    }
}
