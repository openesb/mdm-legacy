package com.sun.inti.components.string.format;

import java.util.ArrayList;
import java.util.List;

import com.sun.inti.components.string.tokenize.RegexTokenizer;
import com.sun.inti.components.string.tokenize.Tokenizer;

public class EnumerationListFormatter extends EnumerationFormatter {
	private Tokenizer tokenizer = new RegexTokenizer("\\s+");
	
	public EnumerationListFormatter(Class<? extends Enum<?>> enumType) {
		super(enumType);
	}
	
	public EnumerationListFormatter(Class<? extends Enum<?>> enumType, boolean useAbbreviation) {
		super(enumType, useAbbreviation);
	}

	@Override
	public String format(Object object) {
		List<?> list = (List<?>) object;
		
		StringBuilder builder = new StringBuilder();
		for (Object o: list) {
			builder.append(super.format(o));
			builder.append(' ');
		}
		builder.setLength(builder.length() - 1);
		
		return builder.toString();
	}

	@Override
	public Object parse(String string) {
		List<Object> list = new ArrayList<Object>();
		
		for (String constantString: this.tokenizer.tokenize(string.trim())) {
			list.add(super.parse(constantString));
		}
		
		return list;
	}
}
