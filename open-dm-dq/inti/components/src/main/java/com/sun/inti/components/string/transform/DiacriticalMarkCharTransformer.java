package com.sun.inti.components.string.transform;

public class DiacriticalMarkCharTransformer implements CharTransformer {

    public char transform(final char c) {

        if (c < 192) {
            return c;
        }
        if (c >= 192 && c <= 197) {
            return 'A';
        }
        if (c == 199) {
            return 'C';
        }
        if (c >= 200 && c <= 203) {
            return 'E';
        }
        if (c >= 204 && c <= 207) {
            return 'I';
        }
        if (c == 208) {
            return 'D';
        }
        if (c == 209) {
            return 'N';
        }
        if (c >= 210 && c <= 214 || c == 216) {
            return 'O';
        }
        if (c >= 217 && c <= 220) {
            return 'U';
        }
        if (c == 221) {
            return 'Y';
        }
        if (c >= 224 && c <= 229) {
            return 'a';
        }
        if (c == 231) {
            return 'c';
        }
        if (c >= 232 && c <= 235) {
            return 'e';
        }
        if (c >= 236 && c <= 239) {
            return 'i';
        }
        if (c == 240) {
            return 'd';
        }
        if (c == 241) {
            return 'n';
        }
        if (c >= 242 && c <= 246 || c == 248) {
            return 'o';
        }
        if (c >= 249 && c <= 252) {
            return 'u';
        }
        if (c == 253 || c == 255) {
            return 'y';
        }

        return c;
    }

}
