package com.sun.inti.components.classloading;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/* TODO Test with
 * 
 *       permission java.net.NetPermission "specifyStreamHandler";
 *       
 * in $JAVA_HOME/jre/lib/java.policy
 */ 

public class JarCacheClassLoaderFactory implements FileClassLoaderFactory {
	public ClassLoader newClassLoader(File[] files, ClassLoader parentClassLoader) throws ClassLoadingException {
		try {
            List<File> directories = null;
            JarCache jarCache = new JarCache();
            for (File file: files) {
            	if (file.isDirectory()) {
            		if (directories == null) {
            			directories = new ArrayList<File>();
            		}
            		directories.add(file);
            	} else {
                    jarCache.addJar(file.getAbsolutePath());
            	}
            }
            
            if (directories != null) {
            	URL[] directoryUrls = new URL[directories.size()];
            	for (int i = 0; i < directories.size(); i++) {
            		directoryUrls[i] = directories.get(i).toURI().toURL();
            	}
                URLClassLoader delegateClassLoader = new URLClassLoader(directoryUrls, parentClassLoader);
                parentClassLoader = delegateClassLoader;
            }
            
            JarCacheClassLoader classLoader = new JarCacheClassLoader(jarCache, parentClassLoader);
            return classLoader;
        } catch (Exception e) {
            throw new ClassLoadingException("Error creating new class loader: " + e, e);
        }
	}

	public void close(ClassLoader classLoader) {
		if (classLoader instanceof JarCacheClassLoader) {
			((JarCacheClassLoader) classLoader).getJarCache().close();
		}
	}
}
