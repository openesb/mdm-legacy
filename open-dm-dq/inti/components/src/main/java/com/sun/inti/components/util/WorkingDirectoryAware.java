package com.sun.inti.components.util;

import java.io.File;

public interface WorkingDirectoryAware {
	public void setWorkingDirectory(File workingDirectory);
}
