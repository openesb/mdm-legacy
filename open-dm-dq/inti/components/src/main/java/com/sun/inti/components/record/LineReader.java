/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.record;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

public class LineReader implements Iterator<String> {
	private final BufferedReader in;
	private final boolean skipEmptyRecords;

	private String record;
	
	public LineReader(Reader reader, boolean skipEmptyRecords) {
		this.in = new BufferedReader(reader);
		this.skipEmptyRecords = skipEmptyRecords;
		
		this.record = nextLine();
	}
	
	public LineReader(Reader reader) {
		this(reader, false);
	}

	private String nextLine() {
		String line;
		
		do {
			line = this.getNextLine();
		} while (line != null && this.skipEmptyRecords && line.trim().length() == 0);
		
		return line;
	}
	
	private String getNextLine() {
		String line = null;
		try {
			line = in.readLine();
			return line;
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if (line == null) {
				try {
					in.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	public boolean hasNext() {
		return record != null;
	}

	public String next() {
		String returnValue = this.record;
		this.record = nextLine();
		return returnValue;
	}

	public void remove() {
		throw new UnsupportedOperationException("remove() unsupported for string iterator");
	}
}