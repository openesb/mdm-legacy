package com.sun.inti.components.string.tokenize;

import java.util.ArrayList;
import java.util.List;

public class FixedLengthTokenizer implements Tokenizer {
	private int length;
	
	public FixedLengthTokenizer() {}
	
	public FixedLengthTokenizer(int length) {
		this.length = length;
	}
	
	public List<String> tokenize(String string) {
		if (string == null) {
			return null;
		}
		int chunkCount = string.length() / this.length;
		if (string.length() % this.length != 0) {
			chunkCount++;
		}
		List<String> result = new ArrayList<String>(chunkCount);
		for (int i = 0; i < chunkCount; i++) {
			int endPosition = this.length * (i + 1);
			if (endPosition > string.length()) {
				endPosition = string.length();
			}
			result.add(string.substring(i * this.length, endPosition));
		}
		return result;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}
