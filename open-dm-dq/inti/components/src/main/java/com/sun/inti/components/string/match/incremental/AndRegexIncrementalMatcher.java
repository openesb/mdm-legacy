package com.sun.inti.components.string.match.incremental;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class AndRegexIncrementalMatcher extends AbstractRegexIncrementalMatcher {
	public AndRegexIncrementalMatcher() {}
	public AndRegexIncrementalMatcher(List<RegexMatcher> matchers) { super(matchers); }
	
	public int match(Iterator<String> tokens) {
	    int count;
		for (count = 0; count < regexMatchers.size() && tokens.hasNext(); count++) {
		    String token = tokens.next();
		    
			if (!regexMatchers.get(count).pattern.matcher(token).matches()) {
				return 0;
			}

			if (regexMatchers.get(count).exceptions != null) {
			    for (Pattern exceptionPattern: regexMatchers.get(count).exceptions) {
			        if (exceptionPattern.matcher(token).matches()) {
			            return 0;
			        }
			    }
			}
		}
		if (count == regexMatchers.size()) {
		    return count;
		}
		return 0;
	}
}
