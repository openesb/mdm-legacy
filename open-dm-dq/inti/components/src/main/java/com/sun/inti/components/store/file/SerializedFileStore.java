package com.sun.inti.components.store.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializedFileStore extends FileStore {
    public SerializedFileStore() {
        super();
    }

    public SerializedFileStore(File repositoryDirectory) {
        super(repositoryDirectory);
    }

    @Override
    protected Object get(File file) throws Exception {
        if (file.length() == 0L) {
            return null;
        }
        final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        @SuppressWarnings("unchecked")
        final Object object = ois.readObject();
        ois.close();
        return object;
    }

    @Override
    protected void put(File file, Object object) throws Exception {
        final FileOutputStream ofs = new FileOutputStream(file);
        if (object == null) {
            ofs.close();
        } else {
            final ObjectOutputStream oos = new ObjectOutputStream(ofs);
            oos.writeObject(object);
            oos.flush();
            oos.close();
        }
    }
}
