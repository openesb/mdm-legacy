package com.sun.inti.components.string.match.incremental;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class OrRegexIncrementalMatcher extends AbstractRegexIncrementalMatcher {
    public OrRegexIncrementalMatcher() {}
    public OrRegexIncrementalMatcher(List<RegexMatcher> matchers) { super(matchers); }
    
    public int match(Iterator<String> tokens) {
        if (tokens.hasNext()) {
            String token = tokens.next();
            
            for (RegexMatcher regexMatcher: regexMatchers) {
                if (regexMatcher.pattern.matcher(token).matches()) {
                    boolean match = true;
                    if (regexMatcher.exceptions != null) {
                        for (Pattern exceptionPattern: regexMatcher.exceptions) {
                            if (exceptionPattern.matcher(token).matches()) {
                                match = false;
                                break;
                            }
                        }
                    }
                    if (match) {
                        return 1;
                    }
                }
            }
        }
        return 0;
    }
}
