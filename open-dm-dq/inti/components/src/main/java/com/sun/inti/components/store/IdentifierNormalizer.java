package com.sun.inti.components.store;

public interface IdentifierNormalizer {
    public String normalizeFilename(String filename);
}
