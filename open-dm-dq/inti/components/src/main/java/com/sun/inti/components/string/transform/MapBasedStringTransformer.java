package com.sun.inti.components.string.transform;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.sun.inti.components.string.LineReaderComponent;

public class MapBasedStringTransformer extends LineReaderComponent implements StringTransformer {
	private static Logger logger = Logger.getLogger(MapBasedStringTransformer.class.getName());

    private Map<String, String> dictionary;
    
    public String transform(String term) {
        if (dictionary == null) {
            dictionary = new LinkedHashMap<String, String>();
            logger.fine("About to load dictionary");
            load();
        }
    	if (dictionary.containsKey(term)) {
            return dictionary.get(term);
    	}
        return term;
    }

    protected void addWords(String[] words) {
        if (words.length >= 2) {
            dictionary.put(words[0], words[1]);
        }
    }
    
    public MapBasedStringTransformer() {}
    
    public MapBasedStringTransformer(Map<String, String> dictionary) {
        this.dictionary = dictionary;
    }

    public MapBasedStringTransformer(String resourceName) throws IOException { super(resourceName); }
    public MapBasedStringTransformer(String resourceName, StringTransformer stringTransformer, String separator) throws IOException {
        super(resourceName, stringTransformer, separator);
    }
    
    public MapBasedStringTransformer(File file) throws IOException { super(file); }
    public MapBasedStringTransformer(File file, StringTransformer stringTransformer, String separator) throws IOException {
        super(file, stringTransformer, separator);
    }
    
    public MapBasedStringTransformer(URL url) throws IOException { super(url); }
    public MapBasedStringTransformer(URL url, StringTransformer stringTransformer, String separator) throws IOException {
        super(url, stringTransformer, separator);
    }
    
    public MapBasedStringTransformer(InputStream is) throws IOException { super(is); }
    public MapBasedStringTransformer(InputStream is, StringTransformer stringTransformer, String separator) throws IOException {
        super(is, stringTransformer, separator);
    }
    
    public MapBasedStringTransformer(Reader reader) throws IOException { super(reader); }
    public MapBasedStringTransformer(Reader reader, StringTransformer stringTransformer, String separator) throws IOException {
        super(reader, stringTransformer, separator);
    }

    public Map<String, String> getDictionary() { return dictionary; }
    public void setDictionary(Map<String, String> dictionary) { this.dictionary = dictionary; }
}
