package com.sun.inti.container.impl;

public interface ContainerConstants {
    public static final String RESOURCE_DIRECTORY_NAME = "resource";
    public static final String LIBRARY_DIRECTORY_NAME = "lib";
    public static final String INSTANCE_DIRECTORY_NAME = "instance";
    
    public final String SERVICE_TYPE_FILENAME = "serviceType.xml";
    public final String SERVICE_INSTANCE_FILENAME = "serviceInstance.xml";
    public final String SERVICE_TYPE_STYLESHEET = "serviceType.xsl";
    public final String SERVICE_INSTANCE_STYLESHEET = "serviceInstance.xsl";
    public final String SERVICE_TYPE_COMPONENT_NAME = "serviceType";
    public final String SERVICE_INSTANCE_COMPONENT_NAME = "serviceInstance";
}
