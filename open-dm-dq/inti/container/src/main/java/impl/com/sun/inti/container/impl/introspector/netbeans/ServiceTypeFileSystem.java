package com.sun.inti.container.impl.introspector.netbeans;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import org.openide.filesystems.AbstractFileSystem;
import org.openide.filesystems.DefaultAttributes;
import org.openide.filesystems.LocalFileSystem;

import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceTypeDescriptor;

public class ServiceTypeFileSystem extends AbstractFileSystem {
    private static final long serialVersionUID = 1L;
    
    private LocalFileSystem.Impl impl;
    private ServiceTypeDescriptor descriptor;
    
    public ServiceTypeFileSystem(final ServiceTypeDescriptor descriptor) {
        this.descriptor = descriptor;

        change = new Change() {
            public void createData(String name) throws IOException {
                throw new UnsupportedOperationException("Cannot create new files for service type directory");
            }
            public void createFolder(String name) throws IOException {
                throw new UnsupportedOperationException("Cannot create new folders for service type directory");
            }
            public void delete(String name) throws IOException {
                throw new UnsupportedOperationException("Cannot delete files for service type directory");
            }
            public void rename(String oldName, String newName) throws IOException {
                throw new UnsupportedOperationException("Cannot rename files for service type directory");
            }
        };
        
        info = new Info() {
            public boolean folder(String name) {
                return false;
            }
            public InputStream inputStream(String name) throws FileNotFoundException {
                try {
                    return new FileInputStream(descriptor.getResourceFile(name));
                } catch (ContainerException e) {
                    throw new FileNotFoundException("Can't open file: " + e);
                }
            }
            public Date lastModified(String name) {
                try {
                    return new Date(descriptor.getResourceFile(name).lastModified());
                } catch (ContainerException e) {
                    throw new RuntimeException("Error determining last modified date: " + e, e);
                }
            }
            public void lock(String name) throws IOException {
                throw new UnsupportedOperationException("Not implemented: lock");
            }
            public void markUnimportant(String name) {
            }
            public String mimeType(String name) {
                return "text/plain";
            }
            public OutputStream outputStream(String name) throws IOException {
                try {
                    return new FileOutputStream(descriptor.getResourceFile(name));
                } catch (ContainerException e) {
                    throw new FileNotFoundException("Can't open file: " + e);
                }
            }
            public boolean readOnly(String name) {
                return false;
            }
            public long size(String name) {
                try {
                    return descriptor.getResourceFile(name).length();
                } catch (ContainerException e) {
                    throw new RuntimeException("Error determining last modified date: " + e, e);
                }
            }
            public void unlock(String name) {
                throw new UnsupportedOperationException("Not implemented: lock");
            }
        };
        
        list = new List() {
            public String[] children(String name) {
                return descriptor.getResourceFilenames();
            }
        };
        
        attr = new DefaultAttributes(info, change, list);
    }
    
    @Override
    public String getDisplayName() {
        return descriptor.getName();
    }
    
    @Override
    public boolean isReadOnly() {
        return false;
    }
}
