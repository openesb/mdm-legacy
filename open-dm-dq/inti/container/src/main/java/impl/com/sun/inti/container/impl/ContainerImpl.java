package com.sun.inti.container.impl;

import static com.sun.inti.container.impl.ContainerConstants.INSTANCE_DIRECTORY_NAME;
import static com.sun.inti.container.impl.ContainerConstants.LIBRARY_DIRECTORY_NAME;
import static com.sun.inti.container.impl.ContainerConstants.RESOURCE_DIRECTORY_NAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_INSTANCE_COMPONENT_NAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_INSTANCE_FILENAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_INSTANCE_STYLESHEET;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_TYPE_COMPONENT_NAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_TYPE_FILENAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_TYPE_STYLESHEET;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.zip.ZipFile;

import com.sun.inti.components.component.ComponentManager;
import com.sun.inti.components.util.ComponentUtils;
import com.sun.inti.components.util.IOUtils;
import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;

public class ContainerImpl extends AbstractContainer {
    public static final Logger logger = Logger.getLogger(ContainerImpl.class.getName());
    
    private static final URL SERVICE_TYPE_STYLESHEET_URL = ContainerImpl.class.getResource(SERVICE_TYPE_STYLESHEET);
    private static final URL SERVICE_INSTANCE_STYLESHEET_URL = ContainerImpl.class.getResource(SERVICE_INSTANCE_STYLESHEET);

    private static int SHUTDOWN = 0;
    private static int INITIALIZED = 1;
    private String[] stateNames = { "Shutdown", "Initialized" };

    public final static FileFilter directoryFilter = IOUtils.getDirectoryFilter("META-INF", "CVS", "svn");

    private int state = SHUTDOWN;
    private File repositoryDirectory;
    private Map<String, ServiceType> serviceTypes = new LinkedHashMap<String, ServiceType>();

    public void initialize(File repositoryDirectory) throws ContainerException {
        logger.info("Initializing deployer");

        if (state != SHUTDOWN) {
            logger.warning("Can't initialize; current state: " + stateNames[state]);
            throw new ContainerException("Can't initialize; current state: " + stateNames[state]);
        }

        repositoryDirectory.mkdirs();
        if (!repositoryDirectory.isDirectory()) {
            logger.warning("Not a directory: " + repositoryDirectory);
            throw new ContainerException("Not a directory: " + repositoryDirectory);
        }
        if (!repositoryDirectory.canRead() || !repositoryDirectory.canWrite()) {
            logger.warning("Can't read/write: " + repositoryDirectory);
            throw new ContainerException("Can't read/write: " + repositoryDirectory);
        }

        this.repositoryDirectory = repositoryDirectory;

        for (File typeDirectory : repositoryDirectory.listFiles(directoryFilter)) {
            deployTypeDirectory(typeDirectory);
        }

        state = INITIALIZED;
        logger.info("Container successfully initialized. Repository: " + repositoryDirectory.getAbsolutePath());
    }

    public ServiceTypeDescriptor deployType(ZipFile deploymentFile) throws ContainerException {
        logger.info("Deploying type from file '" + deploymentFile.getName() + "'");

        if (state != INITIALIZED) {
            throw new IllegalStateException("Can't deploy; current state: " + stateNames[state]);
        }

        String typeName = IOUtils.basename(deploymentFile.getName());
        File typeDirectory = new File(repositoryDirectory, typeName);

        if (typeDirectory.isDirectory()) {
            undeployType(typeDirectory);
        }

        try {
            IOUtils.extract(deploymentFile, typeDirectory);
        } catch (IOException ioe) {
            logger.warning("Error extracting from deployment file'" + deploymentFile.getName() + "': " + ioe);
            throw new ContainerException("Error extracting from deployment file'" + deploymentFile.getName() + "': " + ioe, ioe);
        }

        return deployTypeDirectory(typeDirectory);
    }

    public ServiceInstanceDescriptor deployInstance(String typeName, ZipFile deploymentFile) throws ContainerException {
        logger.info("Deploying instance from file '" + deploymentFile.getName() + "'");

        if (state != INITIALIZED) {
            throw new IllegalStateException("Can't deploy; current state: " + stateNames[state]);
        }
        
        ServiceType serviceType = getServiceType(typeName);

        String instanceName = IOUtils.basename(deploymentFile.getName());
        File instanceDirectory = new File(serviceType.getDeploymentDirectory(), instanceName);

        if (instanceDirectory.isDirectory()) {
            undeployType(instanceDirectory);
        }

        try {
            IOUtils.extract(deploymentFile, instanceDirectory);
        } catch (IOException ioe) {
            logger.warning("Error extracting from instance deployment file'" + deploymentFile.getName() + "': " + ioe);
            throw new ContainerException("Error extracting from instance deployment file'" + deploymentFile.getName() + "': " + ioe, ioe);
        }

        return deployInstanceDirectory(instanceDirectory, serviceType);
    }

    public ServiceTypeDescriptor[] getServiceTypes() {
        return serviceTypes.values().toArray(new ServiceTypeDescriptor[serviceTypes.size()]);
    }

    public ServiceType getServiceType(String typeName) throws ContainerException {
        ServiceType serviceType = serviceTypes.get(typeName);
        if (serviceType == null) {
            logger.warning("No such deployment type: '" + typeName + "'");
            throw new ContainerException("No such deployment type: '" + typeName + "'");
        }
        return serviceType;
    }

    public Object getComponent(String typeName, String instanceName, String componentName) throws ContainerException {
        ServiceType serviceType = getServiceType(typeName);
        ServiceInstance serviceInstance = serviceType.getInstance(instanceName);
        return serviceInstance.getComponent(componentName);
    }

    public void undeployType(String typeName) throws ContainerException {
    	undeployType(getServiceType(typeName));
    }
 
    public void undeployInstance(String typeName, String instanceName) throws ContainerException {
    	undeployInstance(getServiceType(typeName), instanceName);
    }

    public void shutdown() throws ContainerException {
        if (state != INITIALIZED) {
            throw new IllegalStateException("Can't deploy; current state: " + stateNames[state]);
        }
        
        state = SHUTDOWN;
    }

    private ServiceTypeDescriptor deployTypeDirectory(File typeDirectory) throws ContainerException {
        ClassLoader classLoader = createClassLoader(typeDirectory, getClass().getClassLoader());

        File descriptorFile = new File(typeDirectory, SERVICE_TYPE_FILENAME);
        if (!descriptorFile.isFile()) {
        	throw new ContainerException("Missing type deployment descriptor file ");
        }
        ComponentManager componentManager = ComponentUtils.getComponentManager(descriptorFile, SERVICE_TYPE_STYLESHEET_URL, classLoader);
        ServiceType serviceType = (ServiceType) componentManager.getComponent(SERVICE_TYPE_COMPONENT_NAME);
        serviceType.setName(typeDirectory.getName());
        serviceType.setClassLoader(classLoader);
        serviceType.setBaseDirectory(typeDirectory);

        File deploymentDirectory = new File(typeDirectory, INSTANCE_DIRECTORY_NAME);
        if (deploymentDirectory.isDirectory()) {
            for (File instanceDirectory: deploymentDirectory.listFiles(directoryFilter)) {
                ServiceInstance serviceInstance = deployInstanceDirectory(instanceDirectory, serviceType);
                serviceType.addInstance(serviceInstance);
            }
        }
        
        serviceTypes.put(typeDirectory.getName(), serviceType);

        return serviceType;
    }
    
    private ServiceInstance deployInstanceDirectory(File instanceDirectory, ServiceType serviceType) throws ContainerException {
        ClassLoader classLoader = createClassLoader(instanceDirectory, serviceType.getClassLoader());

        ServiceInstance serviceInstance = null;
        File descriptorFile = new File(instanceDirectory, SERVICE_INSTANCE_FILENAME);
        if (descriptorFile.isFile()) {
            ComponentManager componentManager = ComponentUtils.getComponentManager(descriptorFile, SERVICE_INSTANCE_STYLESHEET_URL, classLoader);
            serviceInstance = (ServiceInstance) componentManager.getComponent(SERVICE_INSTANCE_COMPONENT_NAME);
        } else {
            serviceInstance = new ServiceInstance();
        }

        serviceInstance.setName(IOUtils.basename(instanceDirectory.getName()));
        serviceInstance.setBaseDirectory(instanceDirectory);
        serviceInstance.setClassLoader(classLoader);
        
        serviceType.addInstance(serviceInstance);
        
        return serviceInstance;
    }

    private void undeployType(File typeDirectory) throws ContainerException {
        logger.info("Undeploying type from directory '" + typeDirectory.getPath() + "'");
        ServiceType serviceType = serviceTypes.get(typeDirectory.getName());
        undeployType(serviceType);
    }

    private void undeployType(ServiceType serviceType) throws ContainerException {
        logger.info("Undeploying type '" + serviceType.getName() + "'");
        File deploymentDirectories = new File(serviceType.getBaseDirectory(), INSTANCE_DIRECTORY_NAME);
        if (deploymentDirectories.isDirectory()) {
            for (File instanceDirectory: deploymentDirectories.listFiles(directoryFilter)) {
                undeployInstance(serviceType, instanceDirectory.getName());
            }
        }
        IOUtils.recursiveDelete(serviceType.getBaseDirectory());
        serviceTypes.remove(serviceType.getName());
    }

    private void undeployInstance(ServiceType serviceType, String instanceName) throws ContainerException {
        logger.info("Undeploying instance '" + instanceName + "'");
        ServiceInstance serviceInstance = serviceType.getInstance(instanceName);
        IOUtils.recursiveDelete(serviceInstance.getBaseDirectory());
    	serviceType.removeInstance(instanceName);
    }

    private ClassLoader createClassLoader(File directory, ClassLoader parentClassLoader) throws ContainerException {
        List<URL> locations = new ArrayList<URL>();

        File resourceDirectory = new File(directory, RESOURCE_DIRECTORY_NAME);
        if (resourceDirectory.isDirectory()) {
            locations.add(urlFromFile(resourceDirectory));
        }

        File libraryDirectory = new File(directory, LIBRARY_DIRECTORY_NAME);
        if (libraryDirectory.isDirectory()) {
        	File[] libraryFiles = libraryDirectory.listFiles(new FileFilter() {
                public boolean accept(File file) {
                    return file.isFile() && (file.getName().endsWith(".jar") || file.getName().endsWith(".zip"));
                }
            });
        	if (libraryFiles.length > 0) {
                for (File libraryFile: libraryFiles) {
                    locations.add(urlFromFile(libraryFile));
                }
        	}
        }

        if (locations.size() == 0) {
            return parentClassLoader;
        }

        return new URLClassLoader(locations.toArray(new URL[locations.size()]), parentClassLoader);
    }
    
    public static URL urlFromFile(File file) throws ContainerException {
    	try {
    		return file.toURI().toURL();
    	} catch (MalformedURLException mue) {
    		throw new ContainerException("Malformed url for file '" + file.getAbsolutePath() + "': " + mue, mue);
    	}
    }
}
