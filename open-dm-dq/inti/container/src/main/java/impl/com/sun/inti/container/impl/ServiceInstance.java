package com.sun.inti.container.impl;

import java.util.logging.Logger;

import com.sun.inti.components.component.ComponentManager;
import com.sun.inti.components.component.ComponentManagerFactory;
import com.sun.inti.components.util.Parametrizable;
import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceInstanceDescriptor;

public class ServiceInstance extends ServiceContainerDescriptor implements ServiceInstanceDescriptor, ContainerConstants {
    private static final Logger logger = Logger.getLogger(ServiceInstance.class.getName());
    
    private ComponentManager componentManager;
    private ComponentManagerFactory componentManagerFactory;

    public ComponentManagerFactory getComponentManagerFactory() {
        return componentManagerFactory;
    }

    public void setComponentManagerFactory(ComponentManagerFactory componentManagerFactory) {
        this.componentManagerFactory = componentManagerFactory;
    }
    
    public Object getComponent(String componentName) throws ContainerException {
        if (componentManager == null) {
            if (componentManagerFactory == null) {
                logger.warning("No component manager factory defined for instance '" + getName() + "'");
                throw new ContainerException("No component manager factory defined for instance '" + getName() + "'");
            }
            logger.finer("Creating component manager from component factory");
            componentManager = componentManagerFactory.newInstance(getClassLoader(), getParameters());
        }
        
        Object component = componentManager.getComponent(componentName);
        
        if (component instanceof Parametrizable) {
            ((Parametrizable) component).setParameters(getParameters());
        }
        
        return component;
    }
}
