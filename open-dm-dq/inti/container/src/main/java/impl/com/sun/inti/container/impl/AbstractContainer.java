package com.sun.inti.container.impl;

import com.sun.inti.container.Container;
import com.sun.inti.container.ContainerException;

public abstract class AbstractContainer implements Container {
	public Object getComponent(String typeName, String instanceName) throws ContainerException {
		return getComponent(typeName, instanceName, typeName);
	}
}
