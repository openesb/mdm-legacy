package com.sun.inti.container.impl.introspector;

import static com.sun.inti.container.impl.ContainerConstants.INSTANCE_DIRECTORY_NAME;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;
import com.sun.inti.container.impl.AbstractContainerDescriptor;

public class ServiceTypeDescriptorImpl extends AbstractContainerDescriptor implements ServiceTypeDescriptor {
    private String defaultInstanceName;
    private Map<String, ServiceInstanceDescriptorImpl> instances = new LinkedHashMap<String, ServiceInstanceDescriptorImpl>();
    
    public ServiceInstanceDescriptor[] getInstances() {
        return instances.values().toArray(new ServiceInstanceDescriptor[instances.size()]);
    }

    public ServiceInstanceDescriptor getDefaultInstance() {
        return instances.get(defaultInstanceName);
    }
    
    protected void addInstance(ServiceInstanceDescriptorImpl instance) {
        instances.put(instance.getName(), instance);
    }
    
    protected void removeInstance(String instance) {
        instances.remove(instance);
    }
    
    public ServiceInstanceDescriptorImpl getInstance(String instanceName) throws ContainerException {
    	ServiceInstanceDescriptorImpl instance = instances.get(instanceName);
    	if (instance == null) {
    		throw new ContainerException("No such instance: '" + instanceName + "'");
    	}
    	return instance;
    }
    
    private File deploymentDirectory = null;
    protected File getDeploymentDirectory() {
    	if (deploymentDirectory == null) {
    		deploymentDirectory = new File(getBaseDirectory(), INSTANCE_DIRECTORY_NAME);
    		if (!deploymentDirectory.isDirectory()) {
    			deploymentDirectory.mkdirs();
    		}
    	}
    	return deploymentDirectory;
    }

    public String getDefaultInstanceName() {
        return defaultInstanceName;
    }

    public void setDefaultInstanceName(String defaultInstanceName) {
        this.defaultInstanceName = defaultInstanceName;
    }
}
