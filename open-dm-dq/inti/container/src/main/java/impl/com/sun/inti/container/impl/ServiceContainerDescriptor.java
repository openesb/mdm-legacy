package com.sun.inti.container.impl;

public class ServiceContainerDescriptor extends AbstractContainerDescriptor {
    private ClassLoader classLoader;
    
    protected ClassLoader getClassLoader() {
        return classLoader;
    }
    
    protected void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
