package com.sun.inti.container;

import java.io.File;
import java.util.Map;

public interface ContainerDescriptor {
    public String getName();
    public String getDescription();
    public Map<String, Object> getParameters();
    
    public String[] getResourceFilenames();
    public File getResourceFile(String filename) throws ContainerException;
}
