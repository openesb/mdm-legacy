package com.sun.inti.container;

public class ContainerException extends Exception {
    private static final long serialVersionUID = 1L;

    public ContainerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContainerException(String message) {
        super(message);
    }

    public ContainerException(Throwable cause) {
        super(cause);
    }
}
