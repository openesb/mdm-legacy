package com.sun.inti.container;

import java.io.File;
import java.util.zip.ZipFile;

public interface Container {
    public void initialize(File repositoryDirectory) throws ContainerException;

    public ServiceTypeDescriptor deployType(ZipFile deploymentFile) throws ContainerException;
    public ServiceInstanceDescriptor deployInstance(String typeName, ZipFile deploymentFile) throws ContainerException;
    
    public ServiceTypeDescriptor[] getServiceTypes();
    public ServiceTypeDescriptor getServiceType(String typeName) throws ContainerException;

    public Object getComponent(String typeName, String instanceName) throws ContainerException;
    public Object getComponent(String typeName, String instanceName, String componentName) throws ContainerException;

    public void undeployType(String typeName) throws ContainerException;
    public void undeployInstance(String typeName, String instanceName) throws ContainerException;

    public void shutdown() throws ContainerException;
}
