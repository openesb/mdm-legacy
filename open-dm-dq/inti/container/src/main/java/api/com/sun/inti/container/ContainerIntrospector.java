package com.sun.inti.container;

import java.io.File;
import java.util.zip.ZipFile;

public interface ContainerIntrospector {
    public ServiceTypeDescriptor[] setRepository(File repositoryDirectory) throws ContainerException;

    public ServiceTypeDescriptor[] importDirectory(File directory) throws ContainerException;
    public ServiceInstanceDescriptor[] importDirectory(String typeName, File directory) throws ContainerException;

    public ServiceTypeDescriptor[] getServiceTypes();
    public ServiceTypeDescriptor getServiceType(String typeName) throws ContainerException;

    public ContainerDescriptor importFile(ZipFile deploymentFile) throws ContainerException;

    public ServiceTypeDescriptor importServiceType(ZipFile deploymentFile) throws ContainerException;
    public ServiceInstanceDescriptor importServiceInstance(String typeName, ZipFile deploymentFile) throws ContainerException;

    public void removeInstance(String typeName, String instanceName) throws ContainerException;
    public void removeType(String typeName) throws ContainerException;
    
    public void takeSnapshot(File zipDestination) throws ContainerException;
}
