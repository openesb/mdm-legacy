package com.sun.inti.container.test;

import java.util.logging.Logger;

import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;
import com.sun.inti.container.impl.introspector.ContainerIntrospectorImpl;

public class IntrospectorTester {
	private static final Logger logger = Logger.getLogger(IntrospectorTester.class.getName());
	
	public static void main(String[] args) throws Exception {
//		ContainerIntrospectorImpl introspector = new ContainerIntrospectorImpl();
//		File repositoryDirectory = new File("repository");
//		IOUtils.recursiveDelete(repositoryDirectory);
//		introspector.setRepository(repositoryDirectory);
//        
//		introspector.importServiceType(new ZipFile("build/standardizer.zip"));
//		dump(introspector);
//		
//        introspector.importServiceInstance("standardizer", new ZipFile("build/personname.zip"));
//        dump(introspector);
        
	}
	
	private static void dump(ContainerIntrospectorImpl introspector) throws Exception {
        for (ServiceTypeDescriptor type: introspector.getServiceTypes()) {
            logger.info("Deployment descriptor: " + type.getName() + ": " + type.getDescription());
            for (ServiceInstanceDescriptor instance: type.getInstances()) {
                logger.info("    Deployment instance: '" + instance.getName() + "': " + instance.getDescription());
                for (String resourceFilename: instance.getResourceFilenames()) {
                    logger.info("        Resource filename: '" + resourceFilename + "': " + instance.getResourceFile(resourceFilename).getAbsolutePath());
                }
            }
        }
	}
}
