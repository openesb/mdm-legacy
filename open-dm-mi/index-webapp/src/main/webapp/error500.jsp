<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <link type="text/css" href="./css/styles.css"  rel="stylesheet">            
        </head>
        <title>500 Error Page</title>  
        <body>
                        
            <div id="content">  <!-- Main content -->
            <table border="0" cellpadding="3" cellspacing="1">
                <tr>
                    <td> 
                        <div class="errorHeadMessage"> HTTP 500 - Internal Server Error</div> 
                        <div id="errorSummary" class="errorSummary"> 
                            
                            <p>
                                We Apologize there is an error in the page.
                                The server encountered  unexpected condition 
                                which prevented it from fulfilling the request. 
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <!-- App name and logos -->
                    <td><img src="images/appname.gif"> </td>
                    <td align="right"><img src="images/sunlogo.gif"> </td>
                </tr>
            </table>
            <div> <!-- End Main Content -->
            
        </body>
    </html>
    
