<%@ page contentType="text/html"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ page import="com.sun.mdm.index.master.search.potdup.PotentialDuplicateIterator"  %>
<%@ page import="com.sun.mdm.index.master.search.potdup.PotentialDuplicateSummary"  %>
<%@ page import="com.sun.mdm.index.edm.services.configuration.ScreenObject"  %>
<%@ page import="com.sun.mdm.index.edm.services.configuration.FieldConfig"  %>
<%@ page import="com.sun.mdm.index.edm.presentation.managers.CompareDuplicateManager"  %>
<%@ page import="com.sun.mdm.index.edm.presentation.handlers.SearchDuplicatesHandler"  %>
<%@ page import="com.sun.mdm.index.edm.services.masterController.MasterControllerService"  %>
<%@ page import="com.sun.mdm.index.edm.control.QwsController"  %>

<%@ page import="com.sun.mdm.index.objects.EnterpriseObject"%>
<%@ page import="com.sun.mdm.index.objects.ObjectNode"%>
<%@ page import="com.sun.mdm.index.objects.SystemObject"%>
<%@ page import="com.sun.mdm.index.objects.SystemObjectPK"%>
<%@ page import="com.sun.mdm.index.objects.TransactionObject"%>
<%@ page import="com.sun.mdm.index.objects.epath.EPath"%>
<%@ page import="com.sun.mdm.index.objects.epath.EPathArrayList"%>

<%@ page import="java.text.SimpleDateFormat"  %>
<%@ page import="java.util.Date"  %>
<%@ page import="java.math.BigDecimal"  %>
<%@ page import="java.util.HashMap"  %>
<%@ page import="java.util.ArrayList"  %>
<%@ page import="java.util.Collection"  %>
<%@ page import="java.util.Iterator"  %>
<%@ page import="javax.el.*"  %>
<%@ page import="javax.el.ValueExpression" %>


<f:view>
    
    <f:loadBundle basename="com.sun.mdm.index.edm.presentation.messages.Edm" var="msgs" />
    <html>
        <head>
            <title><h:outputText value="#{msgs.application_heading}"/></title>  
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link type="text/css" href="./css/styles.css"  rel="stylesheet" media="screen">
            <link type="text/css" href="./css/calpopup.css" rel="stylesheet" media="screen">
            <link type="text/css" href="./css/DatePicker.css" rel="stylesheet" media="screen">
            
            <script type="text/javascript" src="scripts/yui/yahoo-dom-event.js"></script>             
            <script type="text/javascript" src="scripts/yui/animation.js"></script>            
            <script type="text/javascript" src="scripts/events.js"></script>            
            <script language="JavaScript" src="scripts/edm.js"></script>
            <script type="text/javascript" src="scripts/calpopup.js"></script>
            <script type="text/javascript" src="scripts/Validation.js"></script>
            <script type="text/javascript" src="scripts/Control.js"></script>
            <script type="text/javascript" src="scripts/dateparse.js"></script>
        </head>
        <body>
            <%@include file="./templates/header.jsp"%>
            <div id="mainContent" style="overflow:hidden">   
                    <div id="advancedSearch" class="basicSearchDup" style="visibility:visible;display:block">
                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                <h:form id="searchTypeForm" >
                            <tr>
                                <td>
                                    <h:outputText  rendered="#{SearchDuplicatesHandler.possilbeSearchTypesCount gt 1}"  value="#{msgs.patdet_search_text}"/>&nbsp;
                                    <h:selectOneMenu id="searchType" rendered="#{SearchDuplicatesHandler.possilbeSearchTypesCount gt 1}" 
                                                     onchange="submit();" style="width:220px;" 
                                                     value="#{SearchDuplicatesHandler.searchType}" 
                                                     valueChangeListener="#{SearchDuplicatesHandler.changeSearchType}" >
                                        <f:selectItems  value="#{SearchDuplicatesHandler.possilbeSearchTypes}" />
                                    </h:selectOneMenu>
                                </td>
                            </tr>
                </h:form>
            </table>
            <h:form id="advancedformData" >
                <h:inputHidden id="selectedSearchType" value="#{SearchDuplicatesHandler.selectedSearchType}"/>
                <table border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td>
                            <input id='lidmask' type='hidden' name='lidmask' value='' />
                            <h:dataTable headerClass="tablehead"  
                                         id="fieldConfigId" 
                                         var="feildConfig" 
                                         value="#{SearchDuplicatesHandler.screenConfigArray}">
                                <!--Rendering Non Updateable HTML Text Area-->
                                <h:column>
                                    <nobr>
                                        <h:outputText value="*" rendered="#{feildConfig.required}" />
                                        <h:outputText value="#{feildConfig.displayName}" />
                                    </nobr>
                                </h:column>
                                <!--Rendering HTML Select Menu List-->
                                <h:column rendered="#{feildConfig.guiType eq 'MenuList'}" >
                                    <nobr>
                                        <h:selectOneMenu value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                         onblur="javascript:accumilateSelectFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                         rendered="#{feildConfig.name ne 'SystemCode'}">
                                            <f:selectItem itemLabel="" itemValue="" />
                                            <f:selectItems  value="#{feildConfig.selectOptions}" />
                                        </h:selectOneMenu>
                                        
                                        <h:selectOneMenu  onchange="javascript:setLidMaskValue(this)"
                                                          onblur="javascript:accumilateSelectFieldsOnBlur(this,'#{feildConfig.name}')"
                                                          id="SystemCode" 
                                                          value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}" 
                                                          rendered="#{feildConfig.name eq 'SystemCode'}">
                                            <f:selectItem itemLabel="" itemValue="" />
                                            <f:selectItems  value="#{feildConfig.selectOptions}" />
                                        </h:selectOneMenu>
                                    </nobr>
                                </h:column>
                                <!--Rendering Updateable HTML Text boxes-->
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType ne 6}" >
                                    <nobr>
                                        <h:inputText   required="#{feildConfig.required}" 
                                                       label="#{feildConfig.displayName}" 
                                                       onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                       onkeyup="javascript:qws_field_on_key_up(this)"
                                                       onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                       value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                       maxlength="#{feildConfig.maxLength}" 
                                                       rendered="#{feildConfig.name ne 'LID'}"/>
                                        
                                        <h:inputText   required="#{feildConfig.required}" 
                                                       label="#{feildConfig.displayName}" 
                                                       onkeydown="javascript:qws_field_on_key_down(this, document.advancedformData.lidmask.value)"
                                                       onkeyup="javascript:qws_field_on_key_up(this)"
                                                       onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.name}')"
                                                       value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                       maxlength="#{SourceMergeHandler.lidMaskLength}" 
                                                       rendered="#{feildConfig.name eq 'LID'}"/>
                                                       
                                    </nobr>
                                </h:column>
                                
                                <!--Rendering Updateable HTML Text Area-->
                                <h:column rendered="#{feildConfig.guiType eq 'TextArea'}" >
                                    <nobr>
                                        <h:inputTextarea label="#{feildConfig.displayName}"  id="fieldConfigIdTextArea"   value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}" required="#{feildConfig.required}"/>
                                    </nobr>
                                </h:column>
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && feildConfig.range  && feildConfig.displayName eq 'DOB From'}" >
                                    <nobr>
                                        <h:inputText id="DOBFrom" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.displayName]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.displayName}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var dateFrom =  getDateFieldName('advancedformData','DOBFrom');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,dateFrom)" > 
                                            <h:graphicImage  id="calImgDateFrom" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && feildConfig.range  && feildConfig.displayName eq 'DOB To'}" >
                                    <nobr>
                                        <h:inputText id="DOBTo" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.displayName]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.displayName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var dateTo =  getDateFieldName('advancedformData','DOBTo');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,dateTo)" > 
                                            <h:graphicImage  id="calImgDateTo" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'StartDate'}" >
                                    <nobr>
                                    <h:inputText id="StartDate" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                 required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                 onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                 onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.displayName}')"
                                                 onkeyup="javascript:qws_field_on_key_up(this)" />
                                    <script> var startdate = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                    <a HREF="javascript:void(0);" 
                                       onclick="g_Calendar.show(event,startdate)" > 
                                        <h:graphicImage  id="calImgStartDate" 
                                                         alt="calendar Image" styleClass="imgClass"
                                                         url="./images/cal.gif"/>               
                                    </a>
                                    <nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'EndDate'}" >
                                    <nobr>
                                        <h:inputText id="EndDate" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.displayName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var EndDate = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,EndDate)" > 
                                            <h:graphicImage  id="calImgEndDate" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && feildConfig.name eq 'create_start_date'}" >
                                    <nobr>
                                    <h:inputText id="create_start_date" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                 required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                 onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.name}')"
                                                 onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                 onkeyup="javascript:qws_field_on_key_up(this)" />
                                    <script> var create_start_date = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                   <a HREF="javascript:void(0);" 
                                       onclick="g_Calendar.show(event,create_start_date)" > 
                                        <h:graphicImage  id="calImgcreate_start_date" 
                                                         alt="calendar Image" styleClass="imgClass"
                                                         url="./images/cal.gif"/>               
                                    </a>
                                    <nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && feildConfig.name eq 'create_end_date'}" >
                                    <nobr>
                                        <h:inputText id="create_end_date" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.name}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var create_end_date = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,create_end_date)" > 
                                            <h:graphicImage  id="calImgcreate_end_date" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'DOB'}" >
                                    <nobr>
                                        <h:inputText id="DOB" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var DOB = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,DOB)" > 
                                            <h:graphicImage  id="calImgDOB" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'Dod'}" >
                                    <nobr>
                                        <h:inputText id="Dod" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var Dod = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,Dod)" > 
                                            <h:graphicImage  id="calImgDod" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'Date1'}" >
                                    <nobr>
                                        <h:inputText id="Date1" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var Date1 = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,Date1)" > 
                                            <h:graphicImage  id="calImgDate1" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'Date2'}" >
                                    <nobr>
                                        <h:inputText id="Date2" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var Date2 = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,Date2)" > 
                                            <h:graphicImage  id="calImgDate2" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'Date3'}" >
                                    <nobr>
                                        <h:inputText id="Date3" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var Date3 = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,Date3)" > 
                                            <h:graphicImage  id="calImgDate3" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'Date4'}" >
                                    <nobr>
                                        <h:inputText id="Date4" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var Date4 = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,Date4)" > 
                                            <h:graphicImage  id="calImgDate4" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'Date5'}" >
                                    <nobr>
                                        <h:inputText id="Date5" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var Date5 = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,Date5)" > 
                                            <h:graphicImage  id="calImgDate5" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'PensionExpDate'}" >
                                    <nobr>
                                        <h:inputText id="PensionExpDate" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var PensionExpDate = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,PensionExpDate)" > 
                                            <h:graphicImage  id="calImgPensionExpDate" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'DummyDate'}" >
                                    <nobr>
                                        <h:inputText id="DummyDate" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var DummyDate = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,DummyDate)" > 
                                            <h:graphicImage  id="calImgDummyDate" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                                <h:column rendered="#{feildConfig.guiType eq 'TextBox' && feildConfig.valueType eq 6 && !feildConfig.range  && feildConfig.name eq 'EnterDate'}" >
                                    <nobr>
                                        <h:inputText id="EnterDate" label="#{feildConfig.displayName}"    value="#{SearchDuplicatesHandler.updateableFeildsMap[feildConfig.name]}"
                                                     required="#{feildConfig.required}"  maxlength="#{feildConfig.maxLength}"
                                                     onkeydown="javascript:qws_field_on_key_down(this, '#{feildConfig.inputMask}')"
                                                     onblur="javascript:accumilateFieldsOnBlur(this,'#{feildConfig.fullFieldName}')"
                                                     onkeyup="javascript:qws_field_on_key_up(this)" />
                                        <script> var EnterDate = getDateFieldName('advancedformData','<h:outputText value="#{feildConfig.name }" />');</script>
                                        <a HREF="javascript:void(0);" 
                                           onclick="g_Calendar.show(event,EnterDate)" > 
                                            <h:graphicImage  id="calImgEnterDate" 
                                                             alt="calendar Image" styleClass="imgClass"
                                                             url="./images/cal.gif"/>               
                                        </a>
                                    </nobr>
                                </h:column>
                            </h:dataTable>
                            <table border="0" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td>
                                        <nobr>
                                            <h:outputLink  styleClass="button"  value="javascript:void(0)" onclick="javascript:ClearContents('advancedformData')">
                                                <span><h:outputText value="#{msgs.clear_button_label}"/></span>
                                            </h:outputLink>
                                        </nobr>
                                        <nobr>
                                            <h:commandLink  styleClass="button" rendered="#{Operations.potDup_SearchView}"  action="#{SearchDuplicatesHandler.performSubmit}" >  
                                                <span>
                                                    <h:outputText value="#{msgs.search_button_label}"/>
                                                </span>
                                            </h:commandLink>                                     
                                        </nobr>
                                        
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <h:messages styleClass="errorMessages"  layout="list" />
                        </td>
                    </tr>
                </table>
                <h:inputHidden id="enteredFieldValues" value="#{SearchDuplicatesHandler.enteredFieldValues}"/>
            </h:form>

                   </div>  
                  <%
                    ScreenObject objScreenObject = (ScreenObject) session.getAttribute("ScreenObject");
                    CompareDuplicateManager compareDuplicateManager = new CompareDuplicateManager();
                    
                    EPathArrayList ePathArrayList = compareDuplicateManager.retrieveEPathArrayList(objScreenObject);
                    EPath ePath = null;
                    SimpleDateFormat simpleDateFormatFields = new SimpleDateFormat("MM/dd/yyyy");
                    
                    
                    PotentialDuplicateIterator pdPageIter = (PotentialDuplicateIterator) session.getAttribute("pdPageIter");
                    ArrayList finalArrayList = (ArrayList) request.getAttribute("finalArrayList");
                    PotentialDuplicateIterator asPdIter;
                    PotentialDuplicateSummary mainDuplicateSummary = null;
                    
                    PotentialDuplicateSummary associateDuplicateSummary = null;
                    int countMain = 0;
                    int totalMainDuplicates = 0;
                    int totalAssoDuplicates = 0;
                    String mainDob = null;
                    String assoDob = null;
                    
                    String mainEuidContentDiv = null ;
                    String assoEuidContentDiv = null ;
                    String previewEuidContentDiv = null ;
                    String dupHeading = "<b>Duplicate </b>";
                    SearchDuplicatesHandler searchDuplicatesHandler = new  SearchDuplicatesHandler(); 
                    Iterator searchResultFieldsIter = searchDuplicatesHandler.getResultsConfigArray().iterator();
                    Object[] resultsConfigFeilds  = searchDuplicatesHandler.getResultsConfigArray().toArray();
                    StringBuffer stringBuffer = new StringBuffer();
                    StringBuffer mainEUID = new StringBuffer();
                    StringBuffer dupEUID = new StringBuffer();
                    
                    %>                
                <br>   
                  <%
                     if(finalArrayList != null && finalArrayList.size() >0 ) {
                         request.setAttribute("finalArrayList", request.getAttribute("finalArrayList"));
                 %>                
                     <div class="printClass">
                       <table cellpadding="0" cellspacing="0" border="0">
                         <tr>
                             <td>
                                 <h:outputText value="#{msgs.total_records_text}"/><%=finalArrayList.size()%>&nbsp;&nbsp;
                             </td>
                             <td>
                                  <h:outputLink styleClass="button" 
                                                rendered="#{Operations.potDup_Print}"  
                                                value="JavaScript:window.print();">
                                      <span><h:outputText value="#{msgs.print_text}"/>  </span>
                                 </h:outputLink>              
                             </td>
                         </tr>
                       </table>
                    </div>
                 <%}%>
                <%
                    if(finalArrayList != null && finalArrayList.size() >0 ) {
                %>
                     <div id="dataDiv" class="compareResults" style="overflow:auto;width:1074px;height:1024px;">
                    <div>
                        <table>
                            <tr>
                            <%
                            if(finalArrayList != null && finalArrayList.size() >0 ) {
                                for(int fac=0;fac<finalArrayList.size();fac++) {
                                   
                                %>
                            <div id="mainEuidDiv<%=countMain%>">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img src="images/spacer.gif" width="15"></td>
                                        <%
                                        HashMap resultArrayMapMain = new HashMap();
                                        HashMap resultArrayMapCompare = new HashMap();
                                        String epathValue;
                                        ArrayList arlInner = (ArrayList) finalArrayList.get(fac);
                                        String subscripts[] = compareDuplicateManager.getSubscript(arlInner.size());
                                        for (int j = 0; j < arlInner.size(); j++) {
                                               HashMap eoHashMapValues = (HashMap) arlInner.get(j);
                                               //EnterpriseObject eoSource = compareDuplicateManager.getEnterpriseObject(strDataArray);

                                               //int weight = ((Float) eoHashMapValues.get("Weight")).intValue();
                                               String  weight =  eoHashMapValues.get("Weight").toString();
                                               //weight = (new BigDecimal(weight)).ROUND_CEILING;
                                               //float weight = ((Float) eoHashMapValues.get("Weight")).floatValue();
                                               
                                               HashMap fieldValuesMapSource = (HashMap) eoHashMapValues.get("ENTERPRISE_OBJECT");
                                               

                                               // Code to render headers
                                               if (j>0)
                                                {    dupHeading = "<b> "+j+"<sup>"+subscripts[j] +"</sup> Duplicate </b>";
                                                } else if (j==0)
                                                {    dupHeading = "<b> Main EUID</b>";
                                                }
                                               //String strDataArray = (String) arlInner.get(j);
                                               //EnterpriseObject eoSource = compareDuplicateManager.getEnterpriseObject(strDataArray);
                                               //HashMap fieldValuesMapSource = compareDuplicateManager.getEOFieldValues(eoSource, objScreenObject) ;
                                               for (int ifc = 0; ifc < resultsConfigFeilds.length; ifc++) {
                                                FieldConfig fieldConfigMap = (FieldConfig) resultsConfigFeilds[ifc];
                                                if (fieldConfigMap.getFullFieldName().startsWith(objScreenObject.getRootObj().getName())) {
                                                    epathValue = fieldConfigMap.getFullFieldName();
                                                } else {
                                                    epathValue = objScreenObject.getRootObj().getName() + "." + fieldConfigMap.getFullFieldName();
                                                }
                                                if (j > 0) {
                                                    resultArrayMapCompare.put(epathValue, fieldValuesMapSource.get(epathValue));
                                                } else {
                                                    resultArrayMapMain.put(epathValue, fieldValuesMapSource.get(epathValue));
                                                }
                                              }
                                        
                                               
                                               
                                        %>
                                       <%if(j == 0 ) {%>
                                        <td valign="top">
                                            <div id="mainEuidContent">
                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td valign="top" style="width:100%;height:45px;border-bottom: 1px solid #EFEFEF; ">&nbsp;</td>
                                                    </tr> 
                                                </table>
                                            </div> 
                                             <div id="mainEuidContentDiv<%=countMain%>" class="dynaw169">
                                                <table border="0" cellspacing="0" cellpadding="0" class="w169">
                                                    <%
                                                     for(int ifc=0;ifc<resultsConfigFeilds.length;ifc++) {
                                                        FieldConfig fieldConfigMap = (FieldConfig) resultsConfigFeilds[ifc]; 
                                                             
                                                    %>
                                                    <tr><td><%=fieldConfigMap.getDisplayName()%></td></tr>
                                                    <%}%>
                                                </table>
                                            </div>   
                                        </td>
                                        <td valign="top">
                                            <div id="mainEuidContent" class="w169">
                                                <table border="0" cellspacing="0" cellpadding="0" class="w169">
                                                    <tr>
                                                        <td valign="top" class="menutop">Main EUID</td>
                                                    </tr> 
                                                    <tr>
                                                        <td valign="top" class="dupfirst">
                                                            <a href="#" class="dupbtn">
                                                                <%=fieldValuesMapSource.get("EUID")%>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                        
                                                </table>
                                            </div> 
                                            <div id="mainEuidContentDiv<%=countMain%>" class="dynaw169">
                                                <table border="0" cellspacing="0" cellpadding="0" class="w169">
                                                    <%
                                                     for(int ifc=0;ifc<resultsConfigFeilds.length;ifc++) {
                                                        FieldConfig fieldConfigMap = (FieldConfig) resultsConfigFeilds[ifc]; 
                                                        if (fieldConfigMap.getFullFieldName().startsWith(objScreenObject.getRootObj().getName())) {
                                                             epathValue = fieldConfigMap.getFullFieldName();
                                                         } else {
                                                             epathValue = objScreenObject.getRootObj().getName() + "." + fieldConfigMap.getFullFieldName();
                                                         }
                                                     %>
                                                    <tr>
                                                        <td>

                                                            <%if (fieldValuesMapSource.get(epathValue) != null) {%>
                                                              <%=fieldValuesMapSource.get(epathValue)%>
                                                            <%} else {%>
                                                            &nbsp;
                                                            <%}%>

                                                        </td>                                                        
                                                    </tr>
                                                    <%}%>
                                                </table>
                                            </div>   
                                        </td>
                                        <%} else {%> <!--For duplicates here-->  

                                            <%if (j ==1 && arlInner.size() > 3 ) { %>
                                            <!--Sri-->
                                            <td>
                                                 <div style="overflow:auto;width:507px;overflow-y:hidden;">
                                                     <table>
                                                         <tr>
                                            <%}%>
                                        
                                           <td valign="top">
                                            <div id="mainEuidContent" style="width:169px;overflow:auto;overflow-y:hidden;overflow-x:visible;width:169px;">
                                                <table border="0" cellspacing="0" cellpadding="0" class="w169">
                                                    <tr>
                                                        <td valign="top" class="menutop"><%=dupHeading%>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                      <td valign="top" class="dupfirst">
                                                          <a href="#" class="dupbtn">
                                                             <%=fieldValuesMapSource.get("EUID")%>        
                                                          </a>  
                                                      </td>
                                                    </tr>
                                                </table>
                                            </div> 
                                            <div id = "bar" style = "margin-left:140px;height:100px;width:5px;background-color:green;border-left: 1px solid #000000;border-right: 1px solid #000000;border-top:1px solid #000000;position:absolute;" >
                                                <div style= "height:<%=100 - new Float(weight).floatValue() %>px;width:5px;align:bottom;background-color:#ededed;" ></div> 
                                            </div>                                             
                                            <div id = "bar" style = "margin-left:135px;padding-top:100px;width:5px;position:absolute;font-size:10px;" >
                                                <%=weight%>
                                            </div> 
                                            <div id="mainEuidContentDiv<%=countMain%>" class="dynaw169" >
                                                <table border="0" cellspacing="0" cellpadding="0" class="w169">
                                                    <%
                                                     for(int ifc=0;ifc<resultsConfigFeilds.length;ifc++) {
                                                        FieldConfig fieldConfigMap = (FieldConfig) resultsConfigFeilds[ifc]; 
                                                        if (fieldConfigMap.getFullFieldName().startsWith(objScreenObject.getRootObj().getName())) {
                                                             epathValue = fieldConfigMap.getFullFieldName();
                                                         } else {
                                                             epathValue = objScreenObject.getRootObj().getName() + "." + fieldConfigMap.getFullFieldName();
                                                         }
                                                             
                                                    %>
                                                    <tr>                                                        
                                                       <td>
                                                                <%if (fieldValuesMapSource.get(epathValue) != null) {%>
                                                                
                                                                <%if ((j > 0 && resultArrayMapCompare.get(epathValue) != null && resultArrayMapMain.get(epathValue) != null) &&
            !resultArrayMapCompare.get(epathValue).toString().equalsIgnoreCase(resultArrayMapMain.get(epathValue).toString())) {

                                                                %>
                                                                    
                                                                    <font class="highlight">
                                                                        <%=fieldValuesMapSource.get(epathValue)%>
                                                                    </font>
                                                                <%} else {
                                                                %>
                                                                    <%=fieldValuesMapSource.get(epathValue)%>
                                                                <%}%>
                                                                <%} else {%>
                                                                &nbsp;
                                                                <%}%>
                                                                

                                                        </td>        
                                                    </tr>
                                                    <%}%>
                                                </table>
                                            </div>   
                                        </td>
                                            <%if (arlInner.size() > 3 && j == (arlInner.size()-1 )) { %>
                                            <!--Raj-->
                                                       </tr>
                                                     </table>
                                                 </div>
                                                </td>
                                            <%}%>
                                        
                                        <%}%>
                                        <td class="w7yelbg">&nbsp;</td><!--Separator b/n columns-->
                                        <%}%>
                                      <td class="w169" valign="top">
                                      <div id="previewEuidDiv<%=countMain%>" style="visibility:visible;display:block;">
                                          <table border="0" width="100%" cellspacing="0" cellpadding="0" class="w169">
                                              <tr>
                                                  <td width="100%" class="menutop1">Preview</td>
                                              </tr>
                                          </table>
                                      </div>
                                     <div id="previewEuidContentDiv<%=countMain%>"   class="dynaw169">
                                        <div id="showCompareButtonDiv<%=countMain%>" style="visibility:visible;">
                                            <table border="0" cellspacing="0" cellpadding="0" class="w169"> 
                                                <tr>
                                                        <td>&nbsp;</td>
                                                </tr>
                                                <%
                                                   for (int i = 0; i < resultsConfigFeilds.length - 1; i++) {
                                                     FieldConfig fieldConfig = (FieldConfig) resultsConfigFeilds[i];
                                                    %>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                <%}%>

                                                <tr>
                                                    <td valign="top" align="right">
                                                        <!--Show compare duplicates button-->
                                                        <%
                                                        ValueExpression euidVaueExpressionList = ExpressionFactory.newInstance().createValueExpression(arlInner, arlInner.getClass());
                                                        %>
                                                         <h:form>
                                                            <h:commandLink styleClass="downlink"  
                                                                           action="#{NavigationHandler.toCompareDuplicates}" 
                                                                           actionListener="#{SearchDuplicatesHandler.buildCompareDuplicateEuids}">
                                                                <f:attribute name="euidsMap"  value="<%=euidVaueExpressionList%>"  />
                                                            </h:commandLink>
                                                        </h:form>   

                                                    </td> 
                                                </tr>
                                            </table>
                                        </div>
                                      </div>  
                                    </div>
                                    
                           </tr>
                        </table>
                    </div> 
                    <div id="separator"  class="sep"></div>
                         <%}%> <!--final Array list count loop -->
                         <%}%> <!-- final Array list  condition in session-->
               </div> 
               <%}%>  
               <%
                   if (finalArrayList != null && finalArrayList.size() == 0) {
               %>
               <div class="printClass">
                       <table cellpadding="0" cellspacing="0" border="0">
                         <tr>
                             <td>
                                 <h:outputText value="#{msgs.total_records_text}"/><%=finalArrayList.size()%>&nbsp;
                             </td>
                         </tr>
                       </table>
               </div>
               <%}%>
               
            </div>
        </body>        
        <%
          String[][] lidMaskingArray = searchDuplicatesHandler.getAllSystemCodes();
          
        %>
        <script>
            var systemCodes = new Array();
            var lidMasks = new Array();
        </script>
        
        <%
        for(int i=0;i<lidMaskingArray.length;i++) {
            String[] innerArray = lidMaskingArray[i];
            for(int j=0;j<innerArray.length;j++) {
            
            if(i==0) {
         %>       
         <script>
           systemCodes['<%=j%>']  = '<%=lidMaskingArray[i][j]%>';
         </script>      
         <%       
            } else {
         %>
         <script>
           lidMasks ['<%=j%>']  = '<%=lidMaskingArray[i][j]%>';
         </script>
         <%       
            }
           }
           }
        %>
    <script>
        function setLidMaskValue(field) {
            var  selectedValue = field.options[field.selectedIndex].value;
            document.advancedformData.lidmask.value  = getLidMask(selectedValue,systemCodes,lidMasks);
            //alert(selectedValue);
         }   
         
     var selectedSearchValue = document.getElementById("searchTypeForm:searchType").options[document.getElementById("searchTypeForm:searchType").selectedIndex].value;
     document.getElementById("advancedformData:selectedSearchType").value = selectedSearchValue;

    </script>
     
        
    </html> 
</f:view>