<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <link type="text/css" href="./css/styles.css"  rel="stylesheet">            
        </head>
        <title>404 Error Page</title>  
        <body>
                        
            <div id="content">  <!-- Main content -->
            <table border="0" cellpadding="3" cellspacing="1">
                <tr>
                    <td> 
                        <div class="errorHeadMessage"> Page Not Found </div> 
                        <div id="errorSummary" class="errorSummary"> 
                                        
                            <p>
                                Sorry, there is no web page matching your request.
                                It is possible you typed the address incorrectly,
                                or that the page no longer exists.The page you are 
                                looking for might have been removed, had its name
                                changed, or is temporarily unavailable.
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <!-- App name and logos -->
                    <td><img src="images/appname.gif"> </td>
                    <td align="right"><img src="images/sunlogo.gif"> </td>
                </tr>
            </table>
            <div> <!-- End Main Content -->
        
        </body>
    </html>
    
