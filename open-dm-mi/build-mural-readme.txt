
-- NetGen Software Inc., www.netgensoftware.com --

The open source mural project url is http://openesb-dev.org/projects/mdm-legacy.

Procedures to build open source mural
1. check out open source mural 
   svn checkout --username username http://openesb-dev.org/svnroot/mdm-legacy mdm-legacy
2. edit env.bat under [MDM_LEGACT_ROOT]/mdm-legacy/trunk/open-dm-mi 
3. run  env.bat under [MDM_LEGACT_ROOT]/mdm-legacy/trunk/open-dm-mi
4. mvn install -Dmaven.test.skip=true under [MDM_LEGACT_ROOT]/mdm-legacy/trunk/open-dm-mi
5. mvn install -Dmaven.test.skip=true under [MDM_LEGACT_ROOT]/mdm-legacy/trunk/open-dm-di
6. mvn install -Dmaven.test.skip=true under [MDM_LEGACT_ROOT]/mdm-legacy/trunk/open-dm-dq
7. >ant -f build-mural-nbm.xml make-mural under [MDM_LEGACT_ROOT]/mdm-legacy/trunk/open-dm-mi

Deliverable
[MDM_LEGACT_ROOT]/mdm-legacy/trunk/mural-nbm/mural-1.0.nbm

Install mural-1.0.nbmin Open ESB or Oracle JavaCAPS R6 netbeans:

1. Start NetBeans
2. Click Plugins from NetBeans Tools menu
3. Click Add Plugins from Downloaded tab
4. Click to install mural-1.0.nbm from Install tab
5. Cick Finish and close Plugins window once installation is complete

