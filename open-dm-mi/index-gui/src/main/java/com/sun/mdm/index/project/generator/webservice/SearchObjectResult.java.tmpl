[SBYNTAG:DEFINE:objname]
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import com.sun.mdm.index.webservice.SearchObjectResult;
import com.sun.mdm.index.webservice.ObjectBean;

/** 
   Contains the Search Result invoked from Eview[sbyntag:objname.makeClassName].search method.
 */
public final class Search[sbyntag:objname]Result implements SearchObjectResult{
    public static final int version = 1;
    
    private String meuid;
    private float mcomparison;
    private [sbyntag:objname.makeClassName]Bean m[sbyntag:objname];
    
    public Search[sbyntag:objname]Result() {
    }
    
    /**
     * constructor
     */
    public Search[sbyntag:objname]Result([sbyntag:objname.makeClassName]Bean bean, String euid, float comparisonScore ) {
       meuid = euid;
       mcomparison = comparisonScore;
       m[sbyntag:objname] = bean;
    }

    public String getEUID() {
       return meuid;
    }

    public float getComparisonScore() {
       return mcomparison;
    }

    public [sbyntag:objname.makeClassName]Bean get[sbyntag:objname.makeClassName]() {
      return m[sbyntag:objname];
    }

    /**
     * Getter for object bean
     * @return object bean 
     */
    public ObjectBean pGetObjectBean() {
       return get[sbyntag:objname.makeClassName]();
    }

    public void setEUID(String euid) {
       meuid = euid;
    }

    public void setComparisonScore(float score) {
       mcomparison = score;
    }

    public void set[sbyntag:objname.makeClassName]([sbyntag:objname.makeClassName]Bean bean) {
       m[sbyntag:objname] = bean;
    }
}
