/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.configurator;

import com.sun.mdm.matcher.comparators.validator.MatchComparatorParameters;
//import com.sun.mdm.matcher.weightcomp.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

import com.sun.mdm.matcher.api.MatchingEngine;
import com.sun.mdm.matcher.api.MatcherException;
import com.sun.mdm.matcher.comparators.ComparatorsManager;

import com.sun.mdm.matcher.comparators.configurator.ComparatorsConfigurationService;
import com.sun.mdm.matcher.comparators.configurator.ComparatorsConfigBean;

import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;
import java.util.logging.Level;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatchConfigFile {

    // Parameters name
    private static final String PARAMS = MatchVariables.PARAMS;    
    // Data sources name
    private static final String DATA_SOURCES = MatchVariables.DATA_SOURCES;    
    private static final String TYPE = MatchVariables.TYPE;
    private static final String NAME = MatchVariables.NAME;
    private static final String CURVE_ADJUST = MatchVariables.CURVE_ADJUST;
    
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();      
    
    private int matchFieldsNumber;

    /**
     * Define a factory method that return an instance of the class
     * @return a MatchConfigFile object
     */
    public static MatchConfigFile getInstance(InputStream matchConfigStream) 
        throws IOException {
        // Instance of the class
        MatchConfigFile mcf = new MatchConfigFile();
        // Calculates the number of matching fields in the config file
        mcf.calculateNumMatchFields(matchConfigStream);
                
        // Close the stream to the match config. file.
        matchConfigStream.close();    
    
        // Return the instance
        return mcf;
    }
    
    /*
     * Calculates the number of matching fields in the config file
     */    
    private void calculateNumMatchFields(InputStream matchConfigStream) {
        /* */
        java.io.LineNumberReader r = new java.io.LineNumberReader(new BufferedReader(
                                            new InputStreamReader(matchConfigStream)));
        // The number of lines
        int lineNum = 0;
        String s = null;
      
        try {                      
            while ((s = r.readLine()) != null) {
                // Increment
                if (s.length() > 0) {
                    lineNum++;
                }                
            }                 
        } catch(IOException e) {
            mLogger.severe(mLocalizer.x("MEG020: Problem reading match configuration file lines.", e));                 
        } 
        matchFieldsNumber = lineNum - 1;
    }
    
    /**
     * Returns the number of matching fields in the config file
     */
    public int getMatchFieldsNumber() {
        return matchFieldsNumber;
    }
       
    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)     
     * 
     * @param matchConfigStream a config file stream
     * @param mEng an instance of the MatchingEngine class
     * @param domain geographic location of the compared data
     * @return an integer
     * @throws SbmeMatchingException a generic exception
     * @throws MatcherException a parsing exception
     * @throws IOException an exception
     */
    public int getConfigData(InputStream matchConfigStream, MatchingEngine mEng,
        String domain)
        throws MatcherException, MatcherException, IOException, InstantiationException {
        
        return getConfigData(matchConfigStream, mEng);
    }    
    
    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)
     * 
     * @param matchConfigStream a config file stream
     * @param mEng an instance of the MatchingEngine class
     * @return an integer
     * @throws SbmeMatchingException a generic exception
     * @throws MatcherException a parsing exception
     * @throws IOException an exception
     */
    public int getConfigData(InputStream matchConfigStream, MatchingEngine mEng)
        throws MatcherException, MatcherException, IOException, InstantiationException {

        int i = 0;
        int j = 0;
        int ij;
        int iid;
        int comTypeSwitch;
        String tempS = null;
        int tempInt = 1;
        int option = 0;
        Map context = new HashMap();
        StringBuilder compType = new StringBuilder();
        char displayMode = ' ';
        
        // Get an instance of the class that holds all the variables
        MatchVariables matchVar = mEng.getMatchVar(mEng);
        // Reads the instance class of the match comparators manager
        ComparatorsManager comparMgr = mEng.getComparatorManager(mEng);
        
        // Get an instance of the class that holds the match comparators' config data.
        ComparatorsConfigBean compBean = ComparatorsConfigurationService.getInstance().getMatchComparatorsConfigBean();
        
        matchVar.nCrosses = 0;
        matchVar.criticalTest = 0;

        /* Stream from the configuration match file */
        Reader r = new BufferedReader(new InputStreamReader(matchConfigStream));
        StreamTokenizer psTokens = new StreamTokenizer(r);

        // First line in the config file

        // The switch option for the type of the initial probabilities to be used.
        // The first character is either M for modern or C for classic.
        // The second character is either zero if we use directly the probabilities: {0.999 0.001}, 
        // One if we use the weights: {100, -100}). 
        // The two options are equivalent, but the weights are preferable. 
        psTokens.nextToken();
        tempS = psTokens.sval;
        
        if ((tempS != null) && (tempS.compareTo("ProbabilityType") == 0)) {

            // Read the value of the switch [N, C][0-1]
            psTokens.nextToken();
 
            String pType = psTokens.sval;           
            // Test the length of pType. If one character (the classic config., then, it must be a [0-1].
            // Else, if it is a 2-character length, it should be of the form [N, C][0-1].
            if(psTokens.ttype == StreamTokenizer.TT_NUMBER) {
                tempInt = (int) psTokens.nval;
               
            } else if(psTokens.ttype == StreamTokenizer.TT_WORD && psTokens.sval.length() == 2) {
                displayMode = pType.charAt(0);
                tempInt = Integer.parseInt(pType.substring(1, 2));  
  
                // Validate the type of mode display used (M=modern, C=Classic)
                if (!((displayMode == 'M') || (displayMode == 'C'))) {                    
                    mLogger.severe(mLocalizer.x("MEG021: The Display mode is incorrect. Use M=Modern or C=Classic"));             
                    throw new MatcherException("The Display mode is incorrect. Use M=Modern or C=Classic"); 
                }
            }
            
            if (mLogger.isLoggable(Level.FINE)) {               
                mLogger.fine("Read the probability-type switch" + tempInt);
            }
            
            // Validate the type of weight representation used.
            if ((tempInt == 0) || (tempInt == 1)) {
                comTypeSwitch = tempInt;
            } else {
                mLogger.severe(mLocalizer.x("MEG022: The probability-type switch value is incorrect {0}",tempInt)); 
                throw new MatcherException("The ProbabilityType switch must be 0 or 1");
            }           
            
        } else {
            mLogger.severe(mLocalizer.x("MEG023: The probability-type switch is missing"));            
            throw new MatcherException("The ProbabilityType switch is missing");
        }

        // Read the list of valid matching types (code-name/full class name)
        Map<String, String> compMap = compBean.getClassNamesMap();     
        // Store the list of comparators codes in the MatchVariables class
        matchVar.setClassNamesMap(compMap);        
       
        Object[] std = compMap.keySet().toArray();
        // Details info of the comparatgors list
        Map<String, Map> comparatorDetails = compBean.getComparatorDetails();
        // Class name associated with a given code name
        String className;
        // The details for each comparator
        Map<String, ArrayList<Map<String, String>>> details = new HashMap();
        // Holds a map of the class name - parameters list
        ArrayList<Map<String, String>> params;
        // Holds a map of the class name - data-source list
        ArrayList<Map<String, String>> dataSources;
        // Holds a map of the curveadjustment status
        ArrayList<Map<String, String>> curveAdjust;
        // Rely the field name to the code name
        Map<String, String> fieldNameCode = new HashMap();
        
        // Customize the parameters handling.
        MatchComparatorParameters matchP = new MatchComparatorParameters(matchVar);
        
        // Temporary removed. Will be added in the next version
        matchVar.nTable = 0;
        matchVar.dw = 0;
        matchVar.eMSwitch = 0;
        
        // Initialize the number of matching fields
        int numFields = 1;

        // Start reading the data associated with the matching fields
        for (i = 0; i < numFields; i++) {
            
            if (i == 0) {
                psTokens.nextToken();
            }
  
            // Name of the matching variable
            if (psTokens.ttype == StreamTokenizer.TT_WORD && psTokens.sval != null) {            
                matchVar.fieldName.put(i, psTokens.sval);
                matchVar.fieldNameIndex.put(psTokens.sval, i);
            } else {
                break;
            }

            // Index associated with the matching field
            matchVar.fieldIndex[i] = i;

            // Maximum lenght, in characters, of the matching field
            psTokens.nextToken();
            matchVar.lengthF[i] = ((int) psTokens.nval);

            psTokens.nextToken();
            
            // Flag related to empty/null fields. It specifies a custom value of the weight
            if (psTokens.ttype == StreamTokenizer.TT_WORD) {
                matchVar.nullFlag[i] = psTokens.sval;
            } else {
                matchVar.nullFlag[i] = String.valueOf((int) psTokens.nval);
            }

            psTokens.nextToken();
            
            // Reads the code-name for the used match comparator. The string representation
            // may contain up to 6 characters. 
            matchVar.comparatorType[i] = psTokens.sval;
        
            // Link field name to code name
            fieldNameCode.put(matchVar.fieldName.get(i), matchVar.comparatorType[i]);
            // Verify that the match type is valid
            if(compMap.containsKey(psTokens.sval)) {
                className = compMap.get(psTokens.sval);              
            } else if (matchVar.secondaryCodes.containsKey(psTokens.sval)) {
                className = compMap.get(matchVar.secondaryCodes.get(psTokens.sval));
            } else { 
                mLogger.severe(mLocalizer.x("MEG024: The match comparator: '{0}' "+
                        "is invalid! choose a valid comparator among: {1}",psTokens.sval, compMap)); 
                throw new MatcherException("The match comparator: '" + psTokens.sval
                    + "' is invalid! choose a valid comparator among: \n " + compMap);                       
            }            

            // If the length of the string is less than 6, add spaces
//            iid = 6 - matchVar.comparatorType[i].length();
            
//            if (iid > 0) {
//                for (int k = 0; k < iid; k++) {
//                    matchVar.comparatorType[i].append(" ");
//                }
//            }

            
            // Method 1: Initial value for m-probability and u-probability
            if(displayMode == 'C' || displayMode == ' ') {
                // The initial value for m-probability that the corresponding fields agree
                // given that the records match
                psTokens.nextToken();
      
                if (comTypeSwitch == 0) {
                    matchVar.Mproba[i] = psTokens.nval;
                }

                // The initial value for u-probability that the corresponding fields agree
                // given that the records nonmatch
                psTokens.nextToken();
       
                if (comTypeSwitch == 0) {
                    matchVar.Uproba[i] = psTokens.nval;
                }
            } else {
                if (mLogger.isLoggable(Level.FINE)) {
                    mLogger.fine("You are in a display mode where you need to show only the initial weights"+
                                  " in the matchConfigFile");
                }                
            }
            
            psTokens.nextToken();
            
            // Method 2: Initial value for m-probability that the corresponding fields agree
            // given that the records match           
            if (comTypeSwitch == 1) {
                matchVar.Mproba[i] = psTokens.nval;
                if (mLogger.isLoggable(Level.FINE)) {
                    mLogger.fine("Reading the m-weight");
                }

                if (matchVar.Mproba[i] > 100.0 || matchVar.Mproba[i] < -100.0) {
                    mLogger.severe(mLocalizer.x("MEG025: the maximum weight should be between [100, -100]"));
                    throw new MatcherException("the maximum weight should be between [100, -100]");
                }
            }

            // Method 2: Initial value for u-probability that the corresponding fields agree
            // given that the records nonmatch
            psTokens.nextToken();
            if (comTypeSwitch == 1) {
                matchVar.Uproba[i] = psTokens.nval;
                if (mLogger.isLoggable(Level.FINE)) {
                    mLogger.fine("Reading the u-weight");
                }                
 
                if (matchVar.Uproba[i] < -100.0 || matchVar.Uproba[i] > 100.0) {
                    mLogger.severe(mLocalizer.x("MEG026: the minimum weight should be between [100, -100]"));
                    throw new MatcherException("the minimum weight should be between [100, -100]");

                } else if (matchVar.Uproba[i] > matchVar.Mproba[i]) {
                    mLogger.severe(mLocalizer.x("MEG027: the max weight must be greater than the min weight"));
                    throw new MatcherException("the max weight must be greater than the min weight");
                }
            }

            // value associated with the 3-class emalgorithm
            matchVar.fracVal[i] = 0.3;

            // Integer that defines the number of possible patterns
            // in the frequency count algorithm (Default = 2)
            matchVar.fieldPatt[i] = 2;
            
            // This portion checks if there are any additional fields that represent
            // custom curve-adjustment parameters or any other group of additional fields.

            // Treat the end of line as a token
            psTokens.eolIsSignificant(true);

            // All the configuration details for each comparator class.
            details = comparatorDetails.get(className);              

            psTokens.nextToken();

            if (psTokens.ttype != StreamTokenizer.TT_EOL
                && psTokens.ttype != StreamTokenizer.TT_EOF) {

                // The comparator parameters' details.
                params = details.get(PARAMS);  
                // The comparator data sources info.
                dataSources = details.get(DATA_SOURCES);                 

                // Reads all the comparator's parameters defined within the configuration file.
                if (params != null && !params.isEmpty()) {

                    // Put here the generic parameters handling
                    Iterator<Map<String, String>> paramIter = params.iterator();
                    Map<String, Object> parameters = new HashMap();
                    Map<String, String> elements;
                    int ind = 0;
                    double valN;
                    int len = params.size();
                  
                    while(psTokens.ttype != StreamTokenizer.TT_EOL && ind < len) {
                        
                        if(paramIter.hasNext()) {                       
                            elements = paramIter.next();
                        } else if (psTokens.ttype == StreamTokenizer.TT_EOF) {
                            break;
                        } else {
                            mLogger.severe(mLocalizer.x("MEG028: Wrong number of parameters for the comparator {0}. "+
                                          "You need to define {1}", className, len));
                            throw new MatcherException("Wrong number of parameters of the comparator "+className+
                                                   ". You need to define "+len+" parameters.");                                          
                        }
                        
                        // Increment the index
                        ind++;  
                        
                        // Check for array of characters
                        boolean ssnOption = (elements.get(TYPE).toString().compareTo("java.lang.Character[]") == 0);
                        boolean endList = false;
                        // Case where it is a list of characters patterns
                        if (ssnOption) {  
                            // Temporary string
                            StringBuffer list = new StringBuffer();
                            String valS; 
                            //                                                                    
                            if (elements.containsKey(TYPE)) {
                                // Loop over all the characters
                                while (psTokens.ttype != StreamTokenizer.TT_EOL) {
                                    
                                    if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {
                                        valN = psTokens.nval;  
                                        int intVal = Double.valueOf(valN).intValue();
                                        if (Integer.toString(intVal).length() == 1) {                                        
                                            list.append(String.valueOf((int)valN));
                                            psTokens.nextToken();
                                        } else {
                                            endList = true;
                                            // End of list
                                            break;                              
                                        }                                    
                                    } else if (psTokens.ttype == StreamTokenizer.TT_WORD) {
                                        valS = psTokens.sval; 
                                        
                                        if (valS.length() == 1) {                                        
                                            list.append(valS);
                                            psTokens.nextToken();
                                        } else {
                                            endList = true;
                                            // End of list
                                            break;                             
                                        }                                                         
                                    }
                                    if (!endList && psTokens.ttype != StreamTokenizer.TT_EOL) {
                                        // Append an empty space
                                        list.append(" ");
                                    }
                                }  
                                parameters.put(elements.get(NAME), list.toString());                                                              
                                                                              
                            } else {
                                mLogger.severe(mLocalizer.x("MEG029: You have to define a type attribute "+
                                        "for the parameters of the comparator {0}", className));
                                throw new MatcherException("You have to define a type attribute for the "+
                                        "parameters of the comparator "+className);                                
                            }
                                
                        // Case where it is a number
                        } else if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {
                            
                            valN = psTokens.nval;
                            if (elements.containsKey(TYPE)) {
                                if (elements.get(TYPE).toString().compareTo("java.lang.Integer") == 0) {   
 
                                    if (valN - (int)valN == 0) {                                        
                                        parameters.put(elements.get(NAME), String.valueOf((int)valN));
                                        psTokens.nextToken();
                                    } else {
                                        mLogger.severe(mLocalizer.x("MEG030: You have to use Integer values for the {0}"+
                                                      " parameter of the comparator {1}", ind, className));
                                        throw new MatcherException("You have to use Integer values for the "+ind+
                                                                   " parameter of the comparator "+className);                              
                                    }
                                } else if (elements.get(TYPE).toString().compareTo("java.lang.Double") == 0) { 
                                    parameters.put(elements.get(NAME), String.valueOf(valN));
                                    psTokens.nextToken();
                                } else if (elements.get(TYPE).toString().compareTo("java.lang.Integer|java.lang.Double") == 0) {   
                                    if (valN - (int)valN == 0) {                                        
                                        parameters.put(elements.get(NAME), (int)valN/*String.valueOf((int)valN)*/);
                                        psTokens.nextToken();
                                    } else {
                                        parameters.put(elements.get(NAME), valN/*String.valueOf(valN)*/);
                                        psTokens.nextToken();                          
                                    } 
                                }
                            } else {
                                mLogger.severe(mLocalizer.x("MEG031: You have to define a type attribute "+
                                        "for the parameters of the comparator {0}", className));
                                throw new MatcherException("You have to define a type attribute for the "+
                                        "parameters of the comparator "+className);                                
                            }
                            
                        // Case where it is a string
                        } else if (psTokens.ttype == StreamTokenizer.TT_WORD) {                        
                            if (elements.containsKey(TYPE)) {                   
                                if (elements.get(TYPE).toString().compareTo("java.lang.String") == 0) {                                     
                                        parameters.put(elements.get(NAME), psTokens.sval);
                                        psTokens.nextToken();
                                }
                            } else {
                                mLogger.severe(mLocalizer.x("MEG032: You have to define a type attribute for "+
                                        "the parameters of the comparator {0}", className));
                                throw new MatcherException("You have to define a type attribute for the parameters "+
                                        "of the comparator "+className);                                
                            }                                                        
                        }                                                
                    } 
                    // The match field name
           
                    // Store all the parameters into the params variable for each match variable
                    matchVar.params.put(i, parameters);
                }
                
                
                // Reads all the comparator's data sources info defined within the configuration file.
                if (dataSources != null && !dataSources.isEmpty()) {

                    // Put here the generic data-source handling
                    Iterator<Map<String, String>> sourcesIter = dataSources.iterator();
                    List<String> sources = new ArrayList();
                    Map<String, String> sElements;
//                    int ind1 = 0;
//                    int len1 = dataSources.size();

                    // Check if the data source exists in the config file.
                    while(sourcesIter.hasNext()) {     
                        sElements = sourcesIter.next(); 
                        // Verify the validity of the type
                        if (sElements.containsKey(TYPE) && sElements.get(TYPE).compareTo("java.io.File") != 0) {
                            mLogger.severe(mLocalizer.x("MEG070: This data source type is not supported in this version"+
                                           "We support Files only. {0}", className));
                            throw new MatcherException("This data source type is not supported in this version "+
                                           "We support Files only. "+className);                               
                        }
                        String path = null;

                        // Read the path                                                                     
//                        if (sElements.containsKey(NAME) && sElements.containsValue(valueName)) {  

                            if (psTokens.ttype != StreamTokenizer.TT_EOL || psTokens.ttype != StreamTokenizer.TT_EOF) {
                            path = psTokens.sval;
                            psTokens.nextToken();
                        } else {
                            throw new MatcherException("You have to define a 'path' attribute for the data source "+
                                           "in the matchConfigFile for "+className);          
                        } 
//                            sources.put(valueName, valueName+"|"+path);
       //                 sources.put(valueName, path);  
                        sources.add(path);   
//                        psTokens.nextToken(); 
                        if (!sourcesIter.hasNext()) {
                            // stop
                            break;
                        }                                            
                    }      

                    // All the data
                    matchVar.sources.put(i, sources);                                    
                }
                
//                if (params.isEmpty() && dataSources.isEmpty()) {   
//                    mLogger.severe(mLocalizer.x("MAT002: The {0} comparator should be corrected in the comparatorList config. file.", className));
//                    throw new MatcherException("The "+className+" comparator should be corrected in the comparatorList config. file.");                         
//                }  
                    // Move to the next token 
                    psTokens.nextToken();                 


                if (psTokens.ttype == StreamTokenizer.TT_EOF) {
                    numFields++;
                }

                  

                // The curve adjustment status
                if (details.containsKey(CURVE_ADJUST)) {
                    curveAdjust = details.get(CURVE_ADJUST);  

                    if (curveAdjust != null) {
                        String status = curveAdjust.get(0).get("status");
                        if (status.compareTo("true") == 0) {
                            
                            option = 0;
                            
                            // This portion checks if there are any additional fields that represent
                            // custom curve-adjustment parameters or any other group of additional fields.            
                            // If there is less than 4 curve-adjustment parameters, initialize
                            // the remaining elements to zero                                               
                            if (psTokens.ttype != StreamTokenizer.TT_EOL || psTokens.ttype != StreamTokenizer.TT_EOF) {                            
                                try {
                                    option = Integer.parseInt(psTokens.sval);
                                } catch (NumberFormatException e) {
                                    throw new NumberFormatException("The curve ajustment option must be an integer "+e);
                                }
//                                al.add(option);
                                psTokens.nextToken();   
                            } 
                        }
                    }
                }
            } else {            
                psTokens.nextToken();
            }                    
            // The curve adjustment status
            if (details.containsKey(CURVE_ADJUST)) {
                curveAdjust = details.get(CURVE_ADJUST);  

                if (curveAdjust != null) {
                    String status = curveAdjust.get(0).get("status");
                    if (status.compareTo("true") == 0) {

                        // Method that specify the curve-adjustment parameters for different comparators
                        CurveAdjustmentManager.adjustParameters(matchVar, option, i);                          
                    }
                }
            }                    
            
            // Handle the initial m and u probabilities for both choices
            if (comTypeSwitch == 0 && displayMode == 'C') {
                // Calculate the initial agreement and disagreement weight
                matchVar.agrWeight[i] =
                    Math.log(matchVar.Mproba[i] / matchVar.Uproba[i]) / Math.log(2.);

                matchVar.disWeight[i] = Math.log((1.0 - matchVar.Mproba[i])
                            / (1.0 - matchVar.Uproba[i])) / Math.log(2.);
            } else {
                matchVar.agrWeight[i] = matchVar.Mproba[i];
                matchVar.disWeight[i] = matchVar.Uproba[i];
            }

            // Register the match fields info into fieldNameInfo
            context.put("fieldName", matchVar.fieldName.get(i));
            context.put("codeName", matchVar.comparatorType[i]);
            context.put("fieldLength", matchVar.lengthF[i]);            
            matchVar.fieldNameInfo.add((Map<String, String>)((HashMap) context).clone());
            context.clear();
            
            // Increment the number of match fields
            numFields++;
        }
        
        // First, perform the 'normal' parameters validation
        matchP.handleParametersValidation();
        if(!matchVar.sources.isEmpty()) {
          
            // Second, handle the data sources extra information and validation
            matchVar.sourcesData = matchP.handleDataSourcesValidation();
        }
        
        // Initiate the data within the match comparators
        comparMgr.initiate(matchVar, comparatorDetails, fieldNameCode);
                    
        // The total number of match fields
        numFields -= 1;


        if (matchVar.eMSwitch > 0) {

            // An upper bond on the proportion of pairs estimated as a match.
            // This number is associated with the first class (matches)
            matchVar.proportion = 0.9;

            // The maximum degree of precision wanted in the EM iterative
            // algorithm when estimating the probabilities.
            matchVar.degPrecision = 0.00001;

            // The maximum number of iterations in the E-M algorithm
            matchVar.nIteration = 200;

            // The minimum authorized value for the weights
            matchVar.minD = 0.00001;

            // The maximum authorized value for the weights
            matchVar.maxD = 0.99999;
        }

        // Close the stream to the match config. file.
        matchConfigStream.close();

        return 1;
    }


    /**
     * Returns a set of integers that define uniquely a given matching field's
     * data
     * 
     * @param fieldNames name of a matching field.
     * @param mEng an instance of the MatchingEngine class
     * @return the index associated with the matching field: fieldName
     */
    public int[] getFieldIndices(String[] fieldNames, MatchingEngine mEng) {

        int i;
        int j;
        int numFields = fieldNames.length;
        int[] indices = new int[numFields];
        MatchVariables matchVar = mEng.getMatchVar(mEng);

        for (i = 0; i < numFields; i++) {
            
            // Check if field name is defined
            if (matchVar.fieldName.containsValue(fieldNames[i])) {
                indices[i] = matchVar.fieldNameIndex.get(fieldNames[i]);
            } 
        }
        return indices;
    }    
 }