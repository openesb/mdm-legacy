/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.configurator; 

import com.sun.mdm.matcher.api.MatcherConfigurationException;
import java.util.Map;
import java.util.ArrayList;
        
/**
 * Match comparators configuration information.
 * Handles the parsing of the Phonetic Encoder configuration and exposes the settings.
 *
 * @author souaguenouni
 */
public class ComparatorsConfigBean {

    // The number of different groups of comparators
    private int groupsNumber;
    
    // The map between the code name and the full-path class name (group path + class name)
    private Map<String, String> classNamesMap;      
    
    // The map between the code name and its description
    private Map<String, String> codenamesDesc;   
    // The mapping between the secondary code name and its corresponding code name
    private Map<String, String> secondCodenamesDesc;       
    
    // Hold a simplified view of the comparators' configuration details
    Map<String, Map<String, ArrayList>> paramsInfo;
    
    // The list of the groups index mapped to their attributes (description & path) 
    private ArrayList<String> groupAttributes;     
    
    // Map that holds the comparator class names as keys and the associated elements 
    // of the comparators as the values (group index, code name, description, params, data-sources)
    Map<String, Map> comparatorDetails;
    

    /**
     * Creates new ComparatorsConfigBean
     */
    public ComparatorsConfigBean() {
    }

    /**
     * Returns the number of groups within the comparators list
     *
     * @return the number of groups within the comparators list
     */
    public int getGroupsNumber() {
        return groupsNumber;
    }
    
    /**
     * Sets the number of groups within the comparators list
     */
    public void setGroupsNumber(int groupNumber) {
        this.groupsNumber = groupNumber;
    }    

    /**
     * Returns the list of comparators class names
     *
     * @return the list of comparators class names
     */
    public Map<String, String> getClassNamesMap() {
        return classNamesMap;
    }

    /**
     * Returns the list of code names with their description
     *
     * @return the list of code names with their description
     */
    public Map<String, String> getCodeNamesDesc() {
        return codenamesDesc;
    } 
    /**
     * Returns the list of secondary code names with their 
     * corresponding code names
     *
     * @return the list of secondary code names/ code names
     */
    public Map<String, String> getSecondaryCodeNamesDesc() {
        return secondCodenamesDesc;
    }     
    
    /**
     * A simplified view of the comparators' configuration details
     *
     * @return a simplified view of the comparators' configuration details
     */
    public Map<String, Map<String, ArrayList>> getParametersDetailsForGUI() {
        return paramsInfo;
    }   
    

    /**
     * Sets the number of groups within the comparators list
     */
    public void setClassNamesMap(Map classNamesMap) {
        this.classNamesMap = classNamesMap;
    }    
    
    /**
     * Returns the list of comparators class names
     *
     * @return the list of comparators class names
     */
    public ArrayList<String> getGroupAttributes() {
        return groupAttributes;
    }
    
    /**
     * Sets the number of groups within the comparators list
     */
    public void setGroupAttributes(ArrayList<String> groupAttributes) {
        this.groupAttributes = groupAttributes;
    }  
    
    /**
     * Returns the list of comparators class names
     *
     * @return the list of comparators class names
     */
    public Map<String, Map> getComparatorDetails() {
        return comparatorDetails;
    }
    
    /**
     * Sets the number of groups within the comparators list
     */
    public void setComparatorDetails(Map<String, Map> comparatorDetails) {
        this.comparatorDetails = comparatorDetails;
    }        
    
    /**
     * Sets the number of groups within the comparators list
     */
    public void setCodeNamesDesc(Map<String, String> codeNamesDesc) {
        this.codenamesDesc = codeNamesDesc;
    }   

    /**
     * Sets the mapping secondary codes / codes
     */
    public void setSecondCodeNamesDesc(Map<String, String> secondCodenamesDesc) {
        this.secondCodenamesDesc = secondCodenamesDesc;
    }       
    
    /**
     * Sets a simplified view of the comparators' configuration details
     * to be used within the core GUI
     */
    public void setParametersDetailsForGUI(Map<String, Map<String, ArrayList>> paramsInfo) {
        this.paramsInfo = paramsInfo;
    }       
}
