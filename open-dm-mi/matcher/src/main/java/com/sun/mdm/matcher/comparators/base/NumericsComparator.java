/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.api.MatcherException;
import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.util.NumbersManipulation;

//import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.text.ParseException;
import java.util.Map;
import java.util.List;

/**
 * This class handles comparison between dates and times in a standard format  
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class NumericsComparator implements MatchComparator {

    private Map<String, Map> params;
    private Map<String, Map> dependClassList;
    // The list of parameters
    private Map<String, String> theParams;        
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;
        this.dependClassList = dependClassList;        
    }

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
    }    
    
    /**
     * Close any related data sources streams
     */
    public void stop() {}       
    
    /**
     * 
     *
     * @param numberA  Candidate's string record.
     * @param numberB  Reference's string record.
     * @param index the time unit used
     * @param matchVar an instance of the matchVariables class
     * @return a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     * @throws ParseException if a string parsing failed
     */
    public double compareFields(String numberA, String numberB, Map context)
        throws MatchComparatorException {
        
        char type = '0';
        String strA;
        String strB;
        Number numA = null;
        Number numB = null;
        int theRangeI = 0;
        double theRangeD = 0;
 
        MatchComparator classInstance = (MatchComparator) dependClassList.get("CSC");
        // Set the instance parameter
        Map<String, Map> depContext = dependClassList.get("dependencyContext");
        Map usContext = depContext.get("CSC");        
        
        // The field name
        String codeName = context.get("codeName").toString();
        
        // The list of parameters
        theParams = this.params.get(context.get("fieldName"));
        
        String theSwitch = theParams.get("switch");
        Object range = theParams.get("range"); 

        if (theSwitch == null) {
            throw new MatchComparatorException("You need to define the switch parameters within Numerics comparator");
        } else if (theSwitch.compareTo("y") == 0) {
            if (range instanceof Integer) {
                theRangeI = Integer.parseInt(range.toString());
            } else if (range instanceof Double) {
                theRangeD = Double.parseDouble(range.toString());                
            } else {
                throw new MatchComparatorException("The range parameter must be an integer or a real");
            }
        }            

        
        if (codeName.charAt(0) != 'n') {
            throw new MatchComparatorException("Use a number comparator starting with: 'n'");
        }
        
        if (codeName.length() > 1) {
            type = codeName.charAt(1);   
        } else {
            type = 'R';
        }
    
        // Handle the number format
        NumberFormat nf = NumberFormat.getInstance(Locale.US);

        if (type == 'I') {
            nf.setParseIntegerOnly(true);
        }
        
        try {
            // Before parsing, make sure that both records are numbers
            if (NumbersManipulation.isNumber(numberA) && NumbersManipulation.isNumber(numberB)) {
                try {
                    numA = nf.parse(numberA);
                    numB = nf.parse(numberB);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }                                
            }


            // For use in the transposer
            if ((theSwitch.compareTo("n") == 0)) {

                if (type == 'I') {       

                    strA = ((Long) numA).toString();
                    strB = ((Long) numB).toString();

                // handle doubles
                } else if (type == 'R') {

                    if (numA instanceof Long) {
                         strA = ((Long) nf.parse(numberA)).toString();

                    } else if (numA instanceof Double) {
                        strA = ((Double) nf.parse(numberA)).toString();

                    } else {
                        throw new MatchComparatorException("Use only numbers with the numeric comparator");
                    }

                    if (numB instanceof Long) {
                        strB = ((Long) nf.parse(numberB)).toString();

                    } else if (numB instanceof Double) {
                        strB = ((Double) nf.parse(numberB)).toString();

                    } else {
                        throw new MatchComparatorException("Use only numbers with the numeric comparator");
                    }

                } else { 
                    throw new MatchComparatorException("The comparator you are using does not exist." +
                        "you must use one of the following: 'nI' or 'nR");
                }

                // Call the transposer and return the weight associated with
                // comparing the two integers
                return transposeAndCompareNumbers(strA, strB, type, classInstance, usContext);


            } else {

                // Handle integers
                if (type == 'I') { 
                    int intA = (nf.parse(numberA)).intValue();
                    int intB = (nf.parse(numberB)).intValue();

                    return compareIntegers(intA, intB, theRangeI);

                // handle doubles
                } else if (type == 'R') {
                    double realA = (nf.parse(numberA)).doubleValue();
                    double realB = (nf.parse(numberB)).doubleValue();

                    return compareDoubles(realA, realB, theRangeD);

                } else { 
                    throw new MatchComparatorException("The comparator you are using does not exist." +
                        "you must use one of the following: 'nI', 'nR' or 'nS'");
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
        
        
    /*
     * 
     */    
    private double transposeAndCompareNumbers(String listA, String listB, char unit, MatchComparator classInstance, Map context)
            throws MatchComparatorException {
            
        double weight = 0;        
        // Compare only the year components
        if (unit == 'I') {
            
            // Compare only year
            weight = classInstance.compareFields(listA, listB, context);              
            
        // Compare the year and months elements
        } else if (unit == 'R') {
            
            // Combine months and years into the weight
            weight = classInstance.compareFields(listA, listB, context);                                
        }       
        return weight;
    }


    /*
     * 
     */ 
    private double compareIntegers(int listA, int listB, int theRange)
            throws MatchComparatorException {
        
        int deltaInt = Math.abs(listB - listA);
        double compV;

        if (deltaInt <= theRange) { 
            
            compV = 1.0 - ((double) deltaInt) / (theRange + 1);
        
        } else { 
            compV = 0.0;
        }           
        // Compute the comparator between the 2 integers
        return compV;
    }
 

    /*
     * 
     */     
    private double compareDoubles(double listA, double listB, double theRange)
            throws MatchComparatorException {
        
        double deltaDouble = Math.abs(listB - listA);
        double compV;
        if (deltaDouble <= theRange) {
            
            compV = 1.0 - deltaDouble / (theRange + 1);
        
        } else { 
            compV = 0.0;
        }
            
        // Compute the comparator between the 2 integers
        return compV;
    }
}
