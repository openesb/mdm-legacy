/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators;

import java.util.Map;
import java.util.List;

/**
 *
 * @author souaguenouni
 */
public interface MatchComparator {

    /**
     * Initialize the parameters and data sources info.
     *
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList);    

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value);
    
     /**
     * Reads two strings and measure how close they are relying on an algorithm
     * that compare the proximity of the two strings (zero being very different and
     * one being identical)
     *
     * @param  recordA  Candidate's string record.
     * @param  recordB  Reference's string record.
      *@param context a set of arguments passed to the comparator
     * @return  a real number between zero and one that measures the degree of similarity.
     */
    double compareFields(String recordA, String recordB, Map context) 
       throws MatchComparatorException;     
    
    /**
     * Close any related data sources streams
     */
    void stop();    
}
