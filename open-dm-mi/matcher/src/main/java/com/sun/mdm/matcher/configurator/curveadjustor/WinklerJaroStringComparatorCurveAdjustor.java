/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JaroAdvancedStringComparatorCurveAdjustor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.mdm.matcher.configurator.curveadjustor;

import com.sun.mdm.matcher.configurator.CurveAdjustor;

/**
 *
 * @author souaguenouni
 */
public class WinklerJaroStringComparatorCurveAdjustor implements CurveAdjustor {
    
    private static double defaultCurv[] = {1, 0.0, 4.5, 4.5};
    private static double[] hf = {0.0, 0.0, 0.0, 0.0};   
    
    /**
     * 
     * Creates a new instance of JaroAdvancedStringComparatorCurveAdjustor
     */
    public double[] processCurveAdjustment(String compar, int option) {
  
        int j;
        int defSw = 0;
        double[] adjCurv = new double[4];
        
        for (j = 0; j < 4; j++) {

            // The custom values must be different from zero
            if (option != 0.0)  {
                adjCurv[j] = hf[j];

            } else {
                defSw++;
                // If one of the values is zero, need further consideration.
                break;
            }
        }
            
        // If at least one of the custom adjustment parameters is not
        // specified (i.e. hf[] = 0), use default values.
        if (defSw != 0) {
            // Case where we have uncertainty for first name
            if ((compar.compareTo("ua") == 0)) {
                for (j = 0; j < 4; j++) {
                    // Default adjustment values for first name
                    adjCurv[j] = defaultCurv[j];
                }
            } 
        }  
        return adjCurv;
    }      
}
