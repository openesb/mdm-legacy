/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.util.NumbersManipulation;
import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;
import java.text.NumberFormat;

/**
 * This code computes the ratio of the number of bigrams in common
 * in two different strings to the average length of the strings.
 * It is then adjusted to be conform to a similar value in a standard
 * string comparators.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class ProratedComparator implements MatchComparator {

    private Map<String, Map> params;
    // The list of parameters
    private Map<String, String> theParams;      
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;
    }

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
    }    
    
    /**
     * Close any related data sources streams
     */
    public void stop() {}   
    
    /**
     * Empty method used for the exact comparison and the prorated comparison
     * that are hardcoded inside the code.
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @return     a real number that measures the similarity
     */
    public double compareFields(String recordA, String recordB, Map context) 
        throws MatchComparatorException {
        
        Number numA = null;
        Number numB = null;
        double relDiff;
        double doubleRecA = 0;
        double doubleRecB = 0;
        double range = 0;

        // The list of parameters
        theParams = this.params.get(context.get("fieldName"));
        int rangeP = Integer.parseInt(theParams.get("range"));
        int tolerance1 = Integer.parseInt(theParams.get("tolerance1"));
        int tolerance2 = Integer.parseInt(theParams.get("tolerance2"));

        // Handle the number format
        NumberFormat nf = NumberFormat.getInstance(Locale.US);

        // Before parsing, make sure that both records are numbers
        if (NumbersManipulation.isNumber(recordA) && NumbersManipulation.isNumber(recordB)) {
            try {
                numA = nf.parse(recordA);
                numB = nf.parse(recordB);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            if (numA instanceof Long) {
                doubleRecA = ((double) numA.longValue());

            } else if (numA instanceof Double) {
                doubleRecA = numA.doubleValue();
            }

            if (numB instanceof Long) {
                doubleRecB = ((double) numB.longValue());

            } else if (numB instanceof Double) {
                doubleRecB = numB.doubleValue();
            }
        }

        // Define the minimum value and the difference between the year-fields.
        if (doubleRecA > doubleRecB) {
            relDiff = doubleRecA - doubleRecB;                   
        } else {
            relDiff = doubleRecB - doubleRecA;
        }               


        //
        if (relDiff < tolerance1) {
            return 1.;

        } else if (relDiff >= (rangeP - tolerance2)) {

            return 0.;

        } else {

            range = rangeP - tolerance2 - tolerance1;

            relDiff -= tolerance1;

            double secondaryComp = 1 - relDiff / range;

            // If the calculated weight is lower than the minimum
            // authorized limit, replace it with the minimum value.
            if (secondaryComp < 0) {
                secondaryComp = 1;
            }

            return secondaryComp;
        }
    }
}
