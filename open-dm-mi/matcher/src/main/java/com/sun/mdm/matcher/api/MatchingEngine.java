/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.api;

import com.sun.mdm.matcher.comparators.configurator.ComparatorsConfigurationService;
import com.sun.mdm.matcher.weightcomp.MatchProcessor;
import com.sun.mdm.matcher.configurator.MatchConfigFile;
import com.sun.mdm.matcher.configurator.MatchVariables;
//import com.sun.mdm.matcher.weightcomp.MatchCounts;
import com.sun.mdm.matcher.emalgorithm.EMAlgorithm;
import com.sun.mdm.matcher.emalgorithm.PatternsCounter;

//import com.sun.mdm.matcher.util.ReadMatchConstantsValues;
import com.sun.mdm.matcher.comparators.ComparatorsManager;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;

import java.io.InputStream;
import java.io.IOException;
import java.text.ParseException;


/**
 * Countains all the public methods used to calculate the probalilistic matching weights
 * between any two compared records (or blocks of records) in the reference and candidate data.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatchingEngine {
    
    /*
     * Used as a flag for the one-to-one matching option.
     * The default choice is one-to-many matching.
     */
    private boolean isOneToOneMatch = false;
    
    /*
     * An instance of the base class that process all the matching tasks
     */
    private MatchProcessor matchPro;
    
    /*
     * An instance of the base class that process all the matching tasks
     */
    private MatchConfigFile mConfig;
    
    /*
     * An instance of the class that hold all variables and parameters
     * in the matching engine
     */
    private MatchVariables matchVar;

    /*
     * An instance of the class that calculates the patterns-frequencies
     * if applicable (works with the EM-Algorithm).
     */
    private PatternsCounter pattCount;

    /* 
     * An instance of the EM-Algorithm class that calculates the 
     * conditional probabilities.
     */
    private EMAlgorithm emAl;
    
    /*
     * Provides public methods to read and set the number of
     * different quantities
     */
    private ComparatorsConfigurationService configService;

    /*
     * The input stream for the matching configuration file. 
     */
    private InputStream matchConfigStream;
//    private InputStream comparatorsConfigStream;
    
    /* 
     * The match comparators' registry class
     */
    private ComparatorsManager comparatorsMgr;
    // The resources path
    private String resourcesPath;
    
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();

    /**
     * Setter for the resources path
     * 
     * @param resourcesPath the path
     */
    public void setResourcesPath(String resourcesPath) {
        this.resourcesPath = resourcesPath;
    }
    
    /**
     * Returns an instance of the MatchVariables class
     * 
     * @param mEng a MatchingEngine object
     * @return a MatchVariables object
     */
    public MatchVariables getMatchVar(MatchingEngine mEng) {
        return mEng.matchVar;
    }
    
    /**
     * Returns an instance of the MatchCounts class
     * 
     * @param mEng a MatchingEngine object
     * @return a MatchCounts object
     */
    public ComparatorsManager getComparatorManager(MatchingEngine mEng) {
        return mEng.comparatorsMgr;
    }
    
    
    /**
     * Returns an instance of the MatchConfigFile class
     * 
     * @param mEng a MatchingEngine object
     * @return a MatchConfigFile object
     */
    public MatchConfigFile getMatchConfig(MatchingEngine mEng) {
        return mEng.mConfig;
    }
    
    
    /**
     * Default constructor
     * 
     * @param filesAccess a stream to the data files
     * @throws MatcherConfigurationException a missing file exception
     * @throws SbmeMatchingException a matching exception
     * @throws IOException an I/O exception
     */
    public MatchingEngine(ConfigFilesAccess filesAccess)
        throws MatcherConfigurationException, MatcherException, IOException {

        try {
            // Get input stream from the match configuration file.
            matchConfigStream = filesAccess.getConfigFileAsStream("matchConfigFile.cfg");    
            mConfig = MatchConfigFile.getInstance(matchConfigStream);  
            
            matchVar = new MatchVariables(mConfig.getMatchFieldsNumber());            
            matchPro = new MatchProcessor();

            if (matchVar.eMSwitch > 0) {
                pattCount = PatternsCounter.getInstance();
                emAl = EMAlgorithm.getInstance();
            }
            
        } catch (MatcherException ex) {
            mLogger.severe(mLocalizer.x("MEG500: Matching engine initialization and loading of matchConfigFile failed: {0}", 
                                        ex.getMessage()));
        }
    }

    
    /**
     * Default constructor
     * 
     * @param filesAccess a stream to the data files
     * @throws MatcherConfigurationException a missing file exception
     * @throws SbmeMatchingException a matching exception
     * @throws IOException an I/O exception
     */
    public MatchingEngine(MatchConfigFilesAccess filesAccess)
        throws MatcherConfigurationException, MatcherException, IOException {

        try {    
            // Get the input stream for the matching configuration file 
            // to calculate the number of records
            matchConfigStream = filesAccess.provideMatchConfigFile();
            // Update the resources path
            this.resourcesPath = filesAccess.getConfigPath();
                    
            mConfig = MatchConfigFile.getInstance(matchConfigStream);
            matchVar = new MatchVariables(mConfig.getMatchFieldsNumber());
            // Get the input stream for the matching configuration file 
            // to read all the associated config. data.
            matchConfigStream = filesAccess.provideMatchConfigFile();
            matchPro = new MatchProcessor();
            
            if (matchVar.eMSwitch > 0) {
                pattCount = PatternsCounter.getInstance();
                emAl = EMAlgorithm.getInstance();
            }
        } catch (MatcherException ex) {
            mLogger.severe(mLocalizer.x("MEG501: Matching engine initialization and loading of matchConfigFile failed: {0}", 
                                        ex.getMessage()));
        }
    }    
    
    
    /**
     * Initialization method that upload all the data files.
     * 
     * @param filesAccess a stream to the data files
     * @throws MatcherConfigurationException a missing file exception
     * @throws SbmeMatchingException a matching exception
     * @throws IOException an I/O exception
     * @deprecated Please do not use it anymore. Use only the constructor & the upload 
     * method at startup.
     */
    @Deprecated public void initializeData(ConfigFilesAccess filesAccess) 
        throws MatcherConfigurationException, MatcherException, IOException {
        
        try {
            // Get the input stream for the configuratin file matchConfigStream =
            matchConfigStream = filesAccess.getConfigFileAsStream("matchConfigFile.cfg");
            
        } catch (Exception ex) {
            mLogger.severe(mLocalizer.x("MEG502: Matching engine match configuration initialization (matchConfigFile): {0}", 
                                        ex.getMessage()));
        }
    }
    
    
    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)
     * 
     * @param domain geographic location of the compared data
     * @throws MatcherException a generic match exception
     * @deprecated Please use the upLoadConfigFile() with no argument.
     * @see upLoadConfigFile() 
     */
    @Deprecated public void upLoadConfigFile(String domain)
        throws MatcherException, InstantiationException {
        
        try {  
            String resPath;
            if (this.resourcesPath != null) {
                resPath = this.resourcesPath ;
            } else {
                resPath = "match/";
            }            
            // Register the configured match comparators list
            comparatorsMgr = new ComparatorsManager(resPath+"comparatorsList.xml", true, this);
            
            // Reads the string comparator type, the initial probability/frequency 
            // tables, and the down weighting parameters            
            mConfig.getConfigData(matchConfigStream, this, domain);
            
        } catch (IOException ex) {
            mLogger.severe(mLocalizer.x("MEG503: Either comparatorsList/matchConfigFile Matching configuration Files Not Found: {0}", 
                                        ex.getMessage()));            
            throw new MatcherException("Either comparatorsList/matchConfigFile Matching configuration Files Not Found", ex);
        }
    }
    
    /**
     * Uploads the matching configuration file used by
     * one specific user (one instance)
     *
     * @throws MatcherException a generic match exception
     */
    public void upLoadConfigFile()
        throws MatcherException, InstantiationException {
        
        try { 
            String resPath;
            if (this.resourcesPath != null) {
                resPath = this.resourcesPath ;
            } else {
                resPath = "match/";
            }            
            // Register the configured match comparators list
            comparatorsMgr = new ComparatorsManager(resPath+"comparatorsList.xml", true, this);
            
            // Reads the type-specific comparators, the initial probabilities/frequency 
            // tables, and the down-weighting parameters            
            // Get the input stream for the matching configuration file        
            mConfig.getConfigData(matchConfigStream, this);
            
        } catch (IOException ex) {
            mLogger.severe(mLocalizer.x("MEG504: Either comparatorsList/matchConfigFile Matching configuration Files Not Found: {0}", 
                                        ex.getMessage()));            
            throw new MatcherException("Either comparatorsList/matchConfigFile Matching configuration Files Not Found", ex);
        }
    }
    
        
    /**
     * Returns the status of the one-to-one matching flag.
     * @return a boolean
     */
    public boolean getOneToOneMatch() {
        return isOneToOneMatch;
    }
    
    
    /**
     * Updates the value (true/false) of the one-to-one matching flag.
     *
     * @param value the one to one match value
     */
    public void setOneToOneMatch(boolean value) {
        isOneToOneMatch = value;
    }
    
    
    /**
     * Clean up the memory from any data related to a given Matching Engine
     * instance that is not needed anymore
     */
    public void shutdown() {
    }
    
    
    /**
     * Matches a candidate record against a reference record (defined by
     * instances of the MatchRecord class) and returns the composite weight
     * of all the matching fields within the MatchRecord object.
     *
     * @param candRec an instance of MatchRecord representing a candidate record
     * @param refRec an instance of MatchRecord representing a reference record
     * @return the composite match weight of the compared records.
     * @throws MatcherException the matching exception class
     */
    public double matchWeight(MatchRecord candRec, MatchRecord refRec)
    throws MatcherException {
        
        // Handles all the logic for a record-to-record matching
        return matchPro.performMatch(candRec, refRec, this);
    }
    
    
    /**
     * Matches a candidate record (an array of string fields) against a reference record 
     * (also defined as an array of string fields, for performance reasons) and returns 
     * the composite weight of all the matching fields within the MatchRecord object.
     *
     * @param matchFieldsIDs the fields' names
     * @param candRecVals array of values of the fields in the candidate record
     * @param refRecVals the associated values of the reference record
     * @return the composite match weight of the compared records.
     * @throws MatcherException the matching exception class
     */
    public double matchWeight(String[] matchFieldsIDs, String[] candRecVals,
                              String[] refRecVals)
    throws MatcherException {
        
        // Handles all the logic for a record-to-record matching
        return matchPro.performMatch(matchFieldsIDs, candRecVals, refRecVals, this);
    }


    /**
     * Matches a candidate record (an array of string fields) against a reference record 
     * (also defined as an array of string fields, for performance reasons) and returns 
     * an array of field's weights of the MatchRecord object.
     *
     * @param matchFieldsIDs the fields' names
     * @param candRecVals array of values of the fields in the candidate record
     * @param refRecVals the associated values of the reference record
     * @return an array of field's weights
     * @throws MatcherException the matching exception class
     */
    public double[] matchWeights(String[] matchFieldsIDs, String[] candRecVals,
                              String[] refRecVals)
    throws MatcherException {
        
        // Performs a record-to-record comparison
        return null;
    }
    
    
    /**
     * Matches a candidate record against an array of reference records and returns all 
     * the associated composite weights.
     *
     * @param candRec the candidate record
     * @param refRecArray array of reference records.
     * @return an array of composite weights
     * @throws MatcherException the matching exception class
     */
    public double[] matchWeight(MatchRecord candRec, MatchRecord[] refRecArray)
    throws MatcherException {
        
        // Performs a record-arrayOfRecord matching process
        return matchPro.performMatch(candRec, refRecArray, this);
    }
    
    /**
     * Matches a candidate record (an array of string fields) against a set of reference records 
     * (also defined as a 2D array of string fields, for performance reasons) and returns 
     * the associated composite weights.
     *
     * @param matchFieldsIDs  the types of the fields
     * @param candRecVals the value of the candidate record
     * @param refRecArrayVals 2D array of values associated with the fields of
     *                        an array of the reference records
     * @return the associated composite weights.
     * @throws MatcherException the matching exception class
     */
    public double[] matchWeight(String[] matchFieldsIDs, String[] candRecVals,
    String[][] refRecArrayVals)
    throws MatcherException {
        
        // Performs the record-array of records matching logic.
        return matchPro.performMatch(matchFieldsIDs, candRecVals, refRecArrayVals, this);
    }
    
    
    /**
     * Matches an array of candidate records against an array of reference records and returns all 
     * the associated composite weights as a 2D array.
     *
     * @param candRecArray an array of candidate records
     * @param refRecArray an array of reference records
     * @return a 2D array of the associated composite weights.
     * @throws MatcherException the matching exception class
     */
    public double[][] matchWeight(MatchRecord[] candRecArray, MatchRecord[] refRecArray)
    throws MatcherException {
        
        // Performs the array of records - array of records matching logic
        return matchPro.performMatch(candRecArray, refRecArray, this);
    }
    
    
    /**
     * Matches a set of candidate records (a 2D array of string fields) against a set of reference records 
     * (also defined as a 2D array of string fields, for performance reasons) and returns  all
     * the associated composite weights.
     *
     * @param matchFieldsIDs  the types of the fields
     * @param candRecArrayVals the set of candidate records
     * @param refRecArrayVals the set of reference records
     * @return a 2D array of the associated composite weights.
     * @throws MatcherException the matching exception class
     * @throws ParseException if a string parsing failed
     */
    public double[][] matchWeight(String[] matchFieldsIDs, String[][] candRecArrayVals,
        String[][] refRecArrayVals)
        throws MatcherException, ParseException {
        
        
        if (matchVar.eMSwitch > 0) {        
            // Create the frequency patterns
            pattCount.getPatternFrequencies(matchFieldsIDs, candRecArrayVals,
                                            refRecArrayVals, this);
            
            // Compute the fields' probabilities
            emAl.computeEMProba(matchFieldsIDs, this);
        }
        
        // Performs the array of records - array of records matching logic
        return matchPro.performMatch(matchFieldsIDs, candRecArrayVals,
            refRecArrayVals, this);
    }

    /**
     * Matches a set of candidate records (a 2D array of string fields) against a set of reference records 
     * (also defined as a 2D array of string fields, for performance reasons) and returns  all
     * the associated composite weights.
     *
     * @param matchFieldsIDs  the types of the fields
     * @param candRecArrayVals the set of candidate records
     * @param refRecArrayVals the set of reference records
     * @return a 3D array of the associated individual fields' weights plus the total composite weights.
     * @throws MatcherException the matching exception class
     * @throws ParseException if a string parsing failed
     */
    public double[][][] matchWeightDetails(String[] matchFieldsIDs, String[][] candRecArrayVals,
        String[][] refRecArrayVals)
        throws MatcherException, ParseException {
        
        
        if (matchVar.eMSwitch > 0) {        
            // Create the frequency patterns
            pattCount.getPatternFrequencies(matchFieldsIDs, candRecArrayVals,
                                            refRecArrayVals, this);
            
            // Compute the fields' probabilities
            emAl.computeEMProba(matchFieldsIDs, this);
        }
        
        // Performs the array of records - array of records matching logic
        return matchPro.performMatchDetails(matchFieldsIDs, candRecArrayVals,
            refRecArrayVals, this);
    }
    
    
    /**
     * Matches an array of records with each other (auto-matching)
     *
     * @param selfRecArray an array of records
     * @return a 2D array of the associated composite weights.
     * @throws MatcherException the matching exception class
     */
    public double[][] matchWeight(MatchRecord[] selfRecArray)
    throws MatcherException {
        
        // Perform the self-matching process
        return matchPro.performMatch(selfRecArray, this);
    }
    
    
    /**
     * Matches an array of records with each other (auto-matching)
     *
     * @param matchFieldsIDs  the types of the fields
     * @param selfRecArrayVals the set of records to be matched
     * @return a 2D array of the associated composite weights.
     * @throws SbmeMatchingException the matching exception class
     */
    public double[][] matchWeight(String[] matchFieldsIDs, String[][] selfRecArrayVals)
    throws MatcherException {
        
        // Perform the self-matching process
        return matchPro.performMatch(matchFieldsIDs, selfRecArrayVals, this);
    }
}
