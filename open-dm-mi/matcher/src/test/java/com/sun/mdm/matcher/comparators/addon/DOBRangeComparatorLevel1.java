/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.addon;

import java.util.Map;
import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;

/**
 * This comparator compares the DOB years, and returns a total agreement if the year
 * is the same, otherwise, it returns total disagreement.
 * 
 * This class comparator provides the most basic functionality of the match 
 * comparators plugin framework. It reads two strings, then performs a validation
 * step and compare the strings using only internal parameters/algorithm.
 * 
 * @author souaguenouni
 */
public class DOBRangeComparatorLevel1 implements MatchComparator {

    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {}

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
    }
    
     /**
     * Reads two strings and measure how close they are relying on an algorithm
     * that compare the proximity of the two strings (zero being very different and
     * one being identical)
     *
     * @param  recordA  Candidate's string record.
     * @param  recordB  Reference's string record.
     * @return  a real number between zero and one that measures the degree of similarity.
     */
    public double compareFields(String recordA, String recordB, Map context) 
       throws MatchComparatorException {

        // Compare the DOB year
        String sA = recordA.substring(recordA.lastIndexOf("/")+1);
        String sB = recordB.substring(recordB.lastIndexOf("/")+1);
        // Test if the date format is valid (mm/dd/yyyy)
        validateDOB(recordA, sA, recordB, sB);   
        
        int compare = Math.abs(Integer.parseInt(sB) - Integer.parseInt(sA));
        if (compare == 0) {
            return 1;
        } else {
            return 0;
        }
    } 
    
    private void validateDOB(String recordA, String sA, String recordB, String sB) 
        throws MatchComparatorException {
        
        // Test le length
        if (recordA.length() != 10 || recordB.length() != 10) {       
            throw new MatchComparatorException("The format should be mm/dd/yyyy");
        }
        if ((sA.length() != 4) || (sA.length() != 4)) {
System.out.println("zzzzzzz "+sA+"|"+sB);            
            throw new MatchComparatorException("The year should be have four digits: yyyy");
        }
        for (int i = 0; i < sA.length(); i++) {
            if (!Character.isDigit(sA.charAt(i))) {
                throw new MatchComparatorException("The year in recordA should be all digits: yyyy");
            }
            if (!Character.isDigit(sB.charAt(i))) {
                throw new MatchComparatorException("The year in recordB should be all digits: yyyy");
            }            
        }
    }
    
    /**
     * Close any related data sources streams
     */
    public void stop() {
        
    }
    
}
