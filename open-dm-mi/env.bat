set NB_HOME=C:\Mural.Legacy\netbeans
set GF_HOME=C:\Mural.Legacy\glassfish
set COMMON_LIB=C:\Mural.Legacy\mdm-legacy\trunk\open-common-lib
set MAVEN_HOME=C:\User\apache-maven-2.0.9
set JAVA_HOME=C:\Java\jdk1.6.0_21
set ANT_HOME=C:\User\apache-ant-1.8.0
set RUN_JUNIT=no

set PATH=%MAVEN_HOME%/bin;%PATH%
set ANT_OPTS=-Xmx512m
set MAVEN_OPTS=-Xmx512m

call mvn install:install-file -DgroupId=oracle.jdbc -DartifactId=ojdbc14 -Dversion=10.1.0.2.0 -Dpackaging=jar -Dfile=%COMMON_LIB%/ojdbc14.jar
call mvn install:install-file -DgroupId=msft.sqlserver.jdbc -DartifactId=sqljdbc -Dversion=1.0 -Dpackaging=jar -Dfile=%COMMON_LIB%/sqljdbc.jar
call mvn install:install-file -DgroupId=javaee -DartifactId=javaee-api -Dversion=5 -Dpackaging=jar -Dfile=%COMMON_LIB%/javaee.jar
call mvn install:install-file -DgroupId=org.netbeans.api -DartifactId=org-netbeans-modules-compapp-projects-base -Dversion=RELEASE65 -Dpackaging=jar -Dfile=%COMMON_LIB%/org-netbeans-modules-compapp-projects-base.jar



